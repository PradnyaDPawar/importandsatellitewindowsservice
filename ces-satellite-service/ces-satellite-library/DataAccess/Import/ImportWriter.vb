﻿Public Class ImportWriter
    Inherits BaseService

    Public Sub New(ByVal defId As Integer)

        MyBase.New(defId)

    End Sub
    Public Sub New(ByVal defId As Integer, serverip As String)

        MyBase.New(defId, serverip)

    End Sub

    Public Sub Upload(ByVal fileName As String, ByVal deType As String, ByVal content As Byte(), ByVal serverIp As String)

        Dim _deData = New WebReference.DeDataService()
        Dim _uri As Uri
        Dim str As String = "http://" + serverIp + "/DeDataService.asmx"
        If (Uri.TryCreate(str, UriKind.Absolute, _uri)) Then
            _deData.Url = _uri.AbsoluteUri

        End If

        deType = deType.Replace(" ", "")
        deType = deType.Replace("-", "")

        _returnState = _deData.ImportFile(Serial, GetCiphered, fileName, deType, content, ReturnMessage)

    End Sub


    Public Sub TestAuthenicate(ByVal serverip As String)

        Dim _deData = New WebReference.DeDataService()
        _deData.Url = "http://" + serverip + "/DeDataService.asmx"
        _returnState = _deData.Authenticate(Serial, GetCiphered)

    End Sub

End Class
