﻿Public Class Definition
    Inherits BaseEdit(Of Definition)

    Property Id() As Integer
        Get
            If GetItemValue("id") = "" Then

                Return 0

            Else
                Return GetItemValue("id")

            End If

        End Get
        Set(ByVal value As Integer)
            _itemRow("Id") = value
        End Set
    End Property

    Property AppId() As Integer
        Get
            Return GetItemValue("AppId")
        End Get
        Set(ByVal value As Integer)
            _itemRow("AppId") = value
        End Set
    End Property

    Property Name() As String
        Get
            Return GetItemValue("Name")
        End Get
        Set(ByVal value As String)
            _itemRow("Name") = value
        End Set
    End Property

    Property Key1() As String
        Get
            Return GetItemValue("key1")
        End Get
        Set(ByVal value As String)
            _itemRow("key1") = value
        End Set
    End Property

    Property Key2() As String
        Get
            Return GetItemValue("key2")
        End Get
        Set(ByVal value As String)
            _itemRow("key2") = value
        End Set
    End Property

    Property DeType() As String
        Get
            Return GetItemValue("detype")
        End Get
        Set(ByVal value As String)
            _itemRow("detype") = value
        End Set
    End Property

    Property DeClient() As String
        Get
            Return GetItemValue("declient")

        End Get
        Set(ByVal value As String)
            _itemRow("declient") = value
        End Set
    End Property

    Property Serial() As String
        Get
            Return GetItemValue("serial")

        End Get
        Set(ByVal value As String)
            _itemRow("serial") = value
        End Set
    End Property

    Property StartsWith() As String
        Get
            Return GetItemValue("startswith")

        End Get
        Set(ByVal value As String)
            _itemRow("startswith") = value
        End Set
    End Property

    Property EndsWith() As String
        Get
            Return GetItemValue("endswith")

        End Get
        Set(ByVal value As String)
            _itemRow("endswith") = value
        End Set
    End Property

    Property Contains() As String
        Get
            Return GetItemValue("contains")
        End Get
        Set(ByVal value As String)
            _itemRow("contains") = value
        End Set
    End Property

    Property DataDir() As String
        Get
            Return GetItemValue("datadir")
        End Get
        Set(ByVal value As String)
            _itemRow("datadir") = value
        End Set
    End Property

    Public Sub SetId()

        _itemRow("id") = _rowIndex

    End Sub

    Public Function GetContiansInc() As List(Of String)


        Dim returnList As New List(Of String)

        For Each condition As String In Contains.Split(",")

            If Not condition.Contains("!") Then

                returnList.Add(condition.Replace(",", "").Replace(" ", ""))

            End If

        Next

        Return returnList

    End Function

    Public Function GetContainsEx() As List(Of String)
        Dim returnList As New List(Of String)
        For Each condition As String In Contains.Split(",")
            If condition.Contains("!") Then
                returnList.Add(condition.Replace(",", "").Replace("!", "").Replace(" ", ""))
            End If
        Next
        Return returnList
    End Function

    Public Function GetKey1AsByte() As Byte()

        Return ConvertCsvToByte(Key1)

    End Function

    Public Function GetIVAsByte() As Byte()

        Return ConvertCsvToByte(Key2)

    End Function

    Private Function ConvertCsvToByte(ByVal csv As String) As Byte()

        Dim numbers As String() = csv.Split(",")
        Dim count As Integer = numbers.Count

        Dim returnBytes(count - 1) As Byte

        For i As Integer = 0 To count - 1

            Byte.TryParse(numbers(i), returnBytes(i))

        Next

        Return returnBytes

    End Function

End Class
