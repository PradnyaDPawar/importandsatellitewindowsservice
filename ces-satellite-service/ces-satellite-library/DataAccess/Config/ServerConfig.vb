﻿Public Class ServerConfig
    Private _xml As XDocument
    Private _fullName As String
    Private _autoStart As Boolean
    Private _definitions As New Definitions
    Private _startClientId As Integer
    Private _limit As Integer
    Private _logFolder As String
    Private _outFolder As String
    Private _triggers As String
    Private _identifier As String
    Private _sendFiles As Boolean
    Private _filePrefix As String
    Private _serverip As String
    Public Sub Load(ByVal configXml As XDocument)

        _xml = configXml
        Load()

    End Sub

    ReadOnly Property Definitions() As Definitions
        Get
            Return _definitions
        End Get
    End Property

    ReadOnly Property SendFiles() As Boolean
        Get
            Return _sendFiles
        End Get
    End Property

    ReadOnly Property AutoStart() As Boolean
        Get
            Return _autoStart
        End Get
    End Property

    ReadOnly Property StartClientId() As Integer
        Get
            Return _startClientId
        End Get
    End Property

    ReadOnly Property MaxClientId() As Integer
        Get
            Return _definitions.MaxAppId
        End Get
    End Property

    ReadOnly Property Limit() As Integer
        Get
            Return _limit
        End Get
    End Property

    ReadOnly Property LogFolder() As String
        Get
            Return _logFolder
        End Get
    End Property

    ReadOnly Property OutFolder() As String
        Get
            Return _outFolder
        End Get
    End Property

    ReadOnly Property Triggers() As String
        Get
            Return _triggers
        End Get
    End Property

    ReadOnly Property Identifier() As String
        Get
            Return _identifier
        End Get
    End Property

    ReadOnly Property FilePrefix() As String
        Get
            Return _filePrefix
        End Get
    End Property

    ReadOnly Property ServerIP() As String
        Get
            Return _serverip
        End Get
    End Property
    Private Sub LoadDefinitions()

        _definitions.Load(_xml)

    End Sub
    Public Sub VaildateImportType(ByVal allowedfullTypes As List(Of String))

        For Each definition As Definition In _definitions

            If Not allowedfullTypes.Contains(definition.DeType) Then
                definition.DeType = "Unknown"
            End If

        Next

    End Sub




    Private Function GetStartClientId() As Integer
        Try
            Return NullSafeToString.Convert(_xml...<StartClientId>.Value, "0")
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Function GetLimit() As Integer
        Try
            Return NullSafeToString.Convert(_xml...<Limit>.Value, "100")
        Catch ex As Exception
            Return 100
        End Try
    End Function

    Private Function GetLogFolder() As String
        Try
            Return NullSafeToString.Convert(_xml...<LogFolder>.Value, String.Empty).Trim()
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Function GetOutFolder() As String
        Try
            Return NullSafeToString.Convert(_xml...<OutFolder>.Value, String.Empty).Trim()
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Function GetTriggers() As String
        Try
            Return NullSafeToString.Convert(_xml...<Triggers>.Value, String.Empty).Trim()
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Function GetSendFiles() As Boolean
        Try
            If NullSafeToString.Convert(_xml...<SendFiles>.Value, "0").ToLower = "true" Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function GetIdentifier() As String
        Try
            Return NullSafeToString.Convert(_xml...<Identifier>.Value, String.Empty).Trim()
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Function GetFilePrefix() As String
        Try
            Return NullSafeToString.Convert(_xml...<FilePrefix>.Value, String.Empty).Trim()
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Function GetServerIp() As String
        Try
            Return NullSafeToString.Convert(_xml...<ServerIp>.Value, String.Empty).Trim()
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Function GetAutoStart() As Boolean

        Try
            If NullSafeToString.Convert(_xml...<AutoStart>.Value, "0").ToLower = "true" Then

                Return True

            Else
                Return False

            End If

        Catch ex As Exception

            Return False

        End Try

    End Function
    Public Sub Load()



        For Each type In _xml...<ServerConfig>

            'add each type as a new edit
            'for each format create one row in the table
            ''  For Each serverconfig In type.<ServerConfig>

            LoadDefinitions()

            _autoStart = GetAutoStart()

            _startClientId = GetStartClientId()

            _limit = GetLimit()

            _logFolder = GetLogFolder()

            _outFolder = GetOutFolder()

            _triggers = GetTriggers()

            _identifier = GetIdentifier()

            _sendFiles = GetSendFiles()

            _filePrefix = GetFilePrefix()

            _serverip = GetServerIp()



        Next




        ''  Next

    End Sub
End Class
