﻿Public Class Definitions
    Inherits BaseEdits(Of Definition)

    Dim _xml As XDocument
    Dim _maxAppId As Integer

    Property Xml() As XDocument
        Get
            Return _xml
        End Get
        Set(ByVal value As XDocument)
            _xml = value
        End Set
    End Property

    ReadOnly Property MaxAppId() As Integer ' Zero 0 Based
        Get
            Return _maxAppId
        End Get
    End Property

    Public Sub Load()

        CreateItems()

        Dim elements = _xml...<Definition>

        _maxAppId = 0

        For Each type In elements

            'add each type as a new edit
            'for each format create one row in the table
            For Each client In type.<clients>.<client>


                For Each deformat In client.<formats>.<format>

                    Add()
                    _edit.SetId()
                    _edit.AppId = NullSafeToString.Convert(type.<appid>.Value, "0")
                    _edit.DataDir = NullSafeToString.Convert(type.<datadir>.Value, "")
                    _edit.Name = NullSafeToString.Convert(type.<name>.Value, "")

                    _edit.Serial = NullSafeToString.Convert(client.<serial>.Value, "")
                    _edit.Key1 = NullSafeToString.Convert(client.<key1>.Value, "")
                    _edit.Key2 = NullSafeToString.Convert(client.<key2>.Value, "")
                    _edit.DeClient = NullSafeToString.Convert(client.<declient>.Value, "")


                    _edit.DeType = NullSafeToString.Convert(deformat.<detype>.Value, "")
                    _edit.StartsWith = NullSafeToString.Convert(deformat.<startswith>.Value, "")
                    _edit.EndsWith = NullSafeToString.Convert(deformat.<endswith>.Value, "")
                    _edit.Contains = NullSafeToString.Convert(deformat.<contains>.Value, "")
                Next

            Next


            If _edit.AppId > _maxAppId Then
                _maxAppId = _edit.AppId
            End If

        Next

    End Sub

    Public Sub Load(ByVal configXml As XDocument)

        _xml = configXml
        Load()

    End Sub

    Public Function ToXml() As XElement

        Dim returnXml = <Definitions>
                        </Definitions>

        For Each definition As Definition In Me

            If definition.Serial <> "" Then

                returnXml.Add(<Definition>
                                  <id><%= definition.Id %></id>
                                  <serial><%= definition.Serial %></serial>
                                  <key1><%= definition.Key1 %></key1>
                                  <key2><%= definition.Key2 %></key2>
                                  <startswith><%= definition.StartsWith %></startswith>
                                  <endswith><%= definition.EndsWith %></endswith>
                                  <contains><%= definition.Contains %></contains>
                                  <detype><%= definition.DeType %></detype>
                                  <declient><%= definition.DeClient %></declient>
                              </Definition>)

            End If

        Next

        Return returnXml

    End Function

    Public Function GetEditListByApptId(ByRef appId As Integer) As Definitions
        Dim definitonsByApp As New Definitions
        Dim defByapp As Definition

        definitonsByApp.CreateItems()

        For Each definition As Definition In Me
            If definition.AppId = appId Then
                defByapp = definitonsByApp.Add()
                defByapp.Id = definition.Id
                defByapp.AppId = appId
                defByapp.DataDir = definition.DataDir
                defByapp.DeClient = definition.DeClient
                defByapp.DeType = definition.DeType
                defByapp.EndsWith = definition.EndsWith
                defByapp.Key1 = definition.Key1
                defByapp.Key2 = definition.Key2
                defByapp.Name = definition.Name
                defByapp.Serial = definition.Serial
                defByapp.Contains = definition.Contains
                defByapp.StartsWith = definition.StartsWith
            End If
        Next

        Return definitonsByApp
    End Function

    Public Function GetEditByAppId(ByRef appId As Integer) As Definition
        For Each definition As Definition In Me
            If definition.AppId = appId Then
                Return definition
            End If
        Next
        Return Nothing
    End Function

    'Public Function GetEditBySerial(ByRef serial As String) As Definition

    '    For Each definition As Definition In Me

    '        If definition.Serial = serial Then
    '            Return definition

    '        End If

    '    Next

    '    Return Nothing

    'End Function

    Public Function GetEditById(ByRef id As Integer) As Definition

        For Each definition As Definition In Me

            If definition.Id = id Then
                Return definition

            End If

        Next

        Return Nothing

    End Function

    Public Sub CreateItems()

        _items = New DataTable

        _items.Columns.Add("id")
        _items.Columns.Add("appid")
        _items.Columns.Add("name")

        _items.Columns.Add("datadir")
        _items.Columns.Add("declient")
        _items.Columns.Add("serial")
        _items.Columns.Add("key1")
        _items.Columns.Add("key2")

        _items.Columns.Add("startswith")
        _items.Columns.Add("endswith")
        _items.Columns.Add("contains")
        _items.Columns.Add("detype")


    End Sub
    Public Sub Append(ByVal def As Definitions)
        For Each row In def.Items.Rows
            CreateItems()
            Dim rw = Me.Items.NewRow()
            rw("id") = row("id")

            rw("appid") = row("appid")
            rw("name") = row("name")
            rw("datadir") = row("datadir")
            rw("declient") = row("declient")
            rw("serial") = row("serial")
            rw("key1") = row("key1")
            rw("key2") = row("key2")
            rw("startswith") = row("startswith")
            rw("endswith") = row("endswith")
            rw("contains") = row("contains")
            rw("detype") = row("id")
        Next
    End Sub
End Class
