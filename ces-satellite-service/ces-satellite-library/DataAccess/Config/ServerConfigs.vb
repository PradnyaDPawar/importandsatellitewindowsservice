﻿Public Class ServerConfigs

    Private _sconfigs As List(Of ServerConfig)
    Private _xml As XDocument
    ReadOnly Property ConfigList() As List(Of ServerConfig)
        Get
            Return _sconfigs
        End Get
    End Property
    Public Sub Load(ByVal configXml As XDocument)

        _xml = configXml
        Load()

    End Sub


    Public Sub Load()
        _sconfigs = New List(Of ServerConfig)

        Dim elements = _xml...<config>

        For Each type In elements.Descendants("ServerConfig")

            'add each type as a new edit
            'for each format create one row in the table
            Dim xmlElement = New XDocument(New XElement(type))
            Dim _config = New ServerConfig
            _config.Load(xmlElement)
            _sconfigs.Add(_config)

        Next


    End Sub

End Class
