﻿
Imports System.IO

Public Class ConfigEdit
    'singleton of config

    Private _xml As XDocument
    Private _fileName As String
    Private _fullName As String
    Private _serverconfig As ServerConfigs
    Private Const _CFG_FILE As String = "config.xml"
    Private Const _APPDATA_DIR As String = "Cloud Energy Software"
    Private Const _APP_DIR As String = "CES Satellite Service"

    'singleton instance
    Private Shared _instance As ConfigEdit



    Private Sub New()
        'private new,  this is a singleton

        _fileName = "config.xml"
        _serverconfig = New ServerConfigs

    End Sub

    ReadOnly Property ServerConfig() As ServerConfigs
        Get
            Return _serverconfig
        End Get
    End Property

    Public Shared Function GetEdit() As ConfigEdit

        If _instance Is Nothing Then

            _instance = New ConfigEdit

        End If

        Return _instance

    End Function

    Private Function AppDir() As String

        Return My.Application.Info.DirectoryPath

    End Function

    Public Sub Load()

        Dim appDataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), _APPDATA_DIR)
        ' create the config file path from app data path and file name
        _fullName = Path.Combine(Path.Combine(appDataFolder, _APP_DIR), _CFG_FILE)

        'load if file exists
        If File.Exists(_fullName) Then

            Try

                LoadXml()
                LoadServerConfig()

            Catch ex As Exception
                Throw ex
            End Try
        Else
            Throw New ApplicationException("No Config File Found")
        End If

    End Sub

    Public Sub Save()

        _xml.Save(_fullName)

        'reload edit
        Load()

    End Sub





    Private Sub LoadServerConfig()

        _serverconfig.Load(_xml)

    End Sub

    Private Sub LoadXml()
        Try
            _xml = XDocument.Load(_fullName)
        Catch ex As Exception
            Throw New ApplicationException(ex.Message)
        End Try
    End Sub





End Class
