﻿Public Class DefinitionSerials

    Public Shared Function GetAll(ByVal definitions As Definitions) As List(Of String)

        'get all serials in all import types

        Dim serials As New List(Of String)

        For Each definition As Definition In definitions

            serials.Add(definition.Serial)

        Next

        Return serials

    End Function

End Class
