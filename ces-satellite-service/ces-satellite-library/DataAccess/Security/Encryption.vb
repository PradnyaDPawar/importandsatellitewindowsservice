﻿Imports System.Security.Cryptography
Imports System.IO

Namespace DataAccess

    Public Class Encryption

        Private _key() As Byte
        Private _iV() As Byte

        Property Key() As Byte()
            Get
                Return _key
            End Get
            Set(ByVal value As Byte())
                _key = value
            End Set
        End Property

        Property IV() As Byte()
            Get
                Return _iV
            End Get
            Set(ByVal value As Byte())
                _iV = value
            End Set
        End Property

        Public Sub New()

            Dim keyBuffer(31) As Byte
            Dim IVBuffer(15) As Byte

            _key = keyBuffer
            _iV = IVBuffer

        End Sub

        Public Sub New(ByVal key As Byte(), ByVal iV As Byte())

            'check key and vector lengths are 32 and 16 respectively 
            If key.Count = 32 Then
                _key = key
            Else
                _key(32) = New Byte
            End If

            If iV.Count = 16 Then
                _iV = iV

            Else
                _iV(16) = New Byte

            End If

        End Sub

        Public Function Verify(ByVal plainText As String, ByVal cipheredText As Byte()) As Boolean

            Dim result As Boolean = True
            Dim trueCipheredText As Byte() = Encrypt(plainText)

            For i As Integer = 0 To trueCipheredText.Count - 1

                If trueCipheredText(i) <> cipheredText(i) Then
                    result = False

                End If

            Next

            Return result

        End Function

        Public Function VerifyRange(ByVal plainTexts As List(Of String), ByVal cipheredKey As Byte()) As Boolean

            Dim result As Boolean = False

            For Each plainText As String In plainTexts

                If Verify(plainText, cipheredKey) Then
                    result = True

                End If

            Next

            Return result

        End Function

        Public Function Encrypt(ByVal plainText As String) As Byte()

            Try
                Dim encrypted() As Byte

                ' Create an AesManaged object
                ' with the specified key and IV.
                Using aesAlg As New AesManaged()

                    aesAlg.Key = _key
                    aesAlg.IV = _iV

                    ' Create a decrytor to perform the stream transform.
                    Dim encryptor As ICryptoTransform = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV)
                    ' Create the streams used for encryption.
                    Using msEncrypt As New MemoryStream()
                        Using csEncrypt As New CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write)
                            Using swEncrypt As New StreamWriter(csEncrypt)

                                'Write all data to the stream.
                                swEncrypt.Write(plainText)
                            End Using
                            encrypted = msEncrypt.ToArray()
                        End Using
                    End Using
                End Using

                ' Return the encrypted bytes from the memory stream.
                Return encrypted

            Catch ex As Exception

                Dim blank(32) As Byte


                For Each b As Byte In blank
                    b = 0

                Next

                Return blank
            End Try

        End Function

        Public Function Decrypt(ByVal cipherText() As Byte) As String
            ' Check arguments.
            If cipherText Is Nothing OrElse cipherText.Length <= 0 Then
                Throw New ArgumentNullException("cipherText")
            End If
            If Key Is Nothing OrElse Key.Length <= 0 Then
                Throw New ArgumentNullException("Key")
            End If
            If IV Is Nothing OrElse IV.Length <= 0 Then
                Throw New ArgumentNullException("Key")
            End If
            ' Declare the string used to hold
            ' the decrypted text.
            Dim plaintext As String = Nothing

            ' Create an AesManaged object
            ' with the specified key and IV.
            Using aesAlg As New AesManaged
                aesAlg.Key = _key
                aesAlg.IV = _iV

                ' Create a decrytor to perform the stream transform.
                Dim decryptor As ICryptoTransform = aesAlg.CreateDecryptor()

                ' Create the streams used for decryption.
                Using msDecrypt As New MemoryStream(cipherText)

                    Using csDecrypt As New CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)

                        Using srDecrypt As New StreamReader(csDecrypt)


                            ' Read the decrypted bytes from the decrypting stream
                            ' and place them in a string.
                            plaintext = srDecrypt.ReadToEnd()
                        End Using
                    End Using
                End Using
            End Using

            Return plaintext

        End Function

    End Class

End Namespace
