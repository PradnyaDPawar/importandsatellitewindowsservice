﻿Namespace DataAccess

    Public Class SecurityReader
        Inherits BaseService

        Public Sub New(ByVal defId As Integer)

            MyBase.New(defId)

        End Sub


        Public Function Authenticate() As Boolean

            Dim _deData = New WebReference.DeDataService()
            Return _deData.Authenticate(Serial, GetCiphered())

        End Function

    End Class


End Namespace