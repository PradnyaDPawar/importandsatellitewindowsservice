﻿Imports ces_satellite_library
Imports ces_satellite_library.DataAccess

Public Class BaseService
    'base object for data access service
    Protected _serial As String
    Protected _defId As Integer
    Protected _key1() As Byte
    Protected _iV() As Byte


    Protected _returnMessage As String
    Protected _returnState As Boolean


    ReadOnly Property Serial() As String
        Get
            Return _serial
        End Get

    End Property

    Property ReturnMessage() As String
        Get
            Return _returnMessage
        End Get
        Set(ByVal value As String)
            _returnMessage = value
        End Set
    End Property

    Property ReturnState() As Boolean
        Get
            Return _returnState

        End Get
        Set(ByVal value As Boolean)
            _returnState = value

        End Set
    End Property

    Public Sub New()

        _defId = -1

    End Sub

    Public Sub New(ByVal defId As Integer)

        _defId = defId

        Dim def As Definition = ConfigEdit.GetEdit.ServerConfig.ConfigList(0).Definitions.GetEditById(_defId)
        _serial = def.Serial
        _key1 = def.GetKey1AsByte
        _iV = def.GetIVAsByte

    End Sub
    Public Sub New(ByVal defId As Integer, ByVal serverip As String)

        _defId = defId

        Dim def As Definition = (From ele As ServerConfig In ConfigEdit.GetEdit.ServerConfig.ConfigList Where ele.ServerIP = serverip).FirstOrDefault().Definitions.GetEditById(_defId)
        _serial = def.Serial
        _key1 = def.GetKey1AsByte
        _iV = def.GetIVAsByte

    End Sub

    Public Function GetCiphered() As Byte()

        Try
            Dim encryption As New Encryption
            encryption.Key = _key1
            encryption.IV = _iV

            Return encryption.Encrypt(GetToken())

        Catch ex As Exception

            Dim returnByte() As Byte = {0, 0}
            Return returnByte

        End Try

    End Function

    Protected Function GetToken() As String

        Dim _deData = New WebReference.DeDataService()
        Return _deData.GetToken()

    End Function

End Class
