﻿Imports System.IO

Public Class ImportController
    Public Shared Function LoadFilesOnApp(ByVal currentClientId As Integer) As List(Of DataFiles)
        Dim configEdit As ConfigEdit = configEdit.GetEdit
        Dim dataFilesList As New List(Of DataFiles)
        Dim defByapp As Definition

        ''Dim defsByAppDup = New Definitions
        For Each con As ServerConfig In configEdit.ServerConfig.ConfigList
            Dim dataFiles As New DataFiles
            'get definitions by client
            Dim defsByApp As New Definitions
            defsByApp.CreateItems()
            dataFiles.ServerIp = con.ServerIP
            con.Definitions.Load()

            For Each definition As Definition In con.Definitions

                defByapp = defsByApp.Add()
                defByapp.Id = definition.Id
                defByapp.AppId = definition.AppId
                defByapp.DataDir = definition.DataDir
                defByapp.DeClient = definition.DeClient
                defByapp.DeType = definition.DeType
                defByapp.EndsWith = definition.EndsWith
                defByapp.Key1 = definition.Key1
                defByapp.Key2 = definition.Key2
                defByapp.Name = definition.Name
                defByapp.Serial = definition.Serial
                defByapp.Contains = definition.Contains
                defByapp.StartsWith = definition.StartsWith
            Next
            dataFiles.load(defsByApp)
            dataFilesList.Add(dataFiles)
            defsByApp.Items.Dispose()
        Next

        Return dataFilesList

    End Function



    Public Shared Sub ProcessFile(ByRef dataFile As DataFile, ByVal serverIp As String)

        '  If dataFiles.GetNext(nextFile) Then

        If dataFile.StatusCode = FileStatusEnum.Unidentified_File Then

            MoveUnidentified(dataFile)


        ElseIf dataFile.StatusCode = FileStatusEnum.Ready_To_Send Then
            'sent if identified
            Send(dataFile, serverIp)


        ElseIf dataFile.StatusCode = FileStatusEnum.Multiple_Formats Then
            MoveMultiidentified(dataFile)

        End If



    End Sub

    Public Shared Sub TestAuthernticate(ByRef dataFile As DataFile)

        Try
            'first load file
            Dim file As New FileIO
            file.Load(dataFile.FullName)

            'open writer to send file
            Dim importWriter As New ImportWriter(dataFile.DefId)

            importWriter.TestAuthenicate("")

            If importWriter.ReturnState Then
                dataFile.Message = "Success Test Authenticated"
            Else
                dataFile.Message = importWriter.ReturnMessage
            End If

        Catch ex As CustomException

            'known error
            dataFile.Message = "TestAuthenticateCustomExcpt : " & ex.Message
            dataFile.SetStatusCode(6)

        Catch ex As Exception

            'unhandled exception
            dataFile.Message = "TestAuthenticateExcpt : " & ex.Message
            dataFile.SetStatusCode(6)

        End Try

    End Sub

    Private Shared Sub Send(ByRef dataFile As DataFile, ByVal serverip As String)

        Try
            'first load file
            Dim file As New FileIO
            file.Load(dataFile.FullName)

            'open writer to send file
            Dim importWriter As New ImportWriter(dataFile.DefId, serverip)

            importWriter.Upload(file.Name, dataFile.DeType, file.GetFileAsByte, serverip)

            If importWriter.ReturnState Then
                FileSentPostProcess(dataFile)
            Else
                dataFile.Message = importWriter.ReturnMessage
                dataFile.SetStatusCode(FileStatusEnum.Send_Failed)
            End If

        Catch ex As CustomException

            'known error
            dataFile.Message = ex.Message
            dataFile.SetStatusCode(6)

        Catch ex As Exception

            'unhandled exception
            dataFile.Message = ex.Message
            dataFile.SetStatusCode(6)

        End Try

    End Sub

    Private Shared Sub MoveUnidentified(ByRef dataFile As DataFile)

        File.Move(dataFile.FullName, GetMovedFileName(dataFile.FullName, "Unidentified"))

    End Sub

    Private Shared Sub MoveMultiidentified(ByRef dataFile As DataFile)

        File.Move(dataFile.FullName, GetMovedFileName(dataFile.FullName, "MultiIdentified"))

    End Sub

    Private Shared Sub FileSentPostProcess(ByRef dataFile As DataFile)

        dataFile.SetStatusCode(FileStatusEnum.File_Sent)
        File.Move(dataFile.FullName, GetMovedFileName(dataFile.FullName, "Sent"))

    End Sub

    Private Shared Function GetMovedFileName(ByVal fileFullName As String, ByVal newDirName As String) As String

        Dim saveFileDir As String = Path.Combine(Path.GetDirectoryName(fileFullName), newDirName)
        Dim subFolderName As String = Date.Now.ToString("yyyy-MM-dd")

        'have a sub folder now
        saveFileDir = Path.Combine(saveFileDir, subFolderName)

        'create folder if not exist
        If Not Directory.Exists(saveFileDir) Then
            Directory.CreateDirectory(saveFileDir)
        End If

        Dim fileName As String = Path.GetFileName(fileFullName)

        Dim newFileFullName As String

        Dim dateTime As String = Date.Now.ToString
        dateTime = dateTime.Replace("/", "-")
        dateTime = dateTime.Replace(":", "_")


        For count As Integer = 0 To 100

            newFileFullName = "[]" & dateTime & "[]" & count.ToString & "[]" & fileName
            newFileFullName = Path.Combine(saveFileDir, newFileFullName)

            If Not File.Exists(newFileFullName) Then

                Return (newFileFullName)

            End If

        Next

        Return ""

    End Function

End Class
