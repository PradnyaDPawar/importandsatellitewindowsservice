﻿Public Class ImportFormatEdits
    Inherits BaseEdits(Of ImportFormatEdit)

    Public Function GetAllFullTypes() As List(Of String)

        Return ImportFormatFullTypes.GetAll(Me)

    End Function

    Public Function FullTypeContains(ByVal typeName As String) As Boolean

        If GetAllFullTypes.Contains(typeName) Then
            Return True
        Else
            Return False

        End If

    End Function


End Class
