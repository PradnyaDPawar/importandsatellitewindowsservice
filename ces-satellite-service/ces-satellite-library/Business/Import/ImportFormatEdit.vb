﻿Imports ces_satellite_library.DataAccess

Public Class ImportFormatEdit
    Inherits BaseEdit(Of ImportFormatEdit)

    ReadOnly Property ID() As Integer
        Get
            GetItemValue("ID")
        End Get
        
    End Property

    Property Type() As String
        Get
            Return GetItemValue("Type")
        End Get
        Set(ByVal value As String)
            _itemRow("Type") = value
        End Set
    End Property

    Property DeType() As String
        Get
            Return GetItemValue("DeType")
        End Get
        Set(ByVal value As String)
            _itemRow("DeType") = value
        End Set
    End Property

    Public Function GetFullTypeName() As String

        Return Type() & " - " & DeType

    End Function

End Class
