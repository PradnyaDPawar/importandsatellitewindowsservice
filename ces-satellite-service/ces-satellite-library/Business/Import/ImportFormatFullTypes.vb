﻿Public Class ImportFormatFullTypes

    Public Shared Function GetAll(ByRef importFormatEdits As ImportFormatEdits) As List(Of String)

        Dim deTypes As New List(Of String)

        For Each importType As ImportFormatEdit In importFormatEdits
            deTypes.Add(importType.GetFullTypeName)

        Next

        Return deTypes

    End Function

End Class
