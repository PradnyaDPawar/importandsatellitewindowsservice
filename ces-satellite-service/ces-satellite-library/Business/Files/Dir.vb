﻿Imports System.IO

Public Class Dir

    Private _files As List(Of String)
    Private _dirPath As String
    Private _excludeDir As List(Of String)

    ReadOnly Property Count() As Integer
        Get
            Return _files.Count
        End Get

    End Property

    Property ExcludedDir() As List(Of String)
        Get
            Return _excludeDir
        End Get
        Set(ByVal value As List(Of String))
            _excludeDir = value
        End Set
    End Property

    ReadOnly Property Files() As List(Of String)
        Get
            Return _files
        End Get

    End Property


    Public Sub New()
        _files = New List(Of String)
        _excludeDir = New List(Of String)
    End Sub

    ''' <summary>
    ''' Load all files in dir and sub dir
    ''' </summary>
    ''' <param name="dirPath">Path of parent dir</param>
    ''' <param name="excludeDir">List of excluded directories</param>
    ''' <remarks></remarks>
    Public Sub LoadAll(ByVal dirPath As String, ByVal excludeDir As List(Of String))

        _excludeDir = excludeDir

        LoadAll(dirPath)

    End Sub

    ''' <summary>
    ''' Load all files in dir and sub dir
    ''' </summary>
    ''' <param name="dirPath">Path of parent dir</param>
    ''' <remarks></remarks>
    Public Sub LoadAll(ByVal dirPath As String)

        _dirPath = dirPath

        'excluded directories are already set

        'load files in parent dir
        LoadDir(dirPath)

        'load all files in sub directories
        LoadSubDir(_dirPath)

    End Sub

    Private Sub LoadDir(ByVal dirPath As String)

        _files.AddRange(Directory.GetFiles(dirPath))

    End Sub

    Private Sub LoadSubDir(ByVal dirPath As String)
        'load all files in sub directories using recursion

        For Each dir As String In Directory.GetDirectories(dirPath)

            'exclude files from the sent folder
            If Not ExcludeDir(Path.GetFileName(dir).ToLower) Then

                _files.AddRange(Directory.GetFiles(dir))
                LoadSubDir(dir)

            End If



        Next

    End Sub

    Private Function ExcludeDir(ByVal dirName As String) As Boolean

        For Each exDir As String In _excludeDir

            'dir name found on exclusion list
            If exDir.ToLower = dirName Then
                Return True
            End If

        Next

        'dir not in exclusion list
        Return False

    End Function

End Class
