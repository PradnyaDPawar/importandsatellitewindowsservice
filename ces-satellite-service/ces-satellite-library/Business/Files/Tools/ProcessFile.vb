﻿Public Class ProcessFile

    Public Shared Function GetNext(ByRef dataFiles As DataFiles, ByRef rtnDataFile As DataFile) As Boolean

        For Each file As DataFile In dataFiles

            If file.StatusCode = FileStatusEnum.Ready_To_Send Or file.StatusCode = FileStatusEnum.Unidentified_File Then

                rtnDataFile = file
                Return True
            End If

        Next

        Return False

    End Function

    Public Shared Function GetSendNext(ByRef dataFiles As DataFiles, ByRef rtnDataFile As DataFile) As Boolean

        For Each file As DataFile In dataFiles

            If file.StatusCode = FileStatusEnum.Ready_To_Send Then

                rtnDataFile = file
                Return True
            End If

        Next

        Return False

    End Function



End Class
