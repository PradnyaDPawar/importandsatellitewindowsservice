﻿Public Enum FileStatusEnum

    Unidentified_File = 0
    Multiple_Formats = 1
    Ready_To_Send = 2
    Sending = 3
    Send_Failed = 4
    File_Sent = 5
    File_Error = 6

End Enum
