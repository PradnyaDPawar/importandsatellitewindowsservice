﻿Imports System.IO

Public Class UnknownFiles
    Inherits BaseEdits(Of UnknownFile)

    'count of physical files in parent and sub dir
    Private _physicalFileCount As Integer
    'max limit of edits to hold
    Private _limit As Integer

    ReadOnly Property PhysicalFileCount() As Integer
        Get
            Return _physicalFileCount
        End Get
    End Property

    Property Limit() As Integer
        Get
            Return _limit
        End Get
        Set(ByVal value As Integer)
            _limit = value
        End Set
    End Property

    Public Sub Load()

        CreateItems()
        LoadItems()

    End Sub

    Public Sub New()

        MyBase.New()

        'default limit to number of edits
        _limit = 100

    End Sub

    Private Sub CheckDir(ByVal dataDir As String)

        'create data directory
        If Not Directory.Exists(dataDir) Then

            Directory.CreateDirectory(dataDir)

        End If

    End Sub

    Private Sub CreateItems()
        'create table to store data

        _items = New DataTable

        _items.Columns.Add("filename")
        _items.Columns.Add("directory")
        _items.Columns.Add("datesent")

    End Sub

    Private Sub LoadItems()

        Dim config As ConfigEdit = ConfigEdit.GetEdit
        ' Dim unknownDir As String = config.GetUnknownDir

        'get all files in directory
        Dim dir As New Dir  'get files in all dir and sub dir
        'dir.LoadAll(unknownDir)

        _physicalFileCount = dir.Files.Count

        Dim currentFiles As List(Of String)

        'load upto the allowed edit limit
        If _physicalFileCount > _limit Then
            currentFiles = dir.Files.GetRange(0, _limit)

        Else
            currentFiles = dir.Files
        End If

        For Each file As String In currentFiles

            'add a new edit
            Add()

            'fill new edit
            _edit.SetFullName(file)

        Next

    End Sub







End Class
