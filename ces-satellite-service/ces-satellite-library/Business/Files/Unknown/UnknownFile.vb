﻿Imports System.IO

Public Class UnknownFile
    Inherits BaseEdit(Of UnknownFile)


    Property FileName() As String
        Get
            Return _itemRow("filename")
        End Get
        Private Set(ByVal value As String)
            _itemRow("filename") = value

        End Set
    End Property

    Property Directory() As String
        Get
            Return _itemRow("directory")

        End Get

        Private Set(ByVal value As String)
            _itemRow("directory") = value

        End Set

    End Property

    Property DateSent() As String
        Get
            Return _itemRow("datesent")

        End Get
        Private Set(ByVal value As String)
            _itemRow("datesent") = value

        End Set
    End Property

    Public Sub SetFullName(ByVal fileFullName As String)

        FileName = GetFileName(fileFullName)
        Directory = Path.GetDirectoryName(fileFullName)
        DateSent = GetDateSent(fileFullName)

    End Sub

    Private Function GetDateSent(ByVal fileName As String) As String

        Try
            If fileName.Contains("[]") Then

                Dim dateTime As String = fileName.Split("[]")(1)
                dateTime = dateTime.TrimStart("]").Replace("_", ":")

                Return dateTime

            Else
                Return ""

            End If

        Catch ex As Exception
            Return ""

        End Try

    End Function

    Private Function GetFileName(ByVal fileName As String) As String

        Try
            Dim splitFileName() As String = fileName.Split("[]")

            If fileName.Contains("[]") Then

                Return splitFileName.Last.TrimStart("]")

            Else
                Return Path.GetFileName(fileName)

            End If

        Catch ex As Exception
            Return ""

        End Try

    End Function

End Class
