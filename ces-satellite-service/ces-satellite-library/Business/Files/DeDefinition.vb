﻿Imports System.IO

Public Class DeDefinition

    Public Shared Function Identify(ByRef defsByApp As Definitions, ByVal fileName As String, ByRef deTypes As List(Of String)) As List(Of Integer)

        'work out which import type definition matches file, return list of defIds

        'Dim config As ConfigEdit = ConfigEdit.GetEdit

        Dim defIds As New List(Of Integer)

        For Each def As Definition In defsByApp

            If IsMatch(def.StartsWith, def.EndsWith, def.GetContiansInc, def.GetContainsEx, fileName) Then

                defIds.Add(def.Id)
                deTypes.Add(def.DeType)

            End If

        Next

        Return defIds

    End Function

    Private Shared Function IsMatch(ByVal startsWith As String, ByVal endsWith As String, ByVal containsInc As List(Of String), _
                                    ByVal containsEx As List(Of String), ByVal fileName As String) As Boolean

        'default to match
        Dim result As Boolean = True

        If startsWith <> "" Then

            If Not fileName.StartsWith(startsWith) Then

                result = False

            End If

        End If

        'test ends with
        If endsWith <> "" Then

            If Not Path.GetFileNameWithoutExtension(fileName).EndsWith(endsWith) Then

                result = False

            End If

        End If

        'test contains includes
        For Each condition As String In containsInc

            If condition <> "" Then

                If Not fileName.Contains(condition) Then

                    result = False

                End If

            End If

        Next

        'test contains excludes
        For Each condition As String In containsEx

            If condition <> "" Then

                If fileName.Contains(condition) Then

                    result = False

                End If

            End If

        Next

        Return result

    End Function

End Class
