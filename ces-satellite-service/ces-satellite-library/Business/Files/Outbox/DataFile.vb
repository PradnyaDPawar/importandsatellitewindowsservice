﻿Imports System.IO

Public Class DataFile

    Inherits BaseEdit(Of DataFile)

    Property Directory() As String
        Get
            Return _itemRow("Directory")
        End Get
        Set(ByVal value As String)
            _itemRow("Directory") = value
        End Set
    End Property

    Property Name() As String
        Get
            Return _itemRow("Name")
        End Get
        Set(ByVal value As String)
            _itemRow("Name") = value
        End Set
    End Property

    ReadOnly Property FullName() As String
        Get
            Return Path.Combine(Directory, Name)
        End Get
    End Property

    Property DefId() As Integer
        Get
            Return _itemRow("defid")
        End Get
        Set(ByVal value As Integer)
            _itemRow("defId") = value
        End Set
    End Property

    Property DeType() As String
        Get
            Return _itemRow("DeType")
        End Get
        Set(ByVal value As String)
            _itemRow("DeType") = value
        End Set
    End Property

    Property Status() As String
        Get
            Return _itemRow("Status")
        End Get

        Private Set(ByVal value As String)
            _itemRow("Status") = value
        End Set

    End Property

    Property StatusCode() As Integer
        Get
            Return _itemRow("StatusCode")
        End Get
        Private Set(ByVal value As Integer)
            _itemRow("StatusCode") = value
        End Set

    End Property

    Property Message() As String
        Get
            Return GetItemValue("message")
        End Get
        Set(ByVal value As String)

            _itemRow("message") = value

        End Set
    End Property



    Public Sub SetStatusCode(ByVal sCode As Integer)

        StatusCode = sCode
        Status = CType(StatusCode, FileStatusEnum).ToString().Replace("_", " ")

    End Sub

    Public Function ReadyToSend() As Boolean

        If StatusCode = FileStatusEnum.Ready_To_Send Then
            Return True

        Else
            Return False

        End If

    End Function

    Public Sub Delete()

        _itemRow.Delete()

    End Sub



End Class
