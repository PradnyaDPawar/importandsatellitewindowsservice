﻿Imports System.IO
Imports System.Linq

Public Class DataFiles
    Inherits BaseEdits(Of DataFile)

    'path of parent dir
    Private _dataDirPath As String
    'count of physical files in parent and sub dir
    Private _physicalFileCount As Integer
    'max limit of edits to hold
    Private _limit As Integer
    'Server ip to dump the files
    Private _serverip As String
    'list of definiton by client
    Private _defByApp As Definitions

    ReadOnly Property DataDirPath() As String
        Get
            CheckDir(_dataDirPath)
            Return _dataDirPath

        End Get
    End Property

    ReadOnly Property PhysicalFileCount() As Integer
        Get
            Return _physicalFileCount
        End Get
    End Property

    Property Limit() As Integer
        Get
            Return _limit
        End Get
        Set(ByVal value As Integer)
            _limit = value
        End Set
    End Property
    Property ServerIp() As String
        Get
            Return _serverip
        End Get
        Set(ByVal value As String)
            _serverip = value
        End Set
    End Property


    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub load(ByRef defsByApp As Definitions)

        CheckDir(defsByApp(0).DataDir)

        _dataDirPath = defsByApp(0).DataDir

        _defByApp = defsByApp

        'default limit to number of edits
        _limit = ConfigEdit.GetEdit.ServerConfig.ConfigList(0).Limit

        'create table to hold data
        CreateItems()

        'load files into table
        LoadItems()

    End Sub

    Public Function GetNext(ByRef dataFile As DataFile) As Boolean

        Return ProcessFile.GetNext(Me, dataFile)

    End Function

    Public Function GetSendNext(ByRef dataFile As DataFile) As Boolean

        Return ProcessFile.GetSendNext(Me, dataFile)

    End Function

    Private Sub CheckDir(ByVal dataDir As String)

        'create data directory
        If Not Directory.Exists(dataDir) Then

            Directory.CreateDirectory(dataDir)

        End If

    End Sub

    Public Sub CreateItems()
        'create table to store data
        If _items Is Nothing Then

            _items = New DataTable

            _items.Columns.Add("Name")
            _items.Columns.Add("Directory")
            _items.Columns.Add("DeType")
            _items.Columns.Add("Status")
            _items.Columns.Add("StatusCode")
            _items.Columns.Add("Message")
            _items.Columns.Add("defid")

        End If

    End Sub

    Private Sub LoadItems()
        'Xin : Better to use .Net 4.0 enumralor or ReadFirst in .net 2.0 and custom StreamReader
        Dim dir As New Dir  'get files in all dir and sub dir
        dir.ExcludedDir.Add("sent") 'add exclusion for sent dir
        dir.ExcludedDir.Add("unidentified") 'add exclusion for sent dir
        dir.ExcludedDir.Add("multiidentified") 'add exclusion for multiidentified
        dir.LoadAll(DataDirPath)

        _physicalFileCount = dir.Files.Count

        Dim currentFiles As List(Of String)

        'load upto the allowed edit limit
        If _physicalFileCount > _limit Then
            currentFiles = dir.Files.GetRange(0, _limit)

        Else
            currentFiles = dir.Files
        End If

        'amend the list of edits
        ' DeleteRemovedFiles(currentFiles)
        AddDataNewFiles(currentFiles)

    End Sub

    'Private Sub DeleteRemovedFiles(ByVal currentFiles As List(Of String))

    '    For Each dataFile As DataFile In Me
    '        'delete if physical file is not there any more
    '        If Not currentFiles.Contains(dataFile.FullName) Then
    '            dataFile.Delete()
    '        End If

    '    Next

    'End Sub

    Private Sub AddDataNewFiles(ByVal currentFiles As List(Of String))

        For Each file As String In currentFiles

            ' If Not SearchFileName(file) Then

            'add a new edit
            Add()

            'fill new edit
            SetFullName(file)

            'End If

        Next

    End Sub


    Public Sub SetFullName(ByVal fileFullName As String)
        _edit.Name = Path.GetFileName(fileFullName)
        _edit.Directory = Path.GetDirectoryName(fileFullName)

        'also determine the detype for the file
        SetDeType(_edit.FullName)

    End Sub

    Private Sub SetDeType(ByVal fullName As String)

        Dim deTypes As New List(Of String)
        Dim defIds As List(Of Integer) = DeDefinition.Identify(_defByApp, Path.GetFileName(fullName), deTypes)

        If defIds.Count = 1 Then

            _edit.SetStatusCode(FileStatusEnum.Ready_To_Send)
            _edit.DefId = defIds(0)
            _edit.DeType = deTypes(0)

        ElseIf defIds.Count = 0 Then

            _edit.SetStatusCode(FileStatusEnum.Unidentified_File)

        Else
            'more than one matching type
            _edit.SetStatusCode(FileStatusEnum.Multiple_Formats)

        End If

    End Sub

    'Private Function SearchFileName(ByVal newFileName As String) As Boolean

    '    For Each dataFile As DataFile In Me

    '        If dataFile.FullName = newFileName Then
    '            Return True
    '        End If

    '    Next

    'End Function

    Public Function GetFileStatus() 
        Return From df In _items.AsEnumerable _
                             Group df By Key = df.Field(Of String)("Status") Into Count() _
                             Select Status = Key, Count = Count

        'IEnumerable(of T)
    End Function

End Class
