﻿Public Class BrokenRule

    Private _itemRow As DataRow

    Property ExRowIndex() As Integer
        Get
            Return _itemRow("ExRowIndex")
        End Get
        Set(ByVal value As Integer)
            _itemRow("ExRowIndex") = value
        End Set
    End Property

    Property UserMessage() As String
        Get
            Return _itemRow("UserMessage")
        End Get
        Set(ByVal value As String)
            _itemRow("UserMessage") = value
        End Set
    End Property

    Property InternalMessage() As String
        Get
            Return _itemRow("InternalMessage")
        End Get
        Set(ByVal value As String)
            _itemRow("InternalMessage") = value
        End Set
    End Property

    Property Expected() As Boolean
        Get
            Return _itemRow("Expected")
        End Get
        Set(ByVal value As Boolean)
            _itemRow("Expected") = value
        End Set
    End Property

    Property ClassName() As String
        Get
            Return _itemRow("ClassName")
        End Get
        Set(ByVal value As String)
            _itemRow("ClassName") = value
        End Set
    End Property

    Property FunctionName() As String
        Get
            Return _itemRow("FunctionName")
        End Get
        Set(ByVal value As String)
            _itemRow("FunctionName") = value
        End Set
    End Property

    Public Function Load(ByRef itemRow As DataRow) As BrokenRule
        _itemRow = itemRow
        Return Me

    End Function
End Class
