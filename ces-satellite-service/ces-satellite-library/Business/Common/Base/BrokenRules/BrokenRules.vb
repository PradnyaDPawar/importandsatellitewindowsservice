﻿Public Class BrokenRules
    Implements IEnumerator, IEnumerable

    Private _items As DataTable
    Private _rowIndex As Integer

    Private _brokenRule As BrokenRule

    Private _showAll As Boolean
    Private _exRowIndex As Integer

    Public ReadOnly Property Count() As Integer
        Get
            If _showAll = True Then

                Return _items.Rows.Count

            Else

                Dim rowCount As Integer = _items.Select("ExRowIndex = '" & _exRowIndex & "'").Count
                Return rowCount

            End If

        End Get
    End Property

    Public ReadOnly Property Items() As DataTable
        Get
            Return _items
        End Get
    End Property

    Public ReadOnly Property ExRowIndex() As Integer
        Get
            If _showAll = True Then

                Return -1

            Else
                Return _exRowIndex

            End If

        End Get
    End Property


    Public Function GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator

        Reset()
        Return CType(Me, IEnumerator)

    End Function

    Public Sub Reset() Implements IEnumerator.Reset
        _rowIndex = -1
    End Sub

    Public ReadOnly Property Current() As Object Implements IEnumerator.Current
        Get
            Return _brokenRule.Load(ItemRow)
        End Get
    End Property

    Public Function MoveNext() As Boolean Implements IEnumerator.MoveNext

        If _showAll = True Then

            _rowIndex = _rowIndex + 1
            Return (_rowIndex < Count)

        Else

            Return MoveNextExtRowIndex()

        End If

    End Function

    Private Function MoveNextExtRowIndex() As Boolean

        'loop remaining broken rules
        While (_rowIndex + 1 < Count)

            _rowIndex = _rowIndex + 1
            _brokenRule.Load(ItemRow)

            'return if ex ref matches current ex ref
            If _brokenRule.ExRowIndex = _exRowIndex Then

                Return True

            End If

        End While

        'no more rules relating to the current ex ref
        Return False

    End Function



    Public Sub New()

        _items = New DataTable
        _brokenRule = New BrokenRule

        ShowAll()

        Load()

    End Sub

    Public Function ShowAll() As BrokenRules
        _showAll = True
        _exRowIndex = -1

        Return Me

    End Function

    Public Function Show(ByVal externalId As Integer) As BrokenRules

        _showAll = False
        _exRowIndex = externalId

        Return Me

    End Function

    Public Sub AddRule(ByVal userMessage As String, ByVal intMessage As String, _
                       ByVal expected As Boolean, ByVal className As String, _
                       ByVal functionName As String)

        _brokenRule.Load(AddRow)

        _brokenRule.UserMessage = userMessage
        _brokenRule.InternalMessage = intMessage
        _brokenRule.Expected = expected
        _brokenRule.ClassName = className
        _brokenRule.FunctionName = functionName
        _brokenRule.ExRowIndex = ExRowIndex

    End Sub

    Public Sub AddUnexpected(ByVal userMessage As String, ByVal intMessage As String)

        _brokenRule.Load(AddRow)

        _brokenRule.UserMessage = userMessage
        _brokenRule.InternalMessage = intMessage
        _brokenRule.Expected = False
        _brokenRule.ClassName = "Unknown"
        _brokenRule.FunctionName = "Unknown"
        _brokenRule.ExRowIndex = ExRowIndex

    End Sub



    Private Function ItemRow() As DataRow

        Return _items.Rows(_rowIndex)

    End Function

    Private Function AddRow() As DataRow

        Dim newRow As DataRow = _items.NewRow()
        _items.Rows.Add(newRow)

        _rowIndex = _items.Rows.IndexOf(newRow)

        Return newRow

    End Function

    Private Sub Load()

        'ad columns to broken rules table

        _items.Columns.Add("ExRowIndex")
        _items.Columns.Add("UserMessage")
        _items.Columns.Add("InternalMessage")
        _items.Columns.Add("Expected")
        _items.Columns.Add("ClassName")
        _items.Columns.Add("FunctionName")


    End Sub

End Class
