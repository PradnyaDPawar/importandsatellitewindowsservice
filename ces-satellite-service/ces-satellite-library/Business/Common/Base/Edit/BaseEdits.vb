﻿
'enumerbale base edits, works with base edit

Imports System.Collections

Public MustInherit Class BaseEdits(Of TEdit As {New, BaseEdit(Of TEdit)})

    Implements IEnumerator, IEnumerable

    'specific load implemented in child
    Protected Sub BaseLoad(ByRef items As DataTable)
        _items = items

    End Sub

    Protected _items As DataTable
    Protected _rowIndex As Integer

    Protected _brokenRules As BrokenRules

    Protected _edit As TEdit

    Public ReadOnly Property Count() As Integer
        Get
            Return _items.Rows.Count
        End Get
    End Property

    Public ReadOnly Property Items() As DataTable
        Get
            Return _items
        End Get
    End Property

    Public ReadOnly Property BrokenRules() As BrokenRules
        Get
            Return _brokenRules.ShowAll()

        End Get
    End Property


    Public Function GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator

        Reset()
        Return CType(Me, IEnumerator)

    End Function

    Public Function MoveNext() As Boolean Implements IEnumerator.MoveNext

        _rowIndex = _rowIndex + 1
        Return (_rowIndex < Count)

    End Function

    Public Sub Reset() Implements IEnumerator.Reset
        _rowIndex = -1
    End Sub

    Public ReadOnly Property Current() As Object Implements IEnumerator.Current
        Get
            Return _edit.Load(ItemRow, _rowIndex)

        End Get
    End Property


    Protected Sub New()

        _brokenRules = New BrokenRules

        _edit = New TEdit
        _edit.LoadBrokenRules(_brokenRules)

    End Sub

    Public Function Add() As TEdit

        Return _edit.Load(AddRow, _rowIndex)

    End Function

    Protected Function ItemRow() As DataRow

        Return _items.Rows(_rowIndex)

    End Function

    Protected Function AddRow() As DataRow

        Dim newRow As DataRow = _items.NewRow()
        _items.Rows.Add(newRow)

        _rowIndex = _items.Rows.IndexOf(newRow)

        Return newRow

    End Function

End Class
