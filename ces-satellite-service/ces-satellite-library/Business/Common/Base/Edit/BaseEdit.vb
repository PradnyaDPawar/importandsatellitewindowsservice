﻿Public MustInherit Class BaseEdit(Of TEdit As {BaseEdit(Of TEdit)})

    Protected _itemRow As DataRow

    Protected _brokenRules As BrokenRules

    Protected _rowIndex As Integer

    Public ReadOnly Property BrokenRules() As BrokenRules
        Get
            'restrict the view to only broken rules for this edit
            Return _brokenRules.Show(_rowIndex)

        End Get
    End Property

    Public Function Load(ByRef itemRow As DataRow, ByVal rowIndex As Integer) As TEdit

        _itemRow = itemRow
        _rowIndex = rowIndex

        Return Me

    End Function

    Public Sub LoadBrokenRules(ByRef brokenRules As BrokenRules)

        _brokenRules = brokenRules

    End Sub

    Protected Function GetItemValue(ByVal columnName As String) As String

        If _itemRow.Table.Columns.Contains(columnName) Then
            Return NullSafeToString.Convert(_itemRow(columnName), "")

        Else
            Return ""
        End If

    End Function

End Class
