﻿Imports System.IO

Public Class FileIO

    Private _fullName As String
    Private _file As FileInfo

    ReadOnly Property Fullname() As String
        Get
            Return _fullName
        End Get
      
    End Property

    ReadOnly Property Name() As String
        Get
            Return _file.Name
        End Get
    End Property


    Public Sub Load(ByVal fullName As String)

        _fullName = fullName
        LoadFile()

    End Sub

    Public Function GetFileAsByte() As Byte()

        Try
            Return File.ReadAllBytes(_fullName)

        Catch ex As Exception

            Dim customError As New CustomError
            Throw New CustomException(customError.FileError.LoadFileError, ex)

        End Try

    End Function

    Private Sub LoadFile()

        Try
            If System.IO.File.Exists(_fullName) Then
                _file = New FileInfo(_fullName)

            End If

        Catch ex As Exception

            Dim customError As New CustomError
            Throw New CustomException(CustomError.FileError.LoadFileError, ex)

        End Try

    End Sub

End Class
