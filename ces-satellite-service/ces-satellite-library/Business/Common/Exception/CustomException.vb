﻿Public Class CustomException
    Inherits ApplicationException

    Public Sub New(ByVal Message As String)
        MyBase.New(Message)
    End Sub

    'this new accepts an inner acception
    Public Sub New(ByVal message As String, ByVal inner As Exception)
        MyBase.New(message, inner)

    End Sub




End Class
