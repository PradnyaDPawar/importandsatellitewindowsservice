﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports BusinessLayer.Service.Sercurity
Imports BusinessLayer.Service

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="moc.ku.ygrenelatigid.www")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class DeDataService
    Inherits System.Web.Services.WebService

    '79.171.36.149

    <WebMethod()> _
    Public Function OpenConnTest() As String
        Return "Message From DE Service"

    End Function

    <WebMethod()> _
    Public Function GetToken() As String

        Return Tokens.GetToken()

    End Function

    <WebMethod()> _
    Public Function Authenticate(ByVal serial As String, ByVal cipheredKey As Byte()) As Boolean

        Return SecurityController.Authenticate(serial, cipheredKey)

    End Function

#Region "Files And Raw Formats"

    <WebMethod()> _
    Public Function GetImportFormats() As DataTable

    End Function

    <WebMethod()> _
    Public Function ImportFile(ByVal serial As String, ByVal cipheredKey() As Byte, _
                              ByVal fileName As String, ByVal importType As String, ByRef content As Byte(), ByRef returnMessage As String) As Boolean

        Dim service As New ServiceFacade(serial, cipheredKey)
        service.Import.Upload(fileName, importType, content, DateTime.Now)
        returnMessage = service.ReturnMessage

        Return service.ReturnState

    End Function

#End Region

End Class