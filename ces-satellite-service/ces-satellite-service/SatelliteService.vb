﻿Imports ces_satellite_library
Imports System.IO

Public Class SatelliteService
    Private _configEdit As ConfigEdit
    Private _dataFiles As DataFiles
    Private _unknownFiles As UnknownFiles
    Private _lastSendTime As DateTime
    Private _currentClientId As Integer
    Private Const _LOG_FILE As String = "ServiceLog.txt"
    Private Const _APPDATA_DIR As String = "Cloud Energy Software"
    Private Const _APP_DIR As String = "CES Satellite Service"
    Private _timer As System.Timers.Timer
    Private Shared _sentFiles As DataFiles
    'load the config file
    Private Function LoadConfig() As Boolean
        Try
            _configEdit = ConfigEdit.GetEdit
            _configEdit.Load()
            Return True
        Catch ex As Exception
            AddLogMsg("Fail to load config file.")
            AddLogMsg(ex.Message)
            AddLogMsg("Satellite is stopped")


            '' StopTimer()
        End Try
    End Function

    Private Sub UpdateCurrentClientId()
        _currentClientId += 1

        '' If _currentClientId > _configEdit.MaxClientId Then
        If _currentClientId > 0 Then
            'reset to 0
            _currentClientId = 0
        End If
    End Sub

    Private Sub StartTimer()
        'set start client Id
        ''_currentClientId = _configEdit.StartClientId
        _currentClientId = 0
        WorkerProcess()
        _timer.Start()
    End Sub

    Private Sub StopTimer()
        _timer.Stop()
        ''  TimerEndState()
        'stop the worker the current worker finish
        If BackgroundWorker1.WorkerSupportsCancellation = True Then
            BackgroundWorker1.CancelAsync()
        End If
    End Sub

    Private Sub WorkerProcess()
        If Not BackgroundWorker1.IsBusy = True Then
            ''  lbTransmitting.Visible = True
            BackgroundWorker1.RunWorkerAsync()
            'run doWork
        End If
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork


        Dim dataFiles As New List(Of DataFiles)
        Try
            dataFiles = ImportController.LoadFilesOnApp(_currentClientId)
            For Each dataFilesObj In dataFiles

                If dataFilesObj.Count > 0 Then

                    For i As Integer = 0 To dataFiles.Count - 1
                        'stop worker
                        If BackgroundWorker1.CancellationPending = True Then
                            e.Cancel = True
                            Exit For
                        End If
                        GenerateSendMsg(dataFilesObj)
                        ImportController.ProcessFile(dataFilesObj(i), dataFilesObj.ServerIp)
                        BackgroundWorker1.ReportProgress(CInt((i + 1)))
                        GenerateCompleteMsg(dataFilesObj)

                        dataFilesObj.Items.Dispose()
                    Next
                End If
            Next

            'load DataFiles


        Catch ex As Exception
            AddLogMsg("Fail to process file.")
            AddLogMsg(ex.Message)
            AddLogMsg("Satellite is stopped.")
            AddLogMsg(vbCrLf)

        Finally
            UpdateCurrentClientId()
        End Try



    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        AddLogMsg("File Processed: " & e.ProgressPercentage.ToString)
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        If e.Cancelled = True Then
            AddLogMsg("Worker Canceled!")
        ElseIf e.Error IsNot Nothing Then
            AddLogMsg("Worker Error :" & e.Error.ToString)
        Else
            AddLogMsg("Worker Completed!")
        End If

        BackgroundWorker1.Dispose()
    End Sub



#Region "Files"

    Private Sub GenerateSendMsg(ByRef dataFiles As DataFiles)

        'Get Msg Informations
        Dim definition As Definition = ConfigEdit.GetEdit.ServerConfig.ConfigList(0).Definitions.GetEditByAppId(_currentClientId)

        If definition Is Nothing Then
            Throw New ApplicationException("Cannot find current client definition.")
        End If

        Dim fileStatus = dataFiles.GetFileStatus

        AddLogMsg("Worker Start send files on client: " & "(" & definition.AppId & ") " & definition.Name)
        AddLogMsg(".................................. " & "Load Files " & dataFiles.Items.Rows.Count & "")

        For Each fs In fileStatus
            AddLogMsg(".................................. " & fs.Status & ": " & fs.Count)
        Next

        AddLogMsg("File Sending ......")

    End Sub

    Private Sub GenerateCompleteMsg(ByRef dataFiles As DataFiles)

        If Not dataFiles Is Nothing Then
            Dim fileStatus = dataFiles.GetFileStatus

            For Each fs In fileStatus
                AddLogMsg(".................................. " & fs.Status & ": " & fs.Count)
            Next

            'once there are some files have been processed
            If dataFiles.Count > 0 Then
                UpdateDataFileGrid(dataFiles)
            End If
        Else
            AddLogMsg(".................................. Unable to load file status.")
        End If


    End Sub

#Region " UI "

#Region "global variables"

    Delegate Sub updateLogDelegate(ByVal aMessage As String)
    Public updateLog As updateLogDelegate

    Delegate Sub updateSpaceDelegate()
    Public updateSpace As updateSpaceDelegate

    Delegate Sub updateGridDelegate(ByRef dataFiles As DataFiles)
    Public updateGrid As updateGridDelegate

#End Region

    'sub that updates datagridview on main thread:
    Public Sub AddLogMsg(ByVal aMessage As String)
        Dim appDataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), _APPDATA_DIR)
        ' create the config file path from app data path and file name
        Dim _fullName As String = Path.Combine(Path.Combine(appDataFolder, _APP_DIR), _LOG_FILE)
        Dim fileExists As Boolean = File.Exists(_fullName)
        If (fileExists) Then
            Using sw As New StreamWriter(File.Open(_fullName, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
                sw.WriteLine(DateTime.Now.ToString & " ------ " & aMessage & vbCrLf)
                sw.Close()
            End Using
        Else
            Using sw As New StreamWriter(File.Create(_fullName, FileMode.OpenOrCreate))
                sw.WriteLine(DateTime.Now.ToString & " ------ " & aMessage & vbCrLf)
                sw.Close()
            End Using
        End If

    End Sub



    Public Sub UpdateDataFileGrid(ByRef dataFiles As DataFiles)
        Dim str As String = ""
        str = "Processed Files:"
        For Each file As DataFile In dataFiles
            str += "----------------------" & vbCrLf
            str += "Name: " & file.Name & vbCrLf
            str += "Directory: " & file.Directory & vbCrLf
            str += "DeType: " & file.DeType & vbCrLf
            str += "Status: " & file.Status & vbCrLf
            str += "----------------------" & vbCrLf
        Next
        AddLogMsg(str)


    End Sub

#End Region

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        'reset config file
        ''from load

        _timer = New System.Timers.Timer(10000D) '' 10000 milliseconds = 10 seconds
        _timer.AutoReset = True
        AddHandler _timer.Elapsed, AddressOf _timer_Elapsed
        ''_timer.Start()

        'load config
        If LoadConfig() Then

            If _configEdit.ServerConfig.ConfigList(0).AutoStart Then
                StartTimer()
            Else
                StopTimer()
            End If

            AddLogMsg(ConfigEdit.GetEdit.ServerConfig.ConfigList(0).Identifier)
        End If


        _configEdit = Nothing
        If LoadConfig() Then
            StartTimer()
        End If
    End Sub
#End Region
    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

    Private Sub _timer_Elapsed(sender As Object, ByVal e As System.Timers.ElapsedEventArgs)
        WorkerProcess()
    End Sub
End Class
