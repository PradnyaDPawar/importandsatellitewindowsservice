﻿'enumerbale base edits, works with base edit

Imports System.Collections

    Public MustInherit Class BaseEdits(Of TEdit As {New, BaseEdit})

        Implements IEnumerator, IEnumerable

        Protected _items As DataTable
        Protected _rowIndex As Integer

        Protected _messages As MyMessages

        Protected _edit As TEdit

        'specific load implemented in child
        Public Sub BaseLoad(ByRef items As DataTable)
            _items = items

        End Sub

        Public ReadOnly Property Count() As Integer
            Get
                Return _items.Rows.Count
            End Get
        End Property

        Public ReadOnly Property Items() As DataTable
            Get
                Return _items
            End Get
        End Property

        Public ReadOnly Property Messages() As MyMessages
            Get
                Return _messages.ShowAll
            End Get
        End Property

        Protected Sub New()

            _messages = New MyMessages

            _edit = New TEdit
            _edit.LoadMessages(_messages)

    End Sub

        Public Function NewEdit() As TEdit

            _edit.Load(AddRow, _rowIndex)
            Return _edit

        End Function

        Protected Function ItemRow() As DataRow

            Return _items.Rows(_rowIndex)

        End Function

        Protected Function AddRow() As DataRow

            Dim newRow As DataRow = _items.NewRow()
            _items.Rows.Add(newRow)

            _rowIndex = _items.Rows.IndexOf(newRow)

            Return newRow

        End Function

#Region "enumerator functions"

        Public Function GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator

            Reset()
            Return CType(Me, IEnumerator)

        End Function

        Public Function MoveNext() As Boolean Implements IEnumerator.MoveNext

            _rowIndex = _rowIndex + 1
            Return (_rowIndex < Count)

        End Function

        Public Sub Reset() Implements IEnumerator.Reset
            _rowIndex = -1
        End Sub

        Public ReadOnly Property Current() As Object Implements IEnumerator.Current
            Get

                _edit.Load(ItemRow, _rowIndex)
                Return (_edit)

            End Get
        End Property

#End Region

    End Class
