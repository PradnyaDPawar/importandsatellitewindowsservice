﻿Public MustInherit Class BaseEdit

    Protected _itemRow As DataRow
    Protected _messages As MyMessages

    Protected _rowIndex As Integer

    Public ReadOnly Property Messages() As MyMessages
        Get
            Return _messages.ShowByExternalId(_rowIndex)

        End Get
    End Property

    Public ReadOnly Property RowState() As DataRowState
        Get
            Return _itemRow.RowState
        End Get
    End Property

    Public Sub RowState_SetModified()

        _itemRow.AcceptChanges()
        _itemRow.SetModified()

    End Sub

    Public Sub RowState_AcceptChanges()
        _itemRow.AcceptChanges()

    End Sub


    Public Sub Load(ByRef itemRow As DataRow, ByVal rowIndex As Integer)

        _itemRow = itemRow
        _rowIndex = rowIndex

    End Sub

    Protected Function GetItem(ByVal colName As String) As String
        'normally use this and throw exception in null or not exists
        Return _itemRow(colName)

    End Function

    Public Sub LoadMessages(ByRef messages As MyMessages)
        _messages = messages

    End Sub

End Class

