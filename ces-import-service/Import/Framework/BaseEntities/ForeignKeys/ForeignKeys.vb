﻿Public Class ForeignKeys

    Private _foreignKeys As List(Of ForeignKey)

    ReadOnly Property Keys() As List(Of ForeignKey)
        Get
            Return _foreignKeys
        End Get
    End Property


    Public Sub AddKeyConstraint(ByVal tableName As String, ByVal colName As String, ByVal foreignTable As String, ByVal foreignTableKey As String)

        _foreignKeys = New List(Of ForeignKey)

        Dim newFk As New ForeignKey

        newFk.TableName = tableName
        newFk.Colname = colName
        newFk.ForeignTable = foreignTable
        newFk.ForeignTableKey = foreignTableKey

        _foreignKeys.Add(newFk)

    End Sub

    Public Sub SetKeys(ByRef foreignTable As DataTable, ByRef srcTable As DataTable)

        For Each fk As ForeignKey In _foreignKeys

            'locate foreign table
            If fk.TableName = srcTable.TableName And fk.ForeignTable = foreignTable.TableName Then

                For Each row As DataRow In srcTable.Rows

                    'set key
                    If row(fk.Colname) <= 0 Then

                        Dim foreignRow As Integer = Math.Abs(row(fk.Colname))
                        row(fk.Colname) = foreignTable.Rows(foreignRow)

                    End If

                Next

            End If

        Next

    End Sub

End Class
