﻿Public Class ForeignKey

    Private _tableName As String
    Private _colName As String
    Private _foreignTable As String
    Private _foreignTableKey As String

    Property TableName() As String
        Get
            Return _tableName
        End Get
        Set(ByVal value As String)
            _tableName = value
        End Set
    End Property

    Property Colname() As String
        Get
            Return _colName
        End Get
        Set(ByVal value As String)
            _colName = value
        End Set
    End Property

    Property ForeignTable() As String
        Get
            Return _foreignTable
        End Get
        Set(ByVal value As String)
            _foreignTable = value
        End Set
    End Property

    Property ForeignTableKey() As String
        Get
            Return _foreignTableKey
        End Get
        Set(ByVal value As String)
            _foreignTableKey = value
        End Set
    End Property

End Class
