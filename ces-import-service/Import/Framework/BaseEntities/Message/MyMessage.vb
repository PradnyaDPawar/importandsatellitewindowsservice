﻿
Public Class MyMessage

    Private _externalId As Integer
    Private _text As String
    Private _brokenRule As Boolean

#Region "Properties"

    Property ExternalId() As Integer
        Get
            Return _externalId
        End Get
        Set(ByVal value As Integer)
            _externalId = value
        End Set
    End Property

    Property Text() As String
        Get
            Return _text
        End Get
        Set(ByVal value As String)
            _text = value
        End Set
    End Property

    Property BrokenRule() As Boolean
        Get
            Return _brokenRule
        End Get
        Set(ByVal value As Boolean)
            _brokenRule = value
        End Set
    End Property

#End Region

End Class
