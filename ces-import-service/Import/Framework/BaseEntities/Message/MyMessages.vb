﻿
Imports System.Collections

Public Class MyMessages
    Implements IEnumerator, IEnumerable

    Private _rowIndex As Integer
    Private _messages As List(Of MyMessage)

    Private _showAll As Boolean
    Private _editRowId As Integer

    Public ReadOnly Property Messages() As List(Of MyMessage)
        Get
            Return _messages
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            If _showAll = True Then

                Return _messages.Count

            Else

                Dim rowCount As Integer = 0

                For Each message As MyMessage In _messages

                    If message.ExternalId = _editRowId Then

                        rowCount += 1

                    End If

                Next

                Return rowCount

            End If

        End Get
    End Property

    Public Function BrokenRules() As Boolean

        If _showAll = True Then

            Dim rowCount As Integer = 0

            For Each message As MyMessage In _messages

                If message.BrokenRule = True Then

                    Return True

                End If

            Next

            Return False

        Else

            Dim rowCount As Integer = 0

            For Each message As MyMessage In _messages

                If message.ExternalId = _editRowId And message.BrokenRule = True Then

                    Return True

                End If

            Next

            Return False


        End If

    End Function

    Public Function ToCsv() As String

        Dim returnString As String = ""

        For Each message As MyMessage In _messages

            If message.ExternalId = _editRowId Then

                If returnString = "" Then

                    returnString += message.Text

                Else
                    returnString += ", " + message.Text

                End If

            End If

        Next

        Return returnString

    End Function

#Region "CRUD"

    Public Sub Add(ByVal messsage As MyMessage)

        _messages.Add(messsage)

    End Sub

    Public Sub Add(ByVal text As String, ByVal brokenRule As Boolean)

        Dim newMessage As New MyMessage

        newMessage.ExternalId = _editRowId
        newMessage.Text = text
        newMessage.BrokenRule = brokenRule

        _messages.Add(newMessage)

    End Sub

    Public Sub AddStandardMessage(ByVal message As MessageEnum)

        Add(MessageEnumToString.GetText(message), False)

    End Sub

    Public Sub AddStandardRule(ByVal colName As String, ByVal brokenRule As BrokenRuleEmun)

        'Add(BrokenRuleToString.GetText(colName, brokenRule), True)

    End Sub

    Public Sub AddRule(ByVal text As String)

        Add(text, True)

    End Sub

    Public Sub Clear()

        _messages = New List(Of MyMessage)


    End Sub


#End Region

#Region "Enumerators"

    Public Function GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator

        Reset()
        Return CType(Me, IEnumerator)

    End Function

    Public Sub Reset() Implements IEnumerator.Reset
        _rowIndex = -1
    End Sub

    Public ReadOnly Property Current() As Object Implements IEnumerator.Current
        Get
            Return _messages(_rowIndex)
        End Get
    End Property

    Public Function MoveNext() As Boolean Implements IEnumerator.MoveNext

        If _showAll = True Then

            _rowIndex = _rowIndex + 1
            Return (_rowIndex < Count)

        Else

            Return MoveNextByExternalId()

        End If

    End Function

    Private Function MoveNextByExternalId() As Boolean

        'loop remaining broken rules
        While (_rowIndex + 1 < Count)

            _rowIndex = _rowIndex + 1

            'return if ex ref matches current ex ref
            If _messages(_rowIndex).ExternalId = _editRowId Then

                Return True

            End If

        End While

        'no more rules relating to the current externalid
        Return False

    End Function

    Public Sub New()

        _messages = New List(Of MyMessage)

        ShowAll()

    End Sub

    Public Function ShowAll() As MyMessages
        _showAll = True
        _editRowId = -1

        Return Me

    End Function

    Public Function ShowByExternalId(ByVal editRowId As Integer) As MyMessages

        _showAll = False
        _editRowId = editRowId

        Return Me

    End Function

#End Region

End Class




