﻿Public Enum BrokenRuleEmun

    MustBeUnique = 1
    EmailIsNotValid = 2
    Required = 3
    DateNotInFuture = 4
    DateNotValid = 5
    MustExist = 6
    WordCountExcceded = 7

End Enum

Public Class BrokenRuleToString

    Public Shared Function GetText(ByVal colummName As String, ByVal brokenRuleEmun As BrokenRuleEmun) As String

        'Select Case brokenRuleEmun

        '    Case Business.BrokenRuleEmun.MustBeUnique
        '        Return colummName & " must be unique."

        '    Case Business.BrokenRuleEmun.EmailIsNotValid
        '        Return "Email is not valid."

        '    Case Business.BrokenRuleEmun.Required
        '        Return colummName & " is required."

        '    Case Business.BrokenRuleEmun.DateNotInFuture
        '        Return colummName & " cannot be in the future."

        '    Case Business.BrokenRuleEmun.DateNotValid
        '        Return colummName & " is not a valid date."

        '    Case Business.BrokenRuleEmun.MustExist
        '        Return colummName & " does not exist."

        '    Case Business.BrokenRuleEmun.WordCountExcceded
        '        Return colummName & ": word count exceeded"

        '    Case Else
        '        Return ""

        'End Select

        Return String.Empty

    End Function

End Class
