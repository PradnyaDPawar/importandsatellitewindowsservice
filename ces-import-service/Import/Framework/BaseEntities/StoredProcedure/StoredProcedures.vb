﻿Public Class StoredProcedures

    Private _procedures As List(Of StoredProcedure)

#Region "Gets and Properties"

    Property Procedures() As List(Of StoredProcedure)
        Get
            Return _procedures
        End Get
        Set(ByVal value As List(Of StoredProcedure))

        End Set
    End Property

    ''' <summary>
    ''' return insert procedure if there is one
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property Insert(ByVal tableName As String) As StoredProcedure
        Get
            For Each procedure As StoredProcedure In _procedures

                If procedure.ProcedureType = ProcedureTypeEnum.Insert And procedure.TableName = tableName Then
                    Return procedure

                End If
            Next

            Return Nothing
        End Get
    End Property

    ReadOnly Property Update(ByVal tableName As String) As StoredProcedure
        Get
            For Each procedure As StoredProcedure In _procedures

                If procedure.ProcedureType = ProcedureTypeEnum.Update And procedure.TableName = tableName Then
                    Return procedure

                End If
            Next

            Return Nothing
        End Get
    End Property

    ReadOnly Property Delete(ByVal tableName As String) As StoredProcedure
        Get
            For Each procedure As StoredProcedure In _procedures

                If procedure.ProcedureType = ProcedureTypeEnum.Delete And procedure.TableName = tableName Then
                    Return procedure

                End If
            Next

            Return Nothing
        End Get
    End Property

    Public Function GetTransResult() As Boolean

        For Each proc As StoredProcedure In _procedures

            'failed if any procedure failed
            If proc.GetResultValue = False Then
                Return False
            End If

        Next

        Return True

    End Function

#End Region

    Public Sub New()

        _procedures = New List(Of StoredProcedure)

    End Sub

    Public Sub Add(ByVal procedure As StoredProcedure)

        _procedures.Add(procedure)

    End Sub

    Public Sub Load_And_Add(ByVal procedureName As String)

        Dim procedure As StoredProcedure
        procedure = StoredProcedureReader.GetProcedure(procedureName)
        procedure.ProcedureType = ProcedureTypeEnum.Read

        _procedures.Add(procedure)

    End Sub

    Public Sub Load_Crud(ByVal tableName As String)

        'add insert
        Dim insert As StoredProcedure
        insert = StoredProcedureReader.GetProcedure("[dbo]." & tableName & "_I")
        insert.ProcedureType = ProcedureTypeEnum.Insert
        insert.TableName = tableName
        _procedures.Add(insert)

        'add update
        Dim update As StoredProcedure
        update = StoredProcedureReader.GetProcedure("[dbo]." & tableName & "_U")
        update.ProcedureType = ProcedureTypeEnum.Update
        update.TableName() = tableName
        _procedures.Add(update)

        'add update
        Dim delete As StoredProcedure
        delete = StoredProcedureReader.GetProcedure("[dbo]." & tableName & "_D")
        delete.ProcedureType = ProcedureTypeEnum.Delete
        delete.TableName = tableName

        _procedures.Add(delete)

    End Sub

    Public Function Contains(ByVal procedureName As String) As Boolean

        For Each procedure As StoredProcedure In _procedures

            If procedure.Name = procedureName Then
                Return True

            End If

        Next

        Return False

    End Function

    ''' <summary>
    ''' merge another stored procedures with this one
    ''' </summary>
    ''' <param name="newProcedures"></param>
    ''' <remarks></remarks>
    Public Sub Merge(ByRef newProcedures As StoredProcedures)

        For Each proc As StoredProcedure In newProcedures.Procedures

            _procedures.Add(proc)

        Next

    End Sub

End Class
