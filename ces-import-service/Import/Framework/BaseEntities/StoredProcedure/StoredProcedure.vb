﻿Public Class StoredProcedure

#Region "Property and Gets"

    Private _name As String
    Private _parameters As List(Of Parameter)
    Private _procedureType As ProcedureTypeEnum

    Private _tableName As String

    Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    Property Parameters() As List(Of Parameter)
        Get
            Return _parameters
        End Get
        Set(ByVal value As List(Of Parameter))
            _parameters = value
        End Set
    End Property

    Property TableName() As String
        Get
            Return _tableName
        End Get
        Set(ByVal value As String)
            _tableName = value

        End Set
    End Property


    Property ProcedureType() As ProcedureTypeEnum
        Get
            Return _procedureType
        End Get
        Set(ByVal value As ProcedureTypeEnum)
            _procedureType = value
        End Set
    End Property

    ''' <summary>
    ''' Custom result value from all commits
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetResultValue() As Boolean

        For Each param As Parameter In _parameters

            If param.Input = False And param.Name = "@result_out" Then

                'if nothing it was not used
                If param.Value <> 1 And param.Value <> Nothing Then

                    Return False

                End If

            End If

        Next

        Return True

    End Function

    Public Sub SetResultValue(ByVal value As Boolean)

        For Each param As Parameter In _parameters

            If param.Input = False And param.Name = "@result_out" Then

                param.Value = value

            End If

        Next

    End Sub


    Public Function GetReturnMessage() As String

        For Each param As Parameter In _parameters

            If param.Input = False And param.Name = "@message_out" Then

                Return param.Value

            End If

        Next

        Return ""

    End Function

    Public Sub SetParameterValue(ByVal paramName As String, ByVal value As String)

        For Each param As Parameter In _parameters

            If param.Name = paramName Then
                param.Value = value
            End If

        Next

    End Sub

#End Region

#Region "Functions"

    Public Sub New()

        _parameters = New List(Of Parameter)

    End Sub

    Public Sub New(ByVal name As String)

        _name = name
        _parameters = New List(Of Parameter)

    End Sub

    Public Sub Load(ByVal name As String)

        _name = name
        _parameters = StoredProcedureReader.GetProcedure(name).Parameters

    End Sub

    Public Sub AddValue(ByVal parameterName As String, ByVal value As String)

        For Each parameter As Parameter In _parameters

            If parameter.Name.Replace("@", "").ToLower = parameterName.Replace("@", "").ToLower Then

                parameter.Value = value

            End If

        Next

    End Sub

#End Region

#Region "Function Group: AddParameters"

    Public Sub AddParameter(ByVal name As String, ByVal value As String)

        Dim param As New Parameter
        param.Name = name
        param.Value = value
        param.Input = True
        param.SqlType = SqlDbType.Int

        _parameters.Add(param)

    End Sub

    Public Sub AddOutParameter_Int(ByVal name As String)

        Dim param As New Parameter
        param.Name = name
        param.Input = False
        param.SqlType = SqlDbType.Int

        _parameters.Add(param)

    End Sub

    Public Sub AddOutParameter_Nvarchar(ByVal name As String, ByVal size As Integer)

        Dim param As New Parameter
        param.Name = name
        param.Input = False
        param.SqlType = SqlDbType.NVarChar
        param.Size = size

        _parameters.Add(param)

    End Sub


#End Region

End Class
