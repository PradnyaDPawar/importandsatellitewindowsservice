﻿Public Class Parameter

    Private _name As String
    Private _sqlType As SqlDbType
    Private _size As Integer
    Private _input As Boolean
    Private _value As String

    Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    Property SqlType() As SqlDbType
        Get
            Return _sqlType
        End Get
        Set(ByVal value As SqlDbType)
            _sqlType = value
        End Set
    End Property

    Property Size() As Integer
        Get
            Return _size
        End Get
        Set(ByVal value As Integer)
            _size = value
        End Set
    End Property

    Property Input() As Boolean
        Get
            Return _input
        End Get
        Set(ByVal value As Boolean)
            _input = value
        End Set
    End Property

    Property Value() As String
        Get
            Return _value
        End Get
        Set(ByVal value As String)
            _value = value

        End Set
    End Property

End Class
