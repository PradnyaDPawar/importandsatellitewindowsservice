﻿Imports System.IO

Public Class DeConfig


    Private Shared _deConfig As DeConfig

    Private _importQueueDir As String
    Private _serverIP As String
    Private _configX As XDocument
    Private Const _CON_FILE As String = "config.xml"
    Private Const _APPDATA_DIR As String = "Cloud Energy Software"
    Private Const _APP_DIR As String = "CES Import Service"

    Sub New()

    End Sub

    ReadOnly Property ImportDir() As String
        Get
            Return ConvertToString.NullSafe(_configX...<ImportDir>.Value, "")
        End Get
    End Property

    ReadOnly Property ServerIP() As String
        Get
            Return ConvertToString.NullSafe(_configX...<ServerIP>.Value, "")
        End Get
    End Property

    ReadOnly Property ImportVersion() As String
        Get
            Return ConvertToString.NullSafe(_configX...<ImportVersion>.Value, "")
        End Get
    End Property


    Public Function GetConfig() As DeConfig
        If _deConfig Is Nothing Then
            Return LoadConfig()
        Else
            Return _deConfig
        End If
    End Function


    Private Function LoadConfig() As DeConfig
        Dim appDataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), _APPDATA_DIR)
        ' create the config file path from app data path and file name
        Dim _fullName As String = Path.Combine(Path.Combine(appDataFolder, _APP_DIR), _CON_FILE)
        _configX = XDocument.Load(_fullName)
        _deConfig = Me
        Return _deConfig
    End Function



End Class
