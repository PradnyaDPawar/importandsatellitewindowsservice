﻿Public Class DefaultConnection

    'Should load the server path from the config and hard code username and password
    'Private Const _server As String = "5.159.226.153"
    Private Const _userName As String = "de2"
    Private Const _passWord As String = "digitalenergy"

    Shared ReadOnly Property Server() As String
        Get
            Dim deConfig As New DeConfig
            Return deConfig.GetConfig.ServerIP
        End Get
    End Property

    'assumes windows authentification
    Private Const _winAuth As Boolean = False

    Shared ReadOnly Property UserName() As String
        Get
            Return _userName
        End Get
    End Property

    Shared ReadOnly Property Password() As String
        Get
            Return _passWord
        End Get
    End Property

    Shared ReadOnly Property WinAuth() As Boolean
        Get
            Return _winAuth
        End Get
    End Property


End Class
