﻿Imports System.IO


Public Class ConnectionString
    Private Const _CON_FILE As String = "config.xml"
    Private Const _APPDATA_DIR As String = "Cloud Energy Software"
    Private Const _APP_DIR As String = "CES Import Service"
    'Connection to the default database
    Public Shared Function DefaultDb() As String

        Dim connStr As String

        connStr = "Server = " & DefaultConnection.Server & ";"
        connStr &= "Initial Catalog = DigitalEnergyCentral;"

        If DefaultConnection.WinAuth Then

            'if win authentification, do not need Server, Username and Password
            connStr &= "Trusted_Connection=True;"

        Else

            connStr &= "UId = " & DefaultConnection.UserName & ";"
            connStr &= "PWD = " & DefaultConnection.Password & ";"

        End If

        Return connStr

    End Function

    'Connection to the default database
    Public Shared Function SpecificDb(ByVal dbName As String) As String

        Dim connStr As String

        connStr = "Server = " & DefaultConnection.Server & ";"
        connStr &= "Initial Catalog = " & dbName & ";"

        If DefaultConnection.WinAuth Then

            'if win authentification, do not need Server, Username and Password
            connStr &= "Trusted_Connection=True;"

        Else

            connStr &= "UId = " & DefaultConnection.UserName & ";"
            connStr &= "PWD = " & DefaultConnection.Password & ";"

        End If

        Return connStr

    End Function




    'location of config file
    Public Shared Function Config() As String
        Dim appDataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), _APPDATA_DIR)
        ' create the config file path from app data path and file name
        Dim _fullName As String = Path.Combine(Path.Combine(appDataFolder, _APP_DIR), _CON_FILE)
        'Change application path on web project
        Return _fullName

    End Function

    ''location of the log file  
    'Public Shared Function Log() As String

    '    Return Path.Combine(AppPath, "de_log.xml")

    'End Function

    Private Shared Function AppPath() As String

        'Change application path on web project
        Return My.Application.Info.DirectoryPath

    End Function


End Class
