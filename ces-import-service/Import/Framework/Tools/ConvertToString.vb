﻿
Public Class ConvertToString

    Public Shared Function NullSafe(Of T)(ByVal Value As T, ByVal DefaultValue As T) As T

        If Value Is Nothing Or IsDBNull(Value) Then
            Return DefaultValue
        Else
            Return Value
        End If

    End Function

End Class



