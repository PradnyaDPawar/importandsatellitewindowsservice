﻿Public Class DataTableTools

    'Private _transposedheaderrowindex As Integer

    '   Property TransposedHeaderRowIndex() As Integer

    '   Get
    '      Return _transposedheaderrowindex
    ' End Get
    '
    '   Set(ByVal value As Integer)
    '      _transposedheaderrowindex = value
    ' End Set
    '
    'End Property

    Public Shared Function GetNew(ByVal tableName As String, ByVal columnNames As List(Of String)) As DataTable

        Dim ds As New DataSet
        ds.Tables.Add(tableName)

        For Each col In columnNames
            ds.Tables(tableName).Columns.Add(col)
        Next

        Return ds.Tables(0)

    End Function

    Public Shared Function GetNew(ByVal tableName As String, ByVal colCount As Integer) As DataTable

        Dim ds As New DataSet
        ds.Tables.Add(tableName)

        For index As Integer = 0 To colCount - 1

            ds.Tables(tableName).Columns.Add(index.ToString)

        Next

        Return ds.Tables(0)

    End Function

    'Pain, can be refactored
    Public Shared Sub GetTranspose(ByRef dt As DataTable)
        Dim colcount, rowcount As Integer
        Dim transposeddt As DataTable = New DataTable()
        transposeddt.Columns.Add(New DataColumn("0", GetType(String)))

        For colcount = 0 To dt.Columns.Count - 1 Step 1

            Dim newrow As DataRow = transposeddt.NewRow
            newrow(0) = dt.Columns(colcount).ColumnName

            For rowcount = 1 To dt.Rows.Count Step 1

                If transposeddt.Columns.Count < dt.Rows.Count + 1 Then
                    transposeddt.Columns.Add(New DataColumn(rowcount.ToString, GetType(String)))
                End If

                newrow(rowcount) = dt.Rows(rowcount - 1)(colcount)

            Next

            transposeddt.Rows.Add(newrow)
        Next

        dt = transposeddt

    End Sub

    Public Shared Function SetHeaders(ByRef dt As DataTable, ByVal headerRow As Integer) As DataTable

        Dim headerDtRow As DataRow = dt.Rows(headerRow - 1)
        Dim colName As String
        'loop all columns in table
        For colIndex As Integer = 0 To dt.Columns.Count - 1

            colName = ConvertToString.NullSafe(headerDtRow(colIndex), "")

            colName = GetUniqueColumnName(dt, colName)

            'Modified to check if header column name is not blank - By Prashant on 14 Dec 2016
            If Not String.IsNullOrEmpty(colName) Then
                dt.Columns(colIndex).ColumnName = colName
            End If
        Next
        DeleteHeaderRow(dt, headerRow)
        Return dt
    End Function

    Public Shared Sub DeleteHeaderRow(ByRef dt As DataTable, ByVal headerRow As Integer)

        dt.Rows(headerRow - 1).Delete()
        dt.AcceptChanges()

    End Sub

    Public Shared Function GetUniqueColumnName(ByRef dt As DataTable, ByVal newColName As String) As String

        Dim colNameIndex As Integer = 1

        While dt.Columns.Contains(newColName) And colNameIndex < 500

            newColName = newColName & " " & colNameIndex.ToString

        End While

        Return newColName

    End Function

    Public Shared Function RemoveBlankColumns(ByRef dt As DataTable) As DataTable

        Dim remove As Boolean

        For colcount As Integer = dt.Columns.Count - 1 To 0 Step -1
            remove = True
            For Each row As DataRow In dt.Rows
                If Not IsDBNull(row(colcount)) Then
                    If Not String.IsNullOrEmpty(row(colcount)) Then
                        remove = False
                    End If
                End If

            Next

            If remove Then
                dt.Columns.RemoveAt(colcount)
            End If
        Next

        Return dt

    End Function

    Public Shared Function RemoveBlankRows(ByRef dt As DataTable) As DataTable

        Dim remove As Boolean

        For rowcount As Integer = dt.Rows.Count - 1 To 0 Step -1
            remove = True
            For Each item As Object In dt.Rows(rowcount).ItemArray

                If Not item.ToString = "" Then
                    remove = False
                End If

            Next

            If remove Then
                dt.Rows.RemoveAt(rowcount)
            End If

        Next

        Return dt

    End Function

    Public Shared Function RemoveTopRows(ByRef dt As DataTable, ByVal removeTillIndex As Integer) As DataTable

        For count As Integer = 1 To removeTillIndex
            dt.Rows.RemoveAt(0)
        Next

        Return dt
    End Function

    Public Shared Sub SortDataTable(ByRef dt As DataTable, ByVal sortoncolumnname As String)

        Dim view As DataView = dt.DefaultView
        view.Sort = sortoncolumnname
        dt = view.ToTable()

    End Sub

    Public Shared Function GetHeaderRowIndex(ByVal dt As DataTable, ByVal headers As List(Of String)) As Integer

        Dim reqdRowFlag As Boolean

        For Each row As DataRow In dt.Rows
            reqdRowFlag = True
            For Each header As String In headers
                If Not row.ItemArray.Contains(header) Then
                    reqdRowFlag = False
                End If
            Next
            If reqdRowFlag Then
                Return dt.Rows.IndexOf(row)
            End If
        Next
    End Function

    Public Shared Sub GetHHTableRow(ByRef row As DataRow)
        Dim objDataTableHHClass As New DataTableHHClass()
        objDataTableHHClass._itemRow = row

        objDataTableHHClass.hhTemp = IIf(row("00:00") Is DBNull.Value OrElse row("00:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("00:00")), 0.0, row("00:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0000 = IIf(row("00:30") Is DBNull.Value OrElse row("00:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("00:30")), 0.0, row("00:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0030 = IIf(row("01:00") Is DBNull.Value OrElse row("01:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("01:00")), 0.0, row("01:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0100 = IIf(row("01:30") Is DBNull.Value OrElse row("01:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("01:30")), 0.0, row("01:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0130 = IIf(row("02:00") Is DBNull.Value OrElse row("02:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("02:00")), 0.0, row("02:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0200 = IIf(row("02:30") Is DBNull.Value OrElse row("02:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("02:30")), 0.0, row("02:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0230 = IIf(row("03:00") Is DBNull.Value OrElse row("03:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("03:00")), 0.0, row("03:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0300 = IIf(row("03:30") Is DBNull.Value OrElse row("03:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("03:30")), 0.0, row("03:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0330 = IIf(row("04:00") Is DBNull.Value OrElse row("04:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("04:00")), 0.0, row("04:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0400 = IIf(row("04:30") Is DBNull.Value OrElse row("04:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("04:30")), 0.0, row("04:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0430 = IIf(row("05:00") Is DBNull.Value OrElse row("05:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("05:00")), 0.0, row("05:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0500 = IIf(row("05:30") Is DBNull.Value OrElse row("05:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("05:30")), 0.0, row("05:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0530 = IIf(row("06:00") Is DBNull.Value OrElse row("06:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("06:00")), 0.0, row("06:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0600 = IIf(row("06:30") Is DBNull.Value OrElse row("06:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("06:30")), 0.0, row("06:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0630 = IIf(row("07:00") Is DBNull.Value OrElse row("07:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("07:00")), 0.0, row("07:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0700 = IIf(row("07:30") Is DBNull.Value OrElse row("07:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("07:30")), 0.0, row("07:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0730 = IIf(row("08:00") Is DBNull.Value OrElse row("08:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("08:00")), 0.0, row("08:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0800 = IIf(row("08:30") Is DBNull.Value OrElse row("08:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("08:30")), 0.0, row("08:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0830 = IIf(row("09:00") Is DBNull.Value OrElse row("09:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("09:00")), 0.0, row("09:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0900 = IIf(row("09:30") Is DBNull.Value OrElse row("09:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("09:30")), 0.0, row("09:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0930 = IIf(row("10:00") Is DBNull.Value OrElse row("10:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("10:00")), 0.0, row("10:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1000 = IIf(row("10:30") Is DBNull.Value OrElse row("10:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("10:30")), 0.0, row("10:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1030 = IIf(row("11:00") Is DBNull.Value OrElse row("11:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("11:00")), 0.0, row("11:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1100 = IIf(row("11:30") Is DBNull.Value OrElse row("11:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("11:30")), 0.0, row("11:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1130 = IIf(row("12:00") Is DBNull.Value OrElse row("12:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("12:00")), 0.0, row("12:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1200 = IIf(row("12:30") Is DBNull.Value OrElse row("12:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("12:30")), 0.0, row("12:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1230 = IIf(row("13:00") Is DBNull.Value OrElse row("13:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("13:00")), 0.0, row("13:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1300 = IIf(row("13:30") Is DBNull.Value OrElse row("13:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("13:30")), 0.0, row("13:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1330 = IIf(row("14:00") Is DBNull.Value OrElse row("14:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("14:00")), 0.0, row("14:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1400 = IIf(row("14:30") Is DBNull.Value OrElse row("14:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("14:30")), 0.0, row("14:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1430 = IIf(row("15:00") Is DBNull.Value OrElse row("15:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("15:00")), 0.0, row("15:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1500 = IIf(row("15:30") Is DBNull.Value OrElse row("15:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("15:30")), 0.0, row("15:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1530 = IIf(row("16:00") Is DBNull.Value OrElse row("16:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("16:00")), 0.0, row("16:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1600 = IIf(row("16:30") Is DBNull.Value OrElse row("16:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("16:30")), 0.0, row("16:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1630 = IIf(row("17:00") Is DBNull.Value OrElse row("17:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("17:00")), 0.0, row("17:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1700 = IIf(row("17:30") Is DBNull.Value OrElse row("17:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("17:30")), 0.0, row("17:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1730 = IIf(row("18:00") Is DBNull.Value OrElse row("18:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("18:00")), 0.0, row("18:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1800 = IIf(row("18:30") Is DBNull.Value OrElse row("18:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("18:30")), 0.0, row("18:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1830 = IIf(row("19:00") Is DBNull.Value OrElse row("19:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("19:00")), 0.0, row("19:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1900 = IIf(row("19:30") Is DBNull.Value OrElse row("19:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("19:30")), 0.0, row("19:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1930 = IIf(row("20:00") Is DBNull.Value OrElse row("20:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("20:00")), 0.0, row("20:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2000 = IIf(row("20:30") Is DBNull.Value OrElse row("20:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("20:30")), 0.0, row("20:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2030 = IIf(row("21:00") Is DBNull.Value OrElse row("21:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("21:00")), 0.0, row("21:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2100 = IIf(row("21:30") Is DBNull.Value OrElse row("21:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("21:30")), 0.0, row("21:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2130 = IIf(row("22:00") Is DBNull.Value OrElse row("22:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("22:00")), 0.0, row("22:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2200 = IIf(row("22:30") Is DBNull.Value OrElse row("22:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("22:30")), 0.0, row("22:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2230 = IIf(row("23:00") Is DBNull.Value OrElse row("23:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("23:00")), 0.0, row("23:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2300 = IIf(row("23:30") Is DBNull.Value OrElse row("23:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("23:30")), 0.0, row("23:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2330 = IIf(row("Temp") Is DBNull.Value OrElse row("Temp") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("Temp")), 0.0, row("Temp"))), 2).ToString.Replace(",", ""))
    End Sub

    Public Shared Function GetShiftedTable(ByVal dataTable As DataTable)
        dataTable.Columns.Add("Temp")
        For Each row As DataRow In dataTable.Rows
            GetHHTableRow(row)
        Next
        dataTable.Columns.Remove("Temp")
        Return dataTable
    End Function
End Class

Public Class DataTableHHClass
    Protected _dtRow As DataRow
    Property _itemRow() As DataRow
        Get
            Return _dtRow
        End Get
        Set(ByVal value As DataRow)
            _dtRow = value
        End Set
    End Property
#Region "Property"
    WriteOnly Property hhTemp() As Object
        Set(ByVal value As Object)
            _itemRow("Temp") = value
        End Set
    End Property
    WriteOnly Property hh0000() As Object
        Set(ByVal value As Object)
            _itemRow("00:00") = value
        End Set
    End Property

    WriteOnly Property hh0030() As Object
        Set(ByVal value As Object)
            _itemRow("00:30") = value
        End Set
    End Property

    WriteOnly Property hh0100() As Object
        Set(ByVal value As Object)
            _itemRow("01:00") = value
        End Set
    End Property

    WriteOnly Property hh0130() As Object
        Set(ByVal value As Object)
            _itemRow("01:30") = value
        End Set
    End Property

    WriteOnly Property hh0200() As Object
        Set(ByVal value As Object)
            _itemRow("02:00") = value
        End Set
    End Property

    WriteOnly Property hh0230() As Object
        Set(ByVal value As Object)
            _itemRow("02:30") = value
        End Set
    End Property

    WriteOnly Property hh0300() As Object
        Set(ByVal value As Object)
            _itemRow("03:00") = value
        End Set
    End Property

    WriteOnly Property hh0330() As Object
        Set(ByVal value As Object)
            _itemRow("03:30") = value
        End Set
    End Property

    WriteOnly Property hh0400() As Object
        Set(ByVal value As Object)
            _itemRow("04:00") = value
        End Set
    End Property

    WriteOnly Property hh0430() As Object
        Set(ByVal value As Object)
            _itemRow("04:30") = value
        End Set
    End Property

    WriteOnly Property hh0500() As Object
        Set(ByVal value As Object)
            _itemRow("05:00") = value
        End Set
    End Property

    WriteOnly Property hh0530() As Object
        Set(ByVal value As Object)
            _itemRow("05:30") = value
        End Set
    End Property

    WriteOnly Property hh0600() As Object
        Set(ByVal value As Object)
            _itemRow("06:00") = value
        End Set
    End Property

    WriteOnly Property hh0630() As Object
        Set(ByVal value As Object)
            _itemRow("06:30") = value
        End Set
    End Property

    WriteOnly Property hh0700() As Object
        Set(ByVal value As Object)
            _itemRow("07:00") = value
        End Set
    End Property

    WriteOnly Property hh0730() As Object
        Set(ByVal value As Object)
            _itemRow("07:30") = value
        End Set
    End Property

    WriteOnly Property hh0800() As Object
        Set(ByVal value As Object)
            _itemRow("08:00") = value
        End Set
    End Property

    WriteOnly Property hh0830() As Object
        Set(ByVal value As Object)
            _itemRow("08:30") = value
        End Set
    End Property

    WriteOnly Property hh0900() As Object
        Set(ByVal value As Object)
            _itemRow("09:00") = value
        End Set
    End Property

    WriteOnly Property hh0930() As Object
        Set(ByVal value As Object)
            _itemRow("09:30") = value
        End Set
    End Property

    WriteOnly Property hh1000() As Object
        Set(ByVal value As Object)
            _itemRow("10:00") = value
        End Set
    End Property

    WriteOnly Property hh1030() As Object
        Set(ByVal value As Object)
            _itemRow("10:30") = value
        End Set
    End Property

    WriteOnly Property hh1100() As Object
        Set(ByVal value As Object)
            _itemRow("11:00") = value
        End Set
    End Property

    WriteOnly Property hh1130() As Object
        Set(ByVal value As Object)
            _itemRow("11:30") = value
        End Set
    End Property

    WriteOnly Property hh1200() As Object
        Set(ByVal value As Object)
            _itemRow("12:00") = value
        End Set
    End Property

    WriteOnly Property hh1230() As Object
        Set(ByVal value As Object)
            _itemRow("12:30") = value
        End Set
    End Property

    WriteOnly Property hh1300() As Object
        Set(ByVal value As Object)
            _itemRow("13:00") = value
        End Set
    End Property

    WriteOnly Property hh1330() As Object
        Set(ByVal value As Object)
            _itemRow("13:30") = value
        End Set
    End Property

    WriteOnly Property hh1400() As Object
        Set(ByVal value As Object)
            _itemRow("14:00") = value
        End Set
    End Property

    WriteOnly Property hh1430() As Object
        Set(ByVal value As Object)
            _itemRow("14:30") = value
        End Set
    End Property

    WriteOnly Property hh1500() As Object
        Set(ByVal value As Object)
            _itemRow("15:00") = value
        End Set
    End Property

    WriteOnly Property hh1530() As Object
        Set(ByVal value As Object)
            _itemRow("15:30") = value
        End Set
    End Property

    WriteOnly Property hh1600() As Object
        Set(ByVal value As Object)
            _itemRow("16:00") = value
        End Set
    End Property

    WriteOnly Property hh1630() As Object
        Set(ByVal value As Object)
            _itemRow("16:30") = value
        End Set
    End Property

    WriteOnly Property hh1700() As Object
        Set(ByVal value As Object)
            _itemRow("17:00") = value
        End Set
    End Property

    WriteOnly Property hh1730() As Object
        Set(ByVal value As Object)
            _itemRow("17:30") = value
        End Set
    End Property

    WriteOnly Property hh1800() As Object
        Set(ByVal value As Object)
            _itemRow("18:00") = value
        End Set
    End Property

    WriteOnly Property hh1830() As Object
        Set(ByVal value As Object)
            _itemRow("18:30") = value
        End Set
    End Property

    WriteOnly Property hh1900() As Object
        Set(ByVal value As Object)
            _itemRow("19:00") = value
        End Set
    End Property

    WriteOnly Property hh1930() As Object
        Set(ByVal value As Object)
            _itemRow("19:30") = value
        End Set
    End Property

    WriteOnly Property hh2000() As Object
        Set(ByVal value As Object)
            _itemRow("20:00") = value
        End Set
    End Property

    WriteOnly Property hh2030() As Object
        Set(ByVal value As Object)
            _itemRow("20:30") = value
        End Set
    End Property

    WriteOnly Property hh2100() As Object
        Set(ByVal value As Object)
            _itemRow("21:00") = value
        End Set
    End Property

    WriteOnly Property hh2130() As Object
        Set(ByVal value As Object)
            _itemRow("21:30") = value
        End Set
    End Property

    WriteOnly Property hh2200() As Object
        Set(ByVal value As Object)
            _itemRow("22:00") = value
        End Set
    End Property

    WriteOnly Property hh2230() As Object
        Set(ByVal value As Object)
            _itemRow("22:30") = value
        End Set
    End Property

    WriteOnly Property hh2300() As Object
        Set(ByVal value As Object)
            _itemRow("23:00") = value
        End Set
    End Property

    WriteOnly Property hh2330() As Object
        Set(ByVal value As Object)
            _itemRow("23:30") = value
        End Set
    End Property
#End Region
End Class