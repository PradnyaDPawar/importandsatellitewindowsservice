﻿Imports System.Data.SqlClient

Public Class DbWriterAdaptor

    'Create the command triplet
    Private _con As SqlConnection
    Private _cmd_I As SqlCommand
    Private _cmd_U As SqlCommand
    Private _cmd_D As SqlCommand

    Private _da As SqlDataAdapter

    Private _tableName As String
    Private _crud As StoredProcedures

    Public Sub New()

        'assumes default database
        _con = New SqlConnection(ConnectionString.DefaultDb)
        Initialise()

    End Sub

    Public Sub New(ByRef sqlConn As SqlConnection)

        _con = sqlConn
        Initialise()

    End Sub

    Private Sub Initialise()

        _cmd_I = New SqlCommand
        _cmd_U = New SqlCommand
        _cmd_D = New SqlCommand

        _cmd_I.Connection = _con
        _cmd_U.Connection = _con
        _cmd_D.Connection = _con

        _cmd_I.CommandType = CommandType.StoredProcedure
        _cmd_U.CommandType = CommandType.StoredProcedure
        _cmd_D.CommandType = CommandType.StoredProcedure

        _da = New SqlDataAdapter

    End Sub


    ''' <summary>
    ''' Committ table and write return values to stored procedure object
    ''' </summary>
    ''' <param name="table"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Committ(ByRef table As DataTable) As StoredProcedures

        _tableName = table.TableName
        LoadProcedures(table.TableName)

        'commit to table of the same name
        InvokeCommitt(table)

        Return _crud

    End Function

    Private Sub InvokeCommitt(ByRef table As DataTable)

        AddCmdText()
        AddInsertParams()
        AddUpdateParams()
        AddDeleteParams()

        _da.Update(table)

        SetReturnValues()

    End Sub

    Private Sub LoadProcedures(ByRef tableName As String)
        _crud = New StoredProcedures
        _crud.Load_Crud(tableName)

    End Sub

    Private Sub AddCmdText()

        _cmd_I.CommandText = _crud.Insert(_tableName).Name
        _cmd_U.CommandText = _crud.Update(_tableName).Name
        _cmd_D.CommandText = _crud.Delete(_tableName).Name

    End Sub

    Private Sub AddInsertParams()

        With _da

            .InsertCommand = _cmd_I

            'add parameters
            For Each param As Parameter In _crud.Insert(_tableName).Parameters

                If param.Input = True Then
                    'input parameters

                    _da.InsertCommand.Parameters.Add(param.Name, param.SqlType, param.Size, param.Name.Replace("@", ""))

                ElseIf param.Input = False Then

                    'output parameters
                    If param.Name = "@Id_Out" Then

                        'if new id return to table
                        _da.InsertCommand.Parameters.Add(param.Name, param.SqlType, param.Size, "Id").Direction = ParameterDirection.Output

                    Else
                        _da.InsertCommand.Parameters.Add(param.Name, param.SqlType, param.Size, param.Name.Replace("@", "")).Direction = ParameterDirection.Output


                    End If

                End If

            Next

        End With

    End Sub

    Private Sub AddUpdateParams()

        With _da

            .UpdateCommand = _cmd_U

            'add parameters
            For Each param As Parameter In _crud.Update(_tableName).Parameters

                If param.Input = True Then
                    'input parameters
                    _da.UpdateCommand.Parameters.Add(param.Name, param.SqlType, param.Size, param.Name.Replace("@", ""))

                ElseIf param.Input = False Then
                    'output parameters
                    _da.UpdateCommand.Parameters.Add(param.Name, param.SqlType, param.Size, param.Name.Replace("@", "")).Direction = ParameterDirection.Output

                End If

            Next

        End With

    End Sub

    Private Sub AddDeleteParams()

        With _da

            .DeleteCommand = _cmd_D

            'add id if one is required
            For Each param As Parameter In _crud.Delete(_tableName).Parameters

                If param.Input = True Then
                    'input parameters
                    _da.DeleteCommand.Parameters.Add(param.Name, param.SqlType, param.Size, param.Name.Replace("@", ""))

                ElseIf param.Input = False Then
                    'output parameters
                    _da.DeleteCommand.Parameters.Add(param.Name, param.SqlType, param.Size, param.Name.Replace("@", "")).Direction = ParameterDirection.Output

                End If

            Next

        End With

    End Sub

    Private Sub SetReturnValues()

        'get insert return values
        For Each parameter As SqlParameter In _cmd_I.Parameters()

            If parameter.Direction = ParameterDirection.Output And Not IsDBNull(parameter.Value) Then

                _crud.Insert(_tableName).SetParameterValue(parameter.ParameterName, parameter.Value)

            End If

        Next

        'get update return value
        For Each parameter As SqlParameter In _cmd_U.Parameters()

            If parameter.Direction = ParameterDirection.Output And Not IsDBNull(parameter.Value) Then

                _crud.Update(_tableName).SetParameterValue(parameter.ParameterName, parameter.Value)

            End If

        Next


        'get delete return value
        For Each parameter As SqlParameter In _cmd_D.Parameters()

            If parameter.Direction = ParameterDirection.Output And Not IsDBNull(parameter.Value) Then

                _crud.Delete(_tableName).SetParameterValue(parameter.ParameterName, parameter.Value)

            End If

        Next

    End Sub

End Class

