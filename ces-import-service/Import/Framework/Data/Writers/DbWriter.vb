﻿Imports System.Transactions
Imports System.Data.SqlClient

Public Class DbWriter

    ''' <summary>
    ''' Committ to a database table with the same name as the local table
    ''' Assumes general CRUD procedures, TableName_I, TableName_U, TableName_D
    ''' </summary>
    ''' <param name="dataTable"></param>
    ''' <remarks></remarks>
    Public Shared Function Committ(ByRef dataTable As DataTable) As StoredProcedures

        Dim writeAdaptor As New DbWriterAdaptor
        Return writeAdaptor.Committ(dataTable)

    End Function

    Public Shared Sub ExecProcedure(ByRef procedure As StoredProcedure)

        Dim writer As New DBProcedureWriterAdaptor
        writer.Execute(procedure)

    End Sub

    Public Shared Function Committ(ByRef dataSet As DataSet) As StoredProcedures
        Dim returnProcedures As New StoredProcedures

        Try

            Using scope As New TransactionScope()
                Using connection As New SqlConnection(ConnectionString.DefaultDb)
                    connection.Open()

                    'commit each table in this transaction
                    For Each table As DataTable In dataSet.Tables

                        Dim writeAdaptor As New DbWriterAdaptor(connection)
                        returnProcedures.Merge(writeAdaptor.Committ(table))

                    Next


                End Using

                'this point is not reach on error and the transaction is rolled back automatically
                scope.Complete()

            End Using

        Catch ex As Exception

            For Each procedure As StoredProcedure In returnProcedures.Procedures

                procedure.SetResultValue(False)

            Next

        End Try


        Return returnProcedures

    End Function

    Public Shared Function Committ(ByRef dataSet As DataSet, ByRef foreignKeys As ForeignKeys) As StoredProcedures
        Dim returnProcedures As New StoredProcedures

        Try

            Using scope As New TransactionScope()
                Using connection As New SqlConnection(ConnectionString.DefaultDb)
                    connection.Open()

                    'commit each table in this transaction
                    For Each table As DataTable In dataSet.Tables

                        Dim writeAdaptor As New DbWriterAdaptor(connection)
                        returnProcedures.Merge(writeAdaptor.Committ(table))

                        'set foreign keys
                        SetKeys(table, dataSet, foreignKeys)

                    Next


                End Using

                'this point is not reach on error and the transaction is rolled back automatically
                scope.Complete()

            End Using

        Catch ex As Exception

            For Each proc As StoredProcedure In returnProcedures.Procedures

                proc.SetResultValue(False)

            Next


        End Try


        Return returnProcedures

    End Function

    Public Shared Function CommittBulk(ByRef dataTable As DataTable, ByVal databaseName As String) As Integer

        Try
            Using scope As New TransactionScope()

                Using bulkCopy As New SqlBulkCopy(ConnectionString.SpecificDb(databaseName))
                    bulkCopy.BulkCopyTimeout = 60
                    bulkCopy.BatchSize = 5000
                    bulkCopy.DestinationTableName = dataTable.TableName

                    ' Write from the source to the destination.
                    bulkCopy.WriteToServer(dataTable)
                End Using

                scope.Complete()

            End Using
            'If Success => Return 1
            Return 1
        Catch ex As System.InvalidOperationException
            ' If Column value size exceeds column size => 3
            Return 3
        Catch ex As Exception
            'For all other exception =>6
            Return 6
        End Try
    End Function

    Private Shared Sub SetKeys(ByRef foreignTable As DataTable, ByRef tables As DataSet, ByRef foreignKeys As ForeignKeys)


        For Each table As DataTable In tables.Tables

            If foreignTable.TableName <> table.TableName Then
                'foreignKeys.SetKeys1(foreignTable, table)
                SetKeys1(foreignTable, table, foreignKeys)

            End If

        Next


    End Sub

    Public Shared Sub SetKeys1(ByRef foreignTable As DataTable, ByRef srcTable As DataTable, ByRef foreignKeys As ForeignKeys)

        For Each fk As ForeignKey In foreignKeys.Keys

            'locate foreign table
            If fk.TableName = srcTable.TableName And fk.ForeignTable = foreignTable.TableName Then

                For Each row As DataRow In srcTable.Rows
                    'not deleted row
                    If Not row.RowState = DataRowState.Deleted Then
                        'set key
                        If row(fk.Colname) <= 0 Then

                            Dim foreignRow As Integer

                            If row(fk.Colname) < 0 Then
                                Math.Abs(row(fk.Colname))
                            End If

                            row(fk.Colname) = foreignTable.Rows(foreignRow)(fk.ForeignTableKey)


                        End If
                    End If
                Next

            End If

        Next

    End Sub

End Class