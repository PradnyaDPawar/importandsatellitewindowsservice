﻿Imports System.Data.SqlClient

Public Class DBProcedureWriterAdaptor

    'Create the command triplet
    Private _con As SqlConnection
    Private _cmd As SqlCommand

    Private _procedure As StoredProcedure

    Public Sub New()
        'assume connects to DeCentral.  Overload new for de databases
        _con = New SqlConnection(ConnectionString.DefaultDb)

        _cmd = New SqlCommand
        _cmd.Connection = _con
        _cmd.CommandType = CommandType.StoredProcedure

    End Sub

    Public Sub Execute(ByVal procedure As StoredProcedure)

        _procedure = procedure

        AddCmdText(procedure)
        AddParameters(procedure)

        Invoke()

    End Sub

    Private Sub AddCmdText(ByVal procedure As StoredProcedure)

        _cmd.CommandText = procedure.Name

    End Sub

    Private Sub AddParameters(ByVal procedure As StoredProcedure)

        For Each param As Parameter In procedure.Parameters

            If param.Input = True And param.Name.ToLower <> "@return_value" Then
                _cmd.Parameters.AddWithValue(param.Name, param.Value)

            ElseIf param.Input = False And param.Name.ToLower <> "@return_value" Then

                _cmd.Parameters.Add(param.Name, param.SqlType, param.Size, param.Name.Replace("@", "")).Direction = ParameterDirection.Output

            End If

        Next

    End Sub

    Private Sub Invoke()

        Try
            _con.Open()
            _cmd.ExecuteNonQuery()

            SetReturnParameters()

        Finally
            _con.Close()
            _cmd.Dispose()
            _con.Dispose()

        End Try

    End Sub

    Private Sub SetReturnParameters()

        For Each parameter As Parameter In _procedure.Parameters

            'retirve value for each output parameter
            If parameter.Input = False Then

                parameter.Value = _cmd.Parameters(parameter.Name).Value.ToString

            End If

        Next

    End Sub

End Class
