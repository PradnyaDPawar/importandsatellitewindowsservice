﻿Imports FlexCel.XlsAdapter
Imports FlexCel.Report

Public Class ExcelWriter


    'Write datatable to an empty excel
    Public Shared Function WriteExcel(ByRef excelDataTable As DataTable, ByVal sheetName As String, ByVal newFilePath As String)
        Dim resultXls As New XlsFile(True)
        Dim xlsReport As New FlexCelReport

        Try
            'define formula
            Dim nr As New FlexCel.Core.TXlsNamedRange("__result__", 1, 0, "=Sheet1!$A$2")

            'Create sheet
            resultXls.NewFile(1)

            'Set header and datatable position on excel
            resultXls.SetCellValue(1, 1, "<#result.**>")
            resultXls.SetCellValue(2, 1, "<#result.*>")
            resultXls.SetNamedRange(nr)

            'Set Sheet Name
            If Not sheetName = String.Empty Then
                resultXls.SheetName = sheetName
            End If

            'Add datatable to excel
            xlsReport.AddTable("result", excelDataTable)

            xlsReport.Run(resultXls)

            resultXls.DeleteNamedRange(1)

            resultXls.Save(newFilePath)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class