﻿Imports FlexCel
Imports FlexCel.XlsAdapter
Imports System.IO
Imports System.Data.OleDb

Public Class Excel
#Region "Member Variables"
    ''' <summary>
    ''' The VML file header
    ''' </summary>
    Private vmlFileHeader As String = "urn:schemas-microsoft-com:vml"

    ''' <summary>
    ''' The HTML file connection string
    ''' </summary>
    Private htmlFileConnectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""HTML Import;IMEX=1;HDR=Yes"""

    ''' <summary>
    ''' The XLS file connection string
    ''' </summary>
    Private xlsFileConnectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0;IMEX=1;HDR=Yes"""

    'interface to flexcel
    Private _xlsFile As XlsFile
    Private _sheets As List(Of ExcelSheet)

#End Region

#Region "Public properties"

    ReadOnly Property Sheets() As List(Of ExcelSheet)
        Get
            Return _sheets
        End Get
    End Property

#End Region

    ''' <summary>
    ''' Determines whether file is vml file or not.
    ''' </summary>
    ''' <param name="filePath">The file path.</param>
    ''' <returns><c>true</c> if file is vml file; otherwise, <c>false</c>.</returns>
    Private Function IsVmlFile(filePath As String) As Boolean

        Dim bVmlFile As Boolean = True

        Try
            Dim rd As New StreamReader(filePath)
            bVmlFile = rd.ReadLine().Contains(vmlFileHeader)
            rd.Close()

        Catch ex As Exception
            bVmlFile = False
        End Try

        Return bVmlFile

    End Function

#Region "Public Functions"

    Public Sub Load(ByVal filePath As String)

        _xlsFile = New XlsAdapter.XlsFile(filePath)
        _sheets = New List(Of ExcelSheet)

        'add to a list of sheets in the spreadsheet
        For sIndex As Integer = 1 To _xlsFile.SheetCount

            _sheets.Add(New ExcelSheet(_xlsFile, sIndex))

        Next

    End Sub

    ''' <summary>
    ''' Loads the file by OLEDb.
    ''' </summary>
    ''' <param name="filePath">The file path.</param>
    ''' <returns>Return loaded dataTable with file data.</returns>
    ''' <exception cref="ArgumentException">The worksheet number provided cannot be found in the spreadsheet</exception>
    Public Function LoadByOleDB(filePath As String) As DataTable

        Dim connectionString As String = String.Empty

        If IsVmlFile(filePath) Then
            connectionString = [String].Format(htmlFileConnectionString, filePath)
        Else
            connectionString = [String].Format(xlsFileConnectionString, filePath)
        End If

        Dim worksheetNumber As Integer = 1
        Dim oleConn As New OleDbConnection(connectionString)
        Dim hhDataTable As New DataTable()

        Try
            oleConn.Open()

            Dim schemaTable As DataTable = oleConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)

            If schemaTable.Rows.Count < worksheetNumber Then
                Throw New ArgumentException("The worksheet number provided cannot be found in the spreadsheet")
            End If

            Dim workSheet As String = schemaTable.Rows(worksheetNumber - 1)("table_name").ToString().Replace("'", "")
            Dim sqlQuery As String = [String].Format("SELECT * FROM [{0}]", workSheet)

            Dim dataAdapter = New OleDbDataAdapter(sqlQuery, oleConn)
            dataAdapter.Fill(hhDataTable)

        Catch ex As Exception
            Throw ex

        Finally
            oleConn.Close()

        End Try

        Return hhDataTable

    End Function

    Public Function GetSheet(ByVal name As String, ByRef returnSheet As ExcelSheet) As Boolean
        Dim bReturnValue As Boolean = False

        For Each sheet As ExcelSheet In Sheets

            If sheet.Name = name Then

                returnSheet = sheet
                bReturnValue = True
                Exit For
            End If
        Next

        Return bReturnValue
    End Function

#End Region

End Class