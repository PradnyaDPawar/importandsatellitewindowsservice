﻿Imports FlexCel.XlsAdapter

Public Class ExcelCols

    Private _xlsFile As XlsFile

    ReadOnly Property Count() As Integer
        Get
            Return _xlsFile.GetColCount(_xlsFile.ActiveSheet, False)
        End Get
    End Property

    Public Sub New(ByRef xlsFile As XlsFile)

        _xlsFile = xlsFile

    End Sub

    Public Function CountInRow(ByVal rowIndex As Integer) As Integer

        Return _xlsFile.ColCountInRow(rowIndex)

    End Function

End Class
