﻿Imports FlexCel.XlsAdapter

Friend Class ExcelSheetSearch

    Public Shared Function GetRowId(ByRef xSheet As ExcelSheet, ByVal values As List(Of String), ByVal criteria As ExcelSearchEnum) As Integer

        Select Case criteria

            Case ExcelSearchEnum.ContainsAll
                Return SearchConatinsAll(xSheet, values)

            Case Else
                Return -1

        End Select

    End Function

    Private Shared Function SearchConatinsAll(ByRef xSheet As ExcelSheet, ByVal values As List(Of String)) As Integer

        'loop through excel file and look for matching rows
        Dim rowCount As Integer = xSheet.Rows.Count

        For rIndex As Integer = 1 To rowCount

            If RowMatch(xSheet, rIndex, values) Then

                'return matching row index
                Return rIndex

            End If

        Next

        'no row found
        Return -1

    End Function

    Private Shared Function RowMatch(ByRef xSheet As ExcelSheet, ByVal rIndex As Integer, ByVal values As List(Of String)) As Boolean

        Dim rowValues As List(Of String)
        rowValues = xSheet.Rows.GetRow(rIndex)

        'check if header is present
        For Each refValue As String In values

            If Not rowValues.Contains(refValue.ToLower) Then
                Return False
            End If

        Next

        Return True

    End Function

End Class
