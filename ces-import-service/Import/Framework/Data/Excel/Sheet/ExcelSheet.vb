﻿Imports FlexCel.XlsAdapter

Public Class ExcelSheet

    Private _xlsFile As XlsFile
    Private _cols As ExcelCols
    Private _rows As ExcelRows

    Private _sheetIndex As Integer
    Private _headerRowIndex As Integer

    Property HeaderRowIndex() As Integer
        Get
            Return _headerRowIndex
        End Get
        Set(ByVal value As Integer)
            _headerRowIndex = value
        End Set
    End Property

    ReadOnly Property XlsFile() As XlsFile
        Get
            SetToCurrentSheet()
            Return _xlsFile
        End Get
    End Property

    ReadOnly Property Cols() As ExcelCols
        Get
            SetToCurrentSheet()
            Return _cols
        End Get
    End Property

    ReadOnly Property Rows() As ExcelRows
        Get
            Return _rows
        End Get
    End Property

    Public Function Name() As String

        SetToCurrentSheet()
        Return _xlsFile.SheetName()

    End Function

    Public Function ColCountExBlanks(ByVal rowIndex As Integer) As Integer

        'if no headers, load whole sheet
        If rowIndex < 1 Then

            Return _cols.Count

        Else

            'get values in row
            Dim row As List(Of String) = Rows.GetRow(rowIndex)

            Dim index As Integer = row.Count

            While row(index - 1).Replace(" ", "") = "" And index > 0

                index = index - 1

            End While

            Return index

        End If

    End Function

    Public Function HeaderCount() As Integer

        Return ColCountExBlanks(_headerRowIndex)

    End Function

    Public Function SetHeaderRow(ByVal headers As List(Of String), ByVal criteria As ExcelSearchEnum) As Boolean
        SetToCurrentSheet()

        _headerRowIndex = GetRowId(headers, criteria)

        If _headerRowIndex > 0 Then
            'header row found
            Return True

        Else
            'matching header row not found
            Return False

        End If

    End Function

    Public Sub New(ByRef xlsFile As XlsFile, ByVal sheetIndex As Integer)

        Load(xlsFile, sheetIndex)

        _cols = New ExcelCols(_xlsFile)
        _rows = New ExcelRows(_xlsFile)

        'no default headers 
        _headerRowIndex = 0

    End Sub

    Public Sub Load(ByRef xlsFile As XlsFile, ByVal sheetIndex As Integer)

        _xlsFile = xlsFile
        _sheetIndex = sheetIndex

    End Sub

    Public Function GetDataTable() As DataTable

        'get the whole sheet
        Return ExcelToDataTable.GetDataTable(Me)

    End Function

    Private Sub SetToCurrentSheet()

        _xlsFile.ActiveSheet = _sheetIndex

    End Sub

    Private Function GetRowId(ByVal values As List(Of String), ByVal criteria As ExcelSearchEnum) As Integer

        Return ExcelSheetSearch.GetRowId(Me, values, ExcelSearchEnum.ContainsAll)

    End Function

End Class
