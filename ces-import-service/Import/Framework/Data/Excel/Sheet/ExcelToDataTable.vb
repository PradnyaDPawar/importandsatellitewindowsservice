﻿Imports FlexCel.XlsAdapter

Friend Class ExcelToDataTable

    Public Shared Function GetDataTable(ByRef xSheet As ExcelSheet) As DataTable

        Dim data As DataTable = LoadAll(xSheet)

        'replace headers if row has been defined
        If xSheet.HeaderRowIndex > 0 Then

            SetHeaders(data, xSheet.HeaderRowIndex)
            DeleteHeaderRow(data, xSheet.HeaderRowIndex)
            Return data

        Else

            Return data

        End If

    End Function

    'Private Shared Function LoadAll(ByRef xSheet As ExcelSheet) As DataTable

    '    'create new data table the size of the spreadsheet
    '    Dim resultTable As DataTable = DataTableTools.GetNew(xSheet.Name, xSheet.HeaderCount)

    '    'loop data and fill datatable
    '    Dim rowCount As Integer = xSheet.Rows.Count

    '    For rIndex As Integer = 1 To rowCount

    '        Dim newRow As DataRow = resultTable.NewRow

    '        For cIndex As Integer = 1 To xSheet.HeaderCount Step 1

    '            'read value from excel
    '            Dim cellVal As String = xSheet.XlsFile.GetStringFromCell(rIndex, cIndex).ToString

    '            newRow(cIndex - 1) = cellVal

    '        Next

    '        resultTable.Rows.Add(newRow)

    '    Next

    '    Return resultTable

    'End Function

    ''' <summary>
    ''' New method handling merged columns
    ''' </summary>
    ''' <param name="xSheet"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function LoadAll(ByRef xSheet As ExcelSheet) As DataTable

        'create new data table the size of the spreadsheet
        Dim resultTable As DataTable = DataTableTools.GetNew(xSheet.Name, xSheet.HeaderCount)

        'loop data and fill datatable
        Dim rowCount As Integer = xSheet.Rows.Count

        For rIndex As Integer = 1 To rowCount

            Dim newRow As DataRow = resultTable.NewRow

            For cIndex As Integer = 1 To xSheet.HeaderCount Step 1
                Dim range As FlexCel.Core.TXlsCellRange = xSheet.XlsFile.CellMergedBounds(rIndex, cIndex)
                Dim cellVal As String = String.Empty
                'Check for merged columns
                If range.ColCount > 1 Then
                    Dim rngLeft As Integer = range.Left
                    'read value from excel
                    cellVal = xSheet.XlsFile.GetStringFromCell(rIndex, rngLeft).ToString()
                Else
                    'read value from excel
                    cellVal = xSheet.XlsFile.GetStringFromCell(rIndex, cIndex).ToString()
                End If
                newRow(cIndex - 1) = cellVal
            Next
            resultTable.Rows.Add(newRow)
        Next
        Return resultTable
    End Function

    Private Shared Sub SetHeaders(ByRef srcTable As DataTable, ByVal headerRowIndex As Integer)

        Dim headerRow As DataRow = srcTable.Rows(headerRowIndex - 1)
        Dim colName As String

        'loop all columns in table
        For colIndex As Integer = 0 To srcTable.Columns.Count - 1

            colName = ConvertToString.NullSafe(headerRow(colIndex), "")

            colName = GetUniqueColumnName(srcTable, colName)
            srcTable.Columns(colIndex).ColumnName = colName

        Next

    End Sub

    Private Shared Function GetUniqueColumnName(ByRef srcTable As DataTable, ByVal newColName As String) As String

        Dim colNameIndex As Integer = 1

        While srcTable.Columns.Contains(newColName) And colNameIndex < 500

            newColName = newColName & colNameIndex.ToString

        End While

        Return newColName

    End Function

    Private Shared Sub DeleteHeaderRow(ByRef srcTable As DataTable, ByVal headerRowIndex As Integer)

        srcTable.Rows(headerRowIndex - 1).Delete()
        srcTable.AcceptChanges()

    End Sub

End Class
