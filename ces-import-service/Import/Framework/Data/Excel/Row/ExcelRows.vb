﻿Imports FlexCel.XlsAdapter

Public Class ExcelRows

    Private _xlsFile As XlsFile

    ReadOnly Property Count() As Integer
        Get
            Return _xlsFile.RowCount
        End Get
        
    End Property

    Public Sub New(ByRef xlsFile As XlsFile)

        _xlsFile = xlsFile

    End Sub

    Public Function GetRow(ByVal rowIndex As Integer) As List(Of String)

        Dim colCount As Integer = _xlsFile.ColCountInRow(rowIndex)
        Dim rowValues As New List(Of String)

        'get all values in the row
        For cIndex As Integer = 1 To colCount Step 1

            'read value from excel
            rowValues.Add(_xlsFile.GetStringFromCell(rowIndex, cIndex).ToString.ToLower)

        Next

        Return rowValues

    End Function

End Class
