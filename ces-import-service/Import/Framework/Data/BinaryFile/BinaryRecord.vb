﻿Imports System.Xml

Public Class BinaryRecord

    Public Sub New()
        _items = New List(Of BinaryItem)()
    End Sub

    Public Sub New(ByVal length As Integer)
        Me.New()
        _length = length
    End Sub

    Private _length As Integer
    ReadOnly Property Length() As Integer
        Get
            Return _length
        End Get
    End Property

    Private _items As List(Of BinaryItem)
    ReadOnly Property Items() As List(Of BinaryItem)
        Get
            Return _items
        End Get
    End Property

    Public Function AddItem(ByRef element As XmlElement)
        Dim item As New BinaryItem(element)
        _items.Add(item)
        Return item
    End Function
End Class
