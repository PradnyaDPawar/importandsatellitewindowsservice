﻿Imports System.Xml

Public Class BinaryItem
    Private _id As String = String.Empty
    Private _type As Integer = 0
    Private _location As Integer = -1
    Private _length As Integer = 0
    Private _pow10 As Integer = 0
    Private _bigEndian As Boolean = True
    Private _format As String = Nothing

    Public Sub New()
    End Sub

    Public Sub New(ByVal element As XmlElement)
        Me.New()
        Dim s As String = element.GetAttribute("id")
        _id = IIf(s Is Nothing, _id, s)

        s = element.GetAttribute("type")
        Try
            _type = CInt(s)
        Catch ex As Exception
        End Try

        s = element.GetAttribute("loc")
        Try
            _location = CInt(s)
        Catch ex As Exception
        End Try

        s = element.GetAttribute("len")
        Try
            _length = CInt(s)
        Catch ex As Exception
        End Try

        s = element.GetAttribute("pow10")
        Try
            _pow10 = CInt(s)
        Catch ex As Exception
        End Try

        s = element.GetAttribute("order")
        If Not String.IsNullOrEmpty(s) Then
            If s.ToLower() = "b" Then
                _bigEndian = True
            ElseIf s.ToLower() = "l" Then
                _bigEndian = False
            End If
        End If

        s = element.GetAttribute("fmt")
        _format = IIf(String.IsNullOrEmpty(s), Nothing, s)

    End Sub

    ReadOnly Property Id() As String
        Get
            Return _id
        End Get
    End Property

    ReadOnly Property Type() As Integer
        Get
            Return _type
        End Get
    End Property

    ReadOnly Property Location() As Integer
        Get
            Return _location
        End Get
    End Property

    ReadOnly Property Length() As Integer
        Get
            Return _length
        End Get
    End Property

    ReadOnly Property Pow10() As Integer
        Get
            Return _pow10
        End Get
    End Property

    ReadOnly Property BigEndian() As Boolean
        Get
            Return _bigEndian
        End Get
    End Property

    ReadOnly Property Format() As String
        Get
            Return _format
        End Get
    End Property

End Class
