﻿Imports Microsoft.VisualBasic.FileIO
Imports System.Xml
Imports System.IO

Public Class BinaryFile

    Private _filePath As String = Nothing
    Private _data As DataTable = Nothing
    Private _binRecDefsPath As String = ".\binary-record-defs.xml"

    ReadOnly Property Table() As DataTable
        Get
            Return _data
        End Get
    End Property

    Public Sub New(ByVal path As String, ByVal columnCount As Integer)
        _filePath = path
    End Sub

    Public Sub Load(ByRef dataTable As DataTable)
        _data = New DataTable
        LoadTable()
        dataTable = _data
    End Sub

    Private Sub LoadTable()
        Dim recdef As BinaryRecord = LoadConfig("ezmeter")
        If recdef IsNot Nothing Then
            Dim buffer() As Byte = File.ReadAllBytes(_filePath)
            If buffer IsNot Nothing AndAlso buffer.Length > 0 Then
                For Each item As BinaryItem In recdef.Items
                    _data.Columns.Add(New DataColumn(item.Id))
                Next

                Dim currentRecordStartOffset As Integer = 0
                Dim nextRecordStartOffset As Integer = recdef.Length
                While nextRecordStartOffset <= buffer.Length
                    Dim row As DataRow = _data.NewRow()
                    For Each item As BinaryItem In recdef.Items
                        Dim value As String = Nothing
                        Select Case item.Type
                            Case 1 ' unsigned integer
                                Dim raw As UInteger = ReadUnsignedInteger(buffer, currentRecordStartOffset + item.Location, item.Length, item.BigEndian)
                                Dim real As Decimal = ApplyPowerOfTen(raw, item.Pow10)
                                value = IIf(item.Format Is Nothing, real.ToString, real.ToString(item.Format))
                            Case 2 ' signed integer
                                Dim raw As Integer = ReadSignedInteger(buffer, currentRecordStartOffset + item.Location, item.Length, item.BigEndian)
                                Dim real As Decimal = ApplyPowerOfTen(raw, item.Pow10)
                                value = IIf(item.Format Is Nothing, real.ToString, real.ToString(item.Format))
                            Case 3 ' time
                                Dim time As TimeSpan = ReadTimeUnsignedInteger_hhmm(buffer, currentRecordStartOffset + item.Location, item.Length, item.BigEndian)
                                value = String.Format("{0:00}:{1:00}", time.Hours, time.Minutes)
                            Case 4 ' date
                                Dim d As Date = ReadDateUnsignedInteger_ddmmyy(buffer, currentRecordStartOffset + item.Location, item.Length, item.BigEndian)
                                value = String.Format("{0:00}/{1:00}/{2:0000}", d.Day, d.Month, d.Year)
                            Case 5 ' ezmeter 4 byte quantity
                                Dim raw As Integer = ReadEzMeterFourByteQuantity(buffer, currentRecordStartOffset + item.Location, item.BigEndian)
                                Dim real As Decimal = ApplyPowerOfTen(raw, item.Pow10)
                                value = IIf(item.Format Is Nothing, real.ToString, real.ToString(item.Format))
                            Case Else
                                value = "Unknown type " & item.Type
                        End Select
                        row(item.Id) = value
                    Next
                    _data.Rows.Add(row)
                    currentRecordStartOffset = nextRecordStartOffset
                    nextRecordStartOffset = nextRecordStartOffset + recdef.Length
                End While
            End If
        End If

    End Sub

    Public Function DataAt(ByVal row As Integer, ByVal col As Integer)
        Return _data.Rows.Item(row).Item(col)
    End Function

    Public Function ColumnNameAt(ByVal index As Integer)
        Return _data.Columns.Item(index).ColumnName
    End Function

    Public Function HasColumn(ByVal name As String) As Boolean
        Return _data.Columns.Contains(name)
    End Function

    Private Function LoadConfig(ByVal target As String) As BinaryRecord
        Dim doc As New XmlDocument
        Try
            doc.Load(_binRecDefsPath)
            Dim records As XmlNodeList = doc.GetElementsByTagName("Record")
            For Each recordElement As XmlElement In records
                Dim recordName As String = recordElement.GetAttribute("name")
                If recordName IsNot Nothing AndAlso recordName = target Then
                    Dim length As Integer = recordElement.GetAttribute("len")
                    Dim record As New BinaryRecord(length)
                    Dim items As XmlNodeList = recordElement.GetElementsByTagName("Item")
                    For Each itemElement As XmlElement In items
                        record.AddItem(itemElement)
                    Next
                    Return record
                End If
            Next
        Catch ex As Exception
            Console.WriteLine("EzmeterRead: Error loading meter information file : " & _binRecDefsPath)
        End Try
        Return Nothing
    End Function

    Private Function ReadEzMeterFourByteQuantity(ByRef buffer As Byte(), ByVal offset As Integer, Optional ByVal bigEndian As Boolean = True) As Integer
        Dim sign As Integer = IIf(ReadSignedInteger(buffer, offset, 1) = 0, 1, -1)
        Return ReadUnsignedInteger(buffer, offset + 1, 3, bigEndian) * sign
    End Function

    Private Function ReadDateUnsignedInteger_ddmmyy(ByRef buffer As Byte(), ByVal offset As Integer, ByVal length As Integer, Optional ByVal bigEndian As Boolean = True) As Date
        Dim raw = ReadUnsignedInteger(buffer, offset, length, bigEndian)
        Dim padded = raw.ToString("00000000")
        Return New Date(CInt(padded.Substring(4, 4)), CInt(padded.Substring(2, 2)), Int(padded.Substring(0, 2)))
    End Function

    Private Function ReadTimeUnsignedInteger_hhmm(ByRef buffer As Byte(), ByVal offset As Integer, ByVal length As Integer, Optional ByVal bigEndian As Boolean = True) As TimeSpan
        Dim raw = ReadUnsignedInteger(buffer, offset, length, bigEndian)
        Dim padded = raw.ToString("0000")
        Return New TimeSpan(CInt(padded.Substring(0, 2)), CInt(padded.Substring(2, 2)), 0)
    End Function

    Private Function ReadSignedInteger(ByRef buffer As Byte(), ByVal offset As Integer, ByVal length As Integer, Optional ByVal bigEndian As Boolean = True) As Integer
        Dim result As Integer = 0
        If length = 1 Then
            result = buffer(offset)
            If (result > SByte.MaxValue) Then
                result = result - Byte.MaxValue - 1
            End If
        ElseIf length = 2 Then
            result = ReadUnsignedInteger(buffer, offset, length, bigEndian)
            If (result > Short.MaxValue) Then
                result = result - UShort.MaxValue - 1
            End If
        ElseIf length = 3 Then
            result = ReadUnsignedInteger(buffer, offset, length, bigEndian)
            If (result > &H7FFFFF) Then
                result = result - &HFFFFFF - 1
            End If
        ElseIf length = 4 Then
            Dim uresult = ReadUnsignedInteger(buffer, offset, length, bigEndian)
            If (uresult > Integer.MaxValue) Then
                result = result - UInteger.MaxValue - 1
            End If
        End If
        Return result
    End Function

    Private Function ReadUnsignedInteger(ByRef buffer As Byte(), ByVal offset As Integer, ByVal length As Integer, Optional ByVal bigEndian As Boolean = True) As UInteger
        Dim result As UInteger = 0
        Dim i1 As Integer = offset
        Dim i2 As Integer = offset + length - 1

        Dim sentinel As Integer = offset + length
        If bigEndian Then
            Dim shift As Integer = (length - 1) * 8
            While i1 < sentinel
                Dim b As Byte = buffer(i1)
                Dim intermediate As UInteger = CUInt(b) << shift
                result = result Or intermediate
                i1 = i1 + 1
                i2 = i2 - 1
                shift = shift - 8
            End While
        Else
            Dim shift As Integer = 0
            While i1 < sentinel
                Dim b As Byte = buffer(i1)
                Dim intermediate As UInteger = CUInt(b) << shift
                result = result Or intermediate
                i1 = i1 + 1
                i2 = i2 - 1
                shift = shift + 8
            End While
        End If
        Return result
    End Function

    Private Function ApplyPowerOfTen(ByVal raw As Decimal, ByVal pow10 As Integer) As Decimal
        If pow10 <> 0 Then
            Return CDec(raw * Math.Pow(10, pow10))
        End If
        Return CDec(raw)
    End Function

End Class
