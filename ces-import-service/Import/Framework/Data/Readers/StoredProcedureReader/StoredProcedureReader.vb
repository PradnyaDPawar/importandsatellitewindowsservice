﻿Imports System.Data.SqlClient

''' <summary>
''' Gets a list of all parameters required by a stored procedure
''' </summary>
''' <remarks></remarks>
Public Class StoredProcedureReader

    Public Shared Function GetProcedure(ByVal spName As String) As StoredProcedure

        Dim dbAdaptor As New DbReaderAdaptor
        dbAdaptor.SetCommandText(spName)

        Dim procedure As New StoredProcedure
        procedure.Name = spName

        Try
            dbAdaptor.Con.Open()
            SqlCommandBuilder.DeriveParameters(dbAdaptor.Cmd)

            'fill writer parameters with list from stored procedure
            For Each parameter As SqlParameter In dbAdaptor.Cmd.Parameters

                Dim spParam As New Parameter

                spParam.Name = parameter.ParameterName
                spParam.Size = parameter.Size
                spParam.SqlType = parameter.SqlDbType

                If parameter.Direction = ParameterDirection.Input Then
                    spParam.Input = True

                Else
                    spParam.Input = False

                End If

                'ignore @return_value
                If spParam.Name.ToLower <> "@return_value" Then
                    procedure.Parameters.Add(spParam)

                End If

            Next

            dbAdaptor.Con.Close()
            dbAdaptor.Con.Dispose()

            Return procedure

        Catch ex As Exception
            Throw ex

        Finally
            dbAdaptor.Con.Close()
            dbAdaptor.Con.Dispose()

        End Try

    End Function

End Class
