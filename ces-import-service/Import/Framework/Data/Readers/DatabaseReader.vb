﻿Public Class DbReader

    'common reader to invoke a stored procedure

    Public Shared Function Invoke(ByVal spName As String) As DataSet

        'invoke wihtout parameters
        Dim dbAdaptor As New DbReaderAdaptor
        Dim ds As DataSet

        ds = dbAdaptor.GetDataSet(spName)

        Return ds

    End Function

    Public Shared Function Invoke(ByVal procedure As StoredProcedure) As DataSet

        'invoke with parameters
        Dim dbAdaptor As New DbReaderAdaptor
        Dim ds As DataSet

        ds = dbAdaptor.GetDataSet(procedure)

        'name the first table the same as procedure name
        ds.Tables(0).TableName = procedure.Name

        'index on the rest of the tables
        For i As Integer = 1 To ds.Tables.Count - 1

            ds.Tables(i).TableName = procedure.Name & i

        Next

        Return ds

    End Function


End Class
