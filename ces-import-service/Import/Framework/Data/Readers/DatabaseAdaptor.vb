﻿Imports System.Data.SqlClient

Public Class DbReaderAdaptor

    Private _con As SqlConnection
    Private _cmd As SqlCommand
    Private _reader As SqlDataReader

    Private _da As SqlDataAdapter

    ReadOnly Property Con() As SqlConnection
        Get
            Return _con
        End Get
    End Property

    ReadOnly Property Cmd() As SqlCommand
        Get
            Return _cmd
        End Get

    End Property

    Public Sub New()

        'assume connects to default database
        Dim connStr As String = ConnectionString.DefaultDb
        _con = New SqlConnection(connStr)

        _cmd = _con.CreateCommand
        _cmd.CommandType = CommandType.StoredProcedure

        _da = New SqlDataAdapter

    End Sub

    Public Sub SetCommandText(ByVal cmdText As String)

        _cmd.CommandText = cmdText

    End Sub

    Private Sub AddParams(ByVal parameters As List(Of Parameter))

        For Each parameter As Parameter In parameters

            If parameter.Name.StartsWith("@") Then
                _cmd.Parameters.AddWithValue(parameter.Name, parameter.Value)

            Else
                _cmd.Parameters.AddWithValue("@" & parameter.Name, parameter.Value)

            End If

        Next

    End Sub

    Public Function GetDataSet(ByVal procedure As StoredProcedure) As DataSet

        _cmd.CommandText = procedure.Name

        'how to add output parameters?
        AddParams(procedure.Parameters)

        Try
            Dim ds As New DataSet

            _con.Open()
            _da.SelectCommand = _cmd
            _da.Fill(ds)

            Return ds

        Finally
            _con.Close()
            _cmd.Dispose()
            _con.Dispose()

        End Try

    End Function

    Public Function GetDataSet(ByVal spName As String) As DataSet

        Dim procedure As New StoredProcedure
        procedure.Name = spName

        Return GetDataSet(procedure)

    End Function



End Class
