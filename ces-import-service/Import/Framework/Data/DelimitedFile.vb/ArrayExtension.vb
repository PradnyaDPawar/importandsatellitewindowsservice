﻿Public Module ArrayExtension

    <System.Runtime.CompilerServices.Extension()> _
    Public Sub RemoveAt(Of T)(ByRef a() As T, ByVal index As Integer)
        ' Move elements after "index" down 1 position.
        Array.Copy(a, index + 1, a, index, UBound(a) - index)
        ' Shorten by 1 element.
        ReDim Preserve a(UBound(a) - 1)
    End Sub

    <System.Runtime.CompilerServices.Extension()> _
    Public Sub DropFirstElement(Of T)(ByRef a() As T)
        a.RemoveAt(0)
    End Sub

    <System.Runtime.CompilerServices.Extension()> _
    Public Sub DropLastElement(Of T)(ByRef a() As T)
        a.RemoveAt(UBound(a))
    End Sub
End Module

