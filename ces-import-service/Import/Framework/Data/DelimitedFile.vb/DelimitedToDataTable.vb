﻿Public Class DelimitedToDataTable

    Public Shared Function Convert(ByRef values As List(Of String), ByVal headers As List(Of String)) As DataTable

        Dim returnTable As DataTable = DataTableTools.GetNew("Data", headers)

        RemoveFirstRowHeaders(values, headers)

        FillTable(values, headers.Count, returnTable)

        Return returnTable

    End Function

    Public Shared Function Convert(ByRef values As List(Of String), ByVal colCount As Integer) As DataTable

        Dim returnTable As DataTable = DataTableTools.GetNew("Data", colCount)

        'RemoveFirstRowHeaders(values, headers)

        FillTable(values, colCount, returnTable)

        Return returnTable

    End Function


    Private Shared Sub FillTable(ByVal values As List(Of String), ByVal colCount As Integer, ByRef returnTable As DataTable)

        Dim colIndex As Integer = 0

        Dim newRow As DataRow = returnTable.NewRow

        For Each value As String In values

            newRow(colIndex) = value
            colIndex += 1

            'new row
            If colIndex = colCount Then
                colIndex = 0
                returnTable.Rows.Add(newRow)

                newRow = returnTable.NewRow

            End If

        Next

    End Sub

    Private Shared Sub RemoveFirstRowHeaders(ByRef values As List(Of String), ByVal headers As List(Of String))
        'remove the first row if this contains the headers

        Dim removeRow As Boolean = True

        'get first row in file
        Dim firstRow As List(Of String)

        'get first row header count if possible
        If values.Count >= headers.Count Then
            firstRow = values.GetRange(0, headers.Count)

        Else
            firstRow = New List(Of String)

        End If

        For index As Integer = 0 To firstRow.Count - 1

            firstRow(index) = firstRow(index).Replace(" ", "").ToLower

        Next

        'check is all headers are in the first row
        For Each col As String In headers

            If Not firstRow.Contains(col.Replace(" ", "").ToLower) Then

                removeRow = False

            End If

        Next

        'remove first row if this matches headers
        If removeRow Then

            values.RemoveRange(0, headers.Count)

        End If

    End Sub


End Class
