﻿
Public Class DelimitedFile

    Private _headers As List(Of String)
    Private _items As List(Of String)
    Private _colCount As Integer

    Private _removeQuotes As Boolean
    Private _trimWhiteSpace As Boolean
    Private _colCountStrict As Boolean

    Private _delimiter() As String

    Property ColCount() As Integer
        Get
            Return _colCount
        End Get
        Set(ByVal value As Integer)
            _colCount = value
        End Set
    End Property


    Property Items() As List(Of String)
        Get
            Return _items
        End Get
        Set(ByVal value As List(Of String))
            _items = value
        End Set
    End Property

    Property Headers() As List(Of String)
        Get
            Return _headers
        End Get
        Set(ByVal value As List(Of String))
            _headers = value
        End Set
    End Property

    Property RemoveQuotes() As Boolean
        Get
            Return _removeQuotes
        End Get
        Set(ByVal value As Boolean)
            _removeQuotes = value

        End Set
    End Property

    Property TrimWhiteSpace()
        Get
            Return _trimWhiteSpace
        End Get
        Set(ByVal value)
            _trimWhiteSpace = value
        End Set
    End Property

    Property ColCountString() As Boolean
        Get
            Return _colCountStrict

        End Get
        Set(ByVal value As Boolean)
            _colCountStrict = value
        End Set
    End Property

    ''' <summary>
    ''' Default delimiter is "," csv
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()

        'default is a csv file
        Dim delimeter() As String = {","}
        Init(delimeter)

    End Sub

    Public Sub New(ByVal delimiter() As String)

        Init(delimiter)

    End Sub

    Public Sub Init(ByVal delimiter() As String)

        _removeQuotes = True
        _trimWhiteSpace = True
        _colCountStrict = True
        _delimiter = delimiter

    End Sub


    ''' <summary>
    ''' Load delimited file
    ''' </summary>
    ''' <param name="filePath">Full path of file</param>
    ''' <param name="headers">List of file headers</param>
    ''' <remarks></remarks>
    ''' 
    Public Sub Load(ByRef filePath As String, ByVal headers As List(Of String))

        'headers are always assumed to be the first row
        _headers = headers

        Dim delReader As New DelimitedReader
        _items = delReader.GetFile(filePath, _delimiter, _removeQuotes, _trimWhiteSpace)
        _colCount = delReader.GetColumnCount(filePath, _delimiter, _removeQuotes, _trimWhiteSpace)

        ValidateColCount(True)

    End Sub

    Public Sub Load(ByRef filePath As String)

        Dim delReader As New DelimitedReader
        _items = delReader.GetFile(filePath, _delimiter, _removeQuotes, _trimWhiteSpace)
        _colCount = delReader.GetColumnCount(filePath, _delimiter, _removeQuotes, _trimWhiteSpace)

        ValidateColCount(False)
    End Sub


    Public Function ToDataTable(Optional ByVal ByHeaders As Boolean = True) As DataTable

        ValidateColCount(ByHeaders)  'exception thrown if failed
        If ByHeaders Then
            Return DelimitedToDataTable.Convert(_items, _headers)
        Else
            Return DelimitedToDataTable.Convert(_items, _colCount)
        End If
    End Function


    Private Sub ValidateColCount(ByVal ByHeaders As Boolean)
        'validate the rows each contain a value for each header
        Dim ratio As Double

        If ByHeaders Then

            ratio = (_items.Count / _headers.Count)

        Else

            ratio = (_items.Count / _colCount)

        End If


        'get integer part
        Dim intPart As Integer = Math.Truncate(ratio)

        If ratio - intPart <> 0 Then

            'if there is no decimal
            '  Throw New CustomException("Inconsistent column count in file")

        End If

    End Sub

End Class
