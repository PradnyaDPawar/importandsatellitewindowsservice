﻿Imports Microsoft.VisualBasic.FileIO
Imports System.IO

Public Class DelimitedFile

    Private _filePath As String = Nothing
    Private _delimiter As String = ","
    Private _hasHeader As Boolean = True
    Private _numberOfColumns As Integer = 0
    Private _data As DataTable = Nothing
    Private _isQuoted As Boolean = False
    Private _trimWhiteSpace As Boolean = False

    Property Delimiter() As String
        Get
            Return _delimiter
        End Get
        Set(ByVal value As String)
            _delimiter = value
        End Set
    End Property

    Property TrimWhiteSpace() As Boolean
        Get
            Return _trimWhiteSpace
        End Get
        Set(ByVal value As Boolean)
            _trimWhiteSpace = value
        End Set
    End Property

    Property FieldsAreQuoted() As Boolean
        Get
            Return _isQuoted
        End Get
        Set(ByVal value As Boolean)
            _isQuoted = value
        End Set
    End Property

    Property HasHeader() As Boolean
        Get
            Return _hasHeader
        End Get
        Set(ByVal value As Boolean)
            _hasHeader = value
        End Set
    End Property

    ReadOnly Property NumberOfColumns() As Integer
        Get
            Return _numberOfColumns
        End Get
    End Property

    ReadOnly Property Table() As DataTable
        Get
            Return _data
        End Get
    End Property

    Public Sub New(ByVal path As String, ByVal columnCount As Integer)
        _filePath = path
        _numberOfColumns = columnCount
    End Sub

    Public Sub Load(ByRef dataTable As DataTable)
        _data = New DataTable
        LoadTable()
        dataTable = _data
    End Sub
    'Returns datatable from CSV File =>For Veolia Real Time transpose semicolon seperated file
    Public Sub ReadSemicolonSeperatedCSV()
        Dim sr As New StreamReader(_filePath)
        Dim fullFileStr As String = sr.ReadToEnd()
        sr.Close()
        sr.Dispose()
        Dim lines As String() = fullFileStr.Split(ControlChars.Lf)
        Dim sArr As String() = lines(0).Split(_delimiter + "c")
        For Each s As String In sArr
            'Adds the header row after splitting as per delimiter
            _data.Columns.Add(New DataColumn())
        Next
        'code to add fields after splitting as per delimiter and also if fields are double quotted
        Dim row As DataRow
        Dim finalLine As String = ""
        For Each line As String In lines
            row = _data.NewRow()
            finalLine = line.Replace(Convert.ToString(ControlChars.Cr), "")
            row.ItemArray = finalLine.Replace("""", "").Split(_delimiter + "c")
            _data.Rows.Add(row)
        Next
    End Sub
    Public Sub LoadNonConsistentTable(ByRef dataTable As DataTable, ByRef filedelimiter As String)

        _delimiter = filedelimiter
        _data = New DataTable()
        'if use head then no head in the CSV or consider the header is csv content
        _hasHeader = False
        ReadSemicolonSeperatedCSV()
        dataTable = _data
    End Sub

    Public Sub Load(ByRef dataTable As DataTable, ByRef headers As List(Of String))
        _data = New DataTable()
        'if use head then no head in the CSV or consider the header is csv content
        _hasHeader = False
        LoadTable()
        UpdateColumnName(headers)
        dataTable = _data
    End Sub

    Public Sub LoadNonConsistentTable(ByRef dataTable As DataTable, ByRef headers As List(Of String))
        _data = New DataTable()
        'if use head then no head in the CSV or consider the header is csv content
        _hasHeader = False
        loadNonConsistTable()
        UpdateColumnName(headers)
        dataTable = _data
    End Sub

    Public Sub LoadSms2(ByRef dataTable As DataTable, ByRef headers As List(Of String))
        _data = New DataTable()
        'if use head then no head in the CSV or consider the header is csv content
        _hasHeader = False
        LoadTable()
        _data.Rows(0).Delete()
        For i As Integer = 0 To _data.Columns.Count - 1
            _data.Columns(i).ColumnName = _data.Columns(i).ColumnName.Replace(" ", "").ToLower()
            If (i > 1 And i < 50) Then ''2 column are more in header-kwh n meterserial
                _data.Columns(i).ColumnName = headers(i + 2)
                If (i >= 50) Then
                    _data.Rows(i).Delete()
                End If
            End If
        Next
        _data.Columns(0).ColumnName = headers(0)
        _data.Columns(1).ColumnName = headers(3)

        dataTable = _data
    End Sub

    Private Sub UpdateColumnName(ByVal headers As List(Of String))
        For i As Integer = 0 To _data.Columns.Count - 1
            _data.Columns(i).ColumnName = headers(i)
        Next
    End Sub

    Private Sub LoadTable()
        Using parser As New TextFieldParser(_filePath)
            parser.TextFieldType = FileIO.FieldType.Delimited
            parser.SetDelimiters(_delimiter)
            parser.HasFieldsEnclosedInQuotes = _isQuoted
            parser.TrimWhiteSpace = _trimWhiteSpace

            Dim currentRow As String()
            If Not parser.EndOfData Then
                Try
                    currentRow = parser.ReadFields
                    'remove additional header
                    If _hasHeader Then
                        If currentRow(currentRow.Length - 1) = Nothing Then
                            currentRow.DropLastElement()
                        End If
                    End If

                    If _numberOfColumns > 0 Then
                        If currentRow.Length <> _numberOfColumns Then
                            ' decide what to do when the actual number of columns does not match the expected numevr
                            Throw New ApplicationException("The csv file is invalid: the number of column doesn't match the definition.")
                        End If
                    Else
                        _numberOfColumns = currentRow.Length
                    End If

                    If _hasHeader Then
                        ' add columns with items as column names
                        For Each item As String In currentRow
                            _data.Columns.Add(New DataColumn(item.Trim))
                        Next
                    Else
                        ' add columns with indices as column names
                        For index As Integer = 0 To _numberOfColumns - 1
                            _data.Columns.Add(New DataColumn(index.ToString()))
                        Next

                        ' assign items to column data
                        Dim row As DataRow = _data.NewRow()
                        For index As Integer = 0 To _numberOfColumns - 1
                            row(index) = currentRow.ElementAt(index)
                        Next
                        _data.Rows.Add(row)
                    End If
                Catch ex As MalformedLineException
                    ' decide what to do when theparser can not read the row
                    Throw New ApplicationException("Cannot read the csv file: " & ex.Message)
                End Try



                While Not parser.EndOfData
                    Try

                        'read next row
                        currentRow = parser.ReadFields()

                        ''empty last row
                        If parser.EndOfData = True AndAlso (currentRow.Length = 1 And _numberOfColumns > 1) Then
                            '  If parser.EndOfData = True AndAlso String.IsNullOrEmpty(CStr(currentRow(0))) Then
                            Exit Sub
                        End If

                        'If currentRow(currentRow.Length - 1) = Nothing Then
                        '    currentRow.DropLastElement()
                        'End If

                        If currentRow.Length <> _numberOfColumns Then
                            ' decide what to do when the actual number of columns does not match the expected number

                            If currentRow.Length - 1 = _numberOfColumns AndAlso currentRow(currentRow.Length - 1).Trim = Nothing Then
                                ' if the content row has an additional row with ",<empty>", then still pass
                            Else
                                Throw New ApplicationException("The csv file is invalid: the number of column doesn't match the definition.")
                            End If
                        End If
                        ' assign items to column data
                        Dim row As DataRow = _data.NewRow()
                        For index As Integer = 0 To _numberOfColumns - 1
                            row(index) = currentRow.ElementAt(index)
                        Next
                        _data.Rows.Add(row)
                    Catch ex As MalformedLineException
                        ' decide what to do when theparser can not read the row
                        Throw New ApplicationException("Cannot read the csv file: " & ex.Message)
                    End Try

                End While

            End If

        End Using
    End Sub


    Private Sub loadNonConsistTable()
        Using parser As New TextFieldParser(_filePath)
            parser.TextFieldType = FileIO.FieldType.Delimited
            parser.SetDelimiters(_delimiter)
            parser.HasFieldsEnclosedInQuotes = _isQuoted
            parser.TrimWhiteSpace = _trimWhiteSpace

            Dim currentRow As String()
            If Not parser.EndOfData Then
                Try
                    currentRow = parser.ReadFields

                    If _numberOfColumns <= 0 Then
                        ' asume header column is the longest one if it has header??
                        _numberOfColumns = currentRow.Length
                    End If

                    If _hasHeader Then

                        'For Each item As String In currentRow
                        '    _data.Columns.Add(New DataColumn(item.Trim))
                        'Next
                    Else
                        ' add columns with indices as column names
                        For index As Integer = 0 To _numberOfColumns - 1
                            _data.Columns.Add(New DataColumn(index.ToString()))
                        Next

                        ' assign items to column data
                        Dim row As DataRow = _data.NewRow()
                        For index As Integer = 0 To currentRow.Length - 1
                            row(index) = currentRow.ElementAt(index)
                        Next
                        _data.Rows.Add(row)
                    End If
                Catch ex As MalformedLineException
                    ' decide what to do when theparser can not read the row
                    Throw New ApplicationException("Cannot read the csv file: " & ex.Message)
                End Try



                While Not parser.EndOfData
                    Try
                        'read next row
                        currentRow = parser.ReadFields()

                        'empty last row
                        If parser.EndOfData = True AndAlso currentRow.Length = 1 Then
                            Exit Sub
                        End If

                        ' assign items to column data
                        Dim row As DataRow = _data.NewRow()
                        For index As Integer = 0 To currentRow.Length - 1
                            row(index) = currentRow.ElementAt(index)
                        Next
                        _data.Rows.Add(row)
                    Catch ex As MalformedLineException
                        ' decide what to do when theparser can not read the row
                        Throw New ApplicationException("Cannot read the csv file: " & ex.Message)
                    End Try

                End While

            End If

        End Using
    End Sub

    Public Function DataAt(ByVal row As Integer, ByVal col As Integer)
        Return _data.Rows.Item(row).Item(col)
    End Function

    Public Function ColumnNameAt(ByVal index As Integer)
        Return _data.Columns.Item(index).ColumnName
    End Function

    Public Function HasColumn(ByVal name As String) As Boolean
        Return _data.Columns.Contains(name)
    End Function
End Class
