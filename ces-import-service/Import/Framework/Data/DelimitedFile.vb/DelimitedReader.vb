﻿
Public Class DelimitedReader

    Public Function GetFile(ByVal fileName As String, ByVal delimiter() As String, _
                            ByVal removeQuotes As Boolean, ByVal trimWhitSpace As Boolean) As List(Of String)

        Dim afile As FileIO.TextFieldParser = New FileIO.TextFieldParser(fileName)
        Dim readRows As New List(Of String)
        Dim currentRow() As String
        Dim colcount As Integer

        afile.SetDelimiters(delimiter)

        afile.TrimWhiteSpace = trimWhitSpace
        afile.HasFieldsEnclosedInQuotes = removeQuotes
        colcount = GetColumnCount(fileName, delimiter, removeQuotes, trimWhitSpace)
        ' parse the actual file
        Do While Not afile.EndOfData
            currentRow = afile.ReadFields()
            readRows.AddRange(currentRow)
            If currentRow.Count < colcount Then

                For i As Integer = currentRow.Count To colcount - 1
                    readRows.Add("")
                Next

            ElseIf currentRow.Count > colcount Then

                For i As Integer = currentRow.Count - 1 To colcount Step -1
                    readRows.Remove(readRows.Last)
                Next


            End If


            Array.Clear(currentRow, 0, currentRow.GetUpperBound(0))

        Loop

        afile.Dispose()

        Return readRows

    End Function

    Public Function GetColumnCount(ByVal fileName As String, ByVal delimiter() As String, _
                            ByVal removeQuotes As Boolean, ByVal trimWhitSpace As Boolean) As Integer


        Dim textLine As String = ""
        Dim afile As FileIO.TextFieldParser = New FileIO.TextFieldParser(fileName)
        Dim columncount, rowCount As Integer
        Dim rowElements() As String

        afile.SetDelimiters(delimiter)

        afile.TrimWhiteSpace = True
        afile.HasFieldsEnclosedInQuotes = removeQuotes

        ' parse the actual file
        Do While Not afile.EndOfData
            rowCount = 0
            rowElements = afile.ReadFields()
            Dim lastElement As Integer = rowElements.Count
            Dim loopIndex As Integer = 0
            'Looping to find the count instead of rowElements.Count
            ' in order to exclude the last blank string returned by ReadFields in cases where line ends with comma
            For Each rowelement In rowElements
                loopIndex = loopIndex + 1
                If Not (rowelement = "" And loopIndex = lastElement) Then
                    rowCount = rowCount + 1
                End If
            Next
            If columncount < rowCount Then
                columncount = rowCount

            End If
        Loop

        afile.Dispose()

        Return columncount

    End Function



End Class
