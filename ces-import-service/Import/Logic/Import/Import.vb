﻿
Imports Framework

Public Class Import

    'Public Shared Sub SetInProgress()
    '    'set the next set of imports to be in progress

    '    DbWriter.ExecProcedure(New StoredProcedure("Import_Realtime_InProgress_U"))

    'End Sub

    Public Shared Function GetImportFilePath(ByVal importId As Integer)
        Dim n As Integer = Math.Floor((importId - 1) / 500)
        Dim SubPath As String = ((n + 1) * 500).ToString
        Dim m As Integer = Math.Floor((importId - 1) / 250000)
        Dim dirPath As String = ((m + 1) * 250000).ToString
        Return dirPath & "\" & SubPath
    End Function


End Class
