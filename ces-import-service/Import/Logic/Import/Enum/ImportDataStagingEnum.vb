﻿Public Enum ImportDataStagingEnum

    ImportDataStagingHH = 1
    ImportDataStagingRT = 2
    ImportDataStagingSupplierStatement = 5
    ImportDataStagingStaticVariable = 11
    ImportDataStagingClient = 12
    ImportDataStagingInvoice = 4
    ImportDataStagingOperatingSchedule = 8
    ImportDataStagingAdvancedBuilding = 14
    ImportDataStagingProject = 15
    ImportDataStagingProjectMeter = 16
    ImportDataStagingMeter = 7
    ImportDataStagingBuilding = 6
    ImportDataStagingMeterEdit = 17
    ImportDataStagingAssetEdit = 19
    ImportDataStagingAdvancedAssetEdit = 20
    ImportDataStagingEnergyDriver = 9
    ImportDataStagingTariff = 10     'Tariff
    ImportDataStagingReading = 3     'Digital energy Readings
    ImportDataStagingTenantHistory = 21     'Tenant History
    ImportDataStagingAssetSummaryInformation = 22     'AssetSummaryInformation
    ImportDataStagingAssetCode = 23    'AssetCode
End Enum

