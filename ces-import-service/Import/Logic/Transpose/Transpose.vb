﻿
Public Class Transpose

    Public Shared Sub Transform(ByRef importEdit As ImportEdit, ByRef dsEdits As ImportDataStagingEdits, ByVal importDir As String)

        'Get import file path
        Dim dirPath As String = IO.Path.Combine(importDir, Import.GetImportFilePath(importEdit.Id))
        Dim fullPath As String = IO.Path.Combine(dirPath, importEdit.Name)

        'find transform instance and fillEdit instance
        Dim transformInterface As ITransformDelegate = TransfromConfig.GetTransformInterface(importEdit)
        If Not transformInterface Is Nothing Then

            Try
                'Transform
                transformInterface.LoadData(fullPath)
                transformInterface.ToDeFormat(dsEdits, importEdit)

                'update total rows and status
                importEdit.TotalRow = transformInterface.ReturnTable.Rows.Count
                importEdit.StatusId = StatusEnum.In_Queue

            Catch ex As Exception
                'Transform error handling
                importEdit.StatusId = StatusEnum.Fail
                If TypeOf (ex) Is ApplicationException Then
                    importEdit.Message = ex.Message
                Else
                    importEdit.Message = "Fail to transform the file. " & ex.Message
                End If
            End Try

        Else
            importEdit.StatusId = StatusEnum.Fail
            importEdit.Message = "Unknown Import Type, please check selected import type and file type."
        End If

    End Sub

End Class
