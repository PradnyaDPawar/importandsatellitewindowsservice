﻿''' <summary>
''' Static class with static variables to provide the columns name defined in 'ImportDataStagingInvoice' table of Db.
''' </summary>
Public Class DeFormatInvoiceTableColumnsName

    'MeterSerial, MPAN, InvoiceNumber, TaxPointDate, Tariff, StartDate, EndDate, StartReadEstimated, EndReadEstimated
    'StartRead, EndRead, IsLastDayApportion, Consumption, ForcekWh, Net,Vat, Gross, CostOnly, Description

    Public Shared MeterSerial As String = "MeterSerial"
    Public Shared MPAN As String = "MPAN"
    Public Shared InvoiceNumber As String = "InvoiceNumber"
    Public Shared TaxPointDate As String = "TaxPointDate"
    Public Shared Tariff As String = "Tariff"
    Public Shared StartDate As String = "StartDate"
    Public Shared EndDate As String = "EndDate"
    Public Shared StartReadEstimated As String = "StartReadEstimated"
    Public Shared EndReadEstimated As String = "EndReadEstimated"
    Public Shared StartRead As String = "StartRead"
    Public Shared EndRead As String = "EndRead"
    Public Shared IsLastDayApportion As String = "IsLastDayApportion"
    Public Shared Consumption As String = "Consumption"
    Public Shared ForcekWh As String = "ForcekWh"
    Public Shared Net As String = "Net"
    Public Shared Vat As String = "Vat"
    Public Shared Gross As String = "Gross"
    Public Shared CostOnly As String = "CostOnly"
    Public Shared Description As String = "Description"

End Class