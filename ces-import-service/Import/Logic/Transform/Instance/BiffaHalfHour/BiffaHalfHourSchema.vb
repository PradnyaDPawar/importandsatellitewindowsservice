﻿Partial Public Class BiffaHalfHourSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        RenameColumnNames(dt)
        AddColumns(dt)
        Return dt

    End Function
    Public Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("MeterSerial")

    End Sub

    Public Shared Sub RenameColumnNames(ByRef dt As DataTable)

        dt.Columns("ConsumptionDate").ColumnName = "Date(dd/MM/yyyy)"

        'Rename HH entries
        Dim dateholder As Date = DateTime.Today

        For i As Integer = 1 To 48
            dt.Columns("kWh_" & i.ToString).ColumnName = dateholder.ToString("HH:mm")
            dateholder = dateholder.AddMinutes(30)
        Next

    End Sub



End Class
