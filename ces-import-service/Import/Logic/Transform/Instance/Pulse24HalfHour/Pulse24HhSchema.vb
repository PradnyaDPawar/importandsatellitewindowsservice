﻿Public Class Pulse24HhSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("mp_refno")
        headers.Add("date")
        headers.Add("hr0030")
        headers.Add("hr0100")
        headers.Add("hr0130")
        headers.Add("hr0200")
        headers.Add("hr0230")
        headers.Add("hr0300")
        headers.Add("hr0330")
        headers.Add("hr0400")
        headers.Add("hr0430")
        headers.Add("hr0500")
        headers.Add("hr0530")
        headers.Add("hr0600")
        headers.Add("hr0630")
        headers.Add("hr0700")
        headers.Add("hr0730")
        headers.Add("hr0800")
        headers.Add("hr0830")
        headers.Add("hr0900")
        headers.Add("hr0930")
        headers.Add("hr1000")
        headers.Add("hr1030")
        headers.Add("hr1100")
        headers.Add("hr1130")
        headers.Add("hr1200")
        headers.Add("hr1230")
        headers.Add("hr1300")
        headers.Add("hr1330")
        headers.Add("hr1400")
        headers.Add("hr1430")
        headers.Add("hr1500")
        headers.Add("hr1530")
        headers.Add("hr1600")
        headers.Add("hr1630")
        headers.Add("hr1700")
        headers.Add("hr1730")
        headers.Add("hr1800")
        headers.Add("hr1830")
        headers.Add("hr1900")
        headers.Add("hr1930")
        headers.Add("hr2000")
        headers.Add("hr2030")
        headers.Add("hr2100")
        headers.Add("hr2130")
        headers.Add("hr2200")
        headers.Add("hr2230")
        headers.Add("hr2300")
        headers.Add("hr2330")
        headers.Add("hr0000")

        Return headers

    End Function


    Public Shared Sub ToDeFormat(ByRef dt As DataTable)
        'DataTableTools.SetHeaders(dt, 1)
        RenameColumns(dt)
        AddColumns(dt)
        PopulateColumns(dt)


    End Sub

    Private Shared Sub RenameColumns(ByRef dt As DataTable)
        If dt.Columns.Contains("mp_refno") Then
            dt.Columns("mp_refno").ColumnName = "MPAN"
        ElseIf dt.Columns.Contains("MPR") Then
            dt.Columns("MPR").ColumnName = "MPAN"
        End If

        dt.Columns("Date").ColumnName = "Date(DD/MM/YYYY)"


        'Rename Time Entries
        Dim dateString As String
        Dim dateHolder As DateTime

        For Each column As DataColumn In dt.Columns

            If column.ColumnName.StartsWith("hr") Then
                dateString = column.ColumnName.Replace("hr", "").Insert(2, ":")

                If IsDate(dateString) Then
                    dateHolder = dateString
                    If dateHolder = "00:00" Then
                        column.ColumnName = "23:30"
                    Else
                        column.ColumnName = dateHolder.AddMinutes(-30).ToString("HH:mm")
                    End If


                End If

            End If

        Next

    End Sub


    Private Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForceKwh")
        dt.Columns.Add("MeterSerial")

    End Sub

    Private Shared Sub PopulateColumns(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows

            row("ForceKwh") = "Yes"

        Next

    End Sub
End Class
