﻿Partial Public Class TmaHalfHour

    Public Function GetHeaders() As List(Of String)

        Dim dateholder As Date = Today()

        Dim headers As New List(Of String)
        headers.Add("MPAN")
        headers.Add("SerialAlias")
        headers.Add("MeterSerial")
        headers.Add("Channel")
        headers.Add("AI/AE")
        headers.Add("Date(DD/MM/YYYY)")



        For i = 0 To 47

            headers.Add(dateholder.AddMinutes(i * 30).ToString("HH:mm"))

        Next

        Return headers

    End Function

    Public Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")

    End Sub


    


End Class
