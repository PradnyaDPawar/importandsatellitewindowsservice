﻿Public Class TmaHalfHour
    Implements ITransformDelegate


    Private _returnTable As DataTable

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvFile As New DelimitedFile(fullPath, 54)
        csvFile.Load(_returnTable, GetHeaders())
    End Sub


    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        AddColumns(_returnTable)

        For Each row As DataRow In _returnTable.Rows

            If row("AI/AE") = "AI" Then
                row("ForcekWh") = "Yes"
                FE_HalfHour.Fill(row, dsEdits, importEdit)
            End If

            If row("AI/AE") = "AE" Then
                row("ForcekWh") = "Yes"
                row("MeterSerial") = "e" & row("MeterSerial")
                FE_HalfHour.Fill(row, dsEdits, importEdit)
            End If
            
        Next

    End Sub
End Class
