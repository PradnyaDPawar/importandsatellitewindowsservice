﻿Public Class EonElectricityInvoiceSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable)
        PrefixMainHeaders(dt, 2)
        DataTableTools.SetHeaders(dt, 4)
        DataTableTools.RemoveTopRows(dt, 3)
        DataTableTools.RemoveBlankColumns(dt)
        FormatColumnHeaders(dt)

        Dim invoiceDataTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        CreateInvoiceEntries(dt, invoiceDataTable)
        PopulateIsLastDayApportion(invoiceDataTable)
        RenameColumns(invoiceDataTable)
        dt = invoiceDataTable
        Return dt
    End Function

    Private Shared Sub PrefixMainHeaders(ByRef dt As DataTable, ByVal MainHeaderRowIndex As Integer)
        Dim mainHeader, mainHeader2 As String
        mainHeader2 = ""
        For Each column As DataColumn In dt.Columns
            mainHeader = dt.Rows(MainHeaderRowIndex)(column)

            If Not String.IsNullOrEmpty(mainHeader) Then
                dt.Rows(MainHeaderRowIndex + 1)(column) = mainHeader & " " & dt.Rows(MainHeaderRowIndex + 1)(column)
                mainHeader2 = mainHeader
            ElseIf Not String.IsNullOrEmpty(mainHeader2) Then
                dt.Rows(MainHeaderRowIndex + 1)(column) = mainHeader2 & " " & dt.Rows(MainHeaderRowIndex + 1)(column)
            End If
        Next
    End Sub

    Public Shared Function FormatColumnHeaders(ByRef dt As DataTable) As DataTable
        For Each column As DataColumn In dt.Columns
            column.ColumnName = column.ColumnName.Trim
        Next
        Return dt
    End Function



    Private Shared Sub CreateInvoiceEntries(ByRef dt As DataTable, ByRef invoiceDataTable As DataTable)
        For Each dtRow As DataRow In dt.Rows
            PopulateRateEntries(dtRow, invoiceDataTable)
            PopulateNonEnergyCharge(dtRow, invoiceDataTable, "Standing Charge £")
            PopulateNonEnergyCharge(dtRow, invoiceDataTable, "Availability Cost £")
            PopulateNonEnergyCharge(dtRow, invoiceDataTable, "Reactive Cost £")
            PopulateNonEnergyCharge(dtRow, invoiceDataTable, "Settlements Cost £")
            PopulateNonEnergyCharge(dtRow, invoiceDataTable, "Monthly Triad Cost £")
            PopulateNonEnergyCharge(dtRow, invoiceDataTable, "Annual Triad Cost £")
            PopulateNonEnergyCharge(dtRow, invoiceDataTable, "Uos Cost £")
            PopulateNonEnergyCharge(dtRow, invoiceDataTable, "CCL Cost £")
        Next


    End Sub

    Private Shared Sub PopulateRateEntries(ByRef dtRow As DataRow, ByRef invoiceTable As DataTable)

        Dim dateHolder As Date
        Dim net, vat As Double

        For count As Integer = 1 To 12
            If dtRow.Table.Columns.Contains("Rate" & " " & count.ToString & " " & "Units") Then
                Dim invoiceTableRow As DataRow = invoiceTable.NewRow
                invoiceTableRow("Invoice Number") = dtRow("Account No.") & "-" & dtRow("MPAN")
                invoiceTableRow("MPAN") = dtRow("Mpan")
                invoiceTableRow("Rate") = dtRow("Rate" & " " & count.ToString & " " & "Tariff")

                dateHolder = dtRow("Bill End Date")
                Dim startDate As New Date(dateHolder.Year, dateHolder.Month, 1)
                invoiceTableRow("Start Date") = startDate.ToShortDateString
                dateHolder = dateHolder.AddDays(1)
                invoiceTableRow("End Date") = dateHolder.ToShortDateString

                If dtRow.Table.Columns.Contains("Reading Details" & " " & count.ToString & " " & "Meter Serial Number") Then
                    invoiceTableRow("Meter Serial") = dtRow("Reading Details" & " " & count.ToString & " " & "Meter Serial Number")
                    invoiceTableRow("Start Read") = dtRow("Reading Details" & " " & count.ToString & " " & "Previous Read")
                    invoiceTableRow("Start Read Estimated") = dtRow("Reading Details" & " " & count.ToString & " " & "Read Type")
                    invoiceTableRow("End Read") = dtRow("Reading Details" & " " & count.ToString & " " & "Present Read")
                    invoiceTableRow("End Read Estimated") = dtRow("Reading Details" & " " & count.ToString & " " & "Read Type 1")
                End If

                invoiceTableRow("Consumption") = dtRow("Rate" & " " & count.ToString & " " & "Units")
                invoiceTableRow("Force kWh") = "Yes"
                invoiceTableRow("Net") = dtRow("RATE" & " " & count.ToString & "  " & "Cost £")
                invoiceTableRow("Net") = invoiceTableRow("Net").ToString.Replace("£", "")
                net = invoiceTableRow("Net")
                vat = net * 0.2
                invoiceTableRow("Vat") = vat
                invoiceTableRow("Gross") = (net + vat).ToString("0.00")
                invoiceTableRow("Cost Only") = "No"
                invoiceTableRow("Description") = ""
                invoiceTable.Rows.Add(invoiceTableRow)
            End If
        Next

    End Sub

    Private Shared Sub PopulateNonEnergyCharge(ByRef dtRow As DataRow, ByRef invoiceTable As DataTable, ByVal charge As String)
        If dtRow.Table.Columns.Contains(charge) Then
            If Not String.IsNullOrEmpty(dtRow(charge)) Then
                Dim invoiceTableRow As DataRow = invoiceTable.NewRow
                Dim net, vat As Double

                invoiceTableRow("Invoice Number") = dtRow("Account No.") & "-" & dtRow("MPAN")
                invoiceTableRow("MPAN") = dtRow("Mpan")
                Dim dateHolder As Date = dtRow("Bill End Date")
                Dim startDate As New Date(dateHolder.Year, dateHolder.Month, 1)
                invoiceTableRow("Start Date") = startDate.ToShortDateString
                dateHolder = dateHolder.AddDays(1)
                invoiceTableRow("End Date") = dateHolder.ToShortDateString
                invoiceTableRow("Net") = dtRow(charge)
                invoiceTableRow("Net") = invoiceTableRow("Net").ToString.Replace("£", "")
                net = invoiceTableRow("Net")
                vat = net * 0.2
                invoiceTableRow("Vat") = vat
                invoiceTableRow("Gross") = (net + vat).ToString("0.00")
                invoiceTableRow("Force kWh") = "Yes"
                invoiceTableRow("Cost Only") = "Yes"
                invoiceTableRow("Description") = charge
                invoiceTable.Rows.Add(invoiceTableRow)

            End If
        End If
    End Sub

    Private Shared Sub PopulateIsLastDayApportion(InvoiceDt As DataTable)

        For Each row As DataRow In InvoiceDt.Rows
            row("IsLastDayApportion") = "No"
        Next

    End Sub

    Private Shared Sub RenameColumns(InvoiceDt As DataTable)
        InvoiceDt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        InvoiceDt.Columns("Meter Serial").ColumnName = "MeterSerial"
        InvoiceDt.Columns("Rate").ColumnName = "Tariff"
        InvoiceDt.Columns("Start Date").ColumnName = "StartDate"
        InvoiceDt.Columns("End Date").ColumnName = "EndDate"
        InvoiceDt.Columns("Start Read").ColumnName = "StartRead"
        InvoiceDt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        InvoiceDt.Columns("End Read").ColumnName = "EndRead"
        InvoiceDt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        InvoiceDt.Columns("Force kWh").ColumnName = "ForcekWh"
        InvoiceDt.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub
End Class
