﻿Imports System.Globalization

Public Class ImServ8HalfHourSchema
    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("MPAN")
        headers.Add("Date(DD/MM/YYYY)")
        headers.Add("00:00")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")
        headers.Add("Total")
        Return headers
    End Function
    Public Shared Sub ToDEFormat(ByRef dt As DataTable)
        RenameColumns(dt)
        AddColumns(dt)
        RemoveUnwantedRowsAndColumns(dt)

        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)


    End Sub
    Public Shared Sub RenameColumns(ByRef dt As DataTable)
        'Rename HH entries
        For Each column As DataColumn In dt.Columns
            If IsDate(column.ColumnName.Replace("TP End", "").Trim(" ")) Then
                column.ColumnName = column.ColumnName.Replace("TP End", "").Trim(" ")
            Else
                'replace space
                column.ColumnName = column.ColumnName.Replace(" ", "")
            End If

            'set alias
            If column.ColumnName.ToLower = "portfolioitem" Then
                column.ColumnName = "MPAN"
            End If
            If column.ColumnName.ToLower = "relativeperiod" Then
                column.ColumnName = "Date(DD/MM/YYYY)"
            End If
        Next
    End Sub
    Public Shared Sub AddColumns(ByRef dt As DataTable)
        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("MeterSerial")
    End Sub
    ''nikitah:refactoring code
    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim map As New Dictionary(Of String, DataRow)

        For Each row As DataRow In dt.Rows

            For Each column As DataColumn In dt.Columns
                If IsDate(row("Date(DD/MM/YYYY)")) And (column.ColumnName <> "Date(DD/MM/YYYY)") And (column.ColumnName <> "ForcekWh") And (column.ColumnName <> "MPAN") And (column.ColumnName <> "MeterSerial") Then 'Just to prevent errors
                    Dim theDate As DateTime = column.ColumnName
                    Dim theTime As DateTime = theDate.ToString("HH:mm") 'Converts the dateTime to a time value
                    Dim theTimeString = theTime.ToString("HH:mm")
                    Dim key As String = row("Date(DD/MM/YYYY)") & row("MPAN")
                    If map.ContainsKey(key) Then
                        Dim theRow As DataRow = map(key)
                        theRow(theTimeString) = IIf(row(theTimeString).Trim(" ") = "-", "", row(theTimeString))
                    Else
                        Dim newRow As DataRow = hhDataTable.NewRow
                        newRow("MeterSerial") = row("MeterSerial")    ' Add the Meter Serial Number
                        newRow("Date(DD/MM/YYYY)") = row("Date(DD/MM/YYYY)")     ' Add the date
                        newRow(theTimeString) = IIf(row(theTimeString).Trim(" ") = "-", "", row(theTimeString))     ' And add the Consumption under the correct Time Column in the row.
                        newRow("MPAN") = row("MPAN")
                        hhDataTable.Rows.Add(newRow)

                        map.Add(key, newRow)
                    End If

                End If
            Next

        Next
        dt = hhDataTable
    End Sub
    Private Shared Function RemoveUnwantedRowsAndColumns(dt As DataTable) As DataTable
        For rowcount As Integer = dt.Rows.Count - 1 To 0 Step -1
            If dt.Rows(rowcount)(0).Contains("Total") Then
                dt.Rows.RemoveAt(rowcount)
            End If
        Next
        dt.Columns.Remove("Total")
        Return dt
    End Function

    Public Shared Sub GetHHTableRow(ByRef row As DataRow)
        Dim objDataTableHHClass As New DataTableHHClass()
        objDataTableHHClass._itemRow = row

        objDataTableHHClass.hhTemp = IIf(row("00:00") Is DBNull.Value OrElse row("00:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("00:00")), 0.0, row("00:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0000 = IIf(row("00:30") Is DBNull.Value OrElse row("00:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("00:30")), 0.0, row("00:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0030 = IIf(row("01:00") Is DBNull.Value OrElse row("01:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("01:00")), 0.0, row("01:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0100 = IIf(row("01:30") Is DBNull.Value OrElse row("01:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("01:30")), 0.0, row("01:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0130 = IIf(row("02:00") Is DBNull.Value OrElse row("02:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("02:00")), 0.0, row("02:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0200 = IIf(row("02:30") Is DBNull.Value OrElse row("02:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("02:30")), 0.0, row("02:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0230 = IIf(row("03:00") Is DBNull.Value OrElse row("03:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("03:00")), 0.0, row("03:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0300 = IIf(row("03:30") Is DBNull.Value OrElse row("03:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("03:30")), 0.0, row("03:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0330 = IIf(row("04:00") Is DBNull.Value OrElse row("04:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("04:00")), 0.0, row("04:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0400 = IIf(row("04:30") Is DBNull.Value OrElse row("04:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("04:30")), 0.0, row("04:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0430 = IIf(row("05:00") Is DBNull.Value OrElse row("05:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("05:00")), 0.0, row("05:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0500 = IIf(row("05:30") Is DBNull.Value OrElse row("05:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("05:30")), 0.0, row("05:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0530 = IIf(row("06:00") Is DBNull.Value OrElse row("06:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("06:00")), 0.0, row("06:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0600 = IIf(row("06:30") Is DBNull.Value OrElse row("06:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("06:30")), 0.0, row("06:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0630 = IIf(row("07:00") Is DBNull.Value OrElse row("07:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("07:00")), 0.0, row("07:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0700 = IIf(row("07:30") Is DBNull.Value OrElse row("07:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("07:30")), 0.0, row("07:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0730 = IIf(row("08:00") Is DBNull.Value OrElse row("08:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("08:00")), 0.0, row("08:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0800 = IIf(row("08:30") Is DBNull.Value OrElse row("08:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("08:30")), 0.0, row("08:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0830 = IIf(row("09:00") Is DBNull.Value OrElse row("09:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("09:00")), 0.0, row("09:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0900 = IIf(row("09:30") Is DBNull.Value OrElse row("09:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("09:30")), 0.0, row("09:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh0930 = IIf(row("10:00") Is DBNull.Value OrElse row("10:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("10:00")), 0.0, row("10:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1000 = IIf(row("10:30") Is DBNull.Value OrElse row("10:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("10:30")), 0.0, row("10:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1030 = IIf(row("11:00") Is DBNull.Value OrElse row("11:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("11:00")), 0.0, row("11:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1100 = IIf(row("11:30") Is DBNull.Value OrElse row("11:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("11:30")), 0.0, row("11:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1130 = IIf(row("12:00") Is DBNull.Value OrElse row("12:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("12:00")), 0.0, row("12:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1200 = IIf(row("12:30") Is DBNull.Value OrElse row("12:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("12:30")), 0.0, row("12:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1230 = IIf(row("13:00") Is DBNull.Value OrElse row("13:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("13:00")), 0.0, row("13:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1300 = IIf(row("13:30") Is DBNull.Value OrElse row("13:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("13:30")), 0.0, row("13:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1330 = IIf(row("14:00") Is DBNull.Value OrElse row("14:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("14:00")), 0.0, row("14:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1400 = IIf(row("14:30") Is DBNull.Value OrElse row("14:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("14:30")), 0.0, row("14:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1430 = IIf(row("15:00") Is DBNull.Value OrElse row("15:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("15:00")), 0.0, row("15:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1500 = IIf(row("15:30") Is DBNull.Value OrElse row("15:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("15:30")), 0.0, row("15:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1530 = IIf(row("16:00") Is DBNull.Value OrElse row("16:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("16:00")), 0.0, row("16:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1600 = IIf(row("16:30") Is DBNull.Value OrElse row("16:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("16:30")), 0.0, row("16:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1630 = IIf(row("17:00") Is DBNull.Value OrElse row("17:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("17:00")), 0.0, row("17:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1700 = IIf(row("17:30") Is DBNull.Value OrElse row("17:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("17:30")), 0.0, row("17:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1730 = IIf(row("18:00") Is DBNull.Value OrElse row("18:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("18:00")), 0.0, row("18:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1800 = IIf(row("18:30") Is DBNull.Value OrElse row("18:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("18:30")), 0.0, row("18:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1830 = IIf(row("19:00") Is DBNull.Value OrElse row("19:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("19:00")), 0.0, row("19:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1900 = IIf(row("19:30") Is DBNull.Value OrElse row("19:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("19:30")), 0.0, row("19:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh1930 = IIf(row("20:00") Is DBNull.Value OrElse row("20:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("20:00")), 0.0, row("20:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2000 = IIf(row("20:30") Is DBNull.Value OrElse row("20:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("20:30")), 0.0, row("20:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2030 = IIf(row("21:00") Is DBNull.Value OrElse row("21:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("21:00")), 0.0, row("21:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2100 = IIf(row("21:30") Is DBNull.Value OrElse row("21:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("21:30")), 0.0, row("21:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2130 = IIf(row("22:00") Is DBNull.Value OrElse row("22:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("22:00")), 0.0, row("22:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2200 = IIf(row("22:30") Is DBNull.Value OrElse row("22:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("22:30")), 0.0, row("22:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2230 = IIf(row("23:00") Is DBNull.Value OrElse row("23:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("23:00")), 0.0, row("23:00"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2300 = IIf(row("23:30") Is DBNull.Value OrElse row("23:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("23:30")), 0.0, row("23:30"))), 2).ToString.Replace(",", ""))
        objDataTableHHClass.hh2330 = IIf(row("Temp") Is DBNull.Value OrElse row("Temp") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(row("Temp") Is DBNull.Value OrElse row("Temp") = Nothing, 0.0, row("Temp"))), 2).ToString.Replace(",", ""))
    End Sub

    Public Shared Function GetShiftedTable(ByVal dataTable As DataTable)
        dataTable.Columns.Add("Temp")
        For Each row As DataRow In dataTable.Rows
            GetHHTableRow(row)
        Next
        dataTable.Columns.Remove("Temp")
        Return dataTable
    End Function
End Class


