﻿Public Class ImServ8HalfHour
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel()
        excel.Load(fullPath)
        Dim xSheet As ExcelSheet = excel.Sheets(0)
        xSheet.HeaderRowIndex = 1
        ReturnTable = xSheet.GetDataTable()
    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        ImServ8HalfHourSchema.ToDEFormat(ReturnTable)
        ImServ8HalfHourSchema.GetShiftedTable(ReturnTable)
        For Each row As DataRow In _returnTable.Rows
            row("ForcekWh") = "Yes"
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
