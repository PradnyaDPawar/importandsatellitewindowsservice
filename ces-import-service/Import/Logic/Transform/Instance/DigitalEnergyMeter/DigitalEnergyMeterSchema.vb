﻿Public Class DigitalEnergyMeterSchema

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        Dim deconfig As DeConfig = New DeConfig()
        Dim ImportVersion As String = deconfig.GetConfig().ImportVersion

        'DataTableTools.RemoveBlankColumns(dt)

        DataTableTools.SetHeaders(dt, 1)
        DataTableTools.RemoveBlankRows(dt)
        renamecolumns(dt)
    End Sub
    Private Shared Sub renamecolumns(ByRef dt As DataTable)
        If (dt.Columns.Contains("Building UPRN")) Then
            dt.Columns("Building UPRN").ColumnName = "BuildingUPRN"
        End If
        If (dt.Columns.Contains("Asset UPRN")) Then
            dt.Columns("Asset UPRN").ColumnName = "BuildingUPRN"
        End If
        dt.Columns("Meter Name").ColumnName = "MeterName"
        dt.Columns("Serial Number").ColumnName = "SerialNumber"
        If (dt.Columns.Contains("MPAN / MPRN")) Then
            dt.Columns("MPAN / MPRN").ColumnName = "MPAN"
        End If
        If (dt.Columns.Contains("Account Number")) Then
            dt.Columns("Account Number").ColumnName = "AccountNumber"
        End If
        If (dt.Columns.Contains("AMR Installed Date")) Then
            dt.Columns("AMR Installed Date").ColumnName = "AMRInstalledDate"
        End If
        If (dt.Columns.Contains("Is Active")) Then
            dt.Columns("Is Active").ColumnName = "IsActive"
        End If
        If (dt.Columns.Contains("Create kWh Export Associate Meter")) Then
            dt.Columns("Create kWh Export Associate Meter").ColumnName = "CreatekWhExportAssociateMeter"
        End If
        If (dt.Columns.Contains("Create kVarh Export Associate Meter")) Then
            dt.Columns("Create kVarh Export Associate Meter").ColumnName = "CreatekVarhExportAssociateMeter"
        End If
        If (dt.Columns.Contains("Create kVarh Associate Meter")) Then
            dt.Columns("Create kVarh Associate Meter").ColumnName = "CreatekVarhAssociateMeter"
        End If
        If (dt.Columns.Contains("Export MPAN")) Then
            dt.Columns("Export MPAN").ColumnName = "ExportMPAN"
        End If
        dt.Columns("Reporting Meter").ColumnName = "ReportingMeter"
        If (dt.Columns.Contains("CRC Meter Type")) Then
            dt.Columns("CRC Meter Type").ColumnName = "CRCMeterType"
        End If
        If (dt.Columns.Contains("CRC Fuel Type")) Then
            dt.Columns("CRC Fuel Type").ColumnName = "CRCFuelType"
        End If
        If (dt.Columns.Contains("Include As CRC Residual")) Then
            dt.Columns("Include As CRC Residual").ColumnName = "IncludeAsCRCResidual"
        End If
        If (dt.Columns.Contains("Carbon Trust Standard")) Then
            dt.Columns("Carbon Trust Standard").ColumnName = "CarbonTrustStandard"
        End If
        If (dt.Columns.Contains("CTS Start Date (dd/mm/yyyy)")) Then
            dt.Columns("CTS Start Date (dd/mm/yyyy)").ColumnName = "CTSStartDate"
        End If
        dt.Columns("Operating Schedule 1").ColumnName = "OperatingSchedule1"
        dt.Columns("Operating Schedule 2").ColumnName = "OperatingSchedule2"
        If (dt.Columns.Contains("Consumption Source")) Then
            dt.Columns("Consumption Source").ColumnName = "ConsumptionSource"
        End If
        If (dt.Columns.Contains("CRC Consumption Source")) Then
            dt.Columns("CRC Consumption Source").ColumnName = "CRCConsumptionSource"
        End If
        If (dt.Columns.Contains("Authorised Capacity")) Then
            dt.Columns("Authorised Capacity").ColumnName = "AuthorisedCapacity"
        End If
        If (dt.Columns.Contains("Excess Capacity")) Then
            dt.Columns("Excess Capacity").ColumnName = "ExcessCapacity"
        End If
        If (dt.Columns.Contains("Calorific Value (Gas)")) Then
            dt.Columns("Calorific Value (Gas)").ColumnName = "CalorificValue"
        End If
        If (dt.Columns.Contains("Date Installed")) Then
            dt.Columns("Date Installed").ColumnName = "DateInstalled"
        End If
        If (dt.Columns.Contains("Date Commissioned")) Then
            dt.Columns("Date Commissioned").ColumnName = "DateCommissioned"
        End If
        If (dt.Columns.Contains("Is Stock Meter")) Then
            dt.Columns("Is Stock Meter").ColumnName = "IsStockMeter"
        End If
        If (dt.Columns.Contains("Tank Size")) Then
            dt.Columns("Tank Size").ColumnName = "TankSize"
        End If
        If (dt.Columns.Contains("Invoice Frequency")) Then
            dt.Columns("Invoice Frequency").ColumnName = "InvoiceFrequency"
        End If
        If (dt.Columns.Contains("Time Zone")) Then
            dt.Columns("Time Zone").ColumnName = "TimeZone"
        End If
        If (dt.Columns.Contains("Meter Operator")) Then
            dt.Columns("Meter Operator").ColumnName = "MeterOperator"
        End If
        If (dt.Columns.Contains("Meter Operator Contact/Access")) Then
            dt.Columns("Meter Operator Contact/Access").ColumnName = "MeterOperatorContact"
        End If
        dt.Columns("Correction Factor").ColumnName = "CorrectionFactor"
        dt.Columns("Conversion Factor").ColumnName = "ConversionFactor"
        dt.Columns("Custom Multiplier").ColumnName = "CustomMultiplier"
        dt.Columns("Validate Readings").ColumnName = "ValidateReadings"
        dt.Columns("Reading Wrap Round").ColumnName = "ReadingWrapRound"
        dt.Columns("Max Read Consumption").ColumnName = "MaxReadConsumption"
        dt.Columns("Real Time").ColumnName = "RealTime"
        dt.Columns("Wrap Round Value").ColumnName = "WrapRoundValue"
        dt.Columns("Is Cumulative").ColumnName = "IsCumulative"
        dt.Columns("Is Raw").ColumnName = "IsRaw"
        dt.Columns("Min Entry").ColumnName = "MinEntry"
        dt.Columns("Max Entry").ColumnName = "MaxEntry"
        dt.Columns("Serial Alias").ColumnName = "SerialAlias"
        If (dt.Columns.Contains("Enable Day/Night Active Period")) Then
            dt.Columns("Enable Day/Night Active Period").ColumnName = "EnableDayNightActivePeriod"
        End If
        If (dt.Columns.Contains("Day Period")) Then
            dt.Columns("Day Period").ColumnName = "DayPeriod"
        End If
        If (dt.Columns.Contains("Night Period")) Then
            dt.Columns("Night Period").ColumnName = "NightPeriod"
        End If
        If (dt.Columns.Contains("Data Collector")) Then
            dt.Columns("Data Collector").ColumnName = "DataCollector"
        End If
        If (dt.Columns.Contains("ESOS Compliance Method")) Then
            dt.Columns("ESOS Compliance Method").ColumnName = "ESOSComplianceMethod"
        End If
        If (dt.Columns.Contains("Parent Meter Serial")) Then
            dt.Columns("Parent Meter Serial").ColumnName = "ParentMeterSerial"
        End If


    End Sub
End Class
