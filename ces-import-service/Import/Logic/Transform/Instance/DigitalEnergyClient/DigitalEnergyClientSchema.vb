﻿Partial Public Class DigitalEnergyClient

    Private Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("Name")
        Return headers
    End Function

    Private Sub ColumnValidate()
        Dim rawHeaders As List(Of String) = GetHeaders()
        For Each header As String In rawHeaders
            If Not _returnTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next
    End Sub

    Private Sub RenameColumns()
        For Each column As DataColumn In _returnTable.Columns
            column.ColumnName = column.ColumnName.Replace(" ", "")
        Next
    End Sub

End Class