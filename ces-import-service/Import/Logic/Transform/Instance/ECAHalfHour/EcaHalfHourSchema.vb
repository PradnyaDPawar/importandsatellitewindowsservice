﻿Public Class EcaHalfHourSchema
    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("Customer Name")
        headers.Add("Account Name")
        headers.Add("Site Ref")
        headers.Add("MPR")
        headers.Add("MSN")
        headers.Add("Corrector SN")
        headers.Add("Logger SN")
        headers.Add("Meter Units")
        headers.Add("Date")
        headers.Add("hr0030")
        headers.Add("hr0100")
        headers.Add("hr0130")
        headers.Add("hr0200")
        headers.Add("hr0230")
        headers.Add("hr0300")
        headers.Add("hr0330")
        headers.Add("hr0400")
        headers.Add("hr0430")
        headers.Add("hr0500")
        headers.Add("hr0530")
        headers.Add("hr0600")
        headers.Add("hr0630")
        headers.Add("hr0700")
        headers.Add("hr0730")
        headers.Add("hr0800")
        headers.Add("hr0830")
        headers.Add("hr0900")
        headers.Add("hr0930")
        headers.Add("hr1000")
        headers.Add("hr1030")
        headers.Add("hr1100")
        headers.Add("hr1130")
        headers.Add("hr1200")
        headers.Add("hr1230")
        headers.Add("hr1300")
        headers.Add("hr1330")
        headers.Add("hr1400")
        headers.Add("hr1430")
        headers.Add("hr1500")
        headers.Add("hr1530")
        headers.Add("hr1600")
        headers.Add("hr1630")
        headers.Add("hr1700")
        headers.Add("hr1730")
        headers.Add("hr1800")
        headers.Add("hr1830")
        headers.Add("hr1900")
        headers.Add("hr1930")
        headers.Add("hr2000")
        headers.Add("hr2030")
        headers.Add("hr2100")
        headers.Add("hr2130")
        headers.Add("hr2200")
        headers.Add("hr2230")
        headers.Add("hr2300")
        headers.Add("hr2330")
        headers.Add("hr0000")

        Return headers
    End Function

    Public Shared Function Validate(ByRef srcTable As DataTable) As Boolean

        For Each header As String In GetHeaders()
            If Not srcTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        RenameColumns(dt)
        CreateColumns(dt)
        PopulateForceKwh(dt)
        Return dt
    End Function

    Public Shared Sub CreateColumns(ByRef dt As DataTable)
        dt.Columns.Add("ForcekWh")
    End Sub


    Public Shared Sub PopulateForceKwh(ByRef dt As DataTable)
        For Each row As DataRow In dt.Rows
            If row("Meter Units").ToString.ToLower.Trim.Contains("kwh") Then
                row("ForcekWh") = "Yes"
            Else
                row("ForcekWh") = "No"
            End If
        Next
    End Sub

    Private Shared Sub RenameColumns(ByRef dt As DataTable)
        If dt.Columns.Contains("MPR") Then
            dt.Columns("MPR").ColumnName = "MPAN"
        End If
        If dt.Columns.Contains("MSN") Then
            dt.Columns("MSN").ColumnName = "MeterSerial"
        End If

        dt.Columns("Date").ColumnName = "Date(DD/MM/YYYY)"


        'Rename Time Entries
        Dim dateString As String
        Dim dateHolder As DateTime

        For Each column As DataColumn In dt.Columns

            If column.ColumnName.StartsWith("hr") Then
                dateString = column.ColumnName.Replace("hr", "").Insert(2, ":")

                If IsDate(dateString) Then
                    dateHolder = dateString
                    If dateHolder = "00:00" Then
                        column.ColumnName = "23:30"
                    Else
                        column.ColumnName = dateHolder.AddMinutes(-30).ToString("HH:mm")
                    End If


                End If

            End If

        Next

    End Sub



End Class


