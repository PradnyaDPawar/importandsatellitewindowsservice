﻿Public Class TrendRealTimeSchema
    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable


       
        Dim realTimeDt As DataTable
        realTimeDt = DeFormatTableTemplates.CreateRealTimeTable()
        setColumnHeader(dt)
        PopulateColumns(dt, realTimeDt)
        Return dt


    End Function
    Public Shared Sub setColumnHeader(ByRef dt As DataTable)
        dt.Columns("0").ColumnName = "Serial"
        dt.Columns("1").ColumnName = "DateTime"
        dt.Columns("2").ColumnName = "CountUnits"
    End Sub
    Private Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)

        Dim success As Boolean = False

        For Each row As DataRow In dt.Rows

            If IsDate(row("DateTime")) Then

                For Each Rrow As DataRow In realTimeDt.Rows
                    If Rrow("MeterName") = row("Serial") And Rrow("DateTime") = CDate(row("DateTime")).ToString("dd/MM/yyyy HH:mm") Then
                        Rrow("CountUnits") = IIf(String.IsNullOrEmpty(row("CountUnits")), 0, Convert.ToInt32(row("CountUnits")))
                    End If
                Next

                If Not success Then
                    Dim realTimeDtRow As DataRow = realTimeDt.NewRow

                    realTimeDtRow("MeterName") = Convert.ToString(row("Serial")).TrimStart("0")
                    realTimeDtRow("DateTime") = CDate(row("DateTime")).ToString("dd/MM/yyyy HH:mm")
                    realTimeDtRow("CountUnits") = IIf(String.IsNullOrEmpty(row("CountUnits")), 0, Convert.ToInt32(row("CountUnits")))


                    realTimeDt.Rows.Add(realTimeDtRow)
                End If


                success = False

            End If
        Next


        dt = realTimeDt

    End Sub
End Class
