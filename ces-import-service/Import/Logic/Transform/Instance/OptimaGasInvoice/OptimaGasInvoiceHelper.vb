﻿Public Class OptimaGasInvoiceHelper

    Public Shared Function CreateInvoiceTable() As DataTable

        Dim deInvoiceTable As New DataTable

        deInvoiceTable.Columns.Add("InvoiceNumber", GetType(String))
        deInvoiceTable.Columns.Add("MeterSerial", GetType(String))
        deInvoiceTable.Columns.Add("MPAN", GetType(String))
        deInvoiceTable.Columns.Add("Tariff", GetType(String))
        deInvoiceTable.Columns.Add("StartDate", GetType(String))
        deInvoiceTable.Columns.Add("EndDate", GetType(String))
        deInvoiceTable.Columns.Add("StartRead", GetType(String))
        deInvoiceTable.Columns.Add("StartReadEstimated", GetType(String))
        deInvoiceTable.Columns.Add("EndRead", GetType(String))
        deInvoiceTable.Columns.Add("EndReadEstimated", GetType(String))
        deInvoiceTable.Columns.Add("Consumption", GetType(String))
        deInvoiceTable.Columns.Add("ForcekWh", GetType(String))
        deInvoiceTable.Columns.Add("InvoiceRate", GetType(String))
        deInvoiceTable.Columns.Add("CostUnit", GetType(String))
        deInvoiceTable.Columns.Add("Net", GetType(String))
        deInvoiceTable.Columns.Add("Vat", GetType(String))
        deInvoiceTable.Columns.Add("Gross", GetType(String))
        deInvoiceTable.Columns.Add("CostOnly", GetType(String))
        deInvoiceTable.Columns.Add("Description", GetType(String))
        deInvoiceTable.Columns.Add("TaxPointDate", GetType(String))
        deInvoiceTable.Columns.Add("IsLastDayApportion", GetType(String))

        Return deInvoiceTable

    End Function

End Class