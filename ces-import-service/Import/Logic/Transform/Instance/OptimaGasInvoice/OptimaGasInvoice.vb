﻿Public Class OptimaGasInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel()
        excel.Load(fullPath)

        Dim xsheet As ExcelSheet = excel.Sheets(0)
        excel.GetSheet("Sheet1", xsheet)

        _returnTable = xsheet.GetDataTable()

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Dim invoiceSchema As New OptimaGasInvoiceSchema
        invoiceSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In invoiceSchema.TransposedDataTable.Rows

            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next

    End Sub
End Class