﻿Imports System.Text.RegularExpressions

''' <summary>
''' This class all the schema functionality to prepare the invoice data table from Optima gas invoice file.
''' </summary>
Public Class OptimaGasInvoiceSchema

#Region "Member variables"

    ''' <summary>
    ''' Variable holds the current value of charge number which will be transposed in a row.
    ''' </summary>
    Private _currentChargeNumber As Integer

    ''' <summary>
    ''' variable holds the current value of band number which will be transposed in a row.
    ''' </summary>
    Private _currentBandNumber As Integer

    ''' <summary>
    ''' Final DataTable populated from the data of invoice file , which will be transfered to Sql bulk copy.
    ''' </summary>
    Private _transposedDataTable As DataTable

    ''' <summary>
    ''' Data table holding the actual invoice information loaded from file.
    ''' </summary>
    Private _invoiceDataTable As DataTable

    ''' <summary>
    ''' Variable holds the key as a transposed table row type and value as column name of invoice data table.
    ''' </summary>
    Private _invoiceRowTypeColumnName As Dictionary(Of OptimaGasInvoiceRowType, String)

#End Region

#Region "Constructor"

    Public Sub New()

        _currentChargeNumber = 0

        _transposedDataTable = New DataTable()
        _transposedDataTable.TableName = "TransposedTable"

        ' Add columns in transpose table as per the schema of 'ImportDataStagingInvoice' table of Db.
        _transposedDataTable = OptimaGasInvoiceHelper.CreateInvoiceTable().Clone()

        PopulateRowTypeColumnNameDictionary()

    End Sub

#End Region

#Region "Properties"

    Public Property TransposedDataTable() As DataTable
        Get
            Return _transposedDataTable
        End Get
        Set(ByVal value As DataTable)
            _transposedDataTable = value
        End Set
    End Property

    Public Property InvoiceDataTable() As DataTable
        Get
            Return _invoiceDataTable
        End Get
        Set(ByVal value As DataTable)
            _invoiceDataTable = value
        End Set
    End Property

    Public Property InvoiceRowTypeColumnNameDictionary() As Dictionary(Of OptimaGasInvoiceRowType, String)
        Get
            Return _invoiceRowTypeColumnName
        End Get
        Set(ByVal value As Dictionary(Of OptimaGasInvoiceRowType, String))
            _invoiceRowTypeColumnName = value
        End Set
    End Property

#End Region

#Region "Public methods"

    ''' <summary>
    ''' This method will prepare the digital energy format invoice data table from Optima gas invoice file.
    ''' </summary>
    ''' <param name="invoiceDt">This data table contains the original data of Optima gas invoice file.</param>
    ''' <remarks></remarks>
    Public Sub ToDeFormat(ByRef invoiceDt As DataTable)

        ' Set header of table
        DataTableTools.SetHeaders(invoiceDt, 1)

        ' Remove any rows from the original table.
        DataTableTools.RemoveBlankRows(invoiceDt)

        _invoiceDataTable = New DataTable()
        _invoiceDataTable.TableName = "InvoiceTable"
        _invoiceDataTable = invoiceDt.Copy()

        PopulateDataInTransposeTable()

        invoiceDt = TransposedDataTable

    End Sub

#End Region

#Region "Member methods"

    ''' <summary>
    ''' This method will initiate the functionality of preparing transpose data table from optima gas invoice file.
    ''' </summary>
    Private Sub PopulateDataInTransposeTable()

        ' Add row for various chrages in invoice.
        PopulateChargesRows()

        ' Add row for various bands in invoice
        PopulateBandsRows()

    End Sub

    ''' <summary>
    ''' This method will call the corresponding charges functionality routines
    ''' </summary>
    Private Sub PopulateChargesRows()

        AddStandingChargeRows()
        AddNthChargeRows()
        AddBillAdjustmentRows()
        AddMiscChargesRows()
        AddVATChargeRows()
        AddCCLChargeRows()

    End Sub

    ''' <summary>
    ''' Method used to add the a rows in transpose table from the information of  standing charges column 'Standing Charge (£)' of invoice file.
    ''' </summary>
    Private Sub AddStandingChargeRows()
        AddRowInTransposeTable(OptimaGasInvoiceRowType.StandingCharge)
    End Sub

    ''' <summary>
    ''' Method used to add the rows in transpose table from the information of Each 'Charge Nth (£)' charge column of invoice file.
    ''' </summary>
    Private Sub AddNthChargeRows()

        Dim previousChargeNumber As Integer = 0

        Dim chargeNColumnRegEx As New Regex("Charge \d+")

        For Each column As DataColumn In InvoiceDataTable.Columns

            If chargeNColumnRegEx.IsMatch(column.ColumnName) Then

                If Integer.TryParse(chargeNColumnRegEx.Match(column.ColumnName).Value.Replace("Charge", "").Trim(), _currentChargeNumber) Then

                    If _currentChargeNumber <> previousChargeNumber Then

                        previousChargeNumber = _currentChargeNumber

                        ' Add row for current Nth charge in transpose data table.
                        AddRowInTransposeTable(OptimaGasInvoiceRowType.ChargeN)

                    End If

                End If

            End If
        Next

    End Sub

    ''' <summary>
    ''' This method will add the row in transpose table containing the information 'Bill Adjustment (£)' column of invoice file.
    ''' </summary>
    Private Sub AddBillAdjustmentRows()
        AddRowInTransposeTable(OptimaGasInvoiceRowType.BillAdjustment)
    End Sub

    ''' <summary>
    ''' This method will add the row in transpose table containing the information 'Misc Charge (£)' column of invoice file.
    ''' </summary>
    Private Sub AddMiscChargesRows()
        AddRowInTransposeTable(OptimaGasInvoiceRowType.MiscCharge)
    End Sub

    ''' This method will add the row in transpose table containing the information 'VAT (£)' column of invoice file.
    Private Sub AddVATChargeRows()
        AddRowInTransposeTable(OptimaGasInvoiceRowType.VAT)
    End Sub

    ''' This method will add the row in transpose table containing the information 'CCL (£)' column of invoice file.
    Private Sub AddCCLChargeRows()
        AddRowInTransposeTable(OptimaGasInvoiceRowType.CCL)
    End Sub

    ''' <summary>
    ''' This method provides the functionality to add the rows in transpose table from the information provided in Band columns of invoice file.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateBandsRows()

        Dim previousBandNumber As Integer = 0
        Dim nthUnitBandColumnRegEx As New Regex("Band \d+ Units")

        For Each column As DataColumn In InvoiceDataTable.Columns

            If nthUnitBandColumnRegEx.IsMatch(column.ColumnName) Then

                If Integer.TryParse(nthUnitBandColumnRegEx.Match(column.ColumnName).Value.Replace("Band", "").Replace("Units", "").Trim(), _currentBandNumber) Then

                    If _currentBandNumber <> previousBandNumber Then

                        previousBandNumber = _currentBandNumber

                        'Add transpose row for Nth band in transposed table.
                        AddRowInTransposeTable(OptimaGasInvoiceRowType.BandN)

                    End If

                End If

            End If

        Next

    End Sub

    ''' <summary>
    ''' This method adds the row in transpose table as per the row type.
    ''' </summary>
    ''' <param name="rowType">Row type corresponding to column in invoice file.</param>
    ''' <remarks></remarks>
    Private Sub AddRowInTransposeTable(ByVal rowType As OptimaGasInvoiceRowType)

        Dim column As DataColumn
        Dim columnName As String

        Select Case rowType

            Case OptimaGasInvoiceRowType.ChargeN

                columnName = String.Format(InvoiceRowTypeColumnNameDictionary.Item(rowType), _currentChargeNumber)
                Exit Select

            Case OptimaGasInvoiceRowType.BandN

                columnName = String.Format(InvoiceRowTypeColumnNameDictionary.Item(rowType), _currentBandNumber)

                Exit Select

            Case Else
                columnName = InvoiceRowTypeColumnNameDictionary.Item(rowType)

        End Select

        column = InvoiceDataTable.Columns(columnName)

        If column IsNot Nothing Then

            ' Iterating through all the values of standing charge
            For Each row As DataRow In column.Table.Rows

                Dim cellValue As Object = row(columnName)

                If Not IsDBNull(cellValue) And
                    Not String.IsNullOrEmpty(cellValue) Then

                    Dim value As Double
                    Double.TryParse(cellValue.ToString(), value)

                    If rowType <> OptimaGasInvoiceRowType.BandN And value <> 0.0 Then

                        ' If column is any type of charge column and it has non negative, non zero value, adding row in transpose table.
                        Dim newRow As DataRow = TransposedDataTable.NewRow()
                        CopyRow(newRow, row, rowType)

                        TransposedDataTable.Rows.Add(newRow)

                    ElseIf rowType = OptimaGasInvoiceRowType.BandN Then

                        ' If column is band column and it has non negative value, then adding row in transpose table.
                        Dim newRow As DataRow = TransposedDataTable.NewRow()
                        CopyRow(newRow, row, rowType)

                        TransposedDataTable.Rows.Add(newRow)

                    End If

                End If

            Next

        End If

    End Sub

    ''' <summary>
    ''' This method is used to copy the content of invoice file row to transpose table row.
    ''' </summary>
    ''' <param name="targetRow">It is the row of transpose table which is populated from the data present in invoice file row</param>
    ''' <param name="sourceRow">It is the invoice file row.</param>
    ''' <param name="rowType">Corresponding row in transpose table.</param>
    Private Sub CopyRow(ByRef targetRow As DataRow, ByRef sourceRow As DataRow, ByVal rowType As OptimaGasInvoiceRowType)

        targetRow(DeFormatInvoiceTableColumnsName.MeterSerial) = IIf(Not IsDBNull(sourceRow("MSN")), sourceRow("MSN"), "")
        targetRow(DeFormatInvoiceTableColumnsName.MPAN) = IIf(Not IsDBNull(sourceRow("MPR")), sourceRow("MPR"), "")
        targetRow(DeFormatInvoiceTableColumnsName.InvoiceNumber) = IIf(Not IsDBNull(sourceRow("Invoice Number")), sourceRow("Invoice Number"), "")
        targetRow(DeFormatInvoiceTableColumnsName.TaxPointDate) = IIf(Not IsDBNull(sourceRow("Invoice/Tax Pt Date")), sourceRow("Invoice/Tax Pt Date"), "")
        targetRow(DeFormatInvoiceTableColumnsName.StartDate) = IIf(Not IsDBNull(sourceRow("Invoice Start Date")), sourceRow("Invoice Start Date"), "")
        targetRow(DeFormatInvoiceTableColumnsName.EndDate) = IIf(Not IsDBNull(sourceRow("Invoice End Date")), sourceRow("Invoice End Date"), "")

        targetRow(DeFormatInvoiceTableColumnsName.StartReadEstimated) =
                                                            IIf(Not IsDBNull(sourceRow("Reading Type")), GetReadEstimation(sourceRow("Reading Type")), "")
        targetRow(DeFormatInvoiceTableColumnsName.EndReadEstimated) =
                                                            IIf(Not IsDBNull(sourceRow("Reading Type")), GetReadEstimation(sourceRow("Reading Type")), "")

        targetRow(DeFormatInvoiceTableColumnsName.IsLastDayApportion) = "Yes"
        targetRow(DeFormatInvoiceTableColumnsName.ForcekWh) = "No"

        Select Case rowType

            Case OptimaGasInvoiceRowType.BandN

                Dim startReadValue As String = IIf(Not IsDBNull(sourceRow("Band " + _currentBandNumber.ToString() + " Previous Read")),
                                                   sourceRow("Band " + _currentBandNumber.ToString() + " Previous Read"),
                                                   "")

                Dim endReadValue As String = IIf(Not IsDBNull(sourceRow("Band " + _currentBandNumber.ToString() + " Present Read")),
                                                sourceRow("Band " + _currentBandNumber.ToString() + " Present Read"),
                                                "")

                Dim description As String = IIf(Not IsDBNull(sourceRow("Band " + _currentBandNumber.ToString() + " Description")),
                                                sourceRow("Band " + _currentBandNumber.ToString() + " Description"),
                                                "")

                Dim consumption As String = IIf(Not IsDBNull(sourceRow("Band " + _currentBandNumber.ToString() + " Units")),
                                                sourceRow("Band " + _currentBandNumber.ToString() + " Units"),
                                                "")

                Dim net As String = IIf(Not IsDBNull(sourceRow("Band " + _currentBandNumber.ToString() + " Charge (£)")),
                                        sourceRow("Band " + _currentBandNumber.ToString() + " Charge (£)"),
                                        "")

                targetRow(DeFormatInvoiceTableColumnsName.StartRead) = startReadValue
                targetRow(DeFormatInvoiceTableColumnsName.EndRead) = endReadValue
                targetRow(DeFormatInvoiceTableColumnsName.Consumption) = consumption

                targetRow(DeFormatInvoiceTableColumnsName.Net) = net
                targetRow(DeFormatInvoiceTableColumnsName.Vat) = 0.0
                targetRow(DeFormatInvoiceTableColumnsName.Gross) = net
                targetRow(DeFormatInvoiceTableColumnsName.Tariff) = description
                targetRow(DeFormatInvoiceTableColumnsName.Description) = description

                targetRow(DeFormatInvoiceTableColumnsName.CostOnly) = "No"

                Exit Select

            Case OptimaGasInvoiceRowType.ChargeN

                targetRow(DeFormatInvoiceTableColumnsName.StartRead) = IIf(Not IsDBNull(sourceRow("Start Read")), sourceRow("Start Read"), "")
                targetRow(DeFormatInvoiceTableColumnsName.EndRead) = IIf(Not IsDBNull(sourceRow("End Read")), sourceRow("End Read"), "")
                targetRow(DeFormatInvoiceTableColumnsName.Consumption) = 0.0
                targetRow(DeFormatInvoiceTableColumnsName.Vat) = 0.0

                ' Adding Nth Charge value
                Dim chargeValue As String = IIf(Not IsDBNull(sourceRow(String.Format(InvoiceRowTypeColumnNameDictionary.Item(rowType), _currentChargeNumber))),
                                                sourceRow(String.Format(InvoiceRowTypeColumnNameDictionary.Item(rowType), _currentChargeNumber)),
                                                "")

                targetRow(DeFormatInvoiceTableColumnsName.Net) = chargeValue
                targetRow(DeFormatInvoiceTableColumnsName.Gross) = chargeValue

                ' Adding Nth charge description
                Dim chargeDesc As String = IIf(Not IsDBNull(sourceRow("Charge " + _currentChargeNumber.ToString() + " Description")),
                                                sourceRow("Charge " + _currentChargeNumber.ToString() + " Description"),
                                                "")

                targetRow(DeFormatInvoiceTableColumnsName.Tariff) = chargeDesc
                targetRow(DeFormatInvoiceTableColumnsName.Description) = chargeDesc

                targetRow(DeFormatInvoiceTableColumnsName.CostOnly) = "Yes"

                Exit Select

            Case OptimaGasInvoiceRowType.StandingCharge

                targetRow(DeFormatInvoiceTableColumnsName.StartRead) = IIf(Not IsDBNull(sourceRow("Start Read")), sourceRow("Start Read"), "")
                targetRow(DeFormatInvoiceTableColumnsName.EndRead) = IIf(Not IsDBNull(sourceRow("End Read")), sourceRow("End Read"), "")
                targetRow(DeFormatInvoiceTableColumnsName.Consumption) = 0.0
                targetRow(DeFormatInvoiceTableColumnsName.Net) = IIf(Not IsDBNull(sourceRow(InvoiceRowTypeColumnNameDictionary.Item(rowType))), sourceRow(InvoiceRowTypeColumnNameDictionary.Item(rowType)), "")
                targetRow(DeFormatInvoiceTableColumnsName.Vat) = 0.0
                targetRow(DeFormatInvoiceTableColumnsName.Gross) = IIf(Not IsDBNull(sourceRow(InvoiceRowTypeColumnNameDictionary.Item(rowType))), sourceRow(InvoiceRowTypeColumnNameDictionary.Item(rowType)), "")
                targetRow(DeFormatInvoiceTableColumnsName.Tariff) = "Standing Charge"
                targetRow(DeFormatInvoiceTableColumnsName.Description) = "Standing Charge"
                targetRow(DeFormatInvoiceTableColumnsName.CostOnly) = "Yes"

                Exit Select

            Case OptimaGasInvoiceRowType.BillAdjustment

                targetRow(DeFormatInvoiceTableColumnsName.StartRead) = sourceRow("Start Read")
                targetRow(DeFormatInvoiceTableColumnsName.EndRead) = sourceRow("End Read")
                targetRow(DeFormatInvoiceTableColumnsName.Consumption) = 0.0
                targetRow(DeFormatInvoiceTableColumnsName.Net) = sourceRow(InvoiceRowTypeColumnNameDictionary.Item(rowType))
                targetRow(DeFormatInvoiceTableColumnsName.Vat) = 0.0
                targetRow(DeFormatInvoiceTableColumnsName.Gross) = sourceRow(InvoiceRowTypeColumnNameDictionary.Item(rowType))
                targetRow(DeFormatInvoiceTableColumnsName.Tariff) = "Bill Adjustment"
                targetRow(DeFormatInvoiceTableColumnsName.Description) = "Bill Adjustment"
                targetRow(DeFormatInvoiceTableColumnsName.CostOnly) = "Yes"

                Exit Select

            Case OptimaGasInvoiceRowType.MiscCharge

                targetRow(DeFormatInvoiceTableColumnsName.StartRead) = sourceRow("Start Read")
                targetRow(DeFormatInvoiceTableColumnsName.EndRead) = sourceRow("End Read")
                targetRow(DeFormatInvoiceTableColumnsName.Consumption) = 0.0
                targetRow(DeFormatInvoiceTableColumnsName.Net) = sourceRow(InvoiceRowTypeColumnNameDictionary.Item(rowType))
                targetRow(DeFormatInvoiceTableColumnsName.Vat) = 0.0
                targetRow(DeFormatInvoiceTableColumnsName.Gross) = sourceRow(InvoiceRowTypeColumnNameDictionary.Item(rowType))
                targetRow(DeFormatInvoiceTableColumnsName.Tariff) = "Misc Charge"
                targetRow(DeFormatInvoiceTableColumnsName.Description) = "Misc Charge"
                targetRow(DeFormatInvoiceTableColumnsName.CostOnly) = "Yes"

                Exit Select

            Case OptimaGasInvoiceRowType.VAT

                targetRow(DeFormatInvoiceTableColumnsName.StartRead) = sourceRow("Start Read")
                targetRow(DeFormatInvoiceTableColumnsName.EndRead) = sourceRow("End Read")
                targetRow(DeFormatInvoiceTableColumnsName.Consumption) = 0.0
                targetRow(DeFormatInvoiceTableColumnsName.Net) = 0.0
                targetRow(DeFormatInvoiceTableColumnsName.Vat) = sourceRow(InvoiceRowTypeColumnNameDictionary.Item(rowType))
                targetRow(DeFormatInvoiceTableColumnsName.Gross) = sourceRow(InvoiceRowTypeColumnNameDictionary.Item(rowType))
                targetRow(DeFormatInvoiceTableColumnsName.Tariff) = "VAT"
                targetRow(DeFormatInvoiceTableColumnsName.Description) = DeFormatInvoiceTableColumnsName.Vat
                targetRow(DeFormatInvoiceTableColumnsName.CostOnly) = "Yes"

                Exit Select

            Case OptimaGasInvoiceRowType.CCL

                targetRow(DeFormatInvoiceTableColumnsName.StartRead) = sourceRow("Start Read")
                targetRow(DeFormatInvoiceTableColumnsName.EndRead) = sourceRow("End Read")
                targetRow(DeFormatInvoiceTableColumnsName.Consumption) = 0.0
                targetRow(DeFormatInvoiceTableColumnsName.Net) = sourceRow(InvoiceRowTypeColumnNameDictionary.Item(rowType))
                targetRow(DeFormatInvoiceTableColumnsName.Vat) = 0.0
                targetRow(DeFormatInvoiceTableColumnsName.Gross) = sourceRow(InvoiceRowTypeColumnNameDictionary.Item(rowType))
                targetRow(DeFormatInvoiceTableColumnsName.Tariff) = "CCL"
                targetRow(DeFormatInvoiceTableColumnsName.Description) = "CCL"
                targetRow(DeFormatInvoiceTableColumnsName.CostOnly) = "Yes"

                Exit Select

            Case Else

        End Select

    End Sub

    ''' <summary>
    ''' Method will populate the transpose table row type as a key and invoice file column name as a value, dictionary.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateRowTypeColumnNameDictionary()

        InvoiceRowTypeColumnNameDictionary = New Dictionary(Of OptimaGasInvoiceRowType, String)

        InvoiceRowTypeColumnNameDictionary.Add(OptimaGasInvoiceRowType.StandingCharge, "Standing Charge (£)")
        InvoiceRowTypeColumnNameDictionary.Add(OptimaGasInvoiceRowType.BillAdjustment, "Bill Adjustment (£)")
        InvoiceRowTypeColumnNameDictionary.Add(OptimaGasInvoiceRowType.ChargeN, "Charge {0} (£)")
        InvoiceRowTypeColumnNameDictionary.Add(OptimaGasInvoiceRowType.MiscCharge, "Misc Charge (£)")
        InvoiceRowTypeColumnNameDictionary.Add(OptimaGasInvoiceRowType.VAT, "VAT (£)")
        InvoiceRowTypeColumnNameDictionary.Add(OptimaGasInvoiceRowType.CCL, "CCL (£)")
        InvoiceRowTypeColumnNameDictionary.Add(OptimaGasInvoiceRowType.BandN, "Band {0} Units")

    End Sub

    Private Function GetReadEstimation(ByVal readEstimated As String) As String

        Dim returnValue As String = String.Empty

        Dim ListActual As New List(Of String)(New String() {"normal"})

        If ListActual.Contains(readEstimated.ToString.ToLower) Then
            returnValue = "A"
        Else
            returnValue = "E"
        End If

        Return returnValue

    End Function

#End Region

End Class

''' <summary>
''' Enum containing type of columns present in invoice file.
''' </summary>
Public Enum OptimaGasInvoiceRowType

    StandingCharge = 0
    BillAdjustment
    ChargeN
    MiscCharge
    VAT
    CCL
    BandN

End Enum