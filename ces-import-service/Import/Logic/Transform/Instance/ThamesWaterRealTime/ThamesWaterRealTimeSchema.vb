﻿Public Class ThamesWaterRealTimeSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

        DataTableTools.RemoveBlankRows(dt)

        Dim realTimeDt As DataTable = DeFormatTableTemplates.CreateRealTimeTable()
        PopulateColumns(dt, realTimeDt)

        Return dt

    End Function

    Public Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)

        For Each row As DataRow In dt.Rows

            'Convert American Date form to English format
            Dim DateTimeValue As DateTime = row("sampleDateRef")

            If IsDate(DateTimeValue) Then

                Dim realTimeDtRow As DataRow = realTimeDt.NewRow

                realTimeDtRow("MeterName") = row("EndPointRef")
                realTimeDtRow("DateTime") = DateTimeValue.ToString("dd/MM/yyyy HH:mm")
                realTimeDtRow("CountUnits") = row("volume")

                realTimeDt.Rows.Add(realTimeDtRow)

            End If

        Next

        dt = realTimeDt

    End Sub

End Class