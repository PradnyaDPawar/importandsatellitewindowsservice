﻿Public Class ThamesWaterRealTime
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        Dim csvfile As New DelimitedFile(fullPath, 0)
        csvfile.LoadNonConsistentTable(_returnTable, ";")

        DataTableTools.SetHeaders(_returnTable, 1)

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        ThamesWaterRealTimeSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_RealTime.Fill(row, dsEdits, importEdit)
        Next

    End Sub

End Class