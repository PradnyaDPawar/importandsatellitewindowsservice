﻿Public Class BritishGasSiemensHalfHourSchema

    Public Shared Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("MPAN")
        headers.Add("Date(DD/MM/YYYY)")
        headers.Add("Unit")
        headers.Add("00:00")
        headers.Add("00:00 Read Type")
        headers.Add("00:30")
        headers.Add("00:30 Read Type")
        headers.Add("01:00")
        headers.Add("01:00 Read Type")
        headers.Add("01:30")
        headers.Add("01:30 Read Type")
        headers.Add("02:00")
        headers.Add("02:00 Read Type")
        headers.Add("02:30")
        headers.Add("02:30 Read Type")
        headers.Add("03:00")
        headers.Add("03:00 Read Type")
        headers.Add("03:30")
        headers.Add("03:30 Read Type")
        headers.Add("04:00")
        headers.Add("04:00 Read Type")
        headers.Add("04:30")
        headers.Add("04:30 Read Type")
        headers.Add("05:00")
        headers.Add("05:00 Read Type")
        headers.Add("05:30")
        headers.Add("05:30 Read Type")
        headers.Add("06:00")
        headers.Add("06:00 Read Type")
        headers.Add("06:30")
        headers.Add("06:30 Read Type")
        headers.Add("07:00")
        headers.Add("07:00 Read Type")
        headers.Add("07:30")
        headers.Add("07:30 Read Type")
        headers.Add("08:00")
        headers.Add("08:00 Read Type")
        headers.Add("08:30")
        headers.Add("08:30 Read Type")
        headers.Add("09:00")
        headers.Add("09:00 Read Type")
        headers.Add("09:30")
        headers.Add("09:30 Read Type")
        headers.Add("10:00")
        headers.Add("10:00 Read Type")
        headers.Add("10:30")
        headers.Add("10:30 Read Type")
        headers.Add("11:00")
        headers.Add("11:00 Read Type")
        headers.Add("11:30")
        headers.Add("11:30 Read Type")
        headers.Add("12:00")
        headers.Add("12:00 Read Type")
        headers.Add("12:30")
        headers.Add("12:30 Read Type")
        headers.Add("13:00")
        headers.Add("13:00 Read Type")
        headers.Add("13:30")
        headers.Add("13:30 Read Type")
        headers.Add("14:00")
        headers.Add("14:00 Read Type")
        headers.Add("14:30")
        headers.Add("14:30 Read Type")
        headers.Add("15:00")
        headers.Add("15:00 Read Type")
        headers.Add("15:30")
        headers.Add("15:30 Read Type")
        headers.Add("16:00")
        headers.Add("16:00 Read Type")
        headers.Add("16:30")
        headers.Add("16:30 Read Type")
        headers.Add("17:00")
        headers.Add("17:00 Read Type")
        headers.Add("17:30")
        headers.Add("17:30 Read Type")
        headers.Add("18:00")
        headers.Add("18:00 Read Type")
        headers.Add("18:30")
        headers.Add("18:30 Read Type")
        headers.Add("19:00")
        headers.Add("19:00 Read Type")
        headers.Add("19:30")
        headers.Add("19:30 Read Type")
        headers.Add("20:00")
        headers.Add("20:00 Read Type")
        headers.Add("20:30")
        headers.Add("20:30 Read Type")
        headers.Add("21:00")
        headers.Add("21:00 Read Type")
        headers.Add("21:30")
        headers.Add("21:30 Read Type")
        headers.Add("22:00")
        headers.Add("22:00 Read Type")
        headers.Add("22:30")
        headers.Add("22:30 Read Type")
        headers.Add("23:00")
        headers.Add("23:00 Read Type")
        headers.Add("23:30")
        headers.Add("23:30 Read Type")

        Return headers
    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        DataTableTools.RemoveBlankColumns(dt)
        SetHeaders(dt, GetHeaders())
        CreateColumns(dt)
        PopulateForceKwhColumn(dt)
        Return dt
    End Function

    Private Shared Sub SetHeaders(ByRef dt As DataTable, ByVal headers As List(Of String))
        For Each column As DataColumn In dt.Columns

            column.ColumnName = headers.ElementAt(dt.Columns.IndexOf(column))

        Next
    End Sub

    Private Shared Sub CreateColumns(ByRef dt As DataTable)
        dt.Columns.Add("MeterSerial")
        dt.Columns.Add("ForcekWh")
    End Sub

    Private Shared Sub PopulateForceKwhColumn(ByRef dt As DataTable)
        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            If row("Unit").ToString.ToLower = "kwh" Then
                row("ForcekWh") = "Yes"
            Else
                row("ForcekWh") = "No"
            End If

            If row("Unit").ToString.ToLower = "lag" Then
                row.Delete()
            End If

            If row("Unit").ToString.ToLower = "lead" Then
                row.Delete()
            End If

        Next
    End Sub
End Class
