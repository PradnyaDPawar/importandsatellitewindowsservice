﻿Partial Public Class DigitalEnergyProjectMeter

    Private Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("SerialNumber")
        headers.Add("ProjectName")
        headers.Add("Remove")
        Return headers
    End Function

    Private Sub ColumnValidate()
        Dim rawHeaders As List(Of String) = GetHeaders()
        For Each header As String In rawHeaders
            If Not _returnTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next
    End Sub

    Private Sub RenameColumns()
        Dim renamables() = {"Remove"}
        For Each column As DataColumn In _returnTable.Columns
            column.ColumnName = column.ColumnName.Replace(" ", "")
            For Each prefix In renamables
                If column.ColumnName.StartsWith(prefix) Then
                    column.ColumnName = prefix
                    Exit For
                End If
            Next
        Next
    End Sub

End Class