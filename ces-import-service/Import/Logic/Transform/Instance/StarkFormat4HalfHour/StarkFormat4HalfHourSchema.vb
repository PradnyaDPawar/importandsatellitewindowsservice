﻿Partial Public Class StarkFormat4HalfHour

    Public Function GetHeaders() As List(Of String)

        Dim dateholder As Date = Today()

        Dim headers As New List(Of String)
        headers.Add("Site Name")
        headers.Add("MPAN")
        headers.Add("MeterSerial")
        headers.Add("Elec HH")
        headers.Add("Date(DD/MM/YYYY)")
        headers.Add("ForcekWh")


        For i = 0 To 47

            headers.Add(dateholder.AddMinutes(i * 30).ToString("HH:mm"))
            headers.Add("Read Type-" & (i + 1).ToString)
        Next

        Return headers

    End Function


    Public Sub UpdateForcekWh(ByRef row As DataRow)
        If row("ForcekWh").ToString.ToLower.Trim = "kwh" Then
            row("ForcekWh") = "Yes"
        Else
            row("ForcekWh") = "No"
        End If
    End Sub

    'Requested by client - FEC
    Private Sub AdjustMeterSerials(ByRef row As DataRow)

        Dim mpansToCheck As New List(Of String)
        mpansToCheck.Add("2300000953198")
        mpansToCheck.Add("2300000953170")
        mpansToCheck.Add("2300000434723")
        mpansToCheck.Add("1900060436871")
        mpansToCheck.Add("2000050522480")
        mpansToCheck.Add("2000050662016")
        mpansToCheck.Add("2300000542819")
        mpansToCheck.Add("2300000336496")
        mpansToCheck.Add("2000051970017")
        mpansToCheck.Add("1050000734054")


        If mpansToCheck.Contains(row("MPAN")) Then
            row("MeterSerial") = "e" & row("MeterSerial")
        End If

    End Sub



End Class
