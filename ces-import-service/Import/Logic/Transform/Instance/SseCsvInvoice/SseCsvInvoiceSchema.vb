﻿Partial Public Class SseCsvInvoice

    Public Sub AssociateInvoiceNumber(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows

            If String.IsNullOrEmpty(row("Invoice Number")) Then

                row("Invoice Number") = dt.Rows(dt.Rows.IndexOf(row) - 1)("Invoice Number")

            End If

        Next

    End Sub

    Public Function GetDialList(ByRef dt As DataTable) As List(Of String)

        Dim list As New List(Of String)

        For Each row As DataRow In dt.Rows
            If Not String.IsNullOrEmpty(row("Register Type")) Then

                If Not list.Contains(row("Register Type")) Then
                    list.Add(row("Register Type"))

                End If
            End If

        Next

        Return list

    End Function

    Public Shared Function GetInvoiceMpanList(ByRef dt As DataTable) As List(Of String)

        Dim list As New List(Of String)
        For Each row As DataRow In dt.Rows

            If Not String.IsNullOrEmpty(row("Invoice Number") & ";" & row("MPAN")) Then

                If Not list.Contains(row("Invoice Number") & ";" & row("MPAN")) Then
                    list.Add(row("Invoice Number") & ";" & row("MPAN"))
                End If

            End If


        Next

        Return list

    End Function

    Public Sub PopulateRateEntries(ByVal mpanInvoiceRows() As DataRow, ByRef invoiceTable As DataTable, ByVal charge As String)
        If mpanInvoiceRows(0).Table.Columns.Contains(charge) Then
            If Not String.IsNullOrEmpty(mpanInvoiceRows(0)(charge)) Then
                Dim net, vat As Double
                Dim invoiceTableRow As DataRow = invoiceTable.NewRow
                invoiceTableRow("Invoice Number") = mpanInvoiceRows(0)("Invoice Number")
                invoiceTableRow("Meter Serial") = mpanInvoiceRows(0)("Current Meter No")
                invoiceTableRow("MPAN") = mpanInvoiceRows(0)("MPAN")
                invoiceTableRow("Rate") = mpanInvoiceRows(0)("Tariff Description")
                invoiceTableRow("Start Date") = mpanInvoiceRows(0)("Supply Period Start Date")
                invoiceTableRow("End Date") = mpanInvoiceRows(0)("Supply Period End Date")
                invoiceTableRow("TaxPointDate") = mpanInvoiceRows(0)("Invoice Date")
                invoiceTableRow("IsLastDayApportion") = "No"
                For Each mpanInvoiceRow In mpanInvoiceRows
                    If mpanInvoiceRow("Register Type") = charge Then

                        invoiceTableRow("Start Read Estimated") = mpanInvoiceRow("Previous Reading Type")
                        invoiceTableRow("End Read Estimated") = mpanInvoiceRow("Present Reading Type")

                        invoiceTableRow("Start Read") = mpanInvoiceRow("Previous Reading")
                        invoiceTableRow("End Read") = mpanInvoiceRow("Present Reading")
                    End If
                Next
                invoiceTableRow("Consumption") = mpanInvoiceRows(0)(charge)
                invoiceTableRow("Force kWh") = "Yes"

                If mpanInvoiceRows(0).Table.Columns.Contains(charge & " " & "Rate") Then
                    net = mpanInvoiceRows(0)(charge) * mpanInvoiceRows(0)(charge & " " & "Rate")
                ElseIf mpanInvoiceRows(0).Table.Columns.Contains(Left(charge, Len(charge) - 1) & " " & "Rate") Then
                    net = mpanInvoiceRows(0)(charge) * mpanInvoiceRows(0)(Left(charge, Len(charge) - 1) & " " & "Rate")
                    net = net / 100
                End If
                invoiceTableRow("Net") = net.ToString("0.00")
                vat = net * GetVatRate(mpanInvoiceRows)
                invoiceTableRow("Vat") = vat.ToString("0.00")
                invoiceTableRow("Gross") = (net + vat).ToString("0.00")
                invoiceTableRow("Cost Only") = "No"
                invoiceTableRow("Description") = charge
                invoiceTable.Rows.Add(invoiceTableRow)
            End If
        End If
    End Sub
    Public Sub GetTransformedHeaders()
        RenameColumns(_returnTable)
    End Sub
    Private Sub RenameColumns(returnTable As DataTable)
        returnTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        returnTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        returnTable.Columns("Start Date").ColumnName = "StartDate"
        returnTable.Columns("End Date").ColumnName = "EndDate"
        returnTable.Columns("Start Read").ColumnName = "StartRead"
        returnTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        returnTable.Columns("End Read").ColumnName = "EndRead"
        returnTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        returnTable.Columns("Force kWh").ColumnName = "ForceKwh"
        returnTable.Columns("Rate").ColumnName = "Tariff"
        returnTable.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub

    Public Function GetVatRate(ByVal mpanInvoiceRows() As DataRow) As Double
        Dim vatRate As Double = -1
        For Each row In mpanInvoiceRows

            If Not String.IsNullOrEmpty(row("Lower/Zero VAT Percentage")) Then
                If row("Lower/Zero VAT Percentage") <> 0 Then

                    If Not String.IsNullOrEmpty(row("VAT Rate")) Then
                        If vatRate = -1 Then
                            vatRate = row("VAT Rate") / 100
                        Else
                            Throw New ApplicationException("Multiple VAT rates found")
                        End If

                    End If
                End If
            End If



        Next

        If vatRate <> -1 Then
            Return vatRate
        Else
            Throw New ApplicationException("Vat could not be determined")
        End If


    End Function

    Public Sub PopulateNonConsumptionCharge(ByVal mpanInvoiceRows() As DataRow, ByRef invoiceTable As DataTable, ByVal charge As String)

        If mpanInvoiceRows(0).Table.Columns.Contains(charge) Then
            If Not String.IsNullOrEmpty(mpanInvoiceRows(0)(charge)) Then

                If mpanInvoiceRows(0)(charge) <> 0 Then

                    Dim invoiceTableRow As DataRow = invoiceTable.NewRow
                    Dim net, vat As Double

                    invoiceTableRow("Invoice Number") = mpanInvoiceRows(0)("Invoice Number")
                    invoiceTableRow("MPAN") = mpanInvoiceRows(0)("MPAN")
                    invoiceTableRow("Meter Serial") = mpanInvoiceRows(0)("Current Meter No")
                    invoiceTableRow("Start Date") = mpanInvoiceRows(0)("Supply Period Start Date")
                    invoiceTableRow("End Date") = mpanInvoiceRows(0)("Supply Period End Date")
                    invoiceTableRow("Force kWh") = "No"
                    net = mpanInvoiceRows(0)(charge)
                    invoiceTableRow("Net") = net
                    vat = net * GetVatRate(mpanInvoiceRows)
                    invoiceTableRow("Vat") = vat
                    invoiceTableRow("Gross") = (net + vat).ToString("0.00")
                    invoiceTableRow("Cost Only") = "Yes"
                    invoiceTableRow("Description") = charge
                    invoiceTableRow("TaxPointDate") = mpanInvoiceRows(0)("Invoice Date")
                    invoiceTableRow("IsLastDayApportion") = "No"
                    invoiceTable.Rows.Add(invoiceTableRow)


                End If

            End If
        End If
    End Sub
End Class
