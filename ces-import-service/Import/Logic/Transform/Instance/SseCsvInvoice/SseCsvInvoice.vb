﻿
Public Class SseCsvInvoice
    Implements ITransformDelegate


    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        'Dim csvFile As New DelimitedFile2
        'csvFile.Load(fullPath)
        'ReturnTable = csvFile.ToDataTable(False)
        Dim csv As New DelimitedFile(fullPath, 0)
        csv.FieldsAreQuoted = True
        csv.Load(_returnTable)
    End Sub
    Private _returnTable As DataTable
    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Dim invoiceDt As DataTable
        invoiceDt = DeFormatTableTemplates.CreateInvoiceTable()

        'Associate invoice number with each line in invoice
        AssociateInvoiceNumber(ReturnTable)

        'Build a list of all rates (dials)
        Dim dialList As List(Of String) = GetDialList(ReturnTable)

        'Build a list of MPAN - Invoice Number
        Dim invoiceMpanList As List(Of String) = GetInvoiceMpanList(ReturnTable)
        Dim mpan, invoice As String

        For Each mpanInvoicePair In invoiceMpanList

            Dim str() As String
            str = mpanInvoicePair.Split(";")
            mpan = str(1)
            invoice = str(0)

            'Select Unique invoice rows
            Dim mpanInvoice() As DataRow = ReturnTable.Select("MPAN = '" & mpan & "' AND [Invoice Number] = '" & invoice & "'")

            'Poulate Cost Only No rows
            For Each dial In dialList
                PopulateRateEntries(mpanInvoice, invoiceDt, dial)
            Next

            'Populate Cost Only Yes rows
            PopulateNonConsumptionCharge(mpanInvoice, invoiceDt, "Standing Charge")
            PopulateNonConsumptionCharge(mpanInvoice, invoiceDt, "Available Cap Charge")
            PopulateNonConsumptionCharge(mpanInvoice, invoiceDt, "Total CCL Charge")
            PopulateNonConsumptionCharge(mpanInvoice, invoiceDt, "Other")

        Next

        ReturnTable = invoiceDt
        GetTransformedHeaders()

        For Each row As DataRow In _returnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub

  
End Class
