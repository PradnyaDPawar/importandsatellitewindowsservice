﻿Public Class EzMeterRealTimeSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

        Dim realTimeDt As DataTable = DeFormatTableTemplates.CreateRealTimeTable()
        PopulateColumns(dt, realTimeDt)
        Return dt

    End Function


    Private Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)
        ' a 2D array of string pairs where the first string in each pair is the column name in the source DataTable
        ' and the second string in each pair is the meter name suffix to apply 
        Dim pairs(,) As String = New String(,) { _
            {"1Voltage", "_1Voltage"}, _
            {"1Current", "_1Current"}, _
            {"1Power", "_1Power"}, _
            {"Phase 1 Frequency", "_Phase 1 Frequency"}, _
            {"Phase 1 Power Factor", "_Phase 1 Power Factor"}, _
            {"2Voltage", "_2Voltage"}, _
            {"2Current", "_2Current"}, _
            {"2Power", "_2Power"}, _
            {"Phase 2 Frequency", "_Phase 2 Frequency"}, _
            {"Phase 2 Power Factor", "_Phase 2 Power Factor"}, _
            {"3Voltage", "_3Voltage"}, _
            {"3Current", "_3Current"}, _
            {"3Power", "_3Power"}, _
            {"Phase 3 Frequency", "_Phase 3 Frequency"}, _
            {"Phase 3 Power Factor", "_Phase 3 Power Factor"}, _
            {"4Voltage", "_4Voltage"}, _
            {"4Current", "_4Current"}, _
            {"4Power", "_4Power"}, _
            {"Phase 4 Frequency", "_Phase 4 Frequency"}, _
            {"Phase 4 Power Factor", "_Phase 4 Power Factor"}, _
            {"Acc. Pwr. Ph. 1  Pos", "_Acc. Pwr. Ph. 1  Pos"}, _
            {"Acc. Pwr. Ph. 2  Pos", "_Acc. Pwr. Ph. 2  Pos"}, _
            {"Acc. Pwr. Ph. 3  Pos", "_Acc. Pwr. Ph. 3  Pos"}, _
            {"Acc. Pwr. Ph. 4  Pos", "_Acc. Pwr. Ph. 4  Pos"}, _
            {"Acc. Pwr. Ph. 1  Neg", "_Acc. Pwr. Ph. 1  Neg"}, _
            {"Acc. Pwr. Ph. 2  Neg", "_Acc. Pwr. Ph. 2  Neg"}, _
            {"Acc. Pwr. Ph. 3  Neg", "_Acc. Pwr. Ph. 3  Neg"}, _
            {"Acc. Pwr. Ph. 4  Neg", "_Acc. Pwr. Ph. 4  Neg"}, _
            {"kWh", "_kWh"}, _
            {"Summed accumulation Counter 2", "_Summed accumulation Counter 2"}, _
            {"Summed accumulation Counter 3", "_Summed accumulation Counter 3"}, _
            {"Summed accumulation Counter 4", "_Summed accumulation Counter 4"} _
            }

        For Each row As DataRow In dt.Rows
            Dim bound0 As Integer = pairs.GetUpperBound(0)
            For i As Integer = 0 To bound0
                Dim column As String = pairs(i, 0)
                Dim suffix As String = pairs(i, 1)

                Dim realTimeDtRow As DataRow = realTimeDt.NewRow
                realTimeDtRow("MeterName") = row("serial") & suffix
                realTimeDtRow("DateTime") = row("date") & " " & row("time")
                realTimeDtRow("CountUnits") = row(column)
                realTimeDt.Rows.Add(realTimeDtRow)
            Next
        Next

        dt = realTimeDt
    End Sub

End Class
