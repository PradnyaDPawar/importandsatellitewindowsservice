﻿Public Class SseCsvInvoice2
    Implements ITransformDelegate


    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Dim csv As New DelimitedFile(fullPath, 0)
        csv.FieldsAreQuoted = True
        csv.Load(_returntable)

    End Sub
    Private _returntable As DataTable
    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returntable
        End Get
        Set(value As DataTable)
            _returntable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Dim invoiceDt As DataTable
        invoiceDt = DeFormatTableTemplates.CreateInvoiceTable()

        'Create Invoice Column Associate invoice number with each line in invoice
        ReturnTable.Columns.Add("Invoice Number")
        AssociateInvoiceNumber(ReturnTable)

        'Build a list of all rates (dials)
        Dim dialList As List(Of String) = GetDialList(ReturnTable)

        'Build a list of all cost only rates
        Dim costOnlyRates As List(Of String) = GetCostOnlyRateList(ReturnTable, dialList)

        'Build a list of MPAN - Invoice Number
        Dim invoiceMpanList As List(Of String) = GetInvoiceMpanList(ReturnTable)
        'Dim mpan, invoice As String

        For Each mpanInvoicePair In invoiceMpanList

            Dim str() As String
            str = mpanInvoicePair.Split(";")
            mpan = str(1)
            invoice = str(0)

            'Poulate Cost Only No rows
            For Each dial In dialList
                PopulateRateEntries(ReturnTable, invoiceDt, dial)
            Next

            'Populate Cost Only Yes rows
            For Each costOnlyRate In costOnlyRates
                PopulateNonConsumptionCharge(ReturnTable, invoiceDt, costOnlyRate)
            Next

            PopulateVAT(ReturnTable, invoiceDt)

        Next

        ReturnTable = invoiceDt
        GetTransformedHeaders()

        For Each row As DataRow In _returntable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
