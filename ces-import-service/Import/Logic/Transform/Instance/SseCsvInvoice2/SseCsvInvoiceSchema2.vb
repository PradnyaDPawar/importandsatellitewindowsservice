﻿Partial Public Class SseCsvInvoice2
    Dim mpan, invoice As String

    Public Sub AssociateInvoiceNumber(ByRef dt As DataTable)
        Dim invoiceCounter As Integer
        invoiceCounter = 0
        For Each row As DataRow In dt.Rows
            If row("Indicator") = "S" Then
                invoiceCounter += 1
                row("Invoice Number") = "I" & invoiceCounter.ToString
            Else
                row("Invoice Number") = "I" & invoiceCounter.ToString
            End If
        Next
    End Sub

    Public Function GetDialList(ByRef dt As DataTable) As List(Of String)
        Dim list As New List(Of String)
        For Each row As DataRow In dt.Rows
            If Not String.IsNullOrEmpty(row("Type of Meter")) Then
                If Not list.Contains(row("Type of Meter")) Then
                    list.Add(row("Type of Meter"))
                End If
            End If
        Next
        Return list
    End Function

    Public Function GetCostOnlyRateList(ByVal dt As DataTable, ByVal dialList As List(Of String))

        Dim list As New List(Of String)

        For Each row As DataRow In dt.Rows
            If Not String.IsNullOrEmpty(row("Type of Charge")) Then
                If Not list.Contains(row("Type of Charge")) And Not dialList.Contains(row("Type of Charge")) Then
                    list.Add(row("Type of Charge"))
                End If
            End If
        Next
        Return list
    End Function

    Public Function GetInvoiceMpanList(ByRef dt As DataTable) As List(Of String)
        Dim list As New List(Of String)
        For Each row As DataRow In dt.Rows
            If Not String.IsNullOrEmpty(row("MPAN")) Then
                If Not String.IsNullOrEmpty(row("Invoice Number") & ";" & row("MPAN")) Then
                    If Not list.Contains(row("Invoice Number") & ";" & row("MPAN")) Then
                        list.Add(row("Invoice Number") & ";" & row("MPAN"))
                    End If
                End If
            End If
        Next
        Return list
    End Function

    Public Sub PopulateRateEntries(ByVal dt As DataTable, ByRef invoiceTable As DataTable, ByVal charge As String)

        Dim meterRows() As DataRow = dt.Select("[Invoice Number] = '" & invoice & "' AND [Type of Meter] = '" & charge & "'")

        If meterRows.Count > 0 Then

            ' Dim invoiceNumber, serial, mpanLocal, startDate, endDate, startRead, startReadE, endRead, endReadE As String
            'Get Invoice Number, Meter Serial, Mpan, Start Date and End date

            Dim mpanInvoice() As DataRow = dt.Select("MPAN = '" & mpan & "' AND [Invoice Number] = '" & invoice & "'")


            'Meter Details


            'Consumption and Cost details
            Dim costRows() As DataRow = dt.Select("[Invoice Number] = '" & invoice & "' AND [Type of Charge] = '" & charge & "'")

            If costRows.Count > 0 Then

                For i As Integer = 0 To costRows.Count - 1

                    Dim invoiceTableRow As DataRow = invoiceTable.NewRow

                    invoiceTableRow("Invoice Number") = mpanInvoice(0)("Invoice Number")
                    invoiceTableRow("Meter Serial") = mpanInvoice(0)("Meter Serial Number")
                    invoiceTableRow("MPAN") = Right(mpanInvoice(0)("MPAN").ToString, 13)
                    invoiceTableRow("Start Date") = mpanInvoice(0)("Start Date")
                    invoiceTableRow("End Date") = mpanInvoice(0)("End Date")
                    invoiceTableRow("TaxPointDate") = mpanInvoice(0)("Invoice Date")
                    invoiceTableRow("IsLastDayApportion") = "No"

                    invoiceTableRow("Rate") = charge

                    'Meter Details
                    invoiceTableRow("Start Read Estimated") = meterRows(0)("Previous Reading Type")
                    invoiceTableRow("End Read Estimated") = meterRows(0)("This Reading Type")
                    invoiceTableRow("Start Read") = meterRows(0)("Previous Reading")
                    invoiceTableRow("End Read") = meterRows(0)("This Reading")


                    invoiceTableRow("Consumption") = costRows(i)("Units Billed")
                    invoiceTableRow("Force kWh") = "Yes"
                    invoiceTableRow("Net") = costRows(i)("Total Charge")
                    invoiceTableRow("Gross") = costRows(i)("Total Charge")

                    invoiceTableRow("Cost Only") = "No"
                    invoiceTableRow("Description") = charge
                    invoiceTable.Rows.Add(invoiceTableRow)

                Next



            End If



        End If



    End Sub

    Public Sub PopulateVAT(ByRef dt As DataTable, ByRef invoiceTable As DataTable)
        Dim mpanInvoice() As DataRow = dt.Select("MPAN = '" & mpan & "' AND [Invoice Number] = '" & invoice & "'")
        If dt.Columns.Contains("VAT") Then
            If Not String.IsNullOrEmpty(mpanInvoice(0)("VAT")) Then
                If mpanInvoice(0)("VAT") <> 0 Then

                    Dim invoiceTableRow As DataRow = invoiceTable.NewRow

                    invoiceTableRow("Invoice Number") = mpanInvoice(0)("Invoice Number")
                    invoiceTableRow("MPAN") = Right(mpanInvoice(0)("MPAN").ToString, 13)
                    invoiceTableRow("Meter Serial") = mpanInvoice(0)("Meter Serial Number")
                    invoiceTableRow("Start Date") = mpanInvoice(0)("Start Date")
                    invoiceTableRow("End Date") = mpanInvoice(0)("End Date")
                    invoiceTableRow("Force kWh") = "No"
                    invoiceTableRow("TaxPointDate") = mpanInvoice(0)("Invoice Date")
                    invoiceTableRow("IsLastDayApportion") = "No"

                    invoiceTableRow("Vat") = mpanInvoice(0)("VAT")
                    invoiceTableRow("Gross") = mpanInvoice(0)("VAT")
                    invoiceTableRow("Cost Only") = "Yes"
                    invoiceTableRow("Description") = "VAT"
                    invoiceTable.Rows.Add(invoiceTableRow)


                End If

            End If
        End If
    End Sub

    Public Sub PopulateNonConsumptionCharge(ByVal dt As DataTable, ByRef invoiceTable As DataTable, ByVal charge As String)

        Dim costRows() As DataRow = dt.Select("[Invoice Number] = '" & invoice & "' AND [Type of Charge] = '" & charge & "'")

        If costRows.Count > 0 Then

            For i As Integer = 0 To costRows.Count - 1

                Dim invoiceTableRow As DataRow = invoiceTable.NewRow

                'Get Invoice Number, Meter Serial, Mpan, Start Date and End date

                Dim mpanInvoice() As DataRow = dt.Select("MPAN = '" & mpan & "' AND [Invoice Number] = '" & invoice & "'")

                invoiceTableRow("Invoice Number") = mpanInvoice(0)("Invoice Number")
                invoiceTableRow("Meter Serial") = mpanInvoice(0)("Meter Serial Number")
                invoiceTableRow("MPAN") = Right(mpanInvoice(0)("MPAN").ToString, 13)
                invoiceTableRow("Start Date") = mpanInvoice(0)("Start Date")
                invoiceTableRow("End Date") = mpanInvoice(0)("End Date")
                invoiceTableRow("TaxPointDate") = mpanInvoice(0)("Invoice Date")
                invoiceTableRow("IsLastDayApportion") = "No"

                invoiceTableRow("Force kWh") = "No"
                invoiceTableRow("Net") = costRows(i)("Total Charge")
                invoiceTableRow("Gross") = costRows(i)("Total Charge")
                invoiceTableRow("Cost Only") = "Yes"
                invoiceTableRow("Description") = charge

                invoiceTable.Rows.Add(invoiceTableRow)
            Next
        End If
    End Sub
    Public Sub GetTransformedHeaders()
        RenameColumns(_returnTable)
    End Sub
    Private Sub RenameColumns(returnTable As DataTable)
        returnTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        returnTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        returnTable.Columns("Start Date").ColumnName = "StartDate"
        returnTable.Columns("End Date").ColumnName = "EndDate"
        returnTable.Columns("Start Read").ColumnName = "StartRead"
        returnTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        returnTable.Columns("End Read").ColumnName = "EndRead"
        returnTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        returnTable.Columns("Force kWh").ColumnName = "ForceKwh"
        returnTable.Columns("Rate").ColumnName = "Tariff"
        returnTable.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub

End Class
