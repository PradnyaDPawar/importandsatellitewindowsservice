﻿Public Class TeamSigmaHalfhourSchema
    Public Shared _meterSerial As String
    Public Shared Property MeterSerial() As String

        Get
            Return _meterSerial
        End Get
        Set(ByVal value As String)
            _meterSerial = value
        End Set
    End Property

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()

        AddColumns(dt)
        PopulateUnits(dt)
        PopulateColumns(dt, hhDataTable)
        dt = hhDataTable
    End Sub

    Public Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef hhDataTable As DataTable)

        Dim dateholder As DateTime
        Dim success As Boolean = False
        For Each row As DataRow In dt.Rows

            If IsDate(row("Date")) Then
                dateholder = row("Date") + " " + row("StartTime")
                For Each hhrow As DataRow In hhDataTable.Rows
                    If hhrow("Date(dd/mm/yyyy)") = dateholder.Date And hhrow("MeterSerial") = row("Meter") Then
                        hhrow(dateholder.ToString("HH:mm")) = row("PeriodValue")
                        success = True
                    End If
                Next

                If Not success Then
                    Dim newRow As DataRow = hhDataTable.NewRow
                    newRow("Date(dd/mm/yyyy)") = dateholder.Date
                    newRow(dateholder.ToString("HH:mm")) = row("PeriodValue")
                    newRow("ForcekWh") = row("ForcekWh")
                    newRow("MeterSerial") = row("Meter")
                    hhDataTable.Rows.Add(newRow)
                End If
            End If
            success = False
        Next


    End Sub

    

    Public Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")

    End Sub


    Public Shared Sub PopulateUnits(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows

            If row("Unit").ToString.ToLower = "kwh" Then

                row("ForcekWh") = "Yes"
            Else
                row("ForcekWh") = "No"

            End If

        Next

    End Sub


End Class
