﻿Public Class UnitedUtilitiesWaterInvoiceSchema

    Public Shared Sub ToDEFormat(ByRef dt As DataTable)

        Dim invoiceDt As DataTable = DeFormatTableTemplates.CreateInvoiceTable
        PopulateInvoiceDt(dt, invoiceDt)

    End Sub

    Private Shared Sub PopulateInvoiceDt(ByRef dt As DataTable, invoicedt As DataTable)


        Try

            For Each drow As DataRow In dt.Rows

                Dim invoiceTableRow As DataRow = invoicedt.NewRow

                invoiceTableRow("Invoice Number") = drow("Invoice Number")
                invoiceTableRow("Meter Serial") = drow("Account Number")
                invoiceTableRow("MPAN") = ""
                invoiceTableRow("Description") = drow("Charge Type")
                invoiceTableRow("Start Date") = drow("Charge From Date")

                'if start date and end date is same,increment end date by 1 day

                If invoiceTableRow("Start Date") = drow("Charge To Date") Then
                    invoiceTableRow("End Date") = CDate(drow("Charge To Date")).AddDays(1).ToString("dd-MMM-yy")
                Else
                    invoiceTableRow("End Date") = drow("Charge To Date")
                End If

                invoiceTableRow("Start Read") = drow("Prev Read Value")
                invoiceTableRow("Start Read Estimated") = GetEstimatedValue(Convert.ToString(drow("Prev Read Type")))
                invoiceTableRow("End Read") = drow("Current Read Value")
                invoiceTableRow("End Read Estimated") = GetEstimatedValue(Convert.ToString(drow("Current Read Type")))
                invoiceTableRow("Consumption") = drow("Current Consumption")
                invoiceTableRow("Force kWh") = "No"
                invoiceTableRow("Gross") = drow("Charge Value")
                invoiceTableRow("TaxPointDate") = ""
                invoiceTableRow("IsLastDayApportion") = "No"
                PopulateCostOnlyAndTariffColumn(invoiceTableRow)
                PopulateNetVatColumn(invoiceTableRow)

                invoicedt.Rows.Add(invoiceTableRow)
            Next

            RenameColumns(invoicedt)
            dt = invoicedt

        Catch ex As Exception

        End Try

    End Sub

    Private Shared Function GetCostOnlyNoList() As List(Of String)
        Dim costOnlyNoList As New List(Of String)
        
        costOnlyNoList.Add("Water volume charge".ToLower.Trim)
        costOnlyNoList.Add("Sewerage volume charge".ToLower.Trim)

        Return costOnlyNoList

    End Function

    Private Shared Sub PopulateCostOnlyAndTariffColumn(ByRef invoiceTableRow As DataRow)

        Dim costOnlyNoList As List(Of String) = GetCostOnlyNoList() ' List of cost only yes Charges

        If costOnlyNoList.Contains(Convert.ToString(invoiceTableRow("Description")).ToLower.Trim) Then
            invoiceTableRow("Cost Only") = "No"
            invoiceTableRow("Rate") = Convert.ToString(invoiceTableRow("Description"))
        Else
            invoiceTableRow("Cost Only") = "Yes"
            invoiceTableRow("Rate") = ""
        End If

    End Sub

    Private Shared Sub PopulateNetVatColumn(ByRef invoiceTableRow As DataRow)

        If Convert.ToString(invoiceTableRow("Description")).ToLower.Trim = "vat" Then
            invoiceTableRow("Vat") = invoiceTableRow("Gross")
            invoiceTableRow("Net") = 0
        Else
            invoiceTableRow("Net") = invoiceTableRow("Gross")
            invoiceTableRow("Vat") = 0
        End If


    End Sub

    Private Shared Function GetEstimatedValue(strValue As String) As String

        If strValue.ToLower.Trim = "routine" Or strValue.ToLower.Trim = "initial" Then
            Return "Actual"
        ElseIf strValue.ToLower.Trim.Contains("estimate") Then
            Return "Estimated"
        Else
            Return ""
        End If


    End Function

    Private Shared Sub RenameColumns(InvoiceDt As DataTable)

        InvoiceDt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        InvoiceDt.Columns("Meter Serial").ColumnName = "MeterSerial"
        InvoiceDt.Columns("Rate").ColumnName = "Tariff"
        InvoiceDt.Columns("Start Date").ColumnName = "StartDate"
        InvoiceDt.Columns("End Date").ColumnName = "EndDate"
        InvoiceDt.Columns("Start Read").ColumnName = "StartRead"
        InvoiceDt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        InvoiceDt.Columns("End Read").ColumnName = "EndRead"
        InvoiceDt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        InvoiceDt.Columns("Force kWh").ColumnName = "ForcekWh"
        InvoiceDt.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub

End Class
