﻿Public Class Bglobal2HalfHourSchema

    Public Shared Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)

        headers.Add("MeterSerial")
        headers.Add("Date(dd/MM/yyyy)")
        headers.Add("Hour")
        headers.Add("Consumption")

        Return headers
    End Function

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)
        AddColumns(dt)
        RenameColumnRowValues(dt)
        PopulateColumns(dt)
    End Sub

    Private Shared Sub AddColumns(ByRef dt As DataTable)
        Dim headers As List(Of String) = GetHeaders()
        For i As Integer = 0 To headers.Count - 1
            dt.Columns(i).ColumnName = headers(i)
        Next
    End Sub
    Private Shared Sub RenameColumnRowValues(ByRef dt As DataTable)
        For Each row As DataRow In dt.Rows
            row("Hour") = DateTime.Today.AddMinutes((Integer.Parse(row("Hour").ToString()) - 1) * 30).ToString("HH:mm")
            'row("Hour") = DateTime.Today.AddMinutes((Integer.Parse(row("Hour").ToString()) - 1) * 30).AddMinutes(30).ToString("HH:mm")
        Next
    End Sub

    Private Shared Sub PopulateColumns(ByRef csvDataTable As DataTable)
        Dim halfHourDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable
        Dim halfHourRow As DataRow = Nothing
        Dim map As New Dictionary(Of String, DataRow)
        For Each csvRow As DataRow In csvDataTable.Rows
            If Not String.IsNullOrWhiteSpace(csvRow("MeterSerial")) And Not String.IsNullOrWhiteSpace(csvRow("Date(dd/MM/yyyy)")) Then
                Dim dateHolder As DateTime = CDate(csvRow("Date(dd/MM/yyyy)"))
                Dim key As String = csvRow("MeterSerial") + dateHolder.ToString("dd/MM/yyyy")

                If map.ContainsKey(key) Then
                    halfHourRow = map(key)
                    map.Remove(key)
                    PopulateConsumption(csvRow, halfHourDataTable, halfHourRow)
                Else
                    halfHourRow = halfHourDataTable.NewRow
                    halfHourRow("MPAN") = String.Empty
                    halfHourRow("MeterSerial") = csvRow("MeterSerial")
                    halfHourRow("ForcekWh") = "No"
                    halfHourRow("Date(dd/MM/yyyy)") = dateHolder.ToString("dd/MM/yyyy")
                    PopulateConsumption(csvRow, halfHourDataTable, halfHourRow)
                    halfHourDataTable.Rows.Add(halfHourRow)
                End If

                map.Add(key, halfHourRow)
            End If
        Next
        csvDataTable = halfHourDataTable
    End Sub

    Private Shared Sub PopulateConsumption(ByVal csvRow As DataRow, ByRef halfHourDataTable As DataTable, ByRef halfHourRow As DataRow)
        For Each column As DataColumn In halfHourDataTable.Columns
            If (column.ColumnName.Equals(csvRow("Hour").ToString())) Then
                halfHourRow(column.ColumnName) = csvRow("Consumption")
                Exit For
            End If
        Next
    End Sub
End Class
