﻿Public Class PortalkWhRealtimeSchema
    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        Dim MeterName As String = dt.Rows(0)(1).ToString()
        resetColumnHeader(dt)
        Dim realTimeDt As DataTable
        realTimeDt = DeFormatTableTemplates.CreateRealTimeTable()
        PopulateColumns(dt, realTimeDt, MeterName)
        Return dt
    End Function

    Public Shared Sub resetColumnHeader(ByRef dt As DataTable)

        dt.Columns("0").ColumnName = "DateTime"
        dt.Columns("1").ColumnName = "CountUnits"
        dt.Rows.RemoveAt(0)
    End Sub
    Private Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable, ByRef metername As String)


        Dim DateTimeNew As String = ""

        For Each row As DataRow In dt.Rows
            DateTimeNew = row("DateTime").ToString().Remove(row("DateTime").ToString().Length - 3).ToString()

            Dim realTimeDtRow As DataRow = realTimeDt.NewRow

            realTimeDtRow("MeterName") = metername
            realTimeDtRow("DateTime") = CDate(DateTimeNew).ToString("dd/MM/yyyy H:mm")
            If (row("CountUnits").ToString = "") Then
                realTimeDtRow("CountUnits") = ""
            ElseIf (CInt(row("CountUnits")) = 0) Then
                realTimeDtRow("CountUnits") = 0
            Else
                realTimeDtRow("CountUnits") = row("CountUnits")
            End If
            realTimeDt.Rows.Add(realTimeDtRow)
        Next

        dt = realTimeDt

    End Sub
End Class
