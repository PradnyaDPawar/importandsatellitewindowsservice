﻿Public Class EonInvoice2Schema
    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        DataTableTools.RemoveBlankRows(dt)
        Dim standardInvoiceTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        For Each currentDataRow As DataRow In dt.Rows
            standardInvoiceTable.Rows.Add(GetRow1(standardInvoiceTable, currentDataRow))
            standardInvoiceTable.Rows.Add(GetRow2(standardInvoiceTable, currentDataRow))
            If Not "".Equals(currentDataRow("Climate Levy")) Then
                standardInvoiceTable.Rows.Add(GetRow3(standardInvoiceTable, currentDataRow))
            End If
            standardInvoiceTable.Rows.Add(GetRow4(standardInvoiceTable, currentDataRow))
        Next
        RenameColumns(standardInvoiceTable)
        dt = standardInvoiceTable
        Return dt
    End Function
    Private Shared Sub PopulateStandardData(ByRef drInvoice As DataRow, ByRef drSource As DataRow)
        drInvoice("Invoice Number") = drSource("Invoice Number")
        drInvoice("TaxPointDate") = drSource("Invoice Date")
        drInvoice("MPAN") = drSource("Meter Identifier")
        drInvoice("Meter Serial") = drSource("Meter Reference")
        drInvoice("Start Date") = drSource("From date")
        drInvoice("End Date") = drSource("To Date")
        drInvoice("Force kWh") = "Yes"
        drInvoice("IsLastDayApportion") = "No"
    End Sub
    Private Shared Function GetRow1(ByRef invoiceDt As DataTable, ByRef drSource As DataRow) As DataRow 'Unit Cost
        Dim unitCostRow As DataRow = invoiceDt.NewRow()
        PopulateStandardData(unitCostRow, drSource)
        unitCostRow("Rate") = "Unit Cost"
        unitCostRow("Description") = "Unit Cost"
        unitCostRow("Start Read") = drSource("Previous")
        unitCostRow("Start Read Estimated") = drSource("PQ")
        unitCostRow("End Read") = drSource("Current")
        unitCostRow("End Read Estimated") = drSource("CQ")
        unitCostRow("Consumption") = drSource("Kwh")
        unitCostRow("Net") = drSource("Cost")
        unitCostRow("Gross") = drSource("Cost")
        unitCostRow("Cost Only") = "No"
        Return unitCostRow
    End Function
    Private Shared Function GetRow2(ByRef invoiceDt As DataTable, ByRef drSource As DataRow) As DataRow 'Standing Charge
        Dim unitCostRow As DataRow = invoiceDt.NewRow()
        PopulateStandardData(unitCostRow, drSource)
        unitCostRow("Rate") = "Standing Charge"
        unitCostRow("Description") = "Standing Charge"
        unitCostRow("Net") = drSource("Standing Charge")
        unitCostRow("Gross") = drSource("Standing Charge")
        unitCostRow("Cost Only") = "Yes"
        Return unitCostRow
    End Function
    Private Shared Function GetRow3(ByRef invoiceDt As DataTable, ByRef drSource As DataRow) As DataRow 'CCL
        Dim unitCostRow As DataRow = invoiceDt.NewRow()
        PopulateStandardData(unitCostRow, drSource)
        unitCostRow("Rate") = "CCL"
        unitCostRow("Description") = "CCL"
        unitCostRow("Net") = drSource("Climate Levy")
        unitCostRow("Gross") = drSource("Climate Levy")
        unitCostRow("Cost Only") = "Yes"
        Return unitCostRow
    End Function
    Private Shared Function GetRow4(ByRef invoiceDt As DataTable, ByRef drSource As DataRow) As DataRow 'Vat
        Dim unitCostRow As DataRow = invoiceDt.NewRow()
        PopulateStandardData(unitCostRow, drSource)
        unitCostRow("Rate") = "VAT"
        unitCostRow("Description") = "VAT"
        unitCostRow("Vat") = drSource("Vat")
        unitCostRow("Gross") = drSource("Vat")
        unitCostRow("Cost Only") = "Yes"
        Return unitCostRow
    End Function
    Private Shared Sub RenameColumns(ByRef invoiceTable As DataTable)
        invoiceTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceTable.Columns("Rate").ColumnName = "Tariff"
        invoiceTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceTable.Columns("End Date").ColumnName = "EndDate"
        invoiceTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceTable.Columns("End Read").ColumnName = "EndRead"
        invoiceTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceTable.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub
End Class
