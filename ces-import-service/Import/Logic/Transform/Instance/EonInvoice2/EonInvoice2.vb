﻿Public Class EonInvoice2
    Implements ITransformDelegate

    Private _returnTable As DataTable
    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(value As DataTable)
            _returnTable = value
        End Set
    End Property
    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Dim csv As DelimitedFile = New DelimitedFile(fullPath, 20)
        csv.HasHeader = True
        csv.TrimWhiteSpace = True
        csv.FieldsAreQuoted = True
        csv.Load(_returnTable)
    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        EonInvoice2Schema.ToDeFormat(_returnTable)
        For Each row As DataRow In ReturnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
