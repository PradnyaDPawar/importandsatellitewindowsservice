﻿Partial Public Class BGElectricInvoice

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("Invoice Type")
        headers.Add("Invoice No")
        headers.Add("Revision")
        headers.Add("Line Description")
        headers.Add("Meter Point ID")
        headers.Add("Meter No")
        headers.Add("Start Read Date")
        headers.Add("Start Read Type")
        headers.Add("Start Read")
        headers.Add("End Read Date")
        headers.Add("End Read")
        headers.Add("End Read Type")
        headers.Add("Billable Units")
        headers.Add("Price")
        headers.Add("Unit Type")
        headers.Add("VAT Rate")
        headers.Add("Line Value")
        '  headers.Add("Line VAT")
        headers.Add("Site No")
        headers.Add("Site Name")
        'headers.Add("TaxPointDate")
        'headers.Add("IsLastDayApportion")

        Return headers
    End Function

    Public Sub ColumnValidate()
        Dim rawHeaders As List(Of String) = GetHeaders()

        For Each header As String In rawHeaders
            If Not _returnTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next
    End Sub

    Public Sub GetTransformedHeaders()
        CreateColumns(_returnTable)
        RenameColumns(_returnTable)
        PopulateColumns(_returnTable)

    End Sub

    Private Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("Invoice No").ColumnName = "InvoiceNumber"
        dt.Columns("Meter No").ColumnName = "MeterSerial"
        dt.Columns("Meter Point ID").ColumnName = "MPAN"
        'dt.Columns("Line Description").ColumnName = "Rate"
        dt.Columns("Line Description").ColumnName = "Tariff"
        dt.Columns("Start Read").ColumnName = "StartRead"
        dt.Columns("End Read").ColumnName = "EndRead"
        dt.Columns("Start Read Date").ColumnName = "StartDate"
        dt.Columns("End Read Date").ColumnName = "EndDate"
        dt.Columns("Start Read Type").ColumnName = "StartReadEstimated"
        dt.Columns("End Read Type").ColumnName = "EndReadEstimated"
        dt.Columns("Billable Units").ColumnName = "Consumption"
        dt.Columns("Line VAT").ColumnName = "Vat"
        dt.Columns("Line Value").ColumnName = "Net"

    End Sub

    Private Shared Sub CreateColumns(ByRef dt As DataTable)
        If Not dt.Columns.Contains("Line VAT") Then
            dt.Columns.Add("Line VAT")
            PopulateLineVat(dt)
        End If
        If Not dt.Columns.Contains("TaxPointDate") Then
            dt.Columns.Add("TaxPointDate")
        End If
        If Not dt.Columns.Contains("IsLastDayApportion") Then
            dt.Columns.Add("IsLastDayApportion")
        End If
        dt.Columns.Add("Gross")
        dt.Columns.Add("CostOnly")
        dt.Columns.Add("Description")
        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("IsTransmissionTariff")
        dt.Columns.Add("InvoiceRate")
        dt.Columns.Add("CostUnit")
    End Sub

    Public Shared Sub PopulateColumns(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows

            PopulateCostOnly(row)
            PopulateGross(row)
            PopulateReadEstimation(row)
            PopulateForceKwh(row)
            PopulateIsLastDayApportion(row)
            If String.IsNullOrEmpty(row("MPAN")) Then
                PopulateBlankMpan(row, dt)
            End If

        Next

    End Sub

    Private Shared Sub PopulateLineVat(ByRef dt As DataTable)

        Dim vatRate, net As Double

        For Each row As DataRow In dt.Rows
            vatRate = row("VAT Rate")
            net = row("Line Value")
            row("Line VAT") = (net * vatRate) / 100
        Next

    End Sub

    Private Shared Sub PopulateGross(ByVal row As DataRow)
        Dim net, vat As Double


        row("Vat") = row("Vat").ToString.Replace("£", "")
        vat = row("Vat")

        row("Net") = row("Net").ToString.Replace("£", "")
        net = row("Net")

        row("Gross") = vat + net



    End Sub

    Private Shared Sub PopulateCostOnly(ByVal row As DataRow)

        Dim rates As New List(Of String)

        If String.IsNullOrEmpty(row("MeterSerial")) And String.IsNullOrEmpty(row("MPAN")) Then

            row("CostOnly") = "Yes"
            'if row is cost only, rate should be blank
            row("Description") = row("Tariff")
            row("Tariff") = ""
            row("Consumption") = ""
        Else

            row("CostOnly") = "No"
            row("Description") = row("Tariff")
        End If

    End Sub

    Private Shared Sub PopulateReadEstimation(ByVal row As DataRow)
        Dim ListActual As New List(Of Char)(New Char() {"a", "c", "f", "i", "m", "p", "r", "s", "b"})
        Dim ListEstimated As New List(Of Char)(New Char() {"d", "e"})

        ' Start Read Estimated
        If ListActual.Contains(row("StartReadEstimated").ToString.ToLower) Then
            row("StartReadEstimated") = "A"
        ElseIf ListEstimated.Contains(row("StartReadEstimated").ToString.ToLower) Then
            row("StartReadEstimated") = "E"
        End If

        ' End Read Estimated
        If ListActual.Contains(row("EndReadEstimated").ToString.ToLower) Then
            row("EndReadEstimated") = "A"
        ElseIf ListEstimated.Contains(row("EndReadEstimated").ToString.ToLower) Then
            row("EndReadEstimated") = "E"
        End If

    End Sub

    Private Shared Sub PopulateForceKwh(ByVal row As DataRow)
        row("ForcekWh") = "Yes"
    End Sub

    Private Shared Sub PopulateBlankMpan(ByVal row As DataRow, ByRef dt As DataTable)
        Dim UniqueFlag As Boolean = True
        Dim resultrows() As DataRow
        Dim mpan As String

        Dim inv As String = row("InvoiceNumber")
        resultrows = row.Table.Select("[InvoiceNumber] = '" & row("InvoiceNumber") & "' AND MPAN <> ''")
        If resultrows.Count > 0 Then
            mpan = resultrows(0)("MPAN")
            For Each resultrow As DataRow In resultrows

                If Not String.Equals(mpan, resultrow("MPAN")) Then
                    UniqueFlag = False
                End If
                mpan = resultrow("MPAN")

            Next

            If UniqueFlag Then
                row("MPAN") = mpan
            End If
        End If

    End Sub

    Private Shared Sub PopulateIsLastDayApportion(row As DataRow)
        row("IsLastDayApportion") = "No"
    End Sub

End Class
