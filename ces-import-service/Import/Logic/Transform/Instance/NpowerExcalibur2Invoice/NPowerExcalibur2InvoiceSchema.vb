﻿Imports System.Globalization

Public Class NPowerExcalibur2InvoiceSchema

    Private _InvoiceNumber As String
    Private _Mpan As String
    Private _taxPointDate As String
    Private _startDate As String
    Private _endDate As String

    Private _invoiceFile As DataTable
    Private _deInvoiceTable As DataTable

    Public Sub New()

        _deInvoiceTable = NPowerExcaliburInvoiceSchemaHelper.CreateInvoiceTable()
        _deInvoiceTable.TableName = "DeInvoiceTable"

    End Sub

    Public Property InvoiceNumber() As String
        Get
            Return _InvoiceNumber
        End Get
        Set(ByVal value As String)
            _InvoiceNumber = value
        End Set
    End Property

    Public Property MPAN() As String
        Get
            Return _Mpan
        End Get
        Set(ByVal value As String)
            _Mpan = value
        End Set
    End Property

    Public Property TaxPointDate() As String
        Get
            Return _taxPointDate
        End Get
        Set(ByVal value As String)
            _taxPointDate = value
        End Set
    End Property

    Public Property StartDate() As String
        Get
            Return _startDate
        End Get
        Set(ByVal value As String)
            _startDate = value
        End Set
    End Property

    Public Property EndDate() As String
        Get
            Return _endDate
        End Get
        Set(ByVal value As String)
            _endDate = value
        End Set
    End Property

    Public Property InvoiceFile() As DataTable
        Get
            Return _invoiceFile
        End Get
        Set(ByVal value As DataTable)
            _invoiceFile = value
        End Set
    End Property

    Public Property DeInvoiceTable() As DataTable
        Get
            Return _deInvoiceTable
        End Get
        Set(ByVal value As DataTable)
            _deInvoiceTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dtInvoice As DataTable)

        InvoiceFile = dtInvoice
        InvoiceFile.TableName = "InvoiceFile"

        For Each row As DataRow In InvoiceFile.Rows

            If Not String.IsNullOrEmpty(row("SITE INVOICE").ToString().Trim()) Then

                ' Save current row invoice info
                SaveInvoiceInfo(row)

                ' Add Supply band charges
                AddSupplyBandCharges(row)

                ' Add Total MD Charges
                AddTotalMDCharges(row)

                ' Add Total Available capacity Charges
                AddTotalAvailableCapacityCharge(row)

                ' Add Total Feed In Tariff Charges
                AddTotalFeedInTariff(row)

                ' Add Total Renewable Obligation Units
                AddTotalRenewableObligationCharges(row)

                ' Add Admin Cost
                AddAdminCharges(row)

                ' Add Settlement Cost
                AddSettlementCharges(row)

                ' Add Manual Adjustments
                AddManualAdjustmentsCharges(row)

                ' Add Renewable Charges
                AddRenewableCharges(row)

                ' Add Climate Change Equivalent Charge
                AddClimateChangeEquivalentCharge(row)

                ' Add CCL Charge
                AddCCLCharge(row)

                ' Add VAT @ 5.0%
                AddFivePercentVat(row)

                ' Add VAT @ 20.0%
                AddTwentyPercentVat(row)

            End If

        Next

        dtInvoice = DeInvoiceTable

    End Sub

    Private Sub SaveInvoiceInfo(ByRef currentInvoiceRow As DataRow)

        InvoiceNumber = currentInvoiceRow("SITE INVOICE")
        MPAN = currentInvoiceRow("MPAN")
        TaxPointDate = currentInvoiceRow("INVOICE DATE")

        ' Saving Start Date and End Date
        If Not String.IsNullOrEmpty(currentInvoiceRow("SUPPLY PERIOD").ToString().Trim()) Then

            Dim supplyPeriod As Date = Date.MinValue
            Date.TryParseExact(currentInvoiceRow("SUPPLY PERIOD").ToString().Trim(), "yyyyMM", CultureInfo.CurrentCulture, DateTimeStyles.None, supplyPeriod)

            StartDate = supplyPeriod.ToString("dd-MM-yyyy")
            EndDate = supplyPeriod.AddMonths(1).AddDays(-1).ToString("dd-MM-yyyy")

        Else

            StartDate = TaxPointDate
            EndDate = Date.Parse(TaxPointDate).AddDays(1).ToString("dd/MM/yyyy").Replace("-", "/")

        End If

    End Sub

    Private Sub AddSupplyBandCharges(ByRef row As DataRow)

        For Each column As DataColumn In row.Table.Columns

            If column.ColumnName.Contains("BILLED UNITS") Then

                Dim supplyBand As String = column.ColumnName.Split(",")(0)

                If row.Table.Columns.Contains(column.ColumnName) And
                    row.Table.Columns.Contains(supplyBand & ",CHARGE") Then

                    If Not String.IsNullOrEmpty(row(column).ToString().Trim()) And
                        Not String.IsNullOrEmpty(row(supplyBand & ",CHARGE").ToString().Trim()) Then

                        Dim charge As Double = 0.0
                        Dim unit As Double = 0.0

                        If Double.TryParse(row(column).ToString().Trim(), unit) And
                            Double.TryParse(row(supplyBand & ",CHARGE").ToString().Trim().Replace("£", ""), charge) Then

                            If supplyBand = "Supply Band: 01" Then
                                AddDeInvoiceRow(unit.ToString(), charge.ToString(), "Day Rate 01", False, True)

                            ElseIf supplyBand = "Supply Band: 02" Then
                                AddDeInvoiceRow(unit.ToString(), charge.ToString(), "Evenings and Weekends Rate 02", False, True)

                            ElseIf supplyBand = "Supply Band: 03" Then
                                AddDeInvoiceRow(unit.ToString(), charge.ToString(), "All Day Rate 03", False, True)

                            ElseIf supplyBand = "Supply Band: 04" Then
                                AddDeInvoiceRow(unit.ToString(), charge.ToString(), "Night Rate 04", False, True)

                            ElseIf supplyBand = "Supply Band: 05" Then
                                AddDeInvoiceRow(unit.ToString(), charge.ToString(), "Day Rate 05", False, True)

                            End If

                        End If

                    End If

                End If

            End If

        Next

    End Sub

    Private Sub AddTotalMDCharges(ByRef row As DataRow)

        If row.Table.Columns.Contains("TOTAL MD UNITS") And
            row.Table.Columns.Contains("TOTAL MD CHARGE") Then

            If Not String.IsNullOrEmpty(row("TOTAL MD UNITS").ToString().Trim()) And
                Not String.IsNullOrEmpty(row("TOTAL MD CHARGE").ToString().Trim()) Then

                Dim unit As Double = 0.0
                Dim charge As Double = 0.0

                If Double.TryParse(row("TOTAL MD UNITS").ToString().Trim(), unit) And
                    Double.TryParse(row("TOTAL MD CHARGE").ToString().Trim().Replace("£", ""), charge) Then

                    AddDeInvoiceRow(unit.ToString(), charge.ToString(), "Total MD Units", True, True)

                End If

            End If

        End If

    End Sub

    Private Sub AddTotalAvailableCapacityCharge(ByRef row As DataRow)

        If row.Table.Columns.Contains("TOTAL AVAILABLE CAPACITY UNITS") And
            row.Table.Columns.Contains("TOTAL AVAILABLE CAPACITY CHARGE") Then

            If Not String.IsNullOrEmpty(row("TOTAL AVAILABLE CAPACITY UNITS").ToString().Trim()) And
                Not String.IsNullOrEmpty(row("TOTAL AVAILABLE CAPACITY CHARGE").ToString().Trim()) Then

                Dim unit As Double = 0.0
                Dim charge As Double = 0.0

                If Double.TryParse(row("TOTAL AVAILABLE CAPACITY UNITS").ToString().Trim(), unit) And
                    Double.TryParse(row("TOTAL AVAILABLE CAPACITY CHARGE").ToString().Trim().Replace("£", ""), charge) Then

                    AddDeInvoiceRow(unit.ToString(), charge.ToString(), "Total Available Capacity Units", True, True)

                End If

            End If

        End If

    End Sub

    Private Sub AddTotalFeedInTariff(ByRef row As DataRow)

        If row.Table.Columns.Contains("TOTAL FEED IN TARIFF UNITS") And
            row.Table.Columns.Contains("TOTAL FEED IN TARIFF CHARGE") Then

            If Not String.IsNullOrEmpty(row("TOTAL FEED IN TARIFF UNITS").ToString().Trim()) And
                Not String.IsNullOrEmpty(row("TOTAL FEED IN TARIFF CHARGE").ToString().Trim()) Then

                Dim unit As Double = 0.0
                Dim charge As Double = 0.0

                If Double.TryParse(row("TOTAL FEED IN TARIFF UNITS").ToString().Trim(), unit) And
                    Double.TryParse(row("TOTAL FEED IN TARIFF CHARGE").ToString().Trim().Replace("£", ""), charge) Then

                    AddDeInvoiceRow(unit.ToString(), charge.ToString(), "Total Feed In Tariff Units", True, True)

                End If

            End If

        End If

    End Sub

    Private Sub AddTotalRenewableObligationCharges(ByRef row As DataRow)

        If row.Table.Columns.Contains("TOTAL RENEWABLE OBLIGATION UNITS") And
            row.Table.Columns.Contains("TOTAL RENEWABLE OBLIGATION CHARGE") Then

            If Not String.IsNullOrEmpty(row("TOTAL RENEWABLE OBLIGATION UNITS").ToString().Trim()) And
                Not String.IsNullOrEmpty(row("TOTAL RENEWABLE OBLIGATION CHARGE").ToString().Trim()) Then

                Dim unit As Double = 0.0
                Dim charge As Double = 0.0

                If Double.TryParse(row("TOTAL RENEWABLE OBLIGATION UNITS").ToString().Trim(), unit) And
                    Double.TryParse(row("TOTAL RENEWABLE OBLIGATION CHARGE").ToString().Trim().Replace("£", ""), charge) Then

                    AddDeInvoiceRow(unit.ToString(), charge.ToString(), "Total Renewable Obligation Units", True, True)

                End If

            End If

        End If

    End Sub

    Private Sub AddAdminCharges(ByRef row As DataRow)

        If row.Table.Columns.Contains("ADMIN") Then

            If Not String.IsNullOrEmpty(row("ADMIN").ToString().Trim()) Then

                Dim charge As Double = 0.0

                If Double.TryParse(row("ADMIN").ToString().Trim().Replace("£", ""), charge) Then

                    AddDeInvoiceRow(String.Empty, charge.ToString(), "Admin", True, True)

                End If

            End If

        End If

    End Sub

    Private Sub AddSettlementCharges(ByRef row As DataRow)

        If row.Table.Columns.Contains("SETTLEMENT") Then

            If Not String.IsNullOrEmpty(row("SETTLEMENT").ToString().Trim()) Then

                Dim charge As Double = 0.0

                If Double.TryParse(row("SETTLEMENT").ToString().Trim().Replace("£", ""), charge) Then

                    AddDeInvoiceRow(String.Empty, charge.ToString(), "Settlement", True, True)

                End If

            End If

        End If

    End Sub

    Private Sub AddManualAdjustmentsCharges(ByRef row As DataRow)

        If row.Table.Columns.Contains("MANUAL ADJUSTMENTS (£)") Then

            If Not String.IsNullOrEmpty(row("MANUAL ADJUSTMENTS (£)").ToString().Trim()) Then

                Dim charge As Double = 0.0

                If Double.TryParse(row("MANUAL ADJUSTMENTS (£)").ToString().Trim().Replace("£", ""), charge) Then

                    AddDeInvoiceRow(String.Empty, charge.ToString(), "Manual Adjustments (£)", True, True)

                End If

            End If

        End If

    End Sub

    Private Sub AddRenewableCharges(ByRef row As DataRow)

        If row.Table.Columns.Contains("Renewable Charge") Then

            If Not String.IsNullOrEmpty(row("Renewable Charge").ToString().Trim()) Then

                Dim charge As Double = 0.0

                If Double.TryParse(row("Renewable Charge").ToString().Trim().Replace("£", ""), charge) Then

                    AddDeInvoiceRow(String.Empty, charge.ToString(), "Renewable Charge", True, True)

                End If

            End If

        End If

    End Sub

    Private Sub AddClimateChangeEquivalentCharge(ByRef row As DataRow)

        If row.Table.Columns.Contains("CLIMATE CHANGE EQUIVALENT CHARGE") Then

            If Not String.IsNullOrEmpty(row("CLIMATE CHANGE EQUIVALENT CHARGE").ToString().Trim()) Then

                Dim charge As Double = 0.0

                If Double.TryParse(row("CLIMATE CHANGE EQUIVALENT CHARGE").ToString().Trim().Replace("£", ""), charge) Then

                    AddDeInvoiceRow(String.Empty, charge.ToString(), "Climate Change Equivalent Charge", True, True)

                End If

            End If

        End If

    End Sub

    Private Sub AddCCLCharge(ByRef row As DataRow)

        If row.Table.Columns.Contains("CCL CHARGE") Then

            If Not String.IsNullOrEmpty(row("CCL CHARGE").ToString().Trim()) Then

                Dim charge As Double = 0.0

                If Double.TryParse(row("CCL CHARGE").ToString().Trim().Replace("£", ""), charge) Then

                    AddDeInvoiceRow(String.Empty, charge.ToString(), "CCL Charge", True, True)

                End If

            End If

        End If

    End Sub

    Private Sub AddFivePercentVat(ByRef row As DataRow)

        If row.Table.Columns.Contains("VAT @ 5.0%") Then

            If Not String.IsNullOrEmpty(row("VAT @ 5.0%").ToString().Trim()) Then

                Dim charge As Double = 0.0

                If Double.TryParse(row("VAT @ 5.0%").ToString().Trim().Replace("£", ""), charge) Then

                    AddDeInvoiceRow(String.Empty, charge.ToString(), "VAT @ 5.0%", True, False)

                End If

            End If

        End If

    End Sub

    Private Sub AddTwentyPercentVat(row As DataRow)

        If row.Table.Columns.Contains("VAT @20.0%") Then

            If Not String.IsNullOrEmpty(row("VAT @20.0%").ToString().Trim()) Then

                Dim charge As Double = 0.0

                If Double.TryParse(row("VAT @20.0%").ToString().Trim().Replace("£", ""), charge) Then

                    AddDeInvoiceRow(String.Empty, charge.ToString(), "VAT @20.0%", True, False)

                End If

            End If

        End If

    End Sub

    Private Sub AddDeInvoiceRow(ByVal unit As String, ByVal charge As String, ByVal tariff As String,
                                ByVal isCostOnly As Boolean, ByVal isNetCharge As Boolean)

        Dim deInvoiceRow As DataRow = DeInvoiceTable.NewRow()

        deInvoiceRow(DeFormatInvoiceTableColumnsName.InvoiceNumber) = InvoiceNumber
        deInvoiceRow(DeFormatInvoiceTableColumnsName.MeterSerial) = ""
        deInvoiceRow(DeFormatInvoiceTableColumnsName.MPAN) = MPAN
        deInvoiceRow(DeFormatInvoiceTableColumnsName.Tariff) = tariff
        deInvoiceRow(DeFormatInvoiceTableColumnsName.TaxPointDate) = TaxPointDate
        deInvoiceRow(DeFormatInvoiceTableColumnsName.StartDate) = StartDate
        deInvoiceRow(DeFormatInvoiceTableColumnsName.EndDate) = EndDate

        deInvoiceRow(DeFormatInvoiceTableColumnsName.StartRead) = ""
        deInvoiceRow(DeFormatInvoiceTableColumnsName.EndRead) = ""
        deInvoiceRow(DeFormatInvoiceTableColumnsName.StartReadEstimated) = ""
        deInvoiceRow(DeFormatInvoiceTableColumnsName.StartReadEstimated) = ""

        deInvoiceRow(DeFormatInvoiceTableColumnsName.Consumption) = unit

        If Not isCostOnly Then

            deInvoiceRow(DeFormatInvoiceTableColumnsName.ForcekWh) = "Yes"
            deInvoiceRow(DeFormatInvoiceTableColumnsName.CostOnly) = "No"

        Else

            deInvoiceRow(DeFormatInvoiceTableColumnsName.ForcekWh) = "No"
            deInvoiceRow(DeFormatInvoiceTableColumnsName.CostOnly) = "Yes"

        End If

        If isNetCharge Then

            deInvoiceRow(DeFormatInvoiceTableColumnsName.Net) = charge
            deInvoiceRow(DeFormatInvoiceTableColumnsName.Vat) = ""

        Else

            deInvoiceRow(DeFormatInvoiceTableColumnsName.Net) = ""
            deInvoiceRow(DeFormatInvoiceTableColumnsName.Vat) = charge

        End If

        deInvoiceRow(DeFormatInvoiceTableColumnsName.Gross) = charge

        deInvoiceRow(DeFormatInvoiceTableColumnsName.Description) = tariff
        deInvoiceRow(DeFormatInvoiceTableColumnsName.IsLastDayApportion) = "No"

        DeInvoiceTable.Rows.Add(deInvoiceRow)

    End Sub

End Class