﻿Public Class NPowerExcalibur2Invoice
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTable
        End Get

        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel()

        excel.Load(fullPath)

        For Each xsheet As ExcelSheet In excel.Sheets
            If xsheet.Name.ToLower.Trim <> "front sheet" Then
                Dim dt As DataTable
                dt = xsheet.GetDataTable()
                PrepareDatatable(dt)

                If excel.Sheets.IndexOf(xsheet) = 1 Then
                    _returnTable = dt
                Else
                    _returnTable.Merge(dt)
                End If
            End If
        Next

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Dim schema As New NPowerExcalibur2InvoiceSchema
        schema.ToDeFormat(ReturnTable)

        For Each row As DataRow In schema.DeInvoiceTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next

    End Sub

    Private Sub PrepareDatatable(ByRef dtInvoice As DataTable)

        DataTableTools.RemoveBlankColumns(dtInvoice)

        PrefixMainHeaders(dtInvoice, 0)

        DataTableTools.RemoveTopRows(dtInvoice, 2)
        DataTableTools.SetHeaders(dtInvoice, 1)

        DataTableTools.RemoveBlankColumns(dtInvoice)
        DataTableTools.RemoveBlankRows(dtInvoice)

        FormatColumnHeaders(dtInvoice)

    End Sub

    Private Sub PrefixMainHeaders(ByRef dtInvoice As DataTable, ByVal MainHeaderRowIndex As Integer)

        For Each column As DataColumn In dtInvoice.Columns

            If Not String.IsNullOrEmpty(dtInvoice.Rows(MainHeaderRowIndex)(column).ToString.Trim()) Then

                Dim columnName As String = dtInvoice.Rows(MainHeaderRowIndex)(column).ToString.Trim()
                Dim mainHeaderName As String = dtInvoice.Rows(MainHeaderRowIndex + 2)(column).ToString().Trim()

                dtInvoice.Rows(MainHeaderRowIndex + 2)(column) = columnName + "," + mainHeaderName

            End If

        Next

    End Sub

    Private Sub FormatColumnHeaders(ByRef dtInvoice As DataTable)

        For Each column As DataColumn In dtInvoice.Columns
            column.ColumnName = column.ColumnName.Trim()

        Next

    End Sub

End Class