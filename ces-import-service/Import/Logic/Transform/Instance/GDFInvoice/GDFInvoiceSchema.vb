﻿Public Class GDFInvoiceSchema
    Private Shared isHHorNHH As Boolean = False
    Shared Sub ToDeFormat(ByRef returnTable As DataTable)

        FormatColumnHeaders(returnTable)
        'If AAHEDCC column is present means we need to assum Transpose is for HH format
        If returnTable.Columns.Contains("AAHEDCChargePrice") Then
            isHHorNHH = True
        Else
            isHHorNHH = False
        End If
        Dim invoiceDataTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        CreateInvoiceEntries(returnTable, invoiceDataTable)
        RenameColumns(invoiceDataTable)
        returnTable = invoiceDataTable
    End Sub

    Private Shared Function FormatColumnHeaders(ByRef dt As DataTable) As DataTable
        For Each column As DataColumn In dt.Columns
            'Remove blank Space in between column name
            column.ColumnName = column.ColumnName.Trim.Replace(" ", "")
            'Remove dot if present at the end of column name
            If (column.ColumnName.Substring(column.ColumnName.Length - 1) = ".") Then
                column.ColumnName = column.ColumnName.Trim.Replace(".", "")
            End If
        Next
        Return dt
    End Function

    Private Shared Sub CreateInvoiceEntries(ByRef returnTable As DataTable, ByRef invoiceDataTable As DataTable)
        'Dim lstEstimatedCfDFiTCharge As List(Of String) = New List(Of String)()
        'For Each columns As DataColumn In returnTable.Columns
        '    Dim colName As String = columns.ColumnName
        '    If colName.Contains("EstimatedCfDFiTCharge") Then
        '        If colName.Contains("Units") Then
        '            lstEstimatedCfDFiTCharge.Add(colName)
        '        End If
        '    End If
        'Next
        For Each row As DataRow In returnTable.Rows
            ' isHHorNhh=> True means create dataset for HH format ,False  means create dataset for NHH format
            PopulateEntries(row, invoiceDataTable, "SiteData")
            If isHHorNHH Then
                'HH Column processing
                PopulateEntries(row, invoiceDataTable, "GspData")
                PopulateEntries(row, invoiceDataTable, "AAHEDCChargeUnits")
                PopulateEntries(row, invoiceDataTable, "AvailabilityChargesUnits")
                PopulateEntries(row, invoiceDataTable, "ExcessAvailabilityChargesUnits")
            End If
            'Column common to HH and NHH format
            PopulateEntries(row, invoiceDataTable, "ClimateChangeLevyUnits")
            If isHHorNHH Then
                'HH column processing
                PopulateEntries(row, invoiceDataTable, "LevyExemptCertificate(Renewable)Units")
            Else
                'NHH column processing
                PopulateEntries(row, invoiceDataTable, "EnergyDayRegister1Units")
                PopulateEntries(row, invoiceDataTable, "EnergyDayRegister2Units")
                PopulateEntries(row, invoiceDataTable, "EnergyDayRegister3Units")
                PopulateEntries(row, invoiceDataTable, "EnergyDayRegister4Units")
                PopulateEntries(row, invoiceDataTable, "EnergyDayRegister5Units")
                PopulateEntries(row, invoiceDataTable, "EnergyNightRegister1Units")
                PopulateEntries(row, invoiceDataTable, "EnergyNightRegister2Units")
                PopulateEntries(row, invoiceDataTable, "EnergyNightRegister4Units")
                PopulateEntries(row, invoiceDataTable, "EnergyNightRegister5Units")
                PopulateEntries(row, invoiceDataTable, "EnergyOffPeakRegister1Units")
                PopulateEntries(row, invoiceDataTable, "EnergyOffPeakRegister2Units")
                PopulateEntries(row, invoiceDataTable, "EnergyOffPeakRegister6Units")
                PopulateEntries(row, invoiceDataTable, "EnergyPeakRegister1Units")
                PopulateEntries(row, invoiceDataTable, "EnergyPeakRegister2Units")
                PopulateEntries(row, invoiceDataTable, "EnergyPeakRegister3Units")
                PopulateEntries(row, invoiceDataTable, "EnergyPeakRegister4Units")
                PopulateEntries(row, invoiceDataTable, "EnergyPeakRegister5Units")
                PopulateEntries(row, invoiceDataTable, "EnergyPeakRegister6Units")
                PopulateEntries(row, invoiceDataTable, "EnergySingleRegister1Units")
                PopulateEntries(row, invoiceDataTable, "EnergySingleRegister2Units")
                PopulateEntries(row, invoiceDataTable, "EnergySingleRegister3Units")
                PopulateEntries(row, invoiceDataTable, "CFD_FIT_TTL")
                'For Each lstitem As String In lstEstimatedCfDFiTCharge
                '    PopulateEntries(row, invoiceDataTable, lstitem)
                'Next
                'PopulateEntries(row, invoiceDataTable, "EstimatedCfDFiTCharge(01/01/15-31/01/15)Units")
                'PopulateEntries(row, invoiceDataTable, "EstimatedCfDFiTCharge(01/02/15-28/02/15)Units")
                'PopulateEntries(row, invoiceDataTable, "EstimatedCfDFiTCharge(01/03/15-12/03/15)Units")
                'PopulateEntries(row, invoiceDataTable, "EstimatedCfDFiTCharge(01/03/15-13/03/15)Units")
                'PopulateEntries(row, invoiceDataTable, "EstimatedCfDFiTCharge(01/03/15-22/03/15)Units")
                'PopulateEntries(row, invoiceDataTable, "EstimatedCfDFiTCharge(01/03/15-31/03/15)Units")
                'PopulateEntries(row, invoiceDataTable, "EstimatedCfDFiTCharge(01/04/15-01/05/15)Units")
                'PopulateEntries(row, invoiceDataTable, "EstimatedCfDFiTCharge(01/04/15-02/05/15)Units")
                'PopulateEntries(row, invoiceDataTable, "EstimatedCfDFiTCharge(01/04/15-30/04/15)Units")
                'PopulateEntries(row, invoiceDataTable, "EstimatedCfDFiTCharge(01/05/15-31/05/15)Units")
                'PopulateEntries(row, invoiceDataTable, "EstimatedCfDFiTCharge(23/03/15-31/03/15)Units")
            End If
            'Column common to HH and NHH format
            PopulateEntries(row, invoiceDataTable, "LevyExemptEnergyUnits")
            If isHHorNHH Then
                'HH column processing
                PopulateEntries(row, invoiceDataTable, "EnergyDayUnits")
                PopulateEntries(row, invoiceDataTable, "EnergyNightUnits")
                PopulateEntries(row, invoiceDataTable, "FixedChargesUnits")
                PopulateEntries(row, invoiceDataTable, "MeterReadingChargesUnits")
                PopulateEntries(row, invoiceDataTable, "DUOSRate1Units")
                PopulateEntries(row, invoiceDataTable, "DUOSRate2Units")
                PopulateEntries(row, invoiceDataTable, "DUOSRate3Units")
                PopulateEntries(row, invoiceDataTable, "ReactivePowerChargeUnits")
                PopulateEntries(row, invoiceDataTable, "ROAdjustmentUnits")
                PopulateEntries(row, invoiceDataTable, "CapacityMarketUnits")
                PopulateEntries(row, invoiceDataTable, "VAT@StdCharge")
                PopulateEntries(row, invoiceDataTable, "VAT@ReducedCharge")
                PopulateEntries(row, invoiceDataTable, "Adhoc1Charge")
                PopulateEntries(row, invoiceDataTable, "Adhoc2Charge")
                PopulateEntries(row, invoiceDataTable, "Adhoc3Charge")
                PopulateEntries(row, invoiceDataTable, "Adhoc4Charge")
                PopulateEntries(row, invoiceDataTable, "Adhoc5Charge")
                PopulateEntries(row, invoiceDataTable, "Adhoc6Charge")
                PopulateEntries(row, invoiceDataTable, "Adhoc7Charge")
                PopulateEntries(row, invoiceDataTable, "Adhoc8Charge")
                PopulateEntries(row, invoiceDataTable, "Adhoc9Charge")
                PopulateEntries(row, invoiceDataTable, "AdHocTOTAL")
                PopulateEntries(row, invoiceDataTable, "BSUOSTotal")
                PopulateEntries(row, invoiceDataTable, "FIT_Total")
                PopulateEntries(row, invoiceDataTable, "CFD_Total")
                PopulateEntries(row, invoiceDataTable, "TriadChargeTotal")
            Else
                'NHH column processing
                PopulateEntries(row, invoiceDataTable, "VAT@20.0%Charge")
                PopulateEntries(row, invoiceDataTable, "VAT@5%Charge")
                PopulateEntries(row, invoiceDataTable, "ADDTTL")
                PopulateEntries(row, invoiceDataTable, "AHC_DUS1")
            End If
        Next

    End Sub
    Private Shared Sub PopulateEntries(dtRow As DataRow, invoiceDataTable As DataTable, ColName As String)
        Try
            If dtRow.Table.Columns.Contains(ColName) Then

                If Not String.IsNullOrEmpty(dtRow(ColName)) Then

                    Dim invoiceTableRow As DataRow = invoiceDataTable.NewRow

                    invoiceTableRow("Invoice Number") = dtRow("InvoiceNo")
                    invoiceTableRow("Meter Serial") = ""
                    invoiceTableRow("MPAN") = PopulateMPAN(dtRow)
                    invoiceTableRow("TaxPointDate") = Convert.ToDateTime(dtRow("InvoiceDate")).ToString("dd/MM/yyyy")
                    invoiceTableRow("Start Date") = Convert.ToDateTime(dtRow("BillPeriodStart")).ToString("dd/MM/yyyy")
                    invoiceTableRow("End Date") = Convert.ToDateTime(dtRow("BillPeriodEnd")).ToString("dd/MM/yyyy")
                    invoiceTableRow("Start Read") = ""
                    invoiceTableRow("Start Read Estimated") = ""
                    invoiceTableRow("End Read") = ""
                    invoiceTableRow("End Read Estimated") = ""
                    invoiceTableRow("Force kWh") = "Yes"
                    invoiceTableRow("VAT") = 0.0
                    invoiceTableRow("IsLastDayApportion") = "Yes"

                    'Dim EstimatedCfDFiTChargeCol As String = String.Empty
                    'If ColName.Contains("EstimatedCfDFiTCharge") Then
                    '    EstimatedCfDFiTChargeCol = ColName
                    'End If


                    Select Case ColName
                        Case "SiteData", "GspData", "AdHocTOTAL"
                            PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, ColName, dtRow(ColName), 0.0)

                        Case "AAHEDCChargeUnits", "AvailabilityChargesUnits", "ExcessAvailabilityChargesUnits", "ClimateChangeLevyUnits", "LevyExemptCertificate(Renewable)Units" _
                            , "LevyExemptEnergyUnits", "EnergyDayUnits", "EnergyNightUnits", "FixedChargesUnits", "MeterReadingChargesUnits", "DUOSRate1Units", "DUOSRate2Units" _
                            , "DUOSRate3Units", "ReactivePowerChargeUnits", "ROAdjustmentUnits", "CapacityMarketUnits", "EnergyDayRegister1Units", "EnergyDayRegister2Units" _
                            , "EnergyDayRegister3Units", "EnergyDayRegister4Units", "EnergyDayRegister5Units", "EnergyNightRegister1Units", "EnergyNightRegister2Units" _
                            , "EnergyNightRegister4Units", "EnergyNightRegister5Units", "EnergyOffPeakRegister1Units", "EnergyOffPeakRegister2Units", "EnergyOffPeakRegister6Units" _
                            , "EnergyPeakRegister1Units", "EnergyPeakRegister2Units", "EnergyPeakRegister3Units", "EnergyPeakRegister4Units", "EnergyPeakRegister5Units", "EnergyPeakRegister6Units" _
                            , "EnergySingleRegister1Units", "EnergySingleRegister2Units", "EnergySingleRegister3Units"

                            ' , EstimatedCfDFiTChargeCol,"EstimatedCfDFiTCharge(01/01/15-31/01/15)Units", "EstimatedCfDFiTCharge(01/02/15-28/02/15)Units" _
                            ', "EstimatedCfDFiTCharge(01/03/15-12/03/15)Units", "EstimatedCfDFiTCharge(01/03/15-13/03/15)Units", "EstimatedCfDFiTCharge(01/03/15-22/03/15)Units", "EstimatedCfDFiTCharge(01/03/15-31/03/15)Units" _
                            ', "EstimatedCfDFiTCharge(01/04/15-01/05/15)Units", "EstimatedCfDFiTCharge(01/04/15-02/05/15)Units", "EstimatedCfDFiTCharge(01/04/15-30/04/15)Units" _
                            ', "EstimatedCfDFiTCharge(01/05/15-31/05/15)Units", "EstimatedCfDFiTCharge(23/03/15-31/03/15)Units"

                            PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, ColName, dtRow(ColName), dtRow(ColName.Replace("Units", "Charge")))

                        Case "VAT@StdCharge", "VAT@ReducedCharge", "Adhoc1Charge", "Adhoc2Charge", "Adhoc3Charge", "Adhoc4Charge", "Adhoc5Charge" _
                             , "Adhoc6Charge", "Adhoc7Charge", "Adhoc8Charge", "Adhoc9Charge", "BSUOSTotal", "FIT_Total", "CFD_Total", "TriadChargeTotal" _
                             , "VAT@20.0%Charge", "VAT@5%Charge", "ADDTTL", "AHC_DUS1", "CFD_FIT_TTL"
                            PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, ColName, 0.0, dtRow(ColName))
                    End Select
                    invoiceDataTable.Rows.Add(invoiceTableRow)
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Shared Sub PopulateEntriesBasedOnColName(ByRef invoiceTableRow As DataRow, ByRef dtRow As DataRow, ByRef Column As String, ByVal Consumption As String, ByVal Cost As String)
        Try
           
            'Populate Cost Only
            Dim costOnly As String = PopulateCostOnly(Column.Trim)
            invoiceTableRow("Cost Only") = costOnly

            'If Invoice Starts with "C" and cost only => No then Consumption = Consumption *-1
            If CStr(dtRow("InvoiceNo")).Substring(0, 1) = "C" And costOnly = "No" Then
                invoiceTableRow("Consumption") = Consumption * -1
            Else
                invoiceTableRow("Consumption") = Consumption
            End If


            'Set Vat,Net and Gross
            If Column.Contains("VAT") Then
                invoiceTableRow("Net") = 0.0
                invoiceTableRow("VAT") = Cost
            Else
                invoiceTableRow("Net") = Cost
                invoiceTableRow("VAT") = 0.0
            End If


            If (Column = "EnergyDayUnits" Or Column = "EnergyNightUnits") And (CStr(dtRow("InvoiceNo")).Substring(0, 1) = "C") Then
                invoiceTableRow("Consumption") = Consumption * -1
            End If

            'Dim EstimatedCfDFiTChargeCol As String = String.Empty
            'If Column.Contains("EstimatedCfDFiTCharge") Then
            '    EstimatedCfDFiTChargeCol = Column
            'End If

            'Set Decription based on column names
            Select Case Column
                Case "SiteData", "GspData"
                    If costOnly = "Yes" Then
                        invoiceTableRow("Description") = "Consumption without Losses"
                    Else
                        invoiceTableRow("Description") = "Consumption with Losses"
                    End If
                Case "AAHEDCChargeUnits"
                    invoiceTableRow("Description") = "AAHEDC Charge"
                Case "AvailabilityChargesUnits"
                    invoiceTableRow("Description") = "Availability Charges"
                Case "ExcessAvailabilityChargesUnits"
                    invoiceTableRow("Description") = "Excess Availability Charges"
                Case "ClimateChangeLevyUnits"
                    invoiceTableRow("Description") = "Climate Change Levy"
                Case "LevyExemptCertificate(Renewable)Units"
                    invoiceTableRow("Description") = "Levy Exempt Certificate (Renewable)"
                Case "LevyExemptEnergyUnits"
                    invoiceTableRow("Description") = "Levy Exempt Energy"
                Case "EnergyDayUnits"
                    invoiceTableRow("Description") = "Energy Day (Consumption with Losses)"
                Case "EnergyNightUnits"
                    invoiceTableRow("Description") = "Energy Night (Consumption with Losses)"
                Case "FixedChargesUnits"
                    invoiceTableRow("Description") = "DUOS Standing Charge"
                Case "MeterReadingChargesUnits"
                    invoiceTableRow("Description") = "Meter Reading Charges"
                Case "DUOSRate1Units"
                    invoiceTableRow("Description") = "DUoS Band 1"
                Case "DUOSRate2Units"
                    invoiceTableRow("Description") = "DUoS Band 2"
                Case "DUOSRate3Units"
                    invoiceTableRow("Description") = "DUoS Band 3"
                Case "ReactivePowerChargeUnits"
                    invoiceTableRow("Description") = "Reactive Power Charge"
                Case "ROAdjustmentUnits"
                    invoiceTableRow("Description") = "RO Adjustment"
                Case "CapacityMarketUnits"
                    invoiceTableRow("Description") = "Capacity Market"
                Case "VAT@StdCharge"
                    invoiceTableRow("Description") = "VAT @ Std Charge"
                Case "VAT@ReducedCharge"
                    invoiceTableRow("Description") = "VAT @ Reduced Charge"
                Case "Adhoc1Charge"
                    invoiceTableRow("Description") = "Adhoc 1 Charge"
                Case "Adhoc2Charge"
                    invoiceTableRow("Description") = "Adhoc 2 Charge"
                Case "Adhoc3Charge"
                    invoiceTableRow("Description") = "Adhoc 3 Charge"
                Case "Adhoc4Charge"
                    invoiceTableRow("Description") = "Energy Cost @ APX price"
                Case "Adhoc5Charge"
                    invoiceTableRow("Description") = "Adhoc 5 Charge"
                Case "Adhoc6Charge"
                    invoiceTableRow("Description") = "Adhoc 6 Charge"
                Case "Adhoc7Charge"
                    invoiceTableRow("Description") = "Adhoc 7 Charge"
                Case "Adhoc8Charge"
                    invoiceTableRow("Description") = "Adhoc 8 Charge"
                Case "Adhoc9Charge"
                    invoiceTableRow("Description") = "Adhoc 9 Charge"
                Case "AdHocTOTAL"
                    invoiceTableRow("Description") = "Ad Hoc TOTAL"
                Case "BSUOSTotal"
                    invoiceTableRow("Description") = "BSUOS Total"
                Case "FIT_Total", "CFD_Total"
                    invoiceTableRow("Description") = Column
                Case "TriadChargeTotal"
                    invoiceTableRow("Description") = "Triad Charge Total"
                Case "EnergyDayRegister1Units"
                    invoiceTableRow("Description") = "Energy Day Register 1"
                Case "EnergyDayRegister2Units"
                    invoiceTableRow("Description") = "Energy Day Register 2"
                Case "EnergyDayRegister3Units"
                    invoiceTableRow("Description") = "Energy Day Register 3"
                Case "EnergyDayRegister4Units"
                    invoiceTableRow("Description") = "Energy Day Register 4"
                Case "EnergyDayRegister5Units"
                    invoiceTableRow("Description") = "Energy Day Register 5"
                Case "EnergyNightRegister1Units"
                    invoiceTableRow("Description") = "Energy Night Register 1"
                Case "EnergyNightRegister2Units"
                    invoiceTableRow("Description") = "Energy Night Register 2"
                Case "EnergyNightRegister4Units"
                    invoiceTableRow("Description") = "Energy Night Register 4"
                Case "EnergyNightRegister5Units"
                    invoiceTableRow("Description") = "Energy Night Register 5"
                Case "EnergyOffPeakRegister1Units"
                    invoiceTableRow("Description") = "Energy Off Peak Register 1"
                Case "EnergyOffPeakRegister2Units"
                    invoiceTableRow("Description") = "Energy Off Peak Register 2"
                Case "EnergyOffPeakRegister6Units"
                    invoiceTableRow("Description") = "Energy Off Peak Register 6"
                Case "EnergyPeakRegister1Units"
                    invoiceTableRow("Description") = "Energy Peak Register 1"
                Case "EnergyPeakRegister2Units"
                    invoiceTableRow("Description") = "Energy Peak Register 2"
                Case "EnergyPeakRegister3Units"
                    invoiceTableRow("Description") = "Energy Peak Register 3"
                Case "EnergyPeakRegister4Units"
                    invoiceTableRow("Description") = "Energy Peak Register 4"
                Case "EnergyPeakRegister5Units"
                    invoiceTableRow("Description") = "Energy Peak Register 5"
                Case "EnergyPeakRegister6Units"
                    invoiceTableRow("Description") = "Energy Peak Register 6"
                Case "EnergySingleRegister1Units"
                    invoiceTableRow("Description") = "Energy Single Register 1"
                Case "EnergySingleRegister2Units"
                    invoiceTableRow("Description") = "Energy Single Register 2"
                Case "EnergySingleRegister3Units"
                    invoiceTableRow("Description") = "Energy Single Register 3"
                Case "CFD_FIT_TTL"
                    invoiceTableRow("Description") = "CFD_FIT_TTL"

                    'Case EstimatedCfDFiTChargeCol
                    '    invoiceTableRow("Description") = GetEstimatedCfDFiTChargeCol(EstimatedCfDFiTChargeCol.Replace("Units", ""))
                    'Case "EstimatedCfDFiTCharge(01/01/15-31/01/15)Units"
                    '    invoiceTableRow("Description") = "Estimated CfD FiT Charge (01/01/15 - 31/01/15)"
                    'Case "EstimatedCfDFiTCharge(01/02/15-28/02/15)Units"
                    '    invoiceTableRow("Description") = "Estimated CfD FiT Charge (01/02/15 - 28/02/15)"
                    'Case "EstimatedCfDFiTCharge(01/03/15-12/03/15)Units"
                    '    invoiceTableRow("Description") = "Estimated CfD FiT Charge (01/03/15 - 12/03/15)"
                    'Case "EstimatedCfDFiTCharge(01/03/15-13/03/15)Units"
                    '    invoiceTableRow("Description") = "Estimated CfD FiT Charge (01/03/15 - 13/03/15)"
                    'Case "EstimatedCfDFiTCharge(01/03/15-22/03/15)Units"
                    '    invoiceTableRow("Description") = "Estimated CfD FiT Charge (01/03/15 - 22/03/15)"
                    'Case "EstimatedCfDFiTCharge(01/03/15-31/03/15)Units"
                    '    invoiceTableRow("Description") = "Estimated CfD FiT Charge (01/03/15 - 31/03/15)"
                    'Case "EstimatedCfDFiTCharge(01/04/15-01/05/15)Units"
                    '    invoiceTableRow("Description") = "Estimated CfD FiT Charge (01/04/15 - 01/05/15)"
                    'Case "EstimatedCfDFiTCharge(01/04/15-02/05/15)Units"
                    '    invoiceTableRow("Description") = "Estimated CfD FiT Charge (01/04/15 - 02/05/15)"
                    'Case "EstimatedCfDFiTCharge(01/04/15-30/04/15)Units"
                    '    invoiceTableRow("Description") = "Estimated CfD FiT Charge (01/04/15 - 30/04/15)"
                    'Case "EstimatedCfDFiTCharge(01/05/15-31/05/15)Units"
                    '    invoiceTableRow("Description") = "Estimated CfD FiT Charge (01/05/15 - 31/05/15)"
                    'Case "EstimatedCfDFiTCharge(23/03/15-31/03/15)Units"
                    '    invoiceTableRow("Description") = "Estimated CfD FiT Charge (23/03/15 - 31/03/15)"
                Case "VAT@20.0%Charge"
                    invoiceTableRow("Description") = "VAT @ 20.0% Charge"
                Case "VAT@5%Charge"
                    invoiceTableRow("Description") = "VAT @ 5% Charge"
                Case "AHC_DUS1"
                    invoiceTableRow("Description") = "Standing Charge"
                Case "ADDTTL"
                    invoiceTableRow("Description") = "Adhoc Charge Total"
            End Select

           
            invoiceTableRow("Gross") = Convert.ToDouble(invoiceTableRow("Net")) + Convert.ToDouble(invoiceTableRow("VAT"))
            invoiceTableRow("Rate") = invoiceTableRow("Description")
        Catch ex As Exception
        End Try
    End Sub
    Private Shared Sub RenameColumns(invoiceDataTable As DataTable)
        invoiceDataTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceDataTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceDataTable.Columns("Rate").ColumnName = "Tariff"
        invoiceDataTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceDataTable.Columns("End Date").ColumnName = "EndDate"
        invoiceDataTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceDataTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceDataTable.Columns("End Read").ColumnName = "EndRead"
        invoiceDataTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceDataTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceDataTable.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub
    Private Shared Function PopulateMPAN(row As DataRow) As String
        Dim mpan As String = String.Empty
        Try
            'Remove first /, and only take characters 2-14 (First MPAN)
            mpan = CStr(row("Mpan")).Remove(0, 1).Substring(0, 13)
        Catch ex As Exception
        End Try
        Return mpan
    End Function
    Private Shared Function PopulateCostOnly(ByRef ColName As String) As String
        Dim costOnly As String = String.Empty
        'Set Cost only => Yes by default,if format HH and column =>GspData then Cost only=> No else Cost only=> Yes
        'if format NHH and column =>SiteData then Cost only=> No else Cost only=> Yes
        costOnly = "Yes"
        If (ColName = "GspData") Or (Not isHHorNHH And ColName = "SiteData") Then
            costOnly = "No"
        End If
        Return costOnly
    End Function

    Private Shared Function GetEstimatedCfDFiTChargeCol(ByRef EstimatedCfDFiTChargeCol As String) As String
        Dim column As String() = EstimatedCfDFiTChargeCol.Split("(")
        Dim ColumnDates As String() = column(1).Split("-")
        Return "Estimated CfD FiT Charge ( " + ColumnDates(0) + " - " + ColumnDates(1).Replace(")", "") + " )"
    End Function

End Class
