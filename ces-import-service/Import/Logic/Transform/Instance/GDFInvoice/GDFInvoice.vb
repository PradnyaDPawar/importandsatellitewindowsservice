﻿Public Class GDFInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Try
            Dim excel As New Excel()
            Dim xSheet As ExcelSheet
            excel.Load(fullPath)
            xSheet = excel.Sheets(0)
            _returnTable = xSheet.GetDataTable()
        Catch ex As Exception
            ' Return False
        End Try

    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        DataTableTools.RemoveBlankRows(_returnTable)
        DataTableTools.RemoveBlankColumns(_returnTable)
        RemoveUnwantedRows(_returnTable)


        DataTableTools.SetHeaders(ReturnTable, 1)
        GDFInvoiceSchema.ToDeFormat(_returnTable)
        FillInvoice(_returnTable, dsEdits, importEdit)
    End Sub
    Public Sub FillInvoice(ByVal ReturnTable As DataTable, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)
        For Each row As DataRow In ReturnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub

    Public Shared Function RemoveUnwantedBlankRows(ByRef dt As DataTable) As DataTable
        Dim count As Integer
        For rowcount As Integer = dt.Rows.Count - 1 To 0 Step -1
            count = 0
            For Each item As Object In dt.Rows(rowcount).ItemArray

                If Not item.ToString = "" Then
                    count = count + 1
                End If
            Next
            If dt.Columns.Count <> count Then
                dt.Rows.RemoveAt(rowcount)
            End If
        Next
        Return dt
    End Function

    Public Shared Function RemoveUnwantedRows(ByRef dt As DataTable) As DataTable

        Dim remove As Boolean

        For rowcount As Integer = dt.Rows.Count - 1 To 0 Step -1
            remove = True
            'For Each item As Object In dt.Rows(rowcount).ItemArray

            If IsDate(dt.Rows(rowcount)(5)) Or CStr(dt.Rows(rowcount)(5)).Trim = "Invoice Date" Then
                remove = False
            End If

            'Next

            If remove Then
                dt.Rows.RemoveAt(rowcount)
            End If

        Next

        Return dt

    End Function
End Class
