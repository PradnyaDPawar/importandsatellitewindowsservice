﻿Public Class SchneiderHalfHour2
    Implements ITransformDelegate




    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvfile As New DelimitedFile(fullPath, 0) 'Create a new object of type CSV File

        csvfile.Load(_returnTable, SchneiderHalfHour2Schema.GetHeaders) 'Load the data with headers into the CSV File
    End Sub

    Private _returnTable As DataTable

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        SchneiderHalfHour2Schema.Validate(ReturnTable) 'Validate this table against the schema (Find if there are any missing columns)

        SchneiderHalfHour2Schema.ToDeFormat(ReturnTable) 'Format this table into the DigitalEnergy Fomrat

        For Each row As DataRow In _returnTable.Rows
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
