﻿Imports System.Text.RegularExpressions

Public Class GroundGasRealtimeSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable, ByVal fileName As String) As DataTable

        Dim realTimeDt As DataTable
        realTimeDt = DeFormatTableTemplates.CreateRealTimeTable()

        ' The pattern is a regular expression to identify dd-MM-yyyy followed by anything
        Dim pattern As String = "((?:(?:[0-2]?\d{1})|(?:[3][01]{1}))[-:\/.](?:[0]?[1-9]|[1][012])[-:\/.](?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(?![\d]).*"
        fileName = Regex.Replace(fileName, pattern, String.Empty)
        'The pattern below identifies integer followed by an underscore. E.g. 2119_
        pattern = "(\d+)_"
        fileName = (Regex.Replace(fileName, pattern, String.Empty)).Trim

        For Each row As DataRow In dt.Rows
            If IsDate(row(0)) Then
                For Each col As DataColumn In dt.Columns
                    If col.Ordinal <> 0 Then
                        Dim newRow As DataRow
                        newRow = realTimeDt.NewRow

                        newRow("DateTime") = row(0)
                        newRow("MeterName") = fileName & " " & col.ColumnName
                        newRow("CountUnits") = row(col)

                        realTimeDt.Rows.Add(newRow)
                    End If
                Next
            End If
        Next

        dt = realTimeDt

        Return dt

    End Function

End Class
