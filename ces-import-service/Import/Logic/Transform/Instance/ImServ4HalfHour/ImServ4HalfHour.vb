﻿Public Class ImServ4HalfHour
    Implements ITransformDelegate


    Private _returnTable As DataTable

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim csvFile As New DelimitedFile(fullPath, 0)
        csvFile.Load(_returnTable, ImServ4HalfHourSchema.GetHeaders())

    End Sub


    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        ImServ4HalfHourSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows

            If row("Unit").ToString.ToLower.Trim <> "lag" And row("Unit").ToString.ToLower.Trim <> "lead" Then
                row("Date(dd/MM/yyyy)") = CDate(row("Date(dd/MM/yyyy)")).ToString("dd/MM/yyyy")
                FE_HalfHour.Fill(row, dsEdits, importEdit)
            End If

        Next

    End Sub
End Class
