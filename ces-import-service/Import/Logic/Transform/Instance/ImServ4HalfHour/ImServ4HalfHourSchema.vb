﻿Partial Public Class ImServ4HalfHourSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("MPAN")
        headers.Add("MeterSerial")
        headers.Add("Unit")
        headers.Add("Date(dd/MM/yyyy)")

        Dim dateholder As Date = Today()


        For i = 0 To 98
            If (i Mod 2 = 0) Then
                headers.Add(dateholder.ToString("HH:mm"))
            Else
                headers.Add(dateholder.ToString("HH:mm") & " A/E")
                dateholder = dateholder.AddMinutes(30)
            End If
        Next



        Return headers

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        PopulateUnits(dt)

        Return dt
    End Function


    Public Shared Sub PopulateUnits(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")

        For Each row As DataRow In dt.Rows

            If row("Unit").ToString.ToLower = "kwh" Then
                row("ForcekWh") = "Yes"
            Else
                row("ForcekWh") = "No"
            End If

        Next

    End Sub

   

End Class
