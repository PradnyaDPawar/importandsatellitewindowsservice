﻿Imports System.Globalization

Public Class TotalControlRealTimeSchema

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)
        Dim dtResult As New DataTable, headerRow As DataRow = Nothing, valueRow As DataRow = Nothing
        Dim meterName As String = String.Empty, dtTime As DateTime = Nothing, reading As Double = 0
        dtResult.Columns.Add("MeterName")
        dtResult.Columns.Add("DateTime")
        dtResult.Columns.Add("CountUnits")

        For colIndex As Integer = 1 To dt.Columns.Count Step 3
            headerRow = dt.Rows(0)
            meterName = headerRow(colIndex).ToString()

            For rowIndex As Integer = 1 To dt.Rows.Count - 1
                valueRow = dt.Rows(rowIndex)
                Try

                    Dim newRow = dtResult.NewRow()

                    newRow.Item("MeterName") = meterName

                    dtTime = GetUKFormatDateTime(valueRow((colIndex - 1)))

                    newRow.Item("DateTime") = dtTime.ToString("MM/dd/yyyy hh:mm tt")

                    Double.TryParse(valueRow(colIndex), reading)

                    newRow.Item("CountUnits") = reading

                    dtResult.Rows.Add(newRow)
                Catch ex As Exception
                    Throw ex
                End Try
            Next
        Next

        dt = dtResult
    End Sub

    Private Shared Function GetUKFormatDateTime(ByVal strInputDateTimeValue As String) As DateTime
        Dim dtTime As DateTime = Nothing
        If (DateTime.TryParseExact(strInputDateTimeValue, "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, dtTime)) Then
            Return dtTime
        ElseIf (DateTime.TryParseExact(strInputDateTimeValue, "dd/MM/yyyy h:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, dtTime)) Then
            Return dtTime
        Else
            Throw New Exception("DateTime parse failed.")
            dtTime = Nothing
            Return dtTime
        End If
    End Function
End Class
