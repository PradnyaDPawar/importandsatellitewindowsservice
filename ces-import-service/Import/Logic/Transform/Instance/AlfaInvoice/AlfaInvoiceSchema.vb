﻿Partial Public Class AlfaInvoice

    Shared Sub ToDeFormat(ByRef dt As DataTable)

        Dim invoiceDataTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()

        For Each row As DataRow In dt.Rows
            
            PopulateConsumption(row, invoiceDataTable)
            PopulateStandingCharge(row, invoiceDataTable)
            PopulateCCL(row, invoiceDataTable)
            PopulateVat(row, invoiceDataTable)

        Next

        RenameColumns(invoiceDataTable)
    
        dt = invoiceDataTable

    End Sub

    Private Shared Sub RenameColumns(ByVal invoiceDataTable As DataTable)
        invoiceDataTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceDataTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceDataTable.Columns("Rate").ColumnName = "Tariff"
        invoiceDataTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceDataTable.Columns("End Date").ColumnName = "EndDate"
        invoiceDataTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceDataTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceDataTable.Columns("End Read").ColumnName = "EndRead"
        invoiceDataTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceDataTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceDataTable.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub


    Private Shared Sub PopulateConsumption(ByVal dtRow As DataRow, ByRef invoiceDataTable As DataTable)

        Dim invoiceTableRow As DataRow = invoiceDataTable.NewRow

        invoiceTableRow("Invoice Number") = dtRow("InvoiceNo")
        invoiceTableRow("Meter Serial") = dtRow("MeterNo")
        invoiceTableRow("MPAN") = dtRow("MeterPoint")
        invoiceTableRow("Rate") = ""
        invoiceTableRow("Start Date") = dtRow("StartDate")
        invoiceTableRow("End Date") = dtRow("EndDate")
        invoiceTableRow("Start Read") = dtRow("PreviousReading")
        invoiceTableRow("Start Read Estimated") = dtRow("PreviousType")
        invoiceTableRow("End Read") = dtRow("CurrentReading")
        invoiceTableRow("End Read Estimated") = dtRow("CurrentType")
        invoiceTableRow("Consumption") = dtRow("Energy")
        invoiceTableRow("Force kWh") = "No"
        invoiceTableRow("Net") = dtRow("GasCost").ToString.Replace("£", "")
        invoiceTableRow("Vat") = 0
        invoiceTableRow("Gross") = dtRow("GasCost").ToString.Replace("£", "")
        invoiceTableRow("Cost Only") = "No"
        invoiceTableRow("Description") = "Gas Consumption"
        invoiceTableRow("TaxPointDate") = dtRow("Tax Point Date")
        If invoiceTableRow("Start Date") = invoiceTableRow("End Date") Then
            invoiceTableRow("IsLastDayApportion") = "Yes"
        Else
            invoiceTableRow("IsLastDayApportion") = "No"
        End If


        invoiceDataTable.Rows.Add(invoiceTableRow)

    End Sub


    Private Shared Sub PopulateStandingCharge(ByVal dtRow As DataRow, ByVal InvoiceDataTable As DataTable)

        Dim invoiceTableRow As DataRow = InvoiceDataTable.NewRow

        invoiceTableRow("Invoice Number") = dtRow("InvoiceNo")
        invoiceTableRow("Meter Serial") = dtRow("MeterNo")
        invoiceTableRow("MPAN") = dtRow("MeterPoint")
        invoiceTableRow("Rate") = ""
        invoiceTableRow("Start Date") = dtRow("StartDate")
        invoiceTableRow("End Date") = dtRow("EndDate")
        invoiceTableRow("Start Read") = dtRow("PreviousReading")
        invoiceTableRow("Start Read Estimated") = dtRow("PreviousType")
        invoiceTableRow("End Read") = dtRow("CurrentReading")
        invoiceTableRow("End Read Estimated") = dtRow("CurrentType")
        invoiceTableRow("Consumption") = 0
        invoiceTableRow("Force kWh") = "No"
        invoiceTableRow("Net") = dtRow("Standing Chrg").ToString.Replace("£", "")
        invoiceTableRow("Vat") = 0
        invoiceTableRow("Gross") = dtRow("Standing Chrg").ToString.Replace("£", "")
        invoiceTableRow("Cost Only") = "Yes"
        invoiceTableRow("Description") = "Standing Charge"
        invoiceTableRow("TaxPointDate") = dtRow("Tax Point Date")
        If invoiceTableRow("Start Date") = invoiceTableRow("End Date") Then
            invoiceTableRow("IsLastDayApportion") = "Yes"
        Else
            invoiceTableRow("IsLastDayApportion") = "No"
        End If

        InvoiceDataTable.Rows.Add(invoiceTableRow)

    End Sub


    Private Shared Sub PopulateCCL(ByVal dtRow As DataRow, ByVal InvoiceDataTable As DataTable)

        Dim invoiceTableRow As DataRow = InvoiceDataTable.NewRow
        Dim result, result1 As Double
        invoiceTableRow("Invoice Number") = dtRow("InvoiceNo")
        invoiceTableRow("Meter Serial") = dtRow("MeterNo")
        invoiceTableRow("MPAN") = dtRow("MeterPoint")
        invoiceTableRow("Rate") = ""
        invoiceTableRow("Start Date") = dtRow("StartDate")
        invoiceTableRow("End Date") = dtRow("EndDate")
        invoiceTableRow("Start Read") = dtRow("PreviousReading")
        invoiceTableRow("Start Read Estimated") = dtRow("PreviousType")
        invoiceTableRow("End Read") = dtRow("CurrentReading")
        invoiceTableRow("End Read Estimated") = dtRow("CurrentType")
        invoiceTableRow("Consumption") = 0
        invoiceTableRow("Force kWh") = "No"
        If Not Double.TryParse(dtRow("CC Levey").ToString.Replace("£", ""), result) Then
            result = 0
        End If
        invoiceTableRow("Net") = result

        If Not Double.TryParse(dtRow("CCL VAT").ToString.Replace("£", ""), result1) Then
            result1 = 0
        End If
        invoiceTableRow("Vat") = result1
        
        invoiceTableRow("Gross") = CDbl(invoiceTableRow("Net")) + CDbl(invoiceTableRow("Vat"))
        invoiceTableRow("Cost Only") = "Yes"
        invoiceTableRow("Description") = "CCL"
        invoiceTableRow("TaxPointDate") = dtRow("Tax Point Date")
        If invoiceTableRow("Start Date") = invoiceTableRow("End Date") Then
            invoiceTableRow("IsLastDayApportion") = "Yes"
        Else
            invoiceTableRow("IsLastDayApportion") = "No"
        End If

        InvoiceDataTable.Rows.Add(invoiceTableRow)

    End Sub



    Private Shared Sub PopulateVat(ByVal dtRow As DataRow, ByVal InvoiceDataTable As DataTable)

        Dim invoiceTableRow As DataRow = InvoiceDataTable.NewRow
        Dim result, result1 As Double
        invoiceTableRow("Invoice Number") = dtRow("InvoiceNo")
        invoiceTableRow("Meter Serial") = dtRow("MeterNo")
        invoiceTableRow("MPAN") = dtRow("MeterPoint")
        invoiceTableRow("Rate") = ""
        invoiceTableRow("Start Date") = dtRow("StartDate")
        invoiceTableRow("End Date") = dtRow("EndDate")
        invoiceTableRow("Start Read") = dtRow("PreviousReading")
        invoiceTableRow("Start Read Estimated") = dtRow("PreviousType")
        invoiceTableRow("End Read") = dtRow("CurrentReading")
        invoiceTableRow("End Read Estimated") = dtRow("CurrentType")
        invoiceTableRow("Consumption") = 0
        invoiceTableRow("Force kWh") = "No"
        invoiceTableRow("Net") = 0
        If Not Double.TryParse(dtRow("Com Vat").ToString.Replace("£", ""), result) Then
            result = 0
        End If

        If Not Double.TryParse(dtRow("Dom Vat").ToString.Replace("£", ""), result1) Then
            result1 = 0
        End If

        invoiceTableRow("Vat") = result + result1 'Double.TryParse(dtRow("Com Vat").ToString.Replace("£", ""), result) + Double.TryParse(dtRow("Dom Vat").ToString.Replace("£", ""), result1)
        invoiceTableRow("Gross") = invoiceTableRow("Vat")
        invoiceTableRow("Cost Only") = "Yes"
        invoiceTableRow("Description") = "VAT"
        invoiceTableRow("TaxPointDate") = dtRow("Tax Point Date")
        If invoiceTableRow("Start Date") = invoiceTableRow("End Date") Then
            invoiceTableRow("IsLastDayApportion") = "Yes"
        Else
            invoiceTableRow("IsLastDayApportion") = "No"
        End If

        InvoiceDataTable.Rows.Add(invoiceTableRow)

    End Sub


End Class
