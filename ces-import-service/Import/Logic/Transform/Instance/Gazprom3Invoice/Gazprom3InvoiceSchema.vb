﻿Imports System.Globalization

Public Class Gazprom3InvoiceSchema
#Region "properties"

    Public Shared _InvNumber As String
    Public Shared _meterserial As String
    Public Shared _mpan As String
    Public Shared _startdate As String
    Public Shared _enddate As String
    Public Shared _taxpointdate As String

    Public Shared Property InvoiceNumber() As String
        Get
            Return _InvNumber
        End Get
        Set(ByVal value As String)
            _InvNumber = value
        End Set
    End Property

    Public Shared Property MeterSerial() As String
        Get
            Return _meterserial
        End Get
        Set(ByVal value As String)
            _meterserial = value
        End Set
    End Property

    Public Shared Property MPAN() As String
        Get
            Return _mpan
        End Get
        Set(ByVal value As String)
            _mpan = value
        End Set
    End Property

    Public Shared Property StartDate() As String
        Get
            Return _startdate
        End Get
        Set(ByVal value As String)
            _startdate = value
        End Set
    End Property

    Public Shared Property EndDate() As String
        Get
            Return _enddate
        End Get
        Set(ByVal value As String)
            _enddate = value
        End Set
    End Property
    Public Shared Property TaxPointDate() As String
        Get
            Return _taxpointdate
        End Get
        Set(ByVal value As String)
            _taxpointdate = value
        End Set
    End Property
#End Region

    Public Shared Function GetHeaders(ByVal colcount As Integer) As List(Of String)

        Dim headers As New List(Of String)

        For index = 0 To colcount - 1
            headers.Add(index)
        Next

        Return headers

    End Function
    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        Dim vat, gross, net As Double
        Dim standardInvoiceTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        Dim invoiceflag As Boolean = True

        For Each row As DataRow In dt.Rows


            Dim invoiceTableRow As DataRow = standardInvoiceTable.NewRow
            If row("0").ToString.ToLower = "invoice" Then
                TaxPointDate = row("16")
            End If
            If (row(0).ToString.ToLower = "charge") Or (row(0).ToString.ToLower = "vat") Then
                invoiceTableRow("Invoice Number") = row("2")
                invoiceTableRow("Description") = row("7")
                invoiceTableRow("Consumption") = row("24")

                If row(0).ToString.ToLower = "charge" Then
                    net = row("28")
                Else
                    net = 0
                End If
                If row(0).ToString.ToLower = "vat" Then
                    vat = row("4")
                Else
                    vat = 0
                End If
                invoiceTableRow("Net") = net
                gross = net + vat
                invoiceTableRow("Vat") = vat
                invoiceTableRow("Gross") = gross
                invoiceTableRow("Start Read Estimated") = row("15")
                invoiceTableRow("End Read Estimated") = row("18")
                invoiceTableRow("Start Read") = row("14")
                invoiceTableRow("End Read") = row("17")

                PopulateDate(invoiceTableRow, row.Table, row)
                PopulateDetails(invoiceTableRow, row.Table, row)
                PopulateCostOnlyAndForceKwhColumn(invoiceTableRow)
                PopulateRateColumn(invoiceTableRow, row) 'Also populates description column 
                standardInvoiceTable.Rows.Add(invoiceTableRow)

            End If

        Next
        PopulateIsLastDayApportion(standardInvoiceTable)
        RenameColumns(standardInvoiceTable)
        dt = standardInvoiceTable
        Return dt

    End Function
    Private Shared Sub PopulateCostOnlyAndForceKwhColumn(ByRef invoiceTableRow As DataRow)
        Dim consumption As String = IIf(IsDBNull(invoiceTableRow("Consumption")), "", invoiceTableRow("Consumption"))
        If consumption <> "" Then
            invoiceTableRow("Cost Only") = "No"
            invoiceTableRow("Force kWh") = "Yes"
        Else
            invoiceTableRow("Cost Only") = "Yes"
            invoiceTableRow("Force kWh") = "No"
        End If
    End Sub

    Private Shared Sub PopulateRateColumn(ByRef invoiceTableRow As DataRow, ByRef row As DataRow)
        ' Populate Description, also Rate if Cost Only No
        If row(0).ToString.ToLower = "charge" Then
            invoiceTableRow("Rate") = row("7")
            invoiceTableRow("Description") = row("7")
        ElseIf row(0).ToString.ToLower = "vat" Then
            invoiceTableRow("Rate") = row(0)
            invoiceTableRow("Description") = row(0)
        End If

    End Sub


    Private Shared Sub PopulateIsLastDayApportion(ByVal InvoiceDt As DataTable)

        For Each row As DataRow In InvoiceDt.Rows
            row("IsLastDayApportion") = "No"
        Next

    End Sub

    Private Shared Sub RenameColumns(ByVal InvoiceDt As DataTable)
        InvoiceDt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        InvoiceDt.Columns("Meter Serial").ColumnName = "MeterSerial"
        InvoiceDt.Columns("Rate").ColumnName = "Tariff"
        InvoiceDt.Columns("Start Date").ColumnName = "StartDate"
        InvoiceDt.Columns("End Date").ColumnName = "EndDate"
        InvoiceDt.Columns("Start Read").ColumnName = "StartRead"
        InvoiceDt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        InvoiceDt.Columns("End Read").ColumnName = "EndRead"
        InvoiceDt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        InvoiceDt.Columns("Force kWh").ColumnName = "ForcekWh"
        InvoiceDt.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub

    Private Shared Sub PopulateDetails(ByVal invoiceTableRow As DataRow, ByRef dt As DataTable, ByVal row As DataRow)
        If row("0").ToString.ToLower = "vat" Then
            Dim resultrows() = dt.Select("[11] <> '' and [12]<>'' and [2]='" & row("2") & "' ")

            If resultrows.Length > 0 Then


                MeterSerial = resultrows(0)("12")
                MPAN = resultrows(0)("11")
                InvoiceNumber = resultrows(0)("2")



            End If

            If row("2") = InvoiceNumber Then

                invoiceTableRow("MPAN") = MPAN
                invoiceTableRow("Meter Serial") = MeterSerial

            End If
        ElseIf row("0").ToString.ToLower = "charge" And row("11") <> "" Then

            Dim resultrows() = dt.Select("[2]='" & row("2") & "' and [11]='" & row("11") & "' and [12]='" & row("12") & "'")


            If resultrows.Length > 0 Then


                MeterSerial = resultrows(0)("12")
                MPAN = resultrows(0)("11")
                InvoiceNumber = resultrows(0)("2")



            End If

            If row("2") = InvoiceNumber Then

                invoiceTableRow("MPAN") = MPAN
                invoiceTableRow("Meter Serial") = MeterSerial

            End If

        Else

            If row("11").trim = "" Or row("12").trim = "" Then

                If row("0").ToString.ToLower = "charge" Then

                    Dim resultrows() = dt.Select("[11] <> '' and [12]<>'' and [2]='" & row("2") & "' ")

                    If resultrows.Length > 0 Then


                        MeterSerial = resultrows(0)("12")
                        MPAN = resultrows(0)("11")
                        InvoiceNumber = resultrows(0)("2")



                    End If

                    If row("2") = InvoiceNumber Then

                        invoiceTableRow("MPAN") = MPAN
                        invoiceTableRow("Meter Serial") = MeterSerial

                    End If

                End If

            End If

        End If

    End Sub


    Private Shared Sub PopulateDate(ByVal invoiceTableRow As DataRow, ByRef dt As DataTable, ByVal row As DataRow)

        If row("0").ToString.ToLower = "vat" Then
            Dim resultrows() = dt.Select("[13] <> '' and [16]<>'' and [2]='" & row("2") & "' ")

            If resultrows.Length > 0 Then

                StartDate = resultrows(0)("13")
                EndDate = resultrows(0)("16")

                InvoiceNumber = resultrows(0)("2")

            End If

            If row("2") = InvoiceNumber Then

                invoiceTableRow("Start Date") = DateTime.ParseExact(StartDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()
                invoiceTableRow("End Date") = DateTime.ParseExact(EndDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()
                invoiceTableRow("TaxPointDate") = DateTime.ParseExact(TaxPointDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()
            End If
        ElseIf row("0").ToString.ToLower = "charge" And row("13") <> "" Then

            Dim resultrows() = dt.Select("[2]='" & row("2") & "' and [13]='" & row("13") & "' and [16]='" & row("16") & "'")


            If resultrows.Length > 0 Then


                StartDate = resultrows(0)("13")
                EndDate = resultrows(0)("16")
                InvoiceNumber = resultrows(0)("2")



            End If

            If row("2") = InvoiceNumber Then

                invoiceTableRow("Start Date") = DateTime.ParseExact(StartDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()
                invoiceTableRow("End Date") = DateTime.ParseExact(EndDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()
                invoiceTableRow("TaxPointDate") = DateTime.ParseExact(TaxPointDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()
            End If


        Else
            If row("13").trim = "" Or row("16").trim = "" Then

                If row("0").ToString.ToLower = "charge" Then

                    Dim resultrows() = dt.Select("[13] <> '' and [16]<>'' and [2]='" & row("2") & "' ")

                    If resultrows.Length > 0 Then

                        StartDate = resultrows(0)("13")
                        EndDate = resultrows(0)("16")
                        InvoiceNumber = resultrows(0)("2")

                    End If

                    If row("2") = InvoiceNumber Then

                        invoiceTableRow("Start Date") = DateTime.ParseExact(StartDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()
                        invoiceTableRow("End Date") = DateTime.ParseExact(EndDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()
                        invoiceTableRow("TaxPointDate") = DateTime.ParseExact(TaxPointDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()

                    End If

                End If

            End If
           

        End If
        ' Added IF statement for same dates
        Dim result = DateTime.Compare(invoiceTableRow("Start Date"), invoiceTableRow("End Date"))

        If result = 0 Then
            invoiceTableRow("End Date") = DateTime.ParseExact(StartDate, "yyyyMMdd", CultureInfo.InvariantCulture).AddDays(1).ToShortDateString()
        End If

    End Sub
    'Shared Sub ToDeFormat(ByRef returnTable As DataTable)
    '    Dim invoiceDataTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
    '    CreateInvoiceEntries(returnTable, invoiceDataTable)
    '    RenameColumns(invoiceDataTable)
    '    returnTable = invoiceDataTable
    'End Sub
    'Private Shared Sub CreateInvoiceEntries(ByRef returnTable As DataTable, ByRef invoiceDataTable As DataTable)
    '    Dim invoiceTableRow As DataRow
    '    For Each row As DataRow In returnTable.Rows
    '        If CStr(row("Header1")).ToLower = "invoice" Then
    '            If Not IsNothing(invoiceTableRow) Then
    '                invoiceDataTable.Rows.Add(invoiceTableRow)
    '            End If
    '            invoiceTableRow = invoiceDataTable.NewRow
    '            invoiceTableRow("Invoice Number") = row("Header2")
    '            invoiceTableRow("TaxPointDate") = Convert.ToDateTime(row("Header16")).ToString("dd/MM/yyyy")
    '            Continue For
    '            '
    '        End If
    '        If CStr(row("Header1")).ToLower = "charge" Then
    '            If CStr(row("Header8")).ToLower = "day units" Then
    '                invoiceTableRow("Meter Serial") = row("Header13")
    '                invoiceTableRow("MPAN") = row("Header12")
    '                If Not (String.IsNullOrEmpty(invoiceTableRow("Meter Serial"))) And Not (String.IsNullOrEmpty(invoiceTableRow("MPAN"))) Then
    '                    invoiceTableRow("Start Date") = Convert.ToDateTime(row("Header14")).ToString("dd/MM/yyyy")
    '                    invoiceTableRow("End Date") = Convert.ToDateTime(row("Header17")).ToString("dd/MM/yyyy")
    '                    invoiceTableRow("Start Read") = row("Header15")
    '                    invoiceTableRow("Start Read Estimated") = row("Header16")
    '                    invoiceTableRow("End Read") = row("Header18")
    '                    invoiceTableRow("End Read Estimated") = row("Header19")
    '                End If
    '                Continue For
    '            End If
    '            invoiceTableRow("Rate") = row("Header8")
    '        End If
    '        If CStr(row("Header1")).ToLower = "vat" Then
    '            invoiceTableRow("Rate") = row("Header1")
    '        End If
    '    Next
    'End Sub
    'Private Shared Sub RenameColumns(invoiceDataTable As DataTable)
    '    invoiceDataTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
    '    invoiceDataTable.Columns("Meter Serial").ColumnName = "MeterSerial"
    '    invoiceDataTable.Columns("Rate").ColumnName = "Tariff"
    '    invoiceDataTable.Columns("Start Date").ColumnName = "StartDate"
    '    invoiceDataTable.Columns("End Date").ColumnName = "EndDate"
    '    invoiceDataTable.Columns("Start Read").ColumnName = "StartRead"
    '    invoiceDataTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
    '    invoiceDataTable.Columns("End Read").ColumnName = "EndRead"
    '    invoiceDataTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
    '    invoiceDataTable.Columns("Force kWh").ColumnName = "ForcekWh"
    '    invoiceDataTable.Columns("Cost Only").ColumnName = "CostOnly"
    'End Sub
End Class
