﻿Public Class GazpromEdiInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim list As New List(Of String)

        Dim csv As New DelimitedFile(fullPath, 29)
        csv.HasHeader = False
        csv.TrimWhiteSpace = True
        list = GazpromEdiInvoiceSchema.GetHeaders(29)

        csv.LoadNonConsistentTable(_returnTable, list)


    End Sub

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        GazpromEdiInvoiceSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows

            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next

    End Sub
End Class
