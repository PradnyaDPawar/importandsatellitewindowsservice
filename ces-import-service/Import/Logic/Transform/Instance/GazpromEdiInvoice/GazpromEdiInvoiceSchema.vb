﻿Imports System.Globalization

Public Class GazpromEdiInvoiceSchema

#Region "properties"

    Public Shared _InvNumber As String
    Public Shared _meterserial As String
    Public Shared _mpan As String
    Public Shared _startdate As String
    Public Shared _enddate As String

    Public Shared Property InvoiceNumber() As String
        Get
            Return _InvNumber
        End Get
        Set(ByVal value As String)
            _InvNumber = value
        End Set
    End Property

    Public Shared Property MeterSerial() As String
        Get
            Return _meterserial
        End Get
        Set(ByVal value As String)
            _meterserial = value
        End Set
    End Property

    Public Shared Property MPAN() As String
        Get
            Return _mpan
        End Get
        Set(ByVal value As String)
            _mpan = value
        End Set
    End Property
    
    Public Shared Property StartDate() As String
        Get
            Return _startdate
        End Get
        Set(ByVal value As String)
            _startdate = value
        End Set
    End Property

    Public Shared Property EndDate() As String
        Get
            Return _enddate
        End Get
        Set(ByVal value As String)
            _enddate = value
        End Set
    End Property

#End Region


    Public Shared Function GetHeaders(ByVal colcount As Integer) As List(Of String)

        Dim headers As New List(Of String)

        For index = 0 To colcount - 1
            headers.Add(index)
        Next

        Return headers

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        Dim vat, vatrate, gross, net As Double
        Dim standardInvoiceTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        Dim invoiceflag As Boolean = True
        '   Dim invoiceDate As String
        
        For Each row As DataRow In dt.Rows
            'invoiceDate = (GetInvoiceDate(dt, dt.Rows.IndexOf(row)))
            vatrate = GetVat(dt, dt.Rows.IndexOf(row))
            Dim invoiceTableRow As DataRow = standardInvoiceTable.NewRow
            If row(0).ToString.ToLower = "charge" Then
                invoiceTableRow("Invoice Number") = row("2")
                invoiceTableRow("Description") = row("7")
                'invoiceTableRow("Start Date") = DateTime.ParseExact(row("26"), "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()
                'invoiceTableRow("End Date") = DateTime.ParseExact(row("27"), "yyyyMMdd", CultureInfo.InvariantCulture)
                invoiceTableRow("Consumption") = row("24")
                invoiceTableRow("Force kWh") = "Yes"
                invoiceTableRow("Net") = row("28")
                net = row("28")
                vat = (net * vatrate) / 100
                gross = net + vat
                'invoiceTableRow("MPAN") = row("11")
                'invoiceTableRow("Meter Serial") = row("12")
                invoiceTableRow("Vat") = vat
                invoiceTableRow("Gross") = gross
                invoiceTableRow("Start Read Estimated") = row("15")
                invoiceTableRow("End Read Estimated") = row("18")
                invoiceTableRow("Start Read") = row("14")
                invoiceTableRow("End Read") = row("17")

                '      invoiceTableRow("TaxPointDate") = DateTime.ParseExact(invoiceDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()
                PopulateDate(invoiceTableRow, row.Table, row)
                PopulateDetails(invoiceTableRow, row.Table, row)
                PopulateCostOnlyColumn(invoiceTableRow)
                PopulateRateColumn(invoiceTableRow, row) 'Also populates description column 
                standardInvoiceTable.Rows.Add(invoiceTableRow)

            End If

        Next
        PopulateIsLastDayApportion(standardInvoiceTable)
        RenameColumns(standardInvoiceTable)
        dt = standardInvoiceTable
        Return dt

    End Function

    'Private Shared Function GetInvoiceDate(ByRef dt As DataTable, ByVal currentRow As Integer)

    '    Dim rowcount As Integer = currentRow
    '    While Not dt.Rows(rowcount)(0).ToString.ToLower = "invoice" And rowcount < dt.Rows.Count - 1
    '        rowcount = rowcount + 1
    '    End While

    '    Return dt.Rows(rowcount)("15")

    'End Function
    Private Shared Function GetVat(ByRef dt As DataTable, ByVal currentRow As Integer) As Double

        Dim rowcount As Integer = currentRow
        While Not dt.Rows(rowcount)(0).ToString.ToLower = "vat" And rowcount < dt.Rows.Count - 1
            rowcount = rowcount + 1
        End While

        Return dt.Rows(rowcount)("3")

    End Function

    Private Shared Sub PopulateCostOnlyColumn(ByRef invoiceTableRow As DataRow)

        Dim costOnlyNoList As List(Of String) = GetCostOnlyNoList() ' List of cost only no Charges

        If invoiceTableRow("Consumption") <> "" And costOnlyNoList.Contains(invoiceTableRow("Description")) Then
            invoiceTableRow("Cost Only") = "No"
        Else
            invoiceTableRow("Cost Only") = "Yes"
        End If

    End Sub

    Private Shared Function GetCostOnlyNoList() As List(Of String)
        Dim costOnlyNoList As New List(Of String)

        costOnlyNoList.Add("Day Units")
        costOnlyNoList.Add("Night Units")

        Return costOnlyNoList

    End Function

    Private Shared Sub PopulateRateColumn(ByRef invoiceTableRow As DataRow, ByRef row As DataRow)
        ' Populate Description, also Rate if Cost Only No
        If invoiceTableRow("Cost Only") = "No" Then
            invoiceTableRow("Rate") = row("7")
            invoiceTableRow("Description") = row("7")
        Else
            invoiceTableRow("Description") = row("7")
        End If

    End Sub


    Private Shared Sub PopulateIsLastDayApportion(ByVal InvoiceDt As DataTable)

        For Each row As DataRow In InvoiceDt.Rows
            row("IsLastDayApportion") = "No"
        Next

    End Sub

    Private Shared Sub RenameColumns(ByVal InvoiceDt As DataTable)
        InvoiceDt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        InvoiceDt.Columns("Meter Serial").ColumnName = "MeterSerial"
        InvoiceDt.Columns("Rate").ColumnName = "Tariff"
        InvoiceDt.Columns("Start Date").ColumnName = "StartDate"
        InvoiceDt.Columns("End Date").ColumnName = "EndDate"
        InvoiceDt.Columns("Start Read").ColumnName = "StartRead"
        InvoiceDt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        InvoiceDt.Columns("End Read").ColumnName = "EndRead"
        InvoiceDt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        InvoiceDt.Columns("Force kWh").ColumnName = "ForcekWh"
        InvoiceDt.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub

    Private Shared Sub PopulateDetails(ByVal invoiceTableRow As DataRow, ByRef dt As DataTable, ByVal row As DataRow)


        If row("0").ToString.ToLower = "charge" And row("11") <> "" Then

            Dim resultrows() = dt.Select("[2]='" & row("2") & "' and [11]='" & row("11") & "' and [12]='" & row("12") & "'")


            If resultrows.Length > 0 Then


                MeterSerial = resultrows(0)("12")
                MPAN = resultrows(0)("11")
                InvoiceNumber = resultrows(0)("2")



            End If

            If row("2") = InvoiceNumber Then

                invoiceTableRow("MPAN") = MPAN
                invoiceTableRow("Meter Serial") = MeterSerial

            End If

        Else

            If row("11").trim = "" Or row("12").trim = "" Then

                If row("0").ToString.ToLower = "charge" Then

                    Dim resultrows() = dt.Select("[11] <> '' and [12]<>'' and [2]='" & row("2") & "' ")

                    If resultrows.Length > 0 Then


                        MeterSerial = resultrows(0)("12")
                        MPAN = resultrows(0)("11")
                        InvoiceNumber = resultrows(0)("2")



                    End If

                    If row("2") = InvoiceNumber Then

                        invoiceTableRow("MPAN") = MPAN
                        invoiceTableRow("Meter Serial") = MeterSerial

                    End If

                End If

            End If

        End If

    End Sub


    Private Shared Sub PopulateDate(ByVal invoiceTableRow As DataRow, ByRef dt As DataTable, ByVal row As DataRow)

        If row("0").ToString.ToLower = "charge" And row("13") <> "" Then

            Dim resultrows() = dt.Select("[2]='" & row("2") & "' and [13]='" & row("13") & "' and [16]='" & row("16") & "'")


            If resultrows.Length > 0 Then


                StartDate = resultrows(0)("13")
                EndDate = resultrows(0)("16")
                InvoiceNumber = resultrows(0)("2")



            End If

            If row("2") = InvoiceNumber Then

                invoiceTableRow("Start Date") = DateTime.ParseExact(StartDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()
                invoiceTableRow("End Date") = DateTime.ParseExact(EndDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()

            End If

        Else

            If row("13").trim = "" Or row("16").trim = "" Then

                If row("0").ToString.ToLower = "charge" Then

                    Dim resultrows() = dt.Select("[13] <> '' and [16]<>'' and [2]='" & row("2") & "' ")

                    If resultrows.Length > 0 Then

                        StartDate = resultrows(0)("13")
                        EndDate = resultrows(0)("16")
                        InvoiceNumber = resultrows(0)("2")

                    End If

                    If row("2") = InvoiceNumber Then

                        invoiceTableRow("Start Date") = DateTime.ParseExact(StartDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()
                        invoiceTableRow("End Date") = DateTime.ParseExact(EndDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()


                    End If

                End If

            End If

        End If

    End Sub

End Class
