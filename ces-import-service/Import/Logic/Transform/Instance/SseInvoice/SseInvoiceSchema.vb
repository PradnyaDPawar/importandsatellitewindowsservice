﻿Public Class SseInvoiceSchema

    Public Shared _IdCrac As String
    Public Shared Property IdCrac() As String

        Get
            Return _IdCrac
        End Get
        Set(ByVal value As String)
            _IdCrac = value
        End Set
    End Property
    Public Shared _meterSerial As String
    Public Shared Property MeterSerial() As String

        Get
            Return _meterSerial
        End Get
        Set(ByVal value As String)
            _meterSerial = value
        End Set
    End Property
    Public Shared _mpan As String
    Public Shared Property MPAN() As String

        Get
            Return _mpan
        End Get
        Set(ByVal value As String)
            _mpan = value
        End Set
    End Property

    Public Shared ListCostOnlyYes As New List(Of String)(New String() {"Climate change levy", "Feed in tariff charge", "Quarterly charge", _
                                                    "TPR 01042 T - Split Night", "TPR 01043 T - Split Day", "VAT", _
                                                    "Monthly charge", "TPR 00249B C PK KWH DEC/JAN M3", "TPR 00252B C PK KWH NOV/FEB M3", _
                                                    "Unrestricted KWH Q1", "Unrestricted units", "Data collector charge", "Units kWh  C1", "Available capacity", _
                                                   "Maximum demand unrestricted", "Reactive power adjustment"})




    Public Shared Sub ToDeFormat(ByRef dt As DataTable)


        PopulateMeterSerial(dt)

        Dim invoiceTbl As DataTable = DeFormatTableTemplates.CreateInvoiceTable

        For Each row As DataRow In dt.Rows

            PopulateReadEstimation(row)

            PopulateInvoiceTable(invoiceTbl, row)

        Next

        RenameColumns(invoiceTbl)
        dt = invoiceTbl


    End Sub

    Private Shared Sub PopulateInvoiceTable(ByRef invoiceTbl As DataTable, ByVal row As DataRow)
        Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow

        Dim Vat, net As Double

        newInvoiceTblRow("Invoice Number") = row("INVOICE_NO")

        newInvoiceTblRow("Meter Serial") = row("METER")

        newInvoiceTblRow("MPAN") = row("MPAN")

        If String.IsNullOrEmpty(newInvoiceTblRow("Invoice Number")) Or newInvoiceTblRow("Invoice Number").ToString.ToLower.Trim = "null" Then
            If IsDate(row("BILL_START_DT")) Then
                newInvoiceTblRow("Invoice Number") = CDate(row("BILL_START_DT")).ToString("ddMMyyyy") & "_" & newInvoiceTblRow("MPAN")
            Else
                newInvoiceTblRow("Invoice Number") = "_" & newInvoiceTblRow("MPAN") 'Let the user decide how to deal with this
            End If

        End If

        newInvoiceTblRow("Start Date") = row("BILL_START_DT")

        newInvoiceTblRow("End Date") = row("BILL_END_DT")

        If IsDate(row("PREV_READ_DT")) Then

            newInvoiceTblRow("Start Read") = row("PREV_READ_VAL")

            newInvoiceTblRow("Start Read Estimated") = row("PREV_READ_TYPE")

            newInvoiceTblRow("End Read") = row("CURR_READ_VAL")

            newInvoiceTblRow("End Read Estimated") = row("CURR_READ_TYPE")

        End If

        newInvoiceTblRow("Consumption") = row("USAGE")

        newInvoiceTblRow("Force Kwh") = "Yes"

        net = Val(row("AMOUNT"))

        Vat = Val(row("Vat"))

        newInvoiceTblRow("Net") = net

        If row("BILL_CHRG_DESC") = "VAT" Then

            newInvoiceTblRow("Vat") = Vat
            newInvoiceTblRow("Net") = 0
            newInvoiceTblRow("Gross") = 0 + Vat

        Else

            newInvoiceTblRow("Net") = net
            newInvoiceTblRow("Vat") = 0
            newInvoiceTblRow("Gross") = net + 0
        End If

        newInvoiceTblRow("Description") = row("BILL_CHRG_DESC")

        newInvoiceTblRow("Cost Only") = IsCostOnly(row("BILL_CHRG_DESC"))

        If newInvoiceTblRow("Cost Only") = "No" Then

            newInvoiceTblRow("Rate") = row("BILL_CHRG_DESC")

        End If

        newInvoiceTblRow("TaxPointDate") = row("SENT_DT")

        If newInvoiceTblRow("Start Date") = newInvoiceTblRow("End Date") Then
            newInvoiceTblRow("IsLastDayApportion") = "Yes"
        Else
            newInvoiceTblRow("IsLastDayApportion") = "No"
        End If




        invoiceTbl.Rows.Add(newInvoiceTblRow)

    End Sub



    Public Shared Function IsCostOnly(ByVal value As String)

        If ListCostOnlyYes.Contains(value) Then

            Return "Yes"

        Else
            Return "No"

        End If


    End Function

    'Private Shared Sub PopulateCostOnly(ByVal row As DataRow)

    '    Dim ListCostOnlyYes As New List(Of String)(New String() {"Climate change levy", "Feed in tariff charge", "Quarterly charge",
    '                                                  "TPR 01042 T - Split Night", "TPR 01043 T - Split Day", "VAT",
    '                                                  "Monthly charge", "TPR 00249B C PK KWH DEC/JAN M3", "TPR 00252B C PK KWH NOV/FEB M3",
    '                                                  "Unrestricted KWH Q1", "Unrestricted units", "Data collector charge", "Units kWh  C1", "Available capacity",
    '                                                 "Maximum demand unrestricted", "Reactive power adjustment"})


    'Dim ListCostOnlyNo As New List(Of String)(New String() {"Day Units","Night Units", "TPR 00244 C - WINTER",
    '                                               "TPR 00249 C - PEAK", "TPR 00092 C - OTHER TIMES", "TPR 00210A C - NIGHT", "TPR 00240 C - WINTER",
    '                                              "Unauthorised kVA - Oct2013", "TPR 00038 Wkday 06:30 to 16:00", "TPR 00080 wkday/wkend Peaks", "TPR 00221A NIGHT ALL WEEK",
    '                                              "TPR 00247 Wkday 16:00 to 19:00", "TPR 00132 C WNTR WEEKDY KWH M3", "TPR 00206C - C NIGHT KWH M3",
    '                                               "TPR 00230 - C OTHER TIMES KWH", "Maximum demand kVA C1", "TPR 00001 - Clock",
    '                                               "TPR 00040 - Clock", "TPR 00043 - Clock - Day", "TPR 00080 C EVE/WKEND KWH Q4",
    '                                               "TPR 00148 C WEEKDAY KWH Q4", "TPR 00206 - Clock", "TPR 00210 - Clock - Night",
    '                                               "TPR 00221 C NIGHT KWH Q4", "TPR 00252 C - PEAK"})



    '    If ListCostOnlyYes.Contains(row("BILL_CHRG_DESC")) Then

    '        row("Cost Only") = "Yes"

    '    Else
    '        row("Cost Only") = "No"

    '    End If

    'End Sub

    Private Shared Sub PopulateReadEstimation(ByVal row As DataRow)
        Dim ListActual As New List(Of Char)(New Char() {"a", "c", "f", "i", "m", "p", "r", "s", "b"})
        Dim ListEstimated As New List(Of Char)(New Char() {"d", "e"})

        ' Start Read Estimated
        If ListActual.Contains(row("PREV_READ_TYPE").ToString.ToLower) Then
            row("PREV_READ_TYPE") = "A"
        ElseIf ListEstimated.Contains(row("PREV_READ_TYPE").ToString.ToLower) Then
            row("PREV_READ_TYPE") = "E"
        End If

        ' End Read Estimated

        If ListActual.Contains(row("CURR_READ_TYPE").ToString.ToLower) Then
            row("CURR_READ_TYPE") = "A"
        ElseIf ListEstimated.Contains(row("CURR_READ_TYPE").ToString.ToLower) Then
            row("CURR_READ_TYPE") = "E"
        End If

    End Sub

    Private Shared Sub PopulateMeterSerial(ByRef dt As DataTable)

        'for blank meterserials and mpan

        For Each row As DataRow In dt.Rows

            If row("METER").trim = "" Or row("MPAN").trim = "" Then

                Dim resultrows() = dt.Select("[METER] <> '""' and [MPAN]<>'0' and [ID_CRAC]='" & row("ID_CRAC") & "' ")

                If resultrows.Length > 0 Then


                    MeterSerial = resultrows(0)("METER")
                    MPAN = resultrows(0)("MPAN")
                    IdCrac = resultrows(0)("ID_CRAC")



                End If

                If row("ID_CRAC") = IdCrac Then

                    row("METER") = MeterSerial
                    row("MPAN") = MPAN

                End If

            End If
        Next

    End Sub

    Private Shared Sub RenameColumns(ByVal InvoiceTbl As DataTable)
        InvoiceTbl.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        InvoiceTbl.Columns("Meter Serial").ColumnName = "MeterSerial"
        InvoiceTbl.Columns("Rate").ColumnName = "Tariff"
        InvoiceTbl.Columns("Start Date").ColumnName = "StartDate"
        InvoiceTbl.Columns("End Date").ColumnName = "EndDate"
        InvoiceTbl.Columns("Start Read").ColumnName = "StartRead"
        InvoiceTbl.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        InvoiceTbl.Columns("End Read").ColumnName = "EndRead"
        InvoiceTbl.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        InvoiceTbl.Columns("Force kWh").ColumnName = "ForcekWh"
        InvoiceTbl.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub



End Class
