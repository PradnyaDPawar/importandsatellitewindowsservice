﻿Public Class SseInvoice
    Implements ITransformDelegate
    Private _returnTable As DataTable
    Dim excel As New Excel()

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        excel.Load(fullPath)

        Dim xSheet As ExcelSheet = excel.Sheets(0)
        excel.GetSheet("Sheet1", xSheet)
        xSheet.HeaderRowIndex = 1

        _returnTable = xSheet.GetDataTable()

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        SseInvoiceSchema.ToDeFormat(_returnTable)
        For Each row As DataRow In _returnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next

    End Sub
End Class