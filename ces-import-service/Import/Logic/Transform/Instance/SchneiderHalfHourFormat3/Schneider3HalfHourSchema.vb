﻿Public Class Schneider3HalfHourSchema

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)

        dt = hhDataTable
    End Sub


    ''nikitah:refactoring code
    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim map As New Dictionary(Of String, DataRow)
        Dim dateholder, datetimeHolder As String
        Dim time As DateTime
        For Each row As DataRow In dt.Rows
            If Not String.IsNullOrEmpty(row("2")) Then
                datetimeHolder = row("2")
                time = datetimeHolder.ToString().Split(" ")(2)
                dateholder = datetimeHolder.ToString().Split(" ")(1)
                Dim timestring = time.ToString("HH:mm")
                For Each column As DataColumn In dt.Columns

                    Dim key As String = dateholder & row("1")
                    If map.ContainsKey(key) Then
                        Dim theRow As DataRow = map(key)
                        theRow(timestring) = row("3")
                    Else
                        Dim newRow As DataRow = hhDataTable.NewRow

                        newRow("Date(dd/mm/yyyy)") = dateholder
                        newRow("MeterSerial") = row("1")
                        newRow(timestring) = row("3")

                        hhDataTable.Rows.Add(newRow)
                        map.Add(key, newRow)
                    End If

                Next
            End If
        Next
        dt = hhDataTable
    End Sub

End Class
