﻿Public Class SchneiderRealTime2
    Implements ITransformDelegate



    Private _returnTable As DataTable

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel()

        excel.Load(fullPath)
        Dim xSheet As ExcelSheet = excel.Sheets(0)

        If Not excel.GetSheet("Tabular Report", xSheet) = True Then

            Throw New ApplicationException("Unable to find the Sheet1 sheet.")

        End If

        ReturnTable = xSheet.GetDataTable()
    End Sub

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        SchneiderRealTimeSchema2.ToDeFormat(ReturnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_RealTime.Fill(row, dsEdits, importEdit)
        Next

    End Sub
End Class
