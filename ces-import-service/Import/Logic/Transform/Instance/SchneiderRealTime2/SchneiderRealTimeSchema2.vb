﻿Public Class SchneiderRealTimeSchema2

    Public Shared Function ToDeFormat(ByRef dt As DataTable)
        DeleteRedundantRows(dt)
        DataTableTools.RemoveBlankColumns(dt)
        DataTableTools.SetHeaders(dt, 1)
        Dim rtDt As DataTable = DeFormatTableTemplates.CreateRealTimeTable
        CreateRealTimeEntries(dt, rtDt)
        dt = rtDt
        Return dt
    End Function

    Public Shared Sub DeleteRedundantRows(ByRef dt As DataTable)

        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            If Not row(0).ToString.ToLower.Trim.Replace(" ", "") = "timestamp" Then
                row.Delete()
            Else
                Exit For
            End If

        Next

    End Sub

    Public Shared Sub CreateRealTimeEntries(ByVal dt As DataTable, ByRef rtDt As DataTable)

        For Each column As DataColumn In dt.Columns

            If Not column.ColumnName.ToLower.Trim.Replace(" ", "") = "timestamp" Then

                For Each row As DataRow In dt.Rows

                    If IsDate(row(0)) Then

                        Dim newRtRow As DataRow = rtDt.NewRow

                        newRtRow("DateTime") = row(0)
                        newRtRow("MeterName") = column.ColumnName
                        newRtRow("CountUnits") = row(column.ColumnName)
                        rtDt.Rows.Add(newRtRow)

                    End If

                Next

            End If

        Next


    End Sub
End Class
