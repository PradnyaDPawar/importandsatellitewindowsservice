﻿

Public Class DigitalEnergyTariff
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel
        Dim xSheet As ExcelSheet
        excel.Load(fullPath)
        xSheet = excel.Sheets(0)
        excel.GetSheet("Tariff", xSheet)
        xSheet.HeaderRowIndex = 1
        _returnTable = xSheet.GetDataTable()
        If _returnTable.Rows.Count > 4000 Then
            Throw New ApplicationException("Exceed 4000 rows")
        End If
    End Sub

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        '' ColumnValidate()
        Transform(_returnTable)
        For Each row As DataRow In _returnTable.Rows
            FE_Tariff.Fill(row, dsEdits, importEdit)
        Next

    End Sub



End Class
