﻿Partial Public Class DigitalEnergyTariff

    Private Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("Meter Serial")
        headers.Add("MPAN")
        headers.Add("Tariff Name")
        headers.Add("Contract Name")
        headers.Add("Price")
        headers.Add("VAT(%)")
        Return headers
    End Function


    Private Sub ColumnValidate()
        Dim rawHeaders As List(Of String) = GetHeaders()

        ''Validation for required columns
        For Each header As String In rawHeaders
            If Not header = "MPAN" Then
                If Not _returnTable.Columns.Contains(header) Then
                    Throw New ApplicationException("Missing Column " & header & ". ")
                End If

                ''Validation for required column data
                For Each row As DataRow In _returnTable.Rows
                    If (IsDBNull(row(header)) Or row(header).ToString() = "") Then
                        Throw New ApplicationException("Missing data in column " + header)
                    End If


                    Select Case header 'Validation for data type
                        Case "Price", "VAT(%)"
                            If Not TypeOf (row(header)) Is Double Then
                                Throw New ApplicationException(header + " is not in the right format")
                            End If
                        Case Else
                            If Not TypeOf (row(header)) Is String Then
                                Throw New ApplicationException(header + " is not in the right format")
                            End If
                    End Select
                Next
            End If
        Next

    End Sub

    Private Shared Sub Transform(ByRef importData As DataTable)

        ' transform column headings to a standard name

        For Each col As DataColumn In importData.Columns
            col.ColumnName = col.ColumnName.Replace(" ", "").ToLower()
            Select Case col.ColumnName
                'Meter Serial
                Case "serial"
                    col.ColumnName = "meterserial"

                Case "serialnumber"
                    col.ColumnName = "meterserial"

                Case "meternumber"
                    col.ColumnName = "meterserial"

                    'Mpan
                Case "mprn"
                    col.ColumnName = "mpan"

                Case "mpan/mprn"
                    col.ColumnName = "mpan"

                Case "mpr"
                    col.ColumnName = "mpan"

                    'Vat
                Case "vat(%)"
                    col.ColumnName = "vat"

                    'Price
                Case "price(£)", "price"
                    col.ColumnName = "Price"


            End Select


        Next


    End Sub


End Class
