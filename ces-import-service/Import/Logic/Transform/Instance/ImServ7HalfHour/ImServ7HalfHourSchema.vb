﻿Imports System.Globalization

Public Class ImServ7HalfHourSchema
    Public Shared Sub ToDEFormat(ByRef dt As DataTable)
        AddColumns(dt)
        RenameColumns(dt)
    End Sub

    Public Shared Sub RenameColumns(ByRef dt As DataTable)
        dt.Columns("Meter Number").ColumnName = "MeterSerial"
        dt.Columns("Site Id").ColumnName = "MPAN"
        dt.Columns("Reading Date").ColumnName = "Date(DD/MM/YYYY)"
    End Sub

    Public Shared Sub AddColumns(ByRef dt As DataTable)
        dt.Columns.Add("ForcekWh")
    End Sub
End Class


