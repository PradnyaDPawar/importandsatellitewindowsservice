﻿Public Class BGElectric4Invoice
    Implements ITransformDelegate

    Private _returnTableDetail As DataTable
    Private _returnTableSummary As DataTable

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel()

        excel.Load(fullPath)

        Dim xSheet1 As ExcelSheet = excel.Sheets(0)
        If excel.GetSheet("Detail", xSheet1) Then
            xSheet1.HeaderRowIndex = 1

            _returnTableDetail = xSheet1.GetDataTable()
        Else
            Throw New ApplicationException("Unable to find the 'Detail' sheet.")
        End If

        Dim xSheet2 As ExcelSheet = excel.Sheets(1)
        If excel.GetSheet("Summary", xSheet2) Then
            xSheet2.HeaderRowIndex = 1

            _returnTableSummary = xSheet2.GetDataTable()
        Else
            Throw New ApplicationException("Unable to find the 'Summary' sheet.")
        End If

    End Sub

    Public Property ReturnTableDetails As DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTableDetail
        End Get
        Set(ByVal value As DataTable)
            _returnTableDetail = value
        End Set

    End Property


    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        DataTableTools.RemoveBlankRows(_returnTableDetail)
        DataTableTools.RemoveBlankRows(_returnTableSummary)

        ColumnDetailValidate()
        ColumnSummaryValidate()

        GetTransformedHeaders()

        For Each row As DataRow In _returnTableDetail.Rows

            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
