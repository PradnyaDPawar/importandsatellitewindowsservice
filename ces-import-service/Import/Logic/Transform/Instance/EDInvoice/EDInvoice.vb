﻿Public Class EDInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Try
            Dim excel As New Excel()
            Dim xSheet As ExcelSheet
            excel.Load(fullPath)

            xSheet = excel.Sheets(0)
            'xSheet.SetHeaderRow(GetHeaders(), ExcelSearchEnum.ContainsAll)
            'xSheet.HeaderRowIndex = 1
            _returnTable = xSheet.GetDataTable()

            'If Not xSheet.SetHeaderRow(GetHeaders(), ExcelSearchEnum.ContainsAll) Then

            'Return False

            'End If



            ' DeletePreHeaderRows(ReturnTable, xSheet)
        Catch ex As Exception
            ' Return False
        End Try

    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        DataTableTools.SetHeaders(ReturnTable, 1)

        EDInvoiceSchema.ToDeFormat(_returnTable)
        FillInvoice(_returnTable, dsEdits, importEdit)
        ' DataTableTools.RemoveBlankRows(ReturnTable)


        'GetTransformedHeaders()

        'For Each row As DataRow In _returnTable.Rows

        'FE_Invoice.Fill(row, dsEdits, importEdit)
        'Next
    End Sub
     Public Sub FillInvoice(ByVal ReturnTable As DataTable, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)
        For Each row As DataRow In ReturnTable.Rows
            FE_Invoice.Fill(row, dsEdits, ImportEdit)
        Next
    End Sub
End Class
