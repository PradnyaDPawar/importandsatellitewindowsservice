﻿Public Class EDInvoiceSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("Site UPRN")
        headers.Add("Sitename")
        headers.Add("Utility Code")
        headers.Add("Utility Type")
        headers.Add("Supplier")
        headers.Add("AccountNo")
        headers.Add("MPAN")
        headers.Add("UtilityKey")
        headers.Add("UtilityBillKey")
        headers.Add("Reading Type")
        headers.Add("Profile")
        headers.Add("InvoiceNo")
        headers.Add("InvoiceDate")
        headers.Add("BillingPeriodStartDate")
        headers.Add("BillingPeriodEndDate")
        headers.Add("Tariff001 Previous")
        headers.Add("Tariff001 Present")
        headers.Add("Tariff001 Usage")
        headers.Add("Tariff001 UnitCost")
        headers.Add("Tariff001 Cost")
        headers.Add("Tariff002 Previous")
        headers.Add("Tariff002 Present")
        headers.Add("Tariff002 Usage")
        headers.Add("Tariff002 UnitCost")
        headers.Add("Tariff002 Cost")
        headers.Add("Tariff003 Previous")
        headers.Add("Tariff003 Present")
        headers.Add("Tariff003 Usage")
        headers.Add("Tariff003 UnitCost")
        headers.Add("Tariff003 Cost")
        headers.Add("Tariff004 Previous")
        headers.Add("Tariff004 Present")
        headers.Add("Tariff004 Usage")
        headers.Add("Tariff004 UnitCost")
        headers.Add("Tariff004 Cost")
        headers.Add("Tariff005 Previous")
        headers.Add("Tariff005 Present")
        headers.Add("Tariff005 Usage")
        headers.Add("Tariff005 UnitCost")
        headers.Add("Tariff005 Cost")
        headers.Add("Tariff006 Previous")
        headers.Add("Tariff006 Present")
        headers.Add("Tariff006 Usage")
        headers.Add("Tariff006 UnitCost")
        headers.Add("Tariff006 Cost")
        headers.Add("TotalUtilityCost")
        headers.Add("StandingCharge")
        headers.Add("ClimateTax")
        headers.Add("OtherCharge")
        headers.Add("ExcessCharge")
        headers.Add("Net Amount")
        headers.Add("Tax")
        headers.Add("Gross Amount")
        Return headers
    End Function



    Shared Sub ToDeFormat(ByRef returnTableEDI As DataTable)

        FormatColumnHeaders(returnTableEDI)
        DataTableTools.RemoveBlankRows(returnTableEDI)
        Dim invoiceDataTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        RenameTaxAsVAT(returnTableEDI)
        CreateInvoiceEntries(returnTableEDI, invoiceDataTable)
        RenameColumns(invoiceDataTable)
        returnTableEDI = invoiceDataTable

    End Sub

    Private Shared Function FormatColumnHeaders(ByRef dt As DataTable) As DataTable
        For Each column As DataColumn In dt.Columns
            column.ColumnName = column.ColumnName.Trim.Replace(" ", "")
        Next
        Return dt
    End Function

    Private Shared Sub CreateInvoiceEntries(ByRef returnTableEDI As DataTable, ByRef invoiceDataTable As DataTable)
        For Each row As DataRow In returnTableEDI.Rows
            If Not (row("UtilityType").Trim = "Water") Then
                PopulateEntries(row, invoiceDataTable, "Tariff001Usage")
                PopulateEntries(row, invoiceDataTable, "Tariff002Usage")
                PopulateEntries(row, invoiceDataTable, "Tariff003Usage")
                PopulateEntries(row, invoiceDataTable, "Tariff004Usage")
                PopulateEntries(row, invoiceDataTable, "Tariff005Usage")
                PopulateEntries(row, invoiceDataTable, "Tariff006Usage")
               
            End If
            PopulateEntries(row, invoiceDataTable, "Tariff001Cost")
            PopulateEntries(row, invoiceDataTable, "Tariff002Cost")
            PopulateEntries(row, invoiceDataTable, "Tariff003Cost")
            PopulateEntries(row, invoiceDataTable, "Tariff004Cost")
            PopulateEntries(row, invoiceDataTable, "Tariff005Cost")
            PopulateEntries(row, invoiceDataTable, "Tariff006Cost")
            PopulateEntries(row, invoiceDataTable, "StandingCharge")
            PopulateEntries(row, invoiceDataTable, "ClimateTax")
            PopulateEntries(row, invoiceDataTable, "OtherCharge")
            PopulateEntries(row, invoiceDataTable, "ExcessCharge")
            PopulateEntries(row, invoiceDataTable, "VAT")
        Next

    End Sub
    Private Shared Sub PopulateEntries(dtRow As DataRow, invoiceDataTable As DataTable, ColName As String)
        Try
            If dtRow.Table.Columns.Contains(ColName) Then

                If Not String.IsNullOrEmpty(dtRow(ColName)) Then

                    Dim invoiceTableRow As DataRow = invoiceDataTable.NewRow

                    invoiceTableRow("Invoice Number") = dtRow("InvoiceNo")
                    invoiceTableRow("Meter Serial") = ""
                    invoiceTableRow("MPAN") = PopulateMPAN(dtRow)
                    invoiceTableRow("TaxPointDate") = Convert.ToDateTime(dtRow("InvoiceDate")).ToString("dd-MM-yy")
                    invoiceTableRow("Start Date") = Convert.ToDateTime(dtRow("BillingPeriodStartDate")).ToString("dd-MM-yy")
                    invoiceTableRow("End Date") = Convert.ToDateTime(dtRow("BillingPeriodEndDate")).ToString("dd-MM-yy")
                    invoiceTableRow("Start Read Estimated") = ""
                    invoiceTableRow("End Read Estimated") = PopulateEndReadEstimation(dtRow)
                    invoiceTableRow("Force kWh") = PopulateForceKwh(dtRow)
                    invoiceTableRow("VAT") = 0.0
                    invoiceTableRow("Cost Only") = PopulateCostOnly(ColName.Trim)
                    invoiceTableRow("Description") = ColName
                    invoiceTableRow("IsLastDayApportion") = "Yes"

                    If ColName.Contains("Usage") Then
                        PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, ColName, dtRow(ColName.Substring(0, ColName.Length - 5) + "Previous"), dtRow(ColName.Substring(0, ColName.Length - 5) + "Present"), dtRow(ColName), 0.0)
                    ElseIf (ColName = "StandingCharge") Or (ColName = "ClimateTax") Or (ColName = "OtherCharge") Or (ColName = "ExcessCharge") Then
                        PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, ColName, "", "", 0.0, dtRow(ColName))
                    ElseIf ColName = "VAT" Then
                        PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, ColName, "", "", 0.0, dtRow("VAT"))
                    ElseIf ColName.Contains("Cost") Then
                        PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, ColName, "", "", 0.0, dtRow(ColName))
                    End If
                    invoiceDataTable.Rows.Add(invoiceTableRow)
                End If
            End If
        Catch ex As Exception

        End Try
       
    End Sub
    Private Shared Sub PopulateEntriesBasedOnColName(ByRef invoiceTableRow As DataRow, ByRef dtRow As DataRow, ByRef Rate As String, ByVal StartRead As String _
                                                     , ByVal EndRead As String, ByVal Consumption As String, ByVal Cost As String)
        Try
            invoiceTableRow("Start Read") = StartRead
            invoiceTableRow("End Read") = EndRead
            invoiceTableRow("Consumption") = Consumption
            invoiceTableRow("Rate") = Rate

            If Rate = "VAT" Then
                invoiceTableRow("Net") = 0.0
                invoiceTableRow("VAT") = Cost
            Else
                invoiceTableRow("Net") = Cost
                invoiceTableRow("VAT") = 0.0
            End If
            invoiceTableRow("Gross") = Convert.ToDouble(invoiceTableRow("Net")) + Convert.ToDouble(invoiceTableRow("VAT"))
            'invoiceTableRow("Cost Only") = CostOnly
        Catch ex As Exception

        End Try
       
    End Sub
    Private Shared Sub PopulateEntriesBasedOnColName(ByRef invoiceTableRow As DataRow, ByRef dtRow As DataRow, ByVal StartRead As String, ByVal EndRead As String, ByVal Consumption As String, _
                                              ByVal Rate As String, ByVal Net As String, ByVal Vat As String, ByVal CostOnly As String)
        invoiceTableRow("Start Read") = StartRead
        invoiceTableRow("End Read") = EndRead
        invoiceTableRow("Consumption") = Consumption
        invoiceTableRow("Rate") = Rate
        invoiceTableRow("Net") = Net
        invoiceTableRow("Vat") = Vat
        invoiceTableRow("Gross") = Convert.ToDouble(invoiceTableRow("Net")) + Convert.ToDouble(invoiceTableRow("Vat"))
        invoiceTableRow("Cost Only") = CostOnly
    End Sub

    Private Shared Function CheckIfNumeric(ByVal Cost As String)
        If Cost = "" Then
            Return 0
        Else
            If IsNumeric(Cost) Then
                Return Cost
            Else
                Return 0
            End If
        End If
    End Function
    Private Shared Sub RenameColumns(invoiceDataTable As DataTable)
        invoiceDataTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceDataTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceDataTable.Columns("Rate").ColumnName = "Tariff"
        invoiceDataTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceDataTable.Columns("End Date").ColumnName = "EndDate"
        invoiceDataTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceDataTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceDataTable.Columns("End Read").ColumnName = "EndRead"
        invoiceDataTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceDataTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceDataTable.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub
    
    Private Shared Function PopulateMPAN(row As DataRow) As String
        Dim mpan As String = String.Empty
        Try

            'Check if meter utility type => water 
            If (row("UtilityType").Trim = "Water") Then
                mpan = row("AccountNo")
            Else
                If IsNumeric(row("MPAN")) Then
                    mpan = Convert.ToString(Decimal.Parse(row("MPAN"), Globalization.NumberStyles.Any))
                End If
                'Check if MPAN length>13 if yes => consider last 13 digits as MPAN
                If mpan.Length > 13 Then
                    mpan = mpan.Substring(mpan.Length - 13)
                End If
            End If
        Catch ex As Exception

        End Try

        Return mpan
    End Function
    Private Sub PopulateIsLastDayApportion(row As DataRow)
        row("IsLastDayApportion") = "Yes"
    End Sub
    Private Shared Function PopulateForceKwh(row As DataRow) As String
        Dim forcekWh As String = String.Empty
        'Check if meter utility type => water 
        If (row("UtilityType").Trim = "Water") Then
            forcekWh = "No"
        Else
            forcekWh = "Yes"
        End If
        Return forcekWh
    End Function

    Private Shared Function PopulateCostOnly(ByRef ColName As String) As String
        Dim costOnly As String = String.Empty
        If (ColName.Contains("Tariff")) Then
            If Not (ColName.Contains("Cost")) Then
                costOnly = "No"
            Else
                costOnly = "Yes"
            End If
        Else
            costOnly = "Yes"
        End If
        Return costOnly
    End Function

    Private Shared Function PopulateEndReadEstimation(ByVal row As DataRow) As String
        Dim ListActual As New List(Of String)(New String() {"actual", "computer", "customer"})
        Dim ListEstimated As New List(Of String)(New String() {"estimate (computer)", "estimate (manual)", "no reading"})
        Dim endReadEstimated As String = String.Empty
        ' End Read Estimated
        If ListActual.Contains(row("ReadingType").ToString.ToLower) Then
            endReadEstimated = "A"
        ElseIf ListEstimated.Contains(row("ReadingType").ToString.ToLower) Then
            endReadEstimated = "E"
        End If
        Return endReadEstimated
    End Function

    Private Shared Sub RenameTaxAsVAT(returnTableEDI As DataTable)
        returnTableEDI.Columns("Tax").ColumnName = "VAT"
    End Sub

End Class
