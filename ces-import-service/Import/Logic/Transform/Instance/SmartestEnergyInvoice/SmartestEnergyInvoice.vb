﻿Public Class SmartestEnergyInvoice
    Implements ITransformDelegate
    Private _returnTable As DataTable
    Dim excel As New Excel()


    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        excel.Load(fullPath)

    End Sub

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property


    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Dim SmartestEnergyInvoiceSchema As New SmartestEnergyInvoiceSchema
        SmartestEnergyInvoiceSchema.ToDeFormat(excel, _returnTable)
        For Each row As DataRow In _returnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub


End Class

