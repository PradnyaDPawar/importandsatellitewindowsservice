﻿Public Class SmartestEnergyInvoiceSchema

    Public Shared ListCostOnlyYes As New List(Of String)(New String() {"Climate Change Levy", "Availability Charge", "Standing Charge"})

    Private _grossDt As DataTable
    Property grossDt() As DataTable
        Get
            Return _grossDt
        End Get
        Set(ByVal value As DataTable)
            _grossDt = value
        End Set
    End Property
    Private _dt As DataTable
    Property dt() As DataTable
        Get
            Return _dt
        End Get
        Set(ByVal value As DataTable)
            _dt = value
        End Set
    End Property
    Public Shared _strNet As String
    Public Shared Property strNet() As String
        Get
            Return _strNet
        End Get
        Set(ByVal value As String)
            _strNet = value
        End Set
    End Property
    Public Shared _strVat As String
    Public Shared Property strVat() As String
        Get
            Return _strVat
        End Get
        Set(ByVal value As String)
            _strVat = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef excel As Excel, ByRef dt As DataTable)

        Dim xSheet As ExcelSheet = excel.Sheets(0)

        dt = SheetToDt(excel, xSheet, "Charge Lines", 6)
        grossDt = SheetToDt(excel, xSheet, "Summary Document", 23)


        dt.Rows.RemoveAt(0)
        dt.Rows.RemoveAt(0)

        Dim invoiceTbl As DataTable = DeFormatTableTemplates.CreateInvoiceTable


        For Each row As DataRow In grossDt.Rows
            If Not String.IsNullOrEmpty(row("Mpan")) Then
                PopulateVat(row, invoiceTbl)
                PopulateCharges(row, row("MPAN"), row("Invoice Number"), invoiceTbl, dt)
            End If
        Next

        RenameColumns(invoiceTbl)
        dt = invoiceTbl

    End Sub

    Public Sub PopulateVat(ByVal row As DataRow, ByRef invoiceTbl As DataTable)

        Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow

        Dim stdv, redv As Double

        'Add row into newInvoiTbl

        newInvoiceTblRow("Invoice Number") = row("Invoice Number")

        newInvoiceTblRow("Meter Serial") = ""

        newInvoiceTblRow("MPAN") = row("MPAN")

        newInvoiceTblRow("Start Date") = row("Charge Period Start")

        newInvoiceTblRow("End Date") = row("Charge Period End")

        newInvoiceTblRow("Consumption") = 0

        newInvoiceTblRow("Force Kwh") = "Yes"

        stdv = row("Invoice Standard VAT Amount").ToString.Replace("£", "")

        redv = row("Invoice Reduced VAT Amount").ToString.Replace("£", "")

        newInvoiceTblRow("Net") = 0

        newInvoiceTblRow("Vat") = stdv + redv

        newInvoiceTblRow("Gross") = stdv + redv

        newInvoiceTblRow("Description") = "Vat"

        newInvoiceTblRow("Cost Only") = "Yes"

        newInvoiceTblRow("TaxPointDate") = row("Invoice Date")

        newInvoiceTblRow("IsLastDayApportion") = "No"

        invoiceTbl.Rows.Add(newInvoiceTblRow)


    End Sub

    Public Sub PopulateCharges(ByVal row As DataRow, ByVal mpan As String, ByVal invoiceNumber As String, ByRef invoiceTbl As DataTable, ByRef dt As DataTable)

        Dim resultRows() As DataRow

        resultRows = dt.Select("[Invoice Number]='" & row("Invoice Number") & "'  and [Supply]='" & row("MPAN") & "'")

        For Each row In resultRows


            Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow

            'add row

            newInvoiceTblRow("Invoice Number") = row("Invoice Number")

            newInvoiceTblRow("Meter Serial") = ""

            newInvoiceTblRow("MPAN") = row("Supply")

            newInvoiceTblRow("Start Date") = row("Charge Period Start Date")

            newInvoiceTblRow("End Date") = row("Charge Period End Date")

            newInvoiceTblRow("Consumption") = row("Quantity")

            newInvoiceTblRow("Force Kwh") = "Yes"


            newInvoiceTblRow("Net") = row("Charge Amount").ToString.Replace("£", "")

            newInvoiceTblRow("Vat") = 0

            newInvoiceTblRow("Gross") = newInvoiceTblRow("Net")

            newInvoiceTblRow("Description") = row("Charge Description")

            newInvoiceTblRow("Cost Only") = IsCostOnly(row("Charge Description"))

            If newInvoiceTblRow("Cost Only") = "No" Then

                newInvoiceTblRow("Rate") = row("Charge Description")

            End If

            newInvoiceTblRow("TaxPointDate") = row("Invoice Date")

            newInvoiceTblRow("IsLastDayApportion") = "No"


            invoiceTbl.Rows.Add(newInvoiceTblRow)



        Next

    End Sub



    Public Shared Function IsCostOnly(ByVal value As String)

        If ListCostOnlyYes.Contains(value) Then

            Return "Yes"

        Else
            Return "No"

        End If


    End Function



    Private Function SheetToDt(ByRef excel As Excel, ByRef xSheet As ExcelSheet, ByVal sheetName As String, ByVal headerRowIndex As Integer) As DataTable

        Dim tempDt As New DataTable

        If excel.GetSheet(sheetName, xSheet) Then

            xSheet.HeaderRowIndex = headerRowIndex

            tempDt = xSheet.GetDataTable()

            tempDt.Rows.RemoveAt(0)

            DataTableTools.RemoveBlankColumns(tempDt)

            DataTableTools.RemoveBlankRows(tempDt)

        End If

        Return tempDt

    End Function

    Public Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        dt.Columns("Meter Serial").ColumnName = "MeterSerial"
        dt.Columns("Rate").ColumnName = "Tariff"
        dt.Columns("Start Date").ColumnName = "StartDate"
        dt.Columns("End Date").ColumnName = "EndDate"
        dt.Columns("Start Read").ColumnName = "StartRead"
        dt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        dt.Columns("End Read").ColumnName = "EndRead"
        dt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        dt.Columns("Consumption").ColumnName = "Consumption"
        dt.Columns("Force Kwh").ColumnName = "ForceKwh"
        dt.Columns("Cost Only").ColumnName = "CostOnly"



    End Sub

End Class
