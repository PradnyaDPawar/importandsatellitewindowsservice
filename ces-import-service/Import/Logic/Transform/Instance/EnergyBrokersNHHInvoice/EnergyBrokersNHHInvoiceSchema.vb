﻿Public Class EnergyBrokersNHHInvoiceSchema
    Public Shared Sub ToDeFormat(ByRef dt As DataTable)


        Dim dtresult As DataTable = PopulateColumns(dt)
        dt = dtresult



    End Sub
    Private Shared Function PopulateColumns(ByRef dt As DataTable) As DataTable
        dt.Columns("Read Date").ColumnName = "Start Date"
        dt.Columns("(£) 1 1 1").ColumnName = "Gross"
        dt.Rows.RemoveAt(dt.Rows.Count - 1)

        Dim standardInvoiceTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        For Each row As DataRow In dt.Rows
            Dim invoiceTableRow As DataRow = standardInvoiceTable.NewRow
            invoiceTableRow("Invoice Number") = row("No")
            invoiceTableRow("MPAN") = row("Core Mpan")
            invoiceTableRow("TaxPointDate") = IIf(String.IsNullOrEmpty(row("Invoice Date").ToString()), "", CDate(row("Invoice Date")).ToString("dd/MM/yyyy"))
            invoiceTableRow("Start Date") = IIf(String.IsNullOrEmpty(row("Start Date").ToString()), "", CDate(row("Start Date")).ToString("dd/MM/yyyy"))
            invoiceTableRow("End Date") = IIf(String.IsNullOrEmpty(row("Read Date 1").ToString()), "", CDate(row("Read Date 1")).ToString("dd/MM/yyyy"))
            invoiceTableRow("Force kWh") = "Yes"
            invoiceTableRow("Cost Only") = "No"
            invoiceTableRow("IsLastDayApportion") = "No"
            invoiceTableRow("Start Read") = row("Read From")
            invoiceTableRow("End Read") = row("Read To")
            invoiceTableRow("Start Read Estimated") = row("Type")
            invoiceTableRow("End Read Estimated") = row("Type 1")
            invoiceTableRow("Consumption") = row("Units Used")
            invoiceTableRow("Gross") = row("Gross").ToString().Replace("£", "")
            invoiceTableRow("Description") = "Day NHH Consumption"
            standardInvoiceTable.Rows.Add(invoiceTableRow)
        Next
        RenameColumns(standardInvoiceTable)
        Return standardInvoiceTable
    End Function
    Private Shared Sub RenameColumns(ByRef invoiceDataTable As DataTable)
        invoiceDataTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceDataTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceDataTable.Columns("Rate").ColumnName = "Tariff"
        invoiceDataTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceDataTable.Columns("End Date").ColumnName = "EndDate"
        invoiceDataTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceDataTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceDataTable.Columns("End Read").ColumnName = "EndRead"
        invoiceDataTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceDataTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceDataTable.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub




  

End Class
