﻿Partial Public Class NBMileageStaticVariable

    Public Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("Start Time")
        headers.Add("Start Date")
        headers.Add("VRN")
        headers.Add("Fleet No")
        headers.Add("Charge Station")
        headers.Add("kWh Start")
        headers.Add("kWh End")
        headers.Add("kWh Taken")
        headers.Add("Distance")
        headers.Add("Mins Charged")
        headers.Add("End Time")
        headers.Add("End Date")

        Return headers

    End Function


    Public Function Validate() As Boolean
        Return ValidateHeaders(GetHeaders())
    End Function

    Private Function ValidateHeaders(ByRef headers As List(Of String)) As Boolean
        For Each header As String In headers
            If Not _returnTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next
    End Function

    Public Sub ToDeFormat(ByRef dt As DataTable)

        'Add the required columns
        dt.Columns.Add("Level")
        dt.Columns.Add("Building_MeterSerial")
        dt.Columns.Add("StaticVariableName")
        dt.Columns.Add("kWhUnit")
        dt.Columns.Add("Unit")

        For Each row As DataRow In dt.Rows
            Dim datetimeHolder As DateTime
            Dim time As DateTime
            datetimeHolder = DateTime.ParseExact(row("End Date").ToString.Trim, "dd/MM/yy", System.Globalization.CultureInfo.InvariantCulture)
            time = DateTime.ParseExact(row("End Time").ToString.Trim, "HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture)
            datetimeHolder = datetimeHolder.Add(time.TimeOfDay)
            row("Building_MeterSerial") = row("VRN")
            row("Level") = "Meter"
            row("StaticVariableName") = row("VRN").ToString.Trim & "#" & datetimeHolder.ToString("yyyy-MM-dd HH:mm")
            row("Unit") = "Distance"
            row("kWhUnit") = 0
        Next


    End Sub


    Public Sub RenameColumns()
        _returnTable.Columns("Distance").ColumnName = "Value"
    End Sub



End Class
