﻿Partial Public Class BiffaHalfHourSchema2

    Public Shared Function GetHeaders() As List(Of String)

        Dim dateholder As Date = Today()
        Dim headers As New List(Of String)

        headers.Add("MPAN")
        headers.Add("BgMex")
        headers.Add("Date(dd/MM/yyyy)")

        For i = 0 To 47

            headers.Add(dateholder.AddMinutes(i * 30).ToString("HH:mm"))

        Next

        Return headers

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        AddColumns(dt)
        Return dt

    End Function
    Public Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("MeterSerial")

    End Sub

End Class
