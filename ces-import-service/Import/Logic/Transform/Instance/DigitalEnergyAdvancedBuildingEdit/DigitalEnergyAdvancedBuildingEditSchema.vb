﻿Public Class DigitalEnergyAdvancedBuildingEditSchema
    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        Dim deconfig As DeConfig = New DeConfig()
        Dim ImportVersion As String = deconfig.GetConfig().ImportVersion

        DataTableTools.SetHeaders(dt, 1)
        DataTableTools.RemoveBlankRows(dt)
        Dim bldgTable As DataTable = DeFormatTableTemplates.CreateAdvancedBuildingEditTable(ImportVersion)
        RenameColumns(dt)
        IsAllColumnExist(dt, bldgTable)
        populatetable(bldgTable, dt, ImportVersion)
        Return dt
    End Function

    Private Shared Sub IsAllColumnExist(ByRef dt As DataTable, ByRef bldgtable As DataTable)
        Dim dummy As DataTable = dt.Copy()
        Dim bldgtablecopy As DataTable = bldgtable.Copy()
        For Each dtcol As DataColumn In dummy.Columns
            dtcol.ColumnName = dtcol.ColumnName.ToString().Replace(" ", "")
        Next
        For i As Integer = 0 To bldgtablecopy.Columns.Count - 1
            If Not (dummy.Columns.Contains(bldgtablecopy.Columns(i).ColumnName.ToString().Replace(" ", ""))) Then
                Throw New Exception("Column" + " '" + bldgtablecopy.Columns(i).ColumnName + "' " + "is missing from the imported file. Please ensure this column is reinstated and try again")
            End If
        Next

    End Sub
    Private Shared Sub populatetable(ByRef bldg As DataTable, ByRef dt As DataTable, ByVal ImportVersion As String)
        For Each row As DataRow In dt.Rows
            Dim newRow As DataRow = bldg.NewRow
            newRow("UPRN") = row("UPRN")
            If ImportVersion <> "Vestigo" Then
                newRow("AssetOwner") = row("Asset Owner")
            Else
                newRow("BuildingOwner") = row("Building Owner")
            End If
            newRow("Tenant") = row("Tenant")
            newRow("FacilitiesManager") = row("Facilities Manager")
            newRow("EnergyChampion") = row("Energy Champion")
            newRow("MainSectorBenchmark") = row("Main Sector Benchmark")
            newRow("SubSectorBenchmark") = row("SubSector Benchmark")
            newRow("TotalFloorArea") = row("Total Floor Area")
            newRow("NumberOfOccupants") = row("Number Of Occupants")
            newRow("NumberOfPupils") = row("Number Of Pupils")
            newRow("AnnualHoursOfOccupancy") = row("Annual Hours Of Occupancy")
            newRow("MainHeatingSystem") = row("Main Heating System")
            newRow("MainHeatingFuel") = row("Main Heating Fuel")
            newRow("HeatingSetpoint") = row("Heating Set point")
            newRow("CoolingSetpoint") = row("Cooling Set point")
            newRow("PropertyRef") = row("Property Ref")
            newRow("BuiltDate") = row("Built Date")
            newRow("Subdivision") = row("Subdivision")
            newRow("EPCGrade") = row("EPC Grade")
            newRow("EPCRating") = row("EPC Rating")
            newRow("EPCOther") = row("EPC Other")
            newRow("Notes") = row("Notes")
            newRow("RateableValue") = row("Rateable Value")
            newRow("NetInternalArea") = row("Net Internal Area")
            newRow("Category") = row("Category")
            newRow("PropertyManager") = row("Property Manager")
            newRow("PropertySurveyor") = row("Property Surveyor")
            newRow("Administrator") = row("Administrator")
            If ImportVersion <> "Vestigo" Then
                newRow("AssetCategory") = row("Asset Category")
            Else
                newRow("BuildingCategory") = row("Building Category")
            End If

            newRow("BudgetCode") = row("Budget Code")
            newRow("Division") = row("Division")
            newRow("PropertyType") = row("Property Type")
            newRow("Service") = row("Service")
            bldg.Rows.Add(newRow)
        Next
        dt = bldg
    End Sub

    Private Shared Sub RenameColumns(dt As DataTable)
        dt.Columns("Total Floor Area (GIA)").ColumnName = "Total Floor Area"
        dt.Columns("Net Internal Area (NIA)").ColumnName = "Net Internal Area"
        dt.Columns("Heating Set point (°C)").ColumnName = "Heating Set point"
        dt.Columns("Cooling Set point (°C)").ColumnName = "Cooling Set point"
        'If dt.Columns.Contains("Asset Category") Then
        '    dt.Columns("Asset Category").ColumnName = "Building Category"
        'End If
        'If dt.Columns.Contains("Asset Owner") Then
        '    dt.Columns("Asset Owner").ColumnName = "Building Owner"
        'End If

    End Sub

End Class
