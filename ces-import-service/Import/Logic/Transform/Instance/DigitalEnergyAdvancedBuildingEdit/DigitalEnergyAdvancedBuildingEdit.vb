﻿Public Class DigitalEnergyAdvancedBuildingEdit
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel()
        excel.Load(fullPath)
        Dim xSheet As ExcelSheet = excel.Sheets(0)
        ReturnTable = xSheet.GetDataTable()
    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        DigitalEnergyAdvancedBuildingEditSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_AdvancedBuildingEdit.Fill(row, dsEdits, importEdit)
        Next

    End Sub
End Class
