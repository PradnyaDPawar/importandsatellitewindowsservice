﻿Public Class TotalGasPowerHalfHourSchema


    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("EntityType")
        headers.Add("EntityName")
        headers.Add("ConsumptionDate")
        headers.Add("ConsumptionPeriod")
        headers.Add("EnergyConsumption")

        Return headers

    End Function


    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        RenameColumns(dt)

        ' The incoming file has time entry as 2330 so convert to 23:30
        FormatTimeColumn(dt)

        'Create a new HH datatable in the digitalenergy format
        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()

        'Fill the hh table with consumption values
        CreateHalfHourlyEntries(dt, hhDataTable)

        dt = hhDataTable

    End Sub

    Private Shared Function GetMpan(ByRef dt As DataTable) As String

        For Each row As DataRow In dt.Rows

            If row("EntityType") = "MPR" Then
                Return row("EntityName")
            End If

        Next

        Return "Mpan Not Found"

    End Function

    Private Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("ConsumptionDate").ColumnName = "Date"
        dt.Columns("ConsumptionPeriod").ColumnName = "Time"
        dt.Columns("EnergyConsumption").ColumnName = "Consumption"

    End Sub

    Private Shared Sub PopulateColumns(ByRef hhDataTable As DataTable, ByVal mpan As String)

        For Each row As DataRow In hhDataTable.Rows

            row("MPAN") = mpan
            row("Force kWh") = "Yes"

        Next

    End Sub


    Private Shared Sub FormatTimeColumn(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows

            If IsDate(row("Date")) Then
                row("Time") = row("Time").ToString.Insert(2, ":")
            End If

        Next

    End Sub

    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim dateholder As DateTime
        Dim timeHolder As DateTime
        Dim success As Boolean = False
        For Each row As DataRow In dt.Rows

            If IsDate(row("Date")) Then
                timeHolder = row("Time")

                Dim timeHolderString = timeHolder.ToString("HH:mm")
                If hhDataTable.Columns.Contains(timeHolderString) Then

                    dateholder = row("Date")
                    For Each hhrow As DataRow In hhDataTable.Rows
                        If hhrow("Date (dd/mm/yyyy)") = dateholder.Date And hhrow("MPAN") = row("EntityName") Then
                            hhrow(timeHolderString) = row("Consumption")
                            success = True
                        End If
                    Next

                    If Not success Then
                        Dim newRow As DataRow = hhDataTable.NewRow
                        newRow("Date (dd/mm/yyyy)") = dateholder.Date
                        newRow(timeHolderString) = row("Consumption")
                        newRow("MPAN") = row("EntityName")
                        newRow("Force Kwh") = "Yes"
                        hhDataTable.Rows.Add(newRow)
                    End If
                    success = False
                End If
            End If

        Next


    End Sub

End Class
