﻿Public Class TotalGasPowerHalfHour
    Implements ITransformDelegate


    Public Function Invoke(ByVal stagingFilePath As String, ByRef returnTable As System.Data.DataTable) As Boolean Implements ITransformDelegate.Invoke

        Try

            Dim csvfile As New DelimitedFile

            csvfile.Load(stagingFilePath, TotalGasPowerHalfHourSchema.GetHeaders())

            returnTable = csvfile.ToDataTable()

            TotalGasPowerHalfHourSchema.ToDeFormat(returnTable)

            returnTable.TableName = "Half Hour Format 2"

            Return True

        Catch ex As Exception

            Return False

        End Try

    End Function
End Class
