﻿Imports System.Globalization

Partial Public Class Siemens3HalfHour

    'Public Function GetHeaders() As List(Of String)

    '    Dim headers As New List(Of String)

    '    headers.Add("MeterSerial")
    '    headers.Add("Unit")
    '    headers.Add("Date(DD/MM/YYYY)")
    '    headers.Add("00:00")
    '    headers.Add("00:30")
    '    headers.Add("01:00")
    '    headers.Add("01:30")
    '    headers.Add("02:00")
    '    headers.Add("02:30")
    '    headers.Add("03:00")
    '    headers.Add("03:30")
    '    headers.Add("04:00")
    '    headers.Add("04:30")
    '    headers.Add("05:00")
    '    headers.Add("05:30")
    '    headers.Add("06:00")
    '    headers.Add("06:30")
    '    headers.Add("07:00")
    '    headers.Add("07:30")
    '    headers.Add("08:00")
    '    headers.Add("08:30")
    '    headers.Add("09:00")
    '    headers.Add("09:30")
    '    headers.Add("10:00")
    '    headers.Add("10:30")
    '    headers.Add("11:00")
    '    headers.Add("11:30")
    '    headers.Add("12:00")
    '    headers.Add("12:30")
    '    headers.Add("13:00")
    '    headers.Add("13:30")
    '    headers.Add("14:00")
    '    headers.Add("14:30")
    '    headers.Add("15:00")
    '    headers.Add("15:30")
    '    headers.Add("16:00")
    '    headers.Add("16:30")
    '    headers.Add("17:00")
    '    headers.Add("17:30")
    '    headers.Add("18:00")
    '    headers.Add("18:30")
    '    headers.Add("19:00")
    '    headers.Add("19:30")
    '    headers.Add("20:00")
    '    headers.Add("20:30")
    '    headers.Add("21:00")
    '    headers.Add("21:30")
    '    headers.Add("22:00")
    '    headers.Add("22:30")
    '    headers.Add("23:00")
    '    headers.Add("23:30")

    '    Return headers

    'End Function

    Private Sub RenameColumns()
        _returnTable.Columns("METER/GROUP NAME").ColumnName = "MeterSerial"
        _returnTable.Columns("UOM").ColumnName = "Unit"
        _returnTable.Columns("DATE/COMPARE DATE").ColumnName = "Date(DD/MM/YYYY)"
    End Sub


    Private Sub AddColumns()
        _returnTable.Columns.Add("ForcekWh")
        _returnTable.Columns.Add("MPAN")
    End Sub

    Private Sub ConvertToBritishDate(ByRef row As DataRow)
        row("Date(DD/MM/YYYY)") = Date.ParseExact(row("Date(DD/MM/YYYY)"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy")
    End Sub

    Private Sub FillForceKwh(ByRef row As DataRow)
        If row("Unit").ToString.ToLower.Trim.Contains("kwh") Then
            row("ForcekWh") = "Yes"
        Else
            row("ForcekWh") = "No"
        End If
    End Sub

End Class
