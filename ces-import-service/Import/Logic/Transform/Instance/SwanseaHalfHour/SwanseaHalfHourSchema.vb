﻿Public Class SwanseaHalfHourSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("MPAN")
        headers.Add("Reading Date")
        headers.Add("Units")
        For count As Integer = 1 To 48
            headers.Add(count.ToString)
        Next

        Return headers

    End Function

    Public Shared Function Validate(ByRef dt As DataTable) As Boolean


        For Each header As String In GetHeaders()
            If Not dt.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next


    End Function


    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable


        RenameColumns(dt)
        CreateColumns(dt)
        PopulateColumns(dt)
        ReformatDateColumn(dt)
        DeleteRedundantColumns(dt)

        Return dt

    End Function

    Private Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("MPAN").ColumnName = "MeterSerial"
        dt.Columns("Reading Date").ColumnName = "Date(dd/mm/yyyy)"
        RenameHalfHourlyEntries(dt)

    End Sub

    Private Shared Sub CreateColumns(ByRef dt As DataTable)

        dt.Columns.Add("MPAN")
        dt.Columns.Add("ForcekWh")

    End Sub

    Private Shared Sub PopulateColumns(ByRef dt As DataTable)

        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            If row("Units").ToString.Trim.ToLower() = "kwh" Then
                row("ForcekWh") = "Yes"
            Else
                row("ForcekWh") = "No"
            End If

        Next

    End Sub


    Private Shared Sub ReformatDateColumn(ByRef dt As DataTable)

        Dim dateholder As DateTime

        For Each row As DataRow In dt.Rows

            dateholder = row("Date(dd/mm/yyyy)")
            row("Date(dd/mm/yyyy)") = dateholder.ToShortDateString

        Next


    End Sub

    Private Shared Sub RenameHalfHourlyEntries(ByRef dt As DataTable)

        Dim dateholder As Date = DateTime.Today

        For columncount As Integer = 1 To 48

            dt.Columns(columncount.ToString).ColumnName = dateholder.ToString("HH:mm")

            dateholder = dateholder.AddMinutes(30)

        Next

    End Sub

    Private Shared Sub DeleteRedundantColumns(ByRef dt As DataTable)

        For i As Integer = 49 To dt.Columns.Count - 1
            If dt.Columns.Contains(i.ToString) Then
                dt.Columns.Remove(i.ToString)
            End If
        Next

    End Sub


End Class
