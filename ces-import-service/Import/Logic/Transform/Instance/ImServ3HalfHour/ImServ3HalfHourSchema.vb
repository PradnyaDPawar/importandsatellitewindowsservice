﻿Partial Public Class ImServ3HalfHour

   
    Public Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")

    End Sub

    Public Sub RenameColumnNames(ByRef dt As DataTable)

        dt.Columns("MPRN").ColumnName = "MPAN"
        dt.Columns("SerialNum").ColumnName = "MeterSerial"
        dt.Columns("Date").ColumnName = "Date(dd/MM/yyyy)"

    End Sub

    Public Sub RemoveUnwantedColumns(ByRef dt As DataTable)

        dt.Columns.Remove("UtilityType")
        dt.Columns.Remove("Reading")

    End Sub

    Public Sub PopulateForceKwhColumn(ByRef row As DataRow)

        If row("Unit").ToString.ToLower = "kwh" Then
            row("ForcekWh") = "Yes"
        Else
            row("ForcekWh") = "No"
        End If

    End Sub

End Class
