﻿Public Class TMacRealTime
    Implements ITransformDelegate


    Public Function Invoke(ByVal stagingFilePath As String, ByRef returnTable As System.Data.DataTable) As Boolean Implements ITransformDelegate.Invoke

        Try

            Dim csvFile As New DelimitedFile
            csvFile.Load(stagingFilePath)
            returnTable = csvFile.ToDataTable(False)
            TMacRealTimeSchema.ToDeFormat(returnTable)
            returnTable.TableName = "realtime"
            Return True

        Catch ex As Exception

            Return False

        End Try

    End Function
End Class
