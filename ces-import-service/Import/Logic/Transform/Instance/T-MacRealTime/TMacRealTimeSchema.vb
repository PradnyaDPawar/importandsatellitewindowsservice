﻿Public Class TMacRealTimeSchema

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        DataTableTools.SetHeaders(dt, 1)

        Dim realTimeDt As DataTable
        realTimeDt = DeFormatTableTemplates.CreateRealTimeTable()


        For Each row As DataRow In dt.Rows


            For Each column As DataColumn In dt.Columns

                If column.ColumnName <> "Time" Then

                    Dim realTimeDtRow As DataRow = realTimeDt.NewRow

                    realTimeDtRow("MeterName") = column.ColumnName.Substring(1, column.ColumnName.IndexOf("]") - 1)
                    realTimeDtRow("TimeStamp") = row("Time")
                    realTimeDtRow("Value") = row(column)

                    realTimeDt.Rows.Add(realTimeDtRow)

                End If

            Next

        Next

        dt = realTimeDt

    End Sub

End Class
