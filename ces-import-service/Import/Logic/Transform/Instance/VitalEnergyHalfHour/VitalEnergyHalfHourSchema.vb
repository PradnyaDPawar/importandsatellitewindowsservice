﻿Public Class VitalEnergyHalfHourSchema




    Public Shared Function ToDEFormat(ByRef tdt As DataTable)
        'Create a new Data Table that is compatible with DigitalEnergy
        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        DeleteFirstColumn(tdt)
        DataTableTools.RemoveBlankColumns(tdt)
        DataTableTools.RemoveBlankRows(tdt)
        NameBlankColumns(tdt)
        DataTableTools.SetHeaders(tdt, 1)
        CreateColumns(tdt)
        PopulateNewColumns(tdt)
        'Use all the values from the dt table and insert them into the hhDataTable
        'Brings back 'dt' Datatable which is now in the same format as 'hhDataTable' Datatable
        DeleteFirstTwoRows(tdt)
        PopulateColumns(tdt, hhDataTable)


        Return tdt
    End Function
    ''nikitah:refactored code
    Private Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim map As New Dictionary(Of String, DataRow)
        Dim theDate As DateTime         'Stores the Date from the dt DataTable
        Dim theTime As DateTime         'Stores the Time from the dt DataTable
        Dim mpan As String = String.Empty

        For Each row As DataRow In dt.Rows
            For Each column As DataColumn In dt.Columns
                If IsDate(column.ColumnName) Then
                    '>> Collect date and time values from the current row being processed for the next stage
                    theDate = column.ColumnName
                    theTime = theDate.ToString("HH:mm") 'Converts the dateTime to a time value
                    Dim theTimeString = theTime.ToString("HH:mm") 'Creates a string version of time


                    Dim key As String = row("MeterSerial") & theDate.Date
                    If map.ContainsKey(key) Then

                        Dim theRow As DataRow = map(key)
                        theRow(theTimeString) = row(column)
                    Else
                        Dim newRow As DataRow = hhDataTable.NewRow      ' Create a new row
                        newRow("MeterSerial") = row("MeterSerial")    ' Add the Meter Serial Number
                        newRow("Date(DD/MM/YYYY)") = theDate.Date      ' Add the date
                        newRow(theTimeString) = row(column)      ' And add the Consumption under the correct Time Column in the row.
                        newRow("ForcekWh") = row("ForcekWh")
                        hhDataTable.Rows.Add(newRow)                    ' Add the new row to the Digital Energy Formatted Table.
                        map.Add(key, newRow)
                    End If

                End If

            Next

        Next
        dt = hhDataTable
    End Sub
    Private Shared Sub NameBlankColumns(ByRef tdt As DataTable)
        Dim numbertoappend As Integer = 1

        For Each column As DataColumn In tdt.Columns

            If tdt.Rows(0)(column) = "" Then
                tdt.Rows(0)(column) = "Column 0" & numbertoappend
            End If


        Next

    End Sub

    Private Shared Sub CreateColumns(ByRef tdt As DataTable)

        tdt.Columns.Add("Date(DD/MM/YYYY)")
        tdt.Columns.Add("MPAN")
        tdt.Columns.Add("ForcekWh")
        tdt.Columns.Add("MeterSerial")
        tdt.Columns.Add("Unit")
    End Sub

    Private Shared Sub DeleteFirstColumn(ByRef tdt As DataTable)

        tdt.Columns.RemoveAt(0)

    End Sub
    Private Shared Sub PopulateNewColumns(ByRef tdt As DataTable)
        For Each row As DataRow In tdt.Rows
            PopulateForceKwh(row)
            PopulateMeterSerial(row)
        Next
    End Sub

    Private Shared Sub PopulateMeterSerial(ByRef row As DataRow)

        Dim str As String = row("NGH.TimeStamp").ToString
        If (str.Contains("(")) Then
            Dim meterserial As String = str.Substring(0, str.IndexOf("("))
            row("MeterSerial") = meterserial
        Else
            row("MeterSerial") = str
        End If
    End Sub

    Private Shared Sub PopulateMPAN(ByRef row As DataRow)

        Dim str As String = row("NGH.TimeStamp").ToString

        Dim mpan As String = (str.Substring((str.IndexOf(".") + 1), str.IndexOf("-") - (str.IndexOf(".") + 1))).Trim
        row("MPAN") = mpan
    End Sub

    Private Shared Sub PopulateForceKwh(ByRef row As DataRow)

        Dim str As String = row("NGH.TimeStamp").ToString
        Dim consumptionunit As String = String.Empty
        If (str.Contains("(")) Then
            consumptionunit = str.Substring((str.IndexOf("(") + 1), str.IndexOf(")") - (str.IndexOf("(") + 1)).Trim
            If consumptionunit = "kWh" Then
                row("ForcekWh") = "Yes"
            Else
                row("ForcekWh") = "No"
            End If
        Else
            row("ForcekWh") = "No"
        End If
        row("Unit") = consumptionunit
    End Sub

    Public Shared Function Validate(ByRef tdt As DataTable) As Boolean

        Dim listofdates As New List(Of Date)
        listofdates = GetAllDates(tdt)
        Dim listofdatetime As List(Of DateTime)
        listofdatetime = GetDateTimeEntries(listofdates)

        Dim ValidFlag As Boolean = False

        For Each datetimeentry As DateTime In listofdatetime

            For Each row As DataRow In tdt.Rows
                If IsDate(row(0)) Then
                    If datetimeentry = row(0) Then
                        ValidFlag = True
                    End If
                End If
            Next

            If Not ValidFlag Then
                Return False
            End If

            ValidFlag = False
        Next

        Return True

    End Function

    Private Shared Function GetAllDates(ByRef tdt As DataTable) As List(Of Date)
        Dim listofdates As New List(Of Date)
        Dim datetocheck As Date

        For Each row As DataRow In tdt.Rows

            If IsDate(row(0)) Then
                datetocheck = DateTime.Parse(row(0))
                If Not listofdates.Contains(datetocheck.Date) Then
                    listofdates.Add(datetocheck.Date)
                End If
            End If

        Next
        Return listofdates
    End Function


    Private Shared Function GetDateTimeEntries(ByVal dateEntries As List(Of DateTime)) As List(Of DateTime)
        Dim dateTimeEntries As New List(Of DateTime)

        For Each dateentry As DateTime In dateEntries
            dateTimeEntries.Add(dateentry)
            For addCount As Integer = 1 To 22
                dateentry = dateentry.AddMinutes(30)
                dateTimeEntries.Add(dateentry)
            Next
        Next

        Return dateTimeEntries
    End Function

    Private Shared Sub DeleteFirstTwoRows(ByRef tdt As DataTable)
        'First two rows are not required hence delete from resultset
        For deleteCounter As Integer = 0 To 1
            tdt.Rows(deleteCounter).Delete()
        Next
        tdt.AcceptChanges()
    End Sub

    Public Shared Sub GetHHTableRow(ByRef row As DataRow)
        Dim objVitalEnergyHHData As New VitalEnergyHHData()
        objVitalEnergyHHData._itemRow = row

        objVitalEnergyHHData.hhTemp = IIf(row("00:00") Is DBNull.Value OrElse row("00:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("00:00")), 0.0, row("00:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0000 = IIf(row("00:30") Is DBNull.Value OrElse row("00:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("00:30")), 0.0, row("00:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0030 = IIf(row("01:00") Is DBNull.Value OrElse row("01:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("01:00")), 0.0, row("01:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0100 = IIf(row("01:30") Is DBNull.Value OrElse row("01:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("01:30")), 0.0, row("01:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0130 = IIf(row("02:00") Is DBNull.Value OrElse row("02:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("02:00")), 0.0, row("02:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0200 = IIf(row("02:30") Is DBNull.Value OrElse row("02:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("02:30")), 0.0, row("02:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0230 = IIf(row("03:00") Is DBNull.Value OrElse row("03:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("03:00")), 0.0, row("03:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0300 = IIf(row("03:30") Is DBNull.Value OrElse row("03:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("03:30")), 0.0, row("03:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0330 = IIf(row("04:00") Is DBNull.Value OrElse row("04:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("04:00")), 0.0, row("04:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0400 = IIf(row("04:30") Is DBNull.Value OrElse row("04:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("04:30")), 0.0, row("04:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0430 = IIf(row("05:00") Is DBNull.Value OrElse row("05:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("05:00")), 0.0, row("05:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0500 = IIf(row("05:30") Is DBNull.Value OrElse row("05:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("05:30")), 0.0, row("05:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0530 = IIf(row("06:00") Is DBNull.Value OrElse row("06:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("06:00")), 0.0, row("06:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0600 = IIf(row("06:30") Is DBNull.Value OrElse row("06:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("06:30")), 0.0, row("06:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0630 = IIf(row("07:00") Is DBNull.Value OrElse row("07:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("07:00")), 0.0, row("07:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0700 = IIf(row("07:30") Is DBNull.Value OrElse row("07:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("07:30")), 0.0, row("07:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0730 = IIf(row("08:00") Is DBNull.Value OrElse row("08:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("08:00")), 0.0, row("08:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0800 = IIf(row("08:30") Is DBNull.Value OrElse row("08:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("08:30")), 0.0, row("08:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0830 = IIf(row("09:00") Is DBNull.Value OrElse row("09:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("09:00")), 0.0, row("09:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0900 = IIf(row("09:30") Is DBNull.Value OrElse row("09:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("09:30")), 0.0, row("09:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh0930 = IIf(row("10:00") Is DBNull.Value OrElse row("10:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("10:00")), 0.0, row("10:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1000 = IIf(row("10:30") Is DBNull.Value OrElse row("10:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("10:30")), 0.0, row("10:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1030 = IIf(row("11:00") Is DBNull.Value OrElse row("11:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("11:00")), 0.0, row("11:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1100 = IIf(row("11:30") Is DBNull.Value OrElse row("11:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("11:30")), 0.0, row("11:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1130 = IIf(row("12:00") Is DBNull.Value OrElse row("12:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("12:00")), 0.0, row("12:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1200 = IIf(row("12:30") Is DBNull.Value OrElse row("12:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("12:30")), 0.0, row("12:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1230 = IIf(row("13:00") Is DBNull.Value OrElse row("13:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("13:00")), 0.0, row("13:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1300 = IIf(row("13:30") Is DBNull.Value OrElse row("13:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("13:30")), 0.0, row("13:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1330 = IIf(row("14:00") Is DBNull.Value OrElse row("14:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("14:00")), 0.0, row("14:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1400 = IIf(row("14:30") Is DBNull.Value OrElse row("14:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("14:30")), 0.0, row("14:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1430 = IIf(row("15:00") Is DBNull.Value OrElse row("15:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("15:00")), 0.0, row("15:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1500 = IIf(row("15:30") Is DBNull.Value OrElse row("15:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("15:30")), 0.0, row("15:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1530 = IIf(row("16:00") Is DBNull.Value OrElse row("16:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("16:00")), 0.0, row("16:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1600 = IIf(row("16:30") Is DBNull.Value OrElse row("16:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("16:30")), 0.0, row("16:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1630 = IIf(row("17:00") Is DBNull.Value OrElse row("17:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("17:00")), 0.0, row("17:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1700 = IIf(row("17:30") Is DBNull.Value OrElse row("17:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("17:30")), 0.0, row("17:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1730 = IIf(row("18:00") Is DBNull.Value OrElse row("18:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("18:00")), 0.0, row("18:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1800 = IIf(row("18:30") Is DBNull.Value OrElse row("18:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("18:30")), 0.0, row("18:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1830 = IIf(row("19:00") Is DBNull.Value OrElse row("19:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("19:00")), 0.0, row("19:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1900 = IIf(row("19:30") Is DBNull.Value OrElse row("19:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("19:30")), 0.0, row("19:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh1930 = IIf(row("20:00") Is DBNull.Value OrElse row("20:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("20:00")), 0.0, row("20:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh2000 = IIf(row("20:30") Is DBNull.Value OrElse row("20:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("20:30")), 0.0, row("20:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh2030 = IIf(row("21:00") Is DBNull.Value OrElse row("21:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("21:00")), 0.0, row("21:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh2100 = IIf(row("21:30") Is DBNull.Value OrElse row("21:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("21:30")), 0.0, row("21:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh2130 = IIf(row("22:00") Is DBNull.Value OrElse row("22:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("22:00")), 0.0, row("22:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh2200 = IIf(row("22:30") Is DBNull.Value OrElse row("22:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("22:30")), 0.0, row("22:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh2230 = IIf(row("23:00") Is DBNull.Value OrElse row("23:00") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("23:00")), 0.0, row("23:00"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh2300 = IIf(row("23:30") Is DBNull.Value OrElse row("23:30") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("23:30")), 0.0, row("23:30"))), 2).ToString.Replace(",", ""))
        objVitalEnergyHHData.hh2330 = IIf(row("Temp") Is DBNull.Value OrElse row("Temp") = Nothing, DBNull.Value, Math.Round(Convert.ToDouble(IIf(String.IsNullOrEmpty(row("Temp")), 0.0, row("Temp"))), 2).ToString.Replace(",", ""))
    End Sub

    Public Shared Function GetHHTable(ByVal dataTable As DataTable)
        dataTable.Columns.Add("Temp")
        For Each row As DataRow In dataTable.Rows
            GetHHTableRow(row)
        Next
        dataTable.Columns.Remove("Temp")
        Return dataTable
    End Function
End Class

Public Class VitalEnergyHHData
    Protected _dtRow As DataRow
    Property _itemRow() As DataRow
        Get
            Return _dtRow
        End Get
        Set(ByVal value As DataRow)
            _dtRow = value
        End Set
    End Property
#Region "Property"
    WriteOnly Property hhTemp() As Object
        Set(ByVal value As Object)
            _itemRow("Temp") = value
        End Set
    End Property
    WriteOnly Property hh0000() As Object
        Set(ByVal value As Object)
            _itemRow("00:00") = value
        End Set
    End Property

    WriteOnly Property hh0030() As Object
        Set(ByVal value As Object)
            _itemRow("00:30") = value
        End Set
    End Property

    WriteOnly Property hh0100() As Object
        Set(ByVal value As Object)
            _itemRow("01:00") = value
        End Set
    End Property

    WriteOnly Property hh0130() As Object
        Set(ByVal value As Object)
            _itemRow("01:30") = value
        End Set
    End Property

    WriteOnly Property hh0200() As Object
        Set(ByVal value As Object)
            _itemRow("02:00") = value
        End Set
    End Property

    WriteOnly Property hh0230() As Object
        Set(ByVal value As Object)
            _itemRow("02:30") = value
        End Set
    End Property

    WriteOnly Property hh0300() As Object
        Set(ByVal value As Object)
            _itemRow("03:00") = value
        End Set
    End Property

    WriteOnly Property hh0330() As Object
        Set(ByVal value As Object)
            _itemRow("03:30") = value
        End Set
    End Property

    WriteOnly Property hh0400() As Object
        Set(ByVal value As Object)
            _itemRow("04:00") = value
        End Set
    End Property

    WriteOnly Property hh0430() As Object
        Set(ByVal value As Object)
            _itemRow("04:30") = value
        End Set
    End Property

    WriteOnly Property hh0500() As Object
        Set(ByVal value As Object)
            _itemRow("05:00") = value
        End Set
    End Property

    WriteOnly Property hh0530() As Object
        Set(ByVal value As Object)
            _itemRow("05:30") = value
        End Set
    End Property

    WriteOnly Property hh0600() As Object
        Set(ByVal value As Object)
            _itemRow("06:00") = value
        End Set
    End Property

    WriteOnly Property hh0630() As Object
        Set(ByVal value As Object)
            _itemRow("06:30") = value
        End Set
    End Property

    WriteOnly Property hh0700() As Object
        Set(ByVal value As Object)
            _itemRow("07:00") = value
        End Set
    End Property

    WriteOnly Property hh0730() As Object
        Set(ByVal value As Object)
            _itemRow("07:30") = value
        End Set
    End Property

    WriteOnly Property hh0800() As Object
        Set(ByVal value As Object)
            _itemRow("08:00") = value
        End Set
    End Property

    WriteOnly Property hh0830() As Object
        Set(ByVal value As Object)
            _itemRow("08:30") = value
        End Set
    End Property

    WriteOnly Property hh0900() As Object
        Set(ByVal value As Object)
            _itemRow("09:00") = value
        End Set
    End Property

    WriteOnly Property hh0930() As Object
        Set(ByVal value As Object)
            _itemRow("09:30") = value
        End Set
    End Property

    WriteOnly Property hh1000() As Object
        Set(ByVal value As Object)
            _itemRow("10:00") = value
        End Set
    End Property

    WriteOnly Property hh1030() As Object
        Set(ByVal value As Object)
            _itemRow("10:30") = value
        End Set
    End Property

    WriteOnly Property hh1100() As Object
        Set(ByVal value As Object)
            _itemRow("11:00") = value
        End Set
    End Property

    WriteOnly Property hh1130() As Object
        Set(ByVal value As Object)
            _itemRow("11:30") = value
        End Set
    End Property

    WriteOnly Property hh1200() As Object
        Set(ByVal value As Object)
            _itemRow("12:00") = value
        End Set
    End Property

    WriteOnly Property hh1230() As Object
        Set(ByVal value As Object)
            _itemRow("12:30") = value
        End Set
    End Property

    WriteOnly Property hh1300() As Object
        Set(ByVal value As Object)
            _itemRow("13:00") = value
        End Set
    End Property

    WriteOnly Property hh1330() As Object
        Set(ByVal value As Object)
            _itemRow("13:30") = value
        End Set
    End Property

    WriteOnly Property hh1400() As Object
        Set(ByVal value As Object)
            _itemRow("14:00") = value
        End Set
    End Property

    WriteOnly Property hh1430() As Object
        Set(ByVal value As Object)
            _itemRow("14:30") = value
        End Set
    End Property

    WriteOnly Property hh1500() As Object
        Set(ByVal value As Object)
            _itemRow("15:00") = value
        End Set
    End Property

    WriteOnly Property hh1530() As Object
        Set(ByVal value As Object)
            _itemRow("15:30") = value
        End Set
    End Property

    WriteOnly Property hh1600() As Object
        Set(ByVal value As Object)
            _itemRow("16:00") = value
        End Set
    End Property

    WriteOnly Property hh1630() As Object
        Set(ByVal value As Object)
            _itemRow("16:30") = value
        End Set
    End Property

    WriteOnly Property hh1700() As Object
        Set(ByVal value As Object)
            _itemRow("17:00") = value
        End Set
    End Property

    WriteOnly Property hh1730() As Object
        Set(ByVal value As Object)
            _itemRow("17:30") = value
        End Set
    End Property

    WriteOnly Property hh1800() As Object
        Set(ByVal value As Object)
            _itemRow("18:00") = value
        End Set
    End Property

    WriteOnly Property hh1830() As Object
        Set(ByVal value As Object)
            _itemRow("18:30") = value
        End Set
    End Property

    WriteOnly Property hh1900() As Object
        Set(ByVal value As Object)
            _itemRow("19:00") = value
        End Set
    End Property

    WriteOnly Property hh1930() As Object
        Set(ByVal value As Object)
            _itemRow("19:30") = value
        End Set
    End Property

    WriteOnly Property hh2000() As Object
        Set(ByVal value As Object)
            _itemRow("20:00") = value
        End Set
    End Property

    WriteOnly Property hh2030() As Object
        Set(ByVal value As Object)
            _itemRow("20:30") = value
        End Set
    End Property

    WriteOnly Property hh2100() As Object
        Set(ByVal value As Object)
            _itemRow("21:00") = value
        End Set
    End Property

    WriteOnly Property hh2130() As Object
        Set(ByVal value As Object)
            _itemRow("21:30") = value
        End Set
    End Property

    WriteOnly Property hh2200() As Object
        Set(ByVal value As Object)
            _itemRow("22:00") = value
        End Set
    End Property

    WriteOnly Property hh2230() As Object
        Set(ByVal value As Object)
            _itemRow("22:30") = value
        End Set
    End Property

    WriteOnly Property hh2300() As Object
        Set(ByVal value As Object)
            _itemRow("23:00") = value
        End Set
    End Property

    WriteOnly Property hh2330() As Object
        Set(ByVal value As Object)
            _itemRow("23:30") = value
        End Set
    End Property
#End Region
End Class

