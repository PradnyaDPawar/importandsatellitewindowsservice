﻿Public Class VitalEnergyHalfHour
    Implements ITransformDelegate


    Private _returnTable As DataTable

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set
    End Property



    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvFile As New DelimitedFile(fullPath, 0)
        csvFile.HasHeader = False
        csvFile.Load(_returnTable)
    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        If Not VitalEnergyHalfHourSchema.Validate(ReturnTable) Then
            Throw New ApplicationException("File didn't pass the validation.")
        End If

        DataTableTools.GetTranspose(ReturnTable)

        VitalEnergyHalfHourSchema.ToDEFormat(ReturnTable)

        VitalEnergyHalfHourSchema.GetHHTable(ReturnTable)
       

        For Each row As DataRow In _returnTable.Rows
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next

    End Sub


End Class
