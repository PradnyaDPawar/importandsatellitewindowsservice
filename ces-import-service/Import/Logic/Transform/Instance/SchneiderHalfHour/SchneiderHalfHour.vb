﻿Public Class SchneiderHalfHour
    Implements ITransformDelegate



    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel()
        Dim xSheet As ExcelSheet
        excel.Load(fullPath)
        xSheet = excel.Sheets(0)

        If Not excel.GetSheet("Sheet1", xSheet) = True Then
            Throw New ApplicationException("Unable to find the Sheet1 sheet.")
        End If

        _returnTable = xSheet.GetDataTable()

        If Not SchneiderHalfHourSchema.Validate(_returnTable) Then
            Throw New ApplicationException("The is not passed the validation.")
        End If

    End Sub

    Private _returnTable As DataTable

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        DataTableTools.GetTranspose(ReturnTable)

        SchneiderHalfHourSchema.ToDEFormat(ReturnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next

    End Sub
End Class
