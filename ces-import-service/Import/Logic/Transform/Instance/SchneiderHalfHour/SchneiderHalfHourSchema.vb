﻿Public Class SchneiderHalfHourSchema

    Public Shared Function ToDEFormat(ByRef tdt As DataTable)

        CalcConsumption(tdt)

        DeleteColumns(tdt)

        NameBlankCols(tdt)

        DataTableTools.SetHeaders(tdt, 1)

        CreateColumns(tdt)

        CreateRowWiseDateEntries(tdt)

        RenameHeaders(tdt)

        Return tdt

    End Function

    Private Shared Sub CalcConsumption(ByRef tdt As DataTable)

        Dim consumption As Double
        Dim colcounttill As Integer = GetLastDateIndexFromRows(tdt) + 2

        For colcount As Integer = 0 To tdt.Columns.Count - colcounttill Step 1

            If IsDate(tdt.Rows(0)(colcount)) Then

                For rowcount As Integer = 1 To tdt.Rows.Count - 1
                    consumption = tdt.Rows(rowcount)(colcount + 3) - tdt.Rows(rowcount)(colcount + 1)
                    tdt.Rows(rowcount)(colcount) = consumption.ToString("0.00")
                Next

            End If

        Next

    End Sub
#Region "Delete Redundant Columns"

    Private Shared Sub DeleteColumns(ByRef tdt As DataTable)

        DeleteInitialColumns(tdt)
        DeleteEndColumns(tdt)
        DeleteConsumptionReadings(tdt)

    End Sub

    Private Shared Sub DeleteInitialColumns(ByRef tdt As DataTable)

        'Hardcoding to delete first four columns

        For colcount As Integer = 0 To 3
            tdt.Columns.Remove(colcount.ToString)
        Next

    End Sub

    Private Shared Sub DeleteEndColumns(ByRef tdt As DataTable)

        Dim deleteindex = GetLastDateIndexFromRows(tdt) + 1

        For colcount1 As Integer = tdt.Columns.Count - 1 To tdt.Columns.Count - deleteindex Step -1
            tdt.Columns.RemoveAt(colcount1)
        Next


    End Sub


    Private Shared Sub DeleteConsumptionReadings(ByRef tdt As DataTable)

        Dim colcounttill As Integer = GetFirstDateIndexFromRows(tdt)

        For colcount = tdt.Columns.Count - 1 To colcounttill Step -1
            If tdt.Rows(0)(colcount) = "" Then
                tdt.Columns.RemoveAt(colcount)
            End If
        Next

    End Sub

    Private Shared Sub DeleteBlankCols(ByRef tdt As DataTable)
        Dim Deleteflag As Boolean

        For colcount As Integer = tdt.Columns.Count - 1 To 0 Step -1
            Deleteflag = True
            For Each dr As DataRow In tdt.Rows
                If Not dr(colcount) = "" Then
                    Deleteflag = False
                    Exit For
                End If
            Next

            If Deleteflag Then
                tdt.Columns.RemoveAt(colcount)
            End If

        Next


    End Sub

#End Region


    Private Shared Sub NameBlankCols(ByRef tdt As DataTable)

        tdt.Rows(0)(0) = "Meter Reference"
        tdt.Rows(0)(1) = "Energy"

    End Sub

#Region "Get Indexes for First and Last Date"

    Private Shared Function GetFirstDateIndexFromRows(ByRef tdt As DataTable) As Integer
        Dim StartDateindex As Integer = 0

        Dim colcount As Integer = 0
        While Not IsDate(tdt.Rows(0)(colcount))
            StartDateindex = StartDateindex + 1
            colcount += 1
        End While

        Return StartDateindex

    End Function

    Private Shared Function GetLastDateIndexFromRows(ByRef tdt As DataTable) As Integer
        Dim LastDateindex As Integer = 0

        Dim colcount As Integer = tdt.Columns.Count - 1
        While Not IsDate(tdt.Rows(0)(colcount))
            LastDateindex = LastDateindex + 1
            colcount -= 1
        End While

        Return LastDateindex

    End Function

    Private Shared Function GetFirstDateIndexFromHeaders(ByRef tdt As DataTable) As Integer

        Dim colcount As Integer = 0
        Dim initialdateindex As Integer = 0

        While Not IsDate(tdt.Columns(colcount).ColumnName)
            initialdateindex += 1
            colcount += 1
        End While

        Return initialdateindex

    End Function



    Private Shared Function GetLastDateIndexFromHeaders(ByRef tdt As DataTable) As Integer

        Dim enddateindex As Integer = 0
        Dim colcount As Integer = tdt.Columns.Count - 1

        While Not IsDate(tdt.Columns(colcount).ColumnName)
            enddateindex = enddateindex + 1
            colcount -= 1
        End While

        Return enddateindex
    End Function

#End Region


    Private Shared Function FillFirstDate(ByRef tdt As DataTable) As DataTable

        Dim readingdate As Date

        For Each row In tdt.Rows

            readingdate = tdt.Columns(GetFirstDateIndexFromHeaders(tdt)).ColumnName
            row("Date(DD/MM/YYYY)") = readingdate.ToShortDateString

        Next

        Return tdt
    End Function


    Private Shared Sub CreateRowWiseDateEntries(ByRef tdt As DataTable)

        Dim date1, date2, firstdate As Date
        Dim rowelement(tdt.Columns.Count - 1) As Object  'Object array to hold new row data

        Dim rowcounttill As Integer = tdt.Rows.Count - 1
        Dim colcounttill As Integer = GetLastDateIndexFromHeaders(tdt)

        Dim initialdateindex As Integer = GetFirstDateIndexFromHeaders(tdt)
        Dim colcount As Integer = initialdateindex
        Dim newcolcount As Integer


        date1 = tdt.Columns(initialdateindex).ColumnName

        For rowcount As Integer = 0 To rowcounttill

            If IsDBNull(tdt.Rows(rowcount)("Date(DD/MM/YYYY)")) Then
                firstdate = tdt.Columns(initialdateindex).ColumnName
                tdt.Rows(rowcount)("Date(DD/MM/YYYY)") = firstdate.ToShortDateString
            End If

            While colcount < tdt.Columns.Count - colcounttill - 1

                date2 = tdt.Columns(colcount).ColumnName
                newcolcount = colcount

                If date1.Date <> date2.Date Then

                    rowelement(0) = tdt.Rows(rowcount)(0)
                    rowelement(1) = tdt.Rows(rowcount)(1)
                    rowelement(tdt.Columns("Date(DD/MM/YYYY)").Ordinal()) = date2.ToShortDateString

                    Dim date3 As Date = tdt.Columns(newcolcount).ColumnName
                    Dim i As Integer = 2
                    While date2.Date = date3.Date And newcolcount <= tdt.Columns.Count - colcounttill - 1
                        rowelement(i) = tdt.Rows(rowcount)(newcolcount)
                        i += 1
                        newcolcount += 1

                        If newcolcount < tdt.Columns.Count - colcounttill - 1 Then
                            date3 = tdt.Columns(newcolcount).ColumnName
                        End If

                    End While

                    tdt.Rows.Add(rowelement)
                    Array.Clear(rowelement, 0, tdt.Columns.Count - 1)
                    colcount = newcolcount - 1
                    date1 = date2
                End If

                colcount += 1

            End While

            colcount = initialdateindex

        Next

        DeleteDateEntriesinColumn(tdt)


    End Sub



    Private Shared Sub DeleteDateEntriesinColumn(ByRef tdt As DataTable)
        Dim colcount As Integer = GetFirstDateIndexFromHeaders(tdt) 'should be colcount = initialindex - 1
        Dim date1, date2 As Date
        Dim removefromindex As Integer
        Dim removetillindex As Integer

        date1 = tdt.Columns(colcount).ColumnName
        date2 = tdt.Columns(colcount + 1).ColumnName


        While date1.Date = date2.Date And colcount < tdt.Columns.Count - removetillindex - 1

            colcount += 1
            date1 = tdt.Columns(colcount).ColumnName
            date2 = tdt.Columns(colcount + 1).ColumnName

        End While

        removefromindex = colcount + 1
        removetillindex = GetLastDateIndexFromHeaders(tdt) + 1

        For colcount = tdt.Columns.Count - removetillindex To removefromindex Step -1

            tdt.Columns.RemoveAt(colcount)

        Next

    End Sub

    Private Shared Sub CreateColumns(ByRef tdt As DataTable)

        tdt.Columns.Add("Date(DD/MM/YYYY)")
        tdt.Columns.Add("MeterSerial")
        tdt.Columns.Add("MPAN")
        tdt.Columns.Add("ForcekWh")

    End Sub

    Private Shared Sub RenameHeaders(ByRef tdt As DataTable)
        Dim dateholder As Date

        For Each column As DataColumn In tdt.Columns
            If IsDate(column.ColumnName) Then
                dateholder = column.ColumnName
                column.ColumnName = dateholder.ToString("HH:mm")
            End If
        Next

    End Sub

    Public Shared Function Validate(ByRef dt As DataTable) As Boolean

        Dim listofdates As List(Of Date)
        Dim StartDate As Date = GetReportStartTime(dt)

        Dim EndDate As Date = GetReportEndTime(dt)

        listofdates = GetListOfAllDates(dt)

        While StartDate < EndDate
            If Not listofdates.Contains(StartDate.ToShortDateString) Then
                Return False
            End If
            StartDate = StartDate.AddDays(1)
        End While

        Return True

    End Function

    Private Shared Function GetListOfAllDates(ByRef dt As DataTable) As List(Of Date)

        Dim listofdates As New List(Of Date)
        Dim datetoadd As Date

        For Each row As DataRow In dt.Rows

            If IsDate(row(0)) Then
                datetoadd = row(0)
                If Not listofdates.Contains(datetoadd.ToShortDateString) Then
                    listofdates.Add(datetoadd)
                End If
            End If

        Next
        listofdates.RemoveAt(listofdates.Count - 1) ' To remove the date inserted only to calculate 23:00 consumption

        Return listofdates

    End Function


    Public Shared Function GetReportStartTime(ByRef dt As DataTable) As String
        Dim found As Boolean
        For Each row As DataRow In dt.Rows
            For Each item As Object In row.ItemArray

                If found Then
                    Return (item.ToString)
                End If

                If item.ToString = "Report Start Time:" Then
                    found = True

                End If
            Next
        Next

        Throw New ApplicationException("ReportStartTime not found")

    End Function

    Public Shared Function GetReportEndTime(ByRef dt As DataTable) As String
        Dim found As Boolean
        For Each row As DataRow In dt.Rows
            For Each item As Object In row.ItemArray

                If found Then
                    Return (item.ToString)
                End If

                If item.ToString = "Report End Time:" Then
                    found = True

                End If
            Next
        Next

        Throw New ApplicationException("ReportEndTime not found")

    End Function


End Class
