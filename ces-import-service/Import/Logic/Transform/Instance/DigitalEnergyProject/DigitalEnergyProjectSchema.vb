﻿Partial Public Class DigitalEnergyProject

    Private Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("UPRN")
        headers.Add("ProjectName")
        headers.Add("Description")
        headers.Add("Rationale")
        headers.Add("Risks")
        headers.Add("FuelType")
        headers.Add("AnnualEnergySaving")
        headers.Add("AnnualCarbon")
        headers.Add("AnnualCostSaving")
        headers.Add("EstimatedCost")
        headers.Add("ProjectSpecificUnitCost")
        headers.Add("PaybackPeriod")
        headers.Add("LifeExpectancy")
        headers.Add("ProjectOwner")
        headers.Add("OwnerRole")
        headers.Add("ProjectStartDate")
        headers.Add("ProjectCompletionDate")
        headers.Add("BaselineStartDate")
        headers.Add("BaselineEndDate")
        headers.Add("CurrentStatus")
        headers.Add("TechnologyGroup")
        headers.Add("MainTechnology")
        headers.Add("SubTechnology")
        headers.Add("TypeOfAction")
        headers.Add("ImplementationSteps")
        headers.Add("RelatedInformation")
        headers.Add("RelatedWebLink1")
        headers.Add("RelatedWebLink2")
        Return headers
    End Function

    Private Sub ColumnValidate()
        Dim rawHeaders As List(Of String) = GetHeaders()
        For Each header As String In rawHeaders
            If Not _returnTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next
    End Sub

    Private Sub RenameColumns()
        Dim renamables() = {"AnnualCarbon"}
        For Each column As DataColumn In _returnTable.Columns
            column.ColumnName = column.ColumnName.Replace(" ", "")
            For Each prefix In renamables
                If column.ColumnName.StartsWith(prefix) Then
                    column.ColumnName = prefix
                    Exit For
                End If
            Next
        Next
    End Sub

End Class