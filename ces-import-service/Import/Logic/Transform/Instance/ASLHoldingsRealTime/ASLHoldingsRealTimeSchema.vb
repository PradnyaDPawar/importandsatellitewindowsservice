﻿Public Class ASLHoldingsRealTimeSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

      
        dt.Columns.RemoveAt(0)
        dt.Columns.RemoveAt(3)
        setColumnHeader(dt)

        Dim realTimeDt As DataTable
        realTimeDt = DeFormatTableTemplates.CreateRealTimeTable()
        PopulateColumns(dt, realTimeDt)
        Return dt


    End Function
    Public Shared Sub setColumnHeader(ByRef dt As DataTable)
        dt.Columns("1").ColumnName = "MeterName"
        dt.Columns("2").ColumnName = "DateTime"
        dt.Columns("3").ColumnName = "CountUnits"
    End Sub

    Private Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)

        Dim success As Boolean = False

        For Each row As DataRow In dt.Rows

            If IsDate(row("DateTime")) Then

                For Each Rrow As DataRow In realTimeDt.Rows
                    If Rrow("MeterName") = row("MeterName") And Rrow("DateTime") = CDate(row("DateTime")).ToString("dd/MM/yyyy HH:mm") Then
                        Rrow("CountUnits") = (Convert.ToInt32(row("CountUnits").ToString) / 1000).ToString("00.000")
                        success = True
                    End If
                Next

                If Not success Then
                    Dim realTimeDtRow As DataRow = realTimeDt.NewRow

                    realTimeDtRow("MeterName") = row("MeterName")
                    realTimeDtRow("DateTime") = CDate(row("DateTime")).ToString("dd/MM/yyyy HH:mm")
                    realTimeDtRow("CountUnits") = (Convert.ToInt32(row("CountUnits").ToString) / 1000).ToString("00.000")


                    realTimeDt.Rows.Add(realTimeDtRow)
                End If


                success = False

            End If
        Next
     

        dt = realTimeDt

    End Sub
End Class
