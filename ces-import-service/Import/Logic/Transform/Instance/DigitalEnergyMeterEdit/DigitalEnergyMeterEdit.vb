﻿Public Class DigitalEnergyMeterEdit
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel()
        excel.Load(fullPath)
        Dim xSheet As ExcelSheet = excel.Sheets(0)
        ReturnTable = xSheet.GetDataTable()
    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        DataTableTools.SetHeaders(_returnTable, 1)
        Dim isSubmeter As Boolean
        If (_returnTable.Columns.Contains("Parent Meter Serial")) Then
            isSubmeter = True
        Else
            isSubmeter = False
        End If
        DigitalEnergyMeterEditSchema.ToDeFormat(_returnTable, isSubmeter)
        For Each row As DataRow In _returnTable.Rows
            FE_MeterEdit.Fill(row, dsEdits, importEdit, isSubmeter)
        Next

    End Sub
End Class
