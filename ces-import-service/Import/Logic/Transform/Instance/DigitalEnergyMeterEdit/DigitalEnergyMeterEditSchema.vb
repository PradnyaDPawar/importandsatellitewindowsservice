﻿Public Class DigitalEnergyMeterEditSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable, ByRef isSubmeter As Boolean)

        Dim deconfig As DeConfig = New DeConfig()
        Dim ImportVersion As String = deconfig.GetConfig().ImportVersion

        DataTableTools.RemoveBlankRows(dt)
        Dim dtMeterEdit As DataTable
        If (isSubmeter) Then
            dtMeterEdit = DeFormatTableTemplates.CreateSubMeterEditTable(ImportVersion)
        Else
            dtMeterEdit = DeFormatTableTemplates.CreateMeterEditTable(ImportVersion)
        End If

        IsAllColumnExist(dt, dtMeterEdit)
        Renamecolumn(dt)
        RemoveColumnNameSpaces(dtMeterEdit)
        Populatecolumns(dtMeterEdit, dt, isSubmeter, ImportVersion)
        Return dt
    End Function
    Private Shared Sub Renamecolumn(ByRef dt As DataTable)
        For Each col As DataColumn In dt.Columns
            If (col.ColumnName = "Asset UPRN") Then
                col.ColumnName = "Building UPRN"
                Return
            End If
        Next
    End Sub

    Private Shared Sub IsAllColumnExist(ByRef dt As DataTable, ByRef meteredit As DataTable)
        Dim dummy As DataTable = dt.Copy()
        Dim metereditcopy As DataTable = meteredit.Copy()
        For Each dtcol As DataColumn In dummy.Columns
            If (dtcol.ColumnName = "Meter Deactivation Date (dd/mm/yyyy)") Then
                dtcol.ColumnName = "MeterDeactivationDate"
            ElseIf (dtcol.ColumnName = "MPAN / MPRN") Then
                dtcol.ColumnName = "MPAN"
            ElseIf (dtcol.ColumnName = "CTS Start Date (dd/mm/yyyy)") Then
                dtcol.ColumnName = "CTSStartDate"
            ElseIf (dtcol.ColumnName = "Calorific Value (Gas)") Then
                dtcol.ColumnName = "CalorificValue"
            ElseIf (dtcol.ColumnName = "Asset UPRN") Then
                dtcol.ColumnName = dtcol.ColumnName.ToString().Replace(" ", "")
            ElseIf (dtcol.ColumnName = "Meter Operator Contact/Access") Then
                dtcol.ColumnName = "MeterOperatorContact"
            ElseIf (dtcol.ColumnName = "Enable Day/Night Active Period") Then
                dtcol.ColumnName = "EnableDayNightActivePeriod"
            Else
                dtcol.ColumnName = dtcol.ColumnName.ToString().Replace(" ", "")
            End If

        Next
        For i As Integer = 0 To metereditcopy.Columns.Count - 1
            If Not (dummy.Columns.Contains(metereditcopy.Columns(i).ColumnName.ToString().Replace(" ", ""))) Then
                Throw New Exception("Column" + " '" + metereditcopy.Columns(i).ColumnName + "' " + "is missing from the imported file. Please ensure this column is reinstated and try again")
            End If
        Next

    End Sub
    Private Shared Sub Populatecolumns(ByRef dtMeterEdit As DataTable, ByRef dt As DataTable, ByRef isSubmeter As Boolean, ByVal ImportVersion As String)
        For Each row As DataRow In dt.Rows
            'If Not (String.IsNullOrEmpty(row("Client").ToString())) Then

            Dim newrow As DataRow = dtMeterEdit.NewRow
            newrow("Client") = row("Client")
            newrow("MeterFuel") = row("Meter Fuel")
            newrow("CurrentSerialNumber") = row("Current Serial Number")
            newrow("NewSerialNumber") = row("New Serial Number")
            If ImportVersion <> "Vestigo" Then
                newrow("AssetUPRN") = row("Building UPRN")
            Else
                newrow("BuildingUPRN") = row("Building UPRN")
            End If
            newrow("MeterName") = row("Meter Name")
            newrow("Units") = row("Units")
            newrow("ReportingMeter") = row("Reporting Meter")

            newrow("IsActive") = row("Is Active")
            newrow("MeterDeactivationDate") = row("Meter Deactivation Date (dd/mm/yyyy)")

            newrow("OperatingSchedule1") = row("Operating Schedule 1")
            newrow("OperatingSchedule2") = row("Operating Schedule 2")
            newrow("Location") = row("Location")
            newrow("Description") = row("Description")
            newrow("CorrectionFactor") = row("Correction Factor")
            newrow("ConversionFactor") = row("Conversion Factor")
            newrow("CustomMultiplier") = row("Custom Multiplier")
            newrow("ValidateReadings") = row("Validate Readings")
            newrow("ReadingWrapRound") = row("Reading Wrap Round")
            newrow("MaxReadConsumption") = row("Max Read Consumption")
            newrow("RealTime") = row("Real Time")
            newrow("WrapRoundValue") = row("Wrap Round Value")
            newrow("IsCumulative") = row("Is Cumulative")
            newrow("IsRaw") = row("Is Raw")
            newrow("MinEntry") = row("Min Entry")
            newrow("MaxEntry") = row("Max Entry")
            newrow("Multiplier") = row("Multiplier")
            newrow("SerialAlias") = row("Serial Alias")

            newrow("CreatekWhExportAssociateMeter") = row("Create kWh Export Associate Meter")
            newrow("CreatekVarhExportAssociateMeter") = row("Create kVarh Export Associate Meter")
            newrow("CreatekVarhAssociateMeter") = row("Create kVarh Associate Meter")
            newrow("ExportMPAN") = row("Export MPAN")
            newrow("ConsumptionSource") = row("Consumption Source")
            If (Not isSubmeter) Then
                newrow("MPAN") = row("MPAN / MPRN")
                newrow("AMR") = row("AMR")
                newrow("AMRInstalledDate") = row("AMR Installed Date")
                newrow("CRCMeterType") = row("CRC Meter Type")
                newrow("CRCFuelType") = row("CRC Fuel Type")
                newrow("IncludeAsCRCResidual") = row("Include As CRC Residual")
                newrow("CarbonTrustStandard") = row("Carbon Trust Standard")
                newrow("CTSStartDate") = row("CTS Start Date (dd/mm/yyyy)")
                newrow("CalorificValue") = row("Calorific Value (Gas)")
                newrow("DateInstalled") = row("Date Installed")
                newrow("DateCommissioned") = row("Date Commissioned")
                newrow("IsStockMeter") = row("Is Stock Meter")
                newrow("AccountNumber") = row("Account Number")
                newrow("InvoiceFrequency") = row("Invoice Frequency")
                newrow("AuthorisedCapacity") = row("Authorised Capacity")
                newrow("ExcessCapacity") = row("Excess Capacity")


                If ImportVersion <> "Vestigo" Then
                    newrow("DataCollector") = row("Data Collector")
                    newrow("ESOSComplianceMethod") = row("ESOS Compliance Method")
                Else

                    newrow("TimeZone") = row("Time Zone")
                    newrow("EnableDayNightActivePeriod") = row("Enable Day/Night Active Period")
                    newrow("DayPeriod") = row("Day Period")
                    newrow("NightPeriod") = row("Night Period")
                    
                End If


                newrow("CRCConsumptionSource") = row("CRC Consumption Source")
                newrow("TankSize") = row("Tank Size")
                newrow("Category") = row("Category")
                newrow("Status") = row("Status")
                newrow("MeterOperator") = row("Meter Operator")
                newrow("MeterOperatorContact") = row("Meter Operator Contact/Access")

            Else
                newrow("ParentMeterSerial") = row("Parent Meter Serial")

            End If
            dtMeterEdit.Rows.Add(newrow)
            'End If
        Next
        dt = dtMeterEdit
    End Sub

    Private Shared Sub RemoveColumnNameSpaces(dtMeterEdit As DataTable)

        For Each dtcol As DataColumn In dtMeterEdit.Columns
            dtcol.ColumnName = dtcol.ColumnName.ToString().Replace(" ", "")
        Next

    End Sub


End Class
