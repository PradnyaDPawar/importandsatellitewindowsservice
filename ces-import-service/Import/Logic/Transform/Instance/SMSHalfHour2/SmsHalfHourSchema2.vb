﻿Partial Public Class SmsHalfHour2

    Public Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("MPAN")
        headers.Add("MeterSerial")
        headers.Add("ForceKwh")
        headers.Add("Date(DD/MM/YYYY)")
        headers.Add("00:00")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")
        Return headers

    End Function

    Private Shared Sub Transform(ByRef importData As DataTable)

        ' transform column headings to a standard name

        For Each col As DataColumn In importData.Columns
            col.ColumnName = col.ColumnName.Replace(" ", "").ToLower()
            Select Case col.ColumnName

                'Mpan
                Case "mprn"
                    col.ColumnName = "mpan"

                Case "mpan/mprn"
                    col.ColumnName = "mpan"

                Case "mpr"
                    col.ColumnName = "mpan"

                Case "metername"
                    col.ColumnName = "mpan"

                Case "date"
                    col.ColumnName = "date"
            End Select


        Next


    End Sub

    Private Sub AddColumns()
        If Not _returnTable.Columns.Contains("forcekwh") Then
            _returnTable.Columns.Add("forcekwh")
        End If
        If Not _returnTable.Columns.Contains("meterserial") Then
            _returnTable.Columns.Add("meterserial")
        End If
    End Sub


    Private Sub AdjustData(ByRef row As DataRow)
        row("forcekwh") = "no"
        ' Data recieved in meter serial column of the raw file isn't actually meter serial so make it blank
        row("meterserial") = Nothing
    End Sub

    'Requested by client - FEC
    Private Sub AdjustMeterSerials(ByRef row As DataRow)
        ' Dim resultRows() As DataRow = dt.Select("MPAN IN ('2300000953198','2300000953170','2300000434723')")
        If row("MPAN") = "2300000953198" Or row("MPAN") = "2300000953170" Or row("MPAN") = "2300000434723" Or row("MPAN") = "1900060436871" Or row("MPAN") = "2000050522480" Or row("MPAN") = "2000050662016" Then
            row("MeterSerial") = "e" & row("MeterSerial")
        End If
    End Sub




End Class
