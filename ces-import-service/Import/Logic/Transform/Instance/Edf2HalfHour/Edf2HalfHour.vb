﻿Public Class Edf2HalfHour
    Implements ITransformDelegate

    Private _returnTable As DataTable


    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvfile As New DelimitedFile(fullPath, 52)
        csvfile.HasHeader = True
        csvfile.Load(_returnTable)
    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Validate(ReturnTable)
        RenameColumns(ReturnTable)
        AddColumns(ReturnTable)

        For Each row As DataRow In _returnTable.Rows
            PopulateColumns(row)
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next

    End Sub

End Class
