﻿Public Class NPowerExcaliburInvoice
    Implements ITransformDelegate
    Private _returnTable As DataTable
    Dim excel As New Excel()

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        excel.Load(fullPath)

        For Each xsheet As ExcelSheet In excel.Sheets
            If xsheet.Name.ToLower.Trim <> "front sheet" Then
                Dim dt As DataTable
                dt = xsheet.GetDataTable()
                dt = NPowerExcaliburInvoiceSchema.PrepareDatatable(dt)

                If excel.Sheets.IndexOf(xsheet) = 1 Then
                    _returnTable = dt
                Else
                    _returnTable.Merge(dt)
                End If
            End If
        Next
    End Sub

   

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat


        NPowerExcaliburInvoiceSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next

    End Sub
End Class
