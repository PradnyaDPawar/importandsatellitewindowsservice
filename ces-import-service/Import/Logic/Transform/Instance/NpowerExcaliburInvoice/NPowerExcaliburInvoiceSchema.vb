﻿Public Class NPowerExcaliburInvoiceSchema
    Private Shared _calcCost As Double

    Private Shared Property CalcCost() As Double
        Get
            Return _calcCost
        End Get
        Set(ByVal value As Double)
            _calcCost = value
        End Set
    End Property

    Public Shared Function PrepareDatatable(ByVal dt As DataTable) As DataTable
        DataTableTools.RemoveBlankColumns(dt)
        PrefixMainHeaders(dt, 1)

        DataTableTools.SetHeaders(dt, 3)
        FormatColumnHeaders(dt)
        DataTableTools.RemoveTopRows(dt, 2)
        DataTableTools.RemoveBlankRows(dt)
        DataTableTools.RemoveBlankColumns(dt)
        RemoveTotalsRow(dt)
        Return dt
    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        'UniqueRow(dt.Rows(1))

        FillMissingInfo(dt)
        Dim invoiceDataTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        CreateInvoiceEntries(dt, invoiceDataTable)

        RenameColumns(invoiceDataTable)
        dt = invoiceDataTable
        Return dt
    End Function

    Private Shared Sub UniqueRow(ByRef row As DataRow)
        Dim rowValuesList As New List(Of String)

        For Each column As DataColumn In row.Table.Columns
            If Not String.IsNullOrEmpty(row(column).ToString.Trim) Then
                rowValuesList.Add(row(column).ToString)
                Dim count As Integer = GetOccurrencesCount(row(column).ToString, rowValuesList)
                If count > 0 Then
                    row(column) = row(column).ToString & " " & count.ToString
                End If
            End If
        Next

    End Sub

    Private Shared Function GetOccurrencesCount(ByVal valueToFind As String, ByVal rowValues As List(Of String)) As Integer
        Dim count As Integer = (From temp In rowValues Where (temp.Equals(valueToFind)) Select temp).Count
        Return count
    End Function

    Private Shared Sub PrefixMainHeaders(ByRef dt As DataTable, ByVal MainHeaderRowIndex As Integer)
        Dim mainHeader, mainHeader2 As String
        mainHeader2 = ""
        For Each column As DataColumn In dt.Columns
            mainHeader = dt.Rows(MainHeaderRowIndex)(column)
            mainHeader2 = ""
            If Not String.IsNullOrEmpty(mainHeader) Then
                dt.Rows(MainHeaderRowIndex + 1)(column) = (mainHeader & dt.Rows(MainHeaderRowIndex + 1)(column)).ToString.Replace(" ", "")
                mainHeader2 = mainHeader
            ElseIf Not String.IsNullOrEmpty(mainHeader2) Then
                dt.Rows(MainHeaderRowIndex + 1)(column) = (mainHeader2 & dt.Rows(MainHeaderRowIndex + 1)(column)).ToString.Replace(" ", "")
            End If
        Next
    End Sub

    Private Shared Function FormatColumnHeaders(ByRef dt As DataTable) As DataTable
        For Each column As DataColumn In dt.Columns
            column.ColumnName = column.ColumnName.Trim.Replace(" ", "")
        Next
        Return dt
    End Function

    Private Shared Sub FillMissingInfo(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows
            If (Not String.IsNullOrEmpty(row("SUPPLYPERIOD"))) And IsDBNull(row("TOTALGROSS(£)")) Then

                Dim result() As DataRow = dt.Select("[TOTALGROSS(£)] IS NOT NULL AND [SITEINVOICE] = '" & row("SITEINVOICE") & "'")
                row("TOTALAMENDMENTS") = result(0)("TOTALAMENDMENTS")
                row("VAT@5.0%") = result(0)("VAT@5.0%")
                row("VAT@20.0%") = result(0)("VAT@20.0%")
                row("TOTALGROSS(£)") = result(0)("TOTALGROSS(£)")

            End If
        Next

    End Sub

    Private Shared Sub CreateInvoiceEntries(ByRef dt As DataTable, ByRef invoiceTable As DataTable)
        For Each row As DataRow In dt.Rows
            If Not String.IsNullOrEmpty(row("SUPPLYPERIOD")) Then
                PopulateRateEntries(row, invoiceTable, "Day", "Day")
                PopulateRateEntries(row, invoiceTable, "Night", "Night")
                PopulateRateEntries(row, invoiceTable, "AllDay", "AllDay")
                PopulateRateEntries(row, invoiceTable, "EVENINGANDWEEKENDS", "Evening and Weekends")
                PopulateRateEntries(row, invoiceTable, "ALLYEARMON-SUN0700-2400", "All Year Mon-Sun 0700-2400")
                PopulateRateEntries(row, invoiceTable, "ALLNIGHTS0000-070", "All Nights 0000-070")
                PopulateRateEntries(row, invoiceTable, "JAN-DECMON-FRI07:30-19:00", "Jan-Dec Mon-Fri 07:30-19:00")
                PopulateRateEntries(row, invoiceTable, "JAN-DEC07:30-00:30,JAN-DEC19", "JAN-DEC 07:30-00:30,JAN-DEC 19")

                PopulateNonConsumptionCharge(row, invoiceTable, "ALLDAYTOTALDUOSCONSUMPTIONCHARGE", "DUOS Consumption Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "ALLDAYDUOSFIXEDCHARGE", "DUOS Fixed Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "ReactiveCharge:01REACTIVEPOWERCHARGE", "Reactive Power Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "ReactiveCharge:01METEROPERATORCHARGE", "Meter Operator Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "ReactiveCharge:01TOTALMDCHARGE", "MD Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "ReactiveCharge:01TUOSINFRASTUCTUREDEMANDCHGE", "TUOS Infra Demand Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "ReactiveCharge:01TUOSSYSTEMSSERVICEDEMCHARG", "TUOS System Service Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "ReactiveCharge:01TUOSPOOLING&SETTLEMENTCHAR", "TUOS Pooling Settlement Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "CapacityAvailableCharge:01CHARGE", "Capacity Available Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "CapacityAvailableCharge:01TOTALAVAILABLECAPACITYCHARG", "Total Available Capacity Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "EXCESSCAPACITYCHARGE", "Excess Capacity Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "Admin", "Administration")
                PopulateNonConsumptionCharge(row, invoiceTable, "SETTLEMENT", "Settlement")
                PopulateNonConsumptionCharge(row, invoiceTable, "FLEXIBLEPURCHASINGCHARGE", "Flexible Purchasing Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "INDEXATIONCHARGE", "Indexation Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "UNSETFEES", "Unset Fees")
                PopulateNonConsumptionCharge(row, invoiceTable, "RESETFEES", "Reset Fees")
                PopulateNonConsumptionCharge(row, invoiceTable, "INDEXATIONFEES", "Indexation Fees")
                PopulateNonConsumptionCharge(row, invoiceTable, "BALANCEOFMONTHFEES", "Balance of Month Fees")
                PopulateNonConsumptionCharge(row, invoiceTable, "MANAGEMENTFEES", "Management Fees")
                PopulateNonConsumptionCharge(row, invoiceTable, "MonthlyOnSupplyCharge", "Monthly On Supply Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "ReactiveCharge:01REACTIVECHARGE", "Reactive Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "ReactiveCharge:01GREENPREMIUMCHARGE", "Green Premium Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "ReactiveCharge:01RENEWABLEOBLIGATIONCHARGE", "Renewable Obligation Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "ReactiveCharge:01ClimateChangeEquivalentChar", "Climate Change Equivalent Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "ReactiveCharge:01CclCharge", "CCL Charge")
                PopulateNonConsumptionCharge(row, invoiceTable, "ReactiveCharge:01TotalAmendments", "Amendment")

                'discrepancy in calculated costs and total cost in invoice, so create a new misc fixed cost row as difference
                PopulateMiscFixedCostRow(row, invoiceTable)
                CalcCost = 0
            End If
        Next
    End Sub

    Private Shared Sub PopulateMiscFixedCostRow(ByRef dtRow As DataRow, ByRef invoiceTable As DataTable)

        Dim invCost, diff As Double

        If dtRow.Table.Columns.Contains("ReactiveCharge:01TOTALNET(£)") Then

            invCost = dtRow("ReactiveCharge:01TOTALNET(£)").ToString.Replace("£", "")

        ElseIf dtRow.Table.Columns.Contains("TOTALNET(£)") Then

            invCost = dtRow("TOTALNET(£)").ToString.Replace("£", "")

        End If


        diff = invCost - CalcCost

        If diff <> 0 Then

            Dim invoiceTableRow As DataRow = invoiceTable.NewRow
            Dim net, vat As Double

            invoiceTableRow("Invoice Number") = dtRow("SiteInvoice")

            If dtRow.Table.Columns.Contains("ReactiveCharge:01MPAN") Then

                invoiceTableRow("MPAN") = dtRow("ReactiveCharge:01MPAN")

            ElseIf dtRow.Table.Columns.Contains("MPAN") Then

                invoiceTableRow("MPAN") = dtRow("MPAN")
            End If

            invoiceTableRow("Start Date") = dtRow("PeriodStart")
            invoiceTableRow("End Date") = dtRow("PeriodEnd")
            invoiceTableRow("Net") = diff.ToString("0.00")
            invoiceTableRow("Force kWh") = "No"
            net = invoiceTableRow("Net")
            vat = net * GetVatRate(dtRow)
            invoiceTableRow("Vat") = vat
            invoiceTableRow("Gross") = (net + vat).ToString("0.00")
            invoiceTableRow("Cost Only") = "Yes"
            invoiceTableRow("Description") = "Misc Fixed Charges"
            invoiceTableRow("TaxPointDate") = dtRow("INVOICEDATE")
            invoiceTableRow("IsLastDayApportion") = "No"
            invoiceTable.Rows.Add(invoiceTableRow)


        End If



    End Sub

    Private Shared Sub PopulateRateEntries(ByRef dtRow As DataRow, ByRef invoiceTable As DataTable, ByVal charge As String, ByVal chargeAlias As String)



        If dtRow.Table.Columns.Contains(charge & "BilledUnits") Then

            If Not String.IsNullOrEmpty(dtRow(charge & "BilledUnits")) Then
                Dim net, vat As Double
                Dim invoiceTableRow As DataRow = invoiceTable.NewRow
                invoiceTableRow("Invoice Number") = dtRow("SiteInvoice")

                If dtRow.Table.Columns.Contains("ReactiveCharge:01MPAN") Then
                    invoiceTableRow("MPAN") = dtRow("ReactiveCharge:01MPAN")

                ElseIf dtRow.Table.Columns.Contains("MPAN") Then
                    invoiceTableRow("MPAN") = dtRow("MPAN")
                End If

                invoiceTableRow("Rate") = chargeAlias
                invoiceTableRow("Start Date") = dtRow("PeriodStart")
                invoiceTableRow("End Date") = dtRow("PeriodEnd")

                If dtRow.Table.Columns.Contains(charge & "ReadingType") Then
                    'invoiceTableRow("Start Read Estimated") = dtRow(charge & "ReadingType")
                    invoiceTableRow("End Read Estimated") = dtRow(charge & "ReadingType")

                    'invoiceTableRow("Start Read") = dtRow(charge & "PREVIOUSREADING")
                    'invoiceTableRow("End Read") = dtRow(charge & "CURRENTREADINGS")

                End If

                invoiceTableRow("Consumption") = dtRow(charge & "BilledUnits")
                invoiceTableRow("Force kWh") = "Yes"
                net = dtRow(charge & "Charge").ToString.Replace("£", "")
                invoiceTableRow("Net") = net
                vat = net * GetVatRate(dtRow)
                invoiceTableRow("Vat") = vat
                invoiceTableRow("Gross") = (net + vat).ToString("0.00")
                invoiceTableRow("Cost Only") = "No"
                invoiceTableRow("Description") = chargeAlias
                invoiceTableRow("TaxPointDate") = dtRow("INVOICEDATE")
                invoiceTableRow("IsLastDayApportion") = "No"
                invoiceTable.Rows.Add(invoiceTableRow)
                CalcCost = CalcCost + dtRow(charge & "Charge").ToString.Replace("£", "")

            End If

        End If



    End Sub

    Private Shared Sub PopulateNonConsumptionCharge(ByRef dtRow As DataRow, ByRef invoiceTable As DataTable, ByVal charge As String, ByVal chargeAlias As String)
        If dtRow.Table.Columns.Contains(charge) Then
            If Not String.IsNullOrEmpty(dtRow(charge)) Then

                If dtRow(charge).ToString.Replace("£", "") <> 0 Then

                    Dim invoiceTableRow As DataRow = invoiceTable.NewRow
                    Dim net, vat As Double

                    invoiceTableRow("Invoice Number") = dtRow("SiteInvoice")

                    If dtRow.Table.Columns.Contains("ReactiveCharge:01MPAN") Then
                        invoiceTableRow("MPAN") = dtRow("ReactiveCharge:01MPAN")

                    ElseIf dtRow.Table.Columns.Contains("MPAN") Then
                        invoiceTableRow("MPAN") = dtRow("MPAN")
                    End If

                    invoiceTableRow("Start Date") = dtRow("PeriodStart")
                    invoiceTableRow("End Date") = dtRow("PeriodEnd")
                    invoiceTableRow("Force kWh") = "No"
                    net = dtRow(charge).ToString.Replace("£", "")
                    invoiceTableRow("Net") = net
                    vat = net * GetVatRate(dtRow)
                    invoiceTableRow("Vat") = vat
                    invoiceTableRow("Gross") = (net + vat).ToString("0.00")
                    invoiceTableRow("Cost Only") = "Yes"
                    invoiceTableRow("Description") = chargeAlias
                    invoiceTableRow("TaxPointDate") = dtRow("INVOICEDATE")
                    invoiceTableRow("IsLastDayApportion") = "No"
                    invoiceTable.Rows.Add(invoiceTableRow)

                    CalcCost = CalcCost + dtRow(charge).ToString.Replace("£", "")

                    If charge = "ReactiveCharge:01TotalAmendments" Then
                        invoiceTableRow("Start Date") = dtRow("InvoiceDate")
                        invoiceTableRow("End Date") = dtRow("InvoiceDate")
                    End If

                End If

            End If
        End If
    End Sub

    Private Shared Function GetVatRate(ByVal row As DataRow) As Double
        Dim vatRate As Double
        If row.Table.Columns.Contains("ReactiveCharge:01VAT@20.0%") Then
            If Not String.IsNullOrEmpty(row("ReactiveCharge:01VAT@20.0%")) Then
                vatRate = 0.2
            End If
        End If

        If row.Table.Columns.Contains("VAT@20.0%") Then
            If Not String.IsNullOrEmpty(row("VAT@20.0%")) Then
                vatRate = 0.2
            End If
        End If

        If row.Table.Columns.Contains("ReactiveCharge:01VAT@5.0%") Then
            If Not String.IsNullOrEmpty(row("ReactiveCharge:01VAT@5.0%")) Then
                vatRate = 0.05
            End If
        End If

        If row.Table.Columns.Contains("VAT@5.0%") Then
            If Not String.IsNullOrEmpty(row("VAT@5.0%")) Then
                vatRate = 0.05
            End If
        End If

        If row.Table.Columns.Contains("ReactiveCharge:01VAT@17.5%") Then
            If Not String.IsNullOrEmpty(row("ReactiveCharge:01VAT@17.5%")) Then
                vatRate = 0.175
            End If
        End If

        If row.Table.Columns.Contains("VAT@17.5%") Then
            If Not String.IsNullOrEmpty(row("VAT@17.5%")) Then
                vatRate = 0.175
            End If
        End If

        If row.Table.Columns.Contains("ReactiveCharge:01VAT@15.0%") Then
            If Not String.IsNullOrEmpty(row("ReactiveCharge:01VAT@15.0%")) Then
                vatRate = 0.15
            End If
        End If

        If row.Table.Columns.Contains("VAT@15.0%") Then
            If Not String.IsNullOrEmpty(row("VAT@15.0%")) Then
                vatRate = 0.15
            End If
        End If

        Return vatRate
    End Function

    Private Shared Sub RemoveTotalsRow(ByRef dt As DataTable)

        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If String.IsNullOrEmpty(row("SiteInvoice")) And String.IsNullOrEmpty(row("InvoiceDate")) Then
                dt.Rows.Remove(row)
            End If
        Next

        dt.AcceptChanges()

    End Sub

    Private Shared Sub RenameColumns(invoiceDataTable As DataTable)
        invoiceDataTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceDataTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceDataTable.Columns("Rate").ColumnName = "Tariff"
        invoiceDataTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceDataTable.Columns("End Date").ColumnName = "EndDate"
        invoiceDataTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceDataTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceDataTable.Columns("End Read").ColumnName = "EndRead"
        invoiceDataTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceDataTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceDataTable.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub

End Class
