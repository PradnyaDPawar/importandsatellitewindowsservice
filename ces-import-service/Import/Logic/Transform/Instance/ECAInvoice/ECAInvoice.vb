﻿Public Class ECAInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable
    Public flag As String
    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel()

        excel.Load(fullPath)
        Dim xSheet As ExcelSheet = excel.Sheets(0)
        _returnTable = xSheet.GetDataTable()

    End Sub
  

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        Dim copyDataTable As DataTable
        DataTableTools.RemoveBlankRows(ReturnTable)
        DataTableTools.RemoveBlankColumns(ReturnTable)
        copyDataTable = ReturnTable.Copy()
      

        RemoveUnwantedRows(copyDataTable)
        DataTableTools.SetHeaders(copyDataTable, 1)
        setflag(copyDataTable)


        ECAInvoiceSchema.ToDeFormat(_returnTable, flag)

        For Each row As DataRow In _returnTable.Rows

            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub
    Public Sub setflag(ByRef dt As DataTable)


        For Each col As DataColumn In dt.Columns

            If (col.ColumnName.Contains("Gas Charge")) Then
                flag = "Gas"
            ElseIf (col.ColumnName.Contains("Reactive Power")) Then
                flag = "HH"
            ElseIf (col.ColumnName.Contains("PRESENT METER READ")) Then
                flag = "NHH"



            End If
        Next

    End Sub
    Public Shared Function RemoveUnwantedRows(ByRef dt As DataTable) As DataTable
        For index = 0 To 3
            If (dt.Rows.Count > 0) Then
                dt.Rows.RemoveAt(0)
            End If
        Next
        Return dt
    End Function
End Class
