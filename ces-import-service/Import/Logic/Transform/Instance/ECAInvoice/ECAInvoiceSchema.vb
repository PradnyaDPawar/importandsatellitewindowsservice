﻿Public Class ECAInvoiceSchema





    Shared Sub ToDeFormat(ByRef returnTable As DataTable, ByRef flag As String)


        Dim invoiceDataTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()

        Select Case flag
            Case "Gas"
                tableOperations(flag, returnTable)
                PopulateColumnsForGAS(returnTable, invoiceDataTable, flag)
            Case "HH"
                tableOperations(flag, returnTable)
                PopulateColumnsforHH(returnTable, invoiceDataTable, flag)
            Case "NHH"
                tableOperations(flag, returnTable)
                PopulateColumnsforNHH(returnTable, invoiceDataTable, flag)
        End Select




    End Sub
   
    Private Shared Function PopulateColumnsForGAS(ByRef dt As DataTable, ByRef invoiceTbl As DataTable, ByRef flag As String) As DataTable
        'dt.Columns("VAT Charge").SetOrdinal(dt.Columns.IndexOf("Transportation"))
        Dim lstOfStrings As New List(Of String) From {"Consumption Kwh", "Metering Charge", "Standing Charge", "CCL Charge", "Transportation", "Other Charges", "VAT Charge", "Non Vatable Charges"}
        Dim count As Integer = 0
        Dim columns As DataColumnCollection = dt.Columns

        For Each row As DataRow In dt.Rows
            If (String.IsNullOrEmpty(row("Previous Read Date")) And String.IsNullOrEmpty(row("Present Read Date"))) Then
                row.Delete()
            Else



            For Each lst As String In lstOfStrings
                If (columns.Contains(lst)) Then
                    count = count + 1
                    Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow
                    newInvoiceTblRow("Invoice Number") = row("Invoice Number")
                    newInvoiceTblRow("Meter Serial") = row("MSN")
                    newInvoiceTblRow("MPAN") = row("MPR").ToString().Replace(" ", "")

                    newInvoiceTblRow("Start Date") = IIf(String.IsNullOrEmpty(row("Previous Read Date").ToString()), "", CDate(row("Previous Read Date")).AddDays(1).ToString("dd/MM/yyyy"))

                    newInvoiceTblRow("End Date") = IIf(String.IsNullOrEmpty(row("Present Read Date").ToString()), "", CDate(row("Present Read Date")).ToString("dd/MM/yyyy"))
                    newInvoiceTblRow("TaxPointDate") = IIf(String.IsNullOrEmpty(row("Invoice Date").ToString()), "", CDate(row("Invoice Date")).ToString("dd/MM/yyyy"))

                    If (lst = "Consumption Kwh") Then
                        newInvoiceTblRow("Description") = lst.Trim().Remove(lst.Length - 3) + " @ " + row("Price p/kwh")
                        newInvoiceTblRow("Rate") = lst.Remove(lst.Length - 3) + " @ " + row("Price p/kwh")
                        newInvoiceTblRow("Cost Only") = "No"
                        newInvoiceTblRow("Net") = row("Gas Charge").ToString().Replace("£", "")
                        newInvoiceTblRow("Gross") = row("Gas Charge").ToString().Replace("£", "")
                    Else
                        newInvoiceTblRow("Description") = lst
                        newInvoiceTblRow("Rate") = lst
                        newInvoiceTblRow("Cost Only") = "Yes"
                        newInvoiceTblRow("Net") = IIf(row(lst) = "", "0.00", row(lst).ToString().Replace("£", ""))
                        newInvoiceTblRow("Gross") = IIf(row(lst) = "", "0.00", row(lst).ToString().Replace("£", ""))

                    End If
                    If (lst = "VAT Charge") Then
                        newInvoiceTblRow("VAT") = row(lst).ToString().Replace("£", "")
                        newInvoiceTblRow("Net") = "0.00"
                    Else
                        newInvoiceTblRow("VAT") = 0
                    End If

                    newInvoiceTblRow("Force Kwh") = "Yes"
                    newInvoiceTblRow("IsLastDayApportion") = "Yes"
                    If (count = 1) Then
                        newInvoiceTblRow("End Read") = row("End Read")

                        newInvoiceTblRow("End Read Estimated") = row("Read Type")

                        newInvoiceTblRow("Consumption") = row("Consumption Kwh")
                    Else
                        newInvoiceTblRow("Consumption") = 0
                    End If


                    invoiceTbl.Rows.Add(newInvoiceTblRow)


                End If
            Next
                count = 0
            End If

        Next
                RenameColumns(invoiceTbl)
        dt = invoiceTbl
        Return dt


    End Function

    Private Shared Function PopulateColumnsforHH(ByRef dt As DataTable, ByRef invoiceTbl As DataTable, ByRef flag As String) As DataTable
        Dim lstOfSupplyStrings As New List(Of String) From {"Supply Band 1", "Supply Band 2", "Supply Band 3", "Supply Band 4", "Supply Band 5", "Supply Band 6", "Supply Band 7", "Supply Band 8", "Capacity Available Charge: 1"}
        Dim lstOfOtherStrings = New List(Of String) From {"Fixed Charge", "SETTLEMENT", "FIT Charge", "ROC", "Reactive Power", "Fuel Levy", "TUOS", "DUOS", "TRIAD", "Green", "Other Charges", "CCL  CHARGE", "VAT (£)", "Non Vatable Charges"}
        Dim columns As DataColumnCollection = dt.Columns
        For Each row As DataRow In dt.Rows
            If (String.IsNullOrEmpty(row("SUPPLY PERIOD$FROM DATE")) And String.IsNullOrEmpty(row("SUPPLY PERIOD$TO DATE"))) Then
                row.Delete()
            Else


                For Each lst As String In lstOfSupplyStrings
                    Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow
                    newInvoiceTblRow("Invoice Number") = row("INVOICE NUMBER")
                    newInvoiceTblRow("Meter Serial") = row("Serial")
                    newInvoiceTblRow("MPAN") = row("MPAN").ToString().Replace(" ", "")
                    newInvoiceTblRow("Start Date") = IIf(String.IsNullOrEmpty(row("SUPPLY PERIOD$FROM DATE").ToString()), "", CDate(row("SUPPLY PERIOD$FROM DATE")).AddDays(1).ToString("dd/MM/yyyy"))
                    newInvoiceTblRow("End Date") = IIf(String.IsNullOrEmpty(row("SUPPLY PERIOD$TO DATE").ToString()), "", CDate(row("SUPPLY PERIOD$TO DATE")).ToString("dd/MM/yyyy"))
                    newInvoiceTblRow("TaxPointDate") = IIf(String.IsNullOrEmpty(row("Invoice Date").ToString()), "", CDate(row("Invoice Date")).ToString("dd/MM/yyyy"))
                    newInvoiceTblRow("Force Kwh") = "Yes"
                    newInvoiceTblRow("IsLastDayApportion") = "Yes"
                    newInvoiceTblRow("Cost Only") = "No"

                    If columns.Contains(lst + "$RATE") Then
                        newInvoiceTblRow("Rate") = lst + " @ " + row(lst + "$RATE")
                    End If
                    If columns.Contains(lst + "$CHARGE") Then
                        newInvoiceTblRow("Net") = row(lst + "$CHARGE").ToString().Replace("£", "")
                        newInvoiceTblRow("Gross") = row(lst + "$CHARGE").ToString().Replace("£", "")
                    End If
                    If columns.Contains(lst + "$UNITS") Then
                        newInvoiceTblRow("Consumption") = row(lst + "$UNITS")
                    End If
                    If (lst = "Capacity Available Charge: 1") Then
                        newInvoiceTblRow("Cost Only") = "Yes"
                        newInvoiceTblRow("Description") = lst
                        newInvoiceTblRow("Rate") = lst
                    End If
                    newInvoiceTblRow("Description") = lst
                    invoiceTbl.Rows.Add(newInvoiceTblRow)
                Next
                For Each lstOther As String In lstOfOtherStrings
                    If columns.Contains(lstOther) Then
                        Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow
                        newInvoiceTblRow("Invoice Number") = row("INVOICE NUMBER")
                        newInvoiceTblRow("Meter Serial") = row("Serial")
                        newInvoiceTblRow("MPAN") = row("MPAN").ToString().Replace(" ", "")
                        newInvoiceTblRow("Start Date") = IIf(String.IsNullOrEmpty(row("SUPPLY PERIOD$FROM DATE").ToString()), "", CDate(row("SUPPLY PERIOD$FROM DATE")).AddDays(1).ToString("dd/MM/yyyy"))
                        newInvoiceTblRow("End Date") = IIf(String.IsNullOrEmpty(row("SUPPLY PERIOD$TO DATE").ToString()), "", CDate(row("SUPPLY PERIOD$TO DATE")).ToString("dd/MM/yyyy"))
                        newInvoiceTblRow("TaxPointDate") = IIf(String.IsNullOrEmpty(row("Invoice Date").ToString()), "", CDate(row("Invoice Date")).ToString("dd/MM/yyyy"))
                        newInvoiceTblRow("Force Kwh") = "Yes"
                        newInvoiceTblRow("IsLastDayApportion") = "Yes"

                        newInvoiceTblRow("Description") = lstOther
                        newInvoiceTblRow("Rate") = lstOther
                        newInvoiceTblRow("Consumption") = 0
                        newInvoiceTblRow("Net") = row(lstOther).ToString().Replace("£", "")
                        newInvoiceTblRow("Gross") = row(lstOther).ToString().Replace("£", "")

                        newInvoiceTblRow("Cost Only") = "Yes"
                        If (lstOther = "VAT (£)") Then
                            newInvoiceTblRow("VAT") = row(lstOther).ToString().Replace("£", "")
                            newInvoiceTblRow("Net") = "0.00"
                        Else

                            newInvoiceTblRow("VAT") = 0
                        End If

                        invoiceTbl.Rows.Add(newInvoiceTblRow)

                    End If

                Next
            End If
        Next

        RenameColumns(invoiceTbl)
        dt = invoiceTbl
        Return dt




    End Function
    Private Shared Function PopulateColumnsforNHH(ByRef dt As DataTable, ByRef invoiceTbl As DataTable, ByRef flag As String) As DataTable
        Dim lstOfMeterStrings As New List(Of String) From {"Meter 1", "Meter 2", "Meter 3", "Meter 4", "Meter 5", "Meter 6", "Meter 7", "Meter 8", "Capacity Available Charge: 1"}
        Dim lstOfOtherStrings = New List(Of String) From {"Fixed Charge", "FIT Charge", "Settlements", "Fuel Levy", "TUOS", "DUOS", "TRIAD", "Green", "Other Charges", "CCL  CHARGE", "VAT (£)", "Non Vatable Charges"}
        Dim columns As DataColumnCollection = dt.Columns
        For Each row As DataRow In dt.Rows
            If (String.IsNullOrEmpty(row("SUPPLY PERIOD$FROM DATE")) And String.IsNullOrEmpty(row("SUPPLY PERIOD$TO DATE"))) Then
                row.Delete()
            Else
                For Each lst As String In lstOfMeterStrings


                    Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow

                    newInvoiceTblRow("Invoice Number") = row("INVOICE NUMBER")
                    newInvoiceTblRow("Meter Serial") = row("MSN")
                    newInvoiceTblRow("MPAN") = row("MPAN").ToString().Replace(" ", "")
                    newInvoiceTblRow("Start Date") = IIf(String.IsNullOrEmpty(row("SUPPLY PERIOD$FROM DATE").ToString()), "", CDate(row("SUPPLY PERIOD$FROM DATE")).AddDays(1).ToString("dd/MM/yyyy"))
                    newInvoiceTblRow("End Date") = IIf(String.IsNullOrEmpty(row("SUPPLY PERIOD$TO DATE").ToString()), "", CDate(row("SUPPLY PERIOD$TO DATE")).ToString("dd/MM/yyyy"))
                    newInvoiceTblRow("TaxPointDate") = IIf(String.IsNullOrEmpty(row("Invoice Date").ToString()), "", CDate(row("Invoice Date")).ToString("dd/MM/yyyy"))
                    newInvoiceTblRow("Force Kwh") = "Yes"
                    newInvoiceTblRow("IsLastDayApportion") = "Yes"
                    newInvoiceTblRow("Cost Only") = "No"



                    If columns.Contains(lst + "$RATE") Then

                        newInvoiceTblRow("Rate") = lst + " @ " + row(lst + "$RATE")

                    End If
                    If columns.Contains(lst + "$CHARGE") Then

                        newInvoiceTblRow("Net") = row(lst + "$CHARGE").ToString().Replace("£", "")
                        newInvoiceTblRow("Gross") = row(lst + "$CHARGE").ToString().Replace("£", "")

                    End If
                    If columns.Contains(lst + "$UNITS") Then

                        newInvoiceTblRow("Consumption") = row(lst + "$UNITS")

                    End If
                    If (lst <> "Capacity Available Charge: 1") Then
                        If columns.Contains(lst + "$PRESENT METER READ") Then

                            newInvoiceTblRow("End Read") = IIf(String.IsNullOrEmpty(row(lst + "$PRESENT METER READ").ToString()), "", row(lst + "$PRESENT METER READ"))


                        End If
                        If columns.Contains(lst + "$PRESENT READ TYPE") Then

                            newInvoiceTblRow("End Read Estimated") = IIf(String.IsNullOrEmpty(row(lst + "$PRESENT READ TYPE").ToString()), "", row(lst + "$PRESENT READ TYPE"))

                        End If
                    Else
                        newInvoiceTblRow("Cost Only") = "Yes"
                        newInvoiceTblRow("Description") = lst
                        newInvoiceTblRow("Rate") = lst

                    End If
                    newInvoiceTblRow("Description") = lst


                    invoiceTbl.Rows.Add(newInvoiceTblRow)
                Next
                For Each lstOther As String In lstOfOtherStrings
                    If columns.Contains(lstOther) Then

                    Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow
                    newInvoiceTblRow("Invoice Number") = row("INVOICE NUMBER")
                    newInvoiceTblRow("Meter Serial") = row("MSN")
                    newInvoiceTblRow("MPAN") = row("MPAN").ToString().Replace(" ", "")
                    newInvoiceTblRow("Start Date") = IIf(String.IsNullOrEmpty(row("SUPPLY PERIOD$FROM DATE").ToString()), "", CDate(row("SUPPLY PERIOD$FROM DATE")).AddDays(1).ToString("dd/MM/yyyy"))
                    newInvoiceTblRow("End Date") = IIf(String.IsNullOrEmpty(row("SUPPLY PERIOD$TO DATE").ToString()), "", CDate(row("SUPPLY PERIOD$TO DATE")).ToString("dd/MM/yyyy"))
                    newInvoiceTblRow("TaxPointDate") = IIf(String.IsNullOrEmpty(row("Invoice Date").ToString()), "", CDate(row("Invoice Date")).ToString("dd/MM/yyyy"))
                    newInvoiceTblRow("Force Kwh") = "Yes"
                    newInvoiceTblRow("IsLastDayApportion") = "Yes"

                    newInvoiceTblRow("Description") = lstOther
                    newInvoiceTblRow("Rate") = lstOther
                    newInvoiceTblRow("Consumption") = 0
                    newInvoiceTblRow("Net") = row(lstOther).ToString().Replace("£", "")
                    newInvoiceTblRow("Gross") = row(lstOther).ToString().Replace("£", "")
                    newInvoiceTblRow("End Read") = ""
                    newInvoiceTblRow("End Read Estimated") = ""
                    newInvoiceTblRow("Cost Only") = "Yes"
                    If (lstOther = "VAT (£)") Then
                        newInvoiceTblRow("VAT") = row(lstOther).ToString().Replace("£", "")
                        newInvoiceTblRow("Net") = "0.00"
                    Else

                        newInvoiceTblRow("VAT") = 0
                    End If
                        invoiceTbl.Rows.Add(newInvoiceTblRow)
                    End If

                Next


            End If

        Next

        RenameColumns(invoiceTbl)
        dt = invoiceTbl
        Return dt




    End Function

    Public Shared Sub tableOperations(ByRef flag As String, ByRef dt As DataTable)
        For index = 0 To 2
            If (dt.Rows.Count > 0) Then
                dt.Rows.RemoveAt(0)
            End If
        Next


        Select Case flag
            Case "Gas"

                dt.Rows.RemoveAt(0)
                DataTableTools.SetHeaders(dt, 1)
                dt.Columns("Present Reading").ColumnName = "End Read"
            Case "HH"
                'DataTableTools.SetHeaders(dt, 1)

                PrefixMainHeaders(dt, 0)
                dt.Rows.RemoveAt(0)
                DataTableTools.SetHeaders(dt, 1)
            Case "NHH"
                PrefixMainHeaders(dt, 0)
                dt.Rows.RemoveAt(0)
                DataTableTools.SetHeaders(dt, 1)
        End Select

    End Sub
    Private Shared Sub PrefixMainHeaders(ByRef dt As DataTable, ByVal MainHeaderRowIndex As Integer)
        Dim mainHeader, mainHeader2 As String
        mainHeader2 = ""
        For Each column As DataColumn In dt.Columns
            mainHeader = dt.Rows(MainHeaderRowIndex)(column)
            mainHeader2 = ""
            If Not String.IsNullOrEmpty(mainHeader) Then
                dt.Rows(MainHeaderRowIndex + 1)(column) = (mainHeader & "$" & dt.Rows(MainHeaderRowIndex + 1)(column)).ToString.Replace(" ", " ")
                mainHeader2 = mainHeader
            ElseIf Not String.IsNullOrEmpty(mainHeader2) Then
                dt.Rows(MainHeaderRowIndex + 1)(column) = (mainHeader2 & "$" & dt.Rows(MainHeaderRowIndex + 1)(column)).ToString.Replace(" ", " ")
            End If
        Next
    End Sub
    Private Shared Sub RenameColumns(invoiceDataTable As DataTable)
        invoiceDataTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceDataTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceDataTable.Columns("Rate").ColumnName = "Tariff"
        invoiceDataTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceDataTable.Columns("End Date").ColumnName = "EndDate"
        invoiceDataTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceDataTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceDataTable.Columns("End Read").ColumnName = "EndRead"
        invoiceDataTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceDataTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceDataTable.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub
End Class
