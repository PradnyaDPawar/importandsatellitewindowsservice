﻿Public Class OptimaElectricityInvoiceSchema

#Region "Member variables"

    Private _deInvoiceTable As DataTable
    Private _invoiceTable As DataTable
    Private ChargesColumns As List(Of OptimaElectricityChargeColumnInfo)

#End Region

#Region "Constructor"

    Public Sub New()

        _deInvoiceTable = New DataTable()
        _deInvoiceTable.TableName = "DeInvoiceTable"

        ' Add columns in transpose table as per the schema of 'ImportDataStagingInvoice' table of Db.
        _deInvoiceTable = OptimaElectricityInvoiceSchemaHelper.CreateInvoiceTable().Clone()

    End Sub

#End Region

#Region "Public properties"

    ''' <summary>
    ''' Public property storing the original invoice data loaded from file.
    ''' </summary>
    Public Property InvoiceTable() As DataTable
        Get
            Return _invoiceTable
        End Get
        Set(ByVal value As DataTable)
            _invoiceTable = value
        End Set
    End Property

    ''' <summary>
    ''' Public property storing the invoice data in De format.
    ''' </summary>
    Public Property DeInvoiceTable() As DataTable
        Get
            Return _deInvoiceTable
        End Get
        Set(ByVal value As DataTable)
            _deInvoiceTable = value
        End Set
    End Property

#End Region

    Public Sub ToDeFormat(ByRef invoiceDt As DataTable)

        _invoiceTable = invoiceDt.Copy()
        _invoiceTable.TableName = "InvoiceTable"

        InitializeInvoiceTable()

        ChargesColumns = OptimaElectricityInvoiceSchemaHelper.PrepareChargesColumnsSchema(InvoiceTable)

        PopulateDeInvoiceTable()

        invoiceDt = DeInvoiceTable

    End Sub

#Region "Member methods"

    Private Sub InitializeInvoiceTable()

        DataTableTools.RemoveTopRows(InvoiceTable, 3)
        DataTableTools.RemoveBlankRows(InvoiceTable)
        DataTableTools.RemoveBlankColumns(InvoiceTable)
        DataTableTools.SetHeaders(InvoiceTable, 1)
        DataTableTools.RemoveBlankColumns(InvoiceTable)

    End Sub

    ''' <summary>
    ''' This method is used to populate the DeInvoiceTable from the original invoice data.
    ''' </summary>
    Private Sub PopulateDeInvoiceTable()

        ' Iterating through each row of invoice table and populating DeInvoiceTable with appropriate charges.
        For Each row As DataRow In InvoiceTable.Rows
            For Each chargeColumnInfo As OptimaElectricityChargeColumnInfo In ChargesColumns

                ' If Charge unit (consumption) and cost both have either empty or 0 value than dont add it in result DeInvoiceTable.

                If (InvoiceTable.Columns.Contains(chargeColumnInfo.ConsumptionColumnName) Or InvoiceTable.Columns.Contains(chargeColumnInfo.CostColumnName)) Then
                    If IsValidChargeInfo(row, chargeColumnInfo) Then

                        Dim newRow As DataRow = DeInvoiceTable.NewRow()

                        CopyRow(row, newRow, chargeColumnInfo)
                        DeInvoiceTable.Rows.Add(newRow)
                    End If
                End If

            Next
        Next

    End Sub

    ''' <summary>
    ''' This method contains the logic which defines the validity of charge from the unit and cost value.
    ''' If both unit and cost value is either empty or zero, the charge is invalid and it should be ignored.
    ''' </summary>
    ''' <returns>True, if charge is valid, else false.</returns>
    Private Function IsValidChargeInfo(ByRef sourceRow As DataRow, ByVal charge As OptimaElectricityChargeColumnInfo) As Boolean

        Dim isValidCharge As Boolean = True

        Dim unitValue As Double = 0.0
        Dim costValue As Double = 0.0

        Dim unitString As String = If(Not String.IsNullOrEmpty(charge.ConsumptionColumnName), sourceRow(charge.ConsumptionColumnName), "0.0")
        Dim costString As String = sourceRow(charge.CostColumnName)

        Double.TryParse(unitString, unitValue)
        Double.TryParse(costString, costValue)

        isValidCharge = Not (unitValue = 0.0 AndAlso costValue = 0.0)

        Return isValidCharge

    End Function

    Private Function GetReadEstimation(ByVal readEstimated As String) As String

        Dim returnValue As String = String.Empty

        Dim ListActual As New List(Of Char)(New Char() {"n"})

        If ListActual.Contains(readEstimated.ToString.ToLower) Then
            returnValue = "Actual"
        Else
            returnValue = "Estimated"
        End If

        Return returnValue

    End Function

    Private Sub CopyRow(ByRef sourceRow As DataRow, ByRef targetRow As DataRow, ByRef charge As OptimaElectricityChargeColumnInfo)

        targetRow(DeFormatInvoiceTableColumnsName.MeterSerial) = ""

        Dim mpan As String() = sourceRow("MPAN").ToString().Trim().Split(" ")

        If mpan.Length > 1 Then
            targetRow(DeFormatInvoiceTableColumnsName.MPAN) = mpan(1) & mpan(0)  ' Need to reverse the string

        Else
            targetRow(DeFormatInvoiceTableColumnsName.MPAN) = mpan(0)  ' Copy the same thing

        End If

        targetRow(DeFormatInvoiceTableColumnsName.InvoiceNumber) = sourceRow("Invoice Number")
        targetRow(DeFormatInvoiceTableColumnsName.TaxPointDate) = sourceRow("Invoice/Tax Point Date")
        targetRow(DeFormatInvoiceTableColumnsName.Tariff) = ""
        targetRow(DeFormatInvoiceTableColumnsName.StartDate) = sourceRow("Start Date")
        targetRow(DeFormatInvoiceTableColumnsName.EndDate) = sourceRow("Reading Date")
        targetRow(DeFormatInvoiceTableColumnsName.StartRead) = ""
        targetRow(DeFormatInvoiceTableColumnsName.EndRead) = ""
        targetRow(DeFormatInvoiceTableColumnsName.StartReadEstimated) = GetReadEstimation(sourceRow("Reading Type"))
        targetRow(DeFormatInvoiceTableColumnsName.StartReadEstimated) = GetReadEstimation(sourceRow("Reading Type"))
        targetRow(DeFormatInvoiceTableColumnsName.IsLastDayApportion) = "Yes"

        targetRow(DeFormatInvoiceTableColumnsName.Consumption) = If(Not String.IsNullOrEmpty(charge.ConsumptionColumnName), sourceRow(charge.ConsumptionColumnName), "")
        targetRow(DeFormatInvoiceTableColumnsName.ForcekWh) = charge.ForcekWh
        targetRow(DeFormatInvoiceTableColumnsName.CostOnly) = charge.IsCostOnly
        targetRow(DeFormatInvoiceTableColumnsName.Description) = charge.Description

        If charge.InvoiceRowType = OptimaElectricityInvoiceRowType.VAT Then

            targetRow(DeFormatInvoiceTableColumnsName.Vat) = sourceRow(charge.CostColumnName)
            targetRow(DeFormatInvoiceTableColumnsName.Net) = ""
            targetRow(DeFormatInvoiceTableColumnsName.Gross) = sourceRow(charge.CostColumnName)
        Else

            targetRow(DeFormatInvoiceTableColumnsName.Vat) = ""
            targetRow(DeFormatInvoiceTableColumnsName.Net) = sourceRow(charge.CostColumnName)
            targetRow(DeFormatInvoiceTableColumnsName.Gross) = sourceRow(charge.CostColumnName)
        End If

    End Sub

#End Region

End Class