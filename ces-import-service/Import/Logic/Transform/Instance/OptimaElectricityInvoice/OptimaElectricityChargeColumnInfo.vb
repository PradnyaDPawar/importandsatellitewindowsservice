﻿''' <summary>
''' This class holds the information of specific optima electricity invoice charge.
''' </summary>
Public Class OptimaElectricityChargeColumnInfo

#Region "Member variables"

    Private _forcekWh As String = String.Empty
    Private _description As String = String.Empty
    Private _isCostOnly As String = String.Empty
    Private _consumptionColumnName As String = String.Empty
    Private _costColumnName As String = String.Empty
    Private _rowType As OptimaElectricityInvoiceRowType = OptimaElectricityInvoiceRowType.None

#End Region

#Region "Public properties"

    Public Property InvoiceRowType() As OptimaElectricityInvoiceRowType
        Get
            Return _rowType
        End Get
        Set(ByVal value As OptimaElectricityInvoiceRowType)
            _rowType = value
        End Set
    End Property

    Public Property ConsumptionColumnName() As String
        Get
            Return _consumptionColumnName
        End Get
        Set(ByVal value As String)
            _consumptionColumnName = value
        End Set
    End Property

    Public Property CostColumnName() As String
        Get
            Return _costColumnName
        End Get
        Set(ByVal value As String)
            _costColumnName = value
        End Set
    End Property

    Public Property IsCostOnly() As String
        Get
            Return _isCostOnly
        End Get
        Set(ByVal value As String)
            _isCostOnly = value
        End Set
    End Property

    Public Property ForcekWh() As String
        Get
            Return _forcekWh
        End Get
        Set(ByVal value As String)
            _forcekWh = value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

#End Region

End Class