﻿Imports System.Text.RegularExpressions

Public Class OptimaElectricityInvoiceSchemaHelper

    Public Shared Function CreateInvoiceTable() As DataTable

        Dim deInvoiceTable As New DataTable

        deInvoiceTable.Columns.Add("InvoiceNumber", GetType(String))
        deInvoiceTable.Columns.Add("MeterSerial", GetType(String))
        deInvoiceTable.Columns.Add("MPAN", GetType(String))
        deInvoiceTable.Columns.Add("Tariff", GetType(String))
        deInvoiceTable.Columns.Add("StartDate", GetType(String))
        deInvoiceTable.Columns.Add("EndDate", GetType(String))
        deInvoiceTable.Columns.Add("StartRead", GetType(String))
        deInvoiceTable.Columns.Add("StartReadEstimated", GetType(String))
        deInvoiceTable.Columns.Add("EndRead", GetType(String))
        deInvoiceTable.Columns.Add("EndReadEstimated", GetType(String))
        deInvoiceTable.Columns.Add("Consumption", GetType(String))
        deInvoiceTable.Columns.Add("ForcekWh", GetType(String))
        deInvoiceTable.Columns.Add("InvoiceRate", GetType(String))
        deInvoiceTable.Columns.Add("CostUnit", GetType(String))
        deInvoiceTable.Columns.Add("Net", GetType(String))
        deInvoiceTable.Columns.Add("Vat", GetType(String))
        deInvoiceTable.Columns.Add("Gross", GetType(String))
        deInvoiceTable.Columns.Add("CostOnly", GetType(String))
        deInvoiceTable.Columns.Add("Description", GetType(String))
        deInvoiceTable.Columns.Add("TaxPointDate", GetType(String))
        deInvoiceTable.Columns.Add("IsLastDayApportion", GetType(String))

        Return deInvoiceTable

    End Function

    Public Shared Function PrepareChargesColumnsSchema(ByRef dtInvoice As DataTable) As List(Of OptimaElectricityChargeColumnInfo)

        Dim chargesColumns As New List(Of OptimaElectricityChargeColumnInfo)

        ' 1. Adding Day unit
        AddChargeInList("Day Units kWh", "Day Units £", OptimaElectricityInvoiceRowType.DayCharge, chargesColumns, dtInvoice, True)

        ' 2. Adding night unit
        AddChargeInList("Night Units kWh", "Night Units £", OptimaElectricityInvoiceRowType.NightCharge, chargesColumns, dtInvoice, True)

        ' 3. Adding Max demand
        AddChargeInList("Max Demand (kW)", "MD Charge £", OptimaElectricityInvoiceRowType.MaxDemand, chargesColumns, dtInvoice, False)

        ' 4. Adding TRIAD MD
        AddChargeInList("TRIAD MD", "TRIAD MD Charge £", OptimaElectricityInvoiceRowType.TRIADMD, chargesColumns, dtInvoice, False)

        ' 5. Adding TRIAD MD Charge Adjustment
        AddChargeInList("TRIAD MD", "TRIAD MD Charge Adjustment £", OptimaElectricityInvoiceRowType.TRIADMDChargeAdjustment, chargesColumns, dtInvoice, False)

        ' 6. Adding Annual MD
        AddChargeInList("Annual MD", "Annual MD Charge £", OptimaElectricityInvoiceRowType.AnnualMD, chargesColumns, dtInvoice, False)

        ' 7. Adding Managed MD
        AddChargeInList("Managed MD", "Managed MD Charge £", OptimaElectricityInvoiceRowType.ManagedMD, chargesColumns, dtInvoice, False)

        ' 8. Adding Reactive charge
        AddChargeInList("kVArh", "Reactive Charge £", OptimaElectricityInvoiceRowType.ReactiveCharge, chargesColumns, dtInvoice, False)

        ' 9. Adding FFL
        AddChargeInList(String.Empty, "FFL £", OptimaElectricityInvoiceRowType.FFL, chargesColumns, dtInvoice, False)

        ' 10. Adding Ren. Obligation
        AddChargeInList("Ren. Obligation kWh", "Ren. Obligation £", OptimaElectricityInvoiceRowType.RenObligation, chargesColumns, dtInvoice, False)

        ' 11. Adding Bill Adjustment
        AddChargeInList(String.Empty, "Bill Adjustment £", OptimaElectricityInvoiceRowType.BillAdjustment, chargesColumns, dtInvoice, False)

        ' 12. Adding Administration
        AddChargeInList(String.Empty, "Administration £", OptimaElectricityInvoiceRowType.Administration, chargesColumns, dtInvoice, False)

        ' 13. Adding Misc Charge
        AddChargeInList(String.Empty, "Misc Charge £", OptimaElectricityInvoiceRowType.MiscCharge, chargesColumns, dtInvoice, False)

        ' 14. Adding BSUoS Cost
        AddChargeInList("BSUoS Units", "BSUoS Cost (£)", OptimaElectricityInvoiceRowType.BSUoSCost, chargesColumns, dtInvoice, False)

        ' 15. Adding BSUoS Reconciliation Cost
        AddChargeInList("BSUoS Reconciliation Units", "BSUoS Reconciliation Cost (£)", OptimaElectricityInvoiceRowType.BSUoSReconciliationCost, chargesColumns, dtInvoice, False)

        ' 16. Adding Data Collection charge
        AddChargeInList(String.Empty, "Data Collection Cha", OptimaElectricityInvoiceRowType.DataCollectionCharge, chargesColumns, dtInvoice, False)

        ' 17. Adding Capacity #n
        Dim nthCapcaityColumnRegEx As New Regex("Capacity #\d+")
        For Each column As DataColumn In dtInvoice.Columns

            If nthCapcaityColumnRegEx.IsMatch(column.ColumnName) Then

                Dim capacityNumber As Integer = Convert.ToInt32(column.ColumnName.Substring(column.ColumnName.IndexOf("#") + 1, 1))
                AddChargeInList(column.ColumnName, "Capacity Charge #" + capacityNumber.ToString() + " £",
                                OptimaElectricityInvoiceRowType.NthCapacity, chargesColumns, dtInvoice, False)
            End If
        Next

        ' 18. Adding CCL Consumption #n
        Dim nthCCLConsumptionRegex As New Regex("CCL Consumption #\d+")
        For Each column As DataColumn In dtInvoice.Columns

            If nthCCLConsumptionRegex.IsMatch(column.ColumnName) Then

                Dim cclConsumptionNumber As Integer = Convert.ToInt32(column.ColumnName.Substring(column.ColumnName.IndexOf("#") + 1, 1))
                AddChargeInList(column.ColumnName, "CCL Charge £ #" + cclConsumptionNumber.ToString(),
                                OptimaElectricityInvoiceRowType.NthCCLConsumption, chargesColumns, dtInvoice, False)
            End If
        Next

        ' 19. Adding AAHEDC cost
        Dim nthAAHEDCConsumptionRegex As New Regex("AAHEDC Consumption #\d+")
        For Each column As DataColumn In dtInvoice.Columns

            If nthAAHEDCConsumptionRegex.IsMatch(column.ColumnName) Then

                Dim aahedcConsumptionNumber As Integer = Convert.ToInt32(column.ColumnName.Substring(column.ColumnName.IndexOf("#") + 1, 1))
                AddChargeInList(column.ColumnName, "AAHEDC Cost #" + aahedcConsumptionNumber.ToString() + " (£)",
                                OptimaElectricityInvoiceRowType.NthAAHEDCCost, chargesColumns, dtInvoice, False)
            End If
        Next

        ' 20. Adding Vat
        AddChargeInList(String.Empty, "VAT", OptimaElectricityInvoiceRowType.VAT, chargesColumns, dtInvoice, False)

        ' 21. Standing Charge
        AddChargeInList(String.Empty, "Standing Charge £", OptimaElectricityInvoiceRowType.StandingCharge, chargesColumns, dtInvoice, False)

        ' 22. DA/DC
        AddChargeInList(String.Empty, "DC/DA", OptimaElectricityInvoiceRowType.DA_DC, chargesColumns, dtInvoice, False)

        Return chargesColumns

    End Function

    Private Shared Sub AddChargeInList(ByVal consumptionColumnName As String,
                                       ByVal costColumnName As String,
                                       ByVal invoceRowType As OptimaElectricityInvoiceRowType,
                                       ByRef toAddColumnList As List(Of OptimaElectricityChargeColumnInfo),
                                       ByRef dtInvoice As DataTable, ByVal forceColumnCheck As Boolean)

        Dim consumptionAvailable As Boolean = IIf(Not String.IsNullOrEmpty(consumptionColumnName), IsColumnPresent(consumptionColumnName, dtInvoice), True)
        Dim costAvailable = IsColumnPresent(costColumnName, dtInvoice)
        ''Nikitah:using forceColumnCheck variable to add column(which is not present in file) to the list
        If (consumptionAvailable AndAlso costAvailable) Or (Not forceColumnCheck) Then
            toAddColumnList.Add(CreateChargeColumn(consumptionColumnName, costColumnName, invoceRowType))
        Else
            Throw New Exception("Consumption column '" + consumptionColumnName + "' and/or cost column '" + costColumnName + "' is not available in file.")
        End If

    End Sub

    Private Shared Function CreateChargeColumn(ByVal consumption As String, ByVal cost As String, ByVal transposeRowType As OptimaElectricityInvoiceRowType) As OptimaElectricityChargeColumnInfo

        Dim columnInfo As New OptimaElectricityChargeColumnInfo

        With columnInfo
            .ConsumptionColumnName = consumption
            .CostColumnName = cost
            .InvoiceRowType = transposeRowType
        End With

        Select Case transposeRowType

            Case OptimaElectricityInvoiceRowType.DayCharge
                With columnInfo
                    .ForcekWh = "Yes"
                    .IsCostOnly = "No"
                    .Description = "Day Units"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.NightCharge
                With columnInfo
                    .ForcekWh = "Yes"
                    .IsCostOnly = "No"
                    .Description = "Night Units"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.NthCapacity
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = String.Format("Capacity #{0}", consumption.Substring(consumption.IndexOf("#") + 1, 1))
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.MaxDemand
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "Max Demand"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.TRIADMD
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "TRIAD MD"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.TRIADMDChargeAdjustment
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "TRIAD MD Charge Adjustment"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.NthCCLConsumption
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = String.Format("CCL Charge #{0}", consumption.Substring(consumption.IndexOf("#") + 1, 1))
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.AnnualMD
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "Annual MD"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.ManagedMD
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "Managed MD"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.ReactiveCharge
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "Reactive Charge"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.FFL
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "FFL "
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.RenObligation
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "Ren. Obligation"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.BillAdjustment
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "Bill Adjustment"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.Administration
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "Administration"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.MiscCharge
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "Misc Charge"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.BSUoSCost
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "BSUoS Cost"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.BSUoSReconciliationCost
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "BSUoS Reconciliation Cost"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.NthAAHEDCCost
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = String.Format("AAHEDC Cost #{0} (£)", consumption.Substring(consumption.IndexOf("#") + 1, 1))
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.DataCollectionCharge
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "Data Collection Charge"
                End With

                Exit Select

            Case OptimaElectricityInvoiceRowType.VAT
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "VAT"
                End With
                Exit Select
            Case OptimaElectricityInvoiceRowType.StandingCharge

                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "Standing Charge"
                End With
                Exit Select
            Case OptimaElectricityInvoiceRowType.DA_DC
                With columnInfo
                    .ForcekWh = "No"
                    .IsCostOnly = "Yes"
                    .Description = "DA/DC"
                End With
                Exit Select

        End Select

        Return columnInfo

    End Function

    Private Shared Function IsColumnPresent(ByVal columnName As String, ByRef dtToCheck As DataTable) As Boolean

        Return dtToCheck.Columns.Contains(columnName)

    End Function

End Class