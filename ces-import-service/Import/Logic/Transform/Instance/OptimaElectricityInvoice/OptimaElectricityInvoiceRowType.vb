﻿''' <summary>
''' Enum describing the type of charges an optima electicity invoice has.
''' </summary>
''' <remarks>Total 19 types of invoice charges are present</remarks>
Public Enum OptimaElectricityInvoiceRowType

    None = -1
    DayCharge = 0
    NightCharge
    NthCapacity
    MaxDemand
    TRIADMD
    TRIADMDChargeAdjustment
    NthCCLConsumption
    AnnualMD
    ManagedMD
    ReactiveCharge
    FFL
    RenObligation
    BillAdjustment
    Administration
    MiscCharge
    BSUoSCost
    BSUoSReconciliationCost
    NthAAHEDCCost
    DataCollectionCharge
    VAT
    StandingCharge
    DA_DC

End Enum