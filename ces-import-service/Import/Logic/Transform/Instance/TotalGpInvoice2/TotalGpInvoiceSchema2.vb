﻿Public Class TotalGpInvoiceSchema2

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        Dim invoiceTbl As DataTable = DeFormatTableTemplates.CreateInvoiceTable
        PopulateInvoiceTbl(invoiceTbl, dt)
        dt = invoiceTbl
        RenameColumns(dt)
    End Sub

    Private Shared Sub PopulateInvoiceTbl(ByRef invoiceTbl As DataTable, ByVal dt As DataTable)

        For Each row As DataRow In dt.Rows
            If Not String.IsNullOrEmpty(row("Meter Ser No")) Then
                PopulateGasCharge(invoiceTbl, row)
                PopulateCclCharge(invoiceTbl, row)
            Else
                PopulateStandingCharge(invoiceTbl, row)
            End If
        Next
    End Sub

    Private Shared Sub PopulateGasCharge(ByRef invoiceTbl As DataTable, ByVal row As DataRow)

        Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow
        Dim gasVat, gasCharge As Double
        newInvoiceTblRow("TaxPointDate") = row("Print Date")
        newInvoiceTblRow("Invoice Number") = row("Bill Number")
        newInvoiceTblRow("Meter Serial") = row("Meter Ser No")
        newInvoiceTblRow("MPAN") = row("MPR")
        newInvoiceTblRow("Start Date") = row("Previous Read Date")
        newInvoiceTblRow("End Date") = row("Read Date")
        newInvoiceTblRow("Start Read") = row("Previous Billed Reading")
        newInvoiceTblRow("Start Read Estimated") = GetReadEstimated(row("Prev Read Type"))
        newInvoiceTblRow("End Read") = row("Billed Reading")
        newInvoiceTblRow("End Read Estimated") = GetReadEstimated(row("Read Type"))
        newInvoiceTblRow("Consumption") = row("Consumption Kwh")
        newInvoiceTblRow("Force Kwh") = "Yes"

        gasCharge = row("Gas Charge").Replace("£", "")
        newInvoiceTblRow("Net") = gasCharge

        gasVat = row("Total VAT").Replace("£", "")
        newInvoiceTblRow("Vat") = gasVat
        newInvoiceTblRow("Gross") = gasVat + gasCharge

        newInvoiceTblRow("Cost Only") = "No"
        newInvoiceTblRow("Rate") = "Gas Charge"
        newInvoiceTblRow("Description") = "Gas Charge"
        newInvoiceTblRow("IsLastDayApportion") = "No"

        invoiceTbl.Rows.Add(newInvoiceTblRow)
    End Sub

    Private Shared Sub PopulateCclCharge(ByRef invoiceTbl As DataTable, ByVal row As DataRow)

        Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow

        Dim cclCharge As Double
        newInvoiceTblRow("TaxPointDate") = row("Print Date")
        newInvoiceTblRow("Invoice Number") = row("Bill Number")
        newInvoiceTblRow("Meter Serial") = row("Meter Ser No")
        newInvoiceTblRow("MPAN") = row("MPR")
        newInvoiceTblRow("Start Date") = row("Previous Read Date")
        newInvoiceTblRow("End Date") = row("Read Date")
        newInvoiceTblRow("Start Read") = ""
        newInvoiceTblRow("Start Read Estimated") = ""
        newInvoiceTblRow("End Read") = ""
        newInvoiceTblRow("End Read Estimated") = ""
        newInvoiceTblRow("Consumption") = ""
        newInvoiceTblRow("Force Kwh") = "Yes"

        cclCharge = row("CCL Charge").Replace("£", "")
        newInvoiceTblRow("Net") = cclCharge
        newInvoiceTblRow("Vat") = ""
        newInvoiceTblRow("Gross") = cclCharge

        newInvoiceTblRow("Cost Only") = "Yes"
        newInvoiceTblRow("Rate") = "CCL Charge"
        newInvoiceTblRow("Description") = "CCL Charge"
        newInvoiceTblRow("IsLastDayApportion") = "Yes"

        invoiceTbl.Rows.Add(newInvoiceTblRow)

    End Sub

    Private Shared Sub PopulateStandingCharge(ByRef invoiceTbl As DataTable, ByVal row As DataRow)

        Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow

        Dim scVat, standingCharge As Double
        newInvoiceTblRow("TaxPointDate") = row("Print Date")
        newInvoiceTblRow("Invoice Number") = row("Bill Number")
        newInvoiceTblRow("Meter Serial") = ""
        newInvoiceTblRow("MPAN") = row("MPR")
        newInvoiceTblRow("Start Date") = row("Charge Start Date")
        newInvoiceTblRow("End Date") = row("Charge End Date")
        newInvoiceTblRow("Start Read") = ""
        newInvoiceTblRow("Start Read Estimated") = ""
        newInvoiceTblRow("End Read") = ""
        newInvoiceTblRow("End Read Estimated") = ""
        newInvoiceTblRow("Consumption") = ""
        newInvoiceTblRow("Force Kwh") = "Yes"

        standingCharge = row("Standing Charge").Replace("£", "")
        newInvoiceTblRow("Net") = standingCharge
        scVat = row("Total VAT").Replace("£", "")
        newInvoiceTblRow("Vat") = scVat
        newInvoiceTblRow("Gross") = standingCharge + scVat

        newInvoiceTblRow("Cost Only") = "Yes"
        newInvoiceTblRow("Rate") = "Standing Charge"
        newInvoiceTblRow("Description") = "Standing Charge"
        newInvoiceTblRow("IsLastDayApportion") = "Yes"

        invoiceTbl.Rows.Add(newInvoiceTblRow)
    End Sub

    Private Shared Function GetReadEstimated(ByVal code As String) As String

        If code.Trim = "PA" Then
            Return "A"
        Else
            Return code
        End If

    End Function

    Public Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        dt.Columns("Meter Serial").ColumnName = "MeterSerial"
        dt.Columns("Start Date").ColumnName = "StartDate"
        dt.Columns("Start Read").ColumnName = "StartRead"
        dt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        dt.Columns("Rate").ColumnName = "Tariff"
        dt.Columns("End Date").ColumnName = "EndDate"
        dt.Columns("End Read").ColumnName = "EndRead"
        dt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        dt.Columns("Force Kwh").ColumnName = "ForceKwh"
        dt.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub

End Class
