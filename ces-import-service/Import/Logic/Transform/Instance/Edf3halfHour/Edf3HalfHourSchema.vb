﻿Public Class Edf3HalfHourSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim dateholder As Date = Today()

        Dim headers As New List(Of String)
        headers.Add("Company Name")
        headers.Add("Site Name")
        headers.Add("Online Meter Name")
        headers.Add("MPAN")
        headers.Add("Meter ID")
        headers.Add("Type")
        headers.Add("Est")
        headers.Add("Date")

        For i = 0 To 47

            headers.Add(dateholder.AddMinutes(i * 30).ToString("HH:mm"))

        Next
        headers.Add("Spare1")
        headers.Add("Spare2")
        headers.Add("Spare3")
        headers.Add("Spare4")
        headers.Add("Spare5")

        Return headers

    End Function


    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        AddColumns(dt)
        PopulateUnits(dt)
        RenameColumns(dt)

        Return dt

    End Function

    Public Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")

    End Sub


    Public Shared Sub PopulateUnits(ByRef dt As DataTable)


        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            If IsDate(row("Date")) Then
                Dim dateholder As Date = Today()
                'Remove the - where present
                For i = 0 To 47

                    row(dateholder.AddMinutes(i * 30).ToString("HH:mm")) = row(dateholder.AddMinutes(i * 30).ToString("HH:mm")).ToString.Trim.Replace("-", "")

                Next

                If row("Type").ToString.ToLower = "kwh" Then

                    row("ForcekWh") = "Yes"
                Else
                    row("ForcekWh") = "No"

                End If

            Else

                row.Delete()

            End If

        Next

    End Sub

    Public Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("Meter ID").ColumnName = "MeterSerial"
        dt.Columns("Date").ColumnName = "Date(dd/MM/yyyy)"

    End Sub

End Class
