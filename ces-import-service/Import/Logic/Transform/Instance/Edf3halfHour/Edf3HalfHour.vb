﻿Public Class Edf3HalfHour
    Implements ITransformDelegate

    Private _returnTable As DataTable


    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property


    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvfile As New DelimitedFile(fullPath, 61)
        csvfile.HasHeader = True
        csvfile.LoadNonConsistentTable(_returnTable, Edf3HalfHourSchema.GetHeaders)
    End Sub



    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Edf3HalfHourSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next


    End Sub


End Class
