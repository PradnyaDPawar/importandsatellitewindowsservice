﻿Partial Public Class AirtricityHalfHourSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        AddColumns(dt)
        RenameColumns(dt)
        PopulateColumn(dt)
        RemoveColumn(dt)

        Return dt

    End Function
    Public Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("MeterSerial")
        dt.Columns.Add("00:00")

    End Sub

    Public Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("MPRN").ColumnName = "MPAN"
        dt.Columns("Read Date").ColumnName = "Date(dd/MM/yyyy)"

        Dim column As String

        For columncount As Integer = dt.Columns("301").Ordinal To dt.Columns("930").Ordinal

            If dt.Columns(columncount).ColumnName = "301" Then
                dt.Columns(columncount).ColumnName = "00:30"

            Else
                column = dt.Columns(columncount).ColumnName.Insert(0, "0")
                dt.Columns(columncount).ColumnName = column.Insert(2, ":")

            End If

        Next

        For columncount As Integer = dt.Columns("1000").Ordinal To dt.Columns("2330").Ordinal

            dt.Columns(columncount).ColumnName = dt.Columns(columncount).ColumnName.Insert(2, ":")

        Next

    End Sub

    Public Shared Sub PopulateColumn(ByRef dt As DataTable)

        Dim count As Integer = dt.Rows.Count

        Dim row As DataRow = dt.NewRow()
        dt.Rows.Add(row)

        For index As Integer = 1 To count

            For Each column As DataColumn In dt.Columns

                If column.ColumnName = "2400" Then

                    dt.Rows(index).SetField(dt.Columns("00:00"), dt.Rows(index - 1)(50).ToString())

                End If

            Next

        Next


    End Sub

    Public Shared Sub RemoveColumn(ByRef dt As DataTable)

        dt.Columns.Remove("2400")

    End Sub

End Class
