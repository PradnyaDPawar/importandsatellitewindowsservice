﻿Public Class AirtricityHalfHour
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel()
        excel.Load(fullPath)
        Dim xSheet As ExcelSheet = excel.Sheets(0)
        xSheet.HeaderRowIndex = 7
        ReturnTable = xSheet.GetDataTable()

    End Sub


    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        AirtricityHalfHourSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows

            If IsDate(row("Date(dd/MM/yyyy)")) Then
                row("ForcekWh") = "No"
                FE_HalfHour.Fill(row, dsEdits, importEdit)
            End If

        Next

    End Sub

End Class
