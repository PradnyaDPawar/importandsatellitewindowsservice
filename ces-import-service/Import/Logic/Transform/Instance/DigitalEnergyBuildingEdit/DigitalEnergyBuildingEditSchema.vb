﻿Public Class DigitalEnergyBuildingEditSchema
    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        DataTableTools.SetHeaders(dt, 1)
        DataTableTools.RemoveBlankRows(dt)
        Dim bldgTable As DataTable = DeFormatTableTemplates.CreateBuildingEditImportTable()
        'RenameColumnName(dt)
        IsAllColumnExist(dt, bldgTable)
        RemoveColumnNameSpaces(bldgTable)
        populatetable(bldgTable, dt)
        Return dt
    End Function

    Private Shared Sub IsAllColumnExist(ByRef dt As DataTable, ByRef bldgtable As DataTable)
        Dim dummy As DataTable = dt.Copy()
        For Each dtcol As DataColumn In dummy.Columns
           
            dtcol.ColumnName = dtcol.ColumnName.ToString().Replace(" ", "")
        Next
        For i As Integer = 0 To bldgtable.Columns.Count - 1
            If Not (dummy.Columns.Contains(bldgtable.Columns(i).ColumnName.ToString().Replace(" ", ""))) Then
                Throw New Exception("Column" + " '" + bldgtable.Columns(i).ColumnName + "' " + "is missing from the imported file. Please ensure this column is reinstated and try again")
            End If
        Next

    End Sub

    Private Shared Sub populatetable(ByRef bldg As DataTable, ByRef dt As DataTable)


        For Each row As DataRow In dt.Rows
            Dim newRow As DataRow = bldg.NewRow
            newRow("Name") = row("Name")
            newRow("Address1") = row("Address 1")
            newRow("Address2") = row("Address 2")
            newRow("Address3") = row("Address 3")
            newRow("Address4") = row("Address 4")
            newRow("Town") = row("Town")
            newRow("PostCode") = row("Post Code")
            newRow("CurrentUPRN") = row("Current UPRN")
            newRow("NewUPRN") = row("New UPRN")

            newRow("ParentOrganisation") = row("Parent Organisation")
            newRow("Occupier") = row("Occupier")
            newRow("Client") = row("Client")
            newRow("Group") = row("Group")
            bldg.Rows.Add(newRow)
        Next
        dt = bldg

    End Sub

    Private Shared Sub RenameColumnName(ByRef dt As DataTable)
        For Each dtcol As DataColumn In dt.Columns
            If dtcol.ColumnName = "Parent Organisation" Then
                dt.Columns("Parent Organisation").ColumnName = "CRC Undertaking"
            End If
        Next
    End Sub

    Private Shared Sub RemoveColumnNameSpaces(dtBuildingEdit As DataTable)

        For Each dtcol As DataColumn In dtBuildingEdit.Columns
            dtcol.ColumnName = dtcol.ColumnName.ToString().Replace(" ", "")
        Next

    End Sub
End Class
