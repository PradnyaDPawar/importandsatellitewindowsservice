﻿Public Class StarkFormat8HalfHour
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvFile As New DelimitedFile(fullPath, 100)
        csvFile.FieldsAreQuoted = True
        csvFile.LoadNonConsistentTable(_returnTable, GetHeaders(100))
    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        DataTableTools.RemoveTopRows(_returnTable, 6)
        DataTableTools.RemoveBlankRows(_returnTable)
        DataTableTools.SetHeaders(_returnTable, 1)
        AlterColumns()
        For Each row As DataRow In _returnTable.Rows
            row("forcekwh") = "Yes"
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
