﻿Partial Public Class StarkFormat8HalfHour

     Public Shared Function GetHeaders(ByVal columnCount As Integer) As List(Of String)
        Dim headers As New List(Of String)

        For index = 1 To columnCount
            headers.Add("Header" + index.ToString())
        Next
        Return headers
    End Function


    Private Sub AlterColumns()
        If Not _returnTable.Columns.Contains("forcekwh") Then
            _returnTable.Columns.Add("forcekwh")
        End If
        If Not _returnTable.Columns.Contains("MeterSerial") Then
            _returnTable.Columns.Add("MeterSerial")
        End If

        _returnTable.Columns("Date").ColumnName = "Date(dd/MM/yyyy)"
    End Sub
End Class
