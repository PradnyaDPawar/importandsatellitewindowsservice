﻿Public Class CompassInvoiceSchema

    Private _resultTable As DataTable
    Private _invoiceFuelCategory As CompassInvoiceFuelCategory
    Private _billTaxPointDates As Dictionary(Of String, String)
    Private _billMpanMeterSerial As Dictionary(Of String, List(Of String))

    Public Sub New()

        _resultTable = CreateDeInvoiceTable()
        _resultTable.TableName = "CompassInvoice"

        _invoiceFuelCategory = CompassInvoiceFuelCategory.None
        _billTaxPointDates = New Dictionary(Of String, String)
        _billMpanMeterSerial = New Dictionary(Of String, List(Of String))

    End Sub

    Public Property ResultTable() As DataTable
        Get
            Return _resultTable
        End Get
        Set(ByVal value As DataTable)
            _resultTable = value
        End Set
    End Property

    Public Property InvoiceFuelCategory() As CompassInvoiceFuelCategory
        Get
            Return _invoiceFuelCategory
        End Get
        Set(ByVal value As CompassInvoiceFuelCategory)
            _invoiceFuelCategory = value
        End Set
    End Property

    Public Property BillTaxPointDates() As Dictionary(Of String, String)
        Get
            Return _billTaxPointDates
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            _billTaxPointDates = value
        End Set
    End Property

    Public Property BillMpanMeterSerial() As Dictionary(Of String, List(Of String))
        Get
            Return _billMpanMeterSerial
        End Get
        Set(ByVal value As Dictionary(Of String, List(Of String)))
            _billMpanMeterSerial = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dtSummary As DataTable, ByRef dtDetail As DataTable)

        RemoveBlankColumnsRows(dtSummary, dtDetail)
        SetInvoiceFuelCategory(dtDetail)
        SaveBillTaxPointDates(dtSummary)
        SaveBillMpanAndMeterSerialNumber(dtDetail)
        PopulateDeInvoiceResultTable(dtDetail)

    End Sub

    Private Sub RemoveBlankColumnsRows(ByRef dtSummary As DataTable, ByRef dtDetail As DataTable)

        DataTableTools.RemoveBlankColumns(dtSummary)
        DataTableTools.RemoveBlankColumns(dtDetail)

        DataTableTools.RemoveBlankRows(dtSummary)
        DataTableTools.RemoveBlankRows(dtDetail)

    End Sub

    Private Sub SetHeader(ByRef dtSummary As DataTable, ByRef dtDetail As DataTable)

        DataTableTools.SetHeaders(dtSummary, 1)
        DataTableTools.SetHeaders(dtDetail, 1)

    End Sub

    Private Sub SetInvoiceFuelCategory(ByRef dtDetail As DataTable)

        If dtDetail.Columns.Contains("MPAN") Then
            _invoiceFuelCategory = CompassInvoiceFuelCategory.Electricity

        ElseIf dtDetail.Columns.Contains("Meter Point ID") Then
            _invoiceFuelCategory = CompassInvoiceFuelCategory.Gas

        End If

    End Sub

    Private Sub SaveBillTaxPointDates(ByRef dtSummary As DataTable)

        For Each row As DataRow In dtSummary.Rows

            If Not BillTaxPointDates.ContainsKey(row("Bill Number")) Then

                BillTaxPointDates.Add(row("Bill Number"), row("Dated"))

            End If

        Next

    End Sub

    Private Sub SaveBillMpanAndMeterSerialNumber(ByRef dtDetail As DataTable)

        If InvoiceFuelCategory <> CompassInvoiceFuelCategory.None Then

            For Each row As DataRow In dtDetail.Rows

                If Not BillMpanMeterSerial.ContainsKey(row("Bill No.")) Then

                    Dim mpanAndMeterSerial As New List(Of String)

                    With mpanAndMeterSerial

                        'MPAN
                        If InvoiceFuelCategory = CompassInvoiceFuelCategory.Electricity Then
                            .Add(row("MPAN").ToString())

                        ElseIf InvoiceFuelCategory = CompassInvoiceFuelCategory.Gas Then
                            .Add(row("Meter Point ID").ToString())

                        End If

                        'Meter Serial Number
                        .Add(row("Meter No"))

                    End With

                    BillMpanMeterSerial.Add(row("Bill No.").ToString(), mpanAndMeterSerial)

                End If

            Next

        End If

    End Sub

    Private Sub PopulateDeInvoiceResultTable(ByRef dtDetail As DataTable)

        If InvoiceFuelCategory <> CompassInvoiceFuelCategory.None Then

            For index As Integer = 0 To dtDetail.Rows.Count - 1 Step 1

                Dim row As DataRow = dtDetail.Rows(index)
                Dim newResultRow As DataRow = ResultTable.NewRow()

                'Invoice Number
                newResultRow(DeFormatInvoiceTableColumnsName.InvoiceNumber) = row("Bill No.")

                'MPAN And Meter Serial From dictionary
                newResultRow(DeFormatInvoiceTableColumnsName.MPAN) = BillMpanMeterSerial(row("Bill No."))(0)
                newResultRow(DeFormatInvoiceTableColumnsName.MeterSerial) = BillMpanMeterSerial(row("Bill No."))(1)

                'Tariff
                newResultRow(DeFormatInvoiceTableColumnsName.Tariff) = row("Line Description")

                'Tax Point Date
                newResultRow(DeFormatInvoiceTableColumnsName.TaxPointDate) = BillTaxPointDates(row("Bill No."))

                'Start Date
                newResultRow(DeFormatInvoiceTableColumnsName.StartDate) = row("Start Read Date")

                'End Date
                newResultRow(DeFormatInvoiceTableColumnsName.EndDate) = row("End Read Date")

                'Start Read
                newResultRow(DeFormatInvoiceTableColumnsName.StartRead) = row("Start Read")

                'End Read
                newResultRow(DeFormatInvoiceTableColumnsName.EndRead) = row("End Read")

                'Start Read Estimated
                newResultRow(DeFormatInvoiceTableColumnsName.StartReadEstimated) = row("Start Read Type")

                'End Read Estimated
                newResultRow(DeFormatInvoiceTableColumnsName.EndReadEstimated) = row("End Read Type")

                Dim vatRate As Double = 0.0
                Double.TryParse(row("Vat Rate").ToString(), vatRate)

                Dim net As Double = 0.0
                Double.TryParse(row("Line Value").ToString().Replace("£", ""), net)

                'Consumption
                newResultRow(DeFormatInvoiceTableColumnsName.Consumption) = IIf(net >= 0,
                                                                                row("Billable Units"),
                                                                                Convert.ToDouble(row("Billable Units")) * -1)

                'Force kWh
                newResultRow(DeFormatInvoiceTableColumnsName.ForcekWh) = IIf(row("Unit Type").ToString().ToLower() = "p/kwh", "Yes", "No")

                'Vat
                newResultRow(DeFormatInvoiceTableColumnsName.Vat) = vatRate * net / 100

                'Net
                newResultRow(DeFormatInvoiceTableColumnsName.Net) = net

                'Gross
                newResultRow(DeFormatInvoiceTableColumnsName.Gross) = (vatRate * net / 100) + net

                'Cost Only
                newResultRow(DeFormatInvoiceTableColumnsName.CostOnly) = IIf(row("Unit Type").ToString().ToLower() = "p/kwh", "No", "Yes")

                'Description
                newResultRow(DeFormatInvoiceTableColumnsName.Description) = row("Line Description")

                'Is Last Day Apportion
                newResultRow(DeFormatInvoiceTableColumnsName.IsLastDayApportion) = "Yes"

                'Adding row in result table
                ResultTable.Rows.Add(newResultRow)
            Next

        Else

            Throw New Exception("Invalid File. Column 'MPAN/Meter No' missing.")
        End If

    End Sub

    Public Shared Function CreateDeInvoiceTable() As DataTable

        Dim deInvoiceTable As New DataTable

        deInvoiceTable.Columns.Add("InvoiceNumber", GetType(String))
        deInvoiceTable.Columns.Add("MeterSerial", GetType(String))
        deInvoiceTable.Columns.Add("MPAN", GetType(String))
        deInvoiceTable.Columns.Add("Tariff", GetType(String))
        deInvoiceTable.Columns.Add("StartDate", GetType(String))
        deInvoiceTable.Columns.Add("EndDate", GetType(String))
        deInvoiceTable.Columns.Add("StartRead", GetType(String))
        deInvoiceTable.Columns.Add("StartReadEstimated", GetType(String))
        deInvoiceTable.Columns.Add("EndRead", GetType(String))
        deInvoiceTable.Columns.Add("EndReadEstimated", GetType(String))
        deInvoiceTable.Columns.Add("Consumption", GetType(String))
        deInvoiceTable.Columns.Add("ForcekWh", GetType(String))
        deInvoiceTable.Columns.Add("Net", GetType(String))
        deInvoiceTable.Columns.Add("Vat", GetType(String))
        deInvoiceTable.Columns.Add("Gross", GetType(String))
        deInvoiceTable.Columns.Add("CostOnly", GetType(String))
        deInvoiceTable.Columns.Add("Description", GetType(String))
        deInvoiceTable.Columns.Add("TaxPointDate", GetType(String))
        deInvoiceTable.Columns.Add("IsLastDayApportion", GetType(String))

        Return deInvoiceTable

    End Function
End Class