﻿Public Class CompassInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable
    Private _summaryTable As DataTable

    Public Sub New()

    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTable
        End Get

        Set(value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel()

        excel.Load(fullPath)

        Dim xSheet1 As ExcelSheet = excel.Sheets(0)
        Dim xSheet2 As ExcelSheet = excel.Sheets(1)

        If excel.GetSheet("Detail", xSheet1) Then

            xSheet1.HeaderRowIndex = 1
            _returnTable = xSheet1.GetDataTable()
        Else

            Throw New ApplicationException("Unable to find the 'Detail' sheet.")
        End If

        If excel.GetSheet("Summary", xSheet2) Then

            xSheet2.HeaderRowIndex = 1
            _summaryTable = xSheet2.GetDataTable()
        Else

            Throw New ApplicationException("Unable to find the 'Summary' sheet.")
        End If

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Dim compassSchema As New CompassInvoiceSchema()
        compassSchema.ToDeFormat(_summaryTable, _returnTable)

        For Each row As DataRow In compassSchema.ResultTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next

    End Sub

End Class