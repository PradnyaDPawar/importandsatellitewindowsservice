﻿Public Class ScottishSouthernHalfHourSchema

    Public Shared Function ToDEFormat(ByRef tdt As DataTable)
        modifyColumnHeader(tdt)
        AddColumns(tdt)
        RenameColumns(tdt)
        Return tdt
    End Function
    Public Shared Sub AddColumns(ByRef dt As DataTable)
        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("MPAN")
    End Sub
    Private Shared Sub RenameColumns(ByRef tdt As DataTable)
        tdt.Columns("Date").ColumnName = "Date(DD/MM/YYYY)"
        tdt.Columns("Meter").ColumnName = "MeterSerial"
    End Sub
    Public Shared Function Validate(ByRef tdt As DataTable) As Boolean

        Dim listofdates As New List(Of Date)
        listofdates = GetAllDates(tdt)
        Dim listofdatetime As List(Of DateTime)
        listofdatetime = GetDateTimeEntries(listofdates)

        Dim ValidFlag As Boolean = False

        For Each datetimeentry As DateTime In listofdatetime

            For Each row As DataRow In tdt.Rows
                If IsDate(row(0)) Then
                    If datetimeentry = row(0) Then
                        ValidFlag = True
                    End If
                End If
            Next

            If Not ValidFlag Then
                Return False
            End If

            ValidFlag = False
        Next

        Return True

    End Function

    Private Shared Function GetAllDates(ByRef tdt As DataTable) As List(Of Date)
        Dim listofdates As New List(Of Date)
        Dim datetocheck As Date

        For Each row As DataRow In tdt.Rows

            If IsDate(row(0)) Then
                datetocheck = DateTime.Parse(row(0))
                If Not listofdates.Contains(datetocheck.Date) Then
                    listofdates.Add(datetocheck.Date)
                End If
            End If

        Next
        Return listofdates
    End Function
    Private Shared Function GetDateTimeEntries(ByVal dateEntries As List(Of DateTime)) As List(Of DateTime)
        Dim dateTimeEntries As New List(Of DateTime)

        For Each dateentry As DateTime In dateEntries
            dateTimeEntries.Add(dateentry)
            For addCount As Integer = 1 To 22
                dateentry = dateentry.AddMinutes(30)
                dateTimeEntries.Add(dateentry)
            Next
        Next

        Return dateTimeEntries
    End Function
   
    Private Shared Sub modifyColumnHeader(ByRef tdt As DataTable)
        For Each col As DataColumn In tdt.Columns
            If IsDate(col.ColumnName) Then
                col.ColumnName = Convert.ToDateTime(col.ColumnName).ToString("HH:mm")
            End If
        Next
    End Sub

End Class


