﻿Public Class Tma3HalfHour
    Implements ITransformDelegate

    Dim _returnTable As DataTable

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim csvFile As New DelimitedFile(fullPath, 54)
        csvFile.HasHeader = True
        csvFile.Load(_returnTable)

    End Sub

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Validate(_returnTable)

        AddColumns(_returnTable)

        RenameColumns(_returnTable)

        For Each row As DataRow In ReturnTable.Rows

            If row("Measurement Quantity").ToString.ToLower.Trim = "ai" Then
                row("ForcekWh") = "Yes"
                FE_HalfHour.Fill(row, dsEdits, importEdit)

            ElseIf row("Measurement Quantity").ToString.ToLower.Trim = "ae" Then
                row("ForcekWh") = "Yes"
                row("MeterSerial") = "e" & row("MeterSerial")
                FE_HalfHour.Fill(row, dsEdits, importEdit)
            End If

        Next

    End Sub
End Class
