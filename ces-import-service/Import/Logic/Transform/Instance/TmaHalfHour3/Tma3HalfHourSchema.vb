﻿Partial Public Class Tma3HalfHour

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("MPAN")
        headers.Add("Outstation ID")
        headers.Add("Reference Number")
        headers.Add("Channel Number")
        headers.Add("Measurement Quantity")
        headers.Add("Date")

        For pCount As Integer = 1 To 48
            headers.Add("Period " & pCount.ToString())
        Next

        Return headers

    End Function

    Public Shared Function Validate(ByRef dt As DataTable) As Boolean

        For Each header As String In GetHeaders()
            If Not dt.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next

    End Function

    Public Shared Sub AddColumns(ByRef dt As DataTable)
        If Not dt.Columns.Contains("forcekwh") Then
            dt.Columns.Add("forcekwh")
        End If
    End Sub

    Public Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("Reference Number").ColumnName = "MeterSerial"
        dt.Columns("Date").ColumnName = "Date(DD/MM/YYYY)"
        RenameHalfHourlyEntries(dt)

    End Sub

    Private Shared Sub RenameHalfHourlyEntries(ByRef dt As DataTable)

        Dim dateholder As Date = DateTime.Today

        For columncount As Integer = 1 To 48

            dt.Columns("Period " & columncount.ToString).ColumnName = dateholder.ToString("HH:mm")

            dateholder = dateholder.AddMinutes(30)

        Next

    End Sub


End Class
