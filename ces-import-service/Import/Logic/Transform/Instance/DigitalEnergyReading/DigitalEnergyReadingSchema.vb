﻿Partial Public Class DigitalEnergyReading

    Private Shared Sub ReadingsTransform(ByRef importData As DataTable)

        If importData.Columns.Contains("Estimated") Then

            For Each row As DataRow In importData.Rows
                Select Case row("Estimated").ToString.ToLower

                    Case "a"
                        'the yes no case if handled by the validator
                        row("Estimated") = "No"

                    Case "r"
                        'the yes no case if handled by the validator
                        row("Estimated") = "No"

                    Case "actual"
                        row("Estimated") = "No"

                    Case "real"
                        row("Estimated") = "No"

                    Case "no"
                        row("Estimated") = "No"

                    Case "n"
                        row("Estimated") = "No"

                    Case Else
                        row("Estimated") = "Yes"

                End Select

            Next

        End If

        If importData.Columns.Contains("Rate") Then
            importData.Columns("Rate").ColumnName = "Register"
        End If

    End Sub

    Private Shared Sub ValidateImportData(ByRef importData As DataTable)

        Dim importValidator As New List(Of ImportItem)
        Dim msg As String = ""
        'define all the fields required by the meter edit
        importValidator.Add(New ImportItem("Meter Serial", "String", False))
        importValidator.Add(New ImportItem("Date", "String", True))
        importValidator.Add(New ImportItem("Reading", "Double", True))
        importValidator.Add(New ImportItem("Estimated", "String", False))
        importValidator.Add(New ImportItem("Is Wrap Round", "Boolean", False))
        importValidator.Add(New ImportItem("Is Reset", "Boolean", False))
        importValidator.Add(New ImportItem("Is Consumption", "Boolean", False))

        'validate importData against importItem definition
        For Each importItem As ImportItem In importValidator
            For Each dataRow As DataRow In importData.Rows
                msg = ""
                If dataRow.Table.Columns.Contains(importItem.Name) Then

                    If IsDBNull(dataRow(importItem.Name)) Then
                        dataRow(importItem.Name) = ""
                    End If

                    'if present, check if required
                    If importItem.IsRequired = True AndAlso dataRow(importItem.Name) = "" Then
                        msg += "Missing Column " & importItem.Name & ". |"

                    ElseIf importItem.IsRequired = True AndAlso dataRow(importItem.Name) <> "" Then

                        'Transform data
                        Transform(dataRow, importItem)

                        'check for correct type
                        
                        If Not ValidateDataType(dataRow, importItem.Name, importItem.DataType) Then
                            msg += importItem.Name + " is not in the right format. |"

                        End If

                        ElseIf importItem.IsRequired = False AndAlso IsDBNull(dataRow(importItem.Name)) Then
                            'doesn't need valid here if the column is add during import processing
                    ElseIf importItem.IsRequired = False Then

                        'Transform data
                        Transform(dataRow, importItem)

                        'if not required importItem, check for correct type if something has been entered

                        If Not ValidateDataType(dataRow, importItem.Name, importItem.DataType) Then
                            msg += importItem.Name + " is not in the right format. |"

                        End If
 
                        End If

                Else
                    msg += "Missing Column " & importItem.Name & ". |"
                    dataRow("message") = msg
                End If

            Next

        Next

    End Sub


    Private Shared Function ValidateDataType(ByRef dataRow As DataRow, ByVal fieldName As String, ByVal type As String) As Boolean

        Dim result As Boolean

        'validate the incoming value can be parsed into the required datatype
        Select Case type.ToLower

            Case "string"
                Return True

            Case "integer"
                dataRow.TryParseAsInteger(fieldName, result)
                Return result

            Case "double"
                dataRow.TryParseAsDouble(fieldName, result)
                Return result

            Case "date"
                dataRow.TryParseAsBritishDate(fieldName, result)
                Return result

            Case "boolean"
                dataRow.TryParseAsBoolean(fieldName, result)
                Return result

            Case Else

                Return False

        End Select

    End Function

    Private Shared Sub RenameColumns(ByRef importData As DataTable)

        ' transform column headings to a standard name

        For Each col As DataColumn In importData.Columns

            Select Case col.ColumnName
                'Meter Serial
                Case "serial"
                    col.ColumnName = "meterserial"

                Case "serialnumber"
                    col.ColumnName = "meterserial"

                Case "meternumber"
                    col.ColumnName = "meterserial"

                Case "meter serial"
                    col.ColumnName = "meterserial"

                Case "Meter Serial"
                    col.ColumnName = "meterserial"
                    'Mpan
                Case "mprn"
                    col.ColumnName = "mpan"

                Case "mpan/mprn"
                    col.ColumnName = "mpan"

                Case "mpr"
                    col.ColumnName = "mpan"

                Case "MPAN"
                    col.ColumnName = "mpan"

                    'ContractName
                Case "Contract Name"
                    col.ColumnName = "contractname"

                    'tariffName
                Case "Tariff Name"
                    col.ColumnName = "tariffname"

                    'Vat
                Case "vat(%)"
                    col.ColumnName = "VAT"

                Case "VAT(%)"
                    col.ColumnName = "VAT"

                    'Price
                Case "price(£)", "price", "Price"
                    col.ColumnName = "Price"


            End Select


        Next


    End Sub
    'add columns for the required IDs
    Private Shared Sub AddRequiredIDColumns(ByRef importData As DataTable)

        importData.Columns.Add("dialid")

    End Sub


    Private Shared Sub ValidateAddition(ByRef importData As DataTable)

        If Not importData.Columns.Contains("MPAN") Then
            importData.Columns.Add("MPAN")
        End If

        For Each row As DataRow In importData.Rows

            If IsDBNull(row("MPAN")) Then
                row("MPAN") = ""
            End If

            If row("Meter Serial") + row("MPAN") = "" Then
                Throw New ApplicationException("Either MPAN or Meter Serial is required ")
            End If

        Next

    End Sub


    Private Shared Sub Transform(ByRef data As DataRow, ByVal field As ImportItem)

        'turn yes no into true / false
        If field.DataType.ToLower = "boolean" Then

            Select Case data(field.Name).ToString.ToLower

                Case "yes"
                    data(field.Name) = "True"

                Case "no"
                    data(field.Name) = "False"

                Case ""
                    data(field.Name) = "False"

            End Select

        End If

    End Sub




End Class
