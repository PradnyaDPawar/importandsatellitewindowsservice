﻿Public Class DigitalEnergyReading
    Implements ITransformDelegate

    Private _returnTable As DataTable
    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel
        Dim xSheet As ExcelSheet
        excel.Load(fullPath)
        xSheet = excel.Sheets(0)
        excel.GetSheet("Reading", xSheet)
        xSheet.HeaderRowIndex = 1
        _returnTable = xSheet.GetDataTable()
        If _returnTable.Rows.Count > 4000 Then
            Throw New ApplicationException("Exceed 4000 rows")
        Else
            ReadingsTransform(_returnTable)
            ''ValidateImportData(_returnTable)
            'additional
            ValidateAddition(_returnTable)

            AddRequiredIDColumns(_returnTable)

            'Rename removing spaces and lower case
            For col As Integer = 0 To _returnTable.Columns.Count - 1
                _returnTable.Columns(col).ColumnName = _returnTable.Columns(col).ColumnName.ToLower().Replace(" ", "")
            Next
        End If
    End Sub

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        For Each row As DataRow In _returnTable.Rows
            FE_Reading.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
