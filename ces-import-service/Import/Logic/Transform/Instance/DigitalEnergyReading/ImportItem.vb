﻿Public Class ImportItem

    Private _Name As String
    Private _IsRequired As Boolean
    Private _DataType As String
    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal value As String)

            _Name = value

        End Set
    End Property

    Public Property IsRequired() As Boolean
        Get
            Return IsRequired
        End Get
        Set(ByVal value As Boolean)

            _IsRequired = value

        End Set
    End Property

    Public Property DataType() As String
        Get
            Return _DataType
        End Get
        Set(ByVal value As String)

            _DataType = value.Replace(" ", "").ToLower

        End Set
    End Property


    Public Sub New(ByVal name As String, ByVal type As String, ByVal isRequired As Boolean)


        Me.Name = name
        Me.DataType = type
        Me.IsRequired = isRequired

    End Sub
End Class
