﻿Imports System.Runtime.CompilerServices

Public Module TryParseExtention

#Region "DataRow Extentions"

    'these extentions allow typed retrival of a field from a datarow.  If the value cannot be parsed, a default is returned

    <Extension()> _
    Public Function TryParseAsInteger(ByVal dataRow As DataRow, ByVal field As String, ByRef result As Boolean) As Integer

        Dim returnValue As Integer

        'parse the value into an inetger
        If Integer.TryParse(dataRow(field).ToString(), returnValue) Then
            result = True

        Else
            returnValue = 0
            result = False

        End If

        Return returnValue

    End Function

    <Extension()> _
    Public Function TryParseAsDouble(ByVal dataRow As DataRow, ByVal field As String, ByRef result As Boolean) As Double

        Dim returnValue As Double

        'parse the value into an double
        If Double.TryParse(dataRow(field).ToString(), returnValue) Then
            result = True

        Else
            returnValue = 0.0
            result = False

        End If

        Return returnValue

    End Function

    <Extension()> _
    Public Function TryParseAsDouble(ByVal dataRow As DataRow, ByVal field As String, ByVal defaultValue As Double, ByRef result As Boolean) As Double

        Dim returnValue As Double

        'parse the value into an double
        If Double.TryParse(dataRow(field).ToString(), returnValue) Then
            result = True

        Else
            returnValue = defaultValue
            result = False

        End If

        Return returnValue

    End Function


    <Extension()> _
    Public Function TryParseAsBoolean(ByVal dataRow As DataRow, ByVal field As String, ByVal defaultValue As Boolean, ByRef result As Boolean) As Boolean

        Dim returnValue As Boolean

        'parse the value into an date
        If Boolean.TryParse(dataRow(field).ToString(), returnValue) Then
            result = True

        Else
            returnValue = defaultValue
            result = False

        End If

        Return returnValue

    End Function

    <Extension()> _
    Public Function TryParseAsBoolean(ByVal dataRow As DataRow, ByVal field As String, ByRef result As Boolean) As Boolean

        Dim returnValue As Boolean

        'parse the value into an date
        If Boolean.TryParse(dataRow(field).ToString(), returnValue) Then
            result = True

        Else
            returnValue = False
            result = False

        End If

        Return returnValue

    End Function

    <Extension()> _
    Public Function TryParseAsBritishDate(ByVal dataRow As DataRow, ByVal field As String, ByRef result As Boolean) As Date

        'try to parse as british date 
        Dim returnValue As Date
        Dim cultureInfo As New System.Globalization.CultureInfo("en-GB")

        If Date.TryParse(dataRow(field), cultureInfo, System.Globalization.DateTimeStyles.None, returnValue) Then

            result = True

        Else
            'if parse fail default date
            returnValue = "01/01/2010 00:00:00"
            result = False

        End If

        Return returnValue

    End Function

#End Region


End Module
