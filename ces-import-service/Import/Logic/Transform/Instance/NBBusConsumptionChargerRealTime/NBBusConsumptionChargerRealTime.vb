﻿
Public Class NBBusConsumptionChargerRealTime
    Implements ITransformDelegate

    Private _returnTable As DataTable
    Private _IsConsumption As Boolean
    Private _fileName As String

    Public Sub New(IsConsumption As Boolean)

        _IsConsumption = IsConsumption

    End Sub

    Public Property FileName() As String
        Get
            Return _fileName
        End Get
        Set(value As String)
            _fileName = value
        End Set
    End Property


    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property


    Public Sub LoadData(ByVal stagingFilePath As String) Implements ITransformDelegate.LoadData

        'load csv file
        Dim csvFile As New DelimitedFile(stagingFilePath, 0)
        csvFile.Load(_returnTable)
        FileName = IO.Path.GetFileName(stagingFilePath)

    End Sub


    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat


        'validate the new table
        Validate()
        ToDeFormat(ReturnTable)
        'rename 
        RenameColumns()

        For Each row As DataRow In _returnTable.Rows
            FE_RealTime.Fill(row, dsEdits, importEdit)
        Next

    End Sub




End Class
