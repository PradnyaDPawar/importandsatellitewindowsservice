﻿Partial Public Class NBBusConsumptionChargerRealTime

    Public Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("Start Time")
        headers.Add("Start Date")
        headers.Add("VRN")
        headers.Add("Fleet No")
        headers.Add("Charge Station")
        headers.Add("kWh Start")
        headers.Add("kWh End")
        headers.Add("kWh Taken")
        headers.Add("Distance")
        headers.Add("Mins Charged")
        headers.Add("End Time")
        headers.Add("End Date")

        Return headers

    End Function


    Public Function Validate() As Boolean
        Return ValidateHeaders(GetHeaders())
    End Function

    Private Function ValidateHeaders(ByRef headers As List(Of String)) As Boolean
        For Each header As String In headers
            If Not _returnTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next
    End Function

    Public Sub ToDeFormat(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows
            If _IsConsumption Then
                row("VRN") = row("VRN").ToString.Trim
            Else
                'Is Charger
                row("Charge Station") = IIf(FileName.ToLower.Contains("sherwood"), "Sh_" & row("Charge Station").ToString.Trim, row("Charge Station").ToString.Trim)
            End If
            Dim datetimeHolder As DateTime
            Dim time As DateTime
            datetimeHolder = DateTime.ParseExact(row("End Date").ToString.Trim, "dd/MM/yy", System.Globalization.CultureInfo.InvariantCulture)
            time = DateTime.ParseExact(row("End Time").ToString.Trim, "HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture)
            datetimeHolder = datetimeHolder.Add(time.TimeOfDay)
            row("End Date") = datetimeHolder.ToString("dd/MM/yyyy HH:mm")
        Next


    End Sub


    Public Sub RenameColumns()
        If _returnTable.Columns.Contains("End Date") Then
            _returnTable.Columns("End Date").ColumnName = "datetime"
        End If
        If _returnTable.Columns.Contains("kWh Taken") Then
            _returnTable.Columns("kWh Taken").ColumnName = "countunits"
        End If
        If _IsConsumption Then
            If _returnTable.Columns.Contains("VRN") Then
                _returnTable.Columns("VRN").ColumnName = "metername"
            End If
        Else
            If _returnTable.Columns.Contains("Charge Station") Then
                _returnTable.Columns("Charge Station").ColumnName = "metername"
            End If
        End If

    End Sub



End Class
