﻿Imports System.Globalization

Public Class SystemLinkGasCsvInvoiceSchema
    
    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        Dim lstGasDesc As List(Of String) = getGasDescription()
        Dim standardInvoiceTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()

        For Each column As DataColumn In dt.Columns
            column.ColumnName = column.ToString().Trim().Replace(" ", "")
        Next
       
        For Each row As DataRow In dt.Rows

            For Each item As String In lstGasDesc
                Dim invoiceTableRow As DataRow = standardInvoiceTable.NewRow
                invoiceTableRow("Invoice Number") = PopulateInvoiceNumber(row)
                invoiceTableRow("Meter Serial") = row("Code")
                invoiceTableRow("MPAN") = ""
                invoiceTableRow("Rate") = "" 'IIf(item.Contains("M1"), "M1", "")
                invoiceTableRow("Start Date") = Convert.ToDateTime(row("Date_Previous")).AddDays(1).ToString("dd-MM-yy")
                invoiceTableRow("End Date") = Convert.ToDateTime(row("Date")).ToString("dd-MM-yy")
                invoiceTableRow("Start Read") = IIf(item.Contains("M1"), row("M1_Previous"), "")
                invoiceTableRow("Start Read Estimated") = IIf(String.IsNullOrEmpty(row("Estimate")), "A", row("Estimate"))
                invoiceTableRow("End Read") = IIf(item.Contains("M1"), row("M1_Present"), "")
                invoiceTableRow("End Read Estimated") = IIf(String.IsNullOrEmpty(row("Estimate")), "A", row("Estimate"))
                invoiceTableRow("Consumption") = getConsumption(item, row)
                invoiceTableRow("Force kWh") = "Yes"
                invoiceTableRow("Net") = IIf(checkVatNet(item), 0, getCost(item, row))
                invoiceTableRow("Vat") = IIf(checkVatNet(item), getCost(item, row), 0)
                invoiceTableRow("Gross") = getGross(invoiceTableRow("Net"), invoiceTableRow("Vat"))
                invoiceTableRow("Cost Only") = IIf(item = "M1_Units", "No", "Yes")
                invoiceTableRow("Description") = item
                invoiceTableRow("TaxPointDate") = ""
                invoiceTableRow("IsLastDayApportion") = "Yes"
                standardInvoiceTable.Rows.Add(invoiceTableRow)
            Next
        Next
        RenameColumns(standardInvoiceTable)
        DataTableTools.SortDataTable(standardInvoiceTable, "InvoiceNumber")
        dt = standardInvoiceTable
        Return dt
    End Function

    Public Shared Function getGasDescription() As List(Of String)
        Dim lstGasDescription As List(Of String) = New List(Of String)
        lstGasDescription.Add("M1_Units")
        lstGasDescription.Add("M1_Cost_rate")
        lstGasDescription.Add("Climate_Change_Levy")
        lstGasDescription.Add("Standing_Charge")
        lstGasDescription.Add("Other_Charge")
        lstGasDescription.Add("VAT_Split")
        lstGasDescription.Add("VAT_2")
        lstGasDescription.Add("VAT_1")
        lstGasDescription.Add("M1_Factor_2")
        Return lstGasDescription
    End Function

    Private Shared Function PopulateInvoiceNumber(row As DataRow) As String
        Return row("Code") + " " + row("Invoice_Number") + "_" + row("SupplierName")
    End Function

    Private Shared Function getConsumption(ByRef item As String, ByRef row As DataRow) As String
        Dim strConsumption As String = String.Empty
        Select Case (item)
            Case "M1_Units"
                strConsumption = row("M1_Units")
            Case "M1_Cost_rate"
                strConsumption = row("M1_Cost_rate")
            Case "Climate_Change_Levy"
                strConsumption = row("Climate_Change_Levy_Rate")
            Case "Standing_Charge"
                strConsumption = row("Standing_Charge_Rate")
            Case "Other_Charge"
                strConsumption = ""
            Case "VAT_Split"
                strConsumption = row("VAT_Split")
            Case "VAT_2"
                strConsumption = row("VAT_2_Percent")
            Case "VAT_1"
                strConsumption = row("VAT_1_Percent")
            Case "M1_Factor_2"
                strConsumption = row("M1_Factor_2")
        End Select
        Return strConsumption.Replace("�", "")
    End Function

    Private Shared Function checkVatNet(item As String) As Boolean
        Dim isVat As Boolean = False
        Select Case (item)
            Case "VAT_1"
                isVat = True
            Case "VAT_2"
                isVat = True
            Case "VAT_Split"
                isVat = True
            Case Else
                isVat = False
        End Select
        Return isVat
    End Function

    Private Shared Function getCost(ByRef item As String, ByRef row As DataRow) As Object
        Dim strCost As String = String.Empty
        Select Case (item)

            'VAT_Split, M1_Factor_1/2 are rates and not costs, needs to be recorded in consumption column for the purpose of this transpose only

            Case "M1_Units"
                strCost = CheckBlank(Convert.ToString(row("M1_Cost")))
            Case "M1_Cost_rate"
                strCost = "0"
            Case "Climate_Change_Levy"
                strCost = CheckBlank(Convert.ToString(row("Climate_Change_Levy")))
            Case "Standing_Charge"
                strCost = CheckBlank(Convert.ToString(row("Standing_Charge")))
            Case "VAT_Split"
                strCost = 0
            Case "VAT_2"
                strCost = CheckBlank(Convert.ToString(row("VAT_2")))
            Case "VAT_1"
                strCost = CheckBlank(Convert.ToString(row("VAT_1")))
            Case "M1_Factor_2", "M1_Factor_1"
                strCost = 0
            Case "Other_Charge"
                strCost = CheckBlank(CStr(row("Other_Charge")))
        End Select
        Return strCost.Replace("�", "")
    End Function

    Private Shared Function CheckBlank(ByRef value As String) As String
        Dim strCost As String = String.Empty
        If IsNumeric(value.Replace("�", "")) Then
            strCost = value
        Else
            strCost = "0"
        End If
        Return strCost.Replace("�", "")
    End Function

    Private Shared Function getGross(ByRef net As String, ByRef vat As String) As String
        Dim strGross As String = String.Empty
        strGross = Convert.ToString(Convert.ToDouble(net) + Convert.ToDouble(vat))
        Return strGross
    End Function

    Private Shared Sub RenameColumns(ByRef dt As DataTable)
        dt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        dt.Columns("Meter Serial").ColumnName = "MeterSerial"
        dt.Columns("Rate").ColumnName = "Tariff"
        dt.Columns("Start Date").ColumnName = "StartDate"
        dt.Columns("End Date").ColumnName = "EndDate"
        dt.Columns("Start Read").ColumnName = "StartRead"
        dt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        dt.Columns("End Read").ColumnName = "EndRead"
        dt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        dt.Columns("Force kWh").ColumnName = "ForcekWh"
        dt.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub
End Class
