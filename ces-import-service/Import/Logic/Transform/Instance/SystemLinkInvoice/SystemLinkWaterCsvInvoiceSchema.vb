﻿Public Class SystemLinkWaterCsvInvoiceSchema
    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        Dim lstWaterDesc As List(Of String) = getWaterDescription()
        Dim standardInvoiceTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()

        For Each column As DataColumn In dt.Columns
            column.ColumnName = column.ToString().Trim().Replace(" ", "")
        Next

        For Each row As DataRow In dt.Rows

            For Each item As String In lstWaterDesc
                Dim invoiceTableRow As DataRow = standardInvoiceTable.NewRow
                invoiceTableRow("Invoice Number") = PopulateInvoiceNumber(row)
                invoiceTableRow("Meter Serial") = row("Code")
                invoiceTableRow("MPAN") = ""
                invoiceTableRow("Rate") = IIf(item.Contains("M1"), "M1", "")
                invoiceTableRow("Start Date") = Convert.ToDateTime(row("Date_Previous")).AddDays(1).ToString("dd-MM-yy")
                invoiceTableRow("End Date") = Convert.ToDateTime(row("Date")).ToString("dd-MM-yy")
                invoiceTableRow("Start Read") = IIf(item.Contains("M1"), row("M1_Previous"), "")
                invoiceTableRow("Start Read Estimated") = IIf(String.IsNullOrEmpty(row("Expr1013")), "A", row("Expr1013"))
                invoiceTableRow("End Read") = IIf(item.Contains("M1"), row("M1_Present"), "")
                invoiceTableRow("End Read Estimated") = IIf(String.IsNullOrEmpty(row("Estimate")), "A", row("Estimate"))
                invoiceTableRow("Consumption") = getConsumption(item, row)
                invoiceTableRow("Force kWh") = "No"
                invoiceTableRow("Net") = getCost(item, row)
                invoiceTableRow("Vat") = IIf(item = "Vat", IIf(String.IsNullOrEmpty(row("Vat")), 0, row("Vat")), 0)
                invoiceTableRow("Gross") = getGross(invoiceTableRow("Net"), invoiceTableRow("Vat"))
                invoiceTableRow("Cost Only") = IIf(item = "M1_Units", "No", "Yes")
                invoiceTableRow("Description") = item
                invoiceTableRow("TaxPointDate") = ""
                'Convert.ToDateTime(row("Last_Update")).ToString("dd-MM-yy")
                invoiceTableRow("IsLastDayApportion") = "Yes"
                standardInvoiceTable.Rows.Add(invoiceTableRow)
            Next
        Next
        RenameColumns(standardInvoiceTable)
        DataTableTools.SortDataTable(standardInvoiceTable, "InvoiceNumber")
        dt = standardInvoiceTable
        Return dt
    End Function

    Public Shared Function getWaterDescription() As List(Of String)
        Dim lstWaterDescription As List(Of String) = New List(Of String)
        lstWaterDescription.Add("M1_Units")
        lstWaterDescription.Add("Sewerage_Units")
        lstWaterDescription.Add("Sewerage_Factor")
        lstWaterDescription.Add("Water_Cost_Rate")
        lstWaterDescription.Add("Sewerage_Cost_Rate")
        lstWaterDescription.Add("Rates_Cost")
        lstWaterDescription.Add("Standing_Charge")
        lstWaterDescription.Add("Standing_Charge_2")
        lstWaterDescription.Add("Surface_Water_Cost")
        lstWaterDescription.Add("Other_Charge")
        lstWaterDescription.Add("Vat")
        Return lstWaterDescription
    End Function

    Private Shared Function PopulateInvoiceNumber(row As DataRow) As String
        Return row("Code") + " " + row("Invoice_Number") + "_" + row("SupplierName")
    End Function

    Private Shared Function getConsumption(ByRef item As String, ByRef row As DataRow) As String
        Dim strConsumption As String = String.Empty
        Select Case (item)
            Case "M1_Units"
                strConsumption = row("M1_Units")
            Case "Sewerage_Units"
                strConsumption = row("Sewerage_Units")
            Case "Sewerage_Factor"
                strConsumption = row("Sewerage_Factor")
            Case "Water_Cost_Rate"
                strConsumption = row("Water_Cost_Rate")
            Case "Sewerage_Cost_Rate"
                strConsumption = row("Sewerage_Cost_Rate")
            Case Else
                strConsumption = "0"
        End Select
        Return strConsumption.Replace("�", "")
    End Function

    Private Shared Function getCost(ByRef item As String, ByRef row As DataRow) As Object
        Dim strCost As String = String.Empty
        Select Case (item)
            Case "M1_Units"
                strCost = CheckBlank(Convert.ToString(row("Water_Cost")))
            Case "Sewerage_Units"
                strCost = CheckBlank(Convert.ToString(row("Sewerage_Cost")))
            Case "Sewerage_Factor"
                strCost = "0"
            Case "Water_Cost_Rate"
                strCost = "0"
            Case "Sewerage_Cost_Rate"
                strCost = "0"
            Case "Rates_Cost"
                strCost = CheckBlank(Convert.ToString(row("Rates_Cost")))
            Case "Standing_Charge"
                strCost = CheckBlank(Convert.ToString(row("Standing_Charge")))
            Case "Standing_Charge_2"
                strCost = CheckBlank(Convert.ToString(row("Standing_Charge_2")))
            Case "Surface_Water_Cost"
                strCost = CheckBlank(Convert.ToString(row("Surface_Water_Cost")))
            Case "Other_Charge"
                strCost = CheckBlank(CStr(row("Other_Charge")))
            Case "Vat"
                strCost = 0
        End Select
        Return strCost.Replace("�", "")
    End Function

    Private Shared Function CheckBlank(ByRef value As String) As String
        Dim strCost As String = String.Empty
        If IsNumeric(value.Replace("�", "")) Then
            strCost = value
        Else
            strCost = "0"
        End If
        Return strCost.Replace("�", "")
    End Function

    Private Shared Function getGross(ByRef net As String, ByRef vat As String) As String
        Dim strGross As String = String.Empty
        strGross = Convert.ToString(Convert.ToDouble(net) + Convert.ToDouble(vat))
        Return strGross
    End Function

    Private Shared Sub RenameColumns(ByRef dt As DataTable)
        dt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        dt.Columns("Meter Serial").ColumnName = "MeterSerial"
        dt.Columns("Rate").ColumnName = "Tariff"
        dt.Columns("Start Date").ColumnName = "StartDate"
        dt.Columns("End Date").ColumnName = "EndDate"
        dt.Columns("Start Read").ColumnName = "StartRead"
        dt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        dt.Columns("End Read").ColumnName = "EndRead"
        dt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        dt.Columns("Force kWh").ColumnName = "ForcekWh"
        dt.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub
End Class
