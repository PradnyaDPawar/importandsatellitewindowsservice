﻿Public Class SystemLinkElectCsvInvoiceSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("Code")
        headers.Add("Date")
        headers.Add("Number")
        headers.Add("Date_TaxPoint")
        headers.Add("Last_User")
        headers.Add("Date_Previous")
        headers.Add("Invoice_Number")
        headers.Add("SupplierName")
        headers.Add("M1_Previous")
        headers.Add("M1_Present")
        headers.Add("M2_Previous")
        headers.Add("M2_Present")
        headers.Add("M3_Previous")
        headers.Add("M3_Present")
        headers.Add("M1_Factor")
        headers.Add("M1_Units")
        headers.Add("M1_Split1")
        headers.Add("M2_Units")
        headers.Add("M3_Units")
        headers.Add("Units")
        headers.Add("M1_Cost_Rate")
        headers.Add("M1_Split1_Cost_Rate")
        headers.Add("M2_Cost_Rate")
        headers.Add("M3_Cost_Rate")
        headers.Add("M1_Cost")
        headers.Add("M1_Split1_Cost")
        headers.Add("M2_Cost")
        headers.Add("M3_Cost")
        headers.Add("TUOS")
        headers.Add("DUOS")
        headers.Add("DUOS_Red_Cost")
        headers.Add("DUOS_Amber_Cost")
        headers.Add("DUOS_Green_Cost")
        headers.Add("Settlements")
        headers.Add("Feed_In_Cost")
        headers.Add("Climate_Change_Levy_Rate")
        headers.Add("Climate_Change_Levy")
        headers.Add("Standing_Charge")
        headers.Add("kVA_Cost")
        headers.Add("Reactive_Power_Cost")
        headers.Add("Other_Charge")
        headers.Add("VAT_Split")
        headers.Add("VAT_2_Percent")
        headers.Add("VAT_1_Percent")
        headers.Add("VAT_2")
        headers.Add("VAT_1")
        headers.Add("Cost")
        headers.Add("Screen_Format")

        Return headers
    End Function

    Shared Sub ToDeFormat(ByRef returnTableElect As DataTable)

        FormatColumnHeaders(returnTableElect)
        DataTableTools.RemoveBlankRows(returnTableElect)
        'DataTableTools.RemoveBlankColumns(returnTableElect)

        Dim invoiceDataTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        CreateInvoiceEntries(returnTableElect, invoiceDataTable)

        RenameColumns(invoiceDataTable)
        returnTableElect = invoiceDataTable

    End Sub

    Private Shared Sub CreateInvoiceEntries(ByRef returnTableElect As DataTable, ByRef invoiceDataTable As DataTable)
        For Each row As DataRow In returnTableElect.Rows
            PopulateEntries(row, invoiceDataTable, "M1_Units")
            'PopulateEntries(row, invoiceDataTable, "M1_Split1")
            PopulateEntries(row, invoiceDataTable, "M2_Units")
            PopulateEntries(row, invoiceDataTable, "M3_Units")
            PopulateEntries(row, invoiceDataTable, "M1_Cost_Rate")
            'PopulateEntries(row, invoiceDataTable, "M1_Split1_Cost_Rate")
            PopulateEntries(row, invoiceDataTable, "M2_Cost_Rate")
            PopulateEntries(row, invoiceDataTable, "M3_Cost_Rate")
            PopulateEntries(row, invoiceDataTable, "TUOS")
            PopulateEntries(row, invoiceDataTable, "DUOS")
            PopulateEntries(row, invoiceDataTable, "DUOS_Red_Cost")
            PopulateEntries(row, invoiceDataTable, "DUOS_Amber_Cost")
            PopulateEntries(row, invoiceDataTable, "DUOS_Green_Cost")
            PopulateEntries(row, invoiceDataTable, "Settlements")
            PopulateEntries(row, invoiceDataTable, "Feed_In_Cost")
            'PopulateEntries(row, invoiceDataTable, "Climate_Change_Levy_Rate")
            PopulateEntries(row, invoiceDataTable, "Climate_Change_Levy")
            PopulateEntries(row, invoiceDataTable, "Standing_Charge")
            PopulateEntries(row, invoiceDataTable, "kVA_Cost")
            PopulateEntries(row, invoiceDataTable, "Reactive_Power_Cost")
            PopulateEntries(row, invoiceDataTable, "Other_Charge")
            PopulateEntries(row, invoiceDataTable, "VAT_Split")
            PopulateEntries(row, invoiceDataTable, "VAT_2")
            PopulateEntries(row, invoiceDataTable, "VAT_1")
            'PopulateEntries(row, invoiceDataTable, "VAT_2_Percent")
            'PopulateEntries(row, invoiceDataTable, "VAT_1_Percent")
        Next

    End Sub


    Private Shared Sub PopulateEntries(dtRow As DataRow, invoiceDataTable As DataTable, ColName As String)
        If dtRow.Table.Columns.Contains(ColName) Then

            'If Not String.IsNullOrEmpty(dtRow(ColName)) Then

            Dim invoiceTableRow As DataRow = invoiceDataTable.NewRow

            invoiceTableRow("Invoice Number") = dtRow("Code") & " " & dtRow("Invoice_Number") & "_" & dtRow("SupplierName")
            invoiceTableRow("Meter Serial") = dtRow("Code")
            invoiceTableRow("MPAN") = ""
            invoiceTableRow("TaxPointDate") = ""
            'Convert.ToDateTime(dtRow("Date_TaxPoint")).ToString("dd-MM-yy")
            invoiceTableRow("Start Date") = Convert.ToDateTime(dtRow("Date_Previous")).AddDays(1).ToString("dd-MM-yy")
            invoiceTableRow("End Date") = Convert.ToDateTime(dtRow("Date")).ToString("dd-MM-yy")
            invoiceTableRow("Start Read Estimated") = ""
            invoiceTableRow("End Read Estimated") = ""
            invoiceTableRow("Force kWh") = "Yes"
            invoiceTableRow("Description") = ColName
            invoiceTableRow("IsLastDayApportion") = "Yes"

            If ColName = "M1_Units" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, dtRow("M1_Previous"), dtRow("M1_Present"), dtRow("M1_Units"), "M1", CheckIfNumeric(dtRow("M1_Cost").ToString.Replace("�", "")), "0", "No")
            ElseIf ColName = "M1_Split1" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, dtRow("M1_Previous"), dtRow("M1_Present"), dtRow("M1_Split1"), "M1", CheckIfNumeric(dtRow("M1_Split1_Cost").ToString.Replace("�", "")), "0", "No")
            ElseIf ColName = "M2_Units" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, dtRow("M2_Previous"), dtRow("M2_Present"), dtRow("M2_Units"), "M2", CheckIfNumeric(dtRow("M2_Cost").ToString.Replace("�", "")), "0", "No")
            ElseIf ColName = "M3_Units" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, dtRow("M3_Previous"), dtRow("M3_Present"), dtRow("M3_Units"), "M3", CheckIfNumeric(dtRow("M3_Cost").ToString.Replace("�", "")), "0", "No")
            ElseIf ColName = "M1_Cost_Rate" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, dtRow("M3_Previous"), dtRow("M3_Present"), dtRow("M1_Cost_Rate"), "M1", "0", "0", "Yes")
            ElseIf ColName = "M1_Split1_Cost_Rate" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, dtRow("M3_Previous"), dtRow("M3_Present"), dtRow("M1_Split1_Cost_Rate"), "M1", "0", "0", "Yes")
            ElseIf ColName = "M2_Cost_Rate" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, dtRow("M3_Previous"), dtRow("M3_Present"), dtRow("M2_Cost_Rate"), "M2", "0", "0", "Yes")
            ElseIf ColName = "M3_Cost_Rate" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, dtRow("M3_Previous"), dtRow("M3_Present"), dtRow("M3_Cost_Rate"), "M3", "0", "0", "Yes")
            ElseIf ColName = "TUOS" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", 0, "", CheckIfNumeric(dtRow("TUOS").ToString.Replace("�", "")), 0, "Yes")
            ElseIf ColName = "DUOS" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", 0, "", CheckIfNumeric(dtRow("DUOS").ToString.Replace("�", "")), 0, "Yes")
            ElseIf ColName = "DUOS_Red_Cost" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", 0, "", CheckIfNumeric(dtRow("DUOS_Red_Cost").ToString.Replace("�", "")), 0, "Yes")
            ElseIf ColName = "DUOS_Amber_Cost" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", 0, "", CheckIfNumeric(dtRow("DUOS_Amber_Cost").ToString.Replace("�", "")), 0, "Yes")
            ElseIf ColName = "DUOS_Green_Cost" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", 0, "", CheckIfNumeric(dtRow("DUOS_Green_Cost").ToString.Replace("�", "")), 0, "Yes")
            ElseIf ColName = "Settlements" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", 0, "", CheckIfNumeric(dtRow("Settlements").ToString.Replace("�", "")), 0, "Yes")
            ElseIf ColName = "Feed_In_Cost" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", 0, "", CheckIfNumeric(dtRow("Feed_In_Cost").ToString.Replace("�", "")), 0, "Yes")
            ElseIf ColName = "Climate_Change_Levy" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", CheckIfNumeric(dtRow("Climate_Change_Levy_Rate").ToString.Replace("�", "")), "", CheckIfNumeric(dtRow("Climate_Change_Levy").ToString.Replace("�", "")), 0, "Yes")
                'ElseIf ColName = "Climate_Change_Levy_Rate" Then
                '    PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", 0, "", CheckIfNumeric(dtRow("Climate_Change_Levy_Rate").ToString.Replace("�", "")), 0, "Yes")
            ElseIf ColName = "Standing_Charge" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", 0, "", CheckIfNumeric(dtRow("Standing_Charge").ToString.Replace("�", "")), 0, "Yes")
            ElseIf ColName = "kVA_Cost" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", 0, "", CheckIfNumeric(dtRow("kVA_Cost").ToString.Replace("�", "")), 0, "Yes")
            ElseIf ColName = "Reactive_Power_Cost" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", 0, "", CheckIfNumeric(dtRow("Reactive_Power_Cost").ToString.Replace("�", "")), 0, "Yes")
            ElseIf ColName = "Other_Charge" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", 0, "", CheckIfNumeric(dtRow("Other_Charge").ToString.Replace("�", "")), 0, "Yes")
            ElseIf ColName = "VAT_Split" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", CheckIfNumeric(dtRow("VAT_Split").ToString.Replace("�", "")), "", 0, 0, "Yes")
            ElseIf ColName = "VAT_2" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", dtRow("VAT_2_Percent"), "", 0, CheckIfNumeric(dtRow("VAT_2").ToString.Replace("�", "")), "Yes")
            ElseIf ColName = "VAT_1" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", dtRow("VAT_1_Percent"), "", 0, CheckIfNumeric(dtRow("VAT_1").ToString.Replace("�", "")), "Yes")
            ElseIf ColName = "VAT_2_Percent" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", CheckIfNumeric(dtRow("VAT_2_Percent").ToString.Replace("�", "")), "", 0, 0, "Yes")
            ElseIf ColName = "VAT_1_Percent" Then
                PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, "", "", CheckIfNumeric(dtRow("VAT_1_Percent").ToString.Replace("�", "")), "", 0, 0, "Yes")
            End If

        invoiceDataTable.Rows.Add(invoiceTableRow)
        'End If
        End If
    End Sub

    Private Shared Sub PopulateEntriesBasedOnColName(ByRef invoiceTableRow As DataRow, ByRef dtRow As DataRow, ByVal StartRead As String, ByVal EndRead As String, ByVal Consumption As String, _
                                              ByVal Rate As String, ByVal Net As String, ByVal Vat As String, ByVal CostOnly As String)
        invoiceTableRow("Start Read") = StartRead
        invoiceTableRow("End Read") = EndRead
        invoiceTableRow("Consumption") = Consumption
        invoiceTableRow("Rate") = Rate
        invoiceTableRow("Net") = Net
        invoiceTableRow("Vat") = Vat
        invoiceTableRow("Gross") = Convert.ToDouble(invoiceTableRow("Net")) + Convert.ToDouble(invoiceTableRow("Vat"))
        invoiceTableRow("Cost Only") = CostOnly
    End Sub

    Private Shared Function CheckIfNumeric(ByVal Cost As String)
        If Cost = "" Then
            Return 0
        Else
            If IsNumeric(Cost) Then
                Return Cost
            Else
                Return 0
            End If
        End If
    End Function


    Private Shared Function FormatColumnHeaders(ByRef dt As DataTable) As DataTable
        For Each column As DataColumn In dt.Columns
            column.ColumnName = column.ColumnName.Trim.Replace(" ", "")
        Next
        Return dt
    End Function

    Private Shared Sub RenameColumns(invoiceDataTable As DataTable)
        invoiceDataTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceDataTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceDataTable.Columns("Rate").ColumnName = "Tariff"
        invoiceDataTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceDataTable.Columns("End Date").ColumnName = "EndDate"
        invoiceDataTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceDataTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceDataTable.Columns("End Read").ColumnName = "EndRead"
        invoiceDataTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceDataTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceDataTable.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub
End Class
