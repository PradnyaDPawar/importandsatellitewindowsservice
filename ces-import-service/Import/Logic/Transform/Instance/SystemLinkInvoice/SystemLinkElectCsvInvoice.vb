﻿Public Class SystemLinkElectCsvInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable
    Private xSheet As ExcelSheet
    Dim excel As New Excel()
    Private systemLinkUtil As String

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvFile As New DelimitedFile(fullPath, 0)
        csvFile.HasHeader = False
        csvFile.FieldsAreQuoted = True
        csvFile.Load(_returnTable)


        'excel.Load(fullPath)
        'xSheet = excel.Sheets(0)

        'If excel.GetSheet("Elect", xSheet) = True Then
        '    xSheet.HeaderRowIndex = 1
        '    _returnTable = xSheet.GetDataTable()
        '    TransposeUtil = "Elect"
        'End If
        'If excel.GetSheet("Gas", xSheet) = True Then
        '    xSheet.HeaderRowIndex = 1
        '    _returnTable = xSheet.GetDataTable()
        '    TransposeUtil = "Gas"
        'End If
        'If excel.GetSheet("Water", xSheet) = True Then
        '    xSheet.HeaderRowIndex = 1
        '    _returnTable = xSheet.GetDataTable()
        '    TransposeUtil = "Water"
        'End If
        'If excel.GetSheet("Heat", xSheet) = True Then
        '    xSheet.HeaderRowIndex = 1
        '    _returnTable = xSheet.GetDataTable()
        '    TransposeUtil = "Heat"
        'End If
    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property
    Public Property TransposeUtil As String
        Get
            Return systemLinkUtil
        End Get
        Set(ByVal value As String)
            systemLinkUtil = value
        End Set

    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        DataTableTools.SetHeaders(ReturnTable, 1)
        SystemLinkElectCsvInvoiceSchema.ToDeFormat(_returnTable)
        FillInvoice(_returnTable, dsEdits, importEdit)
        'Select Case (TransposeUtil)
        '    Case "Elect"

        '        SystemLinkElectCsvInvoiceSchema.ToDeFormat(_returnTable)
        '        FillInvoice(_returnTable, dsEdits, importEdit)
        '    Case "Gas"
        '        SystemLinkGasCsvInvoiceSchema.ToDeFormat(_returnTable)
        '        FillInvoice(_returnTable, dsEdits, importEdit)
        '    Case "Water"
        '        SystemLinkWaterCsvInvoiceSchema.ToDeFormat(_returnTable)
        '        FillInvoice(_returnTable, dsEdits, importEdit)
        '    Case "Heat"
        '        SystemLinkHeatCsvInvoiceSchema.ToDeFormat(_returnTable)
        '        FillInvoice(_returnTable, dsEdits, importEdit)
        'End Select
    End Sub

    Public Sub FillInvoice(ByVal ReturnTable As DataTable, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)
        For Each row As DataRow In ReturnTable.Rows
            FE_Invoice.Fill(row, dsEdits, ImportEdit)
        Next
    End Sub
End Class
