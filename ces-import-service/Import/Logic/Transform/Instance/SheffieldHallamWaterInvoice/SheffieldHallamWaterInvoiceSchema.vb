﻿Public Class SheffieldHallamWaterInvoiceSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable)
        DataTableTools.RemoveBlankRows(dt)
        dt.Rows.RemoveAt(dt.Rows.Count - 1) ' Remove the row showing total invoice amount
        DataTableTools.RemoveBlankColumns(dt)
        Dim InvoiceDataTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        CreateInvoiceEntries(dt, InvoiceDataTable)
        dt = InvoiceDataTable
        MapActualEstimated(dt)
        RenameColumns(dt)
        Return dt
    End Function


    Private Shared Sub CreateInvoiceEntries(ByRef dt As DataTable, ByRef InvoiceDataTable As DataTable)


        For Each dtRow As DataRow In dt.Rows

            Dim invoiceDataRow As DataRow = InvoiceDataTable.NewRow
            PopulateWaterCharge1(dtRow, invoiceDataRow)
            InvoiceDataTable.Rows.Add(invoiceDataRow)

            If dt.Columns.Contains("Water Volume 2") Then
                InvoiceDataTable.ImportRow(invoiceDataRow)
                Dim Water2dataRow As DataRow = InvoiceDataTable.Rows(InvoiceDataTable.Rows.Count - 1)
                PopulateWaterCharge2(dtRow, Water2dataRow)
            End If

            If dt.Columns.Contains("Sewerage Volume 1") Then
                InvoiceDataTable.ImportRow(invoiceDataRow)
                Dim Sewage1dataRow As DataRow = InvoiceDataTable.Rows(InvoiceDataTable.Rows.Count - 1)
                PopulateSewageCharge1(dtRow, Sewage1dataRow)
            End If


            If dt.Columns.Contains("Sewerage Volume 2") Then
                InvoiceDataTable.ImportRow(invoiceDataRow)
                Dim Sewage2dataRow As DataRow = InvoiceDataTable.Rows(InvoiceDataTable.Rows.Count - 1)
                PopulateSewageCharge2(dtRow, Sewage2dataRow)
            End If


            If dt.Columns.Contains("Water Standing Charge") Then
                InvoiceDataTable.ImportRow(invoiceDataRow)
                Dim StandingChargedataRow As DataRow = InvoiceDataTable.Rows(InvoiceDataTable.Rows.Count - 1)
                PopulateWaterStandingCharge(dtRow, StandingChargedataRow)
            End If


            If dt.Columns.Contains("Sewerage Standing Charge") Then
                InvoiceDataTable.ImportRow(invoiceDataRow)
                Dim SewageScDataRow As DataRow = InvoiceDataTable.Rows(InvoiceDataTable.Rows.Count - 1)
                PopulateSewageStandingCharge(dtRow, SewageScDataRow)
            End If


            If dt.Columns.Contains("Surface Water Charge") Then
                InvoiceDataTable.ImportRow(invoiceDataRow)
                Dim SurfaceChargeDataRow As DataRow = InvoiceDataTable.Rows(InvoiceDataTable.Rows.Count - 1)
                PopulateSurfaceCharge(dtRow, SurfaceChargeDataRow)
            End If


        Next
    End Sub

    Private Shared Sub PopulateWaterCharge1(ByRef dtRow As DataRow, ByRef invoiceDataRow As DataRow)
        Dim net, vat As Double

        invoiceDataRow("Invoice Number") = dtRow("Invoice No")
        invoiceDataRow("Meter Serial") = dtRow("Serial No")
        invoiceDataRow("MPAN") = ""
        invoiceDataRow("Start Date") = dtRow("Previous Reading Date")
        invoiceDataRow("End Date") = dtRow("Present Reading Date")
        invoiceDataRow("Start Read") = dtRow("Previous Reading")
        invoiceDataRow("Start Read Estimated") = dtRow("Previous Reading Code")
        invoiceDataRow("End Read") = dtRow("Present Reading")
        invoiceDataRow("End Read Estimated") = dtRow("Present Reading Code")
        invoiceDataRow("Force kWh") = "No"
        invoiceDataRow("Consumption") = dtRow("Water Volume 1")
        invoiceDataRow("Rate") = "Water"
        invoiceDataRow("Net") = dtRow("Water Volume Charge 1")
        invoiceDataRow("Vat") = 0

        If Not String.IsNullOrEmpty(invoiceDataRow("Net")) Then
            net = invoiceDataRow("Net")
        End If

        If Not String.IsNullOrEmpty(invoiceDataRow("Vat")) Then
            vat = invoiceDataRow("Vat")
        End If

        invoiceDataRow("Gross") = (net + vat).ToString("0.00")
        invoiceDataRow("Cost Only") = "No"
        invoiceDataRow("Description") = "Water Consumption Charge 1"
        invoiceDataRow("TaxPointDate") = dtRow("Invoice Date")
        invoiceDataRow("IsLastDayApportion") = "No"

    End Sub


    Private Shared Sub PopulateWaterCharge2(ByRef dtRow As DataRow, ByRef water2DataRow As DataRow)
        Dim net, vat As Double
        water2DataRow("Consumption") = dtRow("Water Volume 2")
        water2DataRow("Rate") = "Water 2"
        water2DataRow("Net") = dtRow("Water Volume Charge 2")
        water2DataRow("Vat") = 0
        If Not String.IsNullOrEmpty(water2DataRow("Net")) Then
            net = water2DataRow("Net")
        End If

        If Not String.IsNullOrEmpty(water2DataRow("Vat")) Then
            vat = water2DataRow("Vat")
        End If

        water2DataRow("Gross") = (net + vat).ToString("0.00")
        water2DataRow("Cost Only") = "No"
        water2DataRow("Description") = "Water Consumption Charge 2"
    End Sub

    Private Shared Sub PopulateSewageCharge1(ByRef dtRow As DataRow, ByRef Sewage1dataRow As DataRow)
        Dim net, vat As Double
        Sewage1dataRow("Consumption") = dtRow("Sewerage Volume 1")
        Sewage1dataRow("Rate") = "Sewerage 1"
        Sewage1dataRow("Net") = dtRow("Sewerage Volume Charge 1")
        Sewage1dataRow("Vat") = 0

        If Not String.IsNullOrEmpty(Sewage1dataRow("Net")) Then
            Net = Sewage1dataRow("Net")
        End If

        If Not String.IsNullOrEmpty(Sewage1dataRow("Vat")) Then
            vat = Sewage1dataRow("Vat")
        End If

        Sewage1dataRow("Gross") = (Net + vat).ToString("0.00")
        Sewage1dataRow("Cost Only") = "No"
        Sewage1dataRow("Description") = "Sewage Charge 1"
    End Sub

    Private Shared Sub PopulateSewageCharge2(ByRef dtRow As DataRow, ByRef Sewage2dataRow As DataRow)
        Dim net, vat As Double
        Sewage2dataRow("Consumption") = dtRow("Sewerage Volume 2")
        Sewage2dataRow("Rate") = "Sewerage 2"
        Sewage2dataRow("Net") = dtRow("Sewerage Volume Charge 2")
        Sewage2dataRow("Vat") = 0

        If Not String.IsNullOrEmpty(Sewage2dataRow("Net")) Then
            Net = Sewage2dataRow("Net")
        End If

        If Not String.IsNullOrEmpty(Sewage2dataRow("Vat")) Then
            vat = Sewage2dataRow("Vat")
        End If

        Sewage2dataRow("Gross") = (Net + vat).ToString("0.00")
        Sewage2dataRow("Cost Only") = "No"
        Sewage2dataRow("Description") = "Sewage Charge 2"
    End Sub

    Private Shared Sub PopulateSewageStandingCharge(ByRef dtRow As DataRow, ByRef SewageScDataRow As DataRow)
        Dim net, vat As Double
        SewageScDataRow("Net") = dtRow("Sewerage Standing Charge")
        SewageScDataRow("Vat") = 0

        If Not String.IsNullOrEmpty(SewageScDataRow("Net")) Then
            Net = SewageScDataRow("Net")
        End If

        If Not String.IsNullOrEmpty(SewageScDataRow("Vat")) Then
            vat = SewageScDataRow("Vat")
        End If
        SewageScDataRow("Rate") = ""
        SewageScDataRow("Consumption") = ""
        SewageScDataRow("Gross") = (Net + vat).ToString("0.00")
        SewageScDataRow("Cost Only") = "Yes"
        SewageScDataRow("Description") = "Sewage Water Standing Charge"
        SewageScDataRow("Start Read") = ""
        SewageScDataRow("Start Read Estimated") = ""
        SewageScDataRow("End Read") = ""
        SewageScDataRow("End Read Estimated") = ""
    End Sub

    Private Shared Sub PopulateSurfaceCharge(ByRef dtRow As DataRow, ByRef SurfaceChargeDataRow As DataRow)
        Dim net, vat As Double
        SurfaceChargeDataRow("Net") = dtRow("Surface Water Charge")
        SurfaceChargeDataRow("Vat") = 0

        If Not String.IsNullOrEmpty(SurfaceChargeDataRow("Net")) Then
            Net = SurfaceChargeDataRow("Net")
        End If

        If Not String.IsNullOrEmpty(SurfaceChargeDataRow("Vat")) Then
            vat = SurfaceChargeDataRow("Vat")
        End If

        SurfaceChargeDataRow("Rate") = ""
        SurfaceChargeDataRow("Consumption") = ""
        SurfaceChargeDataRow("Gross") = (Net + vat).ToString("0.00")
        SurfaceChargeDataRow("Cost Only") = "Yes"
        SurfaceChargeDataRow("Description") = "Surface Water Charge"
        SurfaceChargeDataRow("Start Read") = ""
        SurfaceChargeDataRow("Start Read Estimated") = ""
        SurfaceChargeDataRow("End Read") = ""
        SurfaceChargeDataRow("End Read Estimated") = ""
    End Sub

    Private Shared Sub PopulateWaterStandingCharge(ByRef dtRow As DataRow, ByRef StandingChargedataRow As DataRow)
        Dim net, vat As Double
        StandingChargedataRow("Net") = dtRow("Water Standing Charge")
        StandingChargedataRow("Vat") = 0

        If Not String.IsNullOrEmpty(StandingChargedataRow("Net")) Then
            Net = StandingChargedataRow("Net")
        End If

        If Not String.IsNullOrEmpty(StandingChargedataRow("Vat")) Then
            vat = StandingChargedataRow("Vat")
        End If

        StandingChargedataRow("Rate") = ""
        StandingChargedataRow("Consumption") = ""
        StandingChargedataRow("Gross") = (Net + vat).ToString("0.00")
        StandingChargedataRow("Cost Only") = "Yes"
        StandingChargedataRow("Description") = "Water Standing Charge"
        StandingChargedataRow("Start Read") = ""
        StandingChargedataRow("Start Read Estimated") = ""
        StandingChargedataRow("End Read") = ""
        StandingChargedataRow("End Read Estimated") = ""
    End Sub

    Private Shared Sub MapActualEstimated(ByRef dt As DataTable)
        Dim ListActual As New List(Of Char)(New Char() {"", "b", "c", "g", "i", "k", "l", "r", "s", "v", "x"})
        Dim ListEstimated As New List(Of Char)(New Char() {"a", "d", "e", "f", "h", "m", "n", "o", "p", "t", "u", "w", "y", "z"})

        For Each row As DataRow In dt.Rows
            ' Start Read Estimated
            If Not String.IsNullOrEmpty(row("Start Read")) Then

                If ListActual.Contains(row("Start Read Estimated").ToString.ToLower.Trim) Then
                    row("Start Read Estimated") = "A"
                ElseIf ListEstimated.Contains(row("Start Read Estimated").ToString.ToLower.Trim) Then
                    row("Start Read Estimated") = "E"
                End If

            End If

            ' End Read Estimated
            If Not String.IsNullOrEmpty(row("End Read")) Then
                If ListActual.Contains(row("End Read Estimated").ToString.ToLower.Trim) Then
                    row("End Read Estimated") = "A"
                ElseIf ListEstimated.Contains(row("End Read Estimated").ToString.ToLower.Trim) Then
                    row("End Read Estimated") = "E"
                End If
            End If

        Next



    End Sub

    Private Shared Sub RenameColumns(InvoiceDt As DataTable)

        InvoiceDt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        InvoiceDt.Columns("Meter Serial").ColumnName = "MeterSerial"
        InvoiceDt.Columns("Rate").ColumnName = "Tariff"
        InvoiceDt.Columns("Start Date").ColumnName = "StartDate"
        InvoiceDt.Columns("End Date").ColumnName = "EndDate"
        InvoiceDt.Columns("Start Read").ColumnName = "StartRead"
        InvoiceDt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        InvoiceDt.Columns("End Read").ColumnName = "EndRead"
        InvoiceDt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        InvoiceDt.Columns("Force kWh").ColumnName = "ForcekWh"
        InvoiceDt.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub

End Class
