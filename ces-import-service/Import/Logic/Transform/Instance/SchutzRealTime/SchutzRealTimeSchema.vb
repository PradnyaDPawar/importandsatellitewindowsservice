﻿Public Class SchutzRealTimeSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("DateTime")
        headers.Add("MeterName")
        headers.Add("CountUnits")
        headers.Add("Extra")
        Return headers

    End Function

    Public Shared Function ToDeFormat(ByRef tdt As DataTable)
        tdt.Rows.RemoveAt(0)
        For Each row As DataRow In tdt.Rows
            row("DateTime") = row("DateTime").ToString().Replace("T", " ")
            row("CountUnits") = row("CountUnits").ToString.Split(",")(0)
            If (row("CountUnits").ToString() = "-0") Then
                row("CountUnits") = 0
            End If
        Next
        Return tdt
    End Function
End Class
