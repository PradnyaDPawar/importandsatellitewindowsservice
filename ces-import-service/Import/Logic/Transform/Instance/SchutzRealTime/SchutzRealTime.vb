﻿Public Class SchutzRealTime
    Implements ITransformDelegate
    Private _returnTable As DataTable
    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvFile As New DelimitedFile(fullPath, 0)
        csvFile.Delimiter = ";"
        csvFile.HasHeader = False
        csvFile.LoadNonConsistentTable(_returnTable, SchutzRealTimeSchema.GetHeaders())


    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        SchutzRealTimeSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_RealTime.Fill(row, dsEdits, importEdit)
        Next


    End Sub

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property
End Class
