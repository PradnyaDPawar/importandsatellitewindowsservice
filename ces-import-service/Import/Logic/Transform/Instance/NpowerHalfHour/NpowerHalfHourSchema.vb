﻿Public Class NpowerHalfHourSchema

    Public Shared Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("Mpan")
        headers.Add("Date")
        headers.Add("Active Power")
        Return headers
    End Function

    Public Shared Function Validate(ByRef data As DataTable) As Boolean

        For Each header As String In GetHeaders()
            If Not data.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable)
        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)
        Return dt
    End Function

    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim dateholder As DateTime
        Dim success As Boolean = False
        For Each row As DataRow In dt.Rows

            If IsDate(row("Date")) Then
                dateholder = row("Date")
                For Each hhrow As DataRow In hhDataTable.Rows
                    If hhrow("Date(dd/mm/yyyy)") = dateholder.Date And hhrow("MPAN") = row("Mpan") Then
                        hhrow(dateholder.ToString("HH:mm")) = row("Active Power")
                        hhrow("ForcekWh") = "Yes"
                        hhrow("MPAN") = row("Mpan")
                        success = True
                    End If
                Next

                If Not success Then
                    Dim newRow As DataRow = hhDataTable.NewRow
                    newRow("Date(dd/mm/yyyy)") = dateholder.Date
                    newRow(dateholder.ToString("HH:mm")) = row("Active Power")
                    newRow("ForcekWh") = "Yes"
                    newRow("MPAN") = row("Mpan")
                    hhDataTable.Rows.Add(newRow)
                End If
            End If
            success = False
        Next

        dt = hhDataTable

    End Sub

End Class
