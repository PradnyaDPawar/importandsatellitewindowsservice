﻿Public Class EdfHalfhourSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("Account Number")
        headers.Add("Invoice Number")
        headers.Add("Meter or MPAN Number")
        headers.Add("Month")
        headers.Add("Date")
        headers.Add("Measured in")
        headers.Add("00:00")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")
        headers.Add("00:00 Status")
        headers.Add("00:30 Status")
        headers.Add("01:00 Status")
        headers.Add("01:30 Status")
        headers.Add("02:00 Status")
        headers.Add("02:30 Status")
        headers.Add("03:00 Status")
        headers.Add("03:30 Status")
        headers.Add("04:00 Status")
        headers.Add("04:30 Status")
        headers.Add("05:00 Status")
        headers.Add("05:30 Status")
        headers.Add("06:00 Status")
        headers.Add("06:30 Status")
        headers.Add("07:00 Status")
        headers.Add("07:30 Status")
        headers.Add("08:00 Status")
        headers.Add("08:30 Status")
        headers.Add("09:00 Status")
        headers.Add("09:30 Status")
        headers.Add("10:00 Status")
        headers.Add("10:30 Status")
        headers.Add("11:00 Status")
        headers.Add("11:30 Status")
        headers.Add("12:00 Status")
        headers.Add("12:30 Status")
        headers.Add("13:00 Status")
        headers.Add("13:30 Status")
        headers.Add("14:00 Status")
        headers.Add("14:30 Status")
        headers.Add("15:00 Status")
        headers.Add("15:30 Status")
        headers.Add("16:00 Status")
        headers.Add("16:30 Status")
        headers.Add("17:00 Status")
        headers.Add("17:30 Status")
        headers.Add("18:00 Status")
        headers.Add("18:30 Status")
        headers.Add("19:00 Status")
        headers.Add("19:30 Status")
        headers.Add("20:00 Status")
        headers.Add("20:30 Status")
        headers.Add("21:00 Status")
        headers.Add("21:30 Status")
        headers.Add("22:00 Status")
        headers.Add("22:30 Status")
        headers.Add("23:00 Status")
        headers.Add("23:30 Status")

        Return headers

    End Function

    Public Shared Function Validate(ByRef data As DataTable) As Boolean


        For Each header As String In GetHeaders()
            If Not data.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next

    End Function

    Public Shared Sub GetTransformedHeaders(ByRef data As DataTable)
        CreateColumns(data)
        RenameColumns(data)
        PopulateForceKwh(data)
        

    End Sub



    Public Shared Sub CreateColumns(ByRef dt As DataTable)

        dt.Columns.Add("MeterSerial")
        dt.Columns.Add("ForcekWh")

    End Sub

    Public Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("Meter or MPAN Number").ColumnName = "MPAN"
        dt.Columns("Date").ColumnName = "Date(dd/mm/yyyy)"

    End Sub

    Public Shared Sub PopulateForceKwh(ByRef dt As DataTable)

        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            If Not row("Measured in") = "" Then

                If row("Measured in").ToString.ToLower() = "kwh" Then
                    row("ForcekWh") = "yes"
                ElseIf row("Measured in").ToString.ToLower() = "kvarh" Then
                    row.Delete()
                End If

            End If

        Next
        dt.AcceptChanges()
    End Sub
End Class
