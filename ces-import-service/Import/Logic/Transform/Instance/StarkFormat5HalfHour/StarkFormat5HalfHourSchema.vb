﻿Public Class StarkFormat5HalfHourSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("CompanyName")
        headers.Add("SiteName")
        headers.Add("OnlineMeterName")
        headers.Add("MPAN")
        headers.Add("MeterSerial")
        headers.Add("Type")
        headers.Add("Est")
        headers.Add("Date(DD/MM/YYYY)")
        headers.Add("00:00")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")
        headers.Add("spare1")
        headers.Add("spare2")
        headers.Add("spare3")

        Return headers

    End Function

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        'Remove Rows that are not required

        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            If IsDBNull(row("23:00")) And IsDBNull(row("23:30")) Then
                row.Delete()
            ElseIf (String.IsNullOrEmpty(row("23:00")) And String.IsNullOrEmpty(row("23:30"))) Then
                row.Delete()


            ElseIf (row("23:00").ToString() = "23:30" And row("23:30").ToString() = "0:00") Then
                row.Delete()
            End If

        Next

        dt.AcceptChanges()


        'add force kwh column
        AddForceKwh(dt)

    End Sub


    Private Shared Sub AddForceKwh(ByRef data As DataTable)

        If Not data.Columns.Contains("forcekwh") Then

            data.Columns.Add("forcekwh")

        End If

        'set all as forcekwh
        For Each row As DataRow In data.Rows

            If row("Type").ToString.ToLower.Trim = "kwh" Then
                row("forcekwh") = "Yes"
            Else
                row("forcekwh") = "No"
            End If

        Next

    End Sub


End Class
