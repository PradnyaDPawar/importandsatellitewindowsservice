﻿Imports System.Globalization

Partial Public Class DigitalEnergyHalfHour


    Private Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("MPAN")
        headers.Add("MeterSerial")
        headers.Add("ForcekWh")
        headers.Add("Date(dd/MM/yyyy)")
        headers.Add("00:00")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")
        Return headers
    End Function



    Private Sub ColumnValidate()
        Dim rawHeaders As List(Of String) = GetHeaders()

        For Each header As String In rawHeaders
            If Not _returnTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next
    End Sub

    Private Sub RenameColumns()
        'Rename HH entries
        Dim halfHourEntry As DateTime
        For Each column As DataColumn In _returnTable.Columns
            If DateTime.TryParse(column.ColumnName, halfHourEntry) Then
                column.ColumnName = halfHourEntry.ToString("HH:mm")
            Else
                'replace space
                column.ColumnName = column.ColumnName.Replace(" ", "")
            End If

            'set alias
            If column.ColumnName.ToLower = "mprn" Then
                column.ColumnName = "MPAN"
            End If
            If column.ColumnName.ToLower = "date" Then
                column.ColumnName = "Date(dd/MM/yyyy)"
            End If
            If column.ColumnName.ToLower = "serial" Or column.ColumnName.ToLower = "serialnumber" Then
                column.ColumnName = "MeterSerial"
            End If

        Next
    End Sub

   
End Class
