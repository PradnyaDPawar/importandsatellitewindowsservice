﻿Imports System.IO
Imports System.Data.OleDb

Public Class SSEHalfHourly
    Implements ITransformDelegate

#Region "Member variables"

    Private _returnTable As DataTable

#End Region

#Region "Public Properties"

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set
    End Property

#End Region

#Region "Public functions"

    ''' <summary>
    ''' Load data from file and populate data in DataTable.
    ''' </summary>
    ''' <param name="fullPath">The full path.</param>
    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel()
        ReturnTable = excel.LoadByOleDB(fullPath)

        ReturnTable.Columns(0).ColumnName = "MPAN"
        ReturnTable.Columns(1).ColumnName = "Date(dd/MM/yyyy)"
        ReturnTable.Columns(2).ColumnName = "Reading Type"

        DataTableTools.RemoveBlankColumns(ReturnTable)
        DataTableTools.RemoveBlankRows(ReturnTable)

    End Sub

    ''' <summary>
    ''' Convert the HH row data table to DE format.
    ''' </summary>
    ''' <param name="dsEdits">The dsEdits.</param>
    ''' <param name="importEdit">The importEdit.</param>
    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        SSEHalfHourlySchema.ToDEFormat(ReturnTable)

        For Each row As DataRow In _returnTable.Rows

            ' If row has valid data then fill it in staging table.
            If IsDate(row.Item("Date(dd/MM/yyyy)")) Then
                FE_HalfHour.Fill(row, dsEdits, importEdit)
            End If

        Next

    End Sub

#End Region

End Class