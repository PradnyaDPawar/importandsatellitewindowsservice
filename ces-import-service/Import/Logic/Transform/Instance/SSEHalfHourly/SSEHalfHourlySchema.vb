﻿Public Class SSEHalfHourlySchema

    ''' <summary>
    ''' Convert the data table to DE format
    ''' </summary>
    ''' <param name="dt">The dt.</param>
    ''' <returns>System.Object.</returns>
    Public Shared Function ToDEFormat(ByRef dt As DataTable)

        PopulateColumns(dt)

        Return dt

    End Function

    ''' <summary>
    ''' Add column 'MaterialSerial' and rename 'Reading Type' with 'ForceKwh'
    ''' </summary>
    ''' <param name="dt">The dt.</param>
    Private Shared Sub PopulateColumns(ByRef dt As DataTable)

        dt.AcceptChanges()

        dt.Columns.Add("MeterSerial")

        For Each row As DataRow In dt.Rows

            If (row("Reading Type").ToString().ToLower() = "kwh") Then
                row("Reading Type") = "Yes"
            Else
                row("Reading Type") = "No"
            End If
            row("MeterSerial") = ""

            ' If the row is not a valid data row, than delete it from table.
            If Not IsDate(row.Item("Date(dd/MM/yyyy)")) Then
                row.Delete()
            End If
        Next

        dt.Columns("Reading Type").ColumnName = "ForcekWh"

        dt.AcceptChanges()

    End Sub

End Class