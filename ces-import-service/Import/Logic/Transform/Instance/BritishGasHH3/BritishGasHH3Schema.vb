﻿Public Class BritishGasHH3Schema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("meter_identifier")
        headers.Add("read_date")
        headers.Add("hh01")
        headers.Add("hh02")
        headers.Add("hh03")
        headers.Add("hh04")
        headers.Add("hh05")
        headers.Add("hh06")
        headers.Add("hh07")
        headers.Add("hh08")
        headers.Add("hh09")
        headers.Add("hh10")
        headers.Add("hh11")
        headers.Add("hh12")
        headers.Add("hh13")
        headers.Add("hh14")
        headers.Add("hh15")
        headers.Add("hh16")
        headers.Add("hh17")
        headers.Add("hh18")
        headers.Add("hh19")
        headers.Add("hh20")
        headers.Add("hh21")
        headers.Add("hh22")
        headers.Add("hh23")
        headers.Add("hh24")
        headers.Add("hh25")
        headers.Add("hh26")
        headers.Add("hh27")
        headers.Add("hh28")
        headers.Add("hh29")
        headers.Add("hh30")
        headers.Add("hh31")
        headers.Add("hh32")
        headers.Add("hh33")
        headers.Add("hh34")
        headers.Add("hh35")
        headers.Add("hh36")
        headers.Add("hh37")
        headers.Add("hh38")
        headers.Add("hh39")
        headers.Add("hh40")
        headers.Add("hh41")
        headers.Add("hh42")
        headers.Add("hh43")
        headers.Add("hh44")
        headers.Add("hh45")
        headers.Add("hh46")
        headers.Add("hh47")
        headers.Add("hh48")

        Return headers
    End Function


    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        RenameColumns(dt)
        AddColumns(dt)
        PopulateColumns(dt) 'Also Modify MPAN

    End Sub


    Private Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("meter_identifier").ColumnName = "MPAN"
        dt.Columns("read_date").ColumnName = "Date(DD/MM/YYYY)"

        'Rename Time Entries
        Dim dateholder As Date = DateTime.Today

        For columncount As Integer = dt.Columns("hh01").Ordinal To dt.Columns("hh48").Ordinal

            dt.Columns(columncount).ColumnName = dateholder.ToString("HH:mm")

            dateholder = dateholder.AddMinutes(30)

        Next

    End Sub

    Private Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("MeterSerial")
        dt.Columns.Add("ForcekWh")


    End Sub

    Private Shared Sub PopulateColumns(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows

            row("ForceKwh") = "No"

            'Modify MPAN
            row("MPAN") = Left(row("MPAN"), 13)

        Next

    End Sub


End Class
