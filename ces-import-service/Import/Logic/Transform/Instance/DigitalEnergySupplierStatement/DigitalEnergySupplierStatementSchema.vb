﻿Partial Public Class DigitalEnergySupplierStatement
    Private Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        Try
            headers.Add("InvoiceNumber")
            headers.Add("MPAN")
            headers.Add("StartDate")
            headers.Add("EndDate")
            headers.Add("Estimated")
            headers.Add("kWh")
            headers.Add("Description")
        Catch ex As Exception
            Trace.WriteLine("Get Headers : " & ex.Message & "," & ex.StackTrace)
        End Try
        Return headers
    End Function

    Private Sub ColumnValidate()
        Dim rawHeaders As List(Of String) = GetHeaders()
        Try
            For Each header As String In rawHeaders
                If Not _returnTable.Columns.Contains(header) Then
                    Throw New ApplicationException("Missing Column " & header & ". ")
                End If 
            Next
        Catch ex As Exception
            Trace.WriteLine("Column Validate : " & ex.Message & "," & ex.StackTrace)
        End Try

    End Sub

    Private Sub RenameColumns()

        Try
            For Each column As DataColumn In _returnTable.Columns
                Select Case (column.ColumnName.ToLower.Replace(" ", ""))
                    Case "mpan"
                        column.ColumnName = "MPAN"
                    Case "invoicenumber"
                        column.ColumnName = "InvoiceNumber"
                    Case "startdate"
                        column.ColumnName = "StartDate"
                    Case "enddate"
                        column.ColumnName = "EndDate"
                    Case "estimated"
                        column.ColumnName = "Estimated"
                    Case "kwh"
                        column.ColumnName = "kWh"
                    Case "description"
                        column.ColumnName = "Description" 
                End Select
            Next
        Catch ex As Exception
            Trace.WriteLine("Rename Columns : " & ex.Message & "," & ex.StackTrace)
        End Try

    End Sub

 
End Class
