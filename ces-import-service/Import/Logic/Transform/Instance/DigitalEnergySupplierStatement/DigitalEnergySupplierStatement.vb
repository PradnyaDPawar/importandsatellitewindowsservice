﻿Public Class DigitalEnergySupplierStatement
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property


    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel
        Dim xSheet As ExcelSheet
        excel.Load(fullPath)

        xSheet = excel.Sheets(0)

        'try to find "Supplier Statement" Sheet
        excel.GetSheet("Supplier Statement", xSheet)

        xSheet.HeaderRowIndex = 1
        _returnTable = xSheet.GetDataTable()
    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        RenameColumns()
        ColumnValidate()

        For Each row As DataRow In _returnTable.Rows
            FE_SupplierStatement.Fill(row, dsEdits, importEdit)
        Next

    End Sub
End Class
