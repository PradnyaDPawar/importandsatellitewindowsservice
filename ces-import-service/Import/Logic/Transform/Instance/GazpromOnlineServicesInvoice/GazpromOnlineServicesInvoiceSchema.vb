﻿Public Class GazpromOnlineServicesInvoiceSchema

    Private Shared _vatRate As Double
    Private Shared _invoiceNumber As String
    Private Shared _taxpointdate As String

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("MPR")
        headers.Add("SerialNo")
        headers.Add("StartDate")
        headers.Add("StartRead")
        headers.Add("EndDate")
        headers.Add("EndRead")
        headers.Add("Con")
        headers.Add("CV")
        headers.Add("Corr.Fact")
        headers.Add("Gas Price")
        headers.Add("kWh")

        Return headers

    End Function

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        Dim invoiceSummaryDt As DataTable = GetInvoiceSummaryTable(dt)
        Dim invoiceTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable
        _invoiceNumber = SetInvoiceNumber(dt)
        _taxpointdate = SetInvoiceDate(dt)
        SetVatRate(dt)
        PopulateInvoiceTable(invoiceSummaryDt, invoiceTable)
        AddStandingChargeRows(dt, invoiceTable)
        AddCclRows(dt, invoiceTable)
        RenameColumns(invoiceTable)
        PopulateIsLastDayApportion(invoiceTable)


        dt = invoiceTable

    End Sub

    Private Shared Function GetSiteRefNumber(ByVal dt As DataTable) As String
        Dim rowFlag As Boolean = False
        For Each row As DataRow In dt.Rows

            For Each item As Object In row.ItemArray
                If rowFlag And Not String.IsNullOrEmpty(item.ToString) Then
                    Return item.ToString
                End If
                If item.ToString.ToLower.Trim.Replace(" ", "") = "siterefnum:" Then
                    rowFlag = True
                End If
            Next
        Next

        Return vbNullString
    End Function

    Private Shared Function GetDomesticVat(ByVal dt As DataTable) As String

        Dim rowFlag As Boolean = False
        For Each row As DataRow In dt.Rows

            For Each item As Object In row.ItemArray
                If rowFlag And Not String.IsNullOrEmpty(item.ToString) Then
                    Return item.ToString
                End If
                If item.ToString.ToLower.Trim.Replace(" ", "") = "domesticvat@5.0%" Then
                    rowFlag = True
                End If
            Next
        Next

        Return vbNullString

    End Function

    Private Shared Function GetCommercialVat(ByVal dt As DataTable, ByRef vatString As String) As String

        Dim rowFlag As Boolean = False
        For Each row As DataRow In dt.Rows

            For Each item As Object In row.ItemArray
                If rowFlag And Not String.IsNullOrEmpty(item.ToString) Then
                    Return item.ToString
                End If
                If item.ToString.ToLower.Trim.Replace(" ", "") = vatString Then
                    rowFlag = True
                End If
            Next
        Next

        Return vbNullString

    End Function

    Private Shared Function GetStandingCharge(ByVal dt As DataTable) As String

        Dim rowFlag As Boolean = False
        For Each row As DataRow In dt.Rows

            For Each item As Object In row.ItemArray
                If rowFlag And Not String.IsNullOrEmpty(item.ToString) Then
                    Return item.ToString
                End If
                If item.ToString.ToLower.Trim.Replace(" ", "") = "standingcharge" Then
                    rowFlag = True
                End If
            Next
        Next

        Return vbNullString

    End Function

    Private Shared Function GetCcl(ByVal dt As DataTable) As String

        Dim rowFlag As Boolean = False
        For Each row As DataRow In dt.Rows

            For Each item As Object In row.ItemArray
                If rowFlag And Not String.IsNullOrEmpty(item.ToString) Then
                    Return item.ToString
                End If
                If item.ToString.ToLower.Trim.Replace(" ", "") = "climatechangelevy" Then
                    rowFlag = True
                End If
            Next
        Next

        Return vbNullString

    End Function

    Private Shared Function GetInvoiceSummaryTable(ByVal dt As DataTable) As DataTable

        Dim startRowIndex As Integer = GetInvoiceSummaryIndex(dt)
        Dim endRowIndex As Integer = GetEndRowIndex(dt)

        'Old invoice format will have endRowIndex greater than StartRowIndex.
        If endRowIndex < startRowIndex Then
            endRowIndex = dt.Rows.Count - 1
        End If
        Dim InvoiceSummaryDt As DataTable = dt.Clone
        For rowIndexCount = startRowIndex To endRowIndex
            Dim newRow As DataRow = InvoiceSummaryDt.NewRow
            newRow = dt.Rows(rowIndexCount)
            InvoiceSummaryDt.ImportRow(newRow)

        Next


        DataTableTools.RemoveBlankColumns(InvoiceSummaryDt)
        DataTableTools.SetHeaders(InvoiceSummaryDt, 1)
        Return InvoiceSummaryDt
    End Function

    Private Shared Function GetInvoiceSummaryIndex(ByVal dt As DataTable) As Integer

        Dim reqdRowFlag As Boolean

        Dim headers As List(Of String) = GetHeaders()
        For Each row As DataRow In dt.Rows
            reqdRowFlag = True
            For Each header As String In headers
                If Not row.ItemArray.Contains(header) Then
                    reqdRowFlag = False
                End If
            Next
            If reqdRowFlag Then
                Return dt.Rows.IndexOf(row)
            End If
        Next
    End Function

    Private Shared Sub PopulateInvoiceTable(ByRef dt As DataTable, ByVal invoiceTable As DataTable)

        Dim gasPrice, consumption, net, vat As Double


        For Each row As DataRow In dt.Rows
            Dim invoiceTableRow As DataRow = invoiceTable.NewRow
            invoiceTableRow("Invoice Number") = _invoiceNumber
            invoiceTableRow("Meter Serial") = row("SerialNo")
            invoiceTableRow("MPAN") = row("MPR")
            invoiceTableRow("Rate") = "Gas"
            invoiceTableRow("Start Date") = row("StartDate")
            invoiceTableRow("End Date") = row("EndDate")
            invoiceTableRow("Start Read") = row("StartRead").ToString.Substring(0, row("StartRead").ToString.Length - 1)

            invoiceTableRow("Start Read Estimated") = row("StartRead").ToString.Last

            invoiceTableRow("End Read") = row("EndRead").ToString.Substring(0, row("StartRead").ToString.Length - 1)

            invoiceTableRow("End Read Estimated") = row("EndRead").ToString.Last

            invoiceTableRow("Consumption") = row("kWh")

            invoiceTableRow("Force kWh") = "Yes"

            gasPrice = row("Gas Price")
            consumption = row("kWh")
            net = (gasPrice / 100) * consumption
            vat = net * _vatRate
            invoiceTableRow("Net") = net
            invoiceTableRow("Vat") = vat
            invoiceTableRow("Gross") = (net + vat).ToString
            invoiceTableRow("Cost Only") = "No"
            invoiceTableRow("Description") = ""
            invoiceTable.Rows.Add(invoiceTableRow)

        Next

    End Sub

    Private Shared Function SetVatRate(ByRef dt As DataTable) As Double
        Dim domesticVat, commercialVat As Double
        Dim commercialVat20String, commercialVat17_5String, commercialVat15String As String
        domesticVat = GetDomesticVat(dt).Replace("£", "")

        commercialVat20String = GetCommercialVat(dt, "commercialvat@20.0%")
        commercialVat17_5String = GetCommercialVat(dt, "commercialvat@17.5%")
        commercialVat15String = GetCommercialVat(dt, "commercialvat@15.0%")

        If String.IsNullOrEmpty(commercialVat17_5String) And String.IsNullOrEmpty(commercialVat15String) Then
            commercialVat = 0.2
        End If

        If String.IsNullOrEmpty(commercialVat20String) And String.IsNullOrEmpty(commercialVat15String) Then
            commercialVat = 0.175
        End If

        If String.IsNullOrEmpty(commercialVat17_5String) And String.IsNullOrEmpty(commercialVat20String) Then
            commercialVat = 0.15
        End If


        If domesticVat = 0 And commercialVat <> 0 Then
            _vatRate = commercialVat
        End If

        If domesticVat <> 0 And commercialVat = 0 Then
            _vatRate = 0.05
        End If

        If domesticVat <> 0 And commercialVat <> 0 Then
            Throw New ApplicationException("Vat Calculation Unknown")
        End If
    End Function

    Private Shared Function SetInvoiceNumber(ByRef dt As DataTable) As String

        Dim rowFlag As Boolean = False
        For Each row As DataRow In dt.Rows

            For Each item As Object In row.ItemArray
                If rowFlag And Not String.IsNullOrEmpty(item.ToString) Then
                    Return item.ToString
                End If
                If item.ToString.ToLower.Trim.Replace(" ", "").Contains("invoiceno:") Then
                    rowFlag = True
                End If
            Next
        Next

        Return vbNullString

    End Function

    Private Shared Sub AddStandingChargeRows(ByRef dt As DataTable, ByRef invoiceTable As DataTable)


        Dim standingCharge, vat As Double

        standingCharge = GetStandingCharge(dt).Replace("£", "")

        Dim invoiceTableRow As DataRow = invoiceTable.NewRow
        invoiceTableRow("Invoice Number") = _invoiceNumber
        invoiceTableRow("Meter Serial") = ""
        invoiceTableRow("MPAN") = ""
        invoiceTableRow("Rate") = ""
        invoiceTableRow("Start Date") = invoiceTable.Rows(0)("Start Date")
        invoiceTableRow("End Date") = invoiceTable.Rows(0)("End Date")
        invoiceTableRow("Start Read") = ""
        invoiceTableRow("Start Read Estimated") = ""
        invoiceTableRow("End Read") = ""
        invoiceTableRow("End Read Estimated") = ""
        invoiceTableRow("Consumption") = ""
        invoiceTableRow("Force kWh") = "Yes"
        invoiceTableRow("Net") = standingCharge

        vat = standingCharge * _vatRate

        invoiceTableRow("Vat") = vat
        invoiceTableRow("Gross") = (standingCharge + vat).ToString
        invoiceTableRow("Cost Only") = "Yes"
        invoiceTableRow("Description") = "Standing Charge"
        invoiceTable.Rows.Add(invoiceTableRow)


    End Sub

    Private Shared Sub AddCclRows(ByRef dt As DataTable, ByRef invoiceTable As DataTable)


        Dim ccl, vat As Double

        ccl = GetCcl(dt).Replace("£", "")

        Dim invoiceTableRow As DataRow = invoiceTable.NewRow
        invoiceTableRow("Invoice Number") = _invoiceNumber
        invoiceTableRow("Meter Serial") = ""
        invoiceTableRow("MPAN") = ""
        invoiceTableRow("Rate") = ""
        invoiceTableRow("Start Date") = invoiceTable.Rows(0)("Start Date")
        invoiceTableRow("End Date") = invoiceTable.Rows(0)("End Date")
        invoiceTableRow("Start Read") = ""
        invoiceTableRow("Start Read Estimated") = ""
        invoiceTableRow("End Read") = ""
        invoiceTableRow("End Read Estimated") = ""
        invoiceTableRow("Consumption") = ""
        invoiceTableRow("Force kWh") = "Yes"
        invoiceTableRow("Net") = ccl

        vat = ccl * _vatRate

        invoiceTableRow("Vat") = vat
        invoiceTableRow("Gross") = (ccl + vat).ToString
        invoiceTableRow("Cost Only") = "Yes"
        invoiceTableRow("Description") = "CCL"
        invoiceTable.Rows.Add(invoiceTableRow)


    End Sub

    Private Shared Function GetEndRowIndex(ByVal dt As DataTable) As Integer

        For Each row As DataRow In dt.Rows

            For Each item As Object In row.ItemArray
                If item.ToString.ToLower.Trim.Replace(" ", "") = "energykwh(gasusage)" Then
                    Return (dt.Rows.IndexOf(row) - 1)
                End If
            Next
        Next

        Return -1

    End Function

    Private Shared Sub PopulateIsLastDayApportion(InvoiceDt As DataTable)

        For Each row As DataRow In InvoiceDt.Rows
            row("IsLastDayApportion") = "No"
            row("TaxPointDate") = _taxpointdate
        Next

    End Sub

    Private Shared Function SetInvoiceDate(dt As DataTable) As String
        Dim rowFlag As Boolean = False
        For Each row As DataRow In dt.Rows

            For Each item As Object In row.ItemArray
                If rowFlag And Not String.IsNullOrEmpty(item.ToString) Then
                    Return item.ToString
                End If
                If item.ToString.ToLower.Trim.Replace(" ", "").Contains("invoicedate:") Then
                    rowFlag = True
                End If
            Next
        Next

        Return vbNullString
    End Function

    Private Shared Sub RenameColumns(InvoiceDt As DataTable)

        InvoiceDt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        InvoiceDt.Columns("Meter Serial").ColumnName = "MeterSerial"
        InvoiceDt.Columns("Rate").ColumnName = "Tariff"
        InvoiceDt.Columns("Start Date").ColumnName = "StartDate"
        InvoiceDt.Columns("End Date").ColumnName = "EndDate"
        InvoiceDt.Columns("Start Read").ColumnName = "StartRead"
        InvoiceDt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        InvoiceDt.Columns("End Read").ColumnName = "EndRead"
        InvoiceDt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        InvoiceDt.Columns("Force kWh").ColumnName = "ForcekWh"
        InvoiceDt.Columns("Cost Only").ColumnName = "CostOnly"


    End Sub


End Class
