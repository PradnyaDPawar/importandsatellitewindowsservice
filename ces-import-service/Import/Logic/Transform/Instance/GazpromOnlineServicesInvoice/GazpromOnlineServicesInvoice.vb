﻿Public Class GazpromOnlineServicesInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel()

        excel.Load(fullPath)
        Dim xSheet As ExcelSheet = excel.Sheets(0)

        If Not excel.GetSheet("IndividualInvoiceTemplate", xSheet) = True Then
            If Not excel.GetSheet("OldIndividualInvoiceTemplate", xSheet) = True Then
                'Return False
            End If
        End If

        _returnTable = xSheet.GetDataTable()
        DataTableTools.RemoveBlankColumns(_returnTable)
        DataTableTools.RemoveBlankRows(_returnTable)

    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        GazpromOnlineServicesInvoiceSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows

            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next

    End Sub
End Class
