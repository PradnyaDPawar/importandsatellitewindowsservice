﻿Public Class BGElectric2Invoice
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel()

        excel.Load(fullPath)
        Dim xSheet As ExcelSheet = excel.Sheets(0)

        If excel.GetSheet("Invoice Detail", xSheet) Then
            xSheet.HeaderRowIndex = 1

            _returnTable = xSheet.GetDataTable()
        Else
            Throw New ApplicationException("Unable to find the 'Detail' sheet.")
        End If

    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        ColumnValidate()

        GetTransformedHeaders()

        For Each row As DataRow In _returnTable.Rows

            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
