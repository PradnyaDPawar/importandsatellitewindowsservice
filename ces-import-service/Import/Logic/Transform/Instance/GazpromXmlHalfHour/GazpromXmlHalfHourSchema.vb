﻿Public Class GazpromXmlHalfHourSchema

    Public Shared Function ToDeFormat(ByVal uri As String) As DataTable

        Dim dt As DataTable = DeFormatTableTemplates.CreateHalfHourTable
        Dim meterserial, mpan, consumptionDate As String
        Dim hhEntries As New List(Of String)
        Dim xmlElements As XElement = XElement.Load(uri)

        Dim t641Elements = xmlElements...<T641>

        For Each t641Element In t641Elements

            meterserial = t641Element.Attribute("MeterID").Value
            mpan = t641Element.Attribute("SerialNumber").Value

            Dim t642Element = t641Element.Element("T642")
            consumptionDate = t642Element.Attribute("ProfDate").Value
            Dim hhElementName As String
            Dim dateholder As Date = DateTime.Today
            dateholder = dateholder.AddMinutes(30)
            For columncount As Integer = 1 To 48
                hhElementName = dateholder.ToString("HHmm")
                dateholder = dateholder.AddMinutes(30)
                hhElementName = "H" + hhElementName
                If hhElementName = "H0000" Then
                    hhElementName = "H2400"
                End If
                hhEntries.Add(t642Element.Attribute(hhElementName).Value)

            Next

            Dim newRow As DataRow = dt.NewRow
            newRow("Meter Serial") = meterserial
            newRow("MPAN") = mpan
            newRow("Force kWh") = "Yes"
            newRow("Date (DD/MM/YYYY)") = consumptionDate

            dateholder = DateTime.Today
            For columncount As Integer = 1 To 48

                newRow(dateholder.ToString("HH:mm")) = hhEntries.ElementAt(columncount - 1)
                dateholder = dateholder.AddMinutes(30)

            Next
            dt.Rows.Add(newRow)
            hhEntries.Clear()
        Next


        Return dt
    End Function


End Class
