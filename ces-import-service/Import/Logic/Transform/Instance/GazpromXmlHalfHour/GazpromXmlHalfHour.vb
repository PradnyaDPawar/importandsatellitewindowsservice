﻿Public Class GazpromXmlHalfHour
    Implements ITransformDelegate


    Public Function Invoke(ByVal stagingFilePath As String, ByRef returnTable As System.Data.DataTable) As Boolean Implements ITransformDelegate.Invoke

        returnTable = GazpromXmlHalfHourSchema.ToDeFormat(stagingFilePath)

    End Function
End Class
