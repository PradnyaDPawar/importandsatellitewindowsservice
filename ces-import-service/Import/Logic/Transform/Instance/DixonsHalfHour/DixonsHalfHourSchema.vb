﻿Public Class DixonsHalfHourSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("Core")
        headers.Add("Reading Date")

        headers.Add("00:30:00")
        headers.Add("01:00:00")
        headers.Add("01:30:00")
        headers.Add("02:00:00")
        headers.Add("02:30:00")
        headers.Add("03:00:00")
        headers.Add("03:30:00")
        headers.Add("04:00:00")
        headers.Add("04:30:00")
        headers.Add("05:00:00")
        headers.Add("05:30:00")
        headers.Add("06:00:00")
        headers.Add("06:30:00")
        headers.Add("07:00:00")
        headers.Add("07:30:00")
        headers.Add("08:00:00")
        headers.Add("08:30:00")
        headers.Add("09:00:00")
        headers.Add("09:30:00")
        headers.Add("10:00:00")
        headers.Add("10:30:00")
        headers.Add("11:00:00")
        headers.Add("11:30:00")
        headers.Add("12:00:00")
        headers.Add("12:30:00")
        headers.Add("13:00:00")
        headers.Add("13:30:00")
        headers.Add("14:00:00")
        headers.Add("14:30:00")
        headers.Add("15:00:00")
        headers.Add("15:30:00")
        headers.Add("16:00:00")
        headers.Add("16:30:00")
        headers.Add("17:00:00")
        headers.Add("17:30:00")
        headers.Add("18:00:00")
        headers.Add("18:30:00")
        headers.Add("19:00:00")
        headers.Add("19:30:00")
        headers.Add("20:00:00")
        headers.Add("20:30:00")
        headers.Add("21:00:00")
        headers.Add("21:30:00")
        headers.Add("22:00:00")
        headers.Add("22:30:00")
        headers.Add("23:00:00")
        headers.Add("23:30:00")
        headers.Add("24:00:00")

        Return headers
    End Function


    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        AddColumns(dt)
        RenameColumns(dt)
        PopulateColumns(dt)

    End Sub


    Private Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("MeterSerial")
        dt.Columns.Add("ForcekWh")


    End Sub

    Private Shared Sub RenameColumns(ByVal dt As DataTable)
        Dim halfHourEntry As DateTime
        dt.Columns("Reading Date").ColumnName = "Date(dd/mm/yyyy)"
        dt.Columns("Core").ColumnName = "MPAN"

        'Rename HH entries
        For Each column As DataColumn In dt.Columns
            If DateTime.TryParse(column.ColumnName, halfHourEntry) Then
                column.ColumnName = halfHourEntry.ToString("HH:mm")
            End If
        Next

        dt.Columns("24:00").ColumnName = "00:00"

    End Sub

    Private Shared Sub PopulateColumns(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows

            row("ForceKwh") = "Yes"

        Next

    End Sub



End Class
