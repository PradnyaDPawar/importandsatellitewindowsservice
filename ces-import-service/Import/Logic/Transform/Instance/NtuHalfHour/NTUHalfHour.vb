﻿Public Class NTUHalfHour
    Implements ITransformDelegate
    Private _returntable As DataTable

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returntable
        End Get

        Set(ByVal value As DataTable)
            _returntable = value
        End Set
    End Property


    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        Dim csvFile As New DelimitedFile(fullPath, 0)
        csvFile.HasHeader = True
        csvFile.Load(_returntable)

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        NTUHalfHourSchema.ToDeFormat(_returntable)

        For Each row As DataRow In _returntable.Rows
            row("ForcekWh") = "No"
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next

    End Sub

End Class
