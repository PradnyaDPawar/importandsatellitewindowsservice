﻿Public Class NTUHalfHourSchema

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)

    End Sub

    ''nikitah:refactoring code
    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim map As New Dictionary(Of String, DataRow)
        Dim dateholder, datetimeHolder As String
        Dim time As DateTime
        For Each row As DataRow In dt.Rows
            If Not String.IsNullOrEmpty(row("datetime")) Then
                datetimeHolder = row("datetime")
                time = datetimeHolder.ToString().Split(" ")(1)
                dateholder = datetimeHolder.ToString().Split(" ")(0)
                Dim timestring = time.ToString("HH:mm")
                For Each column As DataColumn In dt.Columns
                    If column.ColumnName <> "Date Time" Then
                        Dim key As String = dateholder & row("sensorID")
                        If map.ContainsKey(key) Then
                            Dim theRow As DataRow = map(key)
                            theRow(timestring) = row("Value")
                        Else
                            Dim newRow As DataRow = hhDataTable.NewRow

                            newRow("Date(dd/mm/yyyy)") = dateholder
                            newRow("MeterSerial") = row("sensorID")
                            newRow(timestring) = row("Value")

                            hhDataTable.Rows.Add(newRow)
                            map.Add(key, newRow)
                        End If
                    End If
                Next
            End If
        Next
        dt = hhDataTable
    End Sub

End Class
