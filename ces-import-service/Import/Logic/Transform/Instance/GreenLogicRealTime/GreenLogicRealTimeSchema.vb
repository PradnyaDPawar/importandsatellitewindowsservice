﻿Public Class GreenLogicRealTimeSchema


    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

        Dim realTimeDt As DataTable
        realTimeDt = DeFormatTableTemplates.CreateRealTimeTable()

        PopulateColumns(dt, realTimeDt)

        Return dt

    End Function


    

    Private Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)
        'Dim StartDate As DateTime = System.DateTime.Now
        Dim map As New Dictionary(Of String, DataRow)
        For Each row As DataRow In dt.Rows
            If Not String.IsNullOrEmpty(row("Date Time")) Then
                Dim dateHolder As DateTime = CDate(row("Date Time"))
                Dim theDateTimeString = dateHolder.ToString("dd/MM/yyyy HH:mm")
                For Each column As DataColumn In dt.Columns
                    If column.ColumnName <> "Date Time" Then
                        Dim key As String = column.ColumnName & theDateTimeString
                        If map.ContainsKey(key) Then
                            Dim theRow As DataRow = map(key)
                            theRow("CountUnits") = row(column)
                        Else
                            Dim realTimeDtRow As DataRow = realTimeDt.NewRow
                            realTimeDtRow("MeterName") = column.ColumnName
                            realTimeDtRow("DateTime") = theDateTimeString
                            realTimeDtRow("CountUnits") = row(column)
                            realTimeDt.Rows.Add(realTimeDtRow)
                            map.Add(key, realTimeDtRow)
                        End If
                    End If
                Next
            End If
        Next
        dt = realTimeDt

        'Dim EndDate As DateTime = System.DateTime.Now
        'Dim TimeDifference As TimeSpan = EndDate - StartDate
        'Console.Out.WriteLine("Elapsed time =  " & TimeDifference.TotalMilliseconds.ToString() & " milliseconds. ")

    End Sub
End Class
