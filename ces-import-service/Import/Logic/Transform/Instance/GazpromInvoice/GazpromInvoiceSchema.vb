﻿Public Class GazpromInvoiceSchema


    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("AccountNum")
        headers.Add("InvoiceNo")
        headers.Add("SiteRefNum")
        headers.Add("Tax Point Date")
        headers.Add("Credited")
        headers.Add("MeterPoint")
        headers.Add("MeterNo")
        headers.Add("Corrected Consumption")
        headers.Add("CorrFactor")
        headers.Add("CV")
        headers.Add("Energy")
        headers.Add("UnitPrice")
        headers.Add("GasCost")
        headers.Add("Standing Chrg")
        headers.Add("Sub Total")
        headers.Add("CommPercentage")
        headers.Add("VAT")
        headers.Add("CC Levey")
        headers.Add("CCL VAT")
        headers.Add("Total")
        headers.Add("SiteRefName")
        headers.Add("AddressLine1")
        headers.Add("AddressLine2")
        headers.Add("AddressLine3")
        headers.Add("AddressLine4")
        headers.Add("PostCode")
        headers.Add("CurrentReading")
        headers.Add("CurrentType")
        headers.Add("EndDate")
        headers.Add("PreviousReading")
        headers.Add("PreviousType")
        headers.Add("StartDate")

        Return headers

    End Function

    Public Shared Function Validate(ByRef dt As DataTable) As Boolean

        For Each header As String In GetHeaders()
            If Not dt.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

        RenameColumns(dt)
        CreateColumns(dt)
        PopulateColumns(dt)
        AddCostOnlyLines(dt)
        DataTableTools.SortDataTable(dt, "InvoiceNumber")

        Return dt

    End Function

    Public Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("InvoiceNo").ColumnName = "InvoiceNumber"
        dt.Columns("MeterNo").ColumnName = "MeterSerial"
        dt.Columns("MeterPoint").ColumnName = "MPAN"
        'dt.Columns("StartDate").ColumnName = "Start Date"
        dt.Columns("PreviousReading").ColumnName = "StartRead"
        dt.Columns("PreviousType").ColumnName = "StartReadEstimated"
        'dt.Columns("EndDate").ColumnName = "End Date"
        dt.Columns("CurrentReading").ColumnName = "EndRead"
        dt.Columns("CurrentType").ColumnName = "EndReadEstimated"
        dt.Columns("Energy").ColumnName = "Consumption"
        dt.Columns("VAT").ColumnName = "Original VAT"
        dt.Columns("Tax Point Date").ColumnName = "TaxPointDate"


    End Sub

    Public Shared Sub CreateColumns(ByRef dt As DataTable)

        dt.Columns.Add("Tariff")
        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("InvoiceRate")
        dt.Columns.Add("CostUnit")
        dt.Columns.Add("Net")
        dt.Columns.Add("Vat")
        dt.Columns.Add("Gross")
        dt.Columns.Add("CostOnly")
        dt.Columns.Add("Description")
        dt.Columns.Add("IsLastDayApportion")
        dt.Columns.Add("IsTransmissionTariff")


    End Sub

    Private Shared Sub PopulateColumns(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows

            PopulateTariff(row)
            PopulateForceKwh(row)
            PopulateIsLastDayApportion(row)

        Next

    End Sub

    Private Shared Sub PopulateTariff(ByRef row As DataRow)

        row("Tariff") = "Gas"

    End Sub

    Private Shared Sub PopulateForceKwh(ByRef row As DataRow)

        row("ForcekWh") = "Yes"

    End Sub

    Private Shared Sub PopulateGross(ByRef row As DataRow)
        Dim net, vat As Double
        net = row("net")
        vat = row("vat")
        row("Gross") = net + vat

    End Sub


    Public Shared Sub AddCostOnlyLines(ByRef dt As DataTable)

        Dim caseno As Integer
        Dim vatcost, standingcharge, ccl, cclvatcost, gascost, vat20percent, vat5percent As Double
        vat20percent = 0.2
        vat5percent = 0.05

        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            If row("GasCost").ToString.Replace("£", "") = 0 And row("CC Levey").ToString.Replace("£", "") = 0 Then
                caseno = 1
            End If

            If row("Standing Chrg").ToString.Replace("£", "") <> 0 And row("GasCost").ToString.Replace("£", "") <> 0 Then
                caseno = 2
            End If

            If row("GasCost").ToString.Replace("£", "") <> 0 And row("Standing Chrg").ToString.Replace("£", "") = 0 And row("CC Levey").ToString.Replace("£", "") = 0 Then
                caseno = 3
            End If

            Select Case caseno

                Case 1
                    'Row with no Gas charge. Net charge equals Standing Charge.

                    standingcharge = row("Standing Chrg").ToString.Replace("£", "")
                    row("Net") = standingcharge

                    vatcost = standingcharge * vat5percent
                    row("Vat") = vatcost.ToString("0.00")

                    row("CostOnly") = "Yes"
                    row("Description") = "Standing Charge"
                    PopulateGross(row)


                Case 2
                    'Row with Gas Cost, Standing charge and CCL. Create a new row for including Standing charge
                    'and CCL

                    gascost = row("GasCost").ToString.Replace("£", "")
                    row("Net") = gascost
                    vatcost = gascost * vat20percent
                    row("Vat") = vatcost.ToString("0.00")
                    row("CostOnly") = "No"
                    row("Description") = "Gas Charge"
                    PopulateGross(row)

                    ' Copy row for Standing charge and CCL
                    dt.ImportRow(row)
                    standingcharge = dt.Rows(dt.Rows.Count - 1)("Standing Chrg").ToString.Replace("£", "")
                    dt.Rows(dt.Rows.Count - 1)("Net") = standingcharge
                    vatcost = standingcharge * vat20percent
                    dt.Rows(dt.Rows.Count - 1)("Vat") = vatcost.ToString("0.00")
                    dt.Rows(dt.Rows.Count - 1)("CostOnly") = "Yes"
                    dt.Rows(dt.Rows.Count - 1)("Gross") = (vatcost + standingcharge).ToString("0.00")
                    dt.Rows(dt.Rows.Count - 1)("Description") = "Standing Charge"
                    dt.Rows(dt.Rows.Count - 1)("StartReadEstimated") = ""
                    dt.Rows(dt.Rows.Count - 1)("StartRead") = ""
                    dt.Rows(dt.Rows.Count - 1)("EndReadEstimated") = ""
                    dt.Rows(dt.Rows.Count - 1)("EndRead") = ""
                    dt.Rows(dt.Rows.Count - 1)("Consumption") = ""
                    dt.Rows(dt.Rows.Count - 1)("Tariff") = ""

                    ' Copy row for CCL
                    dt.ImportRow(row)
                    ccl = dt.Rows(dt.Rows.Count - 1)("CC Levey").ToString.Replace("£", "")
                    cclvatcost = dt.Rows(dt.Rows.Count - 1)("CCL VAT").ToString.Replace("£", "")
                    dt.Rows(dt.Rows.Count - 1)("Net") = ccl
                    dt.Rows(dt.Rows.Count - 1)("Vat") = dt.Rows(dt.Rows.Count - 1)("CCL VAT").ToString.Replace("£", "")
                    dt.Rows(dt.Rows.Count - 1)("Gross") = (ccl + cclvatcost).ToString("0.00")
                    dt.Rows(dt.Rows.Count - 1)("Description") = "CCL"
                    dt.Rows(dt.Rows.Count - 1)("CostOnly") = "Yes"
                    dt.Rows(dt.Rows.Count - 1)("StartReadEstimated") = ""
                    dt.Rows(dt.Rows.Count - 1)("StartRead") = ""
                    dt.Rows(dt.Rows.Count - 1)("EndReadEstimated") = ""
                    dt.Rows(dt.Rows.Count - 1)("EndRead") = ""
                    dt.Rows(dt.Rows.Count - 1)("Consumption") = ""
                    dt.Rows(dt.Rows.Count - 1)("Tariff") = ""

                Case 3
                    'Row with only Gas cost. Net equals Gas costs.

                    gascost = row("GasCost").ToString.Replace("£", "")
                    row("Net") = gascost

                    vatcost = gascost * vat20percent
                    row("Vat") = vatcost.ToString("0.00")

                    row("CostOnly") = "No"
                    row("Description") = "Gas Charge"
                    PopulateGross(row)


            End Select


        Next

    End Sub

    Private Shared Sub PopulateIsLastDayApportion(row As DataRow)

        row("IsLastDayApportion") = "No"

    End Sub

End Class
