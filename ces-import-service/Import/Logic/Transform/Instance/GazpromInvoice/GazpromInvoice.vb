﻿Public Class GazpromInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel()

        excel.Load(fullPath)

        Dim xsheet As ExcelSheet = excel.Sheets(0)

        If Not excel.GetSheet("billing summary standard", xsheet) = True Then

            If Not excel.GetSheet("billing summary group id", xsheet) = True Then
                'Return False
            End If

        End If

        xsheet.HeaderRowIndex = 1

        _returnTable = xsheet.GetDataTable()

    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        GazpromInvoiceSchema.Validate(_returnTable)

        GazpromInvoiceSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows

            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
