﻿Public Class NpowerFormat3HHSchema


    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)

        Return dt
    End Function

    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim dateholder As String
        Dim time As DateTime

        Dim success As Boolean = False

        For Each row As DataRow In dt.Rows

            If Not String.IsNullOrEmpty(row("Date")) Then

                dateholder = row("Date")
                time = dateholder.ToString().Split(" ")(1)
                Dim timestring = time.ToString("HH:mm")

                For Each hhrow As DataRow In hhDataTable.Rows

                    If hhrow("Date(dd/mm/yyyy)") = dateholder.ToString().Split(" ")(0) And hhrow("MPAN") = row("Mpan") Then
                        hhrow(timestring) = row("Consumption (kWh)")
                        hhrow("ForcekWh") = "Yes"
                        hhrow("MPAN") = row("Mpan")

                        success = True
                    End If

                Next

                If Not success Then

                    Dim newRow As DataRow = hhDataTable.NewRow
                    newRow("Date(dd/mm/yyyy)") = dateholder.ToString().Split(" ")(0)
                    newRow(timestring) = row("Consumption (kWh)")
                    newRow("ForcekWh") = "Yes"
                    newRow("MPAN") = row("Mpan")
                    hhDataTable.Rows.Add(newRow)

                End If

            End If
            success = False

        Next

        dt = hhDataTable

    End Sub

End Class
