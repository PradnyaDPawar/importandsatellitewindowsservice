﻿Public Class PortalRealTimeSchema
    Public Shared Function ToDeFormat(ByRef dt As DataTable,byref filename As String) As DataTable

        Dim meteralias As String = filename


        Dim realTimeDt As DataTable
        realTimeDt = DeFormatTableTemplates.CreateRealTimeTable()

        dt.Rows.RemoveAt(0)



        PopulateColumns(dt, realTimeDt, meteralias)
        Return dt


    End Function
    Private Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable, ByRef metername As String)
        Dim DateTimeNew As String = ""

        For Each row As DataRow In dt.Rows


            DateTimeNew = row("0").ToString().Remove(row("0").ToString().Length - 3).ToString()

            Dim realTimeDtRow As DataRow = realTimeDt.NewRow

            realTimeDtRow("MeterName") = metername
            realTimeDtRow("DateTime") = CDate(DateTimeNew).ToString("dd/MM/yyyy H:mm")
            If (row("3").ToString = "") Then
                realTimeDtRow("CountUnits") = ""
            ElseIf (CInt(row("3")) = 0) Then
                realTimeDtRow("CountUnits") = 0
            Else
                realTimeDtRow("CountUnits") = row("3")
            End If
            realTimeDt.Rows.Add(realTimeDtRow)
        Next


        dt = realTimeDt

    End Sub
End Class
