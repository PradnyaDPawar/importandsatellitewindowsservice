﻿Public Class PortalRealTime
    Implements ITransformDelegate
    Private _returnTable As DataTable
    Private filename As String
    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property
    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvfile As New DelimitedFile(fullPath, 0)
        filename = System.IO.Path.GetFileNameWithoutExtension(fullPath)
        filename = filename.Substring(filename.IndexOf("_") + 1)
        csvfile.HasHeader = False

        csvfile.Load(_returnTable)





    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat


        PortalRealTimeSchema.ToDeFormat(_returnTable, filename)

        For Each row As DataRow In _returnTable.Rows
            FE_RealTime.Fill(row, dsEdits, importEdit)
        Next


    End Sub
End Class
