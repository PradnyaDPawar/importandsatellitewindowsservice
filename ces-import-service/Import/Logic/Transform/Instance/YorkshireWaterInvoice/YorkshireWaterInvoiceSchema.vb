﻿Public Class YorkshireWaterInvoiceSchema
    Shared Sub ToDeFormat(ByRef returnTable As DataTable)
        Dim invoiceDataTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        CreateInvoiceEntries(returnTable, invoiceDataTable)
        RenameColumns(invoiceDataTable)
        returnTable = invoiceDataTable

    End Sub

    Private Shared Sub CreateInvoiceEntries(ByRef returnTable As DataTable, ByRef invoiceDataTable As DataTable)
        For Each row As DataRow In returnTable.Rows
            PopulateEntries(row, invoiceDataTable, "Water Standing Charge")
            PopulateEntries(row, invoiceDataTable, "Water Volume 1")
            PopulateEntries(row, invoiceDataTable, "Water Volume Rate 1")
            PopulateEntries(row, invoiceDataTable, "Water Volume 2")
            PopulateEntries(row, invoiceDataTable, "Water Volume Rate 2")
            PopulateEntries(row, invoiceDataTable, "Sewerage Standing Charge")
            PopulateEntries(row, invoiceDataTable, "Sewerage %")
            PopulateEntries(row, invoiceDataTable, "Sewerage Volume 1")
            PopulateEntries(row, invoiceDataTable, "Sewerage Volume Rate 1")
            PopulateEntries(row, invoiceDataTable, "Sewerage Volume 2")
            PopulateEntries(row, invoiceDataTable, "Sewerage Volume Rate 2")
            PopulateEntries(row, invoiceDataTable, "Surface Water Charge")
            PopulateEntries(row, invoiceDataTable, "VAT Amount")
        Next
    End Sub
    Private Shared Sub PopulateEntries(dtRow As DataRow, invoiceDataTable As DataTable, ColName As String)
        Try
            If dtRow.Table.Columns.Contains(ColName) Then

                ' If Not String.IsNullOrEmpty(dtRow(ColName)) Then

                Dim invoiceTableRow As DataRow = invoiceDataTable.NewRow

                invoiceTableRow("Invoice Number") = dtRow("Invoice No") + "-" + dtRow("Meter No_")
                invoiceTableRow("Meter Serial") = dtRow("Serial No")
                invoiceTableRow("MPAN") = dtRow("Account No")
                invoiceTableRow("Rate") = ColName
                invoiceTableRow("TaxPointDate") = Convert.ToDateTime(dtRow("Invoice Date")).ToString("dd/MM/yyyy")
                invoiceTableRow("Start Date") = Convert.ToDateTime(dtRow("Period From")).ToString("dd/MM/yyyy")
                invoiceTableRow("End Date") = Convert.ToDateTime(dtRow("Period To")).ToString("dd/MM/yyyy")

                'Populate Cost Only
                Dim costOnly As String = PopulateCostOnly(ColName.Trim)
                invoiceTableRow("Cost Only") = costOnly

                If costOnly = "No" Then
                    invoiceTableRow("Start Read") = dtRow("Previous Reading")
                    PopulateReadEstimation(invoiceTableRow, dtRow)
                    'invoiceTableRow("Start Read Estimated") = ("Previous Reading Code")
                    invoiceTableRow("End Read") = dtRow("Present Reading")
                    'invoiceTableRow("End Read Estimated") = dtRow("Present Reading Code")
                End If

                invoiceTableRow("Force kWh") = "No"
                invoiceTableRow("VAT") = 0.0
                invoiceTableRow("IsLastDayApportion") = "Yes"

                If (ColName.Contains("Volume") Or ColName.Contains("Rate")) And Not (ColName.Contains("Volume Rate")) Then
                    PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, ColName, dtRow(ColName), dtRow(ColName.Replace("1", "Charge 1")))
                ElseIf (ColName.Contains("Volume Rate") Or ColName = "Sewerage %") Then
                    PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, ColName, dtRow(ColName), 0.0)
                Else
                    PopulateEntriesBasedOnColName(invoiceTableRow, dtRow, ColName, 0.0, dtRow(ColName))
                End If

                invoiceDataTable.Rows.Add(invoiceTableRow)
            End If
            'End If
        Catch ex As Exception

        End Try

    End Sub
    Private Shared Sub PopulateEntriesBasedOnColName(ByRef invoiceTableRow As DataRow, ByRef dtRow As DataRow, ByRef Column As String, ByVal Consumption As String, ByVal Cost As String)
        Try
            invoiceTableRow("Consumption") = IIf(String.IsNullOrEmpty(Consumption), 0.0, Consumption)

            'Set Vat,Net and Gross
            If Column.Contains("VAT") Then
                invoiceTableRow("Net") = 0.0
                invoiceTableRow("VAT") = IIf(String.IsNullOrEmpty(Cost), 0.0, Cost)
            Else
                invoiceTableRow("Net") = IIf(String.IsNullOrEmpty(Cost), 0.0, Cost)
                invoiceTableRow("VAT") = 0.0
            End If

            invoiceTableRow("Gross") = Convert.ToDouble(invoiceTableRow("Net")) + Convert.ToDouble(invoiceTableRow("VAT"))
            invoiceTableRow("Description") = invoiceTableRow("Rate")
        Catch ex As Exception
        End Try
    End Sub
    Private Shared Sub RenameColumns(invoiceDataTable As DataTable)
        invoiceDataTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceDataTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceDataTable.Columns("Rate").ColumnName = "Tariff"
        invoiceDataTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceDataTable.Columns("End Date").ColumnName = "EndDate"
        invoiceDataTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceDataTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceDataTable.Columns("End Read").ColumnName = "EndRead"
        invoiceDataTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceDataTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceDataTable.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub
    ''' <summary>
    ''' Cost Only= No when Column => Water Volume Charge 1 and Water Volume Charge 2 , everything else = Yes
    ''' </summary>
    ''' <param name="ColName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function PopulateCostOnly(ByRef ColName As String) As String
        Dim costOnly As String = String.Empty
        Select Case ColName
            Case "Water Volume 1", "Water Volume 2"
                costOnly = "No"
            Case Else
                costOnly = "Yes"
        End Select
        Return costOnly
    End Function

    Private Shared Sub PopulateReadEstimation(ByRef invoiceTableRow As DataRow, ByVal row As DataRow)
        Dim ListActual As New List(Of Char)(New Char() {"a", "b", "c", "f", "i", "k", "l", "m", "p", "r", "s", "v", "x"})
        Dim ListEstimated As New List(Of Char)(New Char() {"d", "e", "t", "y", "z"})

        ' Start Read Estimated
        If ListActual.Contains(row("Previous Reading Code").ToString.ToLower) Then
            invoiceTableRow("Start Read Estimated") = "A"
        ElseIf ListEstimated.Contains(row("Previous Reading Code").ToString.ToLower) Then
            invoiceTableRow("Start Read Estimated") = "E"
        End If

        ' End Read Estimated
        If ListActual.Contains(row("Present Reading Code").ToString.ToLower) Then
            invoiceTableRow("End Read Estimated") = "A"
        ElseIf ListEstimated.Contains(row("Present Reading Code").ToString.ToLower) Then
            invoiceTableRow("End Read Estimated") = "E"
        End If

    End Sub
End Class
