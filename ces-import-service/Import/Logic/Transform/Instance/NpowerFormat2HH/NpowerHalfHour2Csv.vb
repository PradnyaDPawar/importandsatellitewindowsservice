﻿Public Class NpowerHalfHour2Csv
    Implements ITransformDelegate


    Public Function Invoke(ByVal stagingFilePath As String, ByRef returnTable As System.Data.DataTable) As Boolean Implements ITransformDelegate.Invoke
        Try
            Dim csvFile As New DelimitedFile
            csvFile.Load(stagingFilePath)
            returnTable = csvFile.ToDataTable(False)
            DataTableTools.SetHeaders(returnTable, 4)
            DeleteRows(returnTable, 4)
            NpowerHalfHour2Schema.Validate(returnTable)
            NpowerHalfHour2Schema.ToDeFormat(returnTable)
            returnTable.TableName = "Half Hour Format 2"
            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Shared Sub DeleteRows(ByRef dt As DataTable, ByRef headerRowIndex As Integer)
        'Pre header rows
        Dim preheaderrows As Integer
        For preheaderrows = 0 To headerRowIndex - 2 Step 1
            dt.Rows(preheaderrows).Delete()
        Next
        dt.AcceptChanges()

    End Sub

End Class
