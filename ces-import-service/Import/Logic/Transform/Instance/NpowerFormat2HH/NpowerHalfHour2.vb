﻿Public Class NpowerHalfHour2
    Implements ITransformDelegate

    Public Function Invoke(ByVal stagingFilePath As String, ByRef returnTable As System.Data.DataTable) As Boolean Implements ITransformDelegate.Invoke
        Try
            Dim excel As New Excel
            excel.Load(stagingFilePath)
            Dim xsheet As ExcelSheet = excel.Sheets(0)
            If Not xsheet.SetHeaderRow(NpowerHalfHour2Schema.GetHeaders(), ExcelSearchEnum.ContainsAll) Then
                Return False
            End If
            returnTable = xsheet.GetDataTable
            DeletePreHeaderRows(returnTable, xsheet)
            NpowerHalfHour2Schema.Validate(returnTable)
            NpowerHalfHour2Schema.ToDeFormat(returnTable)
            returnTable.TableName = "Half Hour Format 2"
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function


    Public Shared Sub DeletePreHeaderRows(ByRef dt As DataTable, ByRef xlsheet As ExcelSheet)

        For deleteCounter As Integer = 0 To xlsheet.HeaderRowIndex - 2 Step 1
            dt.Rows(deleteCounter).Delete()
        Next
        dt.AcceptChanges()
    End Sub

End Class