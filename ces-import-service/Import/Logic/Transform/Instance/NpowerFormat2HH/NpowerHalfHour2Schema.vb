﻿Public Class NpowerHalfHour2Schema

    Public Shared Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("mpan")
        headers.Add("rdate")
        headers.Add("TotalConsumptionKwh")
        headers.Add("MaxDemand")
        headers.Add("colStatuskwh")
        headers.Add("Kwh1")
        headers.Add("Kwh2")
        headers.Add("Kwh3")
        headers.Add("Kwh4")
        headers.Add("Kwh5")
        headers.Add("Kwh6")
        headers.Add("Kwh7")
        headers.Add("Kwh8")
        headers.Add("Kwh9")
        headers.Add("Kwh10")
        headers.Add("Kwh11")
        headers.Add("Kwh12")
        headers.Add("Kwh13")
        headers.Add("Kwh14")
        headers.Add("Kwh15")
        headers.Add("Kwh16")
        headers.Add("Kwh17")
        headers.Add("Kwh18")
        headers.Add("Kwh19")
        headers.Add("Kwh20")
        headers.Add("Kwh21")
        headers.Add("Kwh22")
        headers.Add("Kwh23")
        headers.Add("Kwh24")
        headers.Add("Kwh25")
        headers.Add("Kwh26")
        headers.Add("Kwh27")
        headers.Add("Kwh28")
        headers.Add("Kwh29")
        headers.Add("Kwh30")
        headers.Add("Kwh31")
        headers.Add("Kwh32")
        headers.Add("Kwh33")
        headers.Add("Kwh34")
        headers.Add("Kwh35")
        headers.Add("Kwh36")
        headers.Add("Kwh37")
        headers.Add("Kwh38")
        headers.Add("Kwh39")
        headers.Add("Kwh40")
        headers.Add("Kwh41")
        headers.Add("Kwh42")
        headers.Add("Kwh43")
        headers.Add("Kwh44")
        headers.Add("Kwh45")
        headers.Add("Kwh46")
        headers.Add("Kwh47")
        headers.Add("Kwh48")

        Return headers
    End Function

    Public Shared Function Validate(ByRef dt As DataTable) As Boolean

        For Each header As String In GetHeaders()
            If Not dt.Columns.Contains(header) Then
                Throw New CustomException("Missing Column " & header & ". ")
            End If
        Next

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable)
        CreateColumns(dt)
        RenameColumns(dt)
        For Each row As DataRow In dt.Rows
            PopulateForceKwh(row)
            ReformatDate(row)
        Next
        Return dt
    End Function

    Private Shared Sub CreateColumns(ByRef dt As DataTable)
        dt.Columns.Add("Meter Serial")
        dt.Columns.Add("Force kWh")
    End Sub

    Private Shared Sub PopulateForceKwh(ByVal row As DataRow)
        row("Force kWh") = "Yes"
    End Sub

    Private Shared Sub ReformatDate(ByVal row As DataRow)
        Dim dateHolder As Date
        dateHolder = row("Date (dd/mm/yyyy)")
        row("Date (dd/mm/yyyy)") = dateHolder.ToShortDateString
    End Sub

    Private Shared Sub RenameColumns(ByRef dt As DataTable)
        dt.Columns("mpan").ColumnName = "MPAN"
        dt.Columns("rdate").ColumnName = "Date (dd/mm/yyyy)"

        'half hour headers
        Dim dateHolder As DateTime = DateTime.Today
        For count As Integer = 1 To 48
            dt.Columns("kWh" & count.ToString).ColumnName = dateHolder.ToString("HH:mm")
            dateHolder = dateHolder.AddMinutes(30)
        Next
    End Sub
End Class
