﻿Public Class TeamSigmaRealTimeSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

        Dim realTimeDt As DataTable
        realTimeDt = DeFormatTableTemplates.CreateRealTimeTable()
        PopulateColumns(dt, realTimeDt)

        Return dt

    End Function

    Public Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)

        'without conversion

        Dim success As Boolean = False
        Dim theDate As DateTime
        Dim theTime As DateTime
        Dim DateTime As DateTime


        For Each row As DataRow In dt.Rows

            If IsDate(row("Date")) Then

                theDate = row("Date")
                theTime = row("StartTime")

                Dim theTimeString = theTime.ToString("HH:mm")
                DateTime = theDate + " " + theTimeString

                For Each Rrow As DataRow In realTimeDt.Rows

                    If Rrow("MeterName") = row("Meter") And Rrow("DateTime") = theDate + " " + theTimeString Then
                        Rrow(theTimeString) = row("TotalValue")
                        success = True
                    End If

                Next


                If Not success Then
                    Dim realTimeDtRow As DataRow = realTimeDt.NewRow

                    realTimeDtRow("MeterName") = row("Meter")
                    realTimeDtRow("DateTime") = theDate + " " + theTimeString
                    realTimeDtRow("CountUnits") = row("TotalValue")

                    realTimeDt.Rows.Add(realTimeDtRow)
                End If

                success = False

            End If
        Next

        dt = realTimeDt

    End Sub

    'Converting mWh into kWh

    'Public Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)

    '    Dim success As Boolean = False
    '    Dim theDate As DateTime
    '    Dim theTime As DateTime
    '    Dim DateTime As DateTime


    '    For Each row As DataRow In dt.Rows

    '        If IsDate(row("Date")) Then

    '            theDate = row("Date")
    '            theTime = row("StartTime")

    '            Dim theTimeString = theTime.ToString("HH:mm")
    '            DateTime = theDate + " " + theTimeString

    '            Dim realTimeDtRow As DataRow = realTimeDt.NewRow

    '            If row("Unit").ToString.ToLower = "mwh" Then

    '                realTimeDtRow("MeterName") = row("Meter")
    '                realTimeDtRow("DateTime") = theDate + " " + theTimeString
    '                realTimeDtRow("CountUnits") = row("TotalValue") * 1000
    '                realTimeDt.Rows.Add(realTimeDtRow)

    '            Else

    '                realTimeDtRow("MeterName") = row("Meter")
    '                realTimeDtRow("DateTime") = theDate + " " + theTimeString
    '                realTimeDtRow("CountUnits") = row("TotalValue")
    '                realTimeDt.Rows.Add(realTimeDtRow)

    '            End If

    '        End If

    '        success = False

    '    Next

    '    dt = realTimeDt

    'End Sub

End Class
