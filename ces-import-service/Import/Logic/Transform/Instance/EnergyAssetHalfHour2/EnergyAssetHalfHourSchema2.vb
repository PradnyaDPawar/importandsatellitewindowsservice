﻿Partial Public Class EnergyAssetHalfHourSchema2


    Public Shared Function GetHeaders() As List(Of String)

        Dim dateholder As Date = Today()

        Dim headers As New List(Of String)

        headers.Add("MPR")
        headers.Add("Date")

        For i = 0 To 47

            headers.Add(dateholder.AddMinutes(i * 30).ToString("HH:mm"))

        Next

        Return headers

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        ' dt.Rows.RemoveAt(0)
        AddColumns(dt)
        RenameColumnNames(dt)

        Return dt
    End Function
    Public Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("MeterSerial")

    End Sub

    Public Shared Sub RenameColumnNames(ByRef dt As DataTable)

        dt.Columns("MPR").ColumnName = "MPAN"
        dt.Columns("Date").ColumnName = "Date(dd/MM/yyyy)"

    End Sub

End Class
