﻿Partial Public Class SouthernCrossAutomationHH

    Public Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("TimeStamp")
        headers.Add("DataValue")
        headers.Add("MeterID")
        Return headers

    End Function


    'Public Function Validate(ByRef data As DataTable) As Boolean

    '    For Each header As String In GetHeaders()
    '        If Not data.Columns.Contains(header) Then
    '            Throw New ApplicationException("Missing Column " & header & ". ")
    '        End If
    '    Next

    'End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        dt.Columns("TimeStamp").ColumnName = "Date"
        dt.Columns("DataValue").ColumnName = "Consumption"
        dt.Columns("MeterID").ColumnName = "MeterSerial"
        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)
        Return dt
    End Function

    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim dateholder As DateTime
        Dim success As Boolean = False
        For Each row As DataRow In dt.Rows

            If IsDate(row("Date")) Then
                dateholder = row("Date")
                For Each hhrow As DataRow In hhDataTable.Rows
                    If hhrow("Date(dd/mm/yyyy)") = dateholder.Date And hhrow("MeterSerial") = row("MeterSerial") Then
                        hhrow(dateholder.ToString("HH:mm")) = row("Consumption")
                        hhrow("ForcekWh") = "Yes"
                        hhrow("MeterSerial") = row("MeterSerial")
                        success = True
                    End If
                Next

                If Not success Then
                    Dim newRow As DataRow = hhDataTable.NewRow
                    newRow("Date(dd/mm/yyyy)") = dateholder.Date
                    newRow(dateholder.ToString("HH:mm")) = row("Consumption")
                    newRow("ForcekWh") = "Yes"
                    newRow("MeterSerial") = row("MeterSerial")
                    hhDataTable.Rows.Add(newRow)
                End If
            End If
            success = False
        Next

        dt = hhDataTable

    End Sub



End Class
