﻿Partial Public Class TotalGPHalfHour


    Public Function GetHeaders() As List(Of String)

        Dim dateholder As Date = Today()

        Dim headers As New List(Of String)
        headers.Add("ConsumerName")
        headers.Add("MPR")
        headers.Add("MPRAlias")
        headers.Add("Site")
        headers.Add("AMRSchedule")
        headers.Add("ReadDate")
        headers.Add("Reading")
        headers.Add("ConsumptionProfileDate")
        headers.Add("ProfileDateConsumption")

        For i = 0 To 47

            headers.Add(dateholder.AddMinutes(i * 30).ToString("HH:mm"))

        Next

        headers.Add("ReportingUnit")

        Return headers

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable)


        dt.Rows.RemoveAt(0)
        AddColumns(dt)
        RenameColumnNames(dt)
        RemoveUnwantedColumns(dt)

        Return dt
    End Function
    Public Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("MeterSerial")


    End Sub

    Public Shared Sub RenameColumnNames(ByRef dt As DataTable)

        dt.Columns("MPR").ColumnName = "MPAN"
        'Has two columns with date information, but sometimes ConsumptionProfileDate is blank
        dt.Columns("ReadDate").ColumnName = "Date(dd/MM/yyyy)"

    End Sub

    Public Shared Sub RemoveUnwantedColumns(ByRef dt As DataTable)

        dt.Columns.Remove("ConsumerName")
        dt.Columns.Remove("MPRAlias")
        dt.Columns.Remove("Site")
        dt.Columns.Remove("AMRSchedule")
        dt.Columns.Remove("ConsumptionProfileDate")
        dt.Columns.Remove("Reading")
        dt.Columns.Remove("ProfileDateConsumption")
        dt.Columns.Remove("ReportingUnit")

    End Sub



End Class
