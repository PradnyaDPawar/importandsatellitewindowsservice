﻿Public Class EdfDenbighshireInvoiceSchema

    Public Shared Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("")
        headers.Add("")
        headers.Add("NIGHT")
        headers.Add("DAY")
        headers.Add("TOTAL")
        headers.Add("UNIT")
        headers.Add("CCL")
        headers.Add("DEMAND")
        headers.Add("TOTAL")
        headers.Add("P/KWH")
        Return headers
    End Function

    Public Shared Function ToDeFormat(dt As DataTable)
        RetainColumnsWithInfo(dt)
        DataTableTools.RemoveBlankColumns(dt)
        DataTableTools.RemoveBlankRows(dt)
        'Added for BVM phase 3
        AddColumns(dt)
        SetHeaders(dt)

        Return dt
    End Function

    Private Shared Sub SetHeaders(ByRef dt As DataTable)
        Dim blankSub As Integer = 0
        Dim monthIndex As Integer
        Dim newColumnName As String = ""
        'Find the index of the row having Month entry
        For Each row As DataRow In dt.Rows
            If row(0).ToString.ToLower.Trim = "month" Then
                monthIndex = dt.Rows.IndexOf(row)
            End If
        Next
        For Each column As DataColumn In dt.Columns

            'Generate new column name
            For count As Integer = 0 To monthIndex
                newColumnName = newColumnName & " " & dt.Rows(count)(column).ToString
            Next

            If Not String.IsNullOrEmpty(newColumnName) Then
                column.ColumnName = newColumnName
                newColumnName = ""
            Else
                column.ColumnName = "Column" & blankSub.ToString
                blankSub += 1
            End If
        Next

        For count As Integer = 0 To monthIndex
            dt.Rows(count).Delete()
        Next
        dt.AcceptChanges()
    End Sub

    Private Shared Sub RetainColumnsWithInfo(ByRef dt As DataTable)
        If GetHeadingRow(dt) >= 0 Then
            Dim headingRowIndex As Integer = GetHeadingRow(dt)
            'Remove rows before headers
            DeleteRows(dt, headingRowIndex)
            DataTableTools.RemoveBlankColumns(dt)
        Else
            Throw New ApplicationException("Invalid Format - Headers could not be located")
        End If


    End Sub

    Private Shared Function GetHeadingRow(ByRef dt As DataTable) As Integer
        For Each row As DataRow In dt.Rows
            For Each item As Object In row.ItemArray
                If item.ToString.ToLower.Trim = "electric consumption" Then
                    Return dt.Rows.IndexOf(row)
                End If
            Next
        Next
        Return -1
    End Function


    Public Shared Sub DeleteRows(ByRef dt As DataTable, ByRef headerRowIndex As Integer)
        'Pre header rows
        Dim preheaderrows As Integer
        For preheaderrows = 0 To headerRowIndex Step 1
            dt.Rows(0).Delete()
        Next
        dt.AcceptChanges()
    End Sub

    Private Shared Sub AddColumns(ByRef dt As DataTable)
        dt.Columns.Add("InvoiceRate")
        dt.Columns.Add("CostUnit")
    End Sub
End Class
