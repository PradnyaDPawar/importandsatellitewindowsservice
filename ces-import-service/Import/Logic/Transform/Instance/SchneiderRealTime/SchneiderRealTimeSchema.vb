﻿Public Class SchneiderRealTimeSchema

    Public Shared Function ToDeFormat(ByRef tdt As DataTable)
        NameBlankColumns(tdt)
        DataTableTools.SetHeaders(tdt, 1)
        GetReadings(tdt)
        DeleteInitialColumns(tdt)
        DeleteEndColumns(tdt)
        DeleteDuplicateReadings(tdt)
        CreateColumns(tdt)
        RenameColumns(tdt)
        PopulateValue(tdt)
        DeleteColumnEntries(tdt)
        SortOnMeterName(tdt)
        Return tdt
    End Function

    Private Shared Sub GetReadings(ByRef tdt As DataTable)
        Dim nextcolumn As Integer

        For Each row As DataRow In tdt.Rows

            For Each column As DataColumn In tdt.Columns

                If IsDate(column.ColumnName) Then
                    nextcolumn = tdt.Columns.IndexOf(column.ColumnName) + 1
                    row(column) = row(nextcolumn)
                End If

            Next

        Next


    End Sub

    Private Shared Sub DeleteInitialColumns(ByRef tdt As DataTable)
        'hardcoding to delete first four columns

        For colcount As Integer = 0 To 3
            tdt.Columns.RemoveAt(0)
        Next


    End Sub

    Private Shared Sub DeleteEndColumns(ByRef tdt As DataTable)
        'delete from last column till last date entry
        Dim lastdateindex As Integer = tdt.Columns.Count - GetLastDateIndexFromHeaders(tdt)
        For colcount As Integer = tdt.Columns.Count - 1 To lastdateindex Step -1
            tdt.Columns.RemoveAt(lastdateindex)
        Next
    End Sub

    Private Shared Sub DeleteDuplicateReadings(ByRef tdt As DataTable)

        Dim colcounttill As Integer = GetFirstDateIndexFromHeaders(tdt)

        For colcount = tdt.Columns.Count - 1 To colcounttill Step -1
            If Not IsDate(tdt.Columns(colcount).ColumnName) Then
                tdt.Columns.RemoveAt(colcount)
            End If
        Next

    End Sub

    Private Shared Sub CreateColumns(ByRef tdt As DataTable)
        tdt.Columns.Add("CountUnits")
        tdt.Columns.Add("DateTime", GetType(DateTime))
    End Sub

    Private Shared Sub RenameColumns(ByRef tdt As DataTable)
        tdt.Columns("Column 02").ColumnName = "MeterName"
        tdt.Columns("Column 03").ColumnName = "Energy"
    End Sub

    Private Shared Sub PopulateValue(ByRef tdt As DataTable)

        Dim firstdateflag As Boolean = True
        Dim firstdate As DateTime = tdt.Columns(GetFirstDateIndexFromHeaders(tdt)).ColumnName

        For Each row As DataRow In tdt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            For Each column As DataColumn In tdt.Columns

                Dim nextdate As DateTime
                If DateTime.TryParse(column.ColumnName, nextdate) Then
                    If Not DateTime.Compare(firstdate, nextdate) = 0 Then
                        firstdateflag = False
                    End If
                End If


                If IsDate(column.ColumnName) And firstdateflag Then

                    row("CountUnits") = row(column)
                    row("DateTime") = column.ColumnName

                ElseIf IsDate(column.ColumnName) And Not firstdateflag Then

                    Dim newrow As DataRow = tdt.NewRow

                    newrow("CountUnits") = row(column)
                    newrow("DateTime") = column.ColumnName
                    newrow("MeterName") = row("MeterName")
                    newrow("Energy") = row("Energy")
                    tdt.Rows.Add(newrow)

                End If

            Next
            firstdateflag = True
        Next

    End Sub

    Private Shared Sub DeleteColumnEntries(ByRef tdt As DataTable)

        For columncount As Integer = tdt.Columns.Count - 1 To 0 Step -1
            If IsDate(tdt.Columns(columncount).ColumnName) Then
                tdt.Columns.RemoveAt(columncount)
            End If
        Next

    End Sub


    Private Shared Sub NameBlankColumns(ByRef tdt As DataTable)
        Dim numbertoappend As Integer = 1

        For Each column As DataColumn In tdt.Columns

            If tdt.Rows(0)(column) = "" Then
                tdt.Rows(0)(column) = "Column 0" & numbertoappend
                numbertoappend = numbertoappend + 1
            End If


        Next

    End Sub

    Private Shared Function GetFirstDateIndexFromHeaders(ByRef tdt As DataTable) As Integer

        Dim colcount As Integer = 0
        Dim initialdateindex As Integer = 0

        While Not IsDate(tdt.Columns(colcount).ColumnName)
            initialdateindex += 1
            colcount += 1
        End While

        Return initialdateindex

    End Function



    Private Shared Function GetLastDateIndexFromHeaders(ByRef tdt As DataTable) As Integer

        Dim enddateindex As Integer = 0
        Dim colcount As Integer = tdt.Columns.Count - 1

        While Not IsDate(tdt.Columns(colcount).ColumnName)
            enddateindex = enddateindex + 1
            colcount -= 1
        End While

        Return enddateindex
    End Function


    Public Shared Function Validate(ByRef dt As DataTable) As Boolean

        Dim listofdates As List(Of Date)
        Dim StartDate As Date = GetReportStartTime(dt)

        Dim EndDate As Date = GetReportEndTime(dt)

        listofdates = GetListOfAllDates(dt)

        While StartDate < EndDate
            If Not listofdates.Contains(StartDate.ToShortDateString) Then
                Return False
            End If
            StartDate = StartDate.AddDays(1)
        End While

        Return True

    End Function

    Private Shared Function GetListOfAllDates(ByRef dt As DataTable) As List(Of Date)

        Dim listofdates As New List(Of Date)
        Dim datetoadd As Date

        For Each row As DataRow In dt.Rows

            If IsDate(row(0)) Then
                datetoadd = row(0)
                If Not listofdates.Contains(datetoadd.ToShortDateString) Then
                    listofdates.Add(datetoadd)
                End If
            End If

        Next


        Return listofdates

    End Function


    Public Shared Function GetReportStartTime(ByRef dt As DataTable) As String
        Dim found As Boolean
        For Each row As DataRow In dt.Rows
            For Each item As Object In row.ItemArray

                If found Then
                    Return (item.ToString)
                End If

                If item.ToString = "Report Start Time:" Then
                    found = True

                End If
            Next
        Next

        Throw New ApplicationException("ReportStartTime not found")

    End Function

    Public Shared Function GetReportEndTime(ByRef dt As DataTable) As String
        Dim found As Boolean
        For Each row As DataRow In dt.Rows
            For Each item As Object In row.ItemArray

                If found Then
                    Return (item.ToString)
                End If

                If item.ToString = "Report End Time:" Then
                    found = True

                End If
            Next
        Next

        Throw New ApplicationException("ReportEndTime not found")

    End Function

    Private Shared Sub SortOnMeterName(ByRef tdt As DataTable)

        Dim view As DataView = tdt.DefaultView
        view.Sort = "MeterName, DateTime"
        tdt = view.ToTable

    End Sub

End Class
