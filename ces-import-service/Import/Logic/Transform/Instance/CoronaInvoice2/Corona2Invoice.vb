﻿Public Class Corona2Invoice
    Implements ITransformDelegate

    Private _returnTable As DataTable
    Public Shared invoicenumber As String

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel
        excel.Load(fullPath)
        Dim xsheet As ExcelSheet = excel.Sheets(0)


        If Not excel.GetSheet("Summary", xsheet) = True Then
            'Return False
        End If

        ReturnTable = xsheet.GetDataTable()
        invoicenumber = GetInvoiceNumber(ReturnTable)


        If Not excel.GetSheet("Details", xsheet) = True Then
            'Return False
        End If
        ReturnTable = xsheet.GetDataTable()

        If Not xsheet.SetHeaderRow(Corona2InvoiceSchema.GetHeaders(), ExcelSearchEnum.ContainsAll) Then
            'Return False
        End If

        ReturnTable = xsheet.GetDataTable()
    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        Corona2InvoiceSchema.Validate(ReturnTable)
        Corona2InvoiceSchema.ToDeFormat(ReturnTable)

        For Each row As DataRow In ReturnTable.Rows

            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next

    End Sub

    Public Shared Function GetInvoiceNumber(ByRef dt As DataTable) As String
        Dim found As Boolean
        For Each row As DataRow In dt.Rows
            For Each item As Object In row.ItemArray

                If found Then
                    Return (item.ToString)
                End If

                If item.ToString.Trim = "Invoice Number:" Then
                    found = True

                End If
            Next
        Next

        Throw New ArgumentException("Invoice Number not found")

    End Function
End Class
