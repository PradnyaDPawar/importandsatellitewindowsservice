﻿Public Class Corona2InvoiceSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("Line Number")
        headers.Add("Site ID")
        headers.Add("Account Code")
        headers.Add("Address")
        headers.Add("PostCode")
        headers.Add("MPRN")
        headers.Add("MSN")
        headers.Add("AQ")
        headers.Add("Opening Read Date")
        headers.Add("Opening Read")
        headers.Add("Opening Read Type")
        headers.Add("Closing Read Date")
        headers.Add("Closing Read")
        headers.Add("Closing Read Type")
        headers.Add("Units")
        headers.Add("Correction Factor")
        headers.Add("Reading Factor")
        headers.Add("Calorific Value")
        headers.Add("kWh Total")
        headers.Add("Gas Price pence per kWh")
        headers.Add("Delivery Cost")
        headers.Add("Days")
        headers.Add("Net on Gas Cost")
        headers.Add("Volume Subject to CCL")
        headers.Add("CCL Relief %")
        headers.Add("CCL Rate pence per kWh")
        headers.Add("CCL Per Meter")
        headers.Add("AMR Amount")
        headers.Add("VAT on CCL per meter")
        headers.Add("AMR Standard VAT")
        headers.Add("AMR Reduced VAT")
        headers.Add("Net at Standard VAT")
        headers.Add("Invoice Standard VAT")
        headers.Add("Net at Reduced VAT")
        headers.Add("Invoice Reduced VAT")
        headers.Add("Total Net")
        headers.Add("Total VAT")
        headers.Add("Gross")
        headers.Add("Tax Point Date")

        Return headers
    End Function

    Public Shared Function Validate(ByRef data As DataTable) As Boolean

        For Each header As String In GetHeaders()
            If Not data.Columns.Contains(header) Then
                Throw New ArgumentException("Missing Column " & header & ". ")
            End If
        Next

    End Function


    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        RemoveFirstRow(dt)
        DataTableTools.RemoveBlankRows(dt)
        RenameColumns(dt)
        CreateColumns(dt)
        PopulateColumns(dt)
        Return dt
    End Function

    Private Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("MSN").ColumnName = "MeterSerial"
        dt.Columns("MPRN").ColumnName = "MPAN"
        dt.Columns("Opening Read Date").ColumnName = "StartDate"
        dt.Columns("Closing Read Date").ColumnName = "EndDate"
        dt.Columns("Opening Read").ColumnName = "StartRead"
        dt.Columns("Opening Read Type").ColumnName = "StartReadEstimated"
        dt.Columns("Closing Read").ColumnName = "EndRead"
        dt.Columns("Closing Read Type").ColumnName = "EndReadEstimated"
        dt.Columns("Gross").ColumnName = "Original Gross"
        dt.Columns("kWh Total").ColumnName = "Consumption"
        dt.Columns("Tax Point Date").ColumnName = "TaxPointDate"

    End Sub

    Private Shared Sub CreateColumns(ByRef dt As DataTable)

        dt.Columns.Add("Tariff")
        dt.Columns.Add("CostOnly")
        dt.Columns.Add("Description")
        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("InvoiceRate")
        dt.Columns.Add("CostUnit")
        dt.Columns.Add("InvoiceNumber")
        dt.Columns.Add("Net")
        dt.Columns.Add("Vat")
        dt.Columns.Add("Gross")
        dt.Columns.Add("IsLastDayApportion")
        dt.Columns.Add("IsTransmissionTariff")

    End Sub

    Private Shared Sub PopulateColumns(ByRef dt As DataTable)

        Dim deliverycost, gascost, ccl, amr, cclvat, amrvatstd, amrvatreduced, invoicevatstd, _
        invoicevatreduced, net, vat As Double

        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            deliverycost = row("Delivery Cost").Replace("£", "")
            gascost = row("Net on Gas Cost").Replace("£", "")
            ccl = row("CCL Per Meter").Replace("£", "")
            amr = row("AMR Amount").Replace("£", "")
            cclvat = row("VAT on CCL per meter").Replace("£", "")
            amrvatstd = row("AMR Standard VAT").Replace("£", "")
            amrvatreduced = row("AMR Reduced VAT").Replace("£", "")
            invoicevatstd = row("Invoice Standard VAT").Replace("£", "")
            invoicevatreduced = row("Invoice Reduced VAT").Replace("£", "")
            net = deliverycost + gascost
            vat = invoicevatreduced + invoicevatstd - cclvat - amrvatreduced - amrvatstd

            'Populate Net,Vat with Gas or Delivery cost
            row("Net") = net
            row("Vat") = vat
            row("Gross") = net + vat
            If deliverycost = 0 Then
                row("CostOnly") = "No"
            Else
                row("CostOnly") = "Yes"
            End If
            row("InvoiceNumber") = Corona2Invoice.invoicenumber
            row("Tariff") = "Gas"
            row("ForcekWh") = "Yes"
            row("IsLastDayApportion") = "No"

            'Create new costonly row for amr
            dt.ImportRow(row)
            dt.Rows(dt.Rows.Count - 1)("Net") = amr
            dt.Rows(dt.Rows.Count - 1)("Vat") = amrvatreduced + amrvatstd
            dt.Rows(dt.Rows.Count - 1)("Gross") = amr + amrvatreduced + amrvatstd
            dt.Rows(dt.Rows.Count - 1)("CostOnly") = "Yes"
            dt.Rows(dt.Rows.Count - 1)("Description") = "AMR Amount"

            'Create new costonly row for CCL
            dt.ImportRow(row)
            dt.Rows(dt.Rows.Count - 1)("Net") = ccl
            dt.Rows(dt.Rows.Count - 1)("Vat") = cclvat
            dt.Rows(dt.Rows.Count - 1)("Gross") = ccl + cclvat
            dt.Rows(dt.Rows.Count - 1)("CostOnly") = "Yes"
            dt.Rows(dt.Rows.Count - 1)("Description") = "CCL Charge"
        Next
    End Sub

    Private Shared Sub RemoveFirstRow(ByRef dt As DataTable)
        dt.Rows(0).Delete()
        dt.AcceptChanges()
    End Sub

End Class
