﻿Imports System.Globalization

Public Class SiemensHalfHour2Schema

    Private Shared _mpan, _serial, _unit, _readingDateStr As String
    Private Shared resultRows() As DataRow = Nothing
    Private Shared resultRow As DataRow
    Private Shared restoreSerial As String = ""

    Public Shared Property Mpan() As String
        Get
            Return _mpan
        End Get
        Set(ByVal value As String)
            _mpan = value
        End Set
    End Property

    Public Shared Property Serial() As String
        Get
            Return _serial
        End Get
        Set(ByVal value As String)
            _serial = value
        End Set
    End Property


    Public Shared Property Unit() As String
        Get
            Return _unit
        End Get
        Set(ByVal value As String)
            _unit = value
        End Set
    End Property

    Public Shared Property ReadingDate() As Date
        Get
            Return _readingDateStr
        End Get
        Set(ByVal value As Date)
            _readingDateStr = value
        End Set
    End Property


    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("Hdr")
        headers.Add("MpanHh") ' Contains MPAN and HH Slot
        headers.Add("Scu") ' Contains Serial/Consumption/Unit
        headers.Add("Interval/Date")
        headers.Add("StartPeriod")
        headers.Add("StartRead")
        headers.Add("TotalPeriods")
        headers.Add("Spare")

        Return headers

    End Function




    Public Shared Function ToDeFormat(ByRef dt As DataTable)
        Dim chunkSize As Integer = 50000 'To divide into multiple datatable to increase performance
        Dim ds As New DataSet
        Dim resultDt As DataTable = DeFormatTableTemplates.CreateHalfHourTable

        For i As Integer = 0 To dt.Rows.Count - 1 Step chunkSize

            ds.Tables.Add(dt.AsEnumerable.Skip(i).Take(chunkSize).CopyToDataTable())

        Next

        For Each table As DataTable In ds.Tables

            'Splitting datatable can give incompletely filled rows. Return the last row to complete it.

            resultDt.Merge(PivotHhSlots(table, resultRow))
            resultRow = resultDt.Rows(resultDt.Rows.Count - 1)
        Next


        dt = resultDt

        Return dt

    End Function


    Private Shared Function PivotHhSlots(ByRef dt As DataTable, ByRef resultRow As DataRow) As DataTable


        Dim dateHolder As Date = Today()
        Dim HhSlot As Integer
        Dim hHTable As New DataTable
        hHTable = DeFormatTableTemplates.CreateHalfHourTable()

        For Each row As DataRow In dt.Rows


            If row("Hdr") = "101" Then
                Mpan = row("MpanHh")
                Serial = row("Scu")
                restoreSerial = Serial
                Unit = ""
                ReadingDate = Nothing
            Else

            End If

            If row("Hdr") = "102" Then

                Dim success As Boolean = False
                Unit = row("Scu")
                If Unit.ToLower.Replace(" ", "") <> "kwh" Then
                    Serial = restoreSerial & "_" & Unit
                Else
                    Serial = restoreSerial
                End If

                ReadingDate = DateTime.ParseExact(row("Interval/Date"), "ddMMyyyy", CultureInfo.InvariantCulture)

                resultRows = hHTable.Select(" [Date(dd/mm/yyyy)] = '" & ReadingDate.ToString("dd/MM/yyyy HH:mm:ss") & "' AND [MeterSerial] = '" & _
                                                             Serial & "'")

                If resultRows.Count = 1 Then
                    resultRow = resultRows(0)
                    success = True
                End If


                If Not success Then
                    Dim newRow As DataRow = hHTable.NewRow
                    newRow("Date(dd/mm/yyyy)") = ReadingDate.Date

                    If Unit.ToLower.Replace(" ", "") = "kwh" Or Unit.ToLower.Replace(" ", "") = "kwh(export)" Then
                        newRow("ForcekWh") = "Yes"
                    Else
                        newRow("ForcekWh") = "No"
                    End If

                    newRow("Mpan") = Mpan

                    newRow("MeterSerial") = Serial

                    hHTable.Rows.Add(newRow)

                    resultRows = hHTable.Select(" [Date(dd/mm/yyyy)] = '" & ReadingDate.ToString("dd/MM/yyyy HH:mm:ss") & "' AND [MeterSerial] = '" & _
                                                             Serial & "'")
                    resultRow = resultRows(0)

                End If

            End If

            If row("Hdr") = "103" Then

                HhSlot = CInt(row("MpanHh")) - 1 'To fill the appropriate HH slot
                resultRow(dateHolder.AddMinutes(HhSlot * 30).ToString("HH:mm")) = row("Scu")

            End If


        Next

        Return hHTable

    End Function




End Class
