﻿Public Class WWLRealTimeSchema


    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("Header1")
        headers.Add("Header2")
        headers.Add("Header3")
        headers.Add("Header4")
        headers.Add("Header5")
        headers.Add("Header6")
        headers.Add("Header7")
        headers.Add("Header8")
        headers.Add("Header9")
        headers.Add("Header10")
        headers.Add("Header11")

        Return headers

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable


        Dim realTimeDt As DataTable
        realTimeDt = DeFormatTableTemplates.CreateRealTimeTable()

        PopulateColumns(dt, realTimeDt)

        Return dt

    End Function


    Private Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)


        Dim success As Boolean = False
        Dim theDate As DateTime
        Dim theTime As DateTime


        For Each row As DataRow In dt.Rows
            'Header 1 indicates if error has occured.
            If IsDate(row("Header3")) And row("Header1").ToString = "0" Then

                theDate = row("Header3")
                theTime = theDate.ToString("HH:mm")
                Dim theTimeString = theTime.ToString("HH:mm")

                For Each Rrow As DataRow In realTimeDt.Rows
                    If Rrow("MeterName") = dt.Rows(1)(4) And Rrow("DateTime") = theDate.Date.ToString("dd/MM/yyyy") Then
                        Rrow(theTimeString) = row("Header5")
                        success = True
                    End If
                Next

                If Not success Then
                    Dim realTimeDtRow As DataRow = realTimeDt.NewRow

                    realTimeDtRow("MeterName") = dt.Rows(1)(4)
                    realTimeDtRow("DateTime") = CDate(row("Header3")).ToString("dd/MM/yyyy HH:mm:ss")
                    realTimeDtRow("CountUnits") = row("Header5")

                    realTimeDt.Rows.Add(realTimeDtRow)
                End If


                success = False

            End If
        Next

        dt = realTimeDt

    End Sub

End Class
