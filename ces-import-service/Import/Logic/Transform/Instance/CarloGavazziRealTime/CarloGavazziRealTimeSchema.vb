﻿Imports System.Globalization

Public Class CarloGavazziRealTimeSchema
    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        DataTableTools.RemoveBlankRows(dt)
        Dim realTimeDt As DataTable = DeFormatTableTemplates.CreateRealTimeTable()
        PopulateColumns(dt, realTimeDt)
        Return dt

    End Function

    Public Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)
        For Each row As DataRow In dt.Rows
            'Convert American Date form to English format
            Dim DateTimeValue As DateTime = GetUKFormatDateTime(Convert.ToString(row("Date")) + " " + Convert.ToString(row("Hour")))
            If IsDate(DateTimeValue) Then
                Dim realTimeDtRow As DataRow = realTimeDt.NewRow

                realTimeDtRow("MeterName") = row("Object Name")
                realTimeDtRow("DateTime") = DateTimeValue.ToString("dd/MM/yyyy HH:mm")
                realTimeDtRow("CountUnits") = GetCountUnits(row("kWh AC [kWh]"))


                realTimeDt.Rows.Add(realTimeDtRow)
            End If
        Next
        dt = realTimeDt
    End Sub
    ''' <summary>
    '''    'Convert American Date form to English format
    ''' </summary>
    ''' <param name="strInputDateTimeValue"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function GetUKFormatDateTime(ByVal strInputDateTimeValue As String) As Date
        Return Date.ParseExact((strInputDateTimeValue), "MM/dd/yyyy HH:mm:ss", CultureInfo.GetCultureInfo("en-GB"))
    End Function
    ''' <summary>
    ''' Get value for count units
    ''' </summary>
    ''' <param name="strCountUnit"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function GetCountUnits(ByVal strCountUnit As String) As Double
        Dim CountUnits As Double = 0.0
        If (String.IsNullOrEmpty(strCountUnit)) Then
            Return CountUnits
        Else
            CountUnits = Math.Round(CDbl(strCountUnit), 2)
        End If
        Return CountUnits
    End Function

End Class
