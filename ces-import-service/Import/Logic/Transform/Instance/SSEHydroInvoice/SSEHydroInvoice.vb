﻿Public Class SSEHydroInvoice
    Implements ITransformDelegate
    Private _returnTable As DataTable
    Dim excel As New Excel()


    'Public Function Invoke(ByVal stagingFilePath As String, ByRef returnTable As System.Data.DataTable) As Boolean Implements ITransformDelegate.Invoke
    '    Try
    '        Dim excel As New DeCommon.Excel()

    '        excel.Load(stagingFilePath)
    '        Dim xSheet As ExcelSheet = excel.Sheets(0)

    '        If Not excel.GetSheet("Sheet1", xSheet) = True Then

    '            Return False

    '        End If

    '        xSheet.HeaderRowIndex = 1

    '        returnTable = xSheet.GetDataTable()

    '        SSEEbillingInvoiceSchema.GetTransformedHeaders(returnTable)
    '        SSEEbillingInvoiceSchema.ToDeFormat(returnTable)

    '        returnTable.TableName = "Invoice"

    '        Return True

    '    Catch ex As Exception
    '        Return False
    '    End Try

    'End Function

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        excel.Load(fullPath)


        Dim xSheet As ExcelSheet = excel.Sheets(0)
        excel.GetSheet("Sheet1", xSheet)
        xSheet.HeaderRowIndex = 1

        _returnTable = xSheet.GetDataTable()


    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property


    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        SSEHydroInvoiceSchema.GetTransformedHeaders(_returnTable)
        SSEHydroInvoiceSchema.ToDeFormat(_returnTable)
        For Each row As DataRow In _returnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next


    End Sub
End Class

