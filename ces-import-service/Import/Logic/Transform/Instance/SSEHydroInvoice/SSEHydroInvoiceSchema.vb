﻿Public Class SSEHydroInvoiceSchema

    Public Shared _ExternalIdCrac As String
    Public Shared _meterSerial As String
    Public Shared _mpan As String

    Public Shared Property ExternalIdCrac() As String

        Get
            Return _ExternalIdCrac
        End Get
        Set(value As String)
            _ExternalIdCrac = value
        End Set
    End Property

    Public Shared Property MeterSerial() As String

        Get
            Return _meterSerial
        End Get
        Set(value As String)
            _meterSerial = value
        End Set
    End Property

    Public Shared Property MPAN() As String

        Get
            Return _mpan
        End Get
        Set(value As String)
            _mpan = value
        End Set
    End Property

    Public Shared ListCostOnlyNo As New List(Of String)(New String() {"Day units", "Night units", "Unrestricted units"})




    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        Dim invoiceTbl As DataTable = DeFormatTableTemplates.CreateInvoiceTable

        For Each row As DataRow In dt.Rows

            PopulateTable(invoiceTbl, row)

        Next

        RenameColumns(invoiceTbl)
        dt = invoiceTbl


    End Sub

    Private Shared Sub PopulateTable(ByRef invoiceTbl As DataTable, ByVal row As DataRow)
        Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow

        Dim Vat, net As Double

        newInvoiceTblRow("Invoice Number") = row("INVOICE_NO")

        newInvoiceTblRow("Meter Serial") = row("METER")

        newInvoiceTblRow("MPAN") = row("MPAN")

        newInvoiceTblRow("Start Date") = row("BILL_START_DT")

        newInvoiceTblRow("End Date") = row("BILL_END_DT")

        newInvoiceTblRow("Start Read") = row("PREV_READ_VAL_NHH")
       
        newInvoiceTblRow("Start Read Estimated") = row("PREV_READ_TYPE_NHH")

        newInvoiceTblRow("End Read") = row("CURR_READ_VAL_NHH")

        newInvoiceTblRow("End Read Estimated") = row("CURR_READ_TYPE_NHH")

        newInvoiceTblRow("Consumption") = row("USAGE")

        newInvoiceTblRow("Force Kwh") = "Yes"

        net = IIf(String.IsNullOrEmpty(row("AMOUNT")), 0.0, row("AMOUNT"))

        Vat = IIf(String.IsNullOrEmpty(row("Vat")), 0.0, row("Vat"))

        newInvoiceTblRow("Net") = net

        If row("BILL_CHRG_DESC") = "VAT" Then

            newInvoiceTblRow("Vat") = Vat
            newInvoiceTblRow("Net") = 0
            newInvoiceTblRow("Gross") = 0 + Vat

        Else

            newInvoiceTblRow("Net") = net
            newInvoiceTblRow("Vat") = 0
            newInvoiceTblRow("Gross") = net + 0
        End If

        newInvoiceTblRow("Description") = row("BILL_CHRG_DESC")

        newInvoiceTblRow("Cost Only") = IsCostOnly(row("BILL_CHRG_DESC"))

        If newInvoiceTblRow("Cost Only") = "No" Then

            newInvoiceTblRow("Rate") = row("BILL_CHRG_DESC")

        End If

        newInvoiceTblRow("TaxPointDate") = row("SENT_DT")

        newInvoiceTblRow("IsLastDayApportion") = "No"

        invoiceTbl.Rows.Add(newInvoiceTblRow)


    End Sub

    Public Shared Sub GetTransformedHeaders(ByRef dt As DataTable)

        PopulateColumns(dt)
        PopulateMeterSerial(dt)

    End Sub

    Public Shared Sub PopulateColumns(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows

            PopulateReadEstimation(row)

        Next

    End Sub


    Public Shared Function IsCostOnly(value As String)

        If ListCostOnlyNo.Contains(value) Then

            Return "No"

        Else
            Return "Yes"

        End If


    End Function

    Private Shared Sub PopulateReadEstimation(ByVal row As DataRow)
        Dim ListActual As New List(Of Char)(New Char() {"a", "c", "f", "i", "m", "p", "r", "s", "b"})
        Dim ListEstimated As New List(Of Char)(New Char() {"d", "e"})

        ' Start Read Estimated
        If ListActual.Contains(row("PREV_READ_TYPE_NHH").ToString.ToLower) Then
            row("PREV_READ_TYPE_NHH") = "A"
        ElseIf ListEstimated.Contains(row("PREV_READ_TYPE_NHH").ToString.ToLower) Then
            row("PREV_READ_TYPE_NHH") = "E"
        Else
            row("PREV_READ_TYPE_NHH") = "A"
        End If

        ' End Read Estimated

        If ListActual.Contains(row("CURR_READ_TYPE_NHH").ToString.ToLower) Then
            row("CURR_READ_TYPE_NHH") = "A"
        ElseIf ListEstimated.Contains(row("CURR_READ_TYPE_NHH").ToString.ToLower) Then
            row("CURR_READ_TYPE_NHH") = "E"
        Else
            row("CURR_READ_TYPE_NHH") = "A"
        End If

    End Sub

    Private Shared Sub PopulateMeterSerial(ByRef dt As DataTable)

        'for blank meterserials and mpan

        For Each row As DataRow In dt.Rows

            If row("METER").trim = "" Or row("MPAN").trim = "" Then

                Dim resultrows() = dt.Select("[METER] <> '""' and [MPAN]<>'0' and [EXTERNAL_SA_ID]='" & row("EXTERNAL_SA_ID") & "' ")

                If resultrows.Length > 0 Then


                    MeterSerial = resultrows(0)("METER")
                    MPAN = resultrows(0)("MPAN")
                    ExternalIdCrac = resultrows(0)("EXTERNAL_SA_ID")



                End If

                If row("EXTERNAL_SA_ID") = ExternalIdCrac Then

                    row("METER") = MeterSerial
                    row("MPAN") = MPAN

                End If

            End If
        Next

    End Sub

    Private Shared Sub RenameColumns(InvoiceTbl As DataTable)
        InvoiceTbl.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        InvoiceTbl.Columns("Meter Serial").ColumnName = "MeterSerial"
        InvoiceTbl.Columns("Rate").ColumnName = "Tariff"
        InvoiceTbl.Columns("Start Date").ColumnName = "StartDate"
        InvoiceTbl.Columns("End Date").ColumnName = "EndDate"
        InvoiceTbl.Columns("Start Read").ColumnName = "StartRead"
        InvoiceTbl.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        InvoiceTbl.Columns("End Read").ColumnName = "EndRead"
        InvoiceTbl.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        InvoiceTbl.Columns("Force kWh").ColumnName = "ForcekWh"
        InvoiceTbl.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub



End Class
