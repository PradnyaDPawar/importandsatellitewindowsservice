﻿Partial Public Class InencoHalfHourSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        RenameColumns(dt)
        AddColumns(dt)
        PopulateForcekWh(dt)

        Return dt
    End Function

    Public Shared Sub RenameColumns(ByRef dt As DataTable)

        Dim dateholder As Date = Today()

        dt.Columns("Date").ColumnName = "Date(dd/MM/yyyy)"
        dt.Columns("Core").ColumnName = "MPAN"

        For i As Integer = 1 To 48

            dt.Columns(i.ToString).ColumnName = dateholder.ToString("HH:mm")
            dateholder = dateholder.AddMinutes(30)

        Next

    End Sub

    Public Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("MeterSerial")

    End Sub

    Public Shared Sub PopulateForcekWh(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows

            If row("Data Type").ToString.ToLower = "kwh" Then

                row("ForcekWh") = "Yes"
            Else
                row("ForcekWh") = "No"

            End If

        Next

    End Sub


End Class
