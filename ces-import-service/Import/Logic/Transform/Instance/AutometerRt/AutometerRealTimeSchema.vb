﻿Public Class AutometerRealTimeSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("MeterName")
        headers.Add("DateTime")
        headers.Add("CountUnits")
        headers.Add("Spare1")
        headers.Add("Spare2")
        Return headers

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

        Dim realTimeDt As DataTable
        realTimeDt = DeFormatTableTemplates.CreateRealTimeTable()
        dt.Rows.RemoveAt(2)
        PopulateColumns(dt, realTimeDt)

        Return dt

    End Function

    Private Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)

        Dim success As Boolean = False
        Dim theDate As String
        Dim theTime As DateTime
        Dim metername As String


        For Each row As DataRow In dt.Rows

            If Not String.IsNullOrEmpty(row("DateTime")) Then

                theDate = row("DateTime")
                theTime = theDate.ToString.Substring(11, 6)
                metername = dt.Rows(0)(0).ToString.Substring(8, 23)

                For Each Rrow As DataRow In realTimeDt.Rows

                    If Rrow("MeterName") = dt.Rows(0)(0) And Rrow("DateTime") = theDate.ToString.Substring(11, 6) Then

                        Rrow(theTime) = row("CountUnits")
                        success = True

                    End If

                Next


                If Not success Then
                    Dim realTimeDtRow As DataRow = realTimeDt.NewRow

                    realTimeDtRow("MeterName") = dt.Rows(0)(0).ToString.Substring(8, 23)
                    realTimeDtRow("DateTime") = CDate(row("DateTime")).ToString("dd/MM/yyyy HH:mm")
                    realTimeDtRow("CountUnits") = row("CountUnits")

                    realTimeDt.Rows.Add(realTimeDtRow)
                End If


                success = False

            End If
        Next

        dt = realTimeDt

    End Sub

End Class
