﻿Public Class Wi5RealTime

    Implements ITransformDelegate

    Private _returnTable As DataTable


    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property


    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvFile As New DelimitedFile(fullPath, 0)
        csvFile.Load(_returnTable, Wi5RealTimeSchema.GetHeaders)
        'Dim excel As New Excel()
        'Dim xSheet As ExcelSheet
        'excel.Load(fullPath)
        'xSheet = excel.Sheets(0)
        'xSheet.SetHeaderRow(Wi5RealTimeSchema.GetHeaders(), ExcelSearchEnum.ContainsAll)
        ''xSheet.HeaderRowIndex = False
        '_returnTable = xSheet.GetDataTable()

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        Wi5RealTimeSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_RealTime.Fill(row, dsEdits, importEdit)
        Next


    End Sub


End Class
