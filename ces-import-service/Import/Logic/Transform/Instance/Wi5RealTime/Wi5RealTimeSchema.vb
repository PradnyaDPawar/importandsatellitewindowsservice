﻿Imports System.Globalization

Public Class Wi5RealTimeSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("RawData")

        Return headers

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

        Dim realTimeDt As DataTable
        realTimeDt = DeFormatTableTemplates.CreateRealTimeTable()

        PopulateColumns(dt, realTimeDt)

        Return dt

    End Function
    Private Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)

        Dim data, metername, count As String
        Dim thedate As String

        For Each row As DataRow In dt.Rows

            data = row("RawData")
            metername = data.Substring(16, 6)
            thedate = data.Substring(0, 14)
            count = data.Substring(24, 6)

            Dim realtimeDtrow As DataRow = realTimeDt.NewRow

            realtimeDtrow("MeterName") = Convert.ToInt32(metername, 16)
            realtimeDtrow("DateTime") = DateTime.ParseExact(thedate, "yyyyMMddHHmmss", CultureInfo.InvariantCulture)
            realtimeDtrow("CountUnits") = Convert.ToInt32(count, 16)
            realTimeDt.Rows.Add(realtimeDtrow)

        Next

        dt = realTimeDt

    End Sub

End Class
