﻿Partial Public Class BritishGasHalfHour


    Private Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("MPAN")
        headers.Add("Settlement Date")
        headers.Add("Maximum Demand")
        headers.Add("Daily Consumption")
        headers.Add("00:00:00")
        headers.Add("00:30:00")
        headers.Add("01:00:00")
        headers.Add("01:30:00")
        headers.Add("02:00:00")
        headers.Add("02:30:00")
        headers.Add("03:00:00")
        headers.Add("03:30:00")
        headers.Add("04:00:00")
        headers.Add("04:30:00")
        headers.Add("05:00:00")
        headers.Add("05:30:00")
        headers.Add("06:00:00")
        headers.Add("06:30:00")
        headers.Add("07:00:00")
        headers.Add("07:30:00")
        headers.Add("08:00:00")
        headers.Add("08:30:00")
        headers.Add("09:00:00")
        headers.Add("09:30:00")
        headers.Add("10:00:00")
        headers.Add("10:30:00")
        headers.Add("11:00:00")
        headers.Add("11:30:00")
        headers.Add("12:00:00")
        headers.Add("12:30:00")
        headers.Add("13:00:00")
        headers.Add("13:30:00")
        headers.Add("14:00:00")
        headers.Add("14:30:00")
        headers.Add("15:00:00")
        headers.Add("15:30:00")
        headers.Add("16:00:00")
        headers.Add("16:30:00")
        headers.Add("17:00:00")
        headers.Add("17:30:00")
        headers.Add("18:00:00")
        headers.Add("18:30:00")
        headers.Add("19:00:00")
        headers.Add("19:30:00")
        headers.Add("20:00:00")
        headers.Add("20:30:00")
        headers.Add("21:00:00")
        headers.Add("21:30:00")
        headers.Add("22:00:00")
        headers.Add("22:30:00")
        headers.Add("23:00:00")
        headers.Add("23:30:00")
        Return headers
    End Function

#Region " To DeFormat "

    Public Sub FormatColumns()
        CreateColumns()
        RenameColumns()
    End Sub

    Public Sub FormatRow(ByRef row As DataRow)
        PopulateForceKwh(row)
        FormatDateColumn(row)
    End Sub


    Public Sub ColumnValidate()
        Dim rawHeaders As List(Of String) = GetHeaders()

        For Each header As String In rawHeaders
            If Not _returnTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next
    End Sub

#End Region




    Private Sub CreateColumns()
        _returnTable.Columns.Add("MeterSerial")
        _returnTable.Columns.Add("ForcekWh")
    End Sub

    Private Sub RenameColumns()
        Dim halfHourEntry As DateTime
        _returnTable.Columns("Settlement Date").ColumnName = "Date(dd/mm/yyyy)"

        'Rename HH entries
        For Each column As DataColumn In _returnTable.Columns
            If DateTime.TryParse(column.ColumnName, halfHourEntry) Then
                column.ColumnName = halfHourEntry.ToString("HH:mm")
            End If
        Next
    End Sub

    Private Sub PopulateForceKwh(ByRef row As DataRow)
        row("ForcekWh") = "Yes"
    End Sub

    Private Sub FormatDateColumn(ByRef row As DataRow)
        Dim dateEntry As DateTime
        If DateTime.TryParse(row("Date(dd/mm/yyyy)"), dateEntry) Then
            row("Date(dd/mm/yyyy)") = dateEntry.ToString("d")
        End If
    End Sub







 

 


End Class
