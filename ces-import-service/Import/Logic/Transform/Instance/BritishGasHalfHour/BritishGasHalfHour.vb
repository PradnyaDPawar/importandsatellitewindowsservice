﻿Partial Public Class BritishGasHalfHour
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Private Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel
        Dim xSheet As ExcelSheet
        excel.Load(fullPath)

        xSheet = excel.Sheets(0)

        If excel.GetSheet("Data", xSheet) Then
            xSheet.HeaderRowIndex = 1
            _returnTable = xSheet.GetDataTable()

        Else
            Throw New ApplicationException("Unable to find the 'Data' sheet.")
        End If

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        ColumnValidate()

        FormatColumns()

        For Each row As DataRow In _returnTable.Rows
            FormatRow(row)

            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next

    End Sub







End Class
