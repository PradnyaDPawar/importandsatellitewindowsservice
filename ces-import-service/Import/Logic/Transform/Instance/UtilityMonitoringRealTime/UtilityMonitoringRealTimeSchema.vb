﻿Partial Public Class UtilityMonitoringRealTime

    Public Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("bat")
        headers.Add("db")
        headers.Add("metername")
        headers.Add("datetime")
        headers.Add("countunits")

        Return headers

    End Function

    Public Function GetReducedHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("metername")
        headers.Add("datetime")
        headers.Add("countunits")

        Return headers

    End Function


    Public Function Validate() As Boolean
        If _returnTable.Columns.Count = 3 Then
            Return ValidateHeaders(GetReducedHeaders())
        Else
            Return ValidateHeaders(GetHeaders())
        End If
    End Function

    Private Function ValidateHeaders(ByRef headers As List(Of String)) As Boolean
        For Each header As String In headers
            If Not _returnTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next
    End Function


    Public Sub RenameColumns()
        If _returnTable.Columns.Contains("timestamp") Then
            _returnTable.Columns("timestamp").ColumnName = "datetime"
        End If
        If _returnTable.Columns.Contains("value") Then
            _returnTable.Columns("value").ColumnName = "countunits"
        End If
    End Sub

    'Private Function ValidateFull(ByRef srcTable As DataTable) As Boolean

    '    For Each header As String In GetHeaders()

    '        If Not srcTable.Columns.Contains(header) Then

    '            Return False

    '        End If

    '    Next

    'End Function




End Class
