﻿'Also know as digitalenergyrealtime csv
Public Class UtilityMonitoringRealTime
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property


    Public Sub LoadData(ByVal stagingFilePath As String) Implements ITransformDelegate.LoadData

        'load csv file
        Dim csvFile As New DelimitedFile(stagingFilePath, 0)
        csvFile.FieldsAreQuoted = True
        csvFile.Load(_returnTable)

    End Sub


    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat


        'rename and validation
        RenameColumns()
        'validate the new table
        Validate()



        For Each row As DataRow In _returnTable.Rows
            FE_RealTime.Fill(row, dsEdits, importEdit)
        Next

    End Sub




End Class
