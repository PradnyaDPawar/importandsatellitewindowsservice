﻿Partial Public Class DigitalEnergyStaticVariable


    Private Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("StaticVariableName")
        headers.Add("Level")
        headers.Add("Building_MeterSerial")
        headers.Add("Value")
        headers.Add("Unit")
        headers.Add("KwhUnit")
        Return headers
    End Function


    Private Sub ColumnValidate()
        Dim rawHeaders As List(Of String) = GetHeaders()

        For Each header As String In rawHeaders
            If Not _returnTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next
    End Sub

    Private Sub RenameColumns()
        
        For Each column As DataColumn In _returnTable.Columns
            column.ColumnName = column.ColumnName.Replace(" ", "")

            'set alias
            If column.ColumnName.ToLower = "applytobuildingormeter?" Then
                column.ColumnName = "Level"
            End If
            If column.ColumnName.ToLower = "kwh/unit" Then
                column.ColumnName = "KwhUnit"
            End If

            If column.ColumnName.ToLower = "buildingname/meterserial" Then
                column.ColumnName = "Building_MeterSerial"
            End If
            

        Next
    End Sub

End Class
