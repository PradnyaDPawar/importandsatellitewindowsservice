﻿Partial Public Class CoronaInvoice3
    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("Customer Ref")
        headers.Add("Our Ref")
        headers.Add("Customer Name")
        headers.Add("Invoice Date")
        headers.Add("Payment Type")
        headers.Add("Invoice Due Date")
        headers.Add("Invoice Number")
        headers.Add("Your Ref")
        headers.Add("Supply Address")
        headers.Add("Supply Postcode")
        headers.Add("MPR")
        headers.Add("Serial No")
        headers.Add("AQ")
        headers.Add("Open Date")
        headers.Add("Open Read")
        headers.Add("Open Type")
        headers.Add("Close Date")
        headers.Add("Close Read")
        headers.Add("Close Type")
        headers.Add("Units")
        headers.Add("Correction Factor")
        headers.Add("Reading Factor")
        headers.Add("Calorific Value")
        headers.Add("kWh per meter")
        headers.Add("Total kWh")
        headers.Add("Gas Price")
        headers.Add("Daily Charge")
        headers.Add("Days")
        headers.Add("Net-Gas Cost Per Meter")
        headers.Add("Vol Subject CCL")
        headers.Add("CCL Relief %")
        headers.Add("CCL Rate")
        headers.Add("CCL per Meter")
        headers.Add("Total CCL")
        headers.Add("AMR Amount")
        headers.Add("Total Net Gas Cost")
        headers.Add("% at Commercial VAT")
        headers.Add("% at Non-Commercial VAT")
        headers.Add("VAT on CCL per meter")
        headers.Add("AMR Commercial VAT")
        headers.Add("AMR Non-Commercial VAT")
        headers.Add("Commercial VAT")
        headers.Add("Non-Commercial VAT")
        headers.Add("VAT")
        headers.Add("Net")
        headers.Add("VAT")
        headers.Add("Gross")

        Return headers

    End Function

    Public Sub Validate()
        For Each header As String In GetHeaders()
            If Not _returnTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next

    End Sub

    Public Sub GetTransformedHeaders()
        RenameColumns(_returnTable)
        CreateColumns(_returnTable)
        PopulateColumns(_returnTable)
    End Sub

    Private Sub RenameColumns(ByRef dt As DataTable)
        dt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        dt.Columns("Serial No").ColumnName = "MeterSerial"
        dt.Columns("MPR").ColumnName = "MPAN"
        dt.Columns("Open Date").ColumnName = "StartDate"
        dt.Columns("Close Date").ColumnName = "EndDate"
        dt.Columns("Open Read").ColumnName = "StartRead"
        dt.Columns("Open Type").ColumnName = "StartReadEstimated"
        dt.Columns("Close Read").ColumnName = "EndRead"
        dt.Columns("Close Type").ColumnName = "EndReadEstimated"
        dt.Columns("kWh per meter").ColumnName = "Consumption"
        dt.Columns("Invoice Date").ColumnName = "TaxPointDate"

        'editing
        dt.Columns("Gross").ColumnName = "OriginalGross"
        dt.Columns("Net").ColumnName = "OriginalNet"
        dt.Columns("VAT").ColumnName = "OriginalVAT"
    End Sub
    Private Sub CreateColumns(ByRef dt As DataTable)
        'dt.Columns.Add("Rate")
        dt.Columns.Add("Tariff")
        dt.Columns.Add("CostOnly")
        dt.Columns.Add("Description")
        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("InvoiceRate")
        dt.Columns.Add("CostUnit")
        dt.Columns.Add("Net")
        dt.Columns.Add("VAT")
        dt.Columns.Add("Gross")
        dt.Columns.Add("IsLastDayApportion")
        dt.Columns.Add("IsTransmissionTariff")
    End Sub

    Private Sub PopulateColumns(ByRef dt As DataTable)
        For Each row As DataRow In dt.Rows

            PopulateRate(row)
            PopulateCostOnly(row)
            PopulateReadEstimation(row)
            'Commented as logic not reuired to add a day to close end date
            'PopulateStartEndDates(row)
            PopulateForceKwh(row)
            PopulateIsLastDayApportion(row)

        Next

        CreateInvoiceEntries(dt)
    End Sub

    Private Sub PopulateRate(row As DataRow)
        'row("Rate") = "Gas"
        row("Tariff") = "Gas"

    End Sub

    Private Sub PopulateCostOnly(row As DataRow)
        row("CostOnly") = "No"
    End Sub
    Private Sub PopulateIsLastDayApportion(row As DataRow)
        row("IsLastDayApportion") = "No"
    End Sub

    Private Sub PopulateReadEstimation(row As DataRow)
        Dim ListActual As New List(Of Char)(New Char() {"a", "r", "n", "c"})
        Dim ListEstimated As New List(Of Char)(New Char() {"c", "d", "f", "e", "i", "m", "p", "r", "s"})

        ' Start Read Estimated
        If ListActual.Contains(row("StartReadEstimated").ToString.ToLower) Then
            row("StartReadEstimated") = "A"
        ElseIf ListEstimated.Contains(row("StartReadEstimated").ToString.ToLower) Then
            row("StartReadEstimated") = "E"
        End If

        ' End Read Estimated
        If ListActual.Contains(row("EndReadEstimated").ToString.ToLower) Then
            row("EndReadEstimated") = "A"
        ElseIf ListEstimated.Contains(row("EndReadEstimated").ToString.ToLower) Then
            row("EndReadEstimated") = "E"
        End If
    End Sub

    Private Sub PopulateStartEndDates(row As DataRow)
        Dim currentdate, newdate As DateTime
        currentdate = row("EndDate")
        newdate = currentdate.AddDays(1)
        row("EndDate") = newdate.ToShortDateString
    End Sub

    Private Sub PopulateForceKwh(row As DataRow)
        row("ForcekWh") = "Yes"
    End Sub

    Private Sub CreateInvoiceEntries(dt As DataTable)
        Dim gross, net, vatcost As Double
        Dim isDuplicate As Boolean = False

        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            ' Add Cost only Yes Lines
            AddDailyChargeCostOnly(row, dt)
            AddCclCostOnly(row, dt)
            isDuplicate = checkDuplicateAMR(row, dt)
            'Check if AMR is duplicate
            If Not isDuplicate Then
                AddAmrCostOnly(row, dt)
            End If

            AddVatCostOnly(row, dt)

            'Consumption and associated charge
            net = row("Net-Gas Cost Per Meter")

            'If row("% at Commercial VAT") = "100" Then
            '    vatcost = net * GetVatRate(row("StartDate"), row("EndDate"))
            'Else
            '    vatcost = net * Vat5Percent
            'End If
            vatcost = 0

            gross = net + vatcost

            row("Net") = net.ToString("0.00")
            row("VAT") = vatcost.ToString("0.00")
            row("Gross") = gross.ToString("0.00")
            row("Description") = "Gas Charge"

        Next
    End Sub

    Private Shared ReadOnly Property Vat5Percent()
        Get
            Vat5Percent = 0.05
            Return Vat5Percent
        End Get

    End Property

    Private Shared Sub AddDailyChargeCostOnly(ByRef row As DataRow, ByRef dt As DataTable)

        Dim gross, net, vatcost As Double

        dt.ImportRow(row)
        net = dt.Rows(dt.Rows.Count - 1)("Daily Charge Per Line")
        'If row("% at Commercial VAT") = "100" Then
        '    vatcost = net * GetVatRate(row("StartDate"), row("EndDate"))
        'Else
        '    vatcost = net * Vat5Percent
        'End If
        vatcost = 0
        gross = net + vatcost

        dt.Rows(dt.Rows.Count - 1)("Net") = net.ToString("0.00")
        dt.Rows(dt.Rows.Count - 1)("Vat") = vatcost.ToString("0.00")
        dt.Rows(dt.Rows.Count - 1)("CostOnly") = "Yes"
        dt.Rows(dt.Rows.Count - 1)("Gross") = gross.ToString("0.00")
        dt.Rows(dt.Rows.Count - 1)("Description") = "Daily Charge"
        dt.Rows(dt.Rows.Count - 1)("Consumption") = ""
        dt.Rows(dt.Rows.Count - 1)("StartRead") = ""
        dt.Rows(dt.Rows.Count - 1)("StartReadEstimated") = ""
        dt.Rows(dt.Rows.Count - 1)("EndRead") = ""
        dt.Rows(dt.Rows.Count - 1)("EndReadEstimated") = ""



    End Sub

    Private Shared Sub AddCclCostOnly(ByRef row As DataRow, ByRef dt As DataTable)

        Dim gross, net, vat As Double

        dt.ImportRow(row)
        net = dt.Rows(dt.Rows.Count - 1)("Total CCL")
        vat = 0 'dt.Rows(dt.Rows.Count - 1)("VAT on CCL per meter")

        gross = net + vat

        dt.Rows(dt.Rows.Count - 1)("Net") = net.ToString("0.00")
        dt.Rows(dt.Rows.Count - 1)("Vat") = vat.ToString("0.00")
        dt.Rows(dt.Rows.Count - 1)("CostOnly") = "Yes"
        dt.Rows(dt.Rows.Count - 1)("Gross") = gross.ToString("0.00")
        dt.Rows(dt.Rows.Count - 1)("Description") = "CCL"
        dt.Rows(dt.Rows.Count - 1)("Consumption") = ""
        dt.Rows(dt.Rows.Count - 1)("Tariff") = ""
        dt.Rows(dt.Rows.Count - 1)("StartRead") = ""
        dt.Rows(dt.Rows.Count - 1)("StartReadEstimated") = ""
        dt.Rows(dt.Rows.Count - 1)("EndRead") = ""
        dt.Rows(dt.Rows.Count - 1)("EndReadEstimated") = ""


    End Sub


    Private Shared Sub AddAmrCostOnly(ByRef row As DataRow, ByRef dt As DataTable)

        Dim gross, net, commvat, noncomvat As Double

        dt.ImportRow(row)
        net = dt.Rows(dt.Rows.Count - 1)("AMR Amount")
        commvat = 0 'dt.Rows(dt.Rows.Count - 1)("AMR Commercial VAT")
        noncomvat = 0 'dt.Rows(dt.Rows.Count - 1)("AMR Non-Commercial VAT")

        gross = net + commvat + noncomvat

        dt.Rows(dt.Rows.Count - 1)("Net") = net.ToString("0.00")
        dt.Rows(dt.Rows.Count - 1)("Vat") = (commvat + noncomvat).ToString("0.00")
        dt.Rows(dt.Rows.Count - 1)("CostOnly") = "Yes"
        dt.Rows(dt.Rows.Count - 1)("Gross") = gross.ToString("0.00")
        dt.Rows(dt.Rows.Count - 1)("Description") = "AMR"
        dt.Rows(dt.Rows.Count - 1)("Consumption") = ""
        dt.Rows(dt.Rows.Count - 1)("Tariff") = ""
        dt.Rows(dt.Rows.Count - 1)("StartRead") = ""
        dt.Rows(dt.Rows.Count - 1)("StartReadEstimated") = ""
        dt.Rows(dt.Rows.Count - 1)("EndRead") = ""
        dt.Rows(dt.Rows.Count - 1)("EndReadEstimated") = ""


    End Sub

    Private Shared Sub AddVatCostOnly(ByRef row As DataRow, ByRef dt As DataTable)

        Dim gross, net, vat As Double

        dt.ImportRow(row)
        net = 0
        vat = dt.Rows(dt.Rows.Count - 1)("VAT1")

        gross = net + vat

        dt.Rows(dt.Rows.Count - 1)("Net") = net.ToString("0.00")
        dt.Rows(dt.Rows.Count - 1)("Vat") = (vat).ToString("0.00")
        dt.Rows(dt.Rows.Count - 1)("CostOnly") = "Yes"
        dt.Rows(dt.Rows.Count - 1)("Gross") = gross.ToString("0.00")
        dt.Rows(dt.Rows.Count - 1)("Description") = "VAT"
        dt.Rows(dt.Rows.Count - 1)("Consumption") = ""
        dt.Rows(dt.Rows.Count - 1)("Tariff") = ""
        dt.Rows(dt.Rows.Count - 1)("StartRead") = ""
        dt.Rows(dt.Rows.Count - 1)("StartReadEstimated") = ""
        dt.Rows(dt.Rows.Count - 1)("EndRead") = ""
        dt.Rows(dt.Rows.Count - 1)("EndReadEstimated") = ""


    End Sub

    Private Shared Function GetVatRate(ByVal startDate As Date, ByVal endDate As Date) As Double
        Dim vatRate As Double

        If Date.Compare(startDate, #11/30/2008#) < 0 Then
            vatRate = 0.175
        End If

        If startDate.Date >= #12/1/2008# And startDate.Date <= #1/1/2010# Then
            vatRate = 0.15
        End If

        If startDate.Date >= #1/2/2010# And startDate.Date <= #1/3/2011# Then
            vatRate = 0.175
        End If

        If startDate.Date >= #1/4/2011# Then
            vatRate = 0.2
        End If

        Return vatRate

    End Function
    ''' <summary>
    ''' Method to check if Amr Amount is duplicate/already exists for same Invoice Number/MeterSerial/MPAN
    ''' </summary>
    ''' <param name="row"></param>
    ''' <param name="dt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function checkDuplicateAMR(ByRef row As DataRow, ByRef dt As DataTable) As Boolean
        Dim isExists As Boolean = False
        'Set column name AMR Amount to filter datatable
        dt.Columns("AMR Amount").ColumnName = "AMRAmount"
        Dim dtable As DataTable = dt.Select("InvoiceNumber=" + row("InvoiceNumber") + "AND MeterSerial='" + row("MeterSerial") + "' AND MPAN='" + row("MPAN") + "'AND AMRAmount='" + row(row.Table.Columns("AMRAmount").Ordinal) + "'").CopyToDataTable()
        'Reset column name AMR Amount 
        dt.Columns("AMRAmount").ColumnName = "AMR Amount"
        'From filtered rows check if AMR exists in description column
        If dtable.Select("Description='AMR'").Length = 0 Then
            isExists = False
        Else
            isExists = True
        End If
        Return isExists
    End Function

End Class
