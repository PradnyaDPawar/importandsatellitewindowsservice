﻿Public Class CoronaInvoice3
    Implements ITransformDelegate

    Private _returnTable As DataTable


    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Try
            Dim excel As New Excel()
            Dim xSheet As ExcelSheet
            excel.Load(fullPath)

            xSheet = excel.Sheets(0)
            xSheet.SetHeaderRow(GetHeaders(), ExcelSearchEnum.ContainsAll)
            _returnTable = xSheet.GetDataTable()

            DeletePreHeaderRows(ReturnTable, xSheet)

        Catch ex As Exception
            ' Return False
        End Try

    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        Validate()
        GetTransformedHeaders()

        For Each row As DataRow In _returnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next

    End Sub

    Public Shared Sub DeletePreHeaderRows(ByRef dt As DataTable, ByRef xlsheet As ExcelSheet)

        For deleteCounter As Integer = 0 To xlsheet.HeaderRowIndex - 2 Step 1
            dt.Rows(deleteCounter).Delete()
        Next
        dt.AcceptChanges()
    End Sub

End Class
