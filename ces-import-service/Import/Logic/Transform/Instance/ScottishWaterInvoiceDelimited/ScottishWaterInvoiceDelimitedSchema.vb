﻿Public Class ScottishWaterInvoiceDelimitedSchema
    Private Shared _mpan As String
    Private Shared _invoiceNo As String
    Private Shared _serial As String
    Private Shared _startDate As String
    Private Shared _endDate As String



    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        DataTableTools.RemoveBlankColumns(dt)
        PrefixMainHeaders(dt, 0)
        DataTableTools.SetHeaders(dt, 2)
        DataTableTools.RemoveTopRows(dt, 1)
        DataTableTools.RemoveBlankRows(dt)

        Dim invoiceDt As DataTable = DeFormatTableTemplates.CreateInvoiceTable
        PopulateInvoiceDt(dt, invoiceDt)
        dt = invoiceDt

    End Sub

    Private Shared Sub PrefixMainHeaders(ByRef dt As DataTable, ByVal MainHeaderRowIndex As Integer)
        Dim mainHeader, mainHeader2 As String
        mainHeader2 = ""
        For Each column As DataColumn In dt.Columns
            mainHeader = dt.Rows(MainHeaderRowIndex)(column)

            If Not String.IsNullOrEmpty(mainHeader) Then
                dt.Rows(MainHeaderRowIndex + 1)(column) = (mainHeader & dt.Rows(MainHeaderRowIndex + 1)(column)).ToString.Replace(" ", "").Replace("Â", "")
                mainHeader2 = mainHeader
            ElseIf Not String.IsNullOrEmpty(mainHeader2) Then
                dt.Rows(MainHeaderRowIndex + 1)(column) = (mainHeader2 & dt.Rows(MainHeaderRowIndex + 1)(column)).ToString.Replace(" ", "").Replace("Â", "")
            End If
        Next
    End Sub

    Private Shared Sub PopulateInvoiceDt(ByRef dt As DataTable, ByRef invoiceDt As DataTable)

        For Each row As DataRow In dt.Rows

            _invoiceNo = row("Customer reference") & "-" & row("Bill No")
            _mpan = row("Water SPID")
            _serial = row("Meter serial no.")
            _startDate = row("From Date")
            _endDate = row("To Date")

            PopulateTariffs(row, invoiceDt, "FixedCharge-MeteredWater", "Yes")
            PopulateTariffs(row, invoiceDt, "VolumetricWaterCharge-Metered", "No")
            PopulateTariffs(row, invoiceDt, "FixedCharge-MeteredWasteWater", "Yes")
            PopulateTariffs(row, invoiceDt, "VolumetricWasteWaterCharge-Metered", "No")
            PopulateTariffs(row, invoiceDt, "PropertyDrainageCharge(p/£RV)", "Yes")
            PopulateTariffs(row, invoiceDt, "RoadsDrainageCharge(p/£RV)", "Yes")
            PopulateTariffs(row, invoiceDt, "NonDomesticMeteredWastewaterFixedCharge", "Yes")
            PopulateTariffs(row, invoiceDt, "NonDomesticMeteredWastewaterVolumeCharge", "No")
            PopulateTariffs(row, invoiceDt, "FixedCharge-Un-meteredWater", "Yes")
            PopulateTariffs(row, invoiceDt, "WaterRVCharge-Un-metered", "Yes")
            PopulateTariffs(row, invoiceDt, "FixedCharge-Un-meteredWasteWater", "Yes")
            PopulateTariffs(row, invoiceDt, "VolumetricWasteWaterCharge-Un-metered", "No")
            PopulateTariffs(row, invoiceDt, "SupplyContractDiscount(Water)", "Yes")
            PopulateTariffs(row, invoiceDt, "SupplyContractDiscount(Waste)", "Yes")
            PopulateTariffs(row, invoiceDt, "OtherDiscount(Water)", "Yes")
            PopulateTariffs(row, invoiceDt, "OtherDiscount(Waste)", "Yes")
            PopulateTariffs(row, invoiceDt, "SecondaryCharges", "Yes")
            PopulateTariffs(row, invoiceDt, "TapsandTroughs", "Yes")



        Next

        RenameColumns(invoiceDt)

    End Sub

    Private Shared Sub PopulateTariffs(ByRef row As DataRow, ByRef invoiceDt As DataTable, ByVal charge As String, ByVal costOnly As String)

        If row.Table.Columns.Contains(charge & "NetAmount") Then

            If Not String.IsNullOrEmpty(row(charge & "NetAmount")) And IsNumeric(row(charge & "NetAmount")) Then

                Dim newInvoiceDtRow As DataRow = invoiceDt.NewRow
                newInvoiceDtRow("Invoice Number") = _invoiceNo
                newInvoiceDtRow("Meter Serial") = _serial
                newInvoiceDtRow("Rate") = "Water"
                newInvoiceDtRow("Start Date") = _startDate
                newInvoiceDtRow("End Date") = _endDate
                newInvoiceDtRow("Force kWh") = "No"
                newInvoiceDtRow("Cost Only") = costOnly
                newInvoiceDtRow("Net") = row(charge & "NetAmount")
                newInvoiceDtRow("Gross") = row(charge & "TotalAmount")
                newInvoiceDtRow("Vat") = row(charge & "VATAmount")

                If _serial.Trim = "-" Then
                    newInvoiceDtRow("MPAN") = _mpan
                    newInvoiceDtRow("Meter Serial") = ""
                Else
                    newInvoiceDtRow("MPAN") = ""
                End If

                If costOnly = "No" Then
                    Dim consumption As Double = 0
                    For i As Integer = 1 To 5

                        If row.Table.Columns.Contains(charge & "Consumption" & i.ToString) Then
                            Dim cons As Double
                            If Double.TryParse(row(charge & "Consumption" & i.ToString), cons) Then
                                consumption = consumption + cons
                            End If

                        End If

                    Next
                    newInvoiceDtRow("Consumption") = consumption
                End If


                newInvoiceDtRow("Description") = charge
                newInvoiceDtRow("TaxPointDate") = row("Date posted")
                newInvoiceDtRow("IsLastDayApportion") = "No"
                invoiceDt.Rows.Add(newInvoiceDtRow)

            End If


        End If
    End Sub

    Private Shared Sub RenameColumns(returnTable As DataTable)
        returnTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        returnTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        returnTable.Columns("Start Date").ColumnName = "StartDate"
        returnTable.Columns("End Date").ColumnName = "EndDate"
        returnTable.Columns("Start Read").ColumnName = "StartRead"
        returnTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        returnTable.Columns("End Read").ColumnName = "EndRead"
        returnTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        returnTable.Columns("Force kWh").ColumnName = "ForceKwh"
        returnTable.Columns("Rate").ColumnName = "Tariff"
        returnTable.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub


End Class
