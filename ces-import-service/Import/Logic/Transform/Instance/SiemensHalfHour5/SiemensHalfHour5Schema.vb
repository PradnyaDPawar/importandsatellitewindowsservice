﻿Public Class Siemens5HalfHourSchema

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        DataTableTools.SetHeaders(dt, 1)

        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)

        SetData(dt)

    End Sub


    ''nikitah:refactored code
    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim map As New Dictionary(Of String, DataRow)
        Dim dateholder, datetimeHolder As String
        Dim time As DateTime
        For Each column As DataColumn In dt.Columns
            If column.ColumnName <> "Timestamp" Then

                For Each row As DataRow In dt.Rows

                    If Not String.IsNullOrEmpty(row("TimeStamp")) Then

                        datetimeHolder = row("TimeStamp")
                        time = datetimeHolder.ToString().Split(" ")(1)
                        If (time <> "00:00") Then
                            time = time.AddMinutes(-30)
                        Else
                            ''for timeslot 00:00 we use 23:30
                            time = time.AddHours(24).AddMinutes(-30)
                        End If
                        ''end
                        dateholder = CDate(datetimeHolder.ToString().Split(" ")(0)).ToString("dd/MM/yyyy")
                        Dim timestring = time.ToString("HH:mm")


                        Dim key As String = column.ColumnName & dateholder



                        If map.ContainsKey(key) Then
                            Dim theRow As DataRow = map(key)
                            theRow(timestring) = row(column)
                        Else
                            Dim newRow As DataRow = hhDataTable.NewRow

                            newRow("Date(dd/mm/yyyy)") = dateholder
                            newRow("MeterSerial") = column.ColumnName
                            newRow(timestring) = row(column)

                            hhDataTable.Rows.Add(newRow)
                            map.Add(key, newRow)
                        End If
                    End If
                Next
            End If


        Next
        dt = hhDataTable
    End Sub
    Public Shared Sub SetData(ByRef dt As DataTable)

        ''logic to rearrage 23:30 column data
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim row As DataRow = dt.Rows(i)
            If i <> dt.Rows.Count - 1 Then
                Dim nextRow As DataRow = dt(i + 1)

                row("23:30") = nextRow("23:30")
            Else
                row("23:30") = Nothing
            End If
        Next

    End Sub
End Class
