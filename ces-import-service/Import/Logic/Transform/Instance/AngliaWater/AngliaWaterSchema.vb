﻿Public Class AngliaWaterSchema

    Public _meterSerial As String

    Public Property GetMeterSerial() As String
        Get
            Return _meterSerial
        End Get
        Set(ByVal value As String)
            _meterSerial = value
        End Set
    End Property

    Public Shared Function ToDEFormat(ByRef tdt As DataTable) As DataTable

        DataTableTools.RemoveBlankColumns(tdt)
        DataTableTools.SetHeaders(tdt, 1)
        ToStandardDateHeaders(tdt)
        AngliaWaterSchema.Merge15minutes(tdt)
        AngliaWaterSchema.Remove15MinuteEntries(tdt)


        Dim objAngliaWaterSchema As New AngliaWaterSchema
        objAngliaWaterSchema.GetMeterSerial = tdt.Rows(2)("Site Name:")

        'Remove columns which are not required
        tdt.Columns.Remove("0 1")
        tdt.Columns.Remove("Site Name:")

        DataTableTools.RemoveBlankRows(tdt)
        AngliaWaterSchema.CreateHalfHourlyEntries(tdt, DeFormatTableTemplates.CreateHalfHourTable())

        For Each row As DataRow In tdt.Rows
            row("Force kWh") = "No"
            row("Meter Serial") = objAngliaWaterSchema.GetMeterSerial
        Next

        Return tdt

    End Function

    Private Shared Sub Merge15minutes(ByRef tdt As DataTable)
        Dim consumption1, consumption2 As Double
        Dim dateentry As DateTime
        Dim consumption As Double
        For Each column As DataColumn In tdt.Columns
            If IsDate(column.ColumnName) Then
                dateentry = column.ColumnName
                If dateentry.Minute = 0 Or dateentry.Minute = 30 Then
                    dateentry = dateentry.AddMinutes(15)

                    If (tdt.Columns.Contains(dateentry.ToString("dd/MM/yyyy HH:mm"))) Then
                        consumption1 = tdt.Rows(0)(dateentry.ToString("dd/MM/yyyy HH:mm"))
                        dateentry = dateentry.AddMinutes(15)

                        If (tdt.Columns.Contains(dateentry.ToString("dd/MM/yyyy HH:mm"))) Then
                            consumption2 = tdt.Rows(0)(dateentry.ToString("dd/MM/yyyy HH:mm"))
                            consumption = (consumption1 + consumption2) / 4 'Dividing to convert into litres from litres per hour
                            tdt.Rows(0)(column) = consumption.ToString("0.00")
                        Else
                            tdt.Rows(0)(column) = ""
                        End If

                    Else
                        tdt.Rows(0)(column) = ""
                    End If

                End If
            End If
        Next

    End Sub


    Private Shared Sub Remove15MinuteEntries(ByRef tdt As DataTable)
        Dim dateEntry As Date

        For colcount As Integer = tdt.Columns.Count - 1 To 0 Step -1
            If IsDate(tdt.Columns(colcount).ColumnName) Then
                dateEntry = tdt.Columns(colcount).ColumnName

                If dateEntry.Minute = 15 Or dateEntry.Minute = 45 Then
                    tdt.Columns.RemoveAt(colcount)
                End If

            End If

        Next
    End Sub


    Private Shared Sub CreateHalfHourlyEntries(ByRef tdt As DataTable, ByRef hhDataTable As DataTable)
        Dim dateholder As DateTime
        Dim success As Boolean = False
        For Each column As DataColumn In tdt.Columns

            If IsDate(column.ColumnName) Then
                dateholder = column.ColumnName
                For Each row As DataRow In hhDataTable.Rows
                    If row("Date (dd/mm/yyyy)") = dateholder.Date Then
                        row(dateholder.ToString("HH:mm")) = tdt.Rows(0)(column)
                        success = True
                    End If
                Next

                If Not success Then
                    Dim newRow As DataRow = hhDataTable.NewRow
                    newRow("Date (dd/mm/yyyy)") = dateholder.Date
                    newRow(dateholder.ToString("HH:mm")) = tdt.Rows(0)(column)
                    hhDataTable.Rows.Add(newRow)
                End If
            End If
            success = False
        Next

        tdt = hhDataTable

    End Sub

    Private Shared Sub ToStandardDateHeaders(ByRef tdt As DataTable)
        Dim dateholder As DateTime

        For Each column As DataColumn In tdt.Columns
            If IsDate(column.ColumnName) Then
                dateholder = column.ColumnName
                column.ColumnName = dateholder.ToString("dd/MM/yyyy HH:mm")
            End If
        Next

    End Sub


    ' Public Shared Function Validate(ByRef dt As DataTable) As Boolean
    'Dim validFlag As Boolean = True
    'Dim dateTimeEntries As List(Of DateTime) = GetDateTimeEntries(dt)
    '   For Each row As DataRow In dt.Rows
    '      If IsDate(row(0)) Then
    '         If Not dateTimeEntries.Contains(row(0)) Then
    '            validFlag = False
    '       End If
    '  End If

    'Next
    ' Return validFlag
    'End Function

    'Public Shared Function GetDateTimeEntries(ByRef dt As DataTable) As List(Of DateTime)

    'Dim dateTimeEntries As New List(Of DateTime)
    'Dim dateHolder, dateHolderChk As DateTime
    '   For Each row As DataRow In dt.Rows
    '      If IsDate(row(0)) Then
    '         If Not dateTimeEntries.Contains(row(0)) Then
    '            dateHolder = row(0)
    ''           dateHolderChk = row(0)
    '        While dateHolder.Date = dateHolderChk.Date
    '             dateTimeEntries.Add(dateHolder)
    '           dateHolder = dateHolder.AddMinutes(15)
    '      End While
    ' End If
    'End If
    'Next
    'Return dateTimeEntries
    'End Function

End Class


