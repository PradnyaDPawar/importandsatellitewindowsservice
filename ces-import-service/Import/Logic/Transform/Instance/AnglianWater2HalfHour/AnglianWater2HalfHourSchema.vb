﻿Partial Public Class AnglianWater2HalfHour

    Private _meterSerial As String


    Public Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("Date")
        headers.Add("Time")
        headers.Add("m3 / 15 min")
        headers.Add("litres / sec")

        Return headers

    End Function

    Public Sub ToDeFormat(ByRef dt As DataTable)

        dt.Columns("m3 / 15 min").ColumnName = "Consumption"
        _meterSerial = GetMeterSerial(dt)
        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)

        dt = hhDataTable

    End Sub

    Private Function GetMeterSerial(ByRef dt As DataTable) As String

        For Each row As DataRow In dt.Rows

            If row(0).ToString.ToLower.Contains("serial number") Then
                Return row(0).ToString.Split(":").ElementAt(1).Trim
            End If

        Next

        Return Nothing
    End Function

    Private Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim dateholder As DateTime
        Dim datetimeHolder As DateTime
        Dim success As Boolean = False
        For Each row As DataRow In dt.Rows

            If IsDate(row("Date")) Then
                dateholder = row("Date")

                datetimeHolder = row("Time")
                datetimeHolder = dateholder.AddHours(datetimeHolder.Hour).AddMinutes(datetimeHolder.Minute)

                If datetimeHolder.Minute = 15 Or datetimeHolder.Minute = 45 Then


                    For Each hhrow As DataRow In hhDataTable.Rows
                        If hhrow("Date(dd/mm/yyyy)") = dateholder.Date Then

                            Dim consumption1, consumption2 As Double
                            consumption1 = row("Consumption")
                            consumption2 = GetConsumptionByDateTime(datetimeHolder.AddMinutes(15), dt)
                            If consumption2 <> -1 Then
                                hhrow(datetimeHolder.AddMinutes(-15).ToString("HH:mm")) = (consumption1 + consumption2).ToString("0.000")
                            Else
                                hhrow(datetimeHolder.AddMinutes(-15).ToString("HH:mm")) = ""
                            End If

                            success = True
                        End If


                    Next

                    If Not success Then
                        Dim newRow As DataRow = hhDataTable.NewRow
                        newRow("Date(dd/mm/yyyy)") = dateholder.Date
                        newRow("MeterSerial") = _meterSerial
                        newRow("ForcekWh") = "No"
                        Dim consumption1, consumption2 As Double
                        consumption1 = row("Consumption")
                        consumption2 = GetConsumptionByDateTime(datetimeHolder.AddMinutes(15), dt)
                        If consumption2 <> -1 Then
                            newRow(datetimeHolder.AddMinutes(-15).ToString("HH:mm")) = (consumption1 + consumption2).ToString("0.000")
                        Else
                            newRow(datetimeHolder.AddMinutes(-15).ToString("HH:mm")) = ""
                        End If

                        hhDataTable.Rows.Add(newRow)
                    End If


                End If
            End If

            success = False
        Next

        dt = hhDataTable

    End Sub


    Public Function GetConsumptionByDateTime(ByVal timeToFind As DateTime, ByVal dt As DataTable)

        Dim dateHolder As Date
        Dim timeHolder As DateTime

        For Each row As DataRow In dt.Rows

            If IsDate(row("Date")) Then

                dateHolder = row("Date")
                timeHolder = row("Time")
                If timeHolder.TimeOfDay = timeToFind.TimeOfDay And dateHolder.Date = timeToFind.Date Then
                    Return row("Consumption")
                End If

            End If

        Next

        Return -1

    End Function

End Class
