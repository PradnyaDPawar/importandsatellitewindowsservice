﻿Imports System.Globalization

Public Class CarloGavazzi3RealTimeSchema

    Private Shared Function GetHeaders(ByVal columnCount As Integer) As List(Of String)
        Dim headers As New List(Of String)

        For index = 1 To columnCount
            headers.Add("Header" + index.ToString())
        Next
        Return headers
    End Function

    Public Shared Sub ToDeFormat(ByRef dtResult As DataTable)
        RenameColumns(dtResult)
        PopulateColumns(dtResult)
    End Sub

    Private Shared Sub RenameColumns(ByRef dtResult As DataTable) 
        For Each column As DataColumn In dtResult.Columns
            column.ColumnName = column.ColumnName.Replace("""", String.Empty).Trim()
        Next
    End Sub

    Private Shared Sub PopulateColumns(ByRef dt As DataTable)
        Dim realTimeDt As DataTable = DeFormatTableTemplates.CreateRealTimeTable()
        Dim convertedDateTime As DateTime = DateTime.Today
        Dim dblCounter1 As Double, dblCounter2 As Double, dblCounter3 As Double, dblCounter4 As Double
        For Each row As DataRow In dt.Rows 
            If Not IsDBNull(row("Object Name")) OrElse Not String.IsNullOrWhiteSpace(row("Object Name")) Then

                Dim newRow As DataRow = realTimeDt.NewRow

                convertedDateTime = GetUKFormatDateTime(Convert.ToString(row("Date")) + " " + Convert.ToString(row("Hour")))
               
                newRow("MeterName") = row("Object Name").ToString().Replace("""", String.Empty).Trim()
                newRow("DateTime") = convertedDateTime.ToString("dd/MM/yyyy HH:mm:ss")

                Double.TryParse(row("COUNTER 1"), dblCounter1)
                Double.TryParse(row("COUNTER 2"), dblCounter2)
                Double.TryParse(row("COUNTER 3"), dblCounter3)
                Double.TryParse(row("COUNTER 4"), dblCounter4)

                newRow("CountUnits") = dblCounter1 + dblCounter2 + dblCounter3 + dblCounter4
                realTimeDt.Rows.Add(newRow)
            End If
        Next

        dt = realTimeDt
    End Sub

    ''' <summary>
    '''    'Convert American Date form to English format
    ''' </summary>
    ''' <param name="strInputDateTimeValue"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function GetUKFormatDateTime(ByVal strInputDateTimeValue As String) As Date
        Return Date.ParseExact((strInputDateTimeValue), "MM/dd/yyyy HH:mm:ss", CultureInfo.GetCultureInfo("en-GB"))
    End Function
End Class