﻿Public Class AvonAndRescueHH

    Implements ITransformDelegate


    Private _returnTable As DataTable

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim csvFile As New DelimitedFile(fullPath, 0)
        csvFile.Load(_returnTable)


    End Sub


    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        AvonAndRescueHHSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            If row("units") = "cum" Then
                row("ForcekWh") = "No"
            Else
                row("ForcekWh") = "Yes"
            End If
            row("MeterSerial") = Nothing
            FE_HalfHour.Fill(row, dsEdits, importEdit)

        Next

    End Sub
End Class


