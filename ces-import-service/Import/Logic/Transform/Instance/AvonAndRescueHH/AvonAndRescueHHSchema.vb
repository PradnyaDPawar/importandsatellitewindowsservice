﻿Public Class AvonAndRescueHHSchema
    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        RenameColumnNames(dt)
        AddColumns(dt)

        Return dt

    End Function
    Public Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")
        If Not dt.Columns.Contains("MeterSerial") Then
            dt.Columns.Add("MeterSerial")
        End If

    End Sub

    Public Shared Sub RenameColumnNames(ByRef dt As DataTable)

        For Each col As DataColumn In dt.Columns
            col.ColumnName = col.ColumnName.Replace(" ", "").ToLower()
         
            Select Case col.ColumnName
                'Meter Serial
                Case "serial"
                    col.ColumnName = "MeterSerial"

                Case "serialnumber"
                    col.ColumnName = "MeterSerial"

                Case "meternumber"
                    col.ColumnName = "MeterSerial"

                    'Mpan
                Case "mprn"
                    col.ColumnName = "MPAN"

                Case "mpan/mprn"
                    col.ColumnName = "MPAN"

                Case "mpr"
                    col.ColumnName = "MPAN"
                Case "meter_identifier"
                    col.ColumnName = "MPAN"
                Case "meter_serial"
                    col.ColumnName = "MeterSerial"
                Case "read_date"
                    col.ColumnName = "Date(dd/MM/yyyy)"
                Case "hh01"
                    col.ColumnName = "00:00"
                Case "hh02"
                    col.ColumnName = "00:30"
                Case "hh03"
                    col.ColumnName = "01:00"
                Case "hh04"
                    col.ColumnName = "01:30"
                Case "hh05"
                    col.ColumnName = "02:00"
                Case "hh06"
                    col.ColumnName = "02:30"
                Case "hh07"
                    col.ColumnName = "03:00"
                Case "hh08"
                    col.ColumnName = "03:30"
                Case "hh09"
                    col.ColumnName = "04:00"
                Case "hh10"
                    col.ColumnName = "04:30"
                Case "hh11"
                    col.ColumnName = "05:00"
                Case "hh12"
                    col.ColumnName = "05:30"
                Case "hh13"
                    col.ColumnName = "06:00"
                Case "hh14"
                    col.ColumnName = "06:30"
                Case "hh15"
                    col.ColumnName = "07:00"
                Case "hh16"
                    col.ColumnName = "07:30"
                Case "hh17"
                    col.ColumnName = "08:00"
                Case "hh18"
                    col.ColumnName = "08:30"
                Case "hh19"
                    col.ColumnName = "09:00"
                Case "hh20"
                    col.ColumnName = "09:30"
                Case "hh21"
                    col.ColumnName = "10:00"
                Case "hh22"
                    col.ColumnName = "10:30"
                Case "hh23"
                    col.ColumnName = "11:00"

                Case "hh24"
                    col.ColumnName = "11:30"
                Case "hh25"
                    col.ColumnName = "12:00"
                Case "hh26"
                    col.ColumnName = "12:30"
                Case "hh27"
                    col.ColumnName = "13:00"
                Case "hh28"
                    col.ColumnName = "13:30"
                Case "hh29"
                    col.ColumnName = "14:00"
                Case "hh30"
                    col.ColumnName = "14:30"
                Case "hh31"
                    col.ColumnName = "15:00"
                Case "hh32"
                    col.ColumnName = "15:30"
                Case "hh33"
                    col.ColumnName = "16:00"
                Case "hh34"
                    col.ColumnName = "16:30"
                Case "hh35"
                    col.ColumnName = "17:00"
                Case "hh36"
                    col.ColumnName = "17:30"
                Case "hh37"
                    col.ColumnName = "18:00"
                Case "hh38"
                    col.ColumnName = "18:30"
                Case "hh39"
                    col.ColumnName = "19:00"
                Case "hh40"
                    col.ColumnName = "19:30"
                Case "hh41"
                    col.ColumnName = "20:00"
                Case "hh42"
                    col.ColumnName = "20:30"
                Case "hh43"
                    col.ColumnName = "21:00"
                Case "hh44"
                    col.ColumnName = "21:30"
                Case "hh45"
                    col.ColumnName = "22:00"
                Case "hh46"
                    col.ColumnName = "22:30"
                Case "hh47"
                    col.ColumnName = "23:00"
                Case "hh48"
                    col.ColumnName = "23:30"
              






            End Select


        Next

    End Sub
End Class
