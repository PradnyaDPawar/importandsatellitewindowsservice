﻿Imports System.Globalization

Public Class HavenInvoiceSchema

    Public Shared Function GetHeaders(ByVal colcount As Integer) As List(Of String)

        Dim headers As New List(Of String)

        For index = 0 To colcount - 1
            headers.Add(index)
        Next

        Return headers

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        Dim mpan As String
        Dim vat, vatrate, gross, net As Double
        Dim standardInvoiceTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        Dim invoiceflag As Boolean = True

        For Each row As DataRow In dt.Rows
            mpan = GetMpan(dt, dt.Rows.IndexOf(row))
            vatrate = GetVat(dt, dt.Rows.IndexOf(row))
            Dim invoiceTableRow As DataRow = standardInvoiceTable.NewRow
            If row(0).ToString.ToLower = "charge" Then
                invoiceTableRow("Invoice Number") = row("2")
                invoiceTableRow("Description") = row("7")
                invoiceTableRow("Start Date") = DateTime.ParseExact(row("24"), "yyyyMMdd", CultureInfo.InvariantCulture)
                invoiceTableRow("End Date") = DateTime.ParseExact(row("25"), "yyyyMMdd", CultureInfo.InvariantCulture)
                AddDayToEndDate(invoiceTableRow)
                invoiceTableRow("Consumption") = row("23")
                invoiceTableRow("Force kWh") = "Yes"
                invoiceTableRow("Net") = row("26")
                net = row("26")
                vat = net * vatrate
                gross = net + vat
                invoiceTableRow("MPAN") = mpan
                invoiceTableRow("Vat") = vat
                invoiceTableRow("Gross") = gross
                invoiceTableRow("Start Read Estimated") = "A"
                invoiceTableRow("End Read Estimated") = "A"
                PopulateCostOnlyColumn(invoiceTableRow)
                PopulateRateColumn(invoiceTableRow, row) 'Also populates description column if cost only is yes
                standardInvoiceTable.Rows.Add(invoiceTableRow)

            End If

        Next
        PopulateIsLastDayApportion(standardInvoiceTable)
        RenameColumns(standardInvoiceTable)
        dt = standardInvoiceTable
        Return dt
    End Function

    Private Shared Function GetMpan(ByRef dt As DataTable, ByVal currentRow As Integer) As String
        Dim mpan As String
        Dim rowcount As Integer = currentRow
        While Not dt.Rows(rowcount)(0).ToString.ToLower = "mpan" And rowcount < dt.Rows.Count - 1
            rowcount = rowcount + 1
        End While

        mpan = dt.Rows(rowcount)("4").ToString + dt.Rows(rowcount)("5").ToString + dt.Rows(rowcount)("6").ToString
        Return mpan

    End Function

    Private Shared Function GetVat(ByRef dt As DataTable, ByVal currentRow As Integer) As Double

        Dim rowcount As Integer = currentRow
        While Not dt.Rows(rowcount)(0).ToString.ToLower = "vat" And rowcount < dt.Rows.Count - 1
            rowcount = rowcount + 1
        End While

        Return dt.Rows(rowcount)("4")

    End Function

    Private Shared Sub PopulateCostOnlyColumn(ByRef invoiceTableRow As DataRow)

        Dim costOnlyYesList As List(Of String) = GetCostOnlyYesList() ' List of cost only yes Charges

        If invoiceTableRow("Consumption") = "" Or costOnlyYesList.Contains(invoiceTableRow("Description")) Then
            invoiceTableRow("Cost Only") = "Yes"
        Else
            invoiceTableRow("Cost Only") = "No"
        End If

    End Sub

    Private Shared Function GetCostOnlyYesList() As List(Of String)
        Dim costOnlyYesList As New List(Of String)
        costOnlyYesList.Add("Climate Change Levy")
        costOnlyYesList.Add("Excess Capacity")
        costOnlyYesList.Add("Data Collector & Data Aggregator Charges")
        costOnlyYesList.Add("Reactive Power")
        costOnlyYesList.Add("Availability Charge")
        costOnlyYesList.Add("Standing Charge")

        Return costOnlyYesList

    End Function

    Private Shared Sub PopulateRateColumn(ByRef invoiceTableRow As DataRow, ByRef row As DataRow)
        ' Populate Description if Cost Only is Yes and leave Rate blank
        If invoiceTableRow("Cost Only") = "No" Then
            invoiceTableRow("Rate") = row("7")
            invoiceTableRow("Description") = ""
        End If

    End Sub

    Private Shared Sub AddDayToEndDate(ByVal row As DataRow)

        Dim currentdate, newdate As DateTime
        currentdate = row("End Date")
        newdate = currentdate.AddDays(1)
        row("End Date") = newdate.ToShortDateString

    End Sub

    Private Shared Sub PopulateIsLastDayApportion(InvoiceDt As DataTable)

        For Each row As DataRow In InvoiceDt.Rows
            row("IsLastDayApportion") = "No"
        Next

    End Sub

    Private Shared Sub RenameColumns(InvoiceDt As DataTable)
        InvoiceDt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        InvoiceDt.Columns("Meter Serial").ColumnName = "MeterSerial"
        InvoiceDt.Columns("Rate").ColumnName = "Tariff"
        InvoiceDt.Columns("Start Date").ColumnName = "StartDate"
        InvoiceDt.Columns("End Date").ColumnName = "EndDate"
        InvoiceDt.Columns("Start Read").ColumnName = "StartRead"
        InvoiceDt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        InvoiceDt.Columns("End Read").ColumnName = "EndRead"
        InvoiceDt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        InvoiceDt.Columns("Force kWh").ColumnName = "ForcekWh"
        InvoiceDt.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub

End Class
