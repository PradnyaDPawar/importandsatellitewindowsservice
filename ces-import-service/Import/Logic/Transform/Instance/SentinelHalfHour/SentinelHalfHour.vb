﻿Public Class SentinelHalfHour
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvFile As New DelimitedFile(fullPath, 52)
        csvFile.Load(_returnTable, GetHeaders)
    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        AddColumns()

        For Each row As DataRow In _returnTable.Rows
            FillForceKwh(row)
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next

    End Sub


End Class
