﻿Partial Public Class OptimaHalfHourSchema

    Private Shared _meterSerial As String
    Public Shared Sub ToDeFormat(ByRef dt As DataTable)
        RenameColumns(dt)
        RemoveUnwantedRowsAndColumns(dt)
        AddColumns(dt)
    End Sub
    Public Shared Sub RenameColumns(ByRef dt As DataTable)
        'Rename HH entries
        For Each column As DataColumn In dt.Columns
            'replace space
            column.ColumnName = column.ColumnName.Replace(" ", "")

            'set alias
            If column.ColumnName.ToLower = "optimahalfhourlydata" Then
                column.ColumnName = "MPAN"
            End If

            If column.ColumnName.ToLower = "readingdate" Then
                column.ColumnName = "Date(DD/MM/YYYY)"
            End If
        Next

        dt.Columns("00:30").ColumnName = "00:00"
        dt.Columns("01:00").ColumnName = "00:30"
        dt.Columns("01:30").ColumnName = "01:00"
        dt.Columns("02:00").ColumnName = "01:30"
        dt.Columns("02:30").ColumnName = "02:00"
        dt.Columns("03:00").ColumnName = "02:30"
        dt.Columns("03:30").ColumnName = "03:00"
        dt.Columns("04:00").ColumnName = "03:30"
        dt.Columns("04:30").ColumnName = "04:00"
        dt.Columns("05:00").ColumnName = "04:30"
        dt.Columns("05:30").ColumnName = "05:00"
        dt.Columns("06:00").ColumnName = "05:30"
        dt.Columns("06:30").ColumnName = "06:00"
        dt.Columns("07:00").ColumnName = "06:30"
        dt.Columns("07:30").ColumnName = "07:00"
        dt.Columns("08:00").ColumnName = "07:30"
        dt.Columns("08:30").ColumnName = "08:00"
        dt.Columns("09:00").ColumnName = "08:30"
        dt.Columns("09:30").ColumnName = "09:00"
        dt.Columns("10:00").ColumnName = "09:30"
        dt.Columns("10:30").ColumnName = "10:00"
        dt.Columns("11:00").ColumnName = "10:30"
        dt.Columns("11:30").ColumnName = "11:00"
        dt.Columns("12:00").ColumnName = "11:30"
        dt.Columns("12:30").ColumnName = "12:00"
        dt.Columns("13:00").ColumnName = "12:30"
        dt.Columns("13:30").ColumnName = "13:00"
        dt.Columns("14:00").ColumnName = "13:30"
        dt.Columns("14:30").ColumnName = "14:00"
        dt.Columns("15:00").ColumnName = "14:30"
        dt.Columns("15:30").ColumnName = "15:00"
        dt.Columns("16:00").ColumnName = "15:30"
        dt.Columns("16:30").ColumnName = "16:00"
        dt.Columns("17:00").ColumnName = "16:30"
        dt.Columns("17:30").ColumnName = "17:00"
        dt.Columns("18:00").ColumnName = "17:30"
        dt.Columns("18:30").ColumnName = "18:00"
        dt.Columns("19:00").ColumnName = "18:30"
        dt.Columns("19:30").ColumnName = "19:00"
        dt.Columns("20:00").ColumnName = "19:30"
        dt.Columns("20:30").ColumnName = "20:00"
        dt.Columns("21:00").ColumnName = "20:30"
        dt.Columns("21:30").ColumnName = "21:00"
        dt.Columns("22:00").ColumnName = "21:30"
        dt.Columns("22:30").ColumnName = "22:00"
        dt.Columns("23:00").ColumnName = "22:30"
        dt.Columns("23:30").ColumnName = "23:00"
        dt.Columns("24:00").ColumnName = "23:30"
    End Sub
    Public Shared Sub AddColumns(ByRef dt As DataTable)
        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("MeterSerial")
    End Sub

    Private Shared Function RemoveUnwantedRowsAndColumns(ByRef dt As DataTable) As DataTable
        dt.Columns.Remove("DailyTotal")
        dt.Columns.Remove("MaxReading")
        dt.Columns.Remove("MinReading")
        dt.Columns.Remove("DataSource")
        dt.Columns.Remove("Status")
        Return dt
    End Function
End Class
