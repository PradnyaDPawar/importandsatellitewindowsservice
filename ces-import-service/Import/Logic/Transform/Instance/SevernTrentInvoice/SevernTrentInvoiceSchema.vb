﻿Public Class SevernTrentInvoiceSchema
    Shared Sub ToDeFormat(ByRef returnTable As DataTable)

        FormatColumnHeaders(returnTable)
        DataTableTools.RemoveBlankRows(returnTable)
        Dim invoiceDataTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        CreateInvoiceEntries(returnTable, invoiceDataTable)
        RenameColumns(invoiceDataTable)
        returnTable = invoiceDataTable

    End Sub

    Private Shared Function FormatColumnHeaders(ByRef dt As DataTable) As DataTable
        For Each column As DataColumn In dt.Columns
            column.ColumnName = column.ColumnName.Trim.Replace(" ", "")
        Next
        Return dt
    End Function

    Private Shared Sub CreateInvoiceEntries(ByRef returnTable As DataTable, ByRef invoiceDataTable As DataTable)
        For Each row As DataRow In returnTable.Rows
            If CheckValidRow(row) Then
                If CheckValidCharge(row("WaterCharge")) Then
                    PopulateEntries(row, invoiceDataTable, "WaterCharge")
                End If
                If CheckValidCharge(row("SewerageCharge")) Then
                    PopulateEntries(row, invoiceDataTable, "SewerageCharge")
                End If
                If CheckValidCharge(row("DrainageCharge")) Then
                    PopulateEntries(row, invoiceDataTable, "DrainageCharge")
                End If
            End If
        Next
    End Sub
    Private Shared Sub PopulateEntries(dtRow As DataRow, invoiceDataTable As DataTable, ColName As String)
        Try
            If dtRow.Table.Columns.Contains(ColName) Then

                If Not String.IsNullOrEmpty(dtRow(ColName)) Then

                    Dim invoiceTableRow As DataRow = invoiceDataTable.NewRow

                    invoiceTableRow("Invoice Number") = dtRow("InvoiceNumber") + "-" + dtRow("PropertyNumber")
                    invoiceTableRow("MPAN") = dtRow("PropertyNumber")
                    invoiceTableRow("Gross") = dtRow(ColName).Replace("£", "")
                    invoiceTableRow("Meter Serial") = dtRow("MeterNumber")


                    invoiceTableRow("Start Date") = Convert.ToDateTime(dtRow("PreviousReadDate")).ToString("dd-MM-yy")
                    invoiceTableRow("End Date") = Convert.ToDateTime(dtRow("CurrentReadDate")).ToString("dd-MM-yy")
                 
                    If ColName = "WaterCharge" Then

                        invoiceTableRow("End Read") = dtRow("CurrentReading")
                        invoiceTableRow("End Read Estimated") = PopulateReadEstimation(dtRow("ReadingIndicator"))
                        invoiceTableRow("Start Read") = dtRow("PreviousReading")
                        invoiceTableRow("Start Read Estimated") = PopulateReadEstimation(dtRow("ReadingIndicator1"))
                        invoiceTableRow("Consumption") = dtRow("Usage")
                    End If

                    invoiceTableRow("Force kWh") = "No"

                    If Not (String.IsNullOrEmpty(Convert.ToString(invoiceTableRow("Consumption")).Trim)) Then
                        invoiceTableRow("Cost Only") = "No"
                    Else
                        invoiceTableRow("Cost Only") = "Yes"
                    End If

                    Dim Description As String = String.Empty
                    Select Case ColName
                        Case "WaterCharge"
                            Description = "Water Charge"
                        Case "SewerageCharge"
                            Description = "Sewerage Charge"
                        Case "DrainageCharge"
                            Description = "Drainage Charge"
                    End Select

                    invoiceTableRow("Rate") = Description
                    invoiceTableRow("Description") = Description
                    invoiceTableRow("IsLastDayApportion") = "Yes"

                    invoiceDataTable.Rows.Add(invoiceTableRow)
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Shared Sub RenameColumns(invoiceDataTable As DataTable)
        invoiceDataTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceDataTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceDataTable.Columns("Rate").ColumnName = "Tariff"
        invoiceDataTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceDataTable.Columns("End Date").ColumnName = "EndDate"
        invoiceDataTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceDataTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceDataTable.Columns("End Read").ColumnName = "EndRead"
        invoiceDataTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceDataTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceDataTable.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub
    Private Shared Function PopulateReadEstimation(ByVal readingIndicator As String) As String
        Dim ListActual As New List(Of Char)(New Char() {"a", "c", "f", "i", "m", "p", "r", "s", "b"})
        Dim ListEstimated As New List(Of Char)(New Char() {"d", "e", "z"})
        Dim readEstimated As String = String.Empty
        ' End Read Estimated
        If ListActual.Contains(readingIndicator.ToString.ToLower) Then
            readEstimated = "a"
        ElseIf ListEstimated.Contains(readingIndicator.ToString.ToLower) Then
            readEstimated = "e"
        End If
        Return readEstimated
    End Function
    Private Shared Function CheckValidCharge(ByVal Charge As String) As Boolean
        If Not String.IsNullOrEmpty(Charge) Then
            Return IIf(Val(Charge.Replace("£", "")) > 0, True, False)
        Else
            Return False
        End If
    End Function

    Private Shared Function CheckValidRow(ByVal row As DataRow) As Boolean
        If String.IsNullOrEmpty(row("CustomerName")) And String.IsNullOrEmpty(row("CostCentreCode")) And
           String.IsNullOrEmpty(row("AccountNumber")) And String.IsNullOrEmpty(row("InvoiceNumber")) And
           String.IsNullOrEmpty(row("PropertyNumber")) Then
            Return False
        Else
            Return True
        End If
    End Function

End Class