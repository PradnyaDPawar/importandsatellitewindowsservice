﻿Public Class SevernTrentInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Try
            Dim excel As New Excel()
            Dim xSheet As ExcelSheet
            excel.Load(fullPath)

            xSheet = excel.Sheets(0)
            _returnTable = xSheet.GetDataTable()
        Catch ex As Exception
        End Try
    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        DataTableTools.SetHeaders(ReturnTable, 1)

        SevernTrentInvoiceSchema.ToDeFormat(_returnTable)
        FillInvoice(_returnTable, dsEdits, importEdit)
    End Sub
    Public Sub FillInvoice(ByVal ReturnTable As DataTable, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)
        For Each row As DataRow In ReturnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
