﻿Imports System.Globalization

Public Class ASLHoldingsHalfHourSchema
    Public Shared Sub ToDEFormat(ByRef dt As DataTable)
        RenameColumns(dt)
        RemoveUnwantedRowsAndColumns(dt)
        ModifyColumnHeaders(dt)
        AddColumns(dt)
    End Sub

    Public Shared Sub RenameColumns(ByRef dt As DataTable)
        'Rename HH entries
        dt.Columns(0).ColumnName = "MPAN"
        dt.Columns(1).ColumnName = "MeterSerial"
        dt.Columns(2).ColumnName = "Date(DD/MM/YYYY)"
        dt.Columns(3).ColumnName = "TotalCumImportkWh"
        dt.Columns(4).ColumnName = "TotalCumExportkWh"
    End Sub

    Public Shared Sub AddColumns(ByRef dt As DataTable)
        dt.Columns.Add("ForcekWh")
    End Sub

    Private Shared Function RemoveUnwantedRowsAndColumns(ByRef dt As DataTable) As DataTable

        For columncount As Integer = dt.Columns.Count - 1 To 5 Step -2
            dt.Columns.RemoveAt(columncount)
        Next
        dt.Columns.Remove("TotalCumImportkWh")
        dt.Columns.Remove("TotalCumExportkWh")
        Return dt
    End Function
    ''' <summary>
    ''' Method to get column header for timeslots
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <remarks></remarks>
    Private Shared Sub ModifyColumnHeaders(ByRef dt As DataTable)
        Dim datetime As New DateTime(2015, 7, 7, 23, 30, 0)
        Dim timeSlot As DateTime = datetime.ToString("HH:mm")

        For columncount As Integer = dt.Columns.Count - 1 To 3 Step -1
            dt.Columns(columncount).ColumnName = timeSlot.ToString("HH:mm")
            If timeSlot.ToString("HH:mm") <> "00:00" Then
                timeSlot = timeSlot.AddMinutes(-30).ToString("HH:mm")
            End If
        Next
    End Sub

End Class


