﻿Public Class ExotericHalfHourSchema

    Public Shared Function ToDEFormat(ByRef tdt As DataTable)
        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        DataTableTools.RemoveBlankColumns(tdt)
        DataTableTools.RemoveBlankRows(tdt)
        'DataTableTools.RemoveTopRows(tdt, 1)
        ProcessColumns(tdt)
        CreateHalfHourlyEntries(tdt, hhDataTable)
        Return tdt
    End Function
    Public Shared Sub ProcessColumns(ByRef dt As DataTable)
        dt.Columns(0).ColumnName = "Date"
        dt.Columns(1).ColumnName = "MPAN"
        dt.Columns(3).ColumnName = "Consumption"
        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("MeterSerial")
    End Sub
    
    Public Shared Function Validate(ByRef tdt As DataTable) As Boolean

        Dim listofdates As New List(Of Date)
        listofdates = GetAllDates(tdt)
        Dim listofdatetime As List(Of DateTime)
        listofdatetime = GetDateTimeEntries(listofdates)

        Dim ValidFlag As Boolean = False

        For Each datetimeentry As DateTime In listofdatetime

            For Each row As DataRow In tdt.Rows
                If IsDate(row(0)) Then
                    If datetimeentry = row(0) Then
                        ValidFlag = True
                    End If
                End If
            Next

            If Not ValidFlag Then
                Return False
            End If

            ValidFlag = False
        Next

        Return True

    End Function

    Private Shared Function GetAllDates(ByRef tdt As DataTable) As List(Of Date)
        Dim listofdates As New List(Of Date)
        Dim datetocheck As Date

        For Each row As DataRow In tdt.Rows

            If IsDate(row(0)) Then
                datetocheck = DateTime.Parse(row(0))
                If Not listofdates.Contains(datetocheck.Date) Then
                    listofdates.Add(datetocheck.Date)
                End If
            End If

        Next
        Return listofdates
    End Function
    Private Shared Function GetDateTimeEntries(ByVal dateEntries As List(Of DateTime)) As List(Of DateTime)
        Dim dateTimeEntries As New List(Of DateTime)

        For Each dateentry As DateTime In dateEntries
            dateTimeEntries.Add(dateentry)
            For addCount As Integer = 1 To 22
                dateentry = dateentry.AddMinutes(30)
                dateTimeEntries.Add(dateentry)
            Next
        Next

        Return dateTimeEntries
    End Function

    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim dateholder As DateTime
        Dim success As Boolean = False
        For Each row As DataRow In dt.Rows

            If IsDate(row("Date")) Then
                dateholder = row("Date")
                For Each hhrow As DataRow In hhDataTable.Rows
                    If hhrow("Date(dd/mm/yyyy)") = dateholder.Date And hhrow("MPAN") = row("Mpan") Then
                        hhrow(dateholder.ToString("HH:mm")) = IIf(row("Consumption") Is DBNull.Value OrElse row("Consumption") = Nothing, DBNull.Value, CInt(row("Consumption").Replace("""", Nothing).ToString())
)
                        hhrow("MPAN") = row("Mpan")
                        success = True
                    End If
                Next

                If Not success Then
                    Dim newRow As DataRow = hhDataTable.NewRow
                    newRow("Date(dd/mm/yyyy)") = dateholder.Date
                    newRow(dateholder.ToString("HH:mm")) = IIf(row("Consumption") Is DBNull.Value OrElse row("Consumption") = Nothing, DBNull.Value, CInt(row("Consumption").Replace("""", Nothing).ToString()))
                    newRow("MPAN") = row("Mpan")
                    hhDataTable.Rows.Add(newRow)
                End If
            End If
            success = False
        Next

        dt = hhDataTable

    End Sub
End Class


