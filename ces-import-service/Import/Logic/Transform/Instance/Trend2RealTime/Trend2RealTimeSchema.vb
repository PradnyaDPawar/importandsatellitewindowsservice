﻿Public Class Trend2RealTimeSchema
    Public Shared Function ToDeFormat(ByRef tdt As DataTable)
        tdt.Columns("PointID").ColumnName = "MeterName"
        tdt.Columns("DataTime").ColumnName = "DateTime"
        tdt.Columns("DataValue").ColumnName = "CountUnits"
        Return tdt
    End Function

    Public Shared Function ConvertToUKDatetimeFormat(ByRef tdt As DataTable)

        Dim rtdt As DataTable = tdt.Clone()

        For Each dr As DataRow In tdt.Rows

            If IsDate(dr("DateTime")) Then

                Dim newRow As DataRow = rtdt.NewRow

                newRow("siteLabel") = dr("siteLabel")
                newRow("MeterName") = dr("MeterName")
                newRow("DateTime") = Convert.ToDateTime(dr("DateTime")).ToString("dd/MM/yyyy HH:mm")
                newRow("CountUnits") = dr("CountUnits")

                rtdt.Rows.Add(newRow)

            End If


        Next

        Return rtdt
    End Function
End Class
