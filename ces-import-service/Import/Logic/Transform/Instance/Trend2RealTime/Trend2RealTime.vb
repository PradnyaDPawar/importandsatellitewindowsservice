﻿Imports System.IO

Public Class Trend2RealTime
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim delimitedFile As New DelimitedFile(fullPath, 5)

        Dim fileExtension As String = Path.GetExtension(fullPath)

        delimitedFile.Delimiter = vbTab

        Dim headersList As New List(Of String)
        headersList.Add("siteLabel")
        headersList.Add("PointID")
        headersList.Add("DataTime")
        headersList.Add("DataValue")
        headersList.Add("BlankColumn")

        delimitedFile.LoadNonConsistentTable(ReturnTable, headersList)

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        DataTableTools.RemoveTopRows(ReturnTable, 2)
        DataTableTools.RemoveBlankColumns(ReturnTable)

        Trend2RealTimeSchema.ToDeFormat(_returnTable)

        _returnTable = Trend2RealTimeSchema.ConvertToUKDatetimeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_RealTime.Fill(row, dsEdits, importEdit)

        Next

    End Sub
End Class