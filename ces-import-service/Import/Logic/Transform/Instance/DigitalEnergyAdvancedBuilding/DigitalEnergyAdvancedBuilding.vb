﻿Public Class DigitalEnergyAdvancedBuilding
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property


    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim strExtension As String
        strExtension = System.IO.Path.GetExtension(fullPath)

        If strExtension = ".csv" Then
            Dim csvFile As DelimitedFile
            csvFile = New DelimitedFile(fullPath, 1)
            csvFile.HasHeader = False
            csvFile.FieldsAreQuoted = True
            csvFile.Load(_returnTable, GetHeaders())
        Else

            Dim excel As New Excel
            excel.Load(fullPath)

            Dim xSheet As ExcelSheet = excel.Sheets(0)
            excel.GetSheet("Advanced Building", xSheet)
            xSheet.HeaderRowIndex = 1
            _returnTable = xSheet.GetDataTable()

        End If

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        RenameColumns()
        ColumnValidate()

        For Each row As DataRow In _returnTable.Rows
            FE_AdvancedBuilding.Fill(row, dsEdits, importEdit)
        Next

    End Sub
End Class
