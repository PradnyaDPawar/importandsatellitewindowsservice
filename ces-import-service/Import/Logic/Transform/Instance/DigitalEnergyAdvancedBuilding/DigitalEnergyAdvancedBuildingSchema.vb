﻿Partial Public Class DigitalEnergyAdvancedBuilding

    Private Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("UPRN")
        headers.Add("BuildingOwner")
        headers.Add("Tenant")
        headers.Add("FacilitiesManager")
        headers.Add("EnergyChampion")
        headers.Add("MainSectorBenchmark")
        headers.Add("SubSectorBenchmark")
        headers.Add("TotalFloorArea")
        headers.Add("NumberOfOccupants")
        headers.Add("NumberOfPupils")
        headers.Add("AnnualHoursOfOccupancy")
        headers.Add("MainHeatingSystem")
        headers.Add("MainHeatingFuel")
        headers.Add("HeatingSetpoint")
        headers.Add("CoolingSetpoint")
        headers.Add("PropertyRef")
        headers.Add("BuiltDate")
        headers.Add("Subdivision")
        headers.Add("EPCGrade")
        headers.Add("EPCRating")
        headers.Add("EPCOther")
        headers.Add("Notes")
        headers.Add("RateableValue")
        headers.Add("NetInternalArea")
        headers.Add("Category")
        headers.Add("PropertyManager")
        headers.Add("PropertySurveyor")
        headers.Add("Administrator")
        headers.Add("AssetCategory")
        headers.Add("BudgetCode")
        headers.Add("Division")
        headers.Add("PropertyType")
        headers.Add("Service")
        Return headers
    End Function

    Private Sub ColumnValidate()
        Dim rawHeaders As List(Of String) = GetHeaders()
        For Each header As String In rawHeaders
            If Not _returnTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next
    End Sub

    Private Sub RenameColumns()
        Dim renamables() = {"TotalFloorArea", "HeatingSetpoint", "CoolingSetpoint", "NetInternalArea"}
        For Each column As DataColumn In _returnTable.Columns
            If (column.ColumnName = "Asset Owner") Then
                column.ColumnName = "Building Owner"

            End If
            column.ColumnName = column.ColumnName.Replace(" ", "")
            For Each prefix In renamables
                If column.ColumnName.StartsWith(prefix) Then
                    column.ColumnName = prefix
                    Exit For
                End If
            Next
        Next
    End Sub

End Class