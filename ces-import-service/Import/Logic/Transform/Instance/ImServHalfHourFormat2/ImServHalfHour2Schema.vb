﻿Public Class ImServHalfHour2Schema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("Site ID")
        headers.Add("Meter Number")
        headers.Add("Data Type")
        headers.Add("Reading Date")
        headers.Add("00:00")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")

        Return headers

    End Function

    Public Shared Function Validate(ByRef data As DataTable) As Boolean

        For Each header As String In GetHeaders()
            If Not data.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next

    End Function


    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

        RenameColumns(dt)
        CreateColumns(dt)
        PopulateForceKwh(dt)
        ReformatDateColumn(dt)

        Return dt

    End Function


    Public Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("Meter Number").ColumnName = "MeterSerial"
        dt.Columns("Reading Date").ColumnName = "Date(dd/mm/yyyy)"

    End Sub


    Public Shared Sub CreateColumns(ByRef dt As DataTable)

        dt.Columns.Add("MPAN")
        dt.Columns.Add("ForcekWh")

    End Sub


    Public Shared Sub PopulateForceKwh(ByRef dt As DataTable)


        For Each row As DataRow In dt.Rows

            If Not row("Data Type") = "" Then

                If row("Data Type").ToString.ToLower() = "kwh" Then
                    row("ForcekWh") = "Yes"
                Else
                    row("ForcekWh") = "No"
                End If

            End If

        Next


    End Sub


    Private Shared Sub ReformatDateColumn(ByRef dt As DataTable)

        Dim dateholder As DateTime

        For Each row As DataRow In dt.Rows

            dateholder = row("Date(dd/mm/yyyy)")
            row("Date(dd/mm/yyyy)") = dateholder.ToShortDateString

        Next

    End Sub

End Class
