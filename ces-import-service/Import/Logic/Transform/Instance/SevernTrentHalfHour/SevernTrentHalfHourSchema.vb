﻿Public Class SevernTrentHalfHourSchema


    Public Shared Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("Header1")
        headers.Add("Header2")
        headers.Add("Header3")
        headers.Add("Header4")
        headers.Add("Header5")
        headers.Add("Header6")
        headers.Add("Header7")
        headers.Add("Header8")
        headers.Add("Header9")
        Return headers
    End Function

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)
        dt = hhDataTable

    End Sub
    Public Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim datetimeHolder As DateTime
        Dim success As Boolean = False

        Dim column As DataColumn
        column = dt.Columns("Header2")

        For Each row As DataRow In dt.Rows

            If IsDate(row("Header1")) Then
                datetimeHolder = CDate(row("Header1"))

                If datetimeHolder.Minute = 0 Or datetimeHolder.Minute = 30 Then

                    For Each hhrow As DataRow In hhDataTable.Rows
                        If hhrow("Date(dd/mm/yyyy)") = datetimeHolder.Date.ToString("dd'/'MM'/'yyyy") And hhrow("MeterSerial") = dt.Rows(0)("Header5").ToString.Replace("""", "") Then

                            Dim consumption1, consumption2 As Double
                            consumption1 = (GetConsumptionByDateTime(datetimeHolder.AddMinutes(15), dt, column)) 'row(column)
                            consumption2 = (GetConsumptionByDateTime(datetimeHolder.AddMinutes(30), dt, column))
                            hhrow(datetimeHolder.ToString("HH:mm")) = ((consumption1 + consumption2) / 4).ToString("0.000")

                            success = True
                        End If

                    Next

                    If Not success Then
                        Dim newRow As DataRow = hhDataTable.NewRow
                        newRow("Date(dd/mm/yyyy)") = datetimeHolder.Date.ToString("dd'/'MM'/'yyyy")
                        newRow("MeterSerial") = dt.Rows(0)("Header5").ToString.Replace("""", "")
                        If dt.Rows(0)(column).ToString.ToLower.Trim <> "kwh" Then
                            newRow("ForcekWh") = "No"
                        Else
                            newRow("ForcekWh") = "Yes"
                        End If
                        Dim consumption1, consumption2 As Double
                        consumption1 = (GetConsumptionByDateTime(datetimeHolder.AddMinutes(15), dt, column)) 'row(column)
                        consumption2 = (GetConsumptionByDateTime(datetimeHolder.AddMinutes(30), dt, column))

                        newRow(datetimeHolder.ToString("HH:mm")) = ((consumption1 + consumption2) / 4).ToString("0.000")

                        hhDataTable.Rows.Add(newRow)

                    End If

                End If

            End If

            success = False

        Next


        dt = hhDataTable

    End Sub

    Public Shared Function GetConsumptionByDateTime(ByVal timeToFind As DateTime, ByVal dt As DataTable, ByVal col As DataColumn)

        Dim dateHolder As Date
        Dim timeHolder As DateTime


        For Each row As DataRow In dt.Rows

            If IsDate(row("Header1")) Then

                dateHolder = row("Header1")
                timeHolder = dateHolder.ToString("HH:mm")
                If timeHolder.TimeOfDay = timeToFind.TimeOfDay And dateHolder.Date = timeToFind.Date Then
                    Return row(col)
                End If

            End If

        Next

        Return 0

    End Function

End Class


