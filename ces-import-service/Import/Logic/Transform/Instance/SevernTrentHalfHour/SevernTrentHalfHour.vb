﻿Public Class SevernTrentHalfHour


    Implements ITransformDelegate
    Private _returntable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returntable
        End Get

        Set(ByVal value As DataTable)
            _returntable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim csvFile As New DelimitedFile(fullPath, 9)
        csvFile.HasHeader = True
        csvFile.LoadNonConsistentTable(_returntable, SevernTrentHalfHourSchema.GetHeaders)

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        SevernTrentHalfHourSchema.ToDeFormat(_returntable)


        For Each row As DataRow In _returntable.Rows
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next

    End Sub

    

End Class



