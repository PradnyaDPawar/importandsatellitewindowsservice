﻿Public Class GazpromFormat2InvoiceSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        UniqueRow(dt.Rows(0))
        PrefixMainHeaders(dt, 0)
        DataTableTools.SetHeaders(dt, 2)
        DataTableTools.RemoveTopRows(dt, 1)
        DataTableTools.RemoveBlankRows(dt)
        RemoveTotalsRow(dt)
        RemoveBlankColumnsExceptMeterID(dt)
        FormatColumnHeaders(dt)
        Dim invoiceDataTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        CreateInvoiceEntries(dt, invoiceDataTable)
        RenameColumns(invoiceDataTable)
        PopulateIsLastDayApportion(invoiceDataTable)
        dt = invoiceDataTable
        Return dt
    End Function

    Private Shared Function RemoveBlankColumnsExceptMeterID(ByRef dt As DataTable) As DataTable

        Dim remove As Boolean

        For colcount As Integer = dt.Columns.Count - 1 To 0 Step -1
            If Not dt.Columns(colcount).ColumnName.Contains("Meter ID") Then

                remove = True
                For Each row As DataRow In dt.Rows

                    If Not row(colcount) = "" Then
                        remove = False
                    End If

                Next

                If remove Then
                    dt.Columns.RemoveAt(colcount)
                End If

            End If
        Next

        Return dt

    End Function

    Private Shared Sub UniqueRow(ByRef row As DataRow)
        Dim rowValuesList As New List(Of String)

        For Each column As DataColumn In row.Table.Columns
            If Not String.IsNullOrEmpty(row(column).ToString.Trim) Then
                rowValuesList.Add(row(column).ToString)
                Dim count As Integer = GetOccurrencesCount(row(column).ToString, rowValuesList)
                If count > 0 Then
                    row(column) = row(column).ToString & " " & count.ToString
                End If
            End If
        Next

    End Sub

    Private Shared Function GetOccurrencesCount(ByVal valueToFind As String, ByVal rowValues As List(Of String)) As Integer
        Dim count As Integer = (From temp In rowValues Where (temp.Equals(valueToFind)) Select temp).Count
        Return count
    End Function

    Private Shared Sub PrefixMainHeaders(ByRef dt As DataTable, ByVal MainHeaderRowIndex As Integer)
        Dim mainHeader, mainHeader2 As String
        mainHeader2 = ""
        For Each column As DataColumn In dt.Columns
            mainHeader = dt.Rows(MainHeaderRowIndex)(column)

            If Not String.IsNullOrEmpty(mainHeader) Then
                dt.Rows(MainHeaderRowIndex + 1)(column) = mainHeader & " " & dt.Rows(MainHeaderRowIndex + 1)(column)
                mainHeader2 = mainHeader
            ElseIf Not String.IsNullOrEmpty(mainHeader2) Then
                dt.Rows(MainHeaderRowIndex + 1)(column) = mainHeader2 & " " & dt.Rows(MainHeaderRowIndex + 1)(column)
            End If
        Next
    End Sub

    Private Shared Function FormatColumnHeaders(ByRef dt As DataTable) As DataTable
        For Each column As DataColumn In dt.Columns
            column.ColumnName = column.ColumnName.Trim
        Next
        Return dt
    End Function

    Private Shared Sub CreateInvoiceEntries(ByRef dt As DataTable, ByRef invoiceTable As DataTable)
        For Each row As DataRow In dt.Rows
            PopulateRateEntries(row, invoiceTable)
            PopulateNonEnergyCharge(row, invoiceTable, "Reactive Charges 1 Total (£)")
            PopulateNonEnergyCharge(row, invoiceTable, "Availability Charge 1 Total (£)")
            PopulateNonEnergyCharge(row, invoiceTable, "Standing Charge 1 Total (£)")
            PopulateNonEnergyCharge(row, invoiceTable, "Levy Charges 1 CCL (£)")
            PopulateNonEnergyCharge(row, invoiceTable, "Levy Charges 1 LEB (£)")
            PopulateNonEnergyCharge(row, invoiceTable, "Levy Charges 1 HDCL (£)")
            PopulateNonEnergyCharge(row, invoiceTable, "Other/Adjusted Energy Charges 1 Total")
        Next
    End Sub

    Private Shared Sub PopulateRateEntries(ByRef dtRow As DataRow, ByRef invoiceTable As DataTable)

        Dim net, vat As Double
        Dim mpan As String

        For count As Integer = 1 To 10
            If dtRow.Table.Columns.Contains("Consumption Details" & " " & count.ToString & " " & "Description") Then
                If Not String.IsNullOrEmpty(dtRow("Consumption Details" & " " & count.ToString & " " & "Meter ID")) Then

                    Dim invoiceTableRow As DataRow = invoiceTable.NewRow
                    invoiceTableRow("Invoice Number") = dtRow("Your Details 1 Site Invoice")
                    invoiceTableRow("Meter Serial") = dtRow("Consumption Details" & " " & count.ToString & " " & "Meter ID")
                    mpan = dtRow("Your Details 1" & " " & "Supply Number")
                    mpan = mpan.Substring(mpan.Length - 13)
                    invoiceTableRow("MPAN") = mpan
                    invoiceTableRow("Rate") = dtRow("Consumption Details" & " " & count.ToString & " " & "Description")
                    invoiceTableRow("Start Date") = dtRow("Your Details 1 From Date")
                    invoiceTableRow("End Date") = dtRow("Your Details 1 To Date")
                    invoiceTableRow("Start Read") = dtRow("Consumption Details" & " " & count.ToString & " " & "Start Read")
                    invoiceTableRow("Start Read Estimated") = dtRow("Consumption Details" & " " & count.ToString & " " & "Start Read Type")
                    invoiceTableRow("End Read") = dtRow("Consumption Details" & " " & count.ToString & " " & "End Read")
                    invoiceTableRow("End Read Estimated") = dtRow("Consumption Details" & " " & count.ToString & " " & "End Read Type")
                    invoiceTableRow("Consumption") = dtRow("Consumption Details" & " " & count.ToString & " " & "Consumption (kWh)")
                    invoiceTableRow("Force kWh") = "Yes"
                    invoiceTableRow("Net") = dtRow("Consumption Details" & " " & count.ToString & " " & "Total (£)")
                    net = invoiceTableRow("Net")
                    vat = net * 0.2
                    invoiceTableRow("Vat") = vat
                    invoiceTableRow("Gross") = (net + vat).ToString("0.00")
                    invoiceTableRow("Cost Only") = "No"
                    invoiceTableRow("Description") = ""
                    invoiceTableRow("TaxPointDate") = dtRow("Your Details 1 Tax Point Date")
                    invoiceTable.Rows.Add(invoiceTableRow)
                End If
            End If

        Next

    End Sub

    Private Shared Sub PopulateNonEnergyCharge(ByRef dtRow As DataRow, ByRef invoiceTable As DataTable, ByVal charge As String)
        If dtRow.Table.Columns.Contains(charge) Then
            If Not String.IsNullOrEmpty(dtRow(charge)) Then
                Dim invoiceTableRow As DataRow = invoiceTable.NewRow
                Dim net, vat As Double
                Dim mpan As String

                invoiceTableRow("Invoice Number") = dtRow("Your Details 1 Site Invoice")
                invoiceTableRow("Meter Serial") = dtRow("Consumption Details 1" & " " & "Meter ID")
                mpan = dtRow("Your Details 1" & " " & "Supply Number")
                mpan = mpan.Substring(mpan.Length - 13)
                invoiceTableRow("MPAN") = mpan
                invoiceTableRow("Start Date") = dtRow("Your Details 1 From Date")
                invoiceTableRow("End Date") = dtRow("Your Details 1 To Date")
                invoiceTableRow("Net") = dtRow(charge)
                net = invoiceTableRow("Net")
                vat = net * GetVatRate(dtRow)
                invoiceTableRow("Vat") = vat.ToString("0.00")
                invoiceTableRow("Gross") = (net + vat).ToString("0.00")
                invoiceTableRow("Force kWh") = "Yes"
                invoiceTableRow("Cost Only") = "Yes"
                invoiceTableRow("Description") = charge
                invoiceTableRow("TaxPointDate") = dtRow("Your Details 1 Tax Point Date")
                invoiceTable.Rows.Add(invoiceTableRow)

            End If
        End If
    End Sub

    Private Shared Function GetVatRate(ByVal row As DataRow) As Double
        Dim vatRate As Double
        If row.Table.Columns.Contains("VAT Charges 1 VAT @ 5% (£)") Then
            If row("VAT Charges 1 VAT @ 5% (£)") <> 0 Then
                vatRate = 0.05
            End If
        End If

        If row.Table.Columns.Contains("VAT Charges 1 VAT @ 15% (£)") Then
            If row("VAT Charges 1 VAT @ 15% (£)") <> 0 Then
                vatRate = 0.15
            End If
        End If

        If row.Table.Columns.Contains("VAT Charges 1 VAT @ 17.5% (£)") Then
            If row("VAT Charges 1 VAT @ 17.5% (£)") <> 0 Then
                vatRate = 0.175
            End If
        End If

        If row.Table.Columns.Contains("VAT Charges 1 VAT @ 20% (£)") Then
            If row("VAT Charges 1 VAT @ 20% (£)") <> 0 Then
                vatRate = 0.2
            End If
        End If

        Return vatRate
    End Function

    Private Shared Sub RemoveTotalsRow(ByRef dt As DataTable)
        Dim rowIndexToRemove As Integer = -1
        For Each column As DataColumn In dt.Columns
            For Each row As DataRow In dt.Rows
                If row(column) = "Totals" And String.IsNullOrEmpty(row("Your Details 1 Supply Number").ToString.Trim) Then
                    rowIndexToRemove = dt.Rows.IndexOf(row)
                End If
            Next
        Next
        If rowIndexToRemove >= 0 Then
            dt.Rows.RemoveAt(rowIndexToRemove)
        End If

    End Sub

    Private Shared Sub PopulateIsLastDayApportion(InvoiceDt As DataTable)

        For Each row As DataRow In InvoiceDt.Rows
            row("IsLastDayApportion") = "No"
        Next

    End Sub

    Private Shared Sub RenameColumns(InvoiceDt As DataTable)
        InvoiceDt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        InvoiceDt.Columns("Meter Serial").ColumnName = "MeterSerial"
        InvoiceDt.Columns("Rate").ColumnName = "Tariff"
        InvoiceDt.Columns("Start Date").ColumnName = "StartDate"
        InvoiceDt.Columns("End Date").ColumnName = "EndDate"
        InvoiceDt.Columns("Start Read").ColumnName = "StartRead"
        InvoiceDt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        InvoiceDt.Columns("End Read").ColumnName = "EndRead"
        InvoiceDt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        InvoiceDt.Columns("Force kWh").ColumnName = "ForcekWh"
        InvoiceDt.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub
End Class
