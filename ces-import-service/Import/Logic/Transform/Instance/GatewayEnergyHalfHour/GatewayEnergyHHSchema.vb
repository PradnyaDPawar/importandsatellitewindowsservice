﻿Public Class GatewayEnergyHHSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        Dim mpan, meterSerial As String
        mpan = GetMpan(dt)
        meterSerial = GetMeterSerial(dt)

        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        dt.Rows.RemoveAt(0)
        dt.Rows.RemoveAt(0)
        dt.Rows.RemoveAt(0)
        dt.Columns(0).ColumnName = "Date"
        dt.Columns(1).ColumnName = "Time"
        dt.Columns(2).ColumnName = "Consumption"
        CreateHalfHourlyEntries(dt, hhDataTable)
        PopulateMpan(dt, mpan)
        PopulateMeterSerial(dt, meterSerial)
        PopulateForceKwh(dt)
        Return dt
    End Function


    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim dateholder As DateTime
        Dim success As Boolean = False
        For Each row As DataRow In dt.Rows

            If IsDate(row("Date")) Then
                dateholder = row("Date")
                For Each hhrow As DataRow In hhDataTable.Rows
                    If hhrow("Date(dd/mm/yyyy)") = dateholder.Date Then
                        hhrow(row("Time")) = row("Consumption")
                        success = True
                    End If
                Next

                If Not success Then
                    Dim newRow As DataRow = hhDataTable.NewRow
                    newRow("Date(dd/mm/yyyy)") = dateholder.Date
                    newRow(row("Time")) = row("Consumption")
                    hhDataTable.Rows.Add(newRow)
                End If
            End If
            success = False
        Next

        dt = hhDataTable

    End Sub

    Private Shared Function GetMpan(ByRef dt As DataTable) As String
        Dim stringHolder, mpan As String
        For Each row As DataRow In dt.Rows
            For Each column As DataColumn In dt.Columns

                stringHolder = row(column)
                If stringHolder.ToLower.StartsWith("mpan") Then
                    mpan = stringHolder.Substring((stringHolder.IndexOf(":") + 1), ((stringHolder.Length - 1) - stringHolder.IndexOf(":"))).Trim
                    Return mpan
                End If
            Next
        Next
        Return String.Empty
    End Function

    Private Shared Function GetMeterSerial(ByRef dt As DataTable) As String
        Dim stringHolder, meterSerial As String
        For Each row As DataRow In dt.Rows
            For Each column As DataColumn In dt.Columns

                stringHolder = row(column)
                If stringHolder.ToLower.StartsWith("serial no") Then
                    meterSerial = stringHolder.Substring(stringHolder.IndexOf(":") + 1, ((stringHolder.Length - 1) - stringHolder.IndexOf(":"))).Trim
                    Return meterSerial
                End If
            Next
        Next
        Return String.Empty
    End Function

    Private Shared Sub PopulateMpan(ByRef dt As DataTable, ByVal mpan As String)
        For Each row As DataRow In dt.Rows
            row("MPAN") = mpan
        Next
    End Sub

    Private Shared Sub PopulateMeterSerial(ByRef dt As DataTable, ByVal meterSerial As String)
        For Each row As DataRow In dt.Rows
            row("MeterSerial") = meterSerial
        Next
    End Sub

    Private Shared Sub PopulateForceKwh(ByRef dt As DataTable)
        For Each row As DataRow In dt.Rows
            row("ForcekWh") = "Yes"
        Next
    End Sub

End Class
