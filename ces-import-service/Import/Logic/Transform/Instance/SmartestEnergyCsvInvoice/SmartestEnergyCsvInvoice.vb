﻿Public Class SmartestEnergyCsvInvoice
    Implements ITransformDelegate
    Private _returnTable As DataTable
    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
 'load csv file
        Dim csvFile As New DelimitedFile(fullPath, 0)
        csvFile.HasHeader = False
        csvFile.FieldsAreQuoted = True
        csvFile.TrimWhiteSpace = True
        csvFile.Load(_returnTable)
    End Sub

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property


    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Dim SmartestEnergyCsvInvoiceSchema As New SmartestEnergyCsvInvoiceSchema
        SmartestEnergyCsvInvoiceSchema.ToDeFormat(_returnTable)
        For Each row As DataRow In _returnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub


End Class

