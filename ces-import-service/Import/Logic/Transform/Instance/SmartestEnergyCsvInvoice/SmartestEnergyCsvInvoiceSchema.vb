﻿Public Class SmartestEnergyCsvInvoiceSchema
    Public Sub ToDeFormat(ByRef dt As DataTable)
        RetainColumnsWithInfo(dt)
        DataTableTools.RemoveBlankColumns(dt)
        DataTableTools.RemoveBlankRows(dt)
        DataTableTools.SetHeaders(dt, 1)

        Dim invoiceTbl As DataTable = DeFormatTableTemplates.CreateInvoiceTable
        CreateInvoiceEntries(dt, invoiceTbl)
        dt = invoiceTbl
        RenameColumns(dt)
    End Sub
    Private Shared Sub CreateInvoiceEntries(ByRef dt As DataTable, ByRef invoiceDataTable As DataTable)
        For Each dtRow As DataRow In dt.Rows
            Dim net, vat As Double
            Dim invoiceTableRow As DataRow = invoiceDataTable.NewRow
            invoiceTableRow("Invoice Number") = dtRow("Invoice Number")
            Dim MPAN As String = Decimal.Parse(dtRow("Supply"), Globalization.NumberStyles.Float)
            invoiceTableRow("MPAN") = MPAN
            invoiceTableRow("Rate") = dtRow("Charge Description")
            invoiceTableRow("Description") = invoiceTableRow("Rate")
            invoiceTableRow("TaxPointDate") = dtRow("Invoice Date")
            invoiceTableRow("Start Date") = dtRow("Charge Period Start Date")
            invoiceTableRow("End Date") = dtRow("Charge Period End Date")
            invoiceTableRow("Consumption") = dtRow("Quantity")
            vat = dtRow("VAT Amount").ToString.Replace("�", "")
            net = dtRow("Charge Amount").ToString.Replace("�", "")
            invoiceTableRow("Vat") = vat
            invoiceTableRow("Net") = net

            invoiceTableRow("Gross") = vat + net


            Dim ChargeUnit As String = dtRow("Charge Unit")

            If ChargeUnit = "�/kWh" Then
                invoiceTableRow("Force kWh") = "Yes"
            Else
                invoiceTableRow("Force kWh") = "No"
            End If

            Dim CostOnlyFactor As String = invoiceTableRow("Description")
            If CostOnlyFactor.ToLower().StartsWith("energy rate") Then
                invoiceTableRow("Cost Only") = "No"
            Else
                invoiceTableRow("Cost Only") = "Yes"
            End If

            invoiceTableRow("IsLastDayApportion") = "Yes"
            invoiceDataTable.Rows.Add(invoiceTableRow)
        Next
    End Sub
    Private Shared Sub RetainColumnsWithInfo(ByRef dt As DataTable)
        If GetHeadingRow(dt) >= 0 Then
            Dim headingRowIndex As Integer = GetHeadingRow(dt)
            'Remove rows before headers
            DeleteRows(dt, headingRowIndex)
            DataTableTools.RemoveBlankColumns(dt)
        Else
            Throw New ApplicationException("Invalid Format - Headers could not be located")
        End If
    End Sub

    Private Shared Function GetHeadingRow(ByRef dt As DataTable) As Integer
        For Each row As DataRow In dt.Rows
            For Each item As Object In row.ItemArray
                If item.ToString.ToLower.Trim = "invoice number" Then
                    Return dt.Rows.IndexOf(row)
                End If
            Next
        Next
        Return -1
    End Function


    Public Shared Sub DeleteRows(ByRef dt As DataTable, ByRef headerRowIndex As Integer)
        'Pre header rows
        Dim preheaderrows As Integer
        For preheaderrows = 0 To headerRowIndex - 1 Step 1
            dt.Rows(0).Delete()
        Next
        dt.AcceptChanges()
    End Sub

    Public Sub RenameColumns(ByRef dt As DataTable)
        dt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        dt.Columns("Meter Serial").ColumnName = "MeterSerial"
        dt.Columns("Rate").ColumnName = "Tariff"
        dt.Columns("Start Date").ColumnName = "StartDate"
        dt.Columns("End Date").ColumnName = "EndDate"
        dt.Columns("Start Read").ColumnName = "StartRead"
        dt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        dt.Columns("End Read").ColumnName = "EndRead"
        dt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        dt.Columns("Consumption").ColumnName = "Consumption"
        dt.Columns("Force Kwh").ColumnName = "ForceKwh"
        dt.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub
End Class
