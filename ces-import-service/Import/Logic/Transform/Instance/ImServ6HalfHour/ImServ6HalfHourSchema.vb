﻿Imports System.Globalization

Public Class ImServ6HalfHourSchema
    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("MPAN")
        headers.Add("Date(DD/MM/YYYY)")
        headers.Add("00:00")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")
        Return headers
    End Function
    Public Shared Sub ToDEFormat(ByRef dt As DataTable)
        AddColumns(dt)
        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)

        dt = hhDataTable
    End Sub
    Public Shared Sub AddColumns(ByRef dt As DataTable)
        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("MeterSerial")
    End Sub
    ''nikitah:refactoring code
    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim map As New Dictionary(Of String, DataRow)
        Dim iskWH As Boolean = False
        For Each row As DataRow In dt.Rows

            For Each column As DataColumn In dt.Columns
                If iskWH And IsDate(row("Date(DD/MM/YYYY)")) And (column.ColumnName <> "Date(DD/MM/YYYY)") And (column.ColumnName <> "ForcekWh") And (column.ColumnName <> "MPAN") And (column.ColumnName <> "MeterSerial") Then 'Just to prevent errors
                    Dim theDate As DateTime = column.ColumnName
                    Dim theTime As DateTime = theDate.ToString("HH:mm") 'Converts the dateTime to a time value
                    Dim theTimeString = theTime.ToString("HH:mm")
                    Dim key As String = row("Date(DD/MM/YYYY)") & row("MPAN")
                    If map.ContainsKey(key) Then
                        Dim theRow As DataRow = map(key)
                        theRow(theTimeString) = row(theTimeString)
                    Else
                        Dim newRow As DataRow = hhDataTable.NewRow
                        newRow("MeterSerial") = row("MeterSerial")    ' Add the Meter Serial Number
                        newRow("Date(DD/MM/YYYY)") = row("Date(DD/MM/YYYY)")     ' Add the date
                        newRow(theTimeString) = row(theTimeString)      ' And add the Consumption under the correct Time Column in the row.
                        newRow("MPAN") = row("MPAN")
                        hhDataTable.Rows.Add(newRow)

                        map.Add(key, newRow)
                    End If
                Else
                    'To take care of 1st row when it is kWH
                    If row(0) = "kWh" Then
                        iskWH = True
                    Else
                        'When not kWH ignore
                        If row(0) = "kW" Or row(0) = "kVArh" Or row(0) = "kVA" Then
                            iskWH = False
                        End If
                    End If
                    'To ignore row with Date,48 hr slots,MPAN,MeterSerial,ForcekWh
                    If row(0) = "Mprn" Then
                    End If
                End If
            Next

        Next
        dt = hhDataTable
    End Sub
End Class


