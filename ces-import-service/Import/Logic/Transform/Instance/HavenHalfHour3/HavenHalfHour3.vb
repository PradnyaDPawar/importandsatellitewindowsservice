﻿Public Class HavenHalfHour3
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set
    End Property



    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim csvfile As New DelimitedFile(fullPath, 0)
        csvfile.HasHeader = False

        csvfile.Load(_returnTable)
        ' csvfile.LoadNonConsistentTable(_returnTable, HavenNewHalfHourSchema.GetHeaders)

        Dim col As Integer

        For col = _returnTable.Columns.Count - 1 To 50 Step -1
            _returnTable.Columns.RemoveAt(col)


        Next
        '_returnTable.AcceptChanges()


        DataTableTools.RemoveBlankRows(_returnTable)
        DataTableTools.SetHeaders(_returnTable, 4)
        HavenHalfHour3Schema.Validate(_returnTable)
        HavenHalfHour3Schema.ValidateAddtionalHeaders(_returnTable)
        HavenHalfHour3Schema.GetTransformedHeaders(_returnTable)
        HavenHalfHour3Schema.DeleteRows(_returnTable, 4)
    End Sub


    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        For Each row As DataRow In _returnTable.Rows
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
