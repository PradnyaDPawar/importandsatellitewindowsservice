﻿Public Class TotalGpInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable
    Private xSheet As ExcelSheet
    Dim excel As New Excel()

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        excel.Load(fullPath)
        xSheet = excel.Sheets(0)

        If Not excel.GetSheet("Payment Details", xSheet) = True Then

            'Return False

        End If

        xSheet.HeaderRowIndex = 1

        ReturnTable = xSheet.GetDataTable()
    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        TotalGpInvoiceSchema.ToDeFormat(ReturnTable)

        'AMR cost details are held in seperate sheet

        If excel.GetSheet("Additional Charges", xSheet) = True Then
            xSheet.HeaderRowIndex = 1
            Dim otherChargesDt As DataTable = xSheet.GetDataTable()
            TotalGpInvoiceSchema.AddAdditionalCharges(ReturnTable, otherChargesDt)

        End If

        TotalGpInvoiceSchema.PopulateIsLastDayApportion(ReturnTable)
        TotalGpInvoiceSchema.RenameColumns(ReturnTable)

        For Each row As DataRow In ReturnTable.Rows

            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next

    End Sub
End Class
