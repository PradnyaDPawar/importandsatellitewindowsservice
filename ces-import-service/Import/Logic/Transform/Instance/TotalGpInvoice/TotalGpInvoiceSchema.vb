﻿Public Class TotalGpInvoiceSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("Consumer Name")
        headers.Add("Address Lines All")
        headers.Add("Post Code")
        headers.Add("Cust Ref")
        headers.Add("Account Code")
        headers.Add("Invoice Number")
        headers.Add("Print Date")
        headers.Add("Meter Serial Number")
        headers.Add("Meter Point Reference")
        headers.Add("Prev. Read Date")
        headers.Add("Prev. Billed Read")
        headers.Add("Prev. Read Type")
        headers.Add("Month End Read Date")
        headers.Add("Month End read")
        ' Excluding as the name might change  
        ' headers.Add("Mnth Read Type")
        headers.Add("Price Pkwh")
        headers.Add("Units Consumed")
        headers.Add("Correction Factor")
        headers.Add("Calorific Value")
        headers.Add("Consumption Kwh")
        headers.Add("Gas Charge")
        headers.Add("CCL Charge")
        headers.Add("VAT at 5%")
        headers.Add("VAT at 15%")
        headers.Add("VAT at 17.5%")
        headers.Add("VAT at 20%")
        headers.Add("Total VAT")
        headers.Add("Standing Charge")
        headers.Add("All Charges")
        headers.Add("Charge Start Date")
        headers.Add("Charge End Date")

        Return headers

    End Function


    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        Dim invoiceTbl As DataTable = DeFormatTableTemplates.CreateInvoiceTable
        PopulateInvoiceTbl(invoiceTbl, dt)
        dt = invoiceTbl

    End Sub

    Public Shared Sub AddAdditionalCharges(ByRef dt As DataTable, ByVal otherChargesDt As DataTable)

        DataTableTools.RemoveBlankRows(otherChargesDt)

        For Each otherChargeRow As DataRow In otherChargesDt.Rows
            If Not String.IsNullOrEmpty(otherChargeRow("Bill Number").ToString) Then
                PopulateAdditionalCharges(otherChargeRow, dt)
            End If

        Next



    End Sub

    Private Shared Sub PopulateAdditionalCharges(ByVal otherChargeRow As DataRow, ByRef dt As DataTable)

        Dim newDtRow As DataRow = dt.NewRow

        Dim net, otherCharges, ccl, standingCharge As Double

        otherCharges = Val(otherChargeRow("Other Charges").ToString.Replace("£", ""))
        ccl = Val(otherChargeRow("CCL Charge").ToString.Replace("£", ""))
        standingCharge = Val(otherChargeRow("Standing Charges").ToString.Replace("£", ""))

        net = otherCharges + ccl + standingCharge

        newDtRow("Invoice Number") = otherChargeRow("Bill Number")
        newDtRow("MPAN") = Right(otherChargeRow("Desc").ToString, Len(otherChargeRow("Desc").ToString) - otherChargeRow("Desc").ToString.LastIndexOf(" "))

        newDtRow("Start Date") = otherChargeRow("Charge Start Date")
        newDtRow("End Date") = otherChargeRow("Charge End Date")

        If newDtRow("End Date") = "" Then
            If newDtRow("MPAN").ToString.ToLower.Replace(" ", "") = "adjustment" Then
                newDtRow("End Date") = DateAdd(DateInterval.Day, 1, CDate(newDtRow("Start Date"))).ToShortDateString
            End If
        End If

        newDtRow("Force Kwh") = "No"
        newDtRow("Vat") = otherChargeRow("Total VAT").ToString.Replace("£", "")
        newDtRow("Net") = net
        newDtRow("Gross") = otherChargeRow("Total Charges").ToString.Replace("£", "")
        newDtRow("Cost Only") = "Yes"
        newDtRow("Description") = otherChargeRow("Desc")

        dt.Rows.Add(newDtRow)

    End Sub

    Private Shared Sub PopulateInvoiceTbl(ByRef invoiceTbl As DataTable, ByVal dt As DataTable)

        For Each row As DataRow In dt.Rows
            If Not String.IsNullOrEmpty(row("Invoice Number")) Then
                If String.IsNullOrEmpty(row("Charge Start Date")) And String.IsNullOrEmpty(row("Charge End Date")) Then


                    PopulateGasCharge(invoiceTbl, row)
                    PopulateCclCharge(invoiceTbl, row)

                Else

                    PopulateStandingCharge(invoiceTbl, row)

                End If
            End If

        Next


    End Sub

    Private Shared Sub PopulateGasCharge(ByRef invoiceTbl As DataTable, ByVal row As DataRow)

        Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow
        Dim gasVat, gasCharge As Double
        newInvoiceTblRow("Invoice Number") = row("Invoice Number")
        newInvoiceTblRow("Meter Serial") = row("Meter Serial Number")
        newInvoiceTblRow("MPAN") = row("Meter Point Reference")
        newInvoiceTblRow("Rate") = "Gas"
        newInvoiceTblRow("Start Date") = row("Prev. Read Date")
        newInvoiceTblRow("Start Read") = row("Prev. Billed Read")
        newInvoiceTblRow("Start Read Estimated") = GetReadEstimated(row("Prev. Read Type"))
        newInvoiceTblRow("End Date") = row("Month End Read Date")
        newInvoiceTblRow("End Read") = row("Month End read")
        newInvoiceTblRow("End Read Estimated") = GetReadEstimated(row("Mnth Read Type"))
        newInvoiceTblRow("Consumption") = row("Consumption Kwh")
        newInvoiceTblRow("Force Kwh") = "Yes"

        gasCharge = row("Gas Charge").Replace("£", "")
        If Double.TryParse(GetVatCharge(row, "Gas"), gasVat) Then

            newInvoiceTblRow("Vat") = gasVat
            newInvoiceTblRow("Net") = gasCharge
            newInvoiceTblRow("Gross") = gasVat + gasCharge
            newInvoiceTblRow("Cost Only") = "No"
            newInvoiceTblRow("Description") = "Gas Charge"
        Else
            newInvoiceTblRow("Vat") = "Multiple Vat Rates found"
        End If


        invoiceTbl.Rows.Add(newInvoiceTblRow)

    End Sub

    Private Shared Sub PopulateCclCharge(ByRef invoiceTbl As DataTable, ByVal row As DataRow)

        Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow

        Dim cclVat, cclCharge As Double

        newInvoiceTblRow("Invoice Number") = row("Invoice Number")
        newInvoiceTblRow("Meter Serial") = row("Meter Serial Number")
        newInvoiceTblRow("MPAN") = row("Meter Point Reference")
        newInvoiceTblRow("Rate") = ""
        newInvoiceTblRow("Start Date") = row("Prev. Read Date")
        newInvoiceTblRow("Start Read") = ""
        newInvoiceTblRow("Start Read Estimated") = ""
        newInvoiceTblRow("End Date") = row("Month End Read Date")
        newInvoiceTblRow("End Read") = ""
        newInvoiceTblRow("End Read Estimated") = ""
        newInvoiceTblRow("Consumption") = ""
        newInvoiceTblRow("Force Kwh") = "Yes"

        cclCharge = row("CCL charge").Replace("£", "")
        If Double.TryParse(GetVatCharge(row, "CCL"), cclVat) Then
            newInvoiceTblRow("Net") = cclCharge
            newInvoiceTblRow("Vat") = cclVat
            newInvoiceTblRow("Gross") = cclCharge + cclVat
            newInvoiceTblRow("Cost Only") = "Yes"
            newInvoiceTblRow("Description") = "CCL Charge"
        Else
            newInvoiceTblRow("Vat") = "Multiple Vat rates found"
        End If


        invoiceTbl.Rows.Add(newInvoiceTblRow)

    End Sub

    Private Shared Sub PopulateStandingCharge(ByRef invoiceTbl As DataTable, ByVal row As DataRow)

        Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow

        Dim scVat, standingCharge As Double

        newInvoiceTblRow("Invoice Number") = row("Invoice Number")
        newInvoiceTblRow("Meter Serial") = row("Meter Serial Number")
        newInvoiceTblRow("MPAN") = row("Meter Point Reference")
        newInvoiceTblRow("Rate") = ""
        newInvoiceTblRow("Start Date") = row("Charge Start Date")
        newInvoiceTblRow("Start Read") = ""
        newInvoiceTblRow("Start Read Estimated") = ""
        newInvoiceTblRow("End Date") = row("Charge End Date")
        newInvoiceTblRow("End Read") = ""
        newInvoiceTblRow("End Read Estimated") = ""
        newInvoiceTblRow("Consumption") = ""
        newInvoiceTblRow("Force Kwh") = "Yes"

        standingCharge = row("Standing Charge").Replace("£", "")
        If Double.TryParse(GetVatCharge(row, "Standing"), scVat) Then
            newInvoiceTblRow("Net") = standingCharge
            newInvoiceTblRow("Vat") = scVat
            newInvoiceTblRow("Gross") = standingCharge + scVat
            newInvoiceTblRow("Cost Only") = "Yes"
            newInvoiceTblRow("Description") = "Standing Charge"
        Else
            newInvoiceTblRow("Vat") = "Multiple Vat rates found"
        End If



        invoiceTbl.Rows.Add(newInvoiceTblRow)

    End Sub

    Private Shared Function GetVatCharge(ByRef row As DataRow, ByVal vatOf As String) As String

        Dim Charge, Vat As Double

        If CanDetermineVat(row) Then


            Charge = row(vatOf & " Charge").Replace("£", "")


            If row("VAT at 5%").Replace("£", "") <> 0 Then
                Vat = Charge * 0.05

            ElseIf row("VAT at 15%").Replace("£", "") <> 0 Then
                Vat = Charge * 0.15

            ElseIf row("VAT at 17.5%").Replace("£", "") <> 0 Then
                Vat = Charge * 0.175

            Else
                Vat = Charge * 0.2

            End If

            Return Vat

        Else

            Return "Multiple VAT rates found"

        End If

    End Function

    Private Shared Function CanDetermineVat(ByRef row As DataRow) As Boolean

        Dim vat5, vat15, vat17_5, vat20 As Integer

        If row("VAT at 5%").Replace("£", "") <> 0 Then
            vat5 = 1
        End If

        If row("VAT at 15%").Replace("£", "") <> 0 Then
            vat15 = 1
        End If

        If row("VAT at 17.5%").Replace("£", "") <> 0 Then
            vat17_5 = 1
        End If

        If row("VAT at 20%").Replace("£", "") <> 0 Then
            vat20 = 1
        End If

        If (vat5 + vat15 + vat17_5 + vat20) > 1 Then
            Return False
        Else
            Return True
        End If

    End Function

    Private Shared Function GetReadEstimated(ByVal code As String) As String

        If code.Trim = "PA" Then
            Return "A"
        Else
            Return code
        End If

    End Function



    Private Shared Sub PopulateColumns(ByRef dt As DataTable)
        Dim vat, gross As Double

        For Each row As DataRow In dt.Rows

            'Force kWh default to yes
            row("Force Kwh") = "Yes"

            'Calculate Net
            vat = row("Vat")
            gross = row("Gross")
            row("Net") = (gross - vat).ToString("0.00")

            If String.IsNullOrEmpty(row("Charge Start Date")) And String.IsNullOrEmpty(row("Charge End Date")) Then
                row("Cost Only") = "No"

            Else

                row("Cost Only") = "Yes"
            End If
        Next

    End Sub

    Public Shared Sub PopulateIsLastDayApportion(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows
            row("IsLastDayApportion") = "No"
        Next

    End Sub

    Public Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        dt.Columns("Meter Serial").ColumnName = "MeterSerial"
        dt.Columns("Start Date").ColumnName = "StartDate"
        dt.Columns("Start Read").ColumnName = "StartRead"
        dt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        dt.Columns("Rate").ColumnName = "Tariff"
        dt.Columns("End Date").ColumnName = "EndDate"
        dt.Columns("End Read").ColumnName = "EndRead"
        dt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        dt.Columns("Force Kwh").ColumnName = "ForceKwh"
        dt.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub

End Class
