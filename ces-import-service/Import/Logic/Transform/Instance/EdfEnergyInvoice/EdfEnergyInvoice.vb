﻿Partial Public Class EdfEnergyInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable
    Private excel As New Excel()

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        'Dim excel As New Excel()

        excel.Load(fullPath)

        'Dim xSheet As ExcelSheet = excel.Sheets(0)

        'xSheet.HeaderRowIndex = 2

        '_returnTable = xSheet.GetDataTable()

    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Dim edfEnergyInvoiceSchema As New EdfEnergyInvoiceSchema

        edfEnergyInvoiceSchema.ToDeFormat(excel, _returnTable)

        For Each row As DataRow In _returnTable.Rows

            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
