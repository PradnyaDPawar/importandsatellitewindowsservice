﻿Public Class EdfEnergyInvoiceSchema

    Private _paiDt, _ecrDt, _mcDt, _vatDt, _interestDt As DataTable
    Private _mpan, _startDate, _endDate, _invoiceRef, _childAcc As String


    'Property Account Charges sheet
    Property PaiDt() As DataTable
        Get
            Return _paiDt
        End Get
        Set(ByVal value As DataTable)
            _paiDt = value
        End Set
    End Property
    'Energy Charges Sheet
    Property EcrDt() As DataTable
        Get
            Return _ecrDt
        End Get
        Set(ByVal value As DataTable)
            _ecrDt = value
        End Set
    End Property
    ' Miscellaneous charges sheet
    Property McDt() As DataTable
        Get
            Return _mcDt
        End Get
        Set(ByVal value As DataTable)
            _mcDt = value
        End Set
    End Property

    'Vat Details Sheet
    Property VatDt() As DataTable
        Get
            Return _vatDt
        End Get
        Set(ByVal value As DataTable)
            _vatDt = value
        End Set
    End Property

    Property InterestDt() As DataTable
        Get
            Return _interestDt
        End Get
        Set(ByVal value As DataTable)
            _interestDt = value
        End Set
    End Property

    Property Mpan() As String
        Get
            Return _mpan
        End Get
        Set(ByVal value As String)
            _mpan = value
        End Set
    End Property

    Property StartDate() As String
        Get
            Return _startDate
        End Get
        Set(ByVal value As String)
            _startDate = value
        End Set
    End Property

    Property EndDate() As String
        Get
            Return _endDate
        End Get
        Set(ByVal value As String)
            _endDate = value
        End Set
    End Property


    Property InvoiceRef() As String
        Get
            Return _invoiceRef
        End Get
        Set(ByVal value As String)
            _invoiceRef = value
        End Set
    End Property

    Property ChildAcc() As String
        Get
            Return _childAcc
        End Get
        Set(ByVal value As String)
            _childAcc = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef excel As Excel, ByRef returnTable As DataTable)
            Dim xSheet As ExcelSheet = excel.Sheets(0)

            'Assign to property
            PaiDt = SheetToDt(excel, xSheet, "Property Account Information", 2)
            PaiDt.Columns.Add("HasCostOnly")
            EcrDt = SheetToDt(excel, xSheet, "Energy Charges Report", 2)
            McDt = SheetToDt(excel, xSheet, "Miscellaneous Charges", 2)
            VatDt = SheetToDt(excel, xSheet, "Vat Charges", 2)
            InterestDt = SheetToDt(excel, xSheet, "Penalty Interest", 2)

            'Get DeFormat Invoice Table
            Dim InvoiceDt As DataTable = DeFormatTableTemplates.CreateInvoiceTable()

            'Fetch Start Date  from EcrDt
            AssociateStartDate(EcrDt, PaiDt)

            'There could be multiple invoices per file. But the invoice number and date range is not repeated for each row of the file,
            'so associate these with each row
            AssociateInvoiceNumber(PaiDt)

            PopulateInvoiceDt(InvoiceDt)

            RenameColumns(InvoiceDt)

            PopulateIsLastDayApportion(InvoiceDt)

            returnTable = InvoiceDt
    End Sub

    Private Sub AssociateStartDate(ByRef ecrDt As DataTable, ByRef paiDt As DataTable)
        Dim dt As Date
        paiDt.Columns.Add("Period Start")

        For Each row As DataRow In paiDt.Rows

            If Not String.IsNullOrEmpty(row("Child Account")) And Not String.IsNullOrEmpty(row("Invoice Reference")) Then

                Dim resultRows() As DataRow = ecrDt.Select("[Child] = '" & row("Child Account") & "'" & _
                                                                  "And [Invoice1] Like '%" & CType(row("Invoice Reference"), Long) & "'")

                If resultRows.Count > 0 Then
                    dt = Date.Parse(row("Period End"))
                    row("Period Start") = (New Date(dt.Year, dt.Month, 1)).ToShortDateString()

                End If

            End If

        Next

    End Sub

    Private Sub AssociateInvoiceNumber(ByRef paiDt As DataTable)
        Dim dt As Date
        For Each row As DataRow In paiDt.Rows

            If String.IsNullOrEmpty(row("Child Account")) And String.IsNullOrEmpty(row("Invoice Reference")) Then
                row("Child Account") = ChildAcc
                row("Invoice Reference") = InvoiceRef
                row("Period Start") = StartDate
                row("Period End") = EndDate
                row("HasCostOnly") = "No"
            Else
                ChildAcc = row("Child Account")
                InvoiceRef = row("Invoice Reference")
                dt = Date.Parse(row("Period End"))
                EndDate = (New Date(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month))).ToShortDateString()
                StartDate = (New Date(dt.Year, dt.Month, 1)).ToShortDateString()
                row("HasCostOnly") = "Yes"
            End If


        Next


    End Sub

    Private Sub PopulateInvoiceDt(ByRef invoiceDt As DataTable)

        For Each row As DataRow In PaiDt.Rows

            PopulateConsumptionRows(row, invoiceDt, "")
            PopulateConsumptionRows(row, invoiceDt, "1")
            PopulateConsumptionRows(row, invoiceDt, "11")
            PopulateConsumptionRows(row, invoiceDt, "111")
            PopulateConsumptionRows(row, invoiceDt, "1111")
            PopulateConsumptionRows(row, invoiceDt, "11111")
            PopulateConsumptionRows(row, invoiceDt, "111111")
            PopulateConsumptionRows(row, invoiceDt, "1111111")

            If row("HasCostOnly") = "Yes" Then

                PopulateCcl(invoiceDt, row)

                PopulateMiscDtCharges(invoiceDt, row)

                PopulateVatDtCharges(invoiceDt, row)

                If InterestDt.Rows.Count > 0 Then
                    PopulateInterestDtCharges(invoiceDt, row)
                End If

            End If

            Mpan = ""
            StartDate = ""
            EndDate = ""

        Next

        'Need to loop again to avoid duplicating some of the cost only entries being created when data is presented in multiple rows for same mpan.

        For Each row As DataRow In PaiDt.Rows

            If row("HasCostOnly") = "Yes" Then

                Mpan = (Long.Parse(row("MPAN Core"))).ToString()
                StartDate = row("Period Start")
                EndDate = row("Period End")
                PopulateEnergyDtCharges(invoiceDt, row)

            End If


        Next


    End Sub

    Private Sub PopulateCcl(ByRef invoiceDt As DataTable, ByVal row As DataRow)

        Dim dt As Date
        'CCL Charge
        If row.Table.Columns.Contains("CCL Units") Then

            Dim newInvoiceDtRow As DataRow = invoiceDt.NewRow
            newInvoiceDtRow("Invoice Number") = row("Child Account") & "-" & CType(row("Invoice Reference"), Long)
            newInvoiceDtRow("MPAN") = Mpan
            newInvoiceDtRow("Consumption") = row("CCL Units")
            newInvoiceDtRow("Gross") = row("CCL Amount")
            newInvoiceDtRow("Force kWh") = "No"
            newInvoiceDtRow("Cost Only") = "Yes"
            newInvoiceDtRow("Description") = "CCL"
            dt = Date.Parse(row("Period End"))
            EndDate = (New Date(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month))).ToShortDateString()
            StartDate = (New Date(dt.Year, dt.Month, 1)).ToShortDateString()
            newInvoiceDtRow("End Date") = (New Date(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month))).ToShortDateString()
            newInvoiceDtRow("Start Date") = (New Date(dt.Year, dt.Month, 1)).ToShortDateString()
            invoiceDt.Rows.Add(newInvoiceDtRow)

        End If

    End Sub


    Private Sub PopulateConsumptionRows(ByVal row As DataRow, ByRef invoiceDt As DataTable, ByVal charge As String)
        Dim dt As Date
        Dim units, price As Double

        'Consumption Rows
        If row.Table.Columns.Contains("Units" & charge) Then

            If Not String.IsNullOrEmpty(row("Units" & charge)) Then


                Dim newInvoiceDtRow As DataRow = invoiceDt.NewRow
                newInvoiceDtRow("Invoice Number") = row("Child Account") & "-" & CType(row("Invoice Reference"), Long)
                Mpan = (Long.Parse(row("MPAN Core"))).ToString()
                newInvoiceDtRow("MPAN") = Mpan
                newInvoiceDtRow("Rate") = row("Rate" & charge)
                newInvoiceDtRow("Start Read") = row("Previous Readings" & charge)
                newInvoiceDtRow("End Read") = row("Current Readings" & charge)
                newInvoiceDtRow("Consumption") = row("Units" & charge)
                units = row("Units" & charge)
                price = row("Price" & charge)
                newInvoiceDtRow("Gross") = Math.Round(units * price, 2)
                newInvoiceDtRow("Force kWh") = "Yes"
                newInvoiceDtRow("Description") = row("Rate" & charge)
                If newInvoiceDtRow("Description") = "AMBER (D)" Or newInvoiceDtRow("Description") = "GREEN (D)" Or newInvoiceDtRow("Description") = "RED (D)" Then
                    newInvoiceDtRow("Cost Only") = "Yes"
                Else
                    newInvoiceDtRow("Cost Only") = "No"
                End If
                dt = Date.Parse(row("Period End"))
                newInvoiceDtRow("End Date") = (New Date(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month))).ToShortDateString()
                newInvoiceDtRow("Start Date") = (New Date(dt.Year, dt.Month, 1)).ToShortDateString()
                invoiceDt.Rows.Add(newInvoiceDtRow)

                StartDate = row("Period Start")
                EndDate = row("Period End")

            End If

            End If



    End Sub


    Private Sub PopulateEnergyDtCharges(ByRef invoiceDt As DataTable, ByVal paiRow As DataRow)


        Dim invoiceRows() As DataRow = EcrDt.Select("[Child] = '" & paiRow("Child Account") & "'" & _
                                                    "And CONVERT([Invoice1],System.Int64) = '" & CType(paiRow("Invoice Reference"), Long) & "'")
      

        For Each row As DataRow In invoiceRows

            If row("Element Type").ToString.ToLower.Trim = "unit" Then

                Dim resultRows() As DataRow = invoiceDt.Select("[Invoice Number] = '" & row("Child") & "-" & CType(row("Invoice1"), Long) & "'" _
                                                               & "And [Rate] LIKE '%" & row("Rate Description").Trim & "%'")

                If resultRows.Count = 0 Then

                    Dim newInvoiceDtRow As DataRow = invoiceDt.NewRow
                    newInvoiceDtRow("Invoice Number") = row("Child") & "-" & CType(row("Invoice1"), Long)
                    newInvoiceDtRow("MPAN") = Mpan
                    ' newInvoiceDtRow("Rate") = row("Charge Description")
                    newInvoiceDtRow("Start Date") = StartDate
                    newInvoiceDtRow("End Date") = EndDate
                    newInvoiceDtRow("Consumption") = row("Charged Units")
                    newInvoiceDtRow("Gross") = row("Amount")
                    newInvoiceDtRow("Force kWh") = "No"
                    newInvoiceDtRow("Description") = row("Charge Description")
                    If newInvoiceDtRow("Description") = "AMBER (D)" Or newInvoiceDtRow("Description") = "GREEN (D)" Or newInvoiceDtRow("Description") = "RED (D)" Then
                        newInvoiceDtRow("Cost Only") = "Yes"
                    Else
                        newInvoiceDtRow("Cost Only") = "No"
                    End If
                    invoiceDt.Rows.Add(newInvoiceDtRow)

                End If

            Else

                Dim newInvoiceDtRow As DataRow = invoiceDt.NewRow
                newInvoiceDtRow("Invoice Number") = row("Child") & "-" & CType(row("Invoice1"), Long)
                newInvoiceDtRow("MPAN") = Mpan
                ' newInvoiceDtRow("Rate") = row("Charge Description")
                newInvoiceDtRow("Start Date") = StartDate
                newInvoiceDtRow("End Date") = EndDate
                newInvoiceDtRow("Consumption") = row("Charged Units")
                newInvoiceDtRow("Gross") = row("Amount")
                newInvoiceDtRow("Force kWh") = "No"
                newInvoiceDtRow("Cost Only") = "Yes"
                newInvoiceDtRow("Description") = row("Charge Description")
                invoiceDt.Rows.Add(newInvoiceDtRow)

            End If


        Next


    End Sub


    Private Sub PopulateMiscDtCharges(ByRef invoiceDt As DataTable, ByVal paiRow As DataRow)

        If McDt.Rows.Count > 0 Then


            Dim invoiceRows() As DataRow = McDt.Select("[Child] = '" & paiRow("Child Account") & "'" & _
                                                       "And CONVERT([Invoice1],System.Int64) = '" & CType(paiRow("Invoice Reference"), Long) & "'")

           
            For Each row As DataRow In invoiceRows

                Dim newInvoiceDtRow As DataRow = invoiceDt.NewRow
                newInvoiceDtRow("Invoice Number") = row("Child") & "-" & CType(row("Invoice1"), Long)
                newInvoiceDtRow("MPAN") = Mpan
                ' newInvoiceDtRow("Rate") = row("Description")
                newInvoiceDtRow("Start Date") = StartDate
                newInvoiceDtRow("End Date") = EndDate
                'newInvoiceDtRow("Consumption") = row("Charged Units")
                newInvoiceDtRow("Gross") = row("Amount")
                newInvoiceDtRow("Force kWh") = "No"
                newInvoiceDtRow("Cost Only") = "Yes"
                newInvoiceDtRow("Description") = row("Decription")
                invoiceDt.Rows.Add(newInvoiceDtRow)

            Next

        End If

    End Sub

    Private Sub PopulateVatDtCharges(ByRef invoiceDt As DataTable, ByVal paiRow As DataRow)

        Dim invoiceRows() As DataRow = VatDt.Select("[Child] = '" & paiRow("Child Account") & "'" & _
                                                    "And CONVERT([Invoice1],System.Int64) = '" & CType(paiRow("Invoice Reference"), Long) & "'")
        For Each row As DataRow In invoiceRows

            Dim newInvoiceDtRow As DataRow = invoiceDt.NewRow
            newInvoiceDtRow("Invoice Number") = row("Child") & "-" & CType(row("Invoice1"), Long)
            newInvoiceDtRow("MPAN") = Mpan
            ' newInvoiceDtRow("Rate") = row("Description")
            newInvoiceDtRow("Start Date") = StartDate
            newInvoiceDtRow("End Date") = EndDate
            'newInvoiceDtRow("Consumption") = row("Charged Units")
            newInvoiceDtRow("Gross") = row("Vat")
            newInvoiceDtRow("Force kWh") = "No"
            newInvoiceDtRow("Cost Only") = "Yes"
            newInvoiceDtRow("Description") = "Vat - " & row("Vat Description")
            invoiceDt.Rows.Add(newInvoiceDtRow)

        Next

    End Sub

    Private Sub PopulateInterestDtCharges(ByRef invoiceDt As DataTable, ByVal paiRow As DataRow)

        Dim invoiceRows() As DataRow = InterestDt.Select("[Child] = '" & paiRow("Child Account") & "'" & _
                                                    "And CONVERT([Invoice1],System.Int64) = '" & CType(paiRow("Invoice Reference"), Long) & "'")
      
        For Each row As DataRow In invoiceRows

            Dim newInvoiceDtRow As DataRow = invoiceDt.NewRow
            newInvoiceDtRow("Invoice Number") = row("Child") & "-" & CType(row("Invoice1"), Long)
            newInvoiceDtRow("MPAN") = Mpan
            ' newInvoiceDtRow("Rate") = row("Description")
            newInvoiceDtRow("Start Date") = StartDate
            newInvoiceDtRow("End Date") = EndDate
            'newInvoiceDtRow("Consumption") = row("Charged Units")
            newInvoiceDtRow("Gross") = row("Due Amount")
            newInvoiceDtRow("Force kWh") = "No"
            newInvoiceDtRow("Cost Only") = "Yes"
            newInvoiceDtRow("Description") = "Penalty Interest for " & row("Days outstanding") & " outstanding days"
            invoiceDt.Rows.Add(newInvoiceDtRow)

        Next

    End Sub




    Private Function SheetToDt(ByRef excel As Excel, ByRef xSheet As ExcelSheet, ByVal sheetName As String, ByVal headerRowIndex As Integer) As DataTable
        Dim tempDt As New DataTable

        If excel.GetSheet(sheetName, xSheet) Then

            xSheet.HeaderRowIndex = headerRowIndex

            tempDt = xSheet.GetDataTable()

            tempDt.Rows.RemoveAt(0)

            '  DataTableTools.RemoveBlankColumns(tempDt)

            DataTableTools.RemoveBlankRows(tempDt)

        End If

        Return tempDt

    End Function

    Private Sub RenameColumns(InvoiceDt As DataTable)
        InvoiceDt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        InvoiceDt.Columns("Meter Serial").ColumnName = "MeterSerial"
        InvoiceDt.Columns("Rate").ColumnName = "Tariff"
        InvoiceDt.Columns("Start Date").ColumnName = "StartDate"
        InvoiceDt.Columns("End Date").ColumnName = "EndDate"
        InvoiceDt.Columns("Start Read").ColumnName = "StartRead"
        InvoiceDt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        InvoiceDt.Columns("End Read").ColumnName = "EndRead"
        InvoiceDt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        InvoiceDt.Columns("Force kWh").ColumnName = "ForcekWh"
        InvoiceDt.Columns("Cost Only").ColumnName = "CostOnly"


    End Sub

    Private Sub PopulateIsLastDayApportion(InvoiceDt As DataTable)

        For Each row As DataRow In InvoiceDt.Rows
            row("IsLastDayApportion") = "No"
        Next

    End Sub


End Class
