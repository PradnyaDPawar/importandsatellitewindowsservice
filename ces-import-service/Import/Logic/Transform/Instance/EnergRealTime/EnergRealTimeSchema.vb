﻿Public Class EnergRealTimeSchema

    Public Shared Sub ToDeFormat(ByRef deTable As DataTable)

        ' Since the time stamp in the raw format gives the end time and date of the period.
        ' So making it start time and date by reducing it half hour.

        For Each row As DataRow In deTable.Rows

            Dim timestamp As String = row("Timestamp").ToString()
            Dim dt As DateTime = DateTime.MinValue

            If DateTime.TryParse(timestamp, dt) Then

                dt = dt.AddMinutes(-30)
                timestamp = dt.ToString("dd/MM/yyyy HH:mm").Replace("-", "/")
                row("timestamp") = timestamp

            Else
                Throw New Exception("Transpose failed.")

            End If

        Next

        ' Renaming column as DeFormat
        deTable.Columns("MeterName").ColumnName = "MeterName"
        deTable.Columns("Timestamp").ColumnName = "DateTime"
        deTable.Columns("Value").ColumnName = "CountUnits"

    End Sub

End Class