﻿Public Class SseInvoice3Schema
    Public Shared Function GetHeaders() As List(Of String)
        ' Create a List of headers according to excel sheet
        Return New List(Of String)() From {"Charge Period Start Date",
                                           "Charge Period End Date",
                                           "Invoice Number",
                                           "Invoice Date",
                                           "Site Name",
                                           "Site Ref",
                                           "Top Line",
                                           "Supply",
                                           "Charge Type",
                                           "Charge Description",
                                           "Days",
                                           "Quantity",
                                           "Unit",
                                           "Rate",
                                           "Charge Unit",
                                           "Charge Amount",
                                           "VAT Code",
                                           "VAT Rate",
                                           "VAT Amount"}
    End Function
    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        DataTableTools.RemoveBlankRows(dt)
        DataTableTools.RemoveTopRows(dt, 6)
        Dim standardInvoiceTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        For Each currentDataRow As DataRow In dt.Rows
            Dim newInvoiceRow As DataRow = standardInvoiceTable.NewRow
            newInvoiceRow("Start Date") = currentDataRow("Charge Period Start Date")
            newInvoiceRow("End Date") = currentDataRow("Charge Period End Date")
            newInvoiceRow("Invoice Number") = currentDataRow("Invoice Number")
            newInvoiceRow("TaxPointDate") = currentDataRow("Invoice Date")
            newInvoiceRow("MPAN") = currentDataRow("Supply")
            newInvoiceRow("Rate") = currentDataRow("Charge Description")
            newInvoiceRow("Description") = currentDataRow("Charge Description")
            newInvoiceRow("Consumption") = currentDataRow("Quantity")
            SetForceKwhCostOnly(newInvoiceRow, currentDataRow("Charge Description"), currentDataRow("Unit"))
            newInvoiceRow("InvoiceRate") = currentDataRow("Rate")
            newInvoiceRow("CostUnit") = currentDataRow("Charge Unit").ToString().Replace("�", "£")
            Dim net, vat As Double
            net = currentDataRow("Charge Amount").ToString().Replace("�", "")
            newInvoiceRow("Net") = net.ToString()
            vat = currentDataRow("VAT Amount").ToString().Replace("�", "")
            newInvoiceRow("Vat") = vat.ToString()
            newInvoiceRow("Gross") = (net + vat).ToString()
            newInvoiceRow("IsLastDayApportion") = "Yes"
            standardInvoiceTable.Rows.Add(newInvoiceRow)
        Next
        RenameColumns(standardInvoiceTable)
        dt = standardInvoiceTable
        Return dt
    End Function
    Private Shared Sub SetForceKwhCostOnly(ByRef invoiceRow As DataRow, ByVal changeDescription As String, ByVal unit As String)
        If changeDescription.Equals("Energy Rate (Metered Cons)") OrElse changeDescription.Equals("Energy Rate (Dist. Losses)") OrElse changeDescription.Equals("Energy Rate (Tran. Losses)") Then
            invoiceRow("Force kWh") = "yes"
            invoiceRow("Cost Only") = "no"
        ElseIf unit.Equals("m3") OrElse unit.Equals("cum") OrElse unit.Equals("cft") Then
            invoiceRow("Force kWh") = "no"
            invoiceRow("Cost Only") = "yes"
        Else
            invoiceRow("Force kWh") = "no"
            invoiceRow("Cost Only") = "yes"
        End If
    End Sub
    Private Shared Sub RenameColumns(ByRef invoiceTable As DataTable)
        invoiceTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceTable.Columns("Rate").ColumnName = "Tariff"
        invoiceTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceTable.Columns("End Date").ColumnName = "EndDate"
        invoiceTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceTable.Columns("End Read").ColumnName = "EndRead"
        invoiceTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceTable.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub
End Class
