﻿Imports System.Globalization

Public Class VestigoHalfHourSchema

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)
        dt = hhDataTable

    End Sub

    Public Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim dateholder, datetimeHolder As String
        Dim time As String
        Dim success As Boolean = False

        For Each row As DataRow In dt.Rows

            If Not String.IsNullOrEmpty(row(1)) Then

                datetimeHolder = row(1)
                time = datetimeHolder.Substring(datetimeHolder.Length - 4, 4)
                dateholder = datetimeHolder.Substring(0, 8)
                dateholder = DateTime.ParseExact(dateholder, "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString()

                Dim timestring = time.Substring(0, 2) + ":" + time.Substring(time.Length - 2, 2)

                Dim serialToCompare As String
                Select Case row(3)
                    Case 9
                        serialToCompare = row(2) + "_kVArh"
                    Case 10
                        serialToCompare = row(2) + "_kWh Export"
                    Case 11
                        serialToCompare = row(2) + "_kVArh Export"
                    Case Else
                        serialToCompare = row(2)
                End Select

                For Each hhrow As DataRow In hhDataTable.Rows

                    If hhrow("Date(dd/mm/yyyy)") = dateholder And hhrow("MeterSerial") = serialToCompare Then
                        hhrow(timestring) = row(0)
                        If row(3) = 1 Then
                            hhrow("ForcekWh") = "Yes"
                        Else
                            hhrow("ForcekWh") = "No"
                        End If
                        success = True
                    End If

                Next

                If Not success Then

                    Dim newRow As DataRow = hhDataTable.NewRow

                    newRow("Date(dd/mm/yyyy)") = dateholder

                    newRow(timestring) = row(0)
                    If row(3) = 1 Then
                        newRow("ForcekWh") = "Yes"
                    Else
                        newRow("ForcekWh") = "No"
                    End If


                    newRow("MeterSerial") = serialToCompare

                    hhDataTable.Rows.Add(newRow)

                End If

            End If

            success = False

        Next

    End Sub

End Class
