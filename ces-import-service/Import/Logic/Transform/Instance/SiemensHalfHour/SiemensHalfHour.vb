﻿Public Class SiemensHalfHour
    Implements ITransformDelegate



    Private _returnTable As DataTable


    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvFile As New DelimitedFile(fullPath, 0)
        csvFile.Load(_returnTable, GetHeaders())
    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        AddColumns()

        For Each row As DataRow In _returnTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            FillForceKwh(row)
            If IsDataRow(row) Then
                FE_HalfHour.Fill(row, dsEdits, importEdit)
            End If
        Next
    End Sub
End Class
