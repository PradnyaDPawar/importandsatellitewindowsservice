﻿Partial Public Class SiemensHalfHour

    Public Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("H1")
        headers.Add("MPAN")
        headers.Add("Channel number")
        headers.Add("Date(DD/MM/YYYY)")
        headers.Add("H2")
        headers.Add("H3")
        headers.Add("H4")
        headers.Add("Unit")
        headers.Add("I/E")
        headers.Add("H5")

        headers.Add("00:00")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")

        headers.Add("Flag1")
        headers.Add("Flag2")
        headers.Add("Flag3")
        headers.Add("Flag4")
        headers.Add("Flag5")
        headers.Add("Flag6")
        headers.Add("Flag7")
        headers.Add("Flag8")
        headers.Add("Flag9")
        headers.Add("Flag10")
        headers.Add("Flag11")
        headers.Add("Flag12")
        headers.Add("Flag13")
        headers.Add("Flag14")
        headers.Add("Flag15")
        headers.Add("Flag16")
        headers.Add("Flag17")
        headers.Add("Flag18")
        headers.Add("Flag19")
        headers.Add("Flag20")
        headers.Add("Flag21")
        headers.Add("Flag22")
        headers.Add("Flag23")
        headers.Add("Flag24")
        headers.Add("Flag25")
        headers.Add("Flag26")
        headers.Add("Flag27")
        headers.Add("Flag28")
        headers.Add("Flag29")
        headers.Add("Flag30")
        headers.Add("Flag31")
        headers.Add("Flag32")
        headers.Add("Flag33")
        headers.Add("Flag34")
        headers.Add("Flag35")
        headers.Add("Flag36")
        headers.Add("Flag37")
        headers.Add("Flag38")
        headers.Add("Flag39")
        headers.Add("Flag40")
        headers.Add("Flag41")
        headers.Add("Flag42")
        headers.Add("Flag43")
        headers.Add("Flag44")
        headers.Add("Flag45")
        headers.Add("Flag46")
        headers.Add("Flag47")
        headers.Add("Flag48")

        Return headers

    End Function

    Public Sub AddColumns()
        _returnTable.Columns.Add("MeterSerial")
        _returnTable.Columns.Add("ForcekWh")
    End Sub

    Private Sub FillForceKwh(ByRef row As DataRow)
        If row("Unit").ToString.ToLower.Trim = "kwh" Then
            row("ForcekWh") = "Yes"
        End If
    End Sub

    Public Function IsDataRow(ByRef row As DataRow) As Boolean
        'Populate Force kWh and delete kvarh rows
        If row("Unit").ToString.ToLower.Trim <> "kvarh" Then
            Return True
        Else
            row.Delete()
            Return False
        End If
    End Function

End Class
