﻿Public Class ScottishPowerInvoice
    Implements ITransformDelegate

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Try
            Dim excel As New Excel()
            excel.Load(fullPath)
            Dim xSheet As ExcelSheet = excel.Sheets(0)

            If Not excel.GetSheet("Sheet4", xSheet) = True Then

            End If
            xSheet.HeaderRowIndex = 1
            ReturnTable = xSheet.GetDataTable()
        Catch ex As Exception

        End Try
    End Sub

    Private _returnTable As DataTable
    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        Dim invoiceDt As DataTable = DeFormatTableTemplates.CreateInvoiceTable

        'Get all the different invoices 

        Dim invoicesList As List(Of String) = GetInvoicesList(ReturnTable)

        For Each invoice As String In invoicesList

            If Not String.IsNullOrEmpty(invoice) Then

                Dim resultrows() As DataRow

                Dim invoiceNumber, billNo As String
                invoiceNumber = invoice.Substring(0, invoice.IndexOf("~"))
                billNo = invoice.Substring(invoice.IndexOf("~") + 1)
                resultrows = ReturnTable.Select("[Invoice Number] = '" & invoiceNumber & "' And [Bill Sequence No.] = '" & billNo & "'")

                'Populate Properties
                If resultrows.Count > 0 Then
                    PopulateProperties(resultrows, invoiceDt)
                End If

            End If

        Next

        ReturnTable = invoiceDt
        GetTransformedHeaders()
        For Each row As DataRow In _returnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
