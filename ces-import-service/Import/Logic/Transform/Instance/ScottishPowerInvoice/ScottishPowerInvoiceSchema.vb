﻿Partial Public Class ScottishPowerInvoice

    Private _invoiceNumber, _mpan, _meterSerial, _startDate, _endDate, _startRead, _endRead, _startReadEst, _endReadEstimated, _
            _consumption, _net, _vat, _gross, _forceKwh, _costOnly, _description, _invoiceDate As String

    Private _rate As String = "Default"

    Private _ccl As Double
#Region "Properties"


    Property InvoiceNumber() As String
        Get
            Return _invoiceNumber
        End Get
        Set(ByVal value As String)
            _invoiceNumber = value
        End Set
    End Property


    Property Mpan() As String
        Get
            Return _mpan
        End Get
        Set(ByVal value As String)
            _mpan = Right(value.Trim, 13)
        End Set
    End Property

    Property MeterSerial() As String
        Get
            Return _meterSerial
        End Get
        Set(ByVal value As String)
            _meterSerial = value
        End Set
    End Property

    Property StartDate() As String
        Get
            Return _startDate
        End Get
        Set(ByVal value As String)
            _startDate = value
        End Set
    End Property

    Property EndDate() As String
        Get
            Return _endDate
        End Get
        Set(ByVal value As String)
            _endDate = value
        End Set
    End Property

    Property InvoiceDate() As String
        Get
            Return _invoiceDate
        End Get
        Set(value As String)
            _invoiceDate = value
        End Set
    End Property
    Property StartRead() As String
        Get
            Return _startRead
        End Get
        Set(ByVal value As String)
            _startRead = value
        End Set
    End Property

    Property EndRead() As String
        Get
            Return _endRead
        End Get
        Set(ByVal value As String)
            _endRead = value
        End Set
    End Property


    Property StartReadEstimated() As String
        Get
            Return GetReadingType(_startReadEst)
        End Get
        Set(ByVal value As String)
            _startReadEst = value
        End Set
    End Property

    Property EndReadEstimated() As String
        Get
            Return GetReadingType(_endReadEstimated)
        End Get
        Set(ByVal value As String)
            _endReadEstimated = value
        End Set
    End Property

    Property Consumption() As String
        Get
            Return _consumption
        End Get
        Set(ByVal value As String)
            _consumption = value
        End Set
    End Property

    ReadOnly Property Rate() As String
        Get
            Return _rate
        End Get

    End Property

    Property Net() As String
        Get
            Return _net
        End Get
        Set(ByVal value As String)
            _net = value
        End Set
    End Property

    Property Vat() As String
        Get
            Return _vat
        End Get
        Set(ByVal value As String)
            _vat = value
        End Set
    End Property

    Property Gross() As String
        Get
            Return _gross
        End Get
        Set(ByVal value As String)
            _gross = value
        End Set
    End Property

    Property ForceKwh() As String
        Get
            Return _forceKwh
        End Get
        Set(ByVal value As String)
            _forceKwh = value
        End Set
    End Property

    Property CostOnly() As String
        Get
            Return _costOnly
        End Get
        Set(ByVal value As String)
            _costOnly = value
        End Set
    End Property


    Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    Property Ccl() As String
        Get
            Return _ccl
        End Get
        Set(ByVal value As String)
            _ccl = value
        End Set
    End Property
#End Region

    Private Function GetReadingType(ByVal readCode As String) As String

        Select Case readCode

            Case "00"
                ' Normal
                Return "A"

            Case "01"
                ' Estimated Manual
                Return "E"

            Case "02"
                ' Estimated Computer
                Return "E"

            Case "03"
                ' Removed Meter
                Return "A"

            Case "04"
                ' Customers Own
                Return "A"

            Case "05"
                ' Computer
                Return "E"

            Case "06"
                ' Exchange Meter
                Return "A"

            Case "07"
                ' Connect Meter
                Return "A"

            Case "08"
                ' No Reading Available	
                Return "E"

            Case "09"
                ' 3rd Party Normal	
                Return "A"

            Case "10"
                ' 3rd Party Normal Manual
                Return "A"

            Case "11"
                ' 3rd Party Normal Computer	
                Return "E"

            Case "12"
                '	Reading for Information Only
                Return "A"

            Case Else
                Return ""
        End Select



    End Function

    Public Shared Function GetInvoicesList(ByRef dt As DataTable) As List(Of String)

        Dim invoicesList As New List(Of String)

        For Each row As DataRow In dt.Rows
            If Not (String.IsNullOrEmpty(row("Invoice Number")) Or String.IsNullOrEmpty(row("Bill Sequence No."))) Then

                If Not invoicesList.Contains(row("Invoice Number") & "~" & row("Bill Sequence No.")) Then
                    invoicesList.Add(row("Invoice Number") & "~" & row("Bill Sequence No."))
                End If

            End If


        Next

        Return invoicesList

    End Function

    Public Sub PopulateProperties(ByVal resultRows() As DataRow, ByRef invoiceDt As DataTable)

        'Loop through to fill the properties that old non-consumption information 
        'and perform another loop through resultRows to fill the consumption details

        For Each resultrow In resultRows


            'When Record Type is Site, get InvoiceNumber, StartDate, EndDate and VAT details

            If resultrow("Record Type").ToString.ToLower.Trim = "site" Then
                InvoiceNumber = resultrow("Invoice Number")
                StartDate = resultrow("Start Date")
                EndDate = resultrow("End Date")
                InvoiceDate = resultrow("Invoice Date")

                If Not Double.TryParse(resultrow("VAT Amount"), Vat) Then
                    Vat = 0
                End If

                Double.TryParse(resultrow("Total CCL"), Ccl)

            End If

            'When the Line Type is Meter: Get Previous/ Current readings and type. Also, get the MPAN and Meter Serial

            If resultrow("Line Type").ToString.ToLower.Trim = "meter" Then

                Mpan = resultrow("Meter Admin. Number")
                MeterSerial = resultrow("Meter Number")

            End If



        Next


        'If CCL charge has been applied, add it to the invoice table
        '  CreateCclEntry(invoiceDt)

        'Add all VAT against one of the charge Cost Only Line

        CreateVatEntry(invoiceDt)

        ' Assign the consumption related details to properties and add to the invoice details
        For Each resultrow In resultRows

            If resultrow("Line Type").ToString.ToLower.Trim = "charge" Then
                'Consumption = resultrow("Units Charged")
                If (resultrow("Base Unit of Measure").ToString().ToLower.Trim = "kwh") Then
                    Consumption = resultrow("Base Units")
                End If
                If resultrow("Unit of Charge").ToString.ToLower.Trim = "kwh" Then
                    ForceKwh = "Yes"

                    PopulateMeterDetails(resultRows)

                    'Cost Only Yes or No
                    If IsCostOnly(resultrow) Then
                        CostOnly = "Yes"

                    Else
                        CostOnly = "No"
                    End If

                Else
                    ForceKwh = "No"
                    CostOnly = "Yes"
                End If

                Net = resultrow("Total Charge")

                Dim vatHolder As Double
                If Double.TryParse(Vat, vatHolder) Then
                    Gross = (Net * Vat) + Net
                End If

                Description = resultrow("Tariff Code")

                CreateInvoiceEntry(invoiceDt)


            End If

        Next
    End Sub

    Public Sub GetTransformedHeaders()
        RenameColumns(_returnTable)
    End Sub

    Private Sub CreateInvoiceEntry(ByRef invoiceDt As DataTable)

        Dim invoiceDataRow As DataRow = invoiceDt.NewRow

        invoiceDataRow("Invoice Number") = InvoiceNumber
        invoiceDataRow("Meter Serial") = MeterSerial
        invoiceDataRow("MPAN") = Mpan
        invoiceDataRow("Start Date") = StartDate
        invoiceDataRow("End Date") = EndDate
        invoiceDataRow("Start Read") = StartRead
        invoiceDataRow("Start Read Estimated") = StartReadEstimated
        invoiceDataRow("End Read") = EndRead
        invoiceDataRow("End Read Estimated") = EndReadEstimated
        invoiceDataRow("Force kWh") = ForceKwh
        If CostOnly = "No" Then
            invoiceDataRow("Consumption") = Consumption
        End If
        invoiceDataRow("Rate") = Rate
        invoiceDataRow("Net") = Net
        invoiceDataRow("Vat") = 0
        invoiceDataRow("Gross") = Net
        invoiceDataRow("Cost Only") = CostOnly
        invoiceDataRow("Description") = Description
        invoiceDataRow("TaxPointDate") = InvoiceDate
        invoiceDataRow("IsLastDayApportion") = "No"
        invoiceDt.Rows.Add(invoiceDataRow)

    End Sub

    Private Sub CreateVatEntry(ByRef invoiceDt As DataTable)

        Dim invoiceDataRow As DataRow = invoiceDt.NewRow

        invoiceDataRow("Invoice Number") = InvoiceNumber
        invoiceDataRow("Meter Serial") = MeterSerial
        invoiceDataRow("MPAN") = Mpan
        invoiceDataRow("Start Date") = StartDate
        invoiceDataRow("End Date") = EndDate
        invoiceDataRow("Start Read") = StartRead
        invoiceDataRow("Start Read Estimated") = StartReadEstimated
        invoiceDataRow("End Read") = EndRead
        invoiceDataRow("End Read Estimated") = EndReadEstimated
        invoiceDataRow("Force kWh") = "No"
        invoiceDataRow("Net") = 0
        invoiceDataRow("Vat") = Vat
        invoiceDataRow("Gross") = Vat
        invoiceDataRow("Cost Only") = "Yes"
        invoiceDataRow("Description") = " Total VAT for the invoice"
        invoiceDataRow("TaxPointDate") = InvoiceDate
        invoiceDataRow("IsLastDayApportion") = "No"

        invoiceDt.Rows.Add(invoiceDataRow)


    End Sub

    Private Sub PopulateMeterDetails(ByVal resultRows() As DataRow)

        'When the Line Type is Meter: Get Previous/ Current readings and type. Also, get the MPAN and Meter Serial

        For Each resultRow As DataRow In resultRows

            If resultRow("Line Type").ToString.ToLower.Trim = "meter" And _
               resultRow("Base Units") = Consumption Then

                StartRead = resultRow("Previous Reading")
                StartReadEstimated = resultRow("Previous Reading Type")
                EndRead = resultRow("Current Reading")
                EndReadEstimated = resultRow("Current Reading Type")
                Mpan = resultRow("Meter Admin. Number")
                MeterSerial = resultRow("Meter Number")
            End If
        Next
    End Sub

    Private Function IsCostOnly(ByVal resultRow As DataRow) As Boolean

        Dim consumptionTariffs As New List(Of String)

        consumptionTariffs.Add("domesticstd")
        consumptionTariffs.Add("allunits")
        consumptionTariffs.Add("domesticday/night")
        consumptionTariffs.Add("domesticoffpeakc")
        consumptionTariffs.Add("nhhdomopcspusage")
        consumptionTariffs.Add("nhhdomcpwmcusage")
        consumptionTariffs.Add("nhhdomcpwmpusage")
        consumptionTariffs.Add("sphhlv-usage")
        consumptionTariffs.Add("sphhhv-usageproduct")
        consumptionTariffs.Add("nhhopc")
        consumptionTariffs.Add("16usage(neithertlf&dlf)")


        If consumptionTariffs.Contains(resultRow("Tariff Code").ToString.Replace(" ", "").ToLower.Trim) Then
            Return False
        Else
            Return True
        End If

    End Function

    Private Sub RenameColumns(returnTable As DataTable)
        returnTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        returnTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        returnTable.Columns("Start Date").ColumnName = "StartDate"
        returnTable.Columns("End Date").ColumnName = "EndDate"
        returnTable.Columns("Start Read").ColumnName = "StartRead"
        returnTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        returnTable.Columns("End Read").ColumnName = "EndRead"
        returnTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        returnTable.Columns("Force kWh").ColumnName = "ForceKwh"
        returnTable.Columns("Rate").ColumnName = "Tariff"
        returnTable.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub
End Class
