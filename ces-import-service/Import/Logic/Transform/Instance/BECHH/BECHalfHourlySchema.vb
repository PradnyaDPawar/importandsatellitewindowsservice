﻿Public Class BECHalfHourlySchema

    Public Shared Function ToDEFormat(ByRef dt As DataTable)

        Dim str As String
        ''str used to get Meter Serial
        str = dt.Rows(0).Item("13").ToString()
        '' To Delete unwanted columns
        DeleteFirstThreeRows(dt)
       
        '' To set column headers
        setColumns(dt)
        '' To Add columns ForcekWh and MeterSerial
        AddColumns(dt)
        '' To add data to new columns
        setColumnData(dt, str)
        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)
        Return dt

    End Function

    Public Shared Sub AddColumns(ByRef dt As DataTable)
        dt.Columns.Add("Force kWh")
        dt.Columns.Add("Meter Serial")
    End Sub
  
    Private Shared Sub DeleteFirstThreeRows(ByRef tdt As DataTable)

        For deleteCounter As Integer = 0 To 1
            tdt.Rows.RemoveAt(deleteCounter)
        Next
        tdt.AcceptChanges()
        For Each item As Object In tdt.Rows(0).ItemArray

            If Not IsNothing(item.ToString()) Then
                tdt.Rows(0).Delete()
                tdt.AcceptChanges()
                Exit For

            End If
        Next
    End Sub
    Private Shared Sub  setColumns(ByRef tdt As DataTable) 
        ''removing header rows of datatable
        tdt.Rows.RemoveAt(0)

        ''setting new header to columns in datatable
        tdt.Columns(0).ColumnName = "Date(DD/MM/YYYY)"
        tdt.Columns(6).ColumnName = "Consumption"
        DataTableTools.RemoveBlankColumns(tdt)
    End Sub
    Private Shared Sub setColumnData(ByRef tdt As DataTable, ByRef str As String)

        For Each row In tdt.Rows
            row("Meter Serial") = str.ToString()
            If (row("Meter Serial").ToString().Contains("Water")) Then
                row("Force kWh") = "No"

            Else
                row("Force kWh") = "Yes"
            End If
        Next
    End Sub

    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        '>> Add MeterSerials and Dates Columns followed by the consumptions

        Dim success As Boolean = False  'Success Variable is to determine if both the Date(dd/mm/yyyy) and Meter Serial Row Exist.
        Dim theDate As DateTime          'Stores the Date from the dt DataTable
        Dim theTime As DateTime          'Stores the Time from the dt DataTable

        For Each row As DataRow In dt.Rows

            If IsDate(row("Date(DD/MM/YYYY)")) Then 'Just to prevent errors

                '>> Collect date and time values from the current row being processed for the next stage

                theDate = row("Date(DD/MM/YYYY)")
                theTime = theDate.ToString("HH:mm") 'Converts the dateTime to a time value
                Dim theTimeString = theTime.ToString("HH:mm") 'Creates a string version of time


                If hhDataTable.Columns.Contains(theTimeString) Then 'Just to prevent errors

                    For Each hhrow As DataRow In hhDataTable.Rows


                        '>> Check to see if there is already a Meter Serial Number with the same Date already in the Di If hhrow("MPAN") = row("MPAN") Then 'Compare current row with other MPAN in the Table
                        If hhrow("Date(DD/MM/YYYY)") = CDate(row("Date(DD/MM/YYYY)")).ToString("dd/MM/yyyy") Then 'Compare current date with other Dates in the Table

                            '>> HH data columns  including the value ‘null’ , replaced cell with a blank cell
                            hhrow(theTimeString) = row("Consumption") 'Add the Consumption under the correct Time Column in the row
                            success = True 'Turn the success variable to true, to avoid adding a duplicate Meter Serial and Date row

                        End If

                    Next

                    '>> If there are no Meter Serial Numbers with the same date of the row being processed then... 
                    If Not success Then
                        Dim newRow As DataRow = hhDataTable.NewRow      ' Create a new row
                        newRow("MeterSerial") = row("Meter Serial") ' Add the Meter Serial Number
                        newRow("ForcekWh") = row("Force kWh") ''Add ForcekWh
                        newRow("Date(DD/MM/YYYY)") = CDate(row("Date(DD/MM/YYYY)")).ToString("dd/MM/yyyy")     ' Add the date
                        '>> HH data columns  including the value ‘null’ , replaced cell with a blank cell
                        newRow(theTimeString) = row("Consumption")    ' And add the Consumption under the correct Time Column in the row.

                        hhDataTable.Rows.Add(newRow)                    ' Add the new row to the Digital Energy Formatted Table.
                    End If
                    success = False 'Reset success counter for the next row
                End If
            End If
        Next

        dt = hhDataTable
    End Sub
End Class
