﻿Public Class BECHalfHourly
    Implements ITransformDelegate
    Private _returnTable As DataTable

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel()
        excel.Load(fullPath)
        Dim xSheet As ExcelSheet = excel.Sheets(0)
        ReturnTable = xSheet.GetDataTable()


        DataTableTools.RemoveBlankColumns(ReturnTable)
        DataTableTools.RemoveBlankRows(ReturnTable)
    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat


        BECHalfHourlySchema.ToDEFormat(ReturnTable)
        For Each row As DataRow In _returnTable.Rows
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
