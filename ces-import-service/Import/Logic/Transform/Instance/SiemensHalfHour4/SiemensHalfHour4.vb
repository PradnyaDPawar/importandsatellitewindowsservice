﻿Public Class SiemensHalfHour4
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim ext As String = System.IO.Path.GetExtension(fullPath).ToLower
        If ext = ".csv" Then
            Dim csvFile As New DelimitedFile(fullPath, 50)
            csvFile.Load(_returnTable, SiemensHalfHour4Schema.GetHeaders())
        ElseIf ext = ".xls" Or ext = ".xlsx" Then
            Dim excel As New Excel()
            excel.Load(fullPath)
            Dim xSheet As ExcelSheet = excel.Sheets(0)
            xSheet.HeaderRowIndex = 1
            _returnTable = xSheet.GetDataTable()
        End If



    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        SiemensHalfHour4Schema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
