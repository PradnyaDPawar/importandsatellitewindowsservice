﻿Imports System.Globalization

Public Class SiemensHalfHour4Schema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("MPAN")
        headers.Add("Date(DD/MM/YYYY)")
        headers.Add("00:00")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")


        Return headers
    End Function


    Public Shared Sub ToDeFormat(ByRef dt As DataTable)
        If dt.Columns.Contains("Date") Then
            dt.Columns("Date").ColumnName = "Date(DD/MM/YYYY)"
        End If
        AddColumns(dt)
        PopulateColumns(dt)

    End Sub


    Private Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("MeterSerial")
        dt.Columns.Add("ForcekWh")


    End Sub

    Private Shared Sub PopulateColumns(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows

            row("ForceKwh") = "Yes"
            'Convert to British Date
            'row("Date(DD/MM/YYYY)") = Date.ParseExact(row("Date(DD/MM/YYYY)"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy")
        Next

    End Sub


End Class
