﻿Public Class HavenHalfHour2Schema

    Public Shared Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("siteRef")
        headers.Add("MPAN")
        headers.Add("ConsumptionDate")
        headers.Add("kWh_1")
        headers.Add("kWh_2")
        headers.Add("kWh_3")
        headers.Add("kWh_4")
        headers.Add("kWh_5")
        headers.Add("kWh_6")
        headers.Add("kWh_7")
        headers.Add("kWh_8")
        headers.Add("kWh_9")
        headers.Add("kWh_10")
        headers.Add("kWh_11")
        headers.Add("kWh_12")
        headers.Add("kWh_13")
        headers.Add("kWh_14")
        headers.Add("kWh_15")
        headers.Add("kWh_16")
        headers.Add("kWh_17")
        headers.Add("kWh_18")
        headers.Add("kWh_19")
        headers.Add("kWh_20")
        headers.Add("kWh_21")
        headers.Add("kWh_22")
        headers.Add("kWh_23")
        headers.Add("kWh_24")
        headers.Add("kWh_25")
        headers.Add("kWh_26")
        headers.Add("kWh_27")
        headers.Add("kWh_28")
        headers.Add("kWh_29")
        headers.Add("kWh_30")
        headers.Add("kWh_31")
        headers.Add("kWh_32")
        headers.Add("kWh_33")
        headers.Add("kWh_34")
        headers.Add("kWh_35")
        headers.Add("kWh_36")
        headers.Add("kWh_37")
        headers.Add("kWh_38")
        headers.Add("kWh_39")
        headers.Add("kWh_40")
        headers.Add("kWh_41")
        headers.Add("kWh_42")
        headers.Add("kWh_43")
        headers.Add("kWh_44")
        headers.Add("kWh_45")
        headers.Add("kWh_46")
        headers.Add("kWh_47")
        headers.Add("kWh_48")
        headers.Add("kVArh_1")
        headers.Add("kVArh_2")
        headers.Add("kVArh_3")
        headers.Add("kVArh_4")
        headers.Add("kVArh_5")
        headers.Add("kVArh_6")
        headers.Add("kVArh_7")
        headers.Add("kVArh_8")
        headers.Add("kVArh_9")
        headers.Add("kVArh_10")
        headers.Add("kVArh_11")
        headers.Add("kVArh_12")
        headers.Add("kVArh_13")
        headers.Add("kVArh_14")
        headers.Add("kVArh_15")
        headers.Add("kVArh_16")
        headers.Add("kVArh_17")
        headers.Add("kVArh_18")
        headers.Add("kVArh_19")
        headers.Add("kVArh_20")
        headers.Add("kVArh_21")
        headers.Add("kVArh_22")
        headers.Add("kVArh_23")
        headers.Add("kVArh_24")
        headers.Add("kVArh_25")
        headers.Add("kVArh_26")
        headers.Add("kVArh_27")
        headers.Add("kVArh_28")
        headers.Add("kVArh_29")
        headers.Add("kVArh_30")
        headers.Add("kVArh_31")
        headers.Add("kVArh_32")
        headers.Add("kVArh_33")
        headers.Add("kVArh_34")
        headers.Add("kVArh_35")
        headers.Add("kVArh_36")
        headers.Add("kVArh_37")
        headers.Add("kVArh_38")
        headers.Add("kVArh_39")
        headers.Add("kVArh_40")
        headers.Add("kVArh_41")
        headers.Add("kVArh_42")
        headers.Add("kVArh_43")
        headers.Add("kVArh_44")
        headers.Add("kVArh_45")
        headers.Add("kVArh_46")
        headers.Add("kVArh_47")
        headers.Add("kVArh_48")

        Return headers
    End Function


    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        CreateColumns(dt)
        PopulateForceKwh(dt)
        RenameColumns(dt)
        Return dt
    End Function

    Private Shared Sub CreateColumns(ByRef dt As DataTable)
        dt.Columns.Add("Meter Serial")
        dt.Columns.Add("Force kWh")
    End Sub

    Private Shared Sub PopulateForceKwh(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows
            row("Force kWh") = "Yes"
        Next

    End Sub

    Private Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("ConsumptionDate").ColumnName = "Date (dd/mm/yyyy)"

        'Rename HH entries
        Dim dateholder As Date = DateTime.Today

        For i As Integer = 1 To 48
            dt.Columns("kWh_" & i.ToString).ColumnName = dateholder.ToString("HH:mm")
            dateholder = dateholder.AddMinutes(30)
        Next

    End Sub
End Class
