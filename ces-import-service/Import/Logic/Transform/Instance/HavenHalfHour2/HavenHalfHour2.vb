﻿Public Class HavenHalfHour2
    Implements ITransformDelegate

    Public Function Invoke(ByVal stagingFilePath As String, ByRef returnTable As System.Data.DataTable) As Boolean Implements ITransformDelegate.Invoke
        Try
            Dim excel As New DeCommon.Excel()
            excel.Load(stagingFilePath)
            Dim xSheet As ExcelSheet = excel.Sheets(0)
            xSheet.HeaderRowIndex = 1
            returnTable = xSheet.GetDataTable()
            HavenHalfHour2Schema.ToDeFormat(returnTable)
            returnTable.TableName = "Half Hour Format 2"
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class
