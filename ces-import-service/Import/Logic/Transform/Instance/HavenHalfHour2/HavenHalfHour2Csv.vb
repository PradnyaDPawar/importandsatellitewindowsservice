﻿Public Class HavenHalfHour2Csv
    Implements ITransformDelegate

    Public Function Invoke(ByVal stagingFilePath As String, ByRef returnTable As System.Data.DataTable) As Boolean Implements ITransformDelegate.Invoke
        Try

            Dim csvFile As New DelimitedFile
            csvFile.Load(stagingFilePath, HavenHalfHour2Schema.GetHeaders())

            returnTable = csvFile.ToDataTable(True)

            HavenHalfHour2Schema.ToDeFormat(returnTable)

            returnTable.TableName = "Half Hour Format 2"

            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
End Class
