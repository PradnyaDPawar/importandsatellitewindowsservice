﻿Imports System.Globalization

Public Class SembcorpHalfHour
    Implements ITransformDelegate

    Public Function Invoke(ByVal stagingFilePath As String, ByRef returnTable As System.Data.DataTable) As Boolean Implements ITransformDelegate.Invoke
        Try
            Dim excel As New Excel
            Dim sembcorpHalfHourSchema As New SembcorpHalfHourSchema

            excel.Load(stagingFilePath)

            For Each xSheet As ExcelSheet In excel.Sheets
                Dim resultDate As Date
                Dim resultDateString As String
                Dim str = xSheet.Name.ToString.Trim
                DateTime.TryParseExact(str, "MMM yy", CultureInfo.InvariantCulture, DateTimeStyles.None, resultDate)
                resultDateString = resultDate.ToString("MMM yy")
                If resultDateString = str Then
                    returnTable = xSheet.GetDataTable()
                    sembcorpHalfHourSchema.ToDeFormat(returnTable)
                End If
            Next

            returnTable = sembcorpHalfHourSchema.HhDataTable
            returnTable.TableName = "Half Hour Format 2"
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function


End Class
