﻿Imports System.Globalization
Imports System.Text.RegularExpressions

Public Class SembcorpHalfHourSchema

    Private _hhDataTable As DataTable

    Public ReadOnly Property HhDataTable() As DataTable
        Get
            Return _hhDataTable
        End Get

    End Property

    Public Sub New()
        _hhDataTable = DeFormatTableTemplates.CreateHalfHourTable()
    End Sub


    Public Sub ToDeFormat(ByRef dt As DataTable)
        'Name columns

        dt.Columns("0").ColumnName = "Date"
        dt.Columns("1").ColumnName = "Time"
        dt.Columns("2").ColumnName = "Actual"
        dt.Columns("3").ColumnName = "Consumption"

        RemoveRedundantColumns(dt)
        DeleteNonConsumptionRows(dt)
        DataTableTools.RemoveBlankRows(dt)

        ChangeTimeFormat(dt)
        FillBlankDates(dt)
        CreateHalfHourlyEntries(dt, _hhDataTable)


    End Sub

    Private Sub RemoveRedundantColumns(ByRef dt As DataTable)
        For count As Integer = 1 To 5
            dt.Columns.RemoveAt(4)
        Next
    End Sub

    Private Sub DeleteNonConsumptionRows(ByRef dt As DataTable)

        For rowIndex = 0 To GetFirstConsumptionRow(dt) - 1
            dt.Rows.RemoveAt(0)
        Next

        'Remove the Totals row
        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If String.IsNullOrEmpty(row("Time")) Then
                dt.Rows.Remove(row)
            End If
        Next
    End Sub

    Private Function GetFirstConsumptionRow(ByRef dt As DataTable) As Integer
        Dim dateString As String
        Dim dateTimeHolder As DateTime
        Dim reg As Regex = New Regex("^([01]\d|2[0-3]):?([0-5]\d)$") 'Regex to match 24 hour format

        For Each row As DataRow In dt.Rows
            If IsDate(row("Time")) Then
                dateTimeHolder = row("Time")
                dateString = dateTimeHolder.ToString("HH:mm")
                Dim m As Match = reg.Match(dateString)
                If m.Success Then
                    Return dt.Rows.IndexOf(row)
                End If
            End If

        Next

        Return -1

    End Function


    Private Sub ChangeTimeFormat(ByRef dt As DataTable)

        Dim dateTimeHolder As DateTime
        For Each row As DataRow In dt.Rows
            If IsDate(row("Time")) Then
                dateTimeHolder = row("Time")
                row("Time") = dateTimeHolder.ToString("HH:mm")
            End If
        Next

    End Sub

    Private Sub FillBlankDates(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows
            If Not IsDate(row("Date")) Then
                If row("Time") = "0:00" Or row("Time") = "00:00" Or row("Time") = "00:00:00" Then
                    row("Date") = dt.Rows(dt.Rows.IndexOf(row) + 1)("Date")
                Else
                    If dt.Rows.IndexOf(row) > 0 Then
                        row("Date") = dt.Rows(dt.Rows.IndexOf(row) - 1)("Date")
                    End If

                End If
            End If
        Next
    End Sub

    Private Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim dateholder As DateTime
        Dim success As Boolean = False
        For Each row As DataRow In dt.Rows

            If IsDate(row("Date")) Then
                dateholder = row("Date")
                For Each hhrow As DataRow In hhDataTable.Rows
                    If hhrow("Date (dd/mm/yyyy)") = dateholder.Date Then
                        hhrow(row("Time")) = row("Consumption")
                        success = True
                    End If
                Next

                If Not success Then
                    Dim newRow As DataRow = hhDataTable.NewRow
                    newRow("Date (dd/mm/yyyy)") = dateholder.Date
                    newRow(row("Time")) = row("Consumption")
                    hhDataTable.Rows.Add(newRow)
                End If
            End If
            success = False
        Next

        '  dt = hhDataTable

    End Sub


End Class
