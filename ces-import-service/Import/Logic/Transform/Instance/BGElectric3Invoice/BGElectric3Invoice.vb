﻿Public Class BGElectric3Invoice
    Implements ITransformDelegate

    Private _returnTable As DataTable
    Private _SummaryTable As DataTable

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel()

        excel.Load(fullPath)
        Dim xSheet As ExcelSheet = excel.Sheets(0)

        If excel.GetSheet("Detail", xSheet) Then
            xSheet.HeaderRowIndex = 1

            _returnTable = xSheet.GetDataTable()
        Else
            Throw New ApplicationException("Unable to find the 'Detail' sheet.")
        End If

        If excel.GetSheet("Summary", xSheet) Then
            xSheet.HeaderRowIndex = 1
            _SummaryTable = xSheet.GetDataTable()
        Else
            Throw New ApplicationException("Unable to find 'Summary' sheet.")
        End If

    End Sub

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        ColumnValidate()

        GetTransformedHeaders()

        PopulateColumns()

        For Each row As DataRow In _returnTable.Rows

            If Not String.IsNullOrEmpty(row("InvoiceNumber")) Then
                FE_Invoice.Fill(row, dsEdits, importEdit)
            End If
        Next
    End Sub
End Class
