﻿Public Class NpowerFormat4HHSchema


    Public Shared Sub ToDeFormat(ByRef dt As DataTable)
        RenameColumns(dt)
        AddColumns(dt)
        RemoveUnwantedkVarhColumns(dt)
        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)

        dt = hhDataTable
    End Sub
    ''' <summary>
    ''' Method to remove unwanted clumns starting from kVarh_1 to kVarh_48 and SiteRef
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function RemoveUnwantedkVarhColumns(ByRef dt As DataTable) As DataTable
        For columncount As Integer = dt.Columns.Count - 1 To 0 Step -1
            If dt.Columns(columncount).ColumnName.Contains("kVArh") Then
                dt.Columns.RemoveAt(columncount)
            End If
        Next
        dt.Columns.Remove("siteRef")
        Return dt
    End Function
    Private Shared Sub RenameColumns(ByRef dt As DataTable)
        dt.Columns("ConsumptionDate").ColumnName = "Date(DD/MM/YYYY)"

        dt.Columns("kWh_1").ColumnName = "00:00"
        dt.Columns("kWh_2").ColumnName = "00:30"
        dt.Columns("kWh_3").ColumnName = "01:00"
        dt.Columns("kWh_4").ColumnName = "01:30"
        dt.Columns("kWh_5").ColumnName = "02:00"
        dt.Columns("kWh_6").ColumnName = "02:30"
        dt.Columns("kWh_7").ColumnName = "03:00"
        dt.Columns("kWh_8").ColumnName = "03:30"
        dt.Columns("kWh_9").ColumnName = "04:00"
        dt.Columns("kWh_10").ColumnName = "04:30"
        dt.Columns("kWh_11").ColumnName = "05:00"
        dt.Columns("kWh_12").ColumnName = "05:30"
        dt.Columns("kWh_13").ColumnName = "06:00"
        dt.Columns("kWh_14").ColumnName = "06:30"
        dt.Columns("kWh_15").ColumnName = "07:00"
        dt.Columns("kWh_16").ColumnName = "07:30"
        dt.Columns("kWh_17").ColumnName = "08:00"
        dt.Columns("kWh_18").ColumnName = "08:30"
        dt.Columns("kWh_19").ColumnName = "09:00"
        dt.Columns("kWh_20").ColumnName = "09:30"
        dt.Columns("kWh_21").ColumnName = "10:00"
        dt.Columns("kWh_22").ColumnName = "10:30"
        dt.Columns("kWh_23").ColumnName = "11:00"
        dt.Columns("kWh_24").ColumnName = "11:30"
        dt.Columns("kWh_25").ColumnName = "12:00"
        dt.Columns("kWh_26").ColumnName = "12:30"
        dt.Columns("kWh_27").ColumnName = "13:00"
        dt.Columns("kWh_28").ColumnName = "13:30"
        dt.Columns("kWh_29").ColumnName = "14:00"
        dt.Columns("kWh_30").ColumnName = "14:30"
        dt.Columns("kWh_31").ColumnName = "15:00"
        dt.Columns("kWh_32").ColumnName = "15:30"
        dt.Columns("kWh_33").ColumnName = "16:00"
        dt.Columns("kWh_34").ColumnName = "16:30"
        dt.Columns("kWh_35").ColumnName = "17:00"
        dt.Columns("kWh_36").ColumnName = "17:30"
        dt.Columns("kWh_37").ColumnName = "18:00"
        dt.Columns("kWh_38").ColumnName = "18:30"
        dt.Columns("kWh_39").ColumnName = "19:00"
        dt.Columns("kWh_40").ColumnName = "19:30"
        dt.Columns("kWh_41").ColumnName = "20:00"
        dt.Columns("kWh_42").ColumnName = "20:30"
        dt.Columns("kWh_43").ColumnName = "21:00"
        dt.Columns("kWh_44").ColumnName = "21:30"
        dt.Columns("kWh_45").ColumnName = "22:00"
        dt.Columns("kWh_46").ColumnName = "22:30"
        dt.Columns("kWh_47").ColumnName = "23:00"
        dt.Columns("kWh_48").ColumnName = "23:30"

    End Sub

    ''nikitah:refactoring code
    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim map As New Dictionary(Of String, DataRow)

        For Each row As DataRow In dt.Rows

            For Each column As DataColumn In dt.Columns
                If IsDate(row("Date(DD/MM/YYYY)")) And (column.ColumnName <> "Date(DD/MM/YYYY)") And (column.ColumnName <> "ForcekWh") And (column.ColumnName <> "MPAN") And (column.ColumnName <> "MeterSerial") Then 'Just to prevent errors
                    Dim theDate As DateTime = column.ColumnName
                    Dim theTime As DateTime = theDate.ToString("HH:mm") 'Converts the dateTime to a time value
                    Dim theTimeString = theTime.ToString("HH:mm")
                    Dim key As String = row("Date(DD/MM/YYYY)") & row("MPAN")
                    If map.ContainsKey(key) Then
                        Dim theRow As DataRow = map(key)
                        theRow(theTimeString) = IIf(row(theTimeString) = "null", "", row(theTimeString))
                    Else
                        Dim newRow As DataRow = hhDataTable.NewRow
                        newRow("MeterSerial") = row("MeterSerial")    ' Add the Meter Serial Number
                        newRow("Date(DD/MM/YYYY)") = row("Date(DD/MM/YYYY)")     ' Add the date
                        newRow(theTimeString) = IIf(row(theTimeString) = "null", "", row(theTimeString))      ' And add the Consumption under the correct Time Column in the row.
                        newRow("MPAN") = row("MPAN")
                        hhDataTable.Rows.Add(newRow)

                        map.Add(key, newRow)
                    End If

                End If
            Next

        Next
        dt = hhDataTable
    End Sub
    Public Shared Sub AddColumns(ByRef dt As DataTable)
        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("MeterSerial")
    End Sub
End Class
