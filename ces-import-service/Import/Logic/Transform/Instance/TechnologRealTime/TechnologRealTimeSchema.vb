﻿Public Class TechnologRealTimeSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("customerref")
        headers.Add("utility")
        headers.Add("field1")
        headers.Add("field2")
        headers.Add("field3")
        headers.Add("field4")
        headers.Add("field5")
        headers.Add("field6")
        headers.Add("units")
        headers.Add("datetime")
        headers.Add("reading")

        Return headers

    End Function


    Public Shared Sub ToDeFormat(ByRef dt As DataTable)
        'Row 1 contains version information 
        dt.Rows(0).Delete()

        dt.Columns("customerref").ColumnName = "MeterName"
        dt.Columns("datetime").ColumnName = "DateTime"
        dt.Columns("reading").ColumnName = "CountUnits"

        'Remove other columns

    End Sub

End Class
