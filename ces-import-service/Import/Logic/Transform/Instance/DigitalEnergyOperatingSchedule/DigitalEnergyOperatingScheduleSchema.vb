﻿Public Class DigitalEnergyOperatingScheduleSchema

    Private _resultTable As DataTable

    Public Sub New()

        _resultTable = New DataTable

    End Sub

    Public Property ResultTable() As DataTable

        Get
            Return _resultTable
        End Get

        Set(value As DataTable)
            _resultTable = value
        End Set

    End Property

    Public Sub ToDeFormat(ByRef dtOperatingSchedule As DataTable)

        ResultTable = dtOperatingSchedule

        PrepareTable()
        RenameColumns()

    End Sub

    Private Sub PrepareTable()

        DataTableTools.RemoveBlankColumns(ResultTable)
        DataTableTools.RemoveBlankRows(ResultTable)

        DataTableTools.SetHeaders(ResultTable, 1)

    End Sub

    Private Sub RenameColumns()

        If ResultTable.Columns.Contains("0:00") Then
            ResultTable.Columns("0:00").ColumnName = "00:00"
        End If
        If ResultTable.Columns.Contains("0:30") Then
            ResultTable.Columns("0:30").ColumnName = "00:30"
        End If
        If ResultTable.Columns.Contains("1:00") Then
            ResultTable.Columns("1:00").ColumnName = "01:00"
        End If
        If ResultTable.Columns.Contains("1:30") Then
            ResultTable.Columns("1:30").ColumnName = "01:30"
        End If
        If ResultTable.Columns.Contains("2:00") Then
            ResultTable.Columns("2:00").ColumnName = "02:00"
        End If
        If ResultTable.Columns.Contains("2:30") Then
            ResultTable.Columns("2:30").ColumnName = "02:30"
        End If
        If ResultTable.Columns.Contains("3:00") Then
            ResultTable.Columns("3:00").ColumnName = "03:00"
        End If
        If ResultTable.Columns.Contains("3:30") Then
            ResultTable.Columns("3:30").ColumnName = "03:30"
        End If
        If ResultTable.Columns.Contains("4:00") Then
            ResultTable.Columns("4:00").ColumnName = "04:00"
        End If
        If ResultTable.Columns.Contains("4:30") Then
            ResultTable.Columns("4:30").ColumnName = "04:30"
        End If
        If ResultTable.Columns.Contains("5:00") Then
            ResultTable.Columns("5:00").ColumnName = "05:00"
        End If
        If ResultTable.Columns.Contains("5:30") Then
            ResultTable.Columns("5:30").ColumnName = "05:30"
        End If
        If ResultTable.Columns.Contains("6:00") Then
            ResultTable.Columns("6:00").ColumnName = "06:00"
        End If
        If ResultTable.Columns.Contains("6:30") Then
            ResultTable.Columns("6:30").ColumnName = "06:30"
        End If
        If ResultTable.Columns.Contains("7:00") Then
            ResultTable.Columns("7:00").ColumnName = "07:00"
        End If
        If ResultTable.Columns.Contains("7:30") Then
            ResultTable.Columns("7:30").ColumnName = "07:30"
        End If
        If ResultTable.Columns.Contains("8:00") Then
            ResultTable.Columns("8:00").ColumnName = "08:00"
        End If
        If ResultTable.Columns.Contains("8:30") Then
            ResultTable.Columns("8:30").ColumnName = "08:30"
        End If
        If ResultTable.Columns.Contains("9:00") Then
            ResultTable.Columns("9:00").ColumnName = "09:00"
        End If
        If ResultTable.Columns.Contains("9:30") Then
            ResultTable.Columns("9:30").ColumnName = "09:30"
        End If

        ResultTable.Columns(0).ColumnName = ResultTable.Columns(0).ColumnName.ToLower()
        ResultTable.Columns(1).ColumnName = ResultTable.Columns(1).ColumnName.ToLower()
        ResultTable.Columns(2).ColumnName = ResultTable.Columns(2).ColumnName.ToLower()

    End Sub

End Class