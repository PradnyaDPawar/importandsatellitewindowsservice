﻿Public Class DigitalEnergyOperatingSchedule
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel()
        excel.Load(fullPath)

        Dim xSheet As ExcelSheet = excel.Sheets(0)

        If xSheet.Name.ToLower.Trim() = "operating schedule" Then
            ReturnTable = xSheet.GetDataTable()

        Else
            Throw New ApplicationException("Could Not Load File.")

        End If

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Dim schema As New DigitalEnergyOperatingScheduleSchema

        schema.ToDeFormat(ReturnTable)

        For Each row As DataRow In schema.ResultTable.Rows

            FE_OperatingSchedule.Fill(row, dsEdits, importEdit)

        Next

    End Sub

End Class