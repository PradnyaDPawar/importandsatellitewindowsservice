﻿Public Class Gazprom4Invoice
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel()

        excel.Load(fullPath)

        Dim xsheet As ExcelSheet = excel.Sheets(0)

        If Not excel.GetSheet("Sheet1", xsheet) = True Then

        End If

        xsheet.HeaderRowIndex = 1

        _returnTable = xsheet.GetDataTable()
    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        Gazprom4InvoiceSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
