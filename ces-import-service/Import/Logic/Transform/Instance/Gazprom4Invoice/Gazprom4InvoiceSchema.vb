﻿Imports System.Globalization

Public Class Gazprom4InvoiceSchema

    ''' <summary>
    ''' headers of input raw file to be imported
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
 
        headers.Add("AccountNum")
        headers.Add("InvoiceNo")
        headers.Add("SiteRefNum")
        headers.Add("Tax Point Date")
        headers.Add("Credited")
        headers.Add("MeterPoint")
        headers.Add("MeterNo")
        headers.Add("Corrected Consumption")
        headers.Add("CorrFactor")
        headers.Add("CV")
        headers.Add("Energy")
        headers.Add("UnitPrice")
        headers.Add("Standing Chrg")
        headers.Add("SC VAT")
        headers.Add("GasCost")
        headers.Add("CommPercentage")
        headers.Add("CommVatContent")
        headers.Add("DomVatContent")
        headers.Add("CC Levey")
        headers.Add("CCL VAT")
        headers.Add("Total")
        headers.Add("SiteRefName")
        headers.Add("AddressLine1")
        headers.Add("AddressLine2")
        headers.Add("AddressLine3")
        headers.Add("AddressLine4")
        headers.Add("PostCode")
        headers.Add("CurrentReading")
        headers.Add("CurrentType")
        headers.Add("EndDate")
        headers.Add("PreviousReading")
        headers.Add("PreviousType")
        headers.Add("StartDate")
        Return headers
    End Function

    Public Shared Function Validate(ByRef dt As DataTable) As Boolean

        For Each header As String In GetHeaders()
            If Not dt.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

        RenameColumns(dt)
        CreateColumns(dt)
        PopulateColumns(dt)

        Return dt

    End Function

    Public Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("InvoiceNo").ColumnName = "InvoiceNumber"
        dt.Columns("MeterNo").ColumnName = "MeterSerial"
        dt.Columns("MeterPoint").ColumnName = "MPAN"
        dt.Columns("Tax Point Date").ColumnName = "TaxPointDate"
        'dt.Columns("StartDate").ColumnName = "StartDate"
        dt.Columns("PreviousReading").ColumnName = "StartRead"
        dt.Columns("PreviousType").ColumnName = "StartReadEstimated"
        'dt.Columns("EndDate").ColumnName = "EndDate"
        dt.Columns("CurrentReading").ColumnName = "EndRead"
        dt.Columns("CurrentType").ColumnName = "EndReadEstimated"
        dt.Columns("Energy").ColumnName = "Consumption" 

    End Sub

    Public Shared Sub CreateColumns(ByRef dt As DataTable)

        dt.Columns.Add("Tariff")
        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("Net")
        dt.Columns.Add("Vat")
        dt.Columns.Add("Gross")
        dt.Columns.Add("CostOnly")
        dt.Columns.Add("Description")
        dt.Columns.Add("IsLastDayApportion")

    End Sub

    Private Shared Sub PopulateColumns(ByRef dt As DataTable)
        Dim dtResult As DataTable = dt.Clone
        If dtResult.Rows.Count = 0 And dt.Rows.Count > 0 Then
            For Each row As DataRow In dt.Rows

                PopulateForceKwh(row)
                PopulateCostOnly(row)
                PopulateIsLastDayApportion(row)
                PopulateDatesInUKFormat(row)
                PopulateCurrencyFormatToNumerics(row)

                ' Gas Consumption Logic
                Dim drGas As DataRow = dtResult.NewRow
                drGas.ItemArray = row.ItemArray
                PopulateGasConsumption(drGas)
                dtResult.Rows.Add(drGas)

                row("Consumption") = 0.0
                row("ForcekWh") = "No"
                row("CostOnly") = "Yes"

                ' Standard Charge Logic
                Dim drStandard As DataRow = dtResult.NewRow
                drStandard.ItemArray = row.ItemArray
                PopulateStandardCharge(drStandard)
                dtResult.Rows.Add(drStandard)

                ' Climate Change Logic
                Dim drClimate As DataRow = dtResult.NewRow
                drClimate.ItemArray = row.ItemArray
                PopulateClimateChange(drClimate)
                dtResult.Rows.Add(drClimate)
            Next
            dt = dtResult
        Else
            Throw New Exception("Runtime error occured while populating columns. . .")
        End If
    End Sub

    Private Shared Sub PopulateDatesInUKFormat(ByRef row As DataRow)
        If IsNothing(row("TaxPointDate")) OrElse String.IsNullOrWhiteSpace(row("TaxPointDate").ToString) Then
            row("TaxPointDate") = Nothing
        Else
            row("TaxPointDate") = DateTime.Parse(row("TaxPointDate")).ToString("dd/MM/yyyy")
        End If
        If IsNothing(row("StartDate")) OrElse String.IsNullOrWhiteSpace(row("StartDate").ToString) Then
            row("StartDate") = Nothing
        Else
            row("StartDate") = DateTime.Parse(row("StartDate")).ToString("dd/MM/yyyy")
        End If
        If IsNothing(row("EndDate")) OrElse String.IsNullOrWhiteSpace(row("EndDate").ToString) Then
            row("EndDate") = Nothing
        Else
            row("EndDate") = DateTime.Parse(row("EndDate")).ToString("dd/MM/yyyy")
        End If 
    End Sub

    Private Shared Sub PopulateCurrencyFormatToNumerics(ByRef row As DataRow)
        If IsNothing(row("GasCost")) OrElse String.IsNullOrWhiteSpace(row("GasCost").ToString) Then
            row("GasCost") = 0.0
        Else
            row("GasCost") = row("GasCost").ToString().Replace("£", "").Trim()
        End If
        If IsNothing(row("CommVatContent")) OrElse String.IsNullOrWhiteSpace(row("CommVatContent").ToString) Then
            row("CommVatContent") = 0.0
        Else
            row("CommVatContent") = row("CommVatContent").ToString().Replace("£", "").Trim()
        End If
        If IsNothing(row("DomVatContent")) OrElse String.IsNullOrWhiteSpace(row("DomVatContent").ToString) Then
            row("DomVatContent") = 0.0
        Else
            row("DomVatContent") = row("DomVatContent").ToString().Replace("£", "").Trim()
        End If
        If IsNothing(row("Standing Chrg")) OrElse String.IsNullOrWhiteSpace(row("Standing Chrg").ToString) Then
            row("Standing Chrg") = 0.0
        Else
            row("Standing Chrg") = row("Standing Chrg").ToString().Replace("£", "").Trim()
        End If
        If IsNothing(row("SC VAT")) OrElse String.IsNullOrWhiteSpace(row("SC VAT").ToString) Then
            row("SC VAT") = 0.0
        Else
            row("SC VAT") = row("SC VAT").ToString().Replace("£", "").Trim()
        End If
        If IsNothing(row("CC Levey")) OrElse String.IsNullOrWhiteSpace(row("CC Levey").ToString) Then
            row("CC Levey") = 0.0
        Else
            row("CC Levey") = row("CC Levey").ToString().Replace("£", "").Trim()
        End If
        If IsNothing(row("CCL VAT")) OrElse String.IsNullOrWhiteSpace(row("CCL VAT").ToString) Then
            row("CCL VAT") = 0.0
        Else
            row("CCL VAT") = row("CCL VAT").ToString().Replace("£", "").Trim()
        End If
    End Sub

    Private Shared Sub PopulateGasConsumption(ByRef row As DataRow)
        Dim commVat, domVat As New Double

        If IsNothing(row("GasCost")) OrElse String.IsNullOrWhiteSpace(row("GasCost").ToString) Then
            row("Net") = 0.0
        Else
            row("Net") = row("GasCost").ToString().Replace("£", "")
        End If
        If IsNothing(row("CommVatContent")) OrElse String.IsNullOrWhiteSpace(row("CommVatContent").ToString) Then
            row("CommVatContent") = 0.0
            commVat = 0.0
        Else
            Double.TryParse(row("CommVatContent"), commVat)
        End If
        If IsNothing(row("DomVatContent")) OrElse String.IsNullOrWhiteSpace(row("DomVatContent").ToString) Then
            row("DomVatContent") = 0.0
            domVat = 0.0
        Else
            Double.TryParse(row("DomVatContent"), domVat)
        End If
        row("Vat") = commVat + domVat
        row("Tariff") = "Gas Consumption"
        row("Description") = "Gas Consumption"
        PopulateGross(row)
    End Sub

    Private Shared Sub PopulateStandardCharge(ByRef row As DataRow)
        If IsNothing(row("Standing Chrg")) OrElse String.IsNullOrWhiteSpace(row("Standing Chrg").ToString) Then
            row("Net") = 0.0
        Else
            row("Net") = row("Standing Chrg").ToString().Replace("£", "")
        End If
        If IsNothing(row("SC VAT")) OrElse String.IsNullOrWhiteSpace(row("SC VAT").ToString) Then
            row("Vat") = 0.0
        Else
            row("Vat") = row("SC VAT").ToString().Replace("£", "")
        End If
        row("Tariff") = "Standing Charge"
        row("Description") = "Standing Charge"
        PopulateGross(row)
    End Sub

    Private Shared Sub PopulateClimateChange(ByRef row As DataRow)
        If IsNothing(row("CC Levey")) OrElse String.IsNullOrWhiteSpace(row("CC Levey").ToString) Then
            row("Net") = 0.0
        Else
            row("Net") = row("CC Levey").ToString().Replace("£", "")
        End If
        If IsNothing(row("CCL VAT")) OrElse String.IsNullOrWhiteSpace(row("CCL VAT").ToString) Then
            row("Vat") = 0.0
        Else
            row("Vat") = row("CCL VAT").ToString().Replace("£", "")
        End If
        row("Tariff") = "Climate Change Levy"
        row("Description") = "Climate Change Levy"
        PopulateGross(row)
    End Sub

    Private Shared Sub PopulateGross(ByRef row As DataRow)
        Dim vat, net As New Double
        Double.TryParse(row("Net"), net)
        Double.TryParse(row("Vat"), vat)
        row("Gross") = net + vat
    End Sub

    Private Shared Sub PopulateCostOnly(ByRef row As DataRow)
        row("CostOnly") = "No"
    End Sub

    Private Shared Sub PopulateForceKwh(ByRef row As DataRow)
        row("ForcekWh") = "Yes"
    End Sub

    Private Shared Sub PopulateIsLastDayApportion(row As DataRow)
        row("IsLastDayApportion") = "No"
    End Sub

End Class
