﻿Public Class DigitalEnergyInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel
        Dim xSheet As ExcelSheet

        Try
            excel.Load(fullPath)

            xSheet = Excel.Sheets(0)

            'try to find "Invoice Format" Sheet
            excel.GetSheet("Invoice", xSheet)

            xSheet.HeaderRowIndex = 1
            _returnTable = xSheet.GetDataTable()

        Catch ex As Exception
            Trace.WriteLine("Load Data : " & ex.Message & "," & ex.StackTrace)
        End Try
    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property
    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        RenameColumns()
        ColumnValidate()
        AddColumns()

        For Each row As DataRow In _returnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub

  
End Class
