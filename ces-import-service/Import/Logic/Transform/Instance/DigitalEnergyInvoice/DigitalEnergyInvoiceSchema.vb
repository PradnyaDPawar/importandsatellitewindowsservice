﻿Imports System.Globalization

Partial Public Class DigitalEnergyInvoice

    Private Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        Try

            headers.Add("InvoiceNumber")
            headers.Add("MeterSerial")
            headers.Add("MPAN")
            headers.Add("Tariff")
            headers.Add("StartDate")
            headers.Add("EndDate")
            headers.Add("StartRead")
            headers.Add("StartReadEstimated")
            headers.Add("EndRead")
            headers.Add("EndReadEstimated")
            headers.Add("Consumption")
            headers.Add("ForcekWh")
            headers.Add("Vat")
            headers.Add("Net")
            headers.Add("Gross")
            headers.Add("CostOnly")
            headers.Add("Description")
            headers.Add("TaxPointDate")
            headers.Add("IsLastDayApportion")
            'headers.Add("Supplier")
            'headers.Add("Batch Name")
            'headers.Add("Iteration")
            'headers.Add("Import Date")

        Catch ex As Exception
            Trace.WriteLine("Get Headers : " & ex.Message & "," & ex.StackTrace)
        End Try
        Return headers
    End Function

    Private Sub ColumnValidate()
        Dim rawHeaders As List(Of String) = GetHeaders()
        Try
            For Each header As String In rawHeaders
                If Not _returnTable.Columns.Contains(header) Then
                    Throw New ApplicationException("Missing Column " & header & ". ")
                End If
            Next
        Catch ex As Exception
            Trace.WriteLine("Column Validate : " & ex.Message & "," & ex.StackTrace)
        End Try

    End Sub

    Private Sub RenameColumns()

        Try
            For Each column As DataColumn In _returnTable.Columns
                If column.ColumnName.ToLower.Replace(" ", "") = "invoicenumber" Then
                    column.ColumnName = "InvoiceNumber"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "meterserial" Then
                    column.ColumnName = "MeterSerial"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "startdate" Then
                    column.ColumnName = "StartDate"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "enddate" Then
                    column.ColumnName = "EndDate"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "startread" Then
                    column.ColumnName = "StartRead"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "startreadestimated" Then
                    column.ColumnName = "StartReadEstimated"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "endread" Then
                    column.ColumnName = "EndRead"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "endreadestimated" Then
                    column.ColumnName = "EndReadEstimated"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "costonly" Then
                    column.ColumnName = "CostOnly"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "taxpointdate" Then
                    column.ColumnName = "TaxPointDate"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "forcekwh" Then
                    column.ColumnName = "ForcekWh"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "islastdayapportion" Then
                    column.ColumnName = "IsLastDayApportion"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "invoicedate" Then
                    column.ColumnName = "TaxPointDate"
                End If
                If column.ColumnName.ToLower = "rate" Then
                    column.ColumnName = "Tariff"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "startreadtype" Then
                    column.ColumnName = "StartReadEstimated"
                End If
                If column.ColumnName.ToLower.Replace(" ", "") = "endreadtype" Then
                    column.ColumnName = "EndReadEstimated"
                End If
            Next
        Catch ex As Exception
            Trace.WriteLine("Rename Columns : " & ex.Message & "," & ex.StackTrace)
        End Try

    End Sub

    Private Sub AddColumns()
        If Not _returnTable.Columns.Contains("Invoice Rate") Then
            _returnTable.Columns.Add("InvoiceRate")
        Else
            _returnTable.Columns("Invoice Rate").ColumnName = "InvoiceRate"
        End If
        If Not _returnTable.Columns.Contains("Cost Unit") Then
            _returnTable.Columns.Add("CostUnit")
        Else
            _returnTable.Columns("Cost Unit").ColumnName = "CostUnit"
        End If
        If Not _returnTable.Columns.Contains("Is Transmission Tariff") Then
            _returnTable.Columns.Add("IsTransmissionTariff")
        Else
            _returnTable.Columns("Is Transmission Tariff").ColumnName = "IsTransmissionTariff"
        End If
    End Sub
End Class