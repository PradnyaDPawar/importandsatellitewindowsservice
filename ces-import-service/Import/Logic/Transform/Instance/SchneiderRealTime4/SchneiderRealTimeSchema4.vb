﻿Public Class SchneiderRealTimeSchema4

    Public Shared Function ToDeFormat(ByRef dt As DataTable)
        DeleteRedundantRows(dt)
        DataTableTools.RemoveBlankColumns(dt)
        RemoveDuplicateColumns(dt)
        NameHeaderColumns(dt)
        DataTableTools.SetHeaders(dt, 2)
        Dim rtDt As DataTable = DeFormatTableTemplates.CreateRealTimeTable
        CreateRealTimeEntries(dt, rtDt)
        dt = rtDt
        Return dt
    End Function
    Public Shared Function RemoveDuplicateColumns(ByRef dt As DataTable) As DataTable
        Dim tempDataCC As New List(Of String)
        For colcount As Integer = dt.Columns.Count - 1 To 0 Step -1
            If Not tempDataCC.Contains(dt.Rows(1)(colcount).ToString()) Then
                tempDataCC.Add(dt.Rows(1)(colcount).ToString())
            Else
                dt.Columns.RemoveAt(colcount)
            End If
        Next
        Return dt
    End Function
    Public Shared Sub DeleteRedundantRows(ByRef dt As DataTable)

        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            If Not row(0).ToString.ToLower.Trim.Replace(" ", "") = "timestamp" Then
                row.Delete()
            Else
                Exit For
            End If

        Next

    End Sub

    Public Shared Sub NameHeaderColumns(ByRef dt As DataTable)
        'Naming the columns in a row which is to be set a header row
        dt.Rows(1)(0) = "TimeStamp"

        For Each column As DataColumn In dt.Columns

            If dt.Rows(1)(column) <> "TimeStamp" Then
                dt.Rows(1)(column) = dt.Rows(1)(column).ToString.Split("/")(0).Substring(0, dt.Rows(1)(column).ToString.Split("/")(0).Length - 3)
            End If

        Next

    End Sub


    Public Shared Sub CreateRealTimeEntries(ByVal dt As DataTable, ByRef rtDt As DataTable)

        For Each column As DataColumn In dt.Columns

            If Not column.ColumnName.ToLower.Trim.Replace(" ", "") = "timestamp" Then

                For Each row As DataRow In dt.Rows

                    If IsDate(row(0)) Then

                        Dim newRtRow As DataRow = rtDt.NewRow

                        newRtRow("DateTime") = row(0)
                        newRtRow("MeterName") = column.ColumnName
                        newRtRow("CountUnits") = row(column.ColumnName)
                        rtDt.Rows.Add(newRtRow)

                    End If

                Next

            End If

        Next


    End Sub




End Class
