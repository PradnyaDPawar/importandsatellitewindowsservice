﻿Public Class EonFormat2HalfHourSchema


    Public Shared Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)

        headers.Add("Description")
        headers.Add("MeterSerial")
        headers.Add("Date(DD/MM/YYYY)")
        headers.Add("Unit")
        headers.Add("00:00")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")

        Return headers
    End Function

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)
        RenameHeaders(GetHeaders(), dt)
        dt.Columns.Remove("Description")
        dt.Columns.Remove("Unit")
        DataTableTools.RemoveBlankRows(dt)
        AddRequiredHeaders(dt)
        PopulateColumns(dt)



    End Sub


    Private Shared Sub RenameHeaders(ByVal headers As List(Of String), ByRef dt As DataTable)
        For Each column As DataColumn In dt.Columns
            column.ColumnName = headers.ElementAt(dt.Columns.IndexOf(column))
        Next
    End Sub


    Private Shared Sub AddRequiredHeaders(ByRef dt As DataTable)

        dt.Columns.Add("MPAN")
        dt.Columns.Add("ForcekWh")

    End Sub

    Private Shared Sub PopulateColumns(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows
            row("ForcekWh") = "No"
        Next

    End Sub


End Class
