﻿Partial Public Class CycleHalfHour

    Public Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("MPAN")
        headers.Add("MeterSerial")
        headers.Add("Unit")
        headers.Add("A/E")
        headers.Add("Date(DD/MM/YYYY)")
        headers.Add("00:00")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")
        headers.Add("Spare1")
        headers.Add("Spare2")
        headers.Add("Spare3")

        Return headers

    End Function


    Private Sub AddColumns()
        If Not _returnTable.Columns.Contains("forcekwh") Then
            _returnTable.Columns.Add("forcekwh")
        End If
    End Sub


    Private Sub FillForceKwh(ByRef row As DataRow)
        If row("Unit").ToString.ToLower.Trim = "kwh" Then
            row("forcekwh") = "Yes"
        Else
            row("forcekwh") = "No"
        End If
    End Sub

    Private Sub RemoveNonConsumptionRows(ByRef row As DataRow)
        If Not IsDate(row("Date(DD/MM/YYYY)")) Then
            _returnTable.Rows.Remove(row)
            _returnTable.AcceptChanges()
        End If
    End Sub


End Class
