﻿Public Class CarloGavazzi2RealTime
    Implements ITransformDelegate

    Private _returnTable As DataTable

    '
    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvFile As New DelimitedFile(fullPath, 100)
        csvFile.Delimiter = ";"
        csvFile.HasHeader = True
        csvFile.LoadNonConsistentTable(_returnTable, CarloGavazzi2RealTimeSchema.GetHeaders(100))
    End Sub



    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        CarloGavazzi2RealTimeSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_RealTime.Fill(row, dsEdits, importEdit)
        Next


    End Sub

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property
End Class


