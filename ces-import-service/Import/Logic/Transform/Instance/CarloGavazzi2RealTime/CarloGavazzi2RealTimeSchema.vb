﻿Imports System.Globalization

Public Class CarloGavazzi2RealTimeSchema


    Public Shared Function GetHeaders(ByVal columnCount As Integer) As List(Of String)
        Dim headers As New List(Of String)

        For index = 1 To columnCount
            headers.Add("Header" + index.ToString())
        Next
        Return headers
    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        DataTableTools.RemoveBlankRows(dt)
        dt.Rows.RemoveAt(0)
        RenameColumns(dt)
        Dim realTimeDt As DataTable = DeFormatTableTemplates.CreateRealTimeTable()
        Dim dtResult As DataTable = CreateTable(dt)
        PopulateColumns(dtResult, realTimeDt)
        dt = dtResult


        Return dt

    End Function
    ''' <summary>
    ''' renaming columns of input table
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <remarks></remarks>
    Public Shared Sub RenameColumns(ByRef dt As DataTable)
        dt.Columns("Header1").ColumnName = "filter"
        dt.Columns("Header4").ColumnName = "address"
        dt.Columns("Header7").ColumnName = "UnixTimeStamp"
        dt.Columns("Header8").ColumnName = "timestamp"
        dt.Columns("Header9").ColumnName = "kwh"
        dt.Columns("Header52").ColumnName = "C1"
        dt.Columns("Header53").ColumnName = "C2"
        dt.Columns("Header54").ColumnName = "C3"
    End Sub
    ''' <summary>
    ''' Method ctreated to get datatable in realtime format 
    ''' Here one row in input table has 4 meterIdentifiers  columns with their count units that are to be formed as row  in resulting table
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function CreateTable(ByRef dt As DataTable)
        ''new datatables that contains rows with Meter identifiers as _kwh,_C1,_C2,C3 (from 1 AC row)
        Dim dtTranspose As DataTable = New DataTable()
        Dim convertedDate As DateTime = New DateTime()
        dtTranspose.Columns.Add("MeterName", GetType(String))
        dtTranspose.Columns.Add("DateTime", GetType(String))
        dtTranspose.Columns.Add("CountUnits", GetType(String))
        For Each row As DataRow In dt.Rows
            ''bug fix:3993 added variable to convert  unixtimestamp value to datetime
            convertedDate = New DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(row("UnixTimeStamp"))

            If (row("filter") = "AC") Then
                For Each col As DataColumn In dt.Columns
                    If Not IsDBNull(row(col.ColumnName)) Then
                        If Not String.IsNullOrEmpty(row(col.ColumnName)) Then
                            Dim newRow As DataRow = dtTranspose.NewRow

                            If (col.ColumnName = "kwh" Or col.ColumnName = "C1" Or col.ColumnName = "C2" Or col.ColumnName = "C3") Then

                                newRow("DateTime") = convertedDate.ToString("dd/MM/yyyy HH:mm")
                                newRow("MeterName") = row("address") + "_" + col.ColumnName
                                newRow("CountUnits") = row(col.ColumnName)

                                dtTranspose.Rows.Add(newRow)
                            End If
                        End If
                    End If
                Next
            End If
        Next
        Return dtTranspose
    End Function


    Public Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)

        Dim success As Boolean = False


        For Each row As DataRow In dt.Rows

            If Not String.IsNullOrEmpty(row("DateTime")) Then

                For Each Rrow As DataRow In realTimeDt.Rows

                    If Rrow("MeterName") = row("MeterName") And Rrow("DateTime") = row("DateTime") Then

                        Rrow("CountUnits") = row("CountUnits")
                        success = True

                    End If

                Next


                If Not success Then
                    Dim realTimeDtRow As DataRow = realTimeDt.NewRow

                    realTimeDtRow("MeterName") = row("MeterName")
                    realTimeDtRow("DateTime") = row("DateTime")
                    realTimeDtRow("CountUnits") = row("CountUnits")

                    realTimeDt.Rows.Add(realTimeDtRow)
                End If


                success = False

            End If
        Next

        dt = realTimeDt

    End Sub

End Class