﻿Public Class ScottishWaterInvoiceSchema

    Private _customerRef As String
    Private _waterMpan As String
    Private _wasteWaterMpan As String
    Private _rv As Double
    Private _returnToSewer As Double

#Region "Properties"

    Property CustomerRef() As String
        Get
            Return _customerRef
        End Get
        Set(ByVal value As String)
            _customerRef = value
        End Set
    End Property

    Property WaterMpan() As String
        Get
            Return _waterMpan
        End Get
        Set(ByVal value As String)
            _waterMpan = value
        End Set
    End Property

    Property WasteWaterMpan() As String
        Get
            Return _wasteWaterMpan
        End Get
        Set(ByVal value As String)
            _wasteWaterMpan = value
        End Set
    End Property

    Property Rv() As Double
        Get
            Return _rv
        End Get
        Set(ByVal value As Double)
            _rv = value
        End Set
    End Property

    Property ReturnToSewer() As Double
        Get
            Return _returnToSewer
        End Get
        Set(ByVal value As Double)
            _returnToSewer = value
        End Set
    End Property

#End Region


    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("Bill")
        headers.Add("Service")
        headers.Add("Date Posted")
        headers.Add("Period From")
        headers.Add("Period To")
        headers.Add("Meter size (Actual)")
        headers.Add("Meter size (Billed)")
        headers.Add("Days Charged")
        headers.Add("Consumption")
        headers.Add("Unit Cost")
        headers.Add("Net (£)")
        headers.Add("VAT (£)")
        headers.Add("Total (£)")


        Return headers

    End Function

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        Dim sc As New ScottishWaterInvoiceSchema
        sc.ToDeFormatInternal(dt)

    End Sub

    Public Sub ToDeFormatInternal(ByRef dt As DataTable)

        Dim invoiceDt As DataTable = DeFormatTableTemplates.CreateInvoiceTable
        Dim meterReadingsDt As New DataTable
        Dim costDetailsDt As New DataTable
        For Each row As DataRow In dt.Rows

            'Assign current values to Properties
            If row(1).ToString.Trim.ToLower.Replace(" ", "") = "customerref" Then
                CustomerRef = row(3)
            End If

            If row(1).ToString.Trim.ToLower.Replace(" ", "") = "waterspid" Then
                WaterMpan = row(3)
            End If

            If row(1).ToString.Trim.ToLower.Replace(" ", "") = "wastewaterspid" Then
                WasteWaterMpan = row(3)
            End If

            If row(1).ToString.Trim.ToLower.Replace(" ", "") = "rv" Then
                Rv = row(3)
            End If

            If row(1).ToString.Trim.ToLower.Replace(" ", "") = "returntosewer" Then

                If Not Double.TryParse(row(3), ReturnToSewer) Then
                    ReturnToSewer = 0
                End If
                'ReturnToSewer = row(3)
            End If

            If row(1).ToString.Trim = "Meter serial number" Then
                meterReadingsDt = GetMeterReadingsDt(row)
            End If

            If row(1).ToString.Trim = "Bill" Then
                costDetailsDt = GetCostDetails(row)
            End If

            If costDetailsDt.Rows.Count > 0 Then
                PopulateInvoiceDt(meterReadingsDt, costDetailsDt, invoiceDt)
                meterReadingsDt.Clear()
                costDetailsDt.Clear()
            End If


        Next

        dt = invoiceDt
    End Sub

    Private Sub PopulateInvoiceDt(ByVal meterReadingsDt As DataTable, ByVal costDt As DataTable, ByRef invoiceDt As DataTable)


        ' Add cost only rows. MPAN level
        For Each row As DataRow In costDt.Rows

            If Not (row("Service").ToString.ToLower.Replace(" ", "") = "volumetricwatercharge-metered" _
            Or row("Service").ToString.ToLower.Replace(" ", "") = "volumetricwastewatercharge-metered" _
            Or row("Service").ToString.ToLower.Replace(" ", "") = "waterrvcharge-un-metered" _
            Or row("Service").ToString.ToLower.Replace(" ", "") = "volumetricwastewatercharge-un-metered") Then

                Dim newInvoiceDtRow As DataRow = invoiceDt.NewRow
                newInvoiceDtRow("Invoice Number") = CustomerRef & "-" & row("Bill")
                If meterReadingsDt.Rows.Count > 0 Then
                    newInvoiceDtRow("Meter Serial") = meterReadingsDt.Rows(0)("Meter Serial")
                End If


                If row("Service").ToString.ToLower.Contains("waste") Or _
                row("Service").ToString.ToLower.Contains("drainage") Then
                    newInvoiceDtRow("MPAN") = WasteWaterMpan
                Else
                    newInvoiceDtRow("MPAN") = WaterMpan
                End If

                newInvoiceDtRow("Rate") = "Water"
                newInvoiceDtRow("Start Date") = row("Period From")
                newInvoiceDtRow("End Date") = row("Period To")
                newInvoiceDtRow("Force kWh") = "No"
                Dim consumption As Double
                If Double.TryParse(row("Consumption"), consumption) Then
                    newInvoiceDtRow("Consumption") = consumption
                End If
                newInvoiceDtRow("Vat") = row("Vat")
                newInvoiceDtRow("Net") = row("Net")
                newInvoiceDtRow("Gross") = row("Total")
                newInvoiceDtRow("Cost Only") = "Yes"
                newInvoiceDtRow("Description") = row("Service")
                invoiceDt.Rows.Add(newInvoiceDtRow)

            Else

                Dim newInvoiceDtRow As DataRow = invoiceDt.NewRow
                newInvoiceDtRow("Invoice Number") = CustomerRef & "-" & row("Bill")
                If meterReadingsDt.Rows.Count > 0 Then
                    newInvoiceDtRow("Meter Serial") = meterReadingsDt.Rows(0)("Meter Serial")
                End If


                If row("Service").ToString.ToLower.Contains("waste") Or _
                row("Service").ToString.ToLower.Contains("drainage") Then
                    newInvoiceDtRow("MPAN") = WasteWaterMpan
                Else
                    newInvoiceDtRow("MPAN") = WaterMpan
                End If

                newInvoiceDtRow("Rate") = "Water"
                newInvoiceDtRow("Start Date") = row("Period From")
                newInvoiceDtRow("End Date") = row("Period To")
                newInvoiceDtRow("Force kWh") = "No"
                newInvoiceDtRow("Consumption") = row("Consumption")

                newInvoiceDtRow("Vat") = row("Vat")
                newInvoiceDtRow("Net") = row("Net")
                newInvoiceDtRow("Gross") = row("Total")
                newInvoiceDtRow("Cost Only") = "No"
                newInvoiceDtRow("Description") = row("Service")
                invoiceDt.Rows.Add(newInvoiceDtRow)


            End If

        Next

        'Add consumption rows


        'For rowCount = 0 To meterReadingsDt.Rows.Count - 1 Step 2
        '    Dim startRead, endRead As Integer

        '    Dim newInvoiceDtRow As DataRow = invoiceDt.NewRow

        '    'Water Consumption and Charge

        '    newInvoiceDtRow("Invoice Number") = CustomerRef & "-" & costDt.Rows(0)("Bill")
        '    newInvoiceDtRow("Meter Serial") = meterReadingsDt(rowCount)("Meter Serial")
        '    'newInvoiceDtRow("MPAN") = ""

        '    newInvoiceDtRow("Rate") = "Water"
        '    newInvoiceDtRow("End Date") = meterReadingsDt(rowCount)("Reading Date")
        '    newInvoiceDtRow("End Read") = meterReadingsDt(rowCount)("Reading")
        '    newInvoiceDtRow("End Read Estimated") = meterReadingsDt(rowCount)("Reading Type")
        '    endRead = newInvoiceDtRow("End Read")

        '    newInvoiceDtRow("Start Date") = meterReadingsDt(rowCount + 1)("Reading Date")
        '    newInvoiceDtRow("Start Read") = meterReadingsDt(rowCount + 1)("Reading")
        '    newInvoiceDtRow("Start Read Estimated") = meterReadingsDt(rowCount + 1)("Reading Type")
        '    startRead = newInvoiceDtRow("Start Read")

        '    newInvoiceDtRow("Consumption") = endRead - startRead
        '    newInvoiceDtRow("Vat") = 0
        '    newInvoiceDtRow("Net") = (endRead - startRead) * GetUnitCharge(costDt, "Service", "volumetricwatercharge-metered")
        '    newInvoiceDtRow("Gross") = newInvoiceDtRow("Net")
        '    newInvoiceDtRow("Cost Only") = "No"
        '    newInvoiceDtRow("Description") = "Volumetric water charge - metered"
        '    invoiceDt.Rows.Add(newInvoiceDtRow)

        '    'Waste Water Consumption and Charge

        '    Dim newInvoiceDtRow1 As DataRow = invoiceDt.NewRow


        '    newInvoiceDtRow1("Invoice Number") = CustomerRef & "-" & costDt.Rows(0)("Bill")
        '    newInvoiceDtRow1("Meter Serial") = meterReadingsDt(rowCount)("Meter Serial")
        '    'newInvoiceDtRow("MPAN") = ""

        '    newInvoiceDtRow1("Rate") = "Water"
        '    newInvoiceDtRow1("End Date") = meterReadingsDt(rowCount)("Reading Date")
        '    newInvoiceDtRow1("End Read") = meterReadingsDt(rowCount)("Reading")
        '    newInvoiceDtRow1("End Read Estimated") = meterReadingsDt(rowCount)("Reading Type")
        '    endRead = newInvoiceDtRow1("End Read")

        '    newInvoiceDtRow1("Start Date") = meterReadingsDt(rowCount + 1)("Reading Date")
        '    newInvoiceDtRow1("Start Read") = meterReadingsDt(rowCount + 1)("Reading")
        '    newInvoiceDtRow1("Start Read Estimated") = meterReadingsDt(rowCount + 1)("Reading Type")
        '    startRead = newInvoiceDtRow1("Start Read")

        '    newInvoiceDtRow1("Consumption") = (ReturnToSewer / 100) * (endRead - startRead)
        '    newInvoiceDtRow1("Vat") = 0


        '    newInvoiceDtRow1("Net") = newInvoiceDtRow1("Consumption") * GetUnitCharge(costDt, "Service", "volumetricwastewatercharge-metered")
        '    newInvoiceDtRow1("Gross") = newInvoiceDtRow1("Net")
        '    newInvoiceDtRow1("Cost Only") = "No"
        '    newInvoiceDtRow1("Description") = "Volumetric waste water charge - metered"
        '    invoiceDt.Rows.Add(newInvoiceDtRow1)



        'Next



    End Sub

    Private Shared Function GetUnitCharge(ByVal lookupDt As DataTable, ByVal lookupColumn As String, ByVal charge As String) As Double

        For Each row As DataRow In lookupDt.Rows

            If row(lookupColumn).ToString.ToLower.Trim.Replace(" ", "") = charge Then

                Return row("Unit Cost")

            End If

        Next

        Return -1

    End Function


    Private Shared Function CheckIfMultipleMeters(ByVal meterReadingsDt As DataTable) As Boolean

        Dim meter1 As String = meterReadingsDt.Rows(0)("Meter Serial")

        For Each row As DataRow In meterReadingsDt.Rows

            If row("Meter Serial") <> meter1 Then
                Return True
            End If

        Next

        Return False

    End Function

    Private Shared Function GetMeterReadingsDt(ByVal row As DataRow) As DataTable
        Dim endOfMeterReads As Boolean = False
        Dim count As Integer = row.Table.Rows.IndexOf(row) + 1

        Dim meterReadsDt As New DataTable
        meterReadsDt.Columns.Add("Meter Serial")
        meterReadsDt.Columns.Add("Reading Date", GetType(Date))
        meterReadsDt.Columns.Add("Reading Type")
        meterReadsDt.Columns.Add("Reading")

        While (count <= row.Table.Rows.Count) And Not endOfMeterReads

            If IsDate(row.Table.Rows(count)(8)) Then

                Dim meterReadsDtRow As DataRow = meterReadsDt.NewRow

                meterReadsDtRow("Meter Serial") = row.Table.Rows(count)(1)
                meterReadsDtRow("Reading Date") = row.Table.Rows(count)(8)
                meterReadsDtRow("Reading Type") = row.Table.Rows(count)(12)
                meterReadsDtRow("Reading") = row.Table.Rows(count)(14)

                meterReadsDt.Rows.Add(meterReadsDtRow)

            End If

            If String.IsNullOrEmpty(row.Table.Rows(count + 1)(1)) Then
                endOfMeterReads = True
            End If

            count += 1
        End While


        Return meterReadsDt

    End Function

    Private Shared Function GetCostDetails(ByVal row As DataRow) As DataTable

        Dim endOfCostDetails As Boolean = False
        Dim count As Integer = row.Table.Rows.IndexOf(row) + 1

        Dim CostsDt As New DataTable
        CostsDt.Columns.Add("Bill")
        CostsDt.Columns.Add("Service")
        CostsDt.Columns.Add("Date Posted")
        CostsDt.Columns.Add("Period From")
        CostsDt.Columns.Add("Period To")
        CostsDt.Columns.Add("Days Charged")
        CostsDt.Columns.Add("Consumption")
        CostsDt.Columns.Add("Unit Cost")
        CostsDt.Columns.Add("Net")
        CostsDt.Columns.Add("Vat")
        CostsDt.Columns.Add("Total")

        While (count <= row.Table.Rows.Count) And Not endOfCostDetails



            Dim CostsDtRow As DataRow = CostsDt.NewRow

            CostsDtRow("Bill") = row.Table.Rows(count)(1)
            CostsDtRow("Service") = row.Table.Rows(count)(2)
            CostsDtRow("Date Posted") = row.Table.Rows(count)(5)
            CostsDtRow("Period From") = row.Table.Rows(count)(6)
            CostsDtRow("Period To") = row.Table.Rows(count)(7)
            CostsDtRow("Days Charged") = row.Table.Rows(count)(10)
            CostsDtRow("Consumption") = row.Table.Rows(count)(11)
            CostsDtRow("Unit Cost") = row.Table.Rows(count)(12)
            CostsDtRow("Net") = row.Table.Rows(count)(13)
            CostsDtRow("Vat") = row.Table.Rows(count)(14)
            CostsDtRow("Total") = row.Table.Rows(count)(15)

            CostsDt.Rows.Add(CostsDtRow)



            If String.IsNullOrEmpty(row.Table.Rows(count + 1)(1)) Then
                endOfCostDetails = True
            End If

            count += 1
        End While

        Return CostsDt

    End Function

End Class
