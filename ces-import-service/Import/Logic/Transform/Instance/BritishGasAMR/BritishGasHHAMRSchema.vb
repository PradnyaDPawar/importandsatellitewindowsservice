﻿Public Class BritishGasHHAMRSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable)
        'DataTableTools.SetHeaders(dt, 1)
        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable
        CreateHalfHourlyEntries(dt, hhDataTable)
        dt = hhDataTable
        Return dt
    End Function

    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDatatable As DataTable)


        Dim dateholder As DateTime
        Dim success As Boolean = False
        For Each row As DataRow In dt.Rows

            If IsDate(row("Date/Month/Day/Time")) Then
                dateholder = row("Date/Month/Day/Time")
                For Each hhrow As DataRow In hhDatatable.Rows
                    If hhrow("Date(dd/mm/yyyy)") = dateholder.Date And hhrow("MPAN") = row("Meter Reference") Then
                        hhrow(dateholder.ToString("HH:mm")) = row("Energy Consumed (kWh)")
                        success = True
                    End If
                Next

                If Not success Then
                    Dim newRow As DataRow = hhDatatable.NewRow
                    newRow("Date(dd/mm/yyyy)") = dateholder.Date.ToShortDateString
                    newRow(dateholder.ToString("HH:mm")) = row("Energy Consumed (kWh)")
                    newRow("MPAN") = row("Meter Reference")
                    newRow("ForcekWh") = "Yes"

                    hhDatatable.Rows.Add(newRow)
                End If
            End If
            success = False
        Next
        dt = hhDatatable

    End Sub

End Class
