﻿Public Class EonHalfHourSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("Site Name")
        headers.Add("Mpan")
        headers.Add("Date")
        headers.Add("Actual_Estimate_Indicators")
        headers.Add("HH_01")
        headers.Add("HH_02")
        headers.Add("HH_03")
        headers.Add("HH_04")
        headers.Add("HH_05")
        headers.Add("HH_06")
        headers.Add("HH_07")
        headers.Add("HH_08")
        headers.Add("HH_09")
        headers.Add("HH_10")
        headers.Add("HH_11")
        headers.Add("HH_12")
        headers.Add("HH_13")
        headers.Add("HH_14")
        headers.Add("HH_15")
        headers.Add("HH_16")
        headers.Add("HH_17")
        headers.Add("HH_18")
        headers.Add("HH_19")
        headers.Add("HH_20")
        headers.Add("HH_21")
        headers.Add("HH_22")
        headers.Add("HH_23")
        headers.Add("HH_24")
        headers.Add("HH_25")
        headers.Add("HH_26")
        headers.Add("HH_27")
        headers.Add("HH_28")
        headers.Add("HH_29")
        headers.Add("HH_30")
        headers.Add("HH_31")
        headers.Add("HH_32")
        headers.Add("HH_33")
        headers.Add("HH_34")
        headers.Add("HH_35")
        headers.Add("HH_36")
        headers.Add("HH_37")
        headers.Add("HH_38")
        headers.Add("HH_39")
        headers.Add("HH_40")
        headers.Add("HH_41")
        headers.Add("HH_42")
        headers.Add("HH_43")
        headers.Add("HH_44")
        headers.Add("HH_45")
        headers.Add("HH_46")
        headers.Add("HH_47")
        headers.Add("HH_48")


        Return headers
    End Function

    Public Shared Function GetHeadersXls() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("Company-Site")
        headers.Add("Mpan")
        headers.Add("Date")
        headers.Add("Actual_Estimate_Indicators")
        headers.Add("HH_01")
        headers.Add("HH_02")
        headers.Add("HH_03")
        headers.Add("HH_04")
        headers.Add("HH_05")
        headers.Add("HH_06")
        headers.Add("HH_07")
        headers.Add("HH_08")
        headers.Add("HH_09")
        headers.Add("HH_10")
        headers.Add("HH_11")
        headers.Add("HH_12")
        headers.Add("HH_13")
        headers.Add("HH_14")
        headers.Add("HH_15")
        headers.Add("HH_16")
        headers.Add("HH_17")
        headers.Add("HH_18")
        headers.Add("HH_19")
        headers.Add("HH_20")
        headers.Add("HH_21")
        headers.Add("HH_22")
        headers.Add("HH_23")
        headers.Add("HH_24")
        headers.Add("HH_25")
        headers.Add("HH_26")
        headers.Add("HH_27")
        headers.Add("HH_28")
        headers.Add("HH_29")
        headers.Add("HH_30")
        headers.Add("HH_31")
        headers.Add("HH_32")
        headers.Add("HH_33")
        headers.Add("HH_34")
        headers.Add("HH_35")
        headers.Add("HH_36")
        headers.Add("HH_37")
        headers.Add("HH_38")
        headers.Add("HH_39")
        headers.Add("HH_40")
        headers.Add("HH_41")
        headers.Add("HH_42")
        headers.Add("HH_43")
        headers.Add("HH_44")
        headers.Add("HH_45")
        headers.Add("HH_46")
        headers.Add("HH_47")
        headers.Add("HH_48")


        Return headers
    End Function

    Public Shared Function Validate(ByRef data As DataTable) As Boolean


        For Each header As String In GetHeaders()
            If Not data.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next

    End Function

    Public Shared Function ValidateXls(ByRef data As DataTable) As Boolean


        For Each header As String In GetHeadersXls()
            If Not data.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next

    End Function



    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

        RenameColumns(dt)
        CreateColumns(dt)
        PopulateForceKwh(dt)
        ReformatDateColumn(dt)
        RenameHalfHourlyEntries(dt)


        Return dt

    End Function

    Private Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("Mpan").ColumnName = "MPAN"
        dt.Columns("Date").ColumnName = "Date(DD/MM/YYYY)"

    End Sub

    Private Shared Sub CreateColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")
        dt.Columns.Add("MeterSerial")

    End Sub

    Private Shared Sub PopulateForceKwh(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows

            row("ForcekWh") = "Yes"

        Next

    End Sub


    Private Shared Sub ReformatDateColumn(ByRef dt As DataTable)

        Dim dateholder As DateTime

        For Each row As DataRow In dt.Rows

            dateholder = row("Date(dd/mm/yyyy)")
            row("Date(dd/mm/yyyy)") = dateholder.ToShortDateString

        Next


    End Sub

    Private Shared Sub RenameHalfHourlyEntries(ByRef dt As DataTable)

        Dim dateholder As Date = DateTime.Today

        For columncount As Integer = dt.Columns("HH_01").Ordinal To dt.Columns("HH_48").Ordinal

            dt.Columns(columncount).ColumnName = dateholder.ToString("HH:mm")

            dateholder = dateholder.AddMinutes(30)

        Next

    End Sub



End Class
