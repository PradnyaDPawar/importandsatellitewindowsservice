﻿Public Class CtsHalfHourSchema2

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("MeterSerial")
        headers.Add("Date(DD/MM/YYYY)")
        headers.Add("Consumption")
        Return headers

    End Function

    Public Shared Function Validate(ByRef data As DataTable) As Boolean

        For Each header As String In GetHeaders()
            If Not data.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next

    End Function


    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

        'Create a new Data Table that is compatible with DigitalEnergy
        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()

        'Use all the values from the dt table and insert them into the hhDataTable
        'Brings back 'dt' Datatable which is now in the same format as 'hhDataTable' Datatable
        PopulateColumns(dt, hhDataTable)

        'Now insert the value 'Yes' under the Force kWh values because the consumption is in kWh
        PopulateForceKwh(dt)

        'Reformat the column 'Date (dd/mm/yyyy)' to short date string, because at the moment it contains both date and time
        ReformatDateColumn(dt)

        Return dt

    End Function


    Private Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef hhDataTable As DataTable)

        '>> Add MeterSerials and Dates Columns followed by the consumptions

        Dim success As Boolean = False  'Success Variable is to determine if both the Date(dd/mm/yyyy) and Meter Serial Row Exist.
        Dim theDate As DateTime         'Stores the Date from the dt DataTable
        Dim theTime As DateTime         'Stores the Time from the dt DataTable


        For Each row As DataRow In dt.Rows

            If IsDate(row("Date(DD/MM/YYYY)")) Then 'Just to prevent errors

                '>> Collect date and time values from the current row being processed for the next stage
                theDate = row("Date(DD/MM/YYYY)")
                theTime = theDate.ToString("HH:mm") 'Converts the dateTime to a time value
                Dim theTimeString = theTime.ToString("HH:mm") 'Creates a string version of time


                If hhDataTable.Columns.Contains(theTimeString) Then 'Just to prevent errors

                    For Each hhrow As DataRow In hhDataTable.Rows

                        '>> Check to see if there is already a Meter Serial Number with the same Date already in the DigialEnergy Formatted Table:
                        If hhrow("MeterSerial") = row("MeterSerial") Then 'Compare current row with other MeterSerials in the Table
                            If hhrow("Date(DD/MM/YYYY)") = theDate.Date Then 'Compare current date with other Dates in the Table

                                '>> There is! Therefore...
                                hhrow(theTimeString) = row("Consumption") 'Add the Consumption under the correct Time Column in the row
                                success = True 'Turn the success variable to true, to avoid adding a duplicate Meter Serial and Date row

                            End If
                        End If

                    Next


                    '>> If there are no Meter Serial Numbers with the same date of the row being processed then... 
                    If Not success Then
                        Dim newRow As DataRow = hhDataTable.NewRow      ' Create a new row
                        newRow("MeterSerial") = row("MeterSerial")    ' Add the Meter Serial Number
                        newRow("Date(DD/MM/YYYY)") = theDate.Date      ' Add the date
                        newRow(theTimeString) = row("Consumption")      ' And add the Consumption under the correct Time Column in the row.
                        hhDataTable.Rows.Add(newRow)                    ' Add the new row to the Digital Energy Formatted Table.
                    End If


                    success = False 'Reset success counter for the next row

                End If
            End If
        Next

        dt = hhDataTable

    End Sub


    Public Shared Sub PopulateForceKwh(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows
            row("ForcekWh") = "yes"
        Next

        dt.AcceptChanges()

    End Sub


    Private Shared Sub ReformatDateColumn(ByRef dt As DataTable)

        Dim dateholder As DateTime

        For Each row As DataRow In dt.Rows

            dateholder = row("Date(DD/MM/YYYY)")
            row("Date(DD/MM/YYYY)") = dateholder.ToShortDateString

        Next

    End Sub

End Class
