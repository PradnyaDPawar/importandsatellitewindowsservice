﻿Public Class CtsHalfHour2
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property


    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvfile As New DelimitedFile(fullPath, 0)

        csvfile.Load(_returnTable, CtsHalfHourSchema2.GetHeaders)

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        CtsHalfHourSchema2.Validate(_returnTable) 'Validate this table against the schema (Find if there are any missing columns)
        CtsHalfHourSchema2.ToDeFormat(_returnTable) 'Format this table into the DigitalEnergy Fomrat
        For Each row As DataRow In _returnTable.Rows
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next
    End Sub

End Class
