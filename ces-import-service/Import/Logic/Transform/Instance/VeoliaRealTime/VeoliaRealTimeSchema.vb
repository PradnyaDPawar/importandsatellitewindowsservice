﻿Public Class VeoliaRealTimeSchema
    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        Dim realTimeDt As DataTable = DeFormatTableTemplates.CreateRealTimeTable()
        PopulateColumns(dt, realTimeDt)
        Return dt

    End Function

    Public Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)
            For Each row As DataRow In dt.Rows

                If IsDate(row("SampleDateRef")) Then
                    Dim realTimeDtRow As DataRow = realTimeDt.NewRow

                    realTimeDtRow("MeterName") = row("EndPointRef")
                    realTimeDtRow("DateTime") = CDate(row("SampleDateRef")).ToString("dd/MM/yyyy HH:mm")

                    If row.Table.Columns.Contains("volume") Then
                        realTimeDtRow("CountUnits") = row("volume")
                    Else
                        If row.Table.Columns.Contains("sampleValue") Then
                            realTimeDtRow("CountUnits") = row("sampleValue")
                        End If
                    End If
                    realTimeDt.Rows.Add(realTimeDtRow)
                End If
            Next
        dt = realTimeDt
    End Sub
End Class
