﻿Public Class CoronaHalfHourSchema

    Private Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("MP Ref No")
        headers.Add("Date")
        headers.Add("Time")
        headers.Add("Reading")
        headers.Add("Consumption")
        headers.Add("kWh")

        Return headers
    End Function

    Public Shared Function Validate(ByRef data As DataTable) As Boolean

        For Each header As String In GetHeaders()
            If Not data.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        'Change the time format of the source data
        ChangeTimeFormat(dt)
        CreateHalfHourlyEntries(dt, hhDataTable)

        Return dt
    End Function

    Private Shared Sub ChangeTimeFormat(ByRef dt As DataTable)
        Dim dateHolder As DateTime
        For Each row As DataRow In dt.Rows
            dateHolder = row("Time")
            row("Time") = dateHolder.ToString("HH:mm")
        Next
    End Sub

    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim dateholder As DateTime
        Dim success As Boolean = False
        For Each row As DataRow In dt.Rows

            If IsDate(row("Date")) Then
                dateholder = row("Date")
                For Each hhrow As DataRow In hhDataTable.Rows
                    If hhrow("Date(dd/mm/yyyy)") = dateholder.Date And hhrow("MPAN") = row("MP Ref No") Then
                        hhrow(row("Time")) = row("kWh")
                        hhrow("ForcekWh") = "Yes"
                        hhrow("MPAN") = row("MP Ref No")
                        success = True
                    End If
                Next

                If Not success Then
                    Dim newRow As DataRow = hhDataTable.NewRow
                    newRow("Date(dd/mm/yyyy)") = dateholder.Date
                    newRow(row("Time")) = row("kWh")
                    newRow("ForcekWh") = "Yes"
                    newRow("MPAN") = row("MP Ref No")
                    hhDataTable.Rows.Add(newRow)
                End If
            End If
            success = False
        Next

        dt = hhDataTable

    End Sub
End Class
