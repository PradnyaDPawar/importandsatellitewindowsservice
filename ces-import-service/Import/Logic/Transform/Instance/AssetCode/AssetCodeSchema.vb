﻿Public Class AssetCodeSchema
    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        RenameColumnNames(dt)
        Return dt

    End Function

    Public Shared Sub RenameColumnNames(ByRef dt As DataTable)

        For Each col As DataColumn In dt.Columns
            col.ColumnName = col.ColumnName.Replace(" ", "").ToLower()
        Next

    End Sub
End Class
