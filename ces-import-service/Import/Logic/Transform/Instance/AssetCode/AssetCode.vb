﻿Public Class AssetCode
    Implements ITransformDelegate


    Private _returnTable As DataTable

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable

        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        'Dim csvFile As New DelimitedFile(fullPath, 0)
        'csvFile.Load(_returnTable)
        Dim excel As New Excel
        Dim xSheet As ExcelSheet
        excel.Load(fullPath)
        xSheet = excel.Sheets(0)
        excel.GetSheet("Asset Code", xSheet)
        xSheet.HeaderRowIndex = 1
        _returnTable = xSheet.GetDataTable()


    End Sub


    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        AssetCodeSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows

            FE_AssetCode.Fill(row, dsEdits, importEdit)

        Next

    End Sub

End Class
