﻿Imports System.Text.RegularExpressions

Partial Public Class ElComponentRealTime

    Private Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("Meter_Identifier")
        headers.Add("Date")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")
        headers.Add("00:00")


        Return headers
    End Function


#Region " To DeFormat "

    Private Sub Formats()

        RenameColumns()
        CreateColumns()
        PopulateValue()
        RemoveHhEntries()
        SortOnMeterName()

    End Sub


#End Region



    Private Sub RenameColumns()

        _returnTable.Columns("Meter_Identifier").ColumnName = "MeterName"
        _returnTable.Columns("Date").ColumnName = "DateTime"

    End Sub

    Private Sub CreateColumns()

        _returnTable.Columns.Add("CountUnits")

    End Sub

    Private Sub PopulateValue()
        Dim first_hh_entry As Boolean
        Dim dateinrow As DateTime
        Dim span As TimeSpan
        Dim reg As Regex = New Regex("^([01]\d|2[0-3]):?([0-5]\d)$") 'Regex to match 24 hour format


        For Each row As DataRow In _returnTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            'ignore empty rows
            If Date.TryParse(row("DateTime"), dateinrow) Then

                first_hh_entry = True

                For Each column As DataColumn In _returnTable.Columns

                    Dim m As Match = reg.Match(column.ColumnName)
                    If m.Success Then

                        If (first_hh_entry) Then

                            If TimeSpan.TryParse(column.ColumnName.ToString, span) Then
                                dateinrow = dateinrow.Date + span
                            End If

                            Dim str As String = dateinrow.ToString()
                            row("DateTime") = dateinrow.ToString("G")
                            If String.IsNullOrEmpty(row(column)) Then
                                row("CountUnits") = row(column)
                            Else
                                Dim number As Double
                                Double.TryParse(row(column), number)
                                row("CountUnits") = number.ToString
                            End If


                        Else

                            Dim newrow As DataRow = _returnTable.NewRow

                            If TimeSpan.TryParse(column.ColumnName.ToString, span) Then
                                dateinrow = dateinrow.Date + span
                            End If

                            If column.ColumnName = "00:00" Then
                                dateinrow = dateinrow.AddDays(1)
                            End If

                            newrow("DateTime") = dateinrow.ToString("G")
                            If String.IsNullOrEmpty(row(column)) Then
                                newrow("CountUnits") = row(column)
                            Else
                                Dim number As Double
                                Double.TryParse(row(column), number)
                                newrow("CountUnits") = number.ToString
                            End If
                            newrow("MeterName") = row("MeterName")
                            _returnTable.Rows.Add(newrow)

                        End If

                        first_hh_entry = False

                    End If

                Next
            End If
        Next

    End Sub

    Private Sub RemoveHhEntries()

        Dim _reg As Regex = New Regex("^([01]\d|2[0-3]):?([0-5]\d)$")

        For columncount As Integer = _returnTable.Columns.Count - 1 To 0 Step -1

            Dim m As Match = _reg.Match(_returnTable.Columns(columncount).ColumnName)
            If m.Success Then
                _returnTable.Columns.RemoveAt(columncount)
            End If
        Next


    End Sub

    Private Sub SortOnMeterName()

        Dim view As DataView = _returnTable.DefaultView
        view.Sort = "MeterName, DateTime"
        _returnTable = view.ToTable

    End Sub


End Class
