﻿Public Class EnergyBrokersWaterInvoiceSchema
    Public Shared Sub ToDeFormat(ByRef dt As DataTable)
        Dim dtresult As DataTable = PopulateColumns(dt)
        dt = dtresult

    End Sub

    Private Shared Function PopulateColumns(ByRef dt As DataTable) As DataTable

        Dim standardInvoiceTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        For Each row As DataRow In dt.Rows


            For Each column As DataColumn In dt.Columns
                If (column.ColumnName.Contains("Water Charge")) Then
                    Dim invoicetablerow As DataRow = standardInvoiceTable.NewRow
                    invoicetablerow("Invoice Number") = row("Invoice Number")
                    invoicetablerow("Meter Serial") = row("Meter Number")
                    invoicetablerow("TaxPointDate") = IIf(String.IsNullOrEmpty(row("Tax Point").ToString()), "", CDate(row("Tax Point")).ToString("dd/MM/yyyy"))
                    invoicetablerow("Start Date") = IIf(String.IsNullOrEmpty(row("Read Date").ToString()), "", CDate(row("Read Date")).ToString("dd/MM/yyyy"))
                    invoicetablerow("End Date") = IIf(String.IsNullOrEmpty(row("Read Date 1").ToString()), "", CDate(row("Read Date 1")).ToString("dd/MM/yyyy"))
                    invoicetablerow("Force kWh") = "No"
                    invoicetablerow("Start Read") = row("Units From")
                    invoicetablerow("End Read") = row("Units To")
                    invoicetablerow("Start Read Estimated") = row("ReadType")
                    invoicetablerow("End Read Estimated") = row("ReadType 1")
                    invoicetablerow("Consumption") = row("Units Used M3")
                    invoicetablerow("Gross") = row("Water Charge(£)").ToString().Replace("£", "")
                    invoicetablerow("Cost Only") = "No"
                    invoicetablerow("Description") = "Water Charge"
                    invoicetablerow("IsLastDayApportion") = "No"
                    standardInvoiceTable.Rows.Add(invoicetablerow)

                ElseIf (column.ColumnName.Contains("Sewerage Charge")) Then
                    Dim invoicetablerow As DataRow = standardInvoiceTable.NewRow
                    invoicetablerow("Invoice Number") = row("Invoice Number")
                    invoicetablerow("Meter Serial") = row("Meter Number")
                    invoicetablerow("TaxPointDate") = IIf(String.IsNullOrEmpty(row("Tax Point").ToString()), "", CDate(row("Tax Point")).ToString("dd/MM/yyyy"))
                    invoicetablerow("Start Date") = IIf(String.IsNullOrEmpty(row("Read Date").ToString()), "", CDate(row("Read Date")).ToString("dd/MM/yyyy"))
                    invoicetablerow("End Date") = IIf(String.IsNullOrEmpty(row("Read Date 1").ToString()), "", CDate(row("Read Date 1")).ToString("dd/MM/yyyy"))
                    invoicetablerow("Force kWh") = "No"
                    invoicetablerow("Gross") = row("Sewerage Charge(£)").ToString().Replace("£", "")
                    invoicetablerow("Cost Only") = "Yes"
                    invoicetablerow("Description") = "Waste Water"
                    invoicetablerow("IsLastDayApportion") = "No"
                    standardInvoiceTable.Rows.Add(invoicetablerow)
                ElseIf (column.ColumnName.Contains("Water Fixed Charge") Or column.ColumnName.Contains("Waste Water Fixed Charge ") Or column.ColumnName.Contains("Usage Allowance") Or column.ColumnName.Contains("VAT (£)") Or column.ColumnName.Contains("Additional Charges")) Then

                    PopulateFixedChargeRows(standardInvoiceTable, row, column.ColumnName)




                End If

            Next
        Next
        dt = standardInvoiceTable
        Dim dataView As New DataView(dt)
        dataView.Sort = "Description ASC"
        dt = dataView.ToTable()
        RenameColumns(dt)

        Return dt
    End Function
    Private Shared Sub PopulateFixedChargeRows(ByRef standardinvoicetable As DataTable, ByRef row As DataRow, ByRef col As String)
        Dim dv As DataView = standardinvoicetable.AsDataView
        Dim invoicetablerow As DataRow = standardinvoicetable.NewRow

        invoicetablerow("Invoice Number") = row("Invoice Number")
        invoicetablerow("Meter Serial") = row("Meter Number")
        invoicetablerow("TaxPointDate") = row("Tax Point")
        invoicetablerow("Start Date") = row("Fixed Charge Start Date")
        invoicetablerow("End Date") = row("Fixed Charge End Date")
        invoicetablerow("Force kWh") = "No"
        invoicetablerow("IsLastDayApportion") = "No"
        If (col.Contains("Water Fixed Charge") And Not col.Contains("Waste Water Fixed Charge")) Then
            dv.RowFilter = "Description like 'Water Fixed Charge%'"
            If (dv.ToTable.Rows.Count = 0) Then
                invoicetablerow("Gross") = row("Water Fixed Charge (£)").ToString().Replace("£", "")
                invoicetablerow("Cost Only") = "Yes"
                invoicetablerow("Description") = "Water Fixed Charge"
                standardinvoicetable.Rows.Add(invoicetablerow)
            End If
        End If
        If (col.Contains("Waste Water Fixed Charge")) Then
            dv.RowFilter = "Description like 'Waste Water Fixed Charge%'"
            If (dv.ToTable.Rows.Count = 0) Then
                invoicetablerow("Gross") = row("Waste Water Fixed Charge (£)").ToString().Replace("£", "")

                invoicetablerow("Cost Only") = "Yes"
                invoicetablerow("Description") = "Waste Water Fixed Charge"
                standardinvoicetable.Rows.Add(invoicetablerow)

            End If
        End If
        If (col.Contains("Usage Allowance")) Then
            dv.RowFilter = "Description like 'Usage Allowance%'"
            If (dv.ToTable.Rows.Count = 0) Then
                invoicetablerow("Gross") = row("Usage Allowance").ToString().Replace("£", "")

                invoicetablerow("Cost Only") = "Yes"
                invoicetablerow("Description") = "Usage Allowance"
                standardinvoicetable.Rows.Add(invoicetablerow)

            End If
        End If
        If (col.Contains("VAT")) Then
            dv.RowFilter = "Description like 'VAT%'"
            If (dv.ToTable.Rows.Count = 0) Then
                invoicetablerow("Gross") = row("VAT (£)").ToString().Replace("£", "")

                invoicetablerow("Cost Only") = "Yes"
                invoicetablerow("Description") = "VAT"
                standardinvoicetable.Rows.Add(invoicetablerow)

            End If
        End If
        If (col.Contains("Additional Charges")) Then
            dv.RowFilter = "Description like 'Additional Charges%'"
            If (dv.ToTable.Rows.Count = 0) Then
                invoicetablerow("Gross") = row("Additional Charges").ToString().Replace("£", "")

                invoicetablerow("Cost Only") = "Yes"
                invoicetablerow("Description") = "Additional Charges"
                standardinvoicetable.Rows.Add(invoicetablerow)

            End If
        End If








    End Sub
    Private Shared Sub RenameColumns(ByRef invoiceDataTable As DataTable)
        invoiceDataTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceDataTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceDataTable.Columns("Rate").ColumnName = "Tariff"
        invoiceDataTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceDataTable.Columns("End Date").ColumnName = "EndDate"
        invoiceDataTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceDataTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceDataTable.Columns("End Read").ColumnName = "EndRead"
        invoiceDataTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceDataTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceDataTable.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub
End Class
