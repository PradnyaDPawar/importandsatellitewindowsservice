﻿Public Class EnergyBrokersGasInvoiceSchema
    Public Shared Sub ToDeFormat(ByRef dt As DataTable)
        Dim dtresult As DataTable = PopulateColumns(dt)
        dt = dtresult

    End Sub
    Private Shared Function PopulateColumns(ByRef dt As DataTable) As DataTable
        dt.Rows.RemoveAt(dt.Rows.Count - 1)
        Dim standardInvoiceTable = DeFormatTableTemplates.CreateInvoiceTable()
        For Each row As DataRow In dt.Rows
            Dim invoicetablerow As DataRow = standardInvoiceTable.NewRow
            invoicetablerow("Invoice Number") = row("Invoice No. ")
            invoicetablerow("Meter Serial") = row("MSN")
            invoicetablerow("MPAN") = row("MPR")
            invoicetablerow("TaxPointDate") = IIf(String.IsNullOrEmpty(row("Invoice Date").ToString()), "", CDate(row("Invoice Date")).ToString("dd/MM/yyyy"))
            invoicetablerow("Start Date") = IIf(String.IsNullOrEmpty(row("Start Read").ToString()), "", CDate(row("Start Read")).ToString("dd/MM/yyyy"))
            invoicetablerow("End Date") = IIf(String.IsNullOrEmpty(row("End Read").ToString()), "", CDate(row("End Read")).ToString("dd/MM/yyyy"))
            invoicetablerow("Force kWh") = "Yes"
            invoicetablerow("Cost Only") = "No"
            invoicetablerow("IsLastDayApportion") = "Yes"
            invoicetablerow("Start Read") = row("Start Read 1")
            invoicetablerow("End Read") = row("End Read 1")
            invoicetablerow("Start Read Estimated") = row("Read Type")
            invoicetablerow("End Read Estimated") = row("Read Type 1")
            invoicetablerow("Consumption") = row("Billed (kWh)")
            invoicetablerow("Gross") = row("Gross Total (£)").ToString().Replace("£", "")
            invoicetablerow("Description") = "Gas Consumption"
            standardInvoiceTable.Rows.Add(invoicetablerow)
        Next
        RenameColumns(standardInvoiceTable)
        Return standardInvoiceTable
    End Function
    Private Shared Sub RenameColumns(ByRef invoiceDataTable As DataTable)
        invoiceDataTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceDataTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceDataTable.Columns("Rate").ColumnName = "Tariff"
        invoiceDataTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceDataTable.Columns("End Date").ColumnName = "EndDate"
        invoiceDataTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceDataTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceDataTable.Columns("End Read").ColumnName = "EndRead"
        invoiceDataTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceDataTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceDataTable.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub
End Class
