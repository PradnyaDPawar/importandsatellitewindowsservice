﻿Public Class DefaultMeterCSVSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("Building UPRN")
        headers.Add("Meter Name")
        headers.Add("Serial Number")
        headers.Add("MPAN / MPRN")
        headers.Add("Account Number")
        headers.Add("MeterFuel")
        headers.Add("Units")
        headers.Add("AMR")
        headers.Add("AMR Installed Date")
        headers.Add("Is Active")
        headers.Add("Reporting Meter")
        headers.Add("CRC Meter Type")
        headers.Add("CRC Fuel Type")
        headers.Add("Include As CRC Residual")
        headers.Add("Carbon Trust Standard")
        headers.Add("Operating Schedule 1")
        headers.Add("Operating Schedule 2")
        headers.Add("CTS Start Date (dd/mm/yyyy)")
        headers.Add("Calorific Value (Gas)")
        headers.Add("Date Installed")
        headers.Add("Date Commissioned")
        headers.Add("Is Stock Meter")
        headers.Add("Invoice Frequency")
        headers.Add("Location")
        headers.Add("Description")
        headers.Add("Correction Factor")
        headers.Add("Conversion Factor")
        headers.Add("Custom Multiplier")
        headers.Add("Validate Readings")
        headers.Add("Reading Wrap Round")
        headers.Add("Max Read Consumption")
        headers.Add("Real Time")
        headers.Add("Wrap Round Value")
        headers.Add("Is Cumulative")
        headers.Add("Is Raw")
        headers.Add("Min Entry")
        headers.Add("Max Entry")
        headers.Add("Multiplier")
        headers.Add("Serial Alias")
        headers.Add("Create kWh Export Associate Meter")
        headers.Add("Create kVarh Export Associate Meter")
        headers.Add("Create kVarh Associate Meter")
        headers.Add("Export MPAN")
        headers.Add("Authorised Capacity")
        headers.Add("Excess Capacity")


        Return headers

    End Function

    Public Shared Sub ToDeFormat(ByRef dt As DataTable, ByRef isSubmeter As Boolean)
        DataTableTools.RemoveBlankRows(dt)
        If Not isSubmeter Then
            AddColumns(dt)
        End If
        renamecolumns(dt)
    End Sub

    Private Shared Sub renamecolumns(ByRef dt As DataTable)
        dt.Columns("Building UPRN").ColumnName = "BuildingUPRN"
        dt.Columns("Meter Name").ColumnName = "MeterName"
        dt.Columns("Serial Number").ColumnName = "SerialNumber"
        If (dt.Columns.Contains("MPAN / MPRN")) Then
            dt.Columns("MPAN / MPRN").ColumnName = "MPAN"
        End If
        If (dt.Columns.Contains("Account Number")) Then
            dt.Columns("Account Number").ColumnName = "AccountNumber"
        End If
        If (dt.Columns.Contains("AMR Installed Date")) Then
            dt.Columns("AMR Installed Date").ColumnName = "AMRInstalledDate"
        End If
        If (dt.Columns.Contains("Is Active")) Then
            dt.Columns("Is Active").ColumnName = "IsActive"
        End If
        If (dt.Columns.Contains("Create kWh Export Associate Meter")) Then
            dt.Columns("Create kWh Export Associate Meter").ColumnName = "CreatekWhExportAssociateMeter"
        End If
        If (dt.Columns.Contains("Create kVarh Export Associate Meter")) Then
            dt.Columns("Create kVarh Export Associate Meter").ColumnName = "CreatekVarhExportAssociateMeter"
        End If
        If (dt.Columns.Contains("Create kVarh Associate Meter")) Then
            dt.Columns("Create kVarh Associate Meter").ColumnName = "CreatekVarhAssociateMeter"
        End If
        If (dt.Columns.Contains("Export MPAN")) Then
            dt.Columns("Export MPAN").ColumnName = "ExportMPAN"
        End If
        dt.Columns("Reporting Meter").ColumnName = "ReportingMeter"
        If (dt.Columns.Contains("CRC Meter Type")) Then
            dt.Columns("CRC Meter Type").ColumnName = "CRCMeterType"
        End If
        If (dt.Columns.Contains("CRC Fuel Type")) Then
            dt.Columns("CRC Fuel Type").ColumnName = "CRCFuelType"
        End If
        If (dt.Columns.Contains("Include As CRC Residual")) Then
            dt.Columns("Include As CRC Residual").ColumnName = "IncludeAsCRCResidual"
        End If
        If (dt.Columns.Contains("Carbon Trust Standard")) Then
            dt.Columns("Carbon Trust Standard").ColumnName = "CarbonTrustStandard"
        End If
        If (dt.Columns.Contains("CTS Start Date (dd/mm/yyyy)")) Then
            dt.Columns("CTS Start Date (dd/mm/yyyy)").ColumnName = "CTSStartDate"
        End If
        dt.Columns("Operating Schedule 1").ColumnName = "OperatingSchedule1"
        dt.Columns("Operating Schedule 2").ColumnName = "OperatingSchedule2"
        If (dt.Columns.Contains("Consumption Source")) Then
            dt.Columns("Consumption Source").ColumnName = "ConsumptionSource"
        End If
        If (dt.Columns.Contains("CRC Consumption Source")) Then
            dt.Columns("CRC Consumption Source").ColumnName = "CRCConsumptionSource"
        End If
        If (dt.Columns.Contains("Authorised Capacity")) Then
            dt.Columns("Authorised Capacity").ColumnName = "AuthorisedCapacity"
        End If
        If (dt.Columns.Contains("Excess Capacity")) Then
            dt.Columns("Excess Capacity").ColumnName = "ExcessCapacity"
        End If
        If (dt.Columns.Contains("Calorific Value (Gas)")) Then
            dt.Columns("Calorific Value (Gas)").ColumnName = "CalorificValue"
        End If
        If (dt.Columns.Contains("Date Installed")) Then
            dt.Columns("Date Installed").ColumnName = "DateInstalled"
        End If
        If (dt.Columns.Contains("Date Commissioned")) Then
            dt.Columns("Date Commissioned").ColumnName = "DateCommissioned"
        End If
        If (dt.Columns.Contains("Is Stock Meter")) Then
            dt.Columns("Is Stock Meter").ColumnName = "IsStockMeter"
        End If
        If (dt.Columns.Contains("Invoice Frequency")) Then
            dt.Columns("Invoice Frequency").ColumnName = "InvoiceFrequency"
        End If
        dt.Columns("Correction Factor").ColumnName = "CorrectionFactor"
        dt.Columns("Conversion Factor").ColumnName = "ConversionFactor"
        dt.Columns("Custom Multiplier").ColumnName = "CustomMultiplier"
        dt.Columns("Validate Readings").ColumnName = "ValidateReadings"
        dt.Columns("Reading Wrap Round").ColumnName = "ReadingWrapRound"
        dt.Columns("Max Read Consumption").ColumnName = "MaxReadConsumption"
        dt.Columns("Real Time").ColumnName = "RealTime"
        dt.Columns("Wrap Round Value").ColumnName = "WrapRoundValue"
        dt.Columns("Is Cumulative").ColumnName = "IsCumulative"
        dt.Columns("Is Raw").ColumnName = "IsRaw"
        dt.Columns("Min Entry").ColumnName = "MinEntry"
        dt.Columns("Max Entry").ColumnName = "MaxEntry"
        dt.Columns("Serial Alias").ColumnName = "SerialAlias"
        If (dt.Columns.Contains("Parent Meter Serial")) Then
            dt.Columns("Parent Meter Serial").ColumnName = "ParentMeterSerial"
        End If


    End Sub

    Public Shared Sub AddColumns(ByRef dt As DataTable)
        dt.Columns.Add("ConsumptionSource")
        dt.Columns.Add("CRCConsumptionSource")
        dt.Columns.Add("TankSize")
        dt.Columns.Add("TimeZone")
        dt.Columns.Add("Category")
        dt.Columns.Add("Status")
        dt.Columns.Add("MeterOperator")
        dt.Columns.Add("MeterOperatorContact")
        dt.Columns.Add("EnableDayNightActivePeriod")
        dt.Columns.Add("DayPeriod")
        dt.Columns.Add("NightPeriod")
    End Sub

End Class
