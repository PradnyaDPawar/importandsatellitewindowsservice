﻿Public Class DefaultMeterCSV
    Implements ITransformDelegate

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvFile As New DelimitedFile(fullPath, 25)
        csvFile.FieldsAreQuoted = True
        csvFile.Load(_returnTable, DefaultMeterCSVSchema.GetHeaders())
    End Sub

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        Dim isSubmeter As Boolean
        If (_returnTable.Columns.Contains("Parent Meter Serial")) Then
            isSubmeter = True
        Else
            isSubmeter = False
        End If
        DefaultMeterCSVSchema.ToDeFormat(_returnTable, isSubmeter)
        For Each row As DataRow In _returnTable.Rows
            FE_Meter.Fill(row, dsEdits, importEdit, isSubmeter)
        Next
    End Sub
End Class
