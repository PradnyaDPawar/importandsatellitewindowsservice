﻿Public Class GDFHalfHourSchema

    Public Shared Function ToDEFormat(ByRef tdt As DataTable)

        DeleteFirstColumn(tdt)
        DataTableTools.RemoveBlankColumns(tdt)
        DataTableTools.RemoveBlankRows(tdt)
        NameBlankColumns(tdt)
        DataTableTools.SetHeaders(tdt, 1)
        CreateColumns(tdt)
        CreateRowWiseDateEntries(tdt)
        DeleteDateEntriesinColumn(tdt)
        PopulateNewColumns(tdt)
        RenameHeaders(tdt)

        Return tdt

    End Function

    Private Shared Sub NameBlankColumns(ByRef tdt As DataTable)
        Dim numbertoappend As Integer = 1

        For Each column As DataColumn In tdt.Columns

            If tdt.Rows(0)(column) = "" Then
                tdt.Rows(0)(column) = "Column 0" & numbertoappend
            End If


        Next

    End Sub

    Private Shared Sub CreateColumns(ByRef tdt As DataTable)

        tdt.Columns.Add("Date(DD/MM/YYYY)")
        tdt.Columns.Add("MPAN")
        tdt.Columns.Add("ForcekWh")
        tdt.Columns.Add("MeterSerial")

    End Sub

    Private Shared Sub DeleteFirstColumn(ByRef tdt As DataTable)

        tdt.Columns.RemoveAt(0)

    End Sub


    Private Shared Sub CreateRowWiseDateEntries(ByRef tdt As DataTable)

        Dim date1, date2, firstdate As Date
        Dim rowelement(tdt.Columns.Count - 1) As Object  'Object array to hold new row data

        Dim rowcounttill As Integer = tdt.Rows.Count - 1
        Dim colcounttill As Integer = GetLastDateIndexFromHeaders(tdt)

        Dim initialdateindex As Integer = GetFirstDateIndexFromHeaders(tdt)
        Dim colcount As Integer = initialdateindex
        Dim newcolcount As Integer


        date1 = tdt.Columns(initialdateindex).ColumnName

        For rowcount As Integer = 0 To rowcounttill

            If IsDBNull(tdt.Rows(rowcount)("Date(DD/MM/YYYY)")) Then
                firstdate = tdt.Columns(initialdateindex).ColumnName
                tdt.Rows(rowcount)("Date(DD/MM/YYYY)") = firstdate.ToShortDateString
            End If

            While colcount < tdt.Columns.Count - colcounttill - 1

                date2 = tdt.Columns(colcount).ColumnName
                newcolcount = colcount

                If date1.Date <> date2.Date Then

                    rowelement(0) = tdt.Rows(rowcount)(0)
                    rowelement(tdt.Columns("Date(DD/MM/YYYY)").Ordinal()) = date2.ToShortDateString

                    Dim date3 As Date = tdt.Columns(newcolcount).ColumnName
                    Dim i As Integer = 1
                    While date2.Date = date3.Date And newcolcount <= tdt.Columns.Count - colcounttill - 1
                        rowelement(i) = tdt.Rows(rowcount)(newcolcount)
                        i += 1
                        newcolcount += 1

                        If newcolcount < tdt.Columns.Count - colcounttill - 1 Then
                            date3 = tdt.Columns(newcolcount).ColumnName
                        End If

                    End While

                    tdt.Rows.Add(rowelement)
                    Array.Clear(rowelement, 0, tdt.Columns.Count - 1)
                    colcount = newcolcount - 1
                    date1 = date2
                End If

                colcount += 1

            End While

            colcount = initialdateindex

        Next


    End Sub


    Private Shared Function GetFirstDateIndexFromHeaders(ByRef tdt As DataTable) As Integer

        Dim colcount As Integer = 0
        Dim initialdateindex As Integer = 0

        While Not IsDate(tdt.Columns(colcount).ColumnName)
            initialdateindex += 1
            colcount += 1
        End While

        Return initialdateindex

    End Function

    Private Shared Function GetLastDateIndexFromHeaders(ByRef tdt As DataTable) As Integer

        Dim enddateindex As Integer = 0
        Dim colcount As Integer = tdt.Columns.Count - 1

        While Not IsDate(tdt.Columns(colcount).ColumnName)
            enddateindex = enddateindex + 1
            colcount -= 1
        End While

        Return enddateindex
    End Function


    Private Shared Sub RenameHeaders(ByRef tdt As DataTable)
        Dim dateholder As Date

        tdt.Columns("TimeStamp").ColumnName = "Meter Identifier"

        For Each column As DataColumn In tdt.Columns
            If IsDate(column.ColumnName) Then
                dateholder = column.ColumnName
                column.ColumnName = dateholder.ToString("HH:mm")
            End If
        Next


    End Sub

    Private Shared Sub PopulateNewColumns(ByRef tdt As DataTable)

        For Each row As DataRow In tdt.Rows

            PopulateForceKwh(row)
            PopulateMPAN(row)
        Next

    End Sub

    Private Shared Sub PopulateMeterSerial(ByRef row As DataRow)

        Dim str As String = row("TimeStamp").ToString
        Dim meterserial As String = str.Substring(0, str.IndexOf("."))
        row("MeterSerial") = meterserial

    End Sub


    Private Shared Sub PopulateMPAN(ByRef row As DataRow)

        Dim str As String = row("TimeStamp").ToString
        
        Dim mpan As String = (str.Substring((str.IndexOf(".") + 1), str.IndexOf("-") - (str.IndexOf(".") + 1))).Trim
        row("MPAN") = mpan


    End Sub

    Private Shared Sub PopulateForceKwh(ByRef row As DataRow)

        Dim str As String = row("TimeStamp").ToString
        Dim consumptionunit As String = str.Substring((str.IndexOf("(") + 1), str.IndexOf(")") - (str.IndexOf("(") + 1)).Trim
        If consumptionunit = "kWh" Then
            row("ForcekWh") = "Yes"
        Else
            row("ForcekWh") = "No"
        End If

    End Sub

    Public Shared Function Validate(ByRef tdt As DataTable) As Boolean

        Dim listofdates As New List(Of Date)
        listofdates = GetAllDates(tdt)
        Dim listofdatetime As List(Of DateTime)
        listofdatetime = GetDateTimeEntries(listofdates)

        Dim ValidFlag As Boolean = False

        For Each datetimeentry As DateTime In listofdatetime

            For Each row As DataRow In tdt.Rows
                If IsDate(row(0)) Then
                    If datetimeentry = row(0) Then
                        ValidFlag = True
                    End If
                End If
            Next

            If Not ValidFlag Then
                Return False
            End If

            ValidFlag = False
        Next

        Return True

    End Function

    Private Shared Function GetAllDates(ByRef tdt As DataTable) As List(Of Date)
        Dim listofdates As New List(Of Date)
        Dim datetocheck As Date

        For Each row As DataRow In tdt.Rows

            If IsDate(row(0)) Then
                datetocheck = DateTime.Parse(row(0))
                If Not listofdates.Contains(datetocheck.Date) Then
                    listofdates.Add(datetocheck.Date)
                End If
            End If

        Next
        Return listofdates
    End Function


    Private Shared Function GetDateTimeEntries(ByVal dateEntries As List(Of DateTime)) As List(Of DateTime)
        Dim dateTimeEntries As New List(Of DateTime)

        For Each dateentry As DateTime In dateEntries
            dateTimeEntries.Add(dateentry)
            For addCount As Integer = 1 To 22
                dateentry = dateentry.AddMinutes(30)
                dateTimeEntries.Add(dateentry)
            Next
        Next

        Return dateTimeEntries
    End Function

    Private Shared Sub DeleteDateEntriesinColumn(ByRef tdt As DataTable)
        Dim colcount As Integer = GetFirstDateIndexFromHeaders(tdt) 'should be colcount = initialindex - 1
        Dim date1, date2 As Date
        Dim removefromindex As Integer
        Dim removetillindex As Integer

        For i = colcount To tdt.Columns.Count - 2

            If IsDate(tdt.Columns(colcount).ColumnName) Then
                date1 = tdt.Columns(colcount).ColumnName
            End If

            If IsDate(tdt.Columns(colcount + 1).ColumnName) Then
                date2 = tdt.Columns(colcount + 1).ColumnName
            End If

            If date1.Date = date2.Date Then
                colcount += 1
            End If

        Next

        ' While date1.Date = date2.Date And colcount < tdt.Columns.Count - 1

        '        colcount += 1
        '       date1 = tdt.Columns(colcount).ColumnName
        '      date2 = tdt.Columns(colcount + 1).ColumnName

        '     End While

        removefromindex = colcount + 1
        removetillindex = GetLastDateIndexFromHeaders(tdt) + 1

        For colcount = tdt.Columns.Count - removetillindex To removefromindex Step -1

            tdt.Columns.RemoveAt(colcount)

        Next

    End Sub

End Class
