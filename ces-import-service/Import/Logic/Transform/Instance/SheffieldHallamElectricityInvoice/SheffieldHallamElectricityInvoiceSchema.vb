﻿Public Class SheffieldHallamElectricityInvoiceSchema

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("Customer Number")
        headers.Add("Account Number")
        headers.Add("profileclassid")
        headers.Add("Meter Class")
        headers.Add("LLF")
        headers.Add("MPAN")
        headers.Add("Site Address 1")
        headers.Add("Site Address 2")
        headers.Add("Site Address 3")
        headers.Add("Site Address 4")
        headers.Add("Emergency Telephone")
        headers.Add("Site Code")
        headers.Add("Charge Category")
        headers.Add("Charge Description")
        headers.Add("Meter Number")
        headers.Add("Register ID")
        headers.Add("Date")
        headers.Add("Reading")
        headers.Add("ReadingType")
        headers.Add("Quantity")
        headers.Add("Units")
        headers.Add("Chargeable Quantity")
        headers.Add("Rate")
        headers.Add("Rate Units")
        headers.Add("Net Charge")
        headers.Add("% VAT at reduced rate")
        headers.Add("% VAT at standard rate")
        headers.Add("De Minimus")
        headers.Add("proportiongreen")
        headers.Add("Special CCL Rate (p/kWh)")
        headers.Add("Effective VAT Rate")
        headers.Add("Consumption for CCL purposes")
        headers.Add("Effective CCL Rate (p/kWh)")
        headers.Add("CCL Charge")
        headers.Add("VAT Charge")

        Return headers
    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        DataTableTools.RemoveBlankRows(dt)
        RemoveSubTotalRows(dt)
        Dim InvoiceDataTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        CreateInvoiceEntries(InvoiceDataTable, dt)
        GetStandingChargeDates(InvoiceDataTable)
        RenameColumns(InvoiceDataTable)
        dt = InvoiceDataTable
        Return dt
    End Function

    Private Shared Sub RemoveSubTotalRows(ByRef dt As DataTable)
        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If row("MPAN").ToString.Contains("Total") Then
                dt.Rows.Remove(row)
            End If
        Next
    End Sub


    Private Shared Sub CreateInvoiceEntries(ByRef InvoiceDatatable As DataTable, ByRef dt As DataTable)
        Dim net, vat, vatRate, ccl As Double


        For Each dtRow As DataRow In dt.Rows

            If Not String.IsNullOrEmpty(dtRow("Meter Number").ToString.Trim) And _
                       Not String.IsNullOrEmpty(dtRow("Date").ToString.Trim) And _
                       Not String.IsNullOrEmpty(dtRow("Reading").ToString.Trim) Then
                Dim invoiceDataRow As DataRow = InvoiceDatatable.NewRow
                Dim prevRowIndex As Integer = dt.Rows.IndexOf(dtRow) - 1

                invoiceDataRow("Invoice Number") = dtRow("MPAN")
                invoiceDataRow("Meter Serial") = dtRow("Meter Number").ToString.Trim
                invoiceDataRow("MPAN") = dtRow("MPAN")
                invoiceDataRow("Rate") = dtRow("Charge Category") & "-" & dtRow("Charge Description")
                invoiceDataRow("Start Date") = dt.Rows(prevRowIndex)("Date")
                invoiceDataRow("End Date") = dtRow("Date")
                invoiceDataRow("Start Read") = dt.Rows(prevRowIndex)("Reading")
                invoiceDataRow("Start Read Estimated") = GetDeFormatEstimatedType(dt.Rows(prevRowIndex))
                invoiceDataRow("End Read") = dtRow("Reading")
                invoiceDataRow("End Read Estimated") = GetDeFormatEstimatedType(dtRow)
                invoiceDataRow("Consumption") = dtRow("Quantity")
                invoiceDataRow("Force kWh") = GetForceKwhValue(dtRow)

                ccl = dtRow("CCL Charge")
                If ccl <> 0 Then
                    invoiceDataRow("Net") = dtRow("Net Charge")
                    net = invoiceDataRow("Net")
                    vatRate = dtRow("Effective VAT Rate").ToString.Replace("%", "")
                    vat = (net * vatRate) / 100
                    invoiceDataRow("Vat") = vat
                Else
                    invoiceDataRow("Net") = dtRow("Net Charge")
                    invoiceDataRow("Vat") = dtRow("VAT Charge")
                    net = invoiceDataRow("Net")
                    vat = invoiceDataRow("Vat")
                End If

                invoiceDataRow("Gross") = (net + vat).ToString("0.00")
                invoiceDataRow("Cost Only") = "No"
                invoiceDataRow("Description") = "Consumption Charge"
                invoiceDataRow("IsLastDayApportion") = "No"
                InvoiceDatatable.Rows.Add(invoiceDataRow)

                'If CCL is non zero, create a row with CCL Charge 

                If ccl <> 0 Then
                    InvoiceDatatable.ImportRow(invoiceDataRow)
                    Dim invoiceCclRow As DataRow = InvoiceDatatable.Rows(InvoiceDatatable.Rows.Count - 1)

                    invoiceCclRow("Net") = ccl
                    invoiceCclRow("Vat") = (ccl * vatRate) / 100
                    vat = invoiceCclRow("Vat")
                    invoiceCclRow("Gross") = ccl + vat
                    invoiceCclRow("Cost Only") = "Yes"
                    invoiceCclRow("Description") = "CCL Charge"
                    invoiceCclRow("Rate") = ""
                    invoiceCclRow("Consumption") = ""


                End If

            ElseIf String.IsNullOrEmpty(dtRow("Meter Number").ToString.Trim) And _
                       String.IsNullOrEmpty(dtRow("Date").ToString.Trim) And _
                       String.IsNullOrEmpty(dtRow("Reading").ToString.Trim) Then
                'Standing Charge
                Dim invoiceDataRow As DataRow = InvoiceDatatable.NewRow

                invoiceDataRow("Invoice Number") = dtRow("MPAN")
                invoiceDataRow("Meter Serial") = dtRow("Meter Number").ToString.Trim
                invoiceDataRow("MPAN") = dtRow("MPAN")
                invoiceDataRow("Rate") = ""
                invoiceDataRow("Start Date") = dtRow("Date")
                invoiceDataRow("End Date") = dtRow("Date")
                invoiceDataRow("Start Read") = dtRow("Reading")
                invoiceDataRow("Start Read Estimated") = GetDeFormatEstimatedType(dtRow)
                invoiceDataRow("End Read") = dtRow("Reading")
                invoiceDataRow("End Read Estimated") = GetDeFormatEstimatedType(dtRow)
                invoiceDataRow("Consumption") = dtRow("Quantity")
                invoiceDataRow("Force kWh") = GetForceKwhValue(dtRow)
                invoiceDataRow("Net") = dtRow("Net Charge")
                invoiceDataRow("Vat") = dtRow("VAT Charge")
                net = invoiceDataRow("Net")
                vat = invoiceDataRow("Vat")
                invoiceDataRow("Gross") = (net + vat).ToString("0.00")
                invoiceDataRow("Cost Only") = "Yes"
                invoiceDataRow("Description") = "Standing Charge"
                invoiceDataRow("IsLastDayApportion") = "No"
                InvoiceDatatable.Rows.Add(invoiceDataRow)
            End If


        Next
    End Sub

    Private Shared Function GetForceKwhValue(ByVal row As DataRow) As String
        If row("Units").ToString.ToLower = "kwh" Then
            Return "Yes"
        Else
            Return "No"
        End If
    End Function

    Private Shared Function GetDeFormatEstimatedType(ByVal row As DataRow) As String
        If row("ReadingType").ToString.ToLower.Contains("act") Or _
           row("ReadingType").ToString.ToLower.Contains("cust") Then
            Return "A"
        ElseIf row("ReadingType").ToString.ToLower.Contains("est") Then
            Return "E"
        End If
        Return ""
    End Function


    Private Shared Sub GetStandingChargeDates(ByRef dt As DataTable)
        For Each row As DataRow In dt.Rows
            If row("Description") = "Standing Charge" Then
                'Start Date will always be on next row
                row("Start Date") = dt.Rows(dt.Rows.IndexOf(row) + 1)("Start Date")
                If (dt.Rows.IndexOf(row) + 2) < dt.Rows.Count Then
                    If dt.Rows(dt.Rows.IndexOf(row) + 2)("Description") = "Standing Charge" Then
                        row("End Date") = dt.Rows(dt.Rows.IndexOf(row) + 1)("End Date")
                    Else
                        row("End Date") = dt.Rows(dt.Rows.IndexOf(row) + 2)("End Date")
                    End If
                Else
                    row("End Date") = dt.Rows(dt.Rows.IndexOf(row) + 1)("End Date")
                End If
            End If

        Next
    End Sub

    Private Shared Sub RenameColumns(InvoiceDt As DataTable)

        InvoiceDt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        InvoiceDt.Columns("Meter Serial").ColumnName = "MeterSerial"
        InvoiceDt.Columns("Rate").ColumnName = "Tariff"
        InvoiceDt.Columns("Start Date").ColumnName = "StartDate"
        InvoiceDt.Columns("End Date").ColumnName = "EndDate"
        InvoiceDt.Columns("Start Read").ColumnName = "StartRead"
        InvoiceDt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        InvoiceDt.Columns("End Read").ColumnName = "EndRead"
        InvoiceDt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        InvoiceDt.Columns("Force kWh").ColumnName = "ForcekWh"
        InvoiceDt.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub

End Class
