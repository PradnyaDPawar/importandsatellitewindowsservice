﻿Public Class SheffieldHallamElectricityInvoice
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel()
        excel.Load(fullPath)
        Dim xSheet As ExcelSheet = Excel.Sheets(0)


        If Not xSheet.SetHeaderRow(SheffieldHallamElectricityInvoiceSchema.GetHeaders(), ExcelSearchEnum.ContainsAll) Then

            xSheet = Excel.Sheets(1)
            If Not xSheet.SetHeaderRow(SheffieldHallamElectricityInvoiceSchema.GetHeaders(), ExcelSearchEnum.ContainsAll) Then
                'Return False
            End If

        End If

        ReturnTable = xSheet.GetDataTable()
    End Sub

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set

    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        SheffieldHallamElectricityInvoiceSchema.ToDeFormat(ReturnTable)

        For Each row As DataRow In ReturnTable.Rows

            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
