﻿

Public Class EnergyBrokersHHInvoiceSchema


    Shared Sub ToDeFormat(ByRef dt As DataTable)
        Dim dtresult As DataTable = PopulateColumns(dt)
        dt = dtresult


    End Sub
    Public Shared Function PopulateColumns(ByRef dt As DataTable) As DataTable
        Dim standardInvoiceTable As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        dt.Rows.RemoveAt(dt.Rows.Count - 1)
        For Each row As DataRow In dt.Rows
            Dim substrings() As String = row("Core 1").ToString().Split(New Char() {"#"})
            If (substrings.Length = 2) Then
                ModifyMPANwiseTable(row, dt, substrings)
            End If

            For Each col As DataColumn In dt.Columns
                If (col.ColumnName = "Day Units kWh" Or col.ColumnName = "Night Units KkW") Then
                    Dim invoiceTableRow As DataRow = standardInvoiceTable.NewRow
                    invoiceTableRow("Invoice Number") = row("Invoice No")
                    invoiceTableRow("TaxPointDate") = IIf(String.IsNullOrEmpty(row("Tax Point Date")), "", CDate(row("Tax Point Date")).ToString("dd/MM/yyyy"))


                    invoiceTableRow("Start Date") = IIf(String.IsNullOrEmpty(row("From")), "", CDate(row("From")).ToString("dd/MM/yyyy"))
                    invoiceTableRow("End Date") = IIf(String.IsNullOrEmpty(row("To")), "", CDate(row("To")).ToString("dd/MM/yyyy"))
                    invoiceTableRow("IsLastDayApportion") = "Yes"
                    invoiceTableRow("Consumption") = row(col)
                    invoiceTableRow("MPAN") = row("Core 1")
                    If (col.ColumnName = "Day Units kWh") Then
                        invoiceTableRow("Net") = row("Total For Invoice Exc CCL & Vat (£)").ToString().Replace("£", "")
                        invoiceTableRow("VAT") = row("Vat (£)").ToString.Replace("£", "")
                        invoiceTableRow("Gross") = row("Total For Invoice Inc CCL & Vat (£)").ToString.Replace("£", "")

                        invoiceTableRow("Description") = "Day HH Consumption"
                    Else
                        invoiceTableRow("Description") = "Night HH Consumption"

                    End If



                    standardInvoiceTable.Rows.Add(invoiceTableRow)
                End If
            Next



        Next
        For Each newrow As DataRow In standardInvoiceTable.Rows
            If (Not String.IsNullOrEmpty(newrow("Consumption"))) Then
                newrow("Force kWh") = "Yes"

            Else
                newrow("Force kWh") = "No"

            End If
            newrow("Cost Only") = "No"
        Next
        RenameColumns(standardInvoiceTable)
        dt = standardInvoiceTable


        Return dt

    End Function
    Public Shared Sub ModifyMPANwiseTable(ByRef dtrow As DataRow, ByRef dt As DataTable, ByRef str() As String)


        dtrow("Core 1") = str(0)
        dt.Rows(dt.Rows.IndexOf(dtrow) + 1)("Core 1") = str(1)
        dtrow("Total For Invoice Exc CCL & Vat (£)") = dt.Rows(dt.Rows.IndexOf(dtrow) + 1)("Total For Invoice Exc CCL & Vat (£)")
        dt.Rows(dt.Rows.IndexOf(dtrow) + 1)("Total For Invoice Exc CCL & Vat (£)") = ""
        dtrow("Vat (£)") = dt.Rows(dt.Rows.IndexOf(dtrow) + 1)("Vat (£)")
        dt.Rows(dt.Rows.IndexOf(dtrow) + 1)("Vat (£)") = ""
        dtrow("Total For Invoice Inc CCL & Vat (£)") = dt.Rows(dt.Rows.IndexOf(dtrow) + 1)("Total For Invoice Inc CCL & Vat (£)")
        dt.Rows(dt.Rows.IndexOf(dtrow) + 1)("Total For Invoice Inc CCL & Vat (£)") = ""





    End Sub
    Private Shared Sub RenameColumns(invoiceDataTable As DataTable)
        invoiceDataTable.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceDataTable.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceDataTable.Columns("Rate").ColumnName = "Tariff"
        invoiceDataTable.Columns("Start Date").ColumnName = "StartDate"
        invoiceDataTable.Columns("End Date").ColumnName = "EndDate"
        invoiceDataTable.Columns("Start Read").ColumnName = "StartRead"
        invoiceDataTable.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceDataTable.Columns("End Read").ColumnName = "EndRead"
        invoiceDataTable.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceDataTable.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceDataTable.Columns("Cost Only").ColumnName = "CostOnly"
    End Sub

End Class
 