﻿Public Class DataServeHalfHourSchema

    Public Shared Function GetHeaders() As List(Of String)


        Dim headers As New List(Of String)
        headers.Add("MPAN")
        headers.Add("Date(DD/MM/YYYY)")
        headers.Add("MeasuringQuantity")
        headers.Add("00:00")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")
        headers.Add("Spare")
        Return headers

    End Function


    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        'Add DE Required Columns

        dt.Columns.Add("MeterSerial")
        dt.Columns.Add("ForcekWh")

        PopulateForceKwh(dt)

        dt.Columns.Remove("Spare")

    End Sub


    Private Shared Sub PopulateForceKwh(ByRef dt As DataTable)

        For Each row As DataRow In dt.Rows

            If row("MeasuringQuantity").ToString.Trim.ToLower = "ai" Then

                row("ForcekWh") = "Yes"

            Else

                row("ForcekWh") = "No"

            End If

        Next


    End Sub

End Class
