﻿Public Class DigitalEnergyBuildingSchema
    Public Shared Function ToDeFormat(ByRef dt As DataTable)
        DataTableTools.RemoveBlankRows(dt)
        RenameColumns(dt)
        Dim bldgTable As DataTable = DeFormatTableTemplates.CreateBuildingImportTable()
        populatetable(bldgTable, dt)
        Return dt
    End Function

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)

        headers.Add("Name")
        headers.Add("Address 1")
        headers.Add("Address 2")
        headers.Add("Address 3")
        headers.Add("Address 4")
        headers.Add("Town")
        headers.Add("Post Code")
        headers.Add("UPRN")
        headers.Add("CRC Undertaking")
        headers.Add("Occupier")
        headers.Add("Client")
        headers.Add("Group")

        Return headers

    End Function

    Private Shared Sub populatetable(ByRef bldg As DataTable, ByRef dt As DataTable)
        For Each row As DataRow In dt.Rows
            Dim newRow As DataRow = bldg.NewRow
            newRow("Name") = row("Name")
            newRow("Address1") = row("Address 1")
            newRow("Address2") = row("Address 2")
            newRow("Address3") = row("Address 3")
            newRow("Address4") = row("Address 4")
            newRow("Town") = row("Town")
            newRow("PostCode") = row("Post Code")
            newRow("UPRN") = row("UPRN")
            newRow("CRCUndertaking") = row("Parent Organisation")
            newRow("Occupier") = row("Occupier")
            newRow("Client") = row("Client")
            newRow("Group") = row("Group")
            bldg.Rows.Add(newRow)
        Next
        dt = bldg
    End Sub

    Private Shared Sub RenameColumns(dt As DataTable)
        If dt.Columns.Contains("CRC Undertaking") Then
            dt.Columns("CRC Undertaking").ColumnName = "Parent Organisation"
        End If

    End Sub

End Class
