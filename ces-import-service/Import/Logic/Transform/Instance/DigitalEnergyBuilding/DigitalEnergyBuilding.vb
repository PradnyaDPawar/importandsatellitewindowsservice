﻿Public Class DigitalEnergyBuilding
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim strExtension As String
        strExtension = System.IO.Path.GetExtension(fullPath)

        If strExtension = ".csv" Then
            Dim csvFile As New DelimitedFile(fullPath, 0)
            'csvFile.HasHeader = False
            csvFile.FieldsAreQuoted = True
            csvFile.Load(_returnTable, DigitalEnergyBuildingSchema.GetHeaders())
        ElseIf strExtension = ".xls" Or strExtension = ".xlsx" Then
            Dim excel As New Excel()
            excel.Load(fullPath)
            Dim xSheet As ExcelSheet = excel.Sheets(0)
            ReturnTable = xSheet.GetDataTable()
            DataTableTools.SetHeaders(ReturnTable, 1)
        End If
        
    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        DigitalEnergyBuildingSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_Building.Fill(row, dsEdits, importEdit)
        Next

    End Sub
End Class
