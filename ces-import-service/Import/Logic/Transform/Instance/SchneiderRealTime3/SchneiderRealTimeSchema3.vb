﻿Public Class SchneiderRealTimeSchema3

    Public Shared Function ToDeFormat(ByRef dt As DataTable)
        DeleteRedundantRows(dt)
        DataTableTools.RemoveBlankColumns(dt)
        NameHeaderColumns(dt)
        DataTableTools.SetHeaders(dt, 1)
        Dim rtDt As DataTable = DeFormatTableTemplates.CreateRealTimeTable
        CreateRealTimeEntries(dt, rtDt)
        dt = rtDt
        Return dt
    End Function

    Public Shared Sub DeleteRedundantRows(ByRef dt As DataTable)

        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            If Not row(0).ToString.ToLower.Trim.Replace(" ", "") = "timestamp" Then
                row.Delete()
            Else
                Exit For
            End If

        Next

    End Sub

    Public Shared Sub NameHeaderColumns(ByRef dt As DataTable)
        'Naming/renaming the columns for row which is to be set a header row
        For Each column As DataColumn In dt.Columns

            If dt.Rows(0)(column) = "" Then
                Dim str As String
                str = dt.Rows(0)(column.Ordinal - 1)
                dt.Rows(0)(column) = Left(str, str.Length - 3) & "rec"

            ElseIf dt.Rows(0)(column).ToString.ToLower.Trim <> "timestamp" Then


                Dim str As String() = dt.Rows(0)(column).Split(".")
                Dim str1 As String() = str(1).Split("_")

                dt.Rows(0)(column) = str1(0) & "_" & str1(1) & "_" & str1(2) & "_del"

            End If


        Next

    End Sub


    Public Shared Sub CreateRealTimeEntries(ByVal dt As DataTable, ByRef rtDt As DataTable)

        For Each column As DataColumn In dt.Columns

            If Not column.ColumnName.ToLower.Trim.Replace(" ", "") = "timestamp" Then

                For Each row As DataRow In dt.Rows

                    If IsDate(row(0)) Then

                        Dim newRtRow As DataRow = rtDt.NewRow

                        newRtRow("DateTime") = row(0)
                        newRtRow("MeterName") = column.ColumnName
                        newRtRow("CountUnits") = row(column.ColumnName)
                        rtDt.Rows.Add(newRtRow)

                    End If

                Next

            End If

        Next


    End Sub



End Class
