﻿Public Class NPowerInvoice
    Implements ITransformDelegate
    Private _returnTable As DataTable


    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData

        Dim list As New List(Of String)
        Dim csvFile As New DelimitedFile(fullPath, 13)
        csvFile.HasHeader = False
        csvFile.TrimWhiteSpace = True
        csvFile.FieldsAreQuoted = True
        list = NPowerInvoiceSchema.GetHeaders(13)

        csvFile.LoadNonConsistentTable(_returnTable, list)

    End Sub

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Dim NPowerInvoiceSchema As New NPowerInvoiceSchema

        NPowerInvoiceSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub



End Class

