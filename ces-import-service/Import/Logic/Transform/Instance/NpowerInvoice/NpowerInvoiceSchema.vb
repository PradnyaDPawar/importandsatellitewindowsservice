﻿Public Class NPowerInvoiceSchema

    Public Shared _InvNo As String
    Public Shared _MPAN As String
    Public Shared _idate As Date
    Public Shared _prevreading As String
    Public Shared _prevreadingtype As String
    Public Shared _currentreading As String
    Public Shared _currentreadingtype As String

#Region "Properties"

    Public Shared Property InvNumber() As String

        Get
            Return _InvNo
        End Get
        Set(ByVal value As String)
            _InvNo = value
        End Set
    End Property

    Public Shared Property MPAN() As String

        Get
            Return _MPAN
        End Get
        Set(ByVal value As String)
            _MPAN = value
        End Set
    End Property

    Public Shared Property idate() As String

        Get
            Return _idate
        End Get
        Set(ByVal value As String)
            _idate = value
        End Set
    End Property

    Public Shared Property PrevReading() As String

        Get
            Return _prevreading
        End Get
        Set(ByVal value As String)
            _prevreading = value
        End Set
    End Property

    Public Shared Property PrevReadingType() As String

        Get
            Return _prevreadingtype
        End Get
        Set(ByVal value As String)
            _prevreadingtype = value
        End Set
    End Property

    Public Shared Property CurrentReading() As String

        Get
            Return _currentreading
        End Get
        Set(ByVal value As String)
            _currentreading = value
        End Set
    End Property

    Public Shared Property CurrentReadingType() As String

        Get
            Return _currentreadingtype
        End Get
        Set(ByVal value As String)
            _currentreadingtype = value
        End Set
    End Property

#End Region

    Public Shared ListCostOnlyYes As New List(Of String)(New String() {"Feed In Tariff", "Maximum Demand kVA", "Administration", "CLIMATE CHANGE LEVY", "VAT"})

    Public Shared Function GetHeaders(ByVal colcount As Integer) As List(Of String)

        Dim headers As New List(Of String)

        For index = 0 To colcount - 1
            headers.Add(index)
        Next

        Return headers
    End Function

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)
        _InvNo = dt.Rows(5)(1).ToString()
        _MPAN = dt.Rows(15)(1).ToString()
        _idate = dt.Rows(6)(1).ToString()

        DataTableTools.SetHeaders(dt, 18)

        For i = 0 To 16 Step 1

            dt.Rows.RemoveAt(0)

        Next

        Dim invoiceTbl As DataTable = DeFormatTableTemplates.CreateInvoiceTable

        For Each row As DataRow In dt.Rows

            ' Ensure not reading charge type

            If row("Charge type") <> "Reading" Then

                If row("Charge type") = "Consumption charge" Then

                    PopulateReadEstimation(row)
                    PopulateConsumptionCharge(row, invoiceTbl)

                Else

                    PopulateOtherCharge(row, invoiceTbl)

                End If

            End If

        Next

        RenameColumns(invoiceTbl)
        dt = invoiceTbl

    End Sub

    Private Shared Sub PopulateConsumptionCharge(ByVal row As DataRow, ByRef invoiceTbl As DataTable)
        'populate non reading details

        Dim sdate As String()

        sdate = row.Table(1)(0).ToString().Split(New String() {"to"}, StringSplitOptions.None)

        Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow

        newInvoiceTblRow("Invoice Number") = InvNumber

        newInvoiceTblRow("Meter Serial") = ""

        newInvoiceTblRow("MPAN") = MPAN

        newInvoiceTblRow("Start Date") = sdate(0).Trim()

        newInvoiceTblRow("End Date") = sdate(1).Trim()

        newInvoiceTblRow("Consumption") = row("Quantity")

        newInvoiceTblRow("Force Kwh") = "Yes"

        newInvoiceTblRow("Net") = row("Charge").ToString.Replace("�", "")

        newInvoiceTblRow("Vat") = 0

        newInvoiceTblRow("Gross") = newInvoiceTblRow("Net") + 0

        newInvoiceTblRow("Description") = row("Description")

        newInvoiceTblRow("Cost Only") = IsCostOnly(row("Description"))

        If newInvoiceTblRow("Cost Only") = "No" Then

            newInvoiceTblRow("Rate") = row("Description")

        End If

        newInvoiceTblRow("TaxPointDate") = idate

        newInvoiceTblRow("IsLastDayApportion") = "No"

        'populate reading using PopulateReadings()
        PopulateReadings(newInvoiceTblRow, row.Table)

        invoiceTbl.Rows.Add(newInvoiceTblRow)

    End Sub

    Private Shared Sub PopulateOtherCharge(ByVal row As DataRow, ByRef invoiceTbl As DataTable)

        Dim net, vat As Double
        Dim sdate As String()

        sdate = row.Table(1)(0).ToString().Split(New String() {"to"}, StringSplitOptions.None)

        Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow

        newInvoiceTblRow("Invoice Number") = InvNumber

        newInvoiceTblRow("Meter Serial") = ""

        newInvoiceTblRow("MPAN") = MPAN

        newInvoiceTblRow("Start Date") = sdate(0).Trim()

        newInvoiceTblRow("End Date") = sdate(1).Trim()

        newInvoiceTblRow("Start Read") = row("Previous reading")

        newInvoiceTblRow("Start Read Estimated") = row("Previous reading type")

        newInvoiceTblRow("End Read") = row("Current reading")

        newInvoiceTblRow("End Read Estimated") = row("Current reading type")

        newInvoiceTblRow("Consumption") = row("Quantity")

        newInvoiceTblRow("Force Kwh") = "Yes"

        row("Charge") = row("Charge").ToString.Replace("�", "")
        net = Val(row("Charge"))

        row("Charge") = row("Charge").ToString.Replace("�", "")
        vat = Val(row("Charge"))

        newInvoiceTblRow("Net") = net

        If row("Description") = "VAT" Then

            newInvoiceTblRow("Vat") = vat
            newInvoiceTblRow("Net") = 0
            newInvoiceTblRow("Gross") = vat + 0

        Else

            newInvoiceTblRow("Net") = net
            newInvoiceTblRow("Vat") = 0
            newInvoiceTblRow("Gross") = net + 0

        End If

        newInvoiceTblRow("Description") = row("Description")

        newInvoiceTblRow("Cost Only") = IsCostOnly(row("Description"))

        If newInvoiceTblRow("Cost Only") = "No" Then

            newInvoiceTblRow("Rate") = row("Description")

        End If

        newInvoiceTblRow("TaxPointDate") = idate

        newInvoiceTblRow("IsLastDayApportion") = "No"

        invoiceTbl.Rows.Add(newInvoiceTblRow)

    End Sub

    Public Shared Function IsCostOnly(ByVal value As String)

        If ListCostOnlyYes.Contains(value) Then

            Return "Yes"

        Else
            Return "No"

        End If


    End Function

    Private Shared Sub PopulateReadEstimation(ByVal row As DataRow)
        Dim ListActual As New List(Of Char)(New Char() {"a", "c", "f", "i", "m", "p", "r", "s", "b"})
        Dim ListEstimated As New List(Of Char)(New Char() {"d", "e"})

        ' Start Read Estimated
        If ListActual.Contains(row("Previous reading type").ToString.ToLower) Then
            row("Previous reading type") = "A"
        ElseIf ListEstimated.Contains(row("Previous reading type").ToString.ToLower) Then
            row("Previous reading type") = "E"
        End If

        ' End Read Estimated

        If ListActual.Contains(row("Current reading type").ToString.ToLower) Then
            row("Current reading type") = "A"
        ElseIf ListEstimated.Contains(row("Current reading type").ToString.ToLower) Then
            row("Current reading type") = "E"
        End If

    End Sub

    Public Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        dt.Columns("Meter Serial").ColumnName = "MeterSerial"
        dt.Columns("Rate").ColumnName = "Tariff"
        dt.Columns("Start Date").ColumnName = "StartDate"
        dt.Columns("End Date").ColumnName = "EndDate"
        dt.Columns("Start Read").ColumnName = "StartRead"
        dt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        dt.Columns("End Read").ColumnName = "EndRead"
        dt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        dt.Columns("Consumption").ColumnName = "Consumption"
        dt.Columns("Force Kwh").ColumnName = "ForceKwh"
        dt.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub


    Private Shared Sub PopulateReadings(ByRef newInvoiceTblRow As DataRow, ByRef dt As DataTable)


        Dim resultRows() As DataRow = dt.Select("[Charge Type] = 'Reading' And [Description] = 'ALL DAY'")

        If resultRows.Count > 0 Then

            PrevReading = resultRows(0)("Previous reading")
            CurrentReading = resultRows(0)("Current reading")
            PrevReadingType = resultRows(0)("Previous reading type")
            CurrentReadingType = resultRows(0)("Current reading type")

            For Each row As DataRow In resultRows

                If row("Previous reading") <= PrevReading Then
                    PrevReading = row("Previous reading")
                    PrevReadingType = row("Previous reading type")
                End If


                If row("current reading") >= CurrentReading Then
                    CurrentReading = row("Current reading")
                    CurrentReadingType = row("Current reading type")
                End If

            Next

            newInvoiceTblRow("Start Read") = PrevReading
            newInvoiceTblRow("Start Read Estimated") = PrevReadingType
            newInvoiceTblRow("End Read") = CurrentReading
            newInvoiceTblRow("End Read Estimated") = CurrentReadingType

        End If


    End Sub

End Class
