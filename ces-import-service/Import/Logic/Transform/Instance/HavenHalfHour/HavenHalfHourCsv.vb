﻿Public Class HavenHalfHourCsv
    Implements ITransformDelegate



    Private _returnTable As DataTable

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvFile As New DelimitedFile(fullPath, 0)
        csvFile.HasHeader = False
        csvFile.Load(_returnTable)
    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        DataTableTools.RemoveBlankRows(ReturnTable)
        DataTableTools.SetHeaders(ReturnTable, 4)
        HavenHalfHourSchema.Validate(ReturnTable)
        HavenHalfHourSchema.ValidateAddtionalHeaders(ReturnTable)
        HavenHalfHourSchema.GetTransformedHeaders(ReturnTable)
        HavenHalfHourSchema.DeleteNonHHDataRows(ReturnTable)
    End Sub
End Class
