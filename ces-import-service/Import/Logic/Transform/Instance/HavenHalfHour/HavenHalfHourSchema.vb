﻿Public Class HavenHalfHourSchema

    Public Shared rowcount As Integer

    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        
        headers.Add("GMT")
        headers.Add("00:00")
        headers.Add("00:30")
        headers.Add("01:00")
        headers.Add("01:30")
        headers.Add("02:00")
        headers.Add("02:30")
        headers.Add("03:00")
        headers.Add("03:30")
        headers.Add("04:00")
        headers.Add("04:30")
        headers.Add("05:00")
        headers.Add("05:30")
        headers.Add("06:00")
        headers.Add("06:30")
        headers.Add("07:00")
        headers.Add("07:30")
        headers.Add("08:00")
        headers.Add("08:30")
        headers.Add("09:00")
        headers.Add("09:30")
        headers.Add("10:00")
        headers.Add("10:30")
        headers.Add("11:00")
        headers.Add("11:30")
        headers.Add("12:00")
        headers.Add("12:30")
        headers.Add("13:00")
        headers.Add("13:30")
        headers.Add("14:00")
        headers.Add("14:30")
        headers.Add("15:00")
        headers.Add("15:30")
        headers.Add("16:00")
        headers.Add("16:30")
        headers.Add("17:00")
        headers.Add("17:30")
        headers.Add("18:00")
        headers.Add("18:30")
        headers.Add("19:00")
        headers.Add("19:30")
        headers.Add("20:00")
        headers.Add("20:30")
        headers.Add("21:00")
        headers.Add("21:30")
        headers.Add("22:00")
        headers.Add("22:30")
        headers.Add("23:00")
        headers.Add("23:30")
       
        Return headers

    End Function

    Public Shared Function GetAdditionalHeaders() As List(Of String)
        Dim Additionalheaders As New List(Of String)

        Additionalheaders.Add("Customer Account Number")
        Additionalheaders.Add("Customer Site Ref:")
        Additionalheaders.Add("Invoice Number:")
        Additionalheaders.Add("Site Reference:")
        Additionalheaders.Add("MPAN Core:")
        Additionalheaders.Add("Period Start:")
        Additionalheaders.Add("Period End:")
        Additionalheaders.Add("Meter Serial Number")
        Additionalheaders.Add("Effective From Date")
        Additionalheaders.Add("Effective To Date")
        Additionalheaders.Add("Maximum Demand(MD)")
        Additionalheaders.Add("Date and Time of MD")

        Return Additionalheaders

    End Function

    Public Shared Function Validate(ByRef data As DataTable) As Boolean

        For Each header As String In GetHeaders()
            If Not data.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next

    End Function

    Public Shared Function ValidateAddtionalHeaders(ByRef data As DataTable) As Boolean

        Dim AdditionalHeaderslist As New List(Of String)
        AdditionalHeaderslist = GetAdditionalHeaders()

        For colcount As Integer = 0 To data.Columns.Count - 1
            For Each row As DataRow In data.Rows
                Dim header As String = row(colcount).ToString.Trim
                If AdditionalHeaderslist.Contains(header) Then
                    AdditionalHeaderslist.Remove(header)
                End If
            Next
        Next

        If AdditionalHeaderslist.Count > 1 Then

            For Each missingheader As String In AdditionalHeaderslist
                Throw New ApplicationException("Missing Column " & missingheader & ". ")
            Next

            Return False

        End If

        Return True
    End Function

    Public Shared Sub GetTransformedHeaders(ByRef data As DataTable)

        RenameColumns(data)
        CreateColumns(data)
        PopulateColumns(data)

    End Sub

    Public Shared Sub RenameColumns(ByRef dt As DataTable)

        dt.Columns("GMT").ColumnName = "Date(dd/mm/yyyy)"

    End Sub

    Public Shared Sub CreateColumns(ByRef dt As DataTable)

        dt.Columns.Add("MPAN")
        dt.Columns.Add("MeterSerial")
        dt.Columns.Add("ForcekWh")

    End Sub

    Public Shared Sub PopulateColumns(ByRef dt As DataTable)

        Dim mpan As String = GetMpan(dt)

        Dim meterserial As String = GetMeterSerial(dt)
       

        For Each row As DataRow In dt.Rows
            If Not row("Date(dd/mm/yyyy)") = "" Then
                PopulateMpan(dt, row, mpan)
                PopulateMeterSerial(dt, row, meterserial)
                PopulateForcekWh(dt, row)
            End If
        Next

    End Sub

    Public Shared Sub PopulateMpan(ByRef dt As DataTable, ByVal rowtopopulate As DataRow, ByVal mpan As String)

        rowtopopulate("MPAN") = mpan

    End Sub

    Public Shared Sub PopulateMeterSerial(ByRef dt As DataTable, ByVal rowtopopulate As DataRow, ByVal meterserial As String)
        rowtopopulate("MeterSerial") = meterserial

    End Sub

    Public Shared Sub PopulateForcekWh(ByRef dt As DataTable, ByVal rowtopopulate As DataRow)
        rowtopopulate("ForcekWh") = "Yes"
    End Sub

    Public Shared Function GetMpan(ByRef dt As DataTable) As String
        Dim found As Boolean
        For Each row As DataRow In dt.Rows
            For Each item As Object In row.ItemArray

                If found Then
                    Return (item.ToString)
                End If

                If item.ToString.Replace(" ", "").ToLower.Trim = "mpancore:" Then
                    found = True

                End If
            Next
        Next

        Throw New ApplicationException("MPAN not found")

    End Function


    Public Shared Function GetMeterSerial(ByRef dt As DataTable) As String
        Dim rowcounter As Integer
        Dim found As Boolean

        For rowcounter = 0 To dt.Rows.Count - 1 Step 1

            If found Then
                Return dt.Rows(rowcounter)(0)
            End If

            If dt.Rows(rowcounter)(0).ToString.Replace(" ", "").ToLower.Trim = "meterserialnumber" Then
                found = True
                rowcount = rowcounter
            End If

        Next

        Throw New ApplicationException("Meter Serial not found")

    End Function

    Public Shared Sub DeleteNonHHDataRows(ByRef dt As DataTable)

        For Each row As DataRow In dt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            If Not IsDate(row("Date(dd/mm/yyyy)")) Then

                row.Delete()

            End If

        Next

        dt.AcceptChanges()

    End Sub


End Class
