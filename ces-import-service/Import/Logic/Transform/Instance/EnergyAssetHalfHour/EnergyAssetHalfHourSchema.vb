﻿Partial Public Class EnergyAssetHalfHourSchema

    Public Shared Function ToDeFormat(ByRef dt As DataTable)

        AddColumns(dt)
        RenameColumnNames(dt)

        Return dt

    End Function
    Public Shared Sub AddColumns(ByRef dt As DataTable)

        dt.Columns.Add("ForcekWh")

    End Sub

    Public Shared Sub RenameColumnNames(ByRef dt As DataTable)

        dt.Columns("Site Id").ColumnName = "MPAN"
        dt.Columns("Meter Number").ColumnName = "MeterSerial"
        dt.Columns("Reading Date").ColumnName = "Date(dd/MM/yyyy)"

    End Sub

End Class
