﻿Public Class DigitalEnergyRealTime
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property


    Public Sub LoadData(ByVal stagingFilePath As String) Implements ITransformDelegate.LoadData

        Dim excel As New Excel
        Dim xSheet As ExcelSheet
        excel.Load(stagingFilePath)

        xSheet = excel.Sheets(0)

        xSheet.HeaderRowIndex = 1
        _returnTable = xSheet.GetDataTable()

    End Sub


    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        'validate the new table

        RenameColumns()
        ValidateHeaders()


        For Each row As DataRow In _returnTable.Rows
            FE_RealTime.Fill(row, dsEdits, importEdit)
        Next

    End Sub




End Class
