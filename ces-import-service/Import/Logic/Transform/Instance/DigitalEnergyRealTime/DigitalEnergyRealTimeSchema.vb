﻿Partial Public Class DigitalEnergyRealTime

    'Public Function GetHeaders() As List(Of String)

    '    Dim headers As New List(Of String)
    '    headers.Add("bat")
    '    headers.Add("db")
    '    headers.Add("metername")
    '    headers.Add("timestamp")
    '    headers.Add("value")

    '    Return headers

    'End Function

    Public Function GetReducedHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("metername")
        headers.Add("datetime")
        headers.Add("countunits")

        Return headers

    End Function


    Public Function ValidateHeaders() As Boolean
        For Each header As String In GetReducedHeaders()
            If Not _returnTable.Columns.Contains(header) Then
                Throw New ApplicationException("Missing Column " & header & ". ")
            End If
        Next
    End Function


    Public Sub RenameColumns()
        For Each column As DataColumn In _returnTable.Columns
            column.ColumnName = column.ColumnName.Replace(" ", "")
            If column.ColumnName.ToLower = "timestamp" Then column.ColumnName = "datetime"
            If column.ColumnName.ToLower = "value" Then column.ColumnName = "countunits"
        Next
    End Sub






End Class
