﻿Public Class Haven2InvoiceSchema

    Private _readDt As DataTable
    Private _summaryDt As DataTable
    Private _otherchargesDt As DataTable
    Private _unitRateDt As DataTable

    Public Shared _invNumber As String

    Private _hasReadData As Boolean

    Public Shared ListCostOnlyYes As New List(Of String)(New String() {"Excess Capacity", "Reactive Power", "Availability", "Climate Change Levy", "Standing Charge", "Data Collector & Data Aggregator"})

#Region "Properties"


    Property ReadDt() As DataTable
        Get
            Return _readDt
        End Get
        Set(ByVal value As DataTable)
            _readDt = value
        End Set
    End Property

    Property UnitRateDt() As DataTable
        Get
            Return _unitRateDt
        End Get
        Set(value As DataTable)
            _unitRateDt = value
        End Set
    End Property


    Property SummaryDt() As DataTable
        Get
            Return _summaryDt
        End Get
        Set(ByVal value As DataTable)
            _summaryDt = value
        End Set
    End Property

    Property OtherChargesDt() As DataTable
        Get
            Return _otherchargesDt
        End Get
        Set(ByVal value As DataTable)
            _otherchargesDt = value
        End Set
    End Property
    Public Shared Property InvoiceNumber() As String
        Get
            Return _invNumber
        End Get
        Set(value As String)
            _invNumber = value
        End Set
    End Property


#End Region


    Public Sub ToDeFormat(ByRef excel As Excel, ByRef dt As DataTable)

        Dim xSheet As ExcelSheet = excel.Sheets(0)

        UnitRateDt = SheetToDt(excel, xSheet, "Unit_Rates", 1)
        SummaryDt = SheetToDt(excel, xSheet, "Summary", 18)
        OtherChargesDt = SheetToDt(excel, xSheet, "Other_Charges", 1)

        If excel.GetSheet("Reads", xSheet) Then
            _hasReadData = True
            ReadDt = SheetToDt(excel, xSheet, "Reads", 1)
        End If


        Dim invoiceTbl As DataTable = DeFormatTableTemplates.CreateInvoiceTable

        For Each row As DataRow In SummaryDt.Rows

            If Not String.IsNullOrEmpty(row("Invoice Number")) And Not String.IsNullOrEmpty(row("Total Charges")) Then

                InvoiceNumber = row("Invoice Number")

                'populate Vat
                PopulateVat(row, invoiceTbl)

                'Populate Unit Rate Charges
                PopulateInvoiceRows(row("MPAN"), UnitRateDt, invoiceTbl)

                'Populate Other charges
                PopulateInvoiceRows(row("MPAN"), OtherChargesDt, invoiceTbl)

            End If

        Next

        RenameColumns(invoiceTbl)
        dt = invoiceTbl


    End Sub



    Public Shared Function IsCostOnly(value As String)

        If ListCostOnlyYes.Contains(value) Then

            Return "Yes"

        Else
            Return "No"

        End If


    End Function


    'Private Shared Sub PopulateReadEstimation(ByRef dt As DataTable, ByRef readDt As DataTable)

    '    dt.Columns.Add("Start Read Estimated")
    '    dt.Columns.Add("End Read Estimated")


    '    For Each row As DataRow In readDt.Rows


    '        Dim ListActual As New List(Of Char)(New Char() {"a", "c", "f", "i", "m", "p", "r", "s", "b"})
    '        Dim ListEstimated As New List(Of Char)(New Char() {"d", "e"})

    '        ' Start Read Estimated
    '        If ListActual.Contains(row("Start Read Type").ToString.ToLower) Then
    '            row("Start Read Type") = "A"
    '        ElseIf ListEstimated.Contains(row("Start Read Type").ToString.ToLower) Then
    '            row("Start Read Type") = "E"
    '        End If

    '        SReadType = row("Start Read Type")
    '        ' End Read Estimated


    '        If ListActual.Contains(row("End Read Type").ToString.ToLower) Then
    '            row("End Read Type") = "A"
    '        ElseIf ListEstimated.Contains(row("End Read Type").ToString.ToLower) Then
    '            row("End Read Type") = "E"
    '        End If

    '        EReadType = row("End Read Type")

    '        For Each dr As DataRow In dt.Rows

    '            dr("Start Read Estimated") = SReadType
    '            dr("End Read Estimated") = EReadType


    '        Next


    '    Next
    'End Sub


    Private Function SheetToDt(ByRef excel As Excel, ByRef xSheet As ExcelSheet, ByVal sheetName As String, ByVal headerRowIndex As Integer) As DataTable

        Dim tempDt As New DataTable

        If excel.GetSheet(sheetName, xSheet) Then

            xSheet.HeaderRowIndex = headerRowIndex

            tempDt = xSheet.GetDataTable()

            'tempDt.Rows.RemoveAt(0)

            DataTableTools.RemoveBlankColumns(tempDt)

            DataTableTools.RemoveBlankRows(tempDt)

        End If

        Return tempDt

    End Function




    Private Shared Sub PopulateVat(row As DataRow, ByRef invoiceTbl As DataTable)

        Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow

        newInvoiceTblRow("Invoice Number") = row("Invoice Number")

        newInvoiceTblRow("MPAN") = row("MPAN")

        newInvoiceTblRow("Start Date") = row("Period Start Date")

        newInvoiceTblRow("End Date") = row("Period End Date")

        newInvoiceTblRow("Force Kwh") = "No"

        newInvoiceTblRow("Net") = 0

        newInvoiceTblRow("Vat") = row("VAT Payable")

        newInvoiceTblRow("Gross") = 0 + row("VAT Payable")

        newInvoiceTblRow("Description") = "Vat"

        newInvoiceTblRow("Cost Only") = "Yes"

        newInvoiceTblRow("TaxPointDate") = ""

        newInvoiceTblRow("IsLastDayApportion") = "No"


        invoiceTbl.Rows.Add(newInvoiceTblRow)


    End Sub

    Private Shared Sub PopulateInvoiceRows(mpan As String, dt As DataTable, ByRef invoiceTbl As DataTable)


        Dim resultrows() = dt.Select("[MPAN]='" & mpan & "'")

        For Each row As DataRow In resultrows


            Dim newInvoiceTblRow As DataRow = invoiceTbl.NewRow

            newInvoiceTblRow("Invoice Number") = InvoiceNumber

            newInvoiceTblRow("MPAN") = row("MPAN")

            newInvoiceTblRow("Rate") = row("Tariff Line Description")

            newInvoiceTblRow("Start Date") = row("Period Start Date")

            newInvoiceTblRow("End Date") = row("Period End Date")

            newInvoiceTblRow("Force Kwh") = "Yes"

            newInvoiceTblRow("Net") = row("Unit Charge")

            newInvoiceTblRow("Vat") = 0

            newInvoiceTblRow("Gross") = row("Unit Charge") + 0

            newInvoiceTblRow("Description") = row("Tariff Line Description")

            newInvoiceTblRow("Consumption") = row("Consumption")

            newInvoiceTblRow("Cost Only") = IsCostOnly(row("Tariff Line Description"))

            newInvoiceTblRow("TaxPointDate") = ""

            newInvoiceTblRow("IsLastDayApportion") = "No"

            invoiceTbl.Rows.Add(newInvoiceTblRow)


        Next

    End Sub

    Private Sub RenameColumns(invoiceTbl As DataTable)

        invoiceTbl.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        invoiceTbl.Columns("Meter Serial").ColumnName = "MeterSerial"
        invoiceTbl.Columns("Rate").ColumnName = "Tariff"
        invoiceTbl.Columns("Start Date").ColumnName = "StartDate"
        invoiceTbl.Columns("End Date").ColumnName = "EndDate"
        invoiceTbl.Columns("Start Read").ColumnName = "StartRead"
        invoiceTbl.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        invoiceTbl.Columns("End Read").ColumnName = "EndRead"
        invoiceTbl.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        invoiceTbl.Columns("Force kWh").ColumnName = "ForcekWh"
        invoiceTbl.Columns("Cost Only").ColumnName = "CostOnly"


    End Sub

End Class
