﻿Imports System.Globalization

Public Class NorthumbrianInvoiceSchema
    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable
        Dim InvoiceDt As DataTable = DeFormatTableTemplates.CreateInvoiceTable()
        Dim dtResult As DataTable = CreateTable(dt)
        RenameColumns(InvoiceDt)
        PopulateColumns(dtResult, InvoiceDt)

        dt = dtResult
        Return dt
    End Function
    ''' <summary>
    ''' Method to create table in de format as per the requirement
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function CreateTable(ByRef dt As DataTable)
        ''0 th blank row and i th row is for total of every cost 
        Dim i As Integer
        dt.Rows.RemoveAt(0)
        For i = 0 To dt.Rows.Count - 1


            If (String.IsNullOrEmpty(dt.Rows(i).Item(0).ToString().Trim())) Then
                dt.Rows.Remove(dt.Rows(i))
            End If
        Next

        Dim indexFixedWater = dt.Columns.IndexOf("FIXED WATER")
        Dim indexVolumeWater = dt.Columns.IndexOf("VOLUME WATER")
        ''shifting in order to get fixed water data row under volume water data row
        dt.Columns("VOLUME WATER").SetOrdinal(indexFixedWater)

        ''created new datatable
        Dim dtTranspose As DataTable = New DataTable()
        dtTranspose.Columns.Add("StartDate", GetType(String))
        dtTranspose.Columns.Add("EndDate", GetType(String))
        dtTranspose.Columns.Add("InvoiceNumber", GetType(String))
        dtTranspose.Columns.Add("MPAN", GetType(String))
        dtTranspose.Columns.Add("MeterSerial", GetType(String))
        dtTranspose.Columns.Add("EndRead", GetType(String))
        dtTranspose.Columns.Add("StartRead", GetType(String))
        dtTranspose.Columns.Add("Consumption", GetType(String))
        dtTranspose.Columns.Add("GrossCost", GetType(String))
        dtTranspose.Columns.Add("Description", GetType(String))
        dtTranspose.Columns.Add("Tariff", GetType(String))
        dtTranspose.Columns.Add("CostOnly", GetType(String))
        dtTranspose.Columns.Add("ForcekWh", GetType(String))
        For Each row As DataRow In dt.Rows

            For Each col As DataColumn In dt.Columns

                Dim newRow As DataRow = dtTranspose.NewRow
                If (col.ColumnName = "FIXED WATER" Or col.ColumnName = "VOLUME WATER" Or col.ColumnName = "FIXED SEW" Or col.ColumnName = "VOLUME SEW") Then
                    Dim sdate As Date = CDate(row("PERIOD").ToString().Split(" ")(0)).ToString("dd/MM/yyyy")
                    Dim edate As Date = CDate(row("PERIOD").ToString().Split(" ")(2)).ToString("dd/MM/yyyy")


                    newRow("StartDate") = sdate.ToString("dd/MM/yyyy")
                    newRow("EndDate") = edate.ToString("dd/MM/yyyy")
                    newRow("InvoiceNumber") = row("ACCOUNT NUMBER").ToString() + row("BILL NB").ToString()
                    newRow("MPAN") = row("ACCOUNT NUMBER")
                    newRow("MeterSerial") = row("METER NB")

                    newRow("GrossCost") = row(col.ColumnName)
                    newRow("Description") = col.ColumnName
                    newRow("Tariff") = col.ColumnName
                    newRow("ForcekWh") = "No"
                    ''for Volume water CostOnly is NO as per spec
                    If (col.ColumnName = "VOLUME WATER") Then
                        newRow("CostOnly") = "No"
                        newRow("EndRead") = row("CURRENT READ")
                        newRow("StartRead") = row("PREVIOUS READ")
                        newRow("Consumption") = row("MEAS VOL")
                    Else

                        newRow("CostOnly") = "Yes"

                    End If
                    dtTranspose.Rows.Add(newRow)

                End If
            Next

        Next

        Return dtTranspose

    End Function
    ''' <summary>
    ''' Method to insert required data into invoice table
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <param name="InvoiceDt"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef InvoiceDt As DataTable)
        For Each row As DataRow In dt.Rows
            Dim newInvoiceTblRow As DataRow = InvoiceDt.NewRow
            newInvoiceTblRow("InvoiceNumber") = row("InvoiceNumber")

            newInvoiceTblRow("MPAN") = row("MPAN")

            newInvoiceTblRow("MeterSerial") = row("MeterSerial")
            newInvoiceTblRow("StartDate") = row("StartDate")

            newInvoiceTblRow("EndDate") = row("EndDate")

            newInvoiceTblRow("ForcekWh") = row("ForcekWh")

            newInvoiceTblRow("Gross") = row("GrossCost")

            newInvoiceTblRow("Description") = row("Description")
            newInvoiceTblRow("Tariff") = row("Tariff")

            newInvoiceTblRow("Consumption") = row("Consumption")

            newInvoiceTblRow("CostOnly") = row("CostOnly")
            newInvoiceTblRow("StartRead") = row("StartRead")
            newInvoiceTblRow("EndRead") = row("EndRead")

            InvoiceDt.Rows.Add(newInvoiceTblRow)

        Next

        dt = InvoiceDt
    End Sub

    '''<summary>
    ''' Method to rename invoice table column name 
    ''' </summary>
    ''' <param name="InvoiceDt"></param>
    ''' <remarks></remarks>
    Private Shared Sub RenameColumns(ByVal InvoiceDt As DataTable)
        InvoiceDt.Columns("Invoice Number").ColumnName = "InvoiceNumber"
        InvoiceDt.Columns("Meter Serial").ColumnName = "MeterSerial"
        InvoiceDt.Columns("Rate").ColumnName = "Tariff"
        InvoiceDt.Columns("Start Date").ColumnName = "StartDate"
        InvoiceDt.Columns("End Date").ColumnName = "EndDate"
        InvoiceDt.Columns("Start Read").ColumnName = "StartRead"
        InvoiceDt.Columns("Start Read Estimated").ColumnName = "StartReadEstimated"
        InvoiceDt.Columns("End Read").ColumnName = "EndRead"
        InvoiceDt.Columns("End Read Estimated").ColumnName = "EndReadEstimated"
        InvoiceDt.Columns("Force kWh").ColumnName = "ForceKwh"
        InvoiceDt.Columns("Cost Only").ColumnName = "CostOnly"

    End Sub
End Class
