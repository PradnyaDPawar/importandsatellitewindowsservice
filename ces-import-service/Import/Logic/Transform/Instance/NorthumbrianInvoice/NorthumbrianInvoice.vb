﻿Imports System.Xml


Public Class NorthumbrianInvoice
    Implements ITransformDelegate
    Private _returnTable As System.Data.DataTable

    Public Property ReturnTable() As System.Data.DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As System.Data.DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim excel As New Excel()
        excel.Load(fullPath)
        Dim xSheet As ExcelSheet = excel.Sheets(0)
        ReturnTable = xSheet.GetDataTable()

        ReturnTable.Rows.RemoveAt(0)


        DataTableTools.SetHeaders(ReturnTable, 2)


    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat


        NorthumbrianInvoiceSchema.ToDeFormat(_returnTable)
        For Each row As DataRow In _returnTable.Rows
            FE_Invoice.Fill(row, dsEdits, importEdit)
        Next
    End Sub
End Class
