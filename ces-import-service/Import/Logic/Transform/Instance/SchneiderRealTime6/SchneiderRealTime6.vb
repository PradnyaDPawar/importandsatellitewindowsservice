﻿Public Class SchneiderRealTime6
    Implements ITransformDelegate

    Public Property ReturnTable As DataTable Implements ITransformDelegate.ReturnTable

    Public Sub LoadData(fullPath As String) Implements ITransformDelegate.LoadData

        Dim csvFile As New DelimitedFile(fullPath, 0)
        csvFile.LoadNonConsistentTable(ReturnTable, ",")

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        Dim realTimeSchema As New SchneiderRealTimeSchema6
        realTimeSchema.ToDeFormat(ReturnTable)

        For Each row As DataRow In realTimeSchema.DeRealTimeTable.Rows

            FE_RealTime.Fill(row, dsEdits, importEdit)
        Next

    End Sub

End Class