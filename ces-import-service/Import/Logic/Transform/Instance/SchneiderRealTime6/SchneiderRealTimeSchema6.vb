﻿Imports System.Globalization

''' <summary>
''' This class provides the logic to prepare the De format real time table for SchneiderRealTime6 import format family.
''' </summary>
Public Class SchneiderRealTimeSchema6

#Region "Member variables"

    Private _meterName As String
    Private _deRealTimeTable As DataTable

#End Region

#Region "Constructor"

    Public Sub New()

        _meterName = String.Empty
        _deRealTimeTable = DeFormatTableTemplates.CreateRealTimeTable()

    End Sub

#End Region

#Region "Public properties"

    Public Property MeterName() As String
        Get
            Return _meterName
        End Get
        Set(ByVal value As String)
            _meterName = value
        End Set
    End Property

    Public Property DeRealTimeTable() As DataTable
        Get
            Return _deRealTimeTable
        End Get
        Set(ByVal value As DataTable)
            _deRealTimeTable = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' This method will prepare the De format real time table from SchneiderRealTime6 data table.
    ''' </summary>
    ''' <param name="dtRealTime">Data table loaded from SchneiderRealTime6 file</param>
    Public Sub ToDeFormat(ByRef dtRealTime As DataTable)

        DataTableTools.SetHeaders(dtRealTime, 1)

        SaveMeterName(dtRealTime)

        DataTableTools.RemoveBlankRows(dtRealTime)
        DataTableTools.RemoveTopRows(dtRealTime, 3)
        DataTableTools.RemoveBlankColumns(dtRealTime)

        DataTableTools.SetHeaders(dtRealTime, 1)

        PopulateDeRealTimeTable(dtRealTime)

    End Sub

#Region "Member methods"

    ''' <summary>
    ''' This method will save the meter name present in
    ''' </summary>
    ''' <param name="dtRealTime"></param> SchneiderRealTime6 table.
    Private Sub SaveMeterName(ByRef dtRealTime As DataTable)

        MeterName = dtRealTime.Rows(0)("Device Name").ToString()

    End Sub

    ''' <summary>
    ''' This method finally adds the rows in De format real time data table from the SchneiderRealTime6 data table data.
    ''' </summary>
    Private Sub PopulateDeRealTimeTable(ByRef dtRealTime As DataTable)

        ' Iterating through each row of real time invoice table.
        For Each row As DataRow In dtRealTime.Rows

            Dim targetRow As DataRow = DeRealTimeTable.NewRow()
            CopyRow(row, targetRow)
            DeRealTimeTable.Rows.Add(targetRow)

        Next

    End Sub

    Private Sub CopyRow(ByRef sourceRow As DataRow, ByRef targetRow As DataRow)

        ' De format realtime table columns name = MeterName, DateTime, CountUnits

        targetRow("MeterName") = MeterName

        If Not String.IsNullOrEmpty(sourceRow("Local Time Stamp").ToString()) Then

            Dim localDateTime As DateTime
            DateTime.TryParseExact(sourceRow("Local Time Stamp").ToString(), "MM-dd-yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, localDateTime)
            targetRow("DateTime") = localDateTime.ToString("dd-MM-yyyy HH:mm")

        Else

            targetRow("DateTime") = sourceRow("Local Time Stamp")

        End If

        targetRow("CountUnits") = sourceRow("Active Energy (Wh)")

    End Sub

#End Region

End Class