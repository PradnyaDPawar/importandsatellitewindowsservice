﻿Public Class VmucEmRealTime

    Implements ITransformDelegate

    Private _returnTable As DataTable


    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvfile As New DelimitedFile(fullPath, 55)

        csvfile.HasHeader = False
        csvfile.Delimiter = ";"
        csvfile.LoadNonConsistentTable(_returnTable, VmucEmRealTimeSchema.GetHeaders)
        'csvfile.Load(_returnTable)

    End Sub



    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat

        'VMURealTimeSchema.GetHeaders()
        VmucEmRealTimeSchema.ToDeFormat(_returnTable)

        For Each row As DataRow In _returnTable.Rows
            FE_RealTime.Fill(row, dsEdits, importEdit)
        Next


    End Sub

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

End Class
