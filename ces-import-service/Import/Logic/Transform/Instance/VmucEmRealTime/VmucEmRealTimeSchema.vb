﻿Public Class VmucEmRealTimeSchema


    Public Shared Function GetHeaders() As List(Of String)

        Dim headers As New List(Of String)
        headers.Add("Header1")
        headers.Add("Header2")
        headers.Add("Header3")
        headers.Add("Header4")
        headers.Add("Header5")
        headers.Add("Header6")
        headers.Add("Header7")
        headers.Add("Header8")
        headers.Add("Header9")
        headers.Add("Header10")
        headers.Add("Header11")
        headers.Add("Header12")
        headers.Add("Header13")
        headers.Add("Header14")
        headers.Add("Header15")
        headers.Add("Header16")
        headers.Add("Header17")
        headers.Add("Header18")
        headers.Add("Header19")
        headers.Add("Header20")
        headers.Add("Header21")
        headers.Add("Header22")
        headers.Add("Header23")
        headers.Add("Header24")
        headers.Add("Header26")
        headers.Add("Header27")
        headers.Add("Header28")
        headers.Add("Header29")
        headers.Add("Header30")
        headers.Add("Header31")
        headers.Add("Header32")
        headers.Add("Header33")
        headers.Add("Header34")
        headers.Add("Header35")
        headers.Add("Header36")
        headers.Add("Header37")
        headers.Add("Header38")
        headers.Add("Header39")
        headers.Add("Header40")
        headers.Add("Header41")
        headers.Add("Header42")
        headers.Add("Header43")
        headers.Add("Header44")
        headers.Add("Header45")
        headers.Add("Header46")
        headers.Add("Header47")
        headers.Add("Header48")
        headers.Add("Header49")
        headers.Add("Header50")
        headers.Add("Header51")
        headers.Add("Header52")
        headers.Add("Header53")
        headers.Add("Header54")
        headers.Add("Header55")
        headers.Add("Header56")

        Return headers

    End Function

    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable


        Dim realTimeDt As DataTable
        realTimeDt = DeFormatTableTemplates.CreateRealTimeTable()

        PopulateColumns(dt, realTimeDt)

        Return dt

    End Function


    Private Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable)


        Dim success As Boolean = False
        Dim dateHolder As DateTime

        For Each row As DataRow In dt.Rows

            If IsDate(row("Header8")) And row("Header1") = "AC" Then

                'theDate = Convert.ToDateTime(row("Header8").ToString.Substring(0, 10)).ToString("dd/MM/yyyy") + " " + row("Header8").ToString().Substring(11, 5)
                dateHolder = CDate(row("Header8"))

                For Each Rrow As DataRow In realTimeDt.Rows

                    If Rrow("MeterName") = row("Header4") And Rrow("DateTime") = dateHolder.ToString("dd/MM/yyyy HH:mm") Then

                        Rrow(dateHolder.ToString("HH:mm")) = row("Header9")
                        success = True

                    End If

                Next


                If Not success Then

                    Dim realTimeDtRow As DataRow = realTimeDt.NewRow

                    realTimeDtRow("MeterName") = row("Header4")
                    realTimeDtRow("DateTime") = dateHolder.ToString("dd/MM/yyyy HH:mm")
                    realTimeDtRow("CountUnits") = row("Header9")

                    realTimeDt.Rows.Add(realTimeDtRow)

                End If


                success = False

            End If
        Next

        dt = realTimeDt

    End Sub

End Class
