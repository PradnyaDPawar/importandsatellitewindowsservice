﻿Partial Public Class AnglianWater3HalfHourSchema

    Private Shared _meterSerial As String


    Public Shared Function GetHeaders() As List(Of String)
        Dim headers As New List(Of String)
        headers.Add("Header1")
        headers.Add("Header2")
        headers.Add("Header3")
        headers.Add("Header4")
        headers.Add("Header5")
        headers.Add("Header6")
        headers.Add("Header7")
        Return headers
    End Function

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)

        dt.Columns("Flow").ColumnName = "Consumption"
        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)

        dt = hhDataTable

    End Sub

    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim dateholder As DateTime
        Dim datetimeHolder As DateTime
        Dim success As Boolean = False
        For Each row As DataRow In dt.Rows
            If row(0).ToString.ToLower.Contains("serial number") Then
                _meterSerial = row(0).ToString.Split(":").ElementAt(1).Trim
            End If
            If IsDate(row("READ_DATE_TIME2")) Then
                dateholder = Convert.ToDateTime(row("READ_DATE_TIME2")).ToString("dd'/'MM'/'yyyy")

                datetimeHolder = Convert.ToDateTime(row("READ_DATE_TIME1")).ToString("HH:mm")
                datetimeHolder = dateholder.AddHours(datetimeHolder.Hour).AddMinutes(datetimeHolder.Minute)

                If datetimeHolder.Minute = 15 Or datetimeHolder.Minute = 45 Then
                    For Each hhrow As DataRow In hhDataTable.Rows
                        If hhrow("MeterSerial") = _meterSerial Then
                            If hhrow("Date(dd/mm/yyyy)") = dateholder.Date.ToString("dd'/'MM'/'yyyy") Then

                                Dim consumption1, consumption2 As Double
                                consumption1 = row("Consumption")
                                consumption2 = GetConsumptionByDateTime(datetimeHolder.AddMinutes(-15), dt, _meterSerial)
                                If consumption2 <> -1 Then
                                    hhrow(datetimeHolder.AddMinutes(-15).ToString("HH:mm")) = (consumption1 + consumption2).ToString("0.000")
                                Else
                                    hhrow(datetimeHolder.AddMinutes(-15).ToString("HH:mm")) = ""
                                End If

                                success = True
                            End If
                        End If
                    Next

                    If Not success Then
                        Dim newRow As DataRow = hhDataTable.NewRow
                        newRow("Date(dd/mm/yyyy)") = dateholder.Date.ToString("dd'/'MM'/'yyyy")
                        newRow("MeterSerial") = _meterSerial
                        newRow("ForcekWh") = "No"
                        Dim consumption1, consumption2 As Double
                        consumption1 = row("Consumption")
                        consumption2 = GetConsumptionByDateTime(datetimeHolder.AddMinutes(-15), dt, _meterSerial)
                        If consumption2 <> -1 Then
                            newRow(datetimeHolder.AddMinutes(-15).ToString("HH:mm")) = (consumption1 + consumption2).ToString("0.000")
                        Else
                            newRow(datetimeHolder.AddMinutes(-15).ToString("HH:mm")) = ""
                        End If

                        hhDataTable.Rows.Add(newRow)
                    End If
                End If
            End If

            success = False
        Next

        dt = hhDataTable

    End Sub


    Public Shared Function GetConsumptionByDateTime(ByVal timeToFind As DateTime, ByVal dt As DataTable, ByRef _meterSerial As String)

        Dim dateHolder As Date
        Dim timeHolder As DateTime
        Dim meterSerial As String
        For Each row As DataRow In dt.Rows
            meterSerial = row(0).ToString.Split(":").ElementAt(1).Trim
            If IsDate(row("READ_DATE_TIME2")) And meterSerial = _meterSerial Then

                dateHolder = row("READ_DATE_TIME2")
                timeHolder = row("READ_DATE_TIME1")
                If timeHolder.TimeOfDay = timeToFind.TimeOfDay And dateHolder.Date = timeToFind.Date Then
                    Return row("Consumption")
                End If
            End If
        Next
        Return -1
    End Function

End Class
