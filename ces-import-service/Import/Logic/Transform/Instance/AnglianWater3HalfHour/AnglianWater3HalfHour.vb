﻿Public Class AnglianWater3HalfHour
    Implements ITransformDelegate

    Private _returnTable As DataTable

    Public Property ReturnTable() As DataTable Implements ITransformDelegate.ReturnTable
        Get
            Return _returnTable
        End Get
        Set(ByVal value As DataTable)
            _returnTable = value
        End Set
    End Property

    Public Sub LoadData(ByVal fullPath As String) Implements ITransformDelegate.LoadData
        Dim csvFile As New DelimitedFile(fullPath, 7)
        csvFile.HasHeader = True
        csvFile.Load(_returnTable, AnglianWater3HalfHourSchema.GetHeaders)

    End Sub

    Public Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) Implements ITransformDelegate.ToDeFormat
        DataTableTools.RemoveTopRows(_returnTable, 3)
        DataTableTools.SetHeaders(_returnTable, 1)
        AnglianWater3HalfHourSchema.ToDeFormat(_returnTable)
        For Each row As DataRow In _returnTable.Rows
            FE_HalfHour.Fill(row, dsEdits, importEdit)
        Next

    End Sub
End Class
