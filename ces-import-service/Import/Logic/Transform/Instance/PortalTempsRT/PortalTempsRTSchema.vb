﻿Public Class PortalTempsRTSchema
    Public Shared Function ToDeFormat(ByRef dt As DataTable) As DataTable

        Dim MeterName As String = (dt.Rows(0)(0)).ToString().Split(":")(1)
        For index = 0 To 2
            If (dt.Rows.Count > 0) Then
                dt.Rows.RemoveAt(0)
            End If
        Next
        setColumnHeader(dt)
        Dim realTimeDt As DataTable
        realTimeDt = DeFormatTableTemplates.CreateRealTimeTable()
        PopulateColumns(dt, realTimeDt, MeterName)
        Return dt


    End Function
    Public Shared Sub setColumnHeader(ByRef dt As DataTable)

        dt.Columns("0").ColumnName = "DateTime"
        dt.Columns("3").ColumnName = "CountUnits"
        dt.Rows.RemoveAt(0)
    End Sub

    Private Shared Sub PopulateColumns(ByRef dt As DataTable, ByRef realTimeDt As DataTable, ByRef metername As String)


        Dim DateTimeNew As String = ""
        Dim CountUnitsNew As String = ""
        For Each row As DataRow In dt.Rows

            If IsDate(row("DateTime")) Then
                DateTimeNew = row("DateTime").ToString().Remove(row("DateTime").ToString().Length - 3).ToString()
                CountUnitsNew = row("CountUnits").ToString().Remove(row("CountUnits").ToString().Length - 2).ToString()
                    Dim realTimeDtRow As DataRow = realTimeDt.NewRow

                realTimeDtRow("MeterName") = metername
                realTimeDtRow("DateTime") = IIf(String.IsNullOrEmpty(CDate(DateTimeNew).ToString("dd/MM/yyyy H:mm")), "", CDate(DateTimeNew).ToString("dd/MM/yyyy H:mm"))
                realTimeDtRow("CountUnits") = IIf(CDbl(CountUnitsNew) = 0, 0, CountUnitsNew)


                    realTimeDt.Rows.Add(realTimeDtRow)
                End If




        Next


        dt = realTimeDt

    End Sub
End Class
