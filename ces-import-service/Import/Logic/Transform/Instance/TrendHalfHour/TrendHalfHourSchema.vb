﻿Imports System.Globalization
Public Class TrendHalfHourSchema

    Public Shared Sub ToDeFormat(ByRef dt As DataTable)
        RenameColumns(dt)
        dt.Columns.Add("Date")
        PopulateDate(dt)
        Merge15minutes(dt)
        Dim hhDataTable As DataTable = DeFormatTableTemplates.CreateHalfHourTable()
        CreateHalfHourlyEntries(dt, hhDataTable)
        PopulateColumns(dt)

    End Sub

    Private Shared Sub RenameColumns(ByRef dt As DataTable)
        dt.Columns(0).ColumnName = "Day"
        dt.Columns(1).ColumnName = "Month"
        dt.Columns(2).ColumnName = "Year"
        dt.Columns(3).ColumnName = "Time"
        dt.Columns(4).ColumnName = "Consumption"

    End Sub

    Private Shared Sub PopulateDate(ByRef dt As DataTable)
        For Each row As DataRow In dt.Rows
            Dim monthName = row("Month")
            Dim month As Integer = DateTime.ParseExact(monthName, "MMMM", CultureInfo.CurrentCulture).Month
            Dim day As Integer = row("Day")
            Dim year As Integer = row("Year")
            Dim dateHolder As New Date(year, month, day)
            row("Date") = dateHolder.ToShortDateString
        Next

    End Sub

    Private Shared Sub Merge15minutes(ByRef dt As DataTable)
        Dim consumption1, consumption2 As Double
        Dim timeEntry As Date
        Dim consumption As Double
        For Each row As DataRow In dt.Rows

            timeEntry = row("Time")

            If timeEntry.Minute = 0 Or timeEntry.Minute = 30 Then
                consumption1 = row("Consumption")

                Dim monthName = row("Month")
                Dim month As Integer = DateTime.ParseExact(monthName, "MMMM", CultureInfo.CurrentCulture).Month
                Dim day As Integer = row("Day")
                Dim year As Integer = row("Year")

                Dim dateEntry As New Date(year, month, day, timeEntry.Hour, timeEntry.Minute, timeEntry.Second)

                dateEntry = dateEntry.AddMinutes(-15)
                consumption2 = dt.Rows(GetRowIndex(dt, dateEntry))("Consumption")
                consumption = consumption1 + consumption2
                row("Consumption") = consumption.ToString
            End If
        Next
    End Sub

    Private Shared Function GetRowIndex(ByRef dt As DataTable, ByVal dateEntry As Date) As Integer
        Dim timeToCheck, dateToCheck As Date
        For Each row As DataRow In dt.Rows
            timeToCheck = row("Time")
            dateToCheck = row("Date")
            If dateEntry.TimeOfDay = timeToCheck.TimeOfDay And dateEntry.Date = dateToCheck Then
                Return dt.Rows.IndexOf(row)
            End If
        Next
        Return -1
    End Function


    Private Shared Sub CreateHalfHourlyEntries(ByRef dt As DataTable, ByRef hhDataTable As DataTable)
        Dim dateholder As DateTime
        Dim timeHolder As DateTime
        Dim success As Boolean = False

        For Each row As DataRow In dt.Rows

            If IsDate(row("Date")) Then
                timeHolder = row("Time")

                Dim timeHolderString = timeHolder.ToString("HH:mm")
                If hhDataTable.Columns.Contains(timeHolderString) Then

                    dateholder = row("Date")
                    For Each hhrow As DataRow In hhDataTable.Rows
                        If hhrow("Date (dd/mm/yyyy)") = dateholder.Date Then
                            hhrow(timeHolderString) = row("Consumption")
                            success = True
                        End If
                    Next

                    If Not success Then
                        Dim newRow As DataRow = hhDataTable.NewRow
                        newRow("Date (dd/mm/yyyy)") = dateholder.Date
                        newRow(timeHolderString) = row("Consumption")
                        hhDataTable.Rows.Add(newRow)
                    End If
                    success = False
                End If
            End If

        Next

        dt = hhDataTable

    End Sub

    Private Shared Sub PopulateColumns(ByRef dt As DataTable)
        For Each row As DataRow In dt.Rows
            row("Force kWh") = "Yes"
            'row("Meter Serial") = ""
        Next
    End Sub

    

End Class
