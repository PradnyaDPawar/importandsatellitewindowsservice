﻿Public Class TrendHalfHour
    Implements ITransformDelegate

    Public Function Invoke(ByVal stagingFilePath As String, ByRef returnTable As System.Data.DataTable) As Boolean Implements ITransformDelegate.Invoke

        Dim delimeter() As String = {" ", vbTab}
        Dim csvFile As New DelimitedFile(delimeter)
        csvFile.Load(stagingFilePath)
        returnTable = csvFile.ToDataTable(False)
        TrendHalfHourSchema.ToDeFormat(returnTable)

    End Function
End Class
