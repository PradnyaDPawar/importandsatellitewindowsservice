﻿Public Class TransfromConfig

    Public Shared Function GetTransformInterface(ByRef ImportEdit As ImportEdit) As ITransformDelegate

        If String.Compare(ImportEdit.Format, "Default", True) = 0 Then
            ImportEdit.Format = "digitalenergy"
        End If

        If String.Compare(ImportEdit.Format, "Default Edit", True) = 0 Then
            ImportEdit.Format = "digitalenergyedit"
        End If

        Dim iTransform As ITransformDelegate

        'Substring "Internal_"
        If ImportEdit.Format.ToLower.StartsWith("internal_") Then
            ImportEdit.Format = ImportEdit.Format.Substring(Len("internal_"))
        End If

        Dim importTypeJoinName As String = ImportEdit.Format & ImportEdit.DeType & ImportEdit.FileExtension

        'add new transform formats here
        Select Case importTypeJoinName.Replace(" ", "").ToLower

            'Case added for tariff
            Case "digitalenergytariff.xls", "digitalenergytariff.xlsx", "digitalenergytariff.csv"
                iTransform = New DigitalEnergyTariff

                'Case added for reading
            Case "digitalenergymeterreadings.xls", "digitalenergymeterreadings.xlsx", "digitalenergymeterreadings.csv"
                iTransform = New DigitalEnergyReading

            Case "digitalenergyadvancedbuilding.xls", "digitalenergyadvancedbuilding.csv", "digitalenergyadvancedasset.xls", "digitalenergyadvancedasset.csv"
                iTransform = New DigitalEnergyAdvancedBuilding
            Case "digitalenergyhalfhourlyconsumptions.xls", "digitalenergyhalfhourlyconsumptions.xlsx", "digitalenergyhalfhourlyconsumptions.csv"
                iTransform = New DigitalEnergyHalfHour
            Case "digitalenergyproject.xls", "digitalenergyproject.csv"
                iTransform = New DigitalEnergyProject
            Case "digitalenergyprojectmeter.xls", "digitalenergyprojectmeter.csv"
                iTransform = New DigitalEnergyProjectMeter
            Case "digitalenergyrealtime.csv", "utilitymonitoringrealtime.csv"
                iTransform = New UtilityMonitoringRealTime
                'this is for download realtime excel re-upload
            Case "digitalenergyrealtime.xls", "digitalenergyrealtime.xlsx"
                iTransform = New DigitalEnergyRealTime
                'above is digitalenergy format
            Case "digitalenergystaticvariable.xls", "digitalenergystaticvariable.xlsx"
                iTransform = New DigitalEnergyStaticVariable()
            Case "digitalenergysupplierstatement.xls", "digitalenergysupplierstatement.xlsx", "digitalenergysupplierstatement.xlsx"
                iTransform = New DigitalEnergySupplierStatement()

            Case "digitalenergyinvoice.xls", "digitalenergyinvoice.xlsx"
                iTransform = New DigitalEnergyInvoice()
            Case "digitalenergyclient.xls", "digitalenergyclient.xlsx", "digitalenergyclient.csv"
                iTransform = New DigitalEnergyClient()
            Case "digitalenergybuildings.xls", "digitalenergybuildings.csv", "defaultbuildingcsvbuildings.csv", "digitalenergybuildingcsvbuildings.csv", "digitalenergyasset.xls", "digitalenergyasset.csv", "defaultassetcsvasset.csv", "digitalenergyassetcsvasset.csv"
                iTransform = New DigitalEnergyBuilding
            Case "digitalenergymeters.xls"
                iTransform = New DigitalEnergyMeter
            Case "defaultmetercsvmeters.csv", "digitalenergymetercsvmeters.csv"
                iTransform = New DefaultMeterCSV()
            Case "digitalenergyenergydriver.xls"
                iTransform = New DigitalEnergyEnergyDriver()
            Case "digitalenergymeteredit.xls", "digitalenergymeteredit.xlsx", "digitalenergymeteredit.csv", "digitalenergyeditmeteredit.xls", "digitalenergyeditmeteredit.xlsx", "digitalenergyeditmeteredit.csv"
                iTransform = New DigitalEnergyMeterEdit()
            Case "digitalenergybuildingedit.xls", "digitalenergybuildingedit.xlsx", "digitalenergybuildingedit.csv", "digitalenergyeditassetedit.xls", "digitalenergyeditassetedit.xlsx", "digitalenergyeditassetedit.csv"
                iTransform = New DigitalEnergyBuildingEdit()
            Case "digitalenergyadvancedbuildingedit.xls", "digitalenergyadvancedbuildingedit.xlsx", "digitalenergyadvancedbuildingedit.csv", "digitalenergyeditadvancedassetedit.xls", "digitalenergyeditadvancedassetedit.xlsx", "digitalenergyeditadvancedassetedit.csv"
                iTransform = New DigitalEnergyAdvancedBuildingEdit()
            Case "digitalenergyoperatingschedule.xls", "digitalenergyoperatingschedule.xlsx"
                iTransform = New DigitalEnergyOperatingSchedule()

            Case "aslholdingshalfhourlyconsumptions.csv"
                iTransform = New ASLHoldingsHalfHour()

            Case "aslholdingsrealtime.csv"
                iTransform = New ASLHoldingsRealTime()

            Case "airtricityhalfhourlyconsumptions.xlsx", "airtricityhalfhourlyconsumptions.xls"
                iTransform = New AirtricityHalfHour()

            Case "anglianwaterhalfhourlyconsumptions.xls", "anglianwaterhalfhourlyconsumptions.xlsx"
                iTransform = New AngliaWaterXl()

            Case "anglianwaterhalfhourlyconsumptions.csv"
                iTransform = New AnglianWaterCsv()

            Case "anglianwater2halfhourlyconsumptions.xls", "anglianwater2halfhourlyconsumptions.xlsx"
                iTransform = New AnglianWater2HalfHour()

            Case "anglianwater3halfhourlyconsumptions.csv"
                iTransform = New AnglianWater3HalfHour()

            Case "autometerrealtime.csv"
                iTransform = New AutometerRealTime()

            Case "bcuschneiderelectrichalfhourlyconsumptions.csv"
                iTransform = New BcuSchneiderElectricHH()

            Case "bechalfhourlyconsumptions.xls", "bechalfhourlyconsumptions.xlsx"
                iTransform = New BECHalfHourly()

            Case "bglobalhalfhourlyconsumptions.csv"
                iTransform = New BglobalHalfHour()      ' Added On 20 Sept 2016 By Santosh Vishwakarma

            Case "bglobal2halfhourlyconsumptions.csv"
                iTransform = New Bglobal2HalfHour()

            Case "biffahalfhourlyconsumptions.csv"
                iTransform = New BiffaHalfHour()

            Case "biffa2halfhourlyconsumptions.csv"
                iTransform = New BiffaHalfHour2()

                'British Gas
            Case "britishgassiemensmeteringhalfhourlyconsumptions.csv"
                iTransform = New BritishGasSiemensHalfHour()

            Case "britishgassiemensmeteringhalfhourlyconsumptions.txt"
                iTransform = New BritishGasSiemensHalfHour()

            Case "britishgashalfhourlyconsumptions.xls", "britishgashalfhourlyconsumptions.xlsx"
                iTransform = New BritishGasHalfHour

            Case "britishgasamrhalfhourlyconsumptions.csv"
                iTransform = New BritishGasAMR()

            Case "britishgas3hhhalfhourlyconsumptions.csv"
                iTransform = New BritishGasHH3()

            Case "carlogavazzirealtime.csv"
                iTransform = New CarloGavazziRealTime()

            Case "carlogavazzi2realtime.csv"
                iTransform = New CarloGavazzi2RealTime()

            Case "carlogavazzi3realtime.csv"
                iTransform = New CarloGavazzi3RealTime()

                'Corona
            Case "coronahalfhourlyconsumptions.xlt", "coronahalfhourlyconsumptions.xls", "coronahalfhourlyconsumptions.xlsx"
                iTransform = New CoronaHalfHour()
            Case "coronahalfhourlyconsumptions.csv"
                iTransform = New CoronaHalfHourCsv()

                '    'cts
            Case "cts2halfhourlyconsumptions.csv"
                iTransform = New CtsHalfHour2()

            Case "cyclehalfhourlyconsumptions.csv"
                iTransform = New CycleHalfHour()

            Case "dixonshalfhourlyconsumptions.csv"
                iTransform = New DixonsHalfHour()

            Case "dataservehalfhourlyconsumptions.csv"
                iTransform = New DataServeHalfHour()

            Case "dataserve2halfhourlyconsumptions.csv"
                iTransform = New DataServe2HalfHour()

                'EDF
            Case "ecahalfhourlyconsumptions.csv"
                iTransform = New EcaHalfHour()

            Case "ecainvoice.xls"
                iTransform = New ECAInvoice()

            Case "edfhalfhourlyconsumptions.csv"
                iTransform = New EdfCsvHalfHour()
            Case "edfhalfhourlyconsumptions.xls", "edfhalfhourlyconsumptions.xlsx"
                iTransform = New EDFHalfHour()

            Case "edf2halfhourlyconsumptions.csv"
                iTransform = New Edf2HalfHour()

            Case "edf3halfhourlyconsumptions.csv"
                iTransform = New Edf3HalfHour()

            Case "edf4halfhourlyconsumptions.csv"
                iTransform = New Edf4HalfHour()

            Case "elcomponentrealtime.csv"
                iTransform = New ElComponentRealTime()

            Case "energyassethalfhourlyconsumptions.csv"
                iTransform = New EnergyAssetHalfHour()

            Case "energyasset2halfhourlyconsumptions.csv"
                iTransform = New EnergyAssetHalfHour2()

            Case "energyasset3halfhourlyconsumptions.csv"
                iTransform = New EnergyAssetHalfHour3()

            Case "energybrokershhinvoice.xls"
                iTransform = New EnergyBrokersHHnvoice()
            Case "energybrokersnhhinvoice.xls"
                iTransform = New EnergyBrokersNHHInvoice()
            Case "energybrokerswaterinvoice.xls"
                iTransform = New EnergyBrokersWaterInvoice()
            Case "energybrokersgasinvoice.xls"
                iTransform = New EnergyBrokersGasInvoice()
            Case "eonhalfhourlyconsumptions.csv"
                iTransform = New EonHalfHourCsv()

            Case "eonhalfhourlyconsumptions.xls", "eonhalfhourlyconsumptions.xlsx"
                iTransform = New EonHalfHourXls()

            Case "eon2halfhourlyconsumptions.xls", "eon2halfhourlyconsumptions.xlsx"
                iTransform = New EonFormat2HalfHour()

            Case "exoterichalfhourlyconsumptions.csv"
                iTransform = New ExotericHalfHour()

            Case "ezmeterrealtime.txt"
                iTransform = New EzMeterRealTime()

            Case "stark2halfhourlyconsumptions.csv"
                iTransform = New GatewayEnergyHH()

            Case "severntrenthalfhourlyconsumptions.csv"
                iTransform = New SevernTrentHalfHour()

            Case "severntrentinvoice.xls", "severntrentinvoice.xlsx"
                iTransform = New SevernTrentInvoice()

            Case "gazpromhalfhourlyconsumptions.csv"
                iTransform = New GazpromHalfHour()

            Case "greenlogicrealtime.csv"
                iTransform = New GreenLogicRealTime()

            Case "groundgasrealtime.csv"
                iTransform = New GroundGasRealtime()

                'Case "gazpromxmlhalfhourlyconsumptions.xml"
                '    transDel = New GazpromXmlHalfHour()

            Case "gdfhalfhourlyconsumptions.csv"
                iTransform = New GDFHalfHour()

            Case "gdfinvoice.xls", "gdfinvoice.xlsx"
                iTransform = New GDFInvoice()
                'Case "smartestenergyhalfhourlyconsumptions.csv"
                '    iTransform = New HavenHalfHourCsv()

            Case "havenpowerhalfhourlyconsumptions.xls", "havenpowerhalfhourlyconsumptions.xlsx", "havenpowerhalfhourlyconsumptions.xlsm"
                iTransform = New HavenHalfHour()

            Case "havenpowerhalfhourlyconsumptions.csv"
                iTransform = New HavenHalfHourCsv()

            Case "havenpower3halfhourlyconsumptions.csv"
                iTransform = New HavenHalfHour3()

                'Case "havenpower2halfhourlyconsumptions.csv"
                '    transDel = New HavenHalfHour2Csv()

                'Case "havenpower2halfhourlyconsumptions.xls"
                '    transDel = New HavenHalfHour2()

                'Case "havenpower2halfhourlyconsumptions.xlsx"
                '    transDel = New HavenHalfHour2()

            Case "imservhalfhourlyconsumptions.csv"
                iTransform = New ImServHH()

            Case "imserv2halfhourlyconsumptions.csv"
                iTransform = New ImServHalfHour2()

            Case "imserv3halfhourlyconsumptions.csv"
                iTransform = New ImServ3HalfHour()

            Case "imserv4halfhourlyconsumptions.csv"
                iTransform = New ImServ4HalfHour()

            Case "imserv5halfhourlyconsumptions.csv"
                iTransform = New ImServ5HalfHour()

            Case "imserv6halfhourlyconsumptions.csv"
                iTransform = New ImServ6HalfHour()

            Case "imserv7halfhourlyconsumptions.csv"
                iTransform = New ImServ7HalfHour()

            Case "imserv8halfhourlyconsumptions.xls"
                iTransform = New ImServ8HalfHour()

            Case "inencohalfhourlyconsumptions.xls", "inencohalfhourlyconsumptions.xlsx"
                iTransform = New InencoHalfHour()

            Case "npowerhalfhourlyconsumptions.csv"
                iTransform = New NpowerHalfHour()

                'Case "npower2halfhourlyconsumptions.xls"
                '    transDel = New NpowerHalfHour2()

                'Case "npower2halfhourlyconsumptions.xlsx"
                '    transDel = New NpowerHalfHour2()

                'Case "npower2halfhourlyconsumptions.csv"
                '    transDel = New NpowerHalfHour2Csv()

            Case "npower3halfhourlyconsumptions.csv"
                iTransform = New NpowerFormat3HH()

            Case "npower4halfhourlyconsumptions.csv"
                iTransform = New NpowerFormat4HH()

            Case "ntuhalfhourlyconsumptions.csv"
                iTransform = New NTUHalfHour()

            Case "nbbusconsumptionrealtime.csv"
                iTransform = New NBBusConsumptionChargerRealTime(True)

            Case "nbchargerconsumptionrealtime.csv"
                iTransform = New NBBusConsumptionChargerRealTime(False)

            Case "nbmileagestaticvariable.csv"
                iTransform = New NBMileageStaticVariable()

            Case "optimahalfhourlyconsumptions.xls"
                iTransform = New OptimaHalfHour()
            Case "optimagasinvoice.xls", "optimagasinvoice.xlsx"
                iTransform = New OptimaGasInvoice()
            Case "optimaelectricityinvoice.xls", "optimaelectricityinvoice.xlsx"
                iTransform = New OptimaElectricityInvoice()

            Case "portalrealtime.csv"
                iTransform = New PortalRealTime()

            Case "portaltempsrealtime.csv"
                iTransform = New PortalTempsRT()
            Case "portalkwhrealtime.csv"
                iTransform = New PortalkWhRealTime()

            Case "pulse24halfhourlyconsumptions.csv"
                iTransform = New Pulse24HH()

                'Case "schneiderelectrichalfhourlyconsumptions.xls", "schneiderelectrichalfhourlyconsumptions.xlsx"
                '    iTransform = New SchneiderHalfHour()

            Case "scottishsouthernhalfhourlyconsumptions.csv"
                iTransform = New ScottishSouthernHalfHour()

            Case "schneiderelectricrealtime.xls", "schneiderelectricrealtime.xlsx"
                iTransform = New SchneiderRealTime()

            Case "schneiderelectric2halfhourlyconsumptions.csv"
                iTransform = New SchneiderHalfHour2()

            Case "schneiderelectric3halfhourlyconsumptions.sigdat"
                iTransform = New Schneider3HalfHour()

            Case "schneiderelectric2realtime.xls", "schneiderelectric2realtime.xlsx"
                iTransform = New SchneiderRealTime2()

            Case "schneiderelectric3realtime.xls", "schneiderelectric3realtime.xlsx"
                iTransform = New SchneiderRealTime3()

            Case "schneiderelectric4realtime.xls", "schneiderelectric4realtime.xlsx"
                iTransform = New SchneiderRealTime4()

            Case "schneiderelectric5realtime.xls", "schneiderelectric5realtime.xlsx"
                iTransform = New SchneiderRealTime5()

            Case "schneiderelectric6realtime.csv"
                iTransform = New SchneiderRealTime6()

                'Case "sembcorphalfhourlyconsumptions.xls"
                '    transDel = New SembcorpHalfHour()

                'Case "sembcorphalfhourlyconsumptions.xlsx"
                '    transDel = New SembcorpHalfHour()
            Case "schutzrealtime.csv"
                iTransform = New SchutzRealTime()

            Case "sentinelhalfhourlyconsumptions.csv"
                iTransform = New SentinelHalfHour()

            Case "siemenscsvhalfhourlyconsumptions.csv"
                iTransform = New SiemensCsvHalfHour()

            Case "siemenshalfhourlyconsumptions.csv"
                iTransform = New SiemensHalfHour()

            Case "siemens2halfhourlyconsumptions.txt"
                iTransform = New SiemensHalfHour2()

            Case "siemens2halfhourlyconsumptions.csv"
                iTransform = New SiemensHalfHour2()

            Case "siemens3halfhourlyconsumptions.csv"
                iTransform = New Siemens3HalfHour()

            Case "siemens3bhalfhourlyconsumptions.csv"
                iTransform = New Siemens3bHalfHour()

            Case "siemens4halfhourlyconsumptions.csv", "siemens4halfhourlyconsumptions.xls", "siemens4halfhourlyconsumptions.xlsx"
                iTransform = New SiemensHalfHour4()

            Case "siemens5halfhourlyconsumptions.csv"
                iTransform = New Siemens5HalfHour()

            Case "smshalfhourlyconsumptions.csv"
                iTransform = New SmsHalfHour()

            Case "starkhalfhourlyconsumptions.csv", "starkhalfhourlyconsumptions.txt"
                iTransform = New StarkHalfHour

            Case "stark3halfhourlyconsumptions.csv"
                iTransform = New StarkFormat3HalfHour

            Case "stark4halfhourlyconsumptions.csv"
                iTransform = New StarkFormat4HalfHour()
            Case "stark5halfhourlyconsumptions.csv"
                iTransform = New StarkFormat5HalfHour()

            Case "stark6halfhourlyconsumptions.csv"
                iTransform = New StarkFormat6HalfHour()

            Case "stark7halfhourlyconsumptions.csv"
                iTransform = New StarkFormat7HalfHour()

            Case "stark8halfhourlyconsumptions.csv"
                iTransform = New StarkFormat8HalfHour()

            Case "schneiderelectricswanseaunihalfhourlyconsumptions.csv"
                iTransform = New SwanseaHalfHour()

            Case "southerncrossautomationhalfhourlyconsumptions.csv"
                iTransform = New SouthernCrossAutomationHH()

            Case "severntrenthalfhourlyconsumptions.csv"
                iTransform = New SevernTrentHalfHour()

            Case "severntrent2halfhourlyconsumptions.csv"
                iTransform = New SevernTrent2HalfHour()

            Case "technologrealtime.csv"
                iTransform = New TechnologRealTime()
                'Case "totalgasandpowerhalfhourlyconsumptions.csv"
                '    transDel = New TotalGasPowerHalfHour()
            Case "teamhalfhourlyconsumptions.csv"
                iTransform = New TeamHalfHour()

            Case "team2halfhourlyconsumptions.csv"
                iTransform = New Team2HalfHour()

            Case "teamsigmarealtime.csv"
                iTransform = New TeamSigmaRealTime()

            Case "teamsigmahalfhourlyconsumptions.csv"
                iTransform = New TeamSigmaHalfhour()

            Case "thameswaterrealtime.csv"
                iTransform = New ThamesWaterRealTime()

            Case "tmahalfhourlyconsumptions.csv"
                iTransform = New TmaHalfHour()

            Case "tma2halfhourlyconsumptions.csv"
                iTransform = New TmaHalfHour2()

            Case "tma3halfhourlyconsumptions.csv"
                iTransform = New Tma3HalfHour()

            Case "totalgasandpowerhalfhourlyconsumptions.csv"
                iTransform = New TotalGPHalfHour()

            Case "trendrealtime.csv"
                iTransform = New TrendRealtime()
            Case "trend2realtime.xls", "trend2realtime.csv", "trend2realtime.txt"
                iTransform = New Trend2RealTime()

            Case "totalcontrolrealtime.csv"
                iTransform = New TotalControlRealTime()

            Case "uplhalfhourlyconsumptions.csv"
                iTransform = New UplHalfHour()

            Case "vestigohalfhourlyconsumptions.csv"
                iTransform = New VestigoHalfHour()

            Case "veoliarealtime.csv"
                iTransform = New VeoliaRealTime()

            Case "vitalenergyhalfhourlyconsumptions.csv"
                iTransform = New VitalEnergyHalfHour()

            Case "vmucemrealtime.csv"
                iTransform = New VmucEmRealTime()

            Case "wwlrealtime.csv"
                iTransform = New WWLRealTime()

            Case "wi5realtime.txt"
                iTransform = New Wi5RealTime()

            Case "energrealtime.csv"
                iTransform = New EnergRealTime()

            Case "alfainvoice.xls", "alfainvoice.xlsx"
                iTransform = New AlfaInvoice()

            Case "britishgasinvoice.xls", "britishgasinvoice.xlsx"
                iTransform = New BGElectricInvoice()

            Case "britishgas2invoice.xls", "britishgas2invoice.xlsx"
                iTransform = New BGElectric2Invoice()

            Case "britishgas3invoice.xls", "britishgas3invoice.xlsx"
                iTransform = New BGElectric3Invoice()

            Case "britishgas4invoice.xls", "britishgas4invoice.xlsx"
                iTransform = New BGElectric4Invoice()

            Case "coronainvoice.xls", "coronainvoice.xlsx"
                iTransform = New CoronaInvoice()

            Case "corona2invoice.xls", "corona2invoice.xlsx"
                iTransform = New Corona2Invoice()

            Case "corona3invoice.xls", "corona3invoice.xlsx"
                iTransform = New CoronaInvoice3()

            Case "edfenergyebillinvoice.xls", "edfenergyebillinvoice.xlsx"
                iTransform = New EdfEnergyInvoice()

            Case "ediinvoice.xls", "ediinvoice.xlsx"
                iTransform = New EDInvoice()

            Case "eoninvoice.xls", "eoninvoice.xls"
                iTransform = New EonElectricityInvoice()

            Case "eon2invoice.csv"
                iTransform = New EonInvoice2()

            Case "gazpromediinvoice.csv"
                iTransform = New GazpromEdiInvoice()

            Case "gazprom2invoice.xls", "gazprom2invoice.xlsx"
                iTransform = New GazpromFormat2Invoice()

            Case "gazprom3invoice.csv"
                iTransform = New Gazprom3Invoice()

            Case "gazprominvoice.xls", "gazprominvoice.xlsx"
                iTransform = New GazpromInvoice()

            Case "gazprom4invoice.xls", "gazprom4invoice.xlsx"
                iTransform = New Gazprom4Invoice()

            Case "gazpromonlineservicesinvoice.xls", "gazpromonlineservicesinvoice.xlsx"
                iTransform = New GazpromOnlineServicesInvoice()

            Case "havenpowerinvoice.csv"
                iTransform = New HavenInvoice()

            Case "havenpower2invoice.xls", "havenpower2invoice.xlsx"
                iTransform = New Haven2Invoice()

            Case "edfdenbighshireinvoice.xls"
                iTransform = New EdfDenbighshireInvoice()

            Case "npowercsvinvoice.csv"
                iTransform = New NPowerInvoice()

            Case "npowerexcaliburinvoice.xls", "npowerexcaliburinvoice.xlsx"
                iTransform = New NPowerExcaliburInvoice()

            Case "npowerexcalibur2invoice.xls", "npowerexcalibur2invoice.xlsx"
                iTransform = New NPowerExcalibur2Invoice()

            Case "northumbrianinvoice.xls"
                iTransform = New NorthumbrianInvoice()

            Case "totalgasandpowerinvoice.xls", "totalgasandpowerinvoice.xlsx"
                iTransform = New TotalGpInvoice()

            Case "totalgasandpower2invoice.xls", "totalgasandpower2invoice.xlsx"
                iTransform = New TotalGpInvoice2()

            Case "scottishwaterinvoice.xls"
                iTransform = New ScottishWaterInvoiceDelimited()

            Case "sheffieldhallamwaterinvoice.xlsx", "sheffieldhallamwaterinvoice.xls"
                iTransform = New SheffieldHallamWaterInvoice()

            Case "scottishpowerinvoice.xls", "scottishpowerinvoice.xlsx"
                iTransform = New ScottishPowerInvoice()

            Case "sheffieldhallamelectricityinvoice.xls", "sheffieldhallamelectricityinvoice.xlsx"
                iTransform = New SheffieldHallamElectricityInvoice()

            Case "smartestenergyinvoice.xls", "smartestenergyinvoice.xlsx"
                iTransform = New SmartestEnergyCsvInvoice()

            Case "smartestenergycsvinvoice.csv"
                iTransform = New SmartestEnergyCsvInvoice()
            Case "ssecsvinvoice.csv"
                iTransform = New SseCsvInvoice()

            Case "ssecsv2invoice.csv"
                iTransform = New SseCsvInvoice2()

            Case "sseinvoice.xls", "sseinvoice.xlsx"
                iTransform = New SseInvoice()

            Case "sse3invoice.csv"
                iTransform = New SseInvoice3()

            Case "ssehalfhourlyconsumptions.xls", "ssehalfhourlyconsumptions.xlsx"
                iTransform = New SSEHalfHourly()

            Case "ssehydroinvoice.xls", "ssehydroinvoice.xlsx"
                iTransform = New SSEHydroInvoice()

            Case "systemlinkelectricityinvoice.csv"
                iTransform = New SystemLinkElectCsvInvoice()

            Case "systemlinkgasinvoice.csv"
                iTransform = New SystemLinkGasCsvInvoice()

            Case "systemlinkwaterinvoice.csv"
                iTransform = New SystemLinkWaterCsvInvoice()

            Case "systemlinkheatinvoice.csv"
                iTransform = New SystemLinkHeatCsvInvoice()

            Case "unitedutilitieswaterinvoice.xls", "unitedutilitieswaterinvoice.xlsx"
                iTransform = New UnitedUtilitiesWaterInvoice()

            Case "yorkshirewaterinvoice.xlsx", "yorkshirewaterinvoice.xls"
                iTransform = New YorkshireWaterInvoice()

            Case "compassinvoice.xls", "compassinvoice.xlsx"
                iTransform = New CompassInvoice()

            Case "sms2halfhourlyconsumptions.csv"
                iTransform = New SmsHalfHour2()

            Case "avonfireandrescuehalfhourlyconsumptions.csv"
                iTransform = New AvonAndRescueHH()

            Case "digitalenergyassetsummaryinformation.xls", "digitalenergyassetsummaryinformation.xlsx"
                iTransform = New AssetStatus()

            Case "digitalenergytenanthistory.xls", "digitalenergytenanthistory.xlsx"
                iTransform = New TenantHistory()

            Case "digitalenergyassetcode.xls", "digitalenergyassetcode.xlsx"
                iTransform = New AssetCode()
            Case Else
                iTransform = Nothing

        End Select

        Return iTransform

    End Function

    'Private Shared Sub GetFillEditsType(ByRef iTransform As ITransformDelegate, ByVal deTypeId As Integer)
    '    Select Case deTypeId
    '        Case DEImportTypeEnum.halfhourlyconsumptions
    '            iTransform.iFillEditsDelegate = New FE_HalfHour
    '        Case DEImportTypeEnum.realtime
    '            iTransform.iFillEditsDelegate = New FE_RealTime
    '    End Select
    'End Sub

End Class