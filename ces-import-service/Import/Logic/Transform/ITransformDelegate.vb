﻿Public Interface ITransformDelegate

#Region " Public Methods "

    Property ReturnTable() As DataTable

    Sub LoadData(ByVal fullPath As String)

    Sub ToDeFormat(ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)

#End Region

End Interface