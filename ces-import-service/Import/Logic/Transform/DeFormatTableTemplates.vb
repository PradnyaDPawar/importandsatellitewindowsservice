﻿Public Class DeFormatTableTemplates

    Public Shared Function CreateHalfHourTable() As DataTable
        Dim hhTable As New DataTable
        hhTable.Columns.Add("MPAN")
        hhTable.Columns.Add("MeterSerial")
        hhTable.Columns.Add("Date(dd/mm/yyyy)")
        hhTable.Columns.Add("ForcekWh")
        hhTable.Columns.Add("00:00")
        hhTable.Columns.Add("00:30")
        hhTable.Columns.Add("01:00")
        hhTable.Columns.Add("01:30")
        hhTable.Columns.Add("02:00")
        hhTable.Columns.Add("02:30")
        hhTable.Columns.Add("03:00")
        hhTable.Columns.Add("03:30")
        hhTable.Columns.Add("04:00")
        hhTable.Columns.Add("04:30")
        hhTable.Columns.Add("05:00")
        hhTable.Columns.Add("05:30")
        hhTable.Columns.Add("06:00")
        hhTable.Columns.Add("06:30")
        hhTable.Columns.Add("07:00")
        hhTable.Columns.Add("07:30")
        hhTable.Columns.Add("08:00")
        hhTable.Columns.Add("08:30")
        hhTable.Columns.Add("09:00")
        hhTable.Columns.Add("09:30")
        hhTable.Columns.Add("10:00")
        hhTable.Columns.Add("10:30")
        hhTable.Columns.Add("11:00")
        hhTable.Columns.Add("11:30")
        hhTable.Columns.Add("12:00")
        hhTable.Columns.Add("12:30")
        hhTable.Columns.Add("13:00")
        hhTable.Columns.Add("13:30")
        hhTable.Columns.Add("14:00")
        hhTable.Columns.Add("14:30")
        hhTable.Columns.Add("15:00")
        hhTable.Columns.Add("15:30")
        hhTable.Columns.Add("16:00")
        hhTable.Columns.Add("16:30")
        hhTable.Columns.Add("17:00")
        hhTable.Columns.Add("17:30")
        hhTable.Columns.Add("18:00")
        hhTable.Columns.Add("18:30")
        hhTable.Columns.Add("19:00")
        hhTable.Columns.Add("19:30")
        hhTable.Columns.Add("20:00")
        hhTable.Columns.Add("20:30")
        hhTable.Columns.Add("21:00")
        hhTable.Columns.Add("21:30")
        hhTable.Columns.Add("22:00")
        hhTable.Columns.Add("22:30")
        hhTable.Columns.Add("23:00")
        hhTable.Columns.Add("23:30")
        Return hhTable
    End Function

    Public Shared Function CreateInvoiceTable() As DataTable

        Dim deInvoiceTable As New DataTable

        deInvoiceTable.Columns.Add("Invoice Number", GetType(String))
        deInvoiceTable.Columns.Add("Meter Serial", GetType(String))
        deInvoiceTable.Columns.Add("MPAN", GetType(String))
        deInvoiceTable.Columns.Add("Rate", GetType(String))
        deInvoiceTable.Columns.Add("Start Date", GetType(String))
        deInvoiceTable.Columns.Add("End Date", GetType(String))
        deInvoiceTable.Columns.Add("Start Read", GetType(String))
        deInvoiceTable.Columns.Add("Start Read Estimated", GetType(String))
        deInvoiceTable.Columns.Add("End Read", GetType(String))
        deInvoiceTable.Columns.Add("End Read Estimated", GetType(String))
        deInvoiceTable.Columns.Add("Consumption", GetType(String))
        deInvoiceTable.Columns.Add("Force kWh", GetType(String))
        deInvoiceTable.Columns.Add("InvoiceRate", GetType(String))
        deInvoiceTable.Columns.Add("CostUnit", GetType(String))
        deInvoiceTable.Columns.Add("Net", GetType(String))
        deInvoiceTable.Columns.Add("Vat", GetType(String))
        deInvoiceTable.Columns.Add("Gross", GetType(String))
        deInvoiceTable.Columns.Add("Cost Only", GetType(String))
        deInvoiceTable.Columns.Add("Description", GetType(String))
        deInvoiceTable.Columns.Add("TaxPointDate", GetType(String))
        deInvoiceTable.Columns.Add("IsLastDayApportion", GetType(String))
        deInvoiceTable.Columns.Add("IsTransmissionTariff", GetType(String))




        Return deInvoiceTable
    End Function


    Public Shared Function CreateRealTimeTable() As DataTable

        Dim deRealTimeTable As New DataTable

        deRealTimeTable.Columns.Add("MeterName")
        deRealTimeTable.Columns.Add("DateTime")
        deRealTimeTable.Columns.Add("CountUnits")

        Return deRealTimeTable

    End Function

    Public Shared Function CreateEnergyDriverTable() As DataTable
        Dim edTable As New DataTable
        'edTable.Columns.Add("EDRN")
        edTable.Columns.Add("EnergyDriverReference")
        'edTable.Columns.Add("UPRN")
        edTable.Columns.Add("Date")
        edTable.Columns.Add("00:00")
        edTable.Columns.Add("00:30")
        edTable.Columns.Add("01:00")
        edTable.Columns.Add("01:30")
        edTable.Columns.Add("02:00")
        edTable.Columns.Add("02:30")
        edTable.Columns.Add("03:00")
        edTable.Columns.Add("03:30")
        edTable.Columns.Add("04:00")
        edTable.Columns.Add("04:30")
        edTable.Columns.Add("05:00")
        edTable.Columns.Add("05:30")
        edTable.Columns.Add("06:00")
        edTable.Columns.Add("06:30")
        edTable.Columns.Add("07:00")
        edTable.Columns.Add("07:30")
        edTable.Columns.Add("08:00")
        edTable.Columns.Add("08:30")
        edTable.Columns.Add("09:00")
        edTable.Columns.Add("09:30")
        edTable.Columns.Add("10:00")
        edTable.Columns.Add("10:30")
        edTable.Columns.Add("11:00")
        edTable.Columns.Add("11:30")
        edTable.Columns.Add("12:00")
        edTable.Columns.Add("12:30")
        edTable.Columns.Add("13:00")
        edTable.Columns.Add("13:30")
        edTable.Columns.Add("14:00")
        edTable.Columns.Add("14:30")
        edTable.Columns.Add("15:00")
        edTable.Columns.Add("15:30")
        edTable.Columns.Add("16:00")
        edTable.Columns.Add("16:30")
        edTable.Columns.Add("17:00")
        edTable.Columns.Add("17:30")
        edTable.Columns.Add("18:00")
        edTable.Columns.Add("18:30")
        edTable.Columns.Add("19:00")
        edTable.Columns.Add("19:30")
        edTable.Columns.Add("20:00")
        edTable.Columns.Add("20:30")
        edTable.Columns.Add("21:00")
        edTable.Columns.Add("21:30")
        edTable.Columns.Add("22:00")
        edTable.Columns.Add("22:30")
        edTable.Columns.Add("23:00")
        edTable.Columns.Add("23:30")
        Return edTable
    End Function

    Public Shared Function CreateSubMeterTable() As DataTable
        Dim edTable As New DataTable
        edTable.Columns.Add("BuildingUPRN")
        edTable.Columns.Add("MeterName")
        edTable.Columns.Add("SerialNumber")
        edTable.Columns.Add("Units")
        edTable.Columns.Add("ReportingMeter")
        edTable.Columns.Add("OperatingSchedule1")
        edTable.Columns.Add("OperatingSchedule2")
        edTable.Columns.Add("Location")
        edTable.Columns.Add("Description")
        edTable.Columns.Add("CorrectionFactor")
        edTable.Columns.Add("ConversionFactor")
        edTable.Columns.Add("CustomMultiplier")
        edTable.Columns.Add("ValidateReadings")
        edTable.Columns.Add("ReadingWrapRound")
        edTable.Columns.Add("MaxReadConsumption")
        edTable.Columns.Add("RealTime")
        edTable.Columns.Add("WrapRoundValue")
        edTable.Columns.Add("IsCumulative")
        edTable.Columns.Add("IsRaw")
        edTable.Columns.Add("MinEntry")
        edTable.Columns.Add("MaxEntry")
        edTable.Columns.Add("Multiplier")
        edTable.Columns.Add("SerialAlias")
        edTable.Columns.Add("ParentMeterSerial")

        Return edTable
    End Function

    Public Shared Function CreateMeterTable(ByVal ImportVersion As String) As DataTable
        Dim edTable As New DataTable
        edTable.Columns.Add("BuildingUPRN")
        edTable.Columns.Add("MeterName")
        edTable.Columns.Add("SerialNumber")
        edTable.Columns.Add("MPAN")
        edTable.Columns.Add("AccountNumber")
        edTable.Columns.Add("MeterFuel")
        edTable.Columns.Add("Units")
        edTable.Columns.Add("AMR")
        edTable.Columns.Add("AMRInstalledDate")
        edTable.Columns.Add("IsActive")
        edTable.Columns.Add("CreatekWhExportAssociateMeter")
        edTable.Columns.Add("CreatekVarhExportAssociateMeter")
        edTable.Columns.Add("CreatekVarhAssociateMeter")
        edTable.Columns.Add("ExportMPAN")
        edTable.Columns.Add("ReportingMeter")
        edTable.Columns.Add("CRCMeterType")
        edTable.Columns.Add("CRCFuelType")
        edTable.Columns.Add("IncludeAsCRCResidual")
        edTable.Columns.Add("CarbonTrustStandard")
        edTable.Columns.Add("CTSStartDate")
        edTable.Columns.Add("OperatingSchedule1")
        edTable.Columns.Add("OperatingSchedule2")
        edTable.Columns.Add("Consumption Source")
        edTable.Columns.Add("CRC Consumption Source")
        edTable.Columns.Add("AuthorisedCapacity")
        edTable.Columns.Add("ExcessCapacity")
        edTable.Columns.Add("CalorificValue")
        edTable.Columns.Add("DateInstalled")
        edTable.Columns.Add("DateCommissioned")
        edTable.Columns.Add("IsStockMeter")
        edTable.Columns.Add("TankSize")
        edTable.Columns.Add("InvoiceFrequency")
        edTable.Columns.Add("TimeZone")
        edTable.Columns.Add("Category")
        edTable.Columns.Add("Status")
        edTable.Columns.Add("MeterOperator")
        edTable.Columns.Add("MeterOperatorContact")
        edTable.Columns.Add("Location")
        edTable.Columns.Add("Description")
        edTable.Columns.Add("CorrectionFactor")
        edTable.Columns.Add("ConversionFactor")
        edTable.Columns.Add("CustomMultiplier")
        edTable.Columns.Add("ValidateReadings")
        edTable.Columns.Add("ReadingWrapRound")
        edTable.Columns.Add("MaxReadConsumption")
        edTable.Columns.Add("RealTime")
        edTable.Columns.Add("WrapRoundValue")
        edTable.Columns.Add("IsCumulative")
        edTable.Columns.Add("IsRaw")
        edTable.Columns.Add("MinEntry")
        edTable.Columns.Add("MaxEntry")
        edTable.Columns.Add("Multiplier")
        edTable.Columns.Add("SerialAlias")
        edTable.Columns.Add("EnableDayNightActivePeriod")
        edTable.Columns.Add("DayPeriod")
        edTable.Columns.Add("NightPeriod")

        If ImportVersion <> "Vestigo" Then
            edTable.Columns.Add("DataCollector")
            edTable.Columns.Add("ESOSComplianceMethod")
        End If

        Return edTable
    End Function
    Public Shared Function CreateSubMeterEditTable(ByVal ImportVersion As String) As DataTable
        Dim edTable As New DataTable
        edTable.Columns.Add("Client")
        edTable.Columns.Add("Meter Fuel")
        edTable.Columns.Add("Current Serial Number")
        edTable.Columns.Add("New Serial Number")

        If ImportVersion = "Vestigo" Then
            edTable.Columns.Add("BuildingUPRN")
        Else
            edTable.Columns.Add("Asset UPRN")
        End If
        edTable.Columns.Add("Meter Name")
        edTable.Columns.Add("Units")
        edTable.Columns.Add("Reporting Meter")
        'edTable.Columns.Add("AccountNumber")
        edTable.Columns.Add("Is Active")
        edTable.Columns.Add("Meter Deactivation Date")
        'edTable.Columns.Add("InvoiceFrequency")
        edTable.Columns.Add("Operating Schedule 1")
        edTable.Columns.Add("Operating Schedule 2")
        edTable.Columns.Add("Location")
        edTable.Columns.Add("Description")
        edTable.Columns.Add("Correction Factor")
        edTable.Columns.Add("Conversion Factor")
        edTable.Columns.Add("Custom Multiplier")
        edTable.Columns.Add("Validate Readings")
        edTable.Columns.Add("Reading Wrap Round")
        edTable.Columns.Add("Max Read Consumption")
        edTable.Columns.Add("Real Time")
        edTable.Columns.Add("Wrap Round Value")
        edTable.Columns.Add("Is Cumulative")
        edTable.Columns.Add("Is Raw")
        edTable.Columns.Add("Min Entry")
        edTable.Columns.Add("Max Entry")
        edTable.Columns.Add("Multiplier")
        edTable.Columns.Add("Serial Alias")
        edTable.Columns.Add("Consumption Source")
        edTable.Columns.Add("Parent Meter Serial")
        edTable.Columns.Add("Create kWhExport Associate Meter")
        edTable.Columns.Add("Create kVarhExport Associate Meter")
        edTable.Columns.Add("Create kVarh AssociateMeter")
        edTable.Columns.Add("Export MPAN")
        'edTable.Columns.Add("AuthorisedCapacity")
        'edTable.Columns.Add("ExcessCapacity")

        Return edTable

    End Function

    Public Shared Function CreateMeterEditTable(ByVal ImportVersion As String) As DataTable
        Dim edTable As New DataTable
        edTable.Columns.Add("Client")
        edTable.Columns.Add("Meter Fuel")
        edTable.Columns.Add("Current Serial Number")
        edTable.Columns.Add("New Serial Number")

        If ImportVersion = "Vestigo" Then
            edTable.Columns.Add("BuildingUPRN")
        Else
            edTable.Columns.Add("Asset UPRN")
        End If

        edTable.Columns.Add("Meter Name")
        edTable.Columns.Add("MPAN")
        edTable.Columns.Add("Account Number")
        edTable.Columns.Add("Units")
        edTable.Columns.Add("AMR")
        edTable.Columns.Add("AMR Installed Date")
        edTable.Columns.Add("Is Active")
        edTable.Columns.Add("Meter Deactivation Date")
        edTable.Columns.Add("Create kWhExport Associate Meter")
        edTable.Columns.Add("Create kVarhExport Associate Meter")
        edTable.Columns.Add("Create kVarh Associate Meter")
        edTable.Columns.Add("Export MPAN")
        edTable.Columns.Add("Reporting Meter")
        edTable.Columns.Add("CRC Meter Type")
        edTable.Columns.Add("CRC Fuel Type")
        edTable.Columns.Add("Include As CRC Residual")
        edTable.Columns.Add("Carbon Trust Standard")
        edTable.Columns.Add("CTS Start Date")
        edTable.Columns.Add("Operating Schedule 1")
        edTable.Columns.Add("Operating Schedule 2")
        edTable.Columns.Add("Authorised Capacity")
        edTable.Columns.Add("Excess Capacity")
        edTable.Columns.Add("Calorific Value")
        edTable.Columns.Add("Date Installed")
        edTable.Columns.Add("Date Commissioned")
        edTable.Columns.Add("Is Stock Meter")
        edTable.Columns.Add("Invoice Frequency")



        edTable.Columns.Add("Location")
        edTable.Columns.Add("Description")
        edTable.Columns.Add("Correction Factor")
        edTable.Columns.Add("Conversion Factor")
        edTable.Columns.Add("Custom Multiplier")
        edTable.Columns.Add("Validate Readings")
        edTable.Columns.Add("Reading Wrap Round")
        edTable.Columns.Add("Max Read Consumption")
        edTable.Columns.Add("Real Time")
        edTable.Columns.Add("Wrap Round Value")
        edTable.Columns.Add("Is Cumulative")
        edTable.Columns.Add("IsRaw")
        edTable.Columns.Add("Min Entry")
        edTable.Columns.Add("Max Entry")
        edTable.Columns.Add("Multiplier")
        edTable.Columns.Add("Serial Alias")
        edTable.Columns.Add("Consumption Source")
        edTable.Columns.Add("CRC Consumption Source")
        edTable.Columns.Add("Tank Size")
        edTable.Columns.Add("Category")
        edTable.Columns.Add("Status")
        edTable.Columns.Add("Meter Operator")
        edTable.Columns.Add("Meter Operator Contact")

        If ImportVersion <> "Vestigo" Then
            edTable.Columns.Add("Data Collector")
            edTable.Columns.Add("ESOS Compliance Method")
        Else
            edTable.Columns.Add("TimeZone")
            edTable.Columns.Add("EnableDayNightActivePeriod")
            edTable.Columns.Add("DayPeriod")
            edTable.Columns.Add("NightPeriod")
        End If

        Return edTable
    End Function

    Public Shared Function CreateBuildingImportTable() As DataTable
        Dim edTable As New DataTable
        edTable.Columns.Add("Name")
        edTable.Columns.Add("Address1")
        edTable.Columns.Add("Address2")
        edTable.Columns.Add("Address3")
        edTable.Columns.Add("Address4")
        edTable.Columns.Add("Town")
        edTable.Columns.Add("PostCode")
        edTable.Columns.Add("UPRN")
        edTable.Columns.Add("CRCUndertaking")
        edTable.Columns.Add("Occupier")
        edTable.Columns.Add("Client")
        edTable.Columns.Add("Group")
        edTable.Columns.Add("UndertakingId")
        edTable.Columns.Add("GroupId")

        Return edTable
    End Function
    Public Shared Function CreateBuildingEditImportTable() As DataTable
        Dim edTable As New DataTable
        edTable.Columns.Add("Name")
        edTable.Columns.Add("Address 1")
        edTable.Columns.Add("Address 2")
        edTable.Columns.Add("Address 3")
        edTable.Columns.Add("Address 4")
        edTable.Columns.Add("Town")
        edTable.Columns.Add("Post Code")
        edTable.Columns.Add("Current UPRN")
        edTable.Columns.Add("New UPRN")
        edTable.Columns.Add("Parent Organisation")
        edTable.Columns.Add("Occupier")
        edTable.Columns.Add("Client")
        edTable.Columns.Add("Group")


        Return edTable
    End Function
    Public Shared Function CreateAdvancedBuildingEditTable(ByVal ImportVersion As String) As DataTable
        Dim edTable As New DataTable
        edTable.Columns.Add("UPRN")
        If ImportVersion = "Vestigo" Then
            edTable.Columns.Add("BuildingOwner")
        Else
            edTable.Columns.Add("AssetOwner")
        End If
        edTable.Columns.Add("Tenant")
        edTable.Columns.Add("FacilitiesManager")
        edTable.Columns.Add("EnergyChampion")
        edTable.Columns.Add("MainSectorBenchmark")
        edTable.Columns.Add("SubSectorBenchmark")
        edTable.Columns.Add("TotalFloorArea")
        edTable.Columns.Add("NumberOfOccupants")
        edTable.Columns.Add("NumberOfPupils")
        edTable.Columns.Add("AnnualHoursOfOccupancy")
        edTable.Columns.Add("MainHeatingSystem")
        edTable.Columns.Add("MainHeatingFuel")
        edTable.Columns.Add("HeatingSetpoint")
        edTable.Columns.Add("CoolingSetpoint")
        edTable.Columns.Add("PropertyRef")
        edTable.Columns.Add("BuiltDate")
        edTable.Columns.Add("Subdivision")
        edTable.Columns.Add("EPCGrade")
        edTable.Columns.Add("EPCRating")
        edTable.Columns.Add("EPCOther")
        edTable.Columns.Add("Notes")
        edTable.Columns.Add("RateableValue")
        edTable.Columns.Add("NetInternalArea")
        edTable.Columns.Add("Category")
        edTable.Columns.Add("PropertyManager")
        edTable.Columns.Add("PropertySurveyor")
        edTable.Columns.Add("Administrator")
        If ImportVersion = "Vestigo" Then
            edTable.Columns.Add("BuildingCategory")
        Else
            edTable.Columns.Add("AssetCategory")
        End If
        edTable.Columns.Add("BudgetCode")
        edTable.Columns.Add("Division")
        edTable.Columns.Add("PropertyType")
        edTable.Columns.Add("Service")
        Return edTable
    End Function


    'Function for tariff import
    Public Shared Function CreateTariffImportTable() As DataTable
        Dim edTable As New DataTable
        edTable.Columns.Add("Meter Serial")
        edTable.Columns.Add("MPAN")
        edTable.Columns.Add("Tariff Name")
        edTable.Columns.Add("Contract Name")
        edTable.Columns.Add("Price")
        edTable.Columns.Add("Vat(%)")
        Return edTable
    End Function
End Class
