﻿Public Class FE_Tariff

    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)
        Dim rtEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Dim deconfig As DeConfig = New DeConfig()
        Dim ImportVersion As String = deconfig.GetConfig().ImportVersion

        Try

            rtEdit.ImportId = importEdit.Id
            rtEdit.StatusId = StatusEnum.In_Queue
            rtEdit.Database = importEdit.DatabaseName

            rtEdit.MeterSerial = IIf(row("MeterSerial") Is DBNull.Value OrElse row("MeterSerial") = Nothing, DBNull.Value.ToString(), row("MeterSerial"))
            rtEdit.TariffMpan = IIf(row("MPAN") Is DBNull.Value OrElse row("MPAN") = Nothing, DBNull.Value.ToString(), row("MPAN"))
            rtEdit.ContractName = IIf(row("ContractName") Is DBNull.Value OrElse row("ContractName") = Nothing, DBNull.Value.ToString(), row("ContractName"))
            rtEdit.TariffName = IIf(row("TariffName") Is DBNull.Value OrElse row("TariffName") = Nothing, DBNull.Value.ToString(), row("TariffName"))
            rtEdit.Price = IIf(row("Price") Is DBNull.Value OrElse row("Price") = Nothing, DBNull.Value.ToString(), row("Price"))
            rtEdit.TariffVat = IIf(row("VAT") Is DBNull.Value OrElse row("VAT") = Nothing, DBNull.Value.ToString(), row("VAT"))

        Catch ex As Exception

            rtEdit.StatusId = StatusEnum.Fail
            rtEdit.Message = "Fail to import data (fill meter edit)."

        End Try
    End Sub


End Class
