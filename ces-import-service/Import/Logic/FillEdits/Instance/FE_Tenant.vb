﻿Public Class FE_Tenant
    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)
        Dim rtEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Dim deconfig As DeConfig = New DeConfig()
        Dim ImportVersion As String = deconfig.GetConfig().ImportVersion



        rtEdit.ImportId = importEdit.Id
        rtEdit.StatusId = StatusEnum.In_Queue
        rtEdit.Database = importEdit.DatabaseName

        rtEdit.TenantName = IIf(row("tenantname") Is DBNull.Value OrElse row("tenantname") = Nothing, DBNull.Value.ToString(), row("tenantname"))
        rtEdit.TenantStartDate = IIf(row("startdate") Is DBNull.Value OrElse row("startdate") = Nothing, DBNull.Value.ToString(), row("startdate"))
        rtEdit.TenantEndDate = IIf(row("enddate") Is DBNull.Value OrElse row("enddate") = Nothing, DBNull.Value.ToString(), row("enddate"))
        rtEdit.TenantContact = IIf(row("contact") Is DBNull.Value OrElse row("contact") = Nothing, DBNull.Value.ToString(), row("contact"))
        rtEdit.TenantUprn = IIf(row("uprn") Is DBNull.Value OrElse row("uprn") = Nothing, DBNull.Value.ToString(), row("uprn"))

    End Sub
End Class
