﻿Public Class FE_OperatingSchedule

    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)

        Dim osEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Try

            osEdit.ImportId = importEdit.Id
            osEdit.StatusId = StatusEnum.In_Queue
            osEdit.Database = importEdit.DatabaseName

            osEdit.OS_Name = IIf(row("name") Is DBNull.Value OrElse row("name") = Nothing OrElse String.IsNullOrEmpty(row("name").ToString().Trim()),
                                 DBNull.Value,
                                 row("name"))

            Dim osDate As String = String.Empty

            If IsDate(row("date")) Then
                osDate = CDate(row("date").ToString().Trim()).ToString("yyyy-MM-dd")
            Else
                osDate = row("date")
            End If

            osEdit.OS_Date = IIf(row("date") Is DBNull.Value OrElse row("date") = Nothing OrElse String.IsNullOrEmpty(row("date").ToString().Trim()),
                                 DBNull.Value,
                                 osDate)

            osEdit.OS_DefaultForFuel = IIf(row("default for fuel") Is DBNull.Value OrElse row("default for fuel") = Nothing OrElse String.IsNullOrEmpty(row("default for fuel").ToString().Trim()),
                                         DBNull.Value,
                                         row("default for fuel"))

            'Removes commas and only selects the first 20 characters as that is the maximum length accepted in DB for HH slots

            osEdit.hh0000 = IIf(row("00:00") Is DBNull.Value OrElse row("00:00") = Nothing, DBNull.Value, Left(row("00:00").ToString.Replace(",", ""), 20))
            osEdit.hh0030 = IIf(row("00:30") Is DBNull.Value OrElse row("00:30") = Nothing, DBNull.Value, Left(row("00:30").ToString.Replace(",", ""), 20))
            osEdit.hh0100 = IIf(row("01:00") Is DBNull.Value OrElse row("01:00") = Nothing, DBNull.Value, Left(row("01:00").ToString.Replace(",", ""), 20))
            osEdit.hh0130 = IIf(row("01:30") Is DBNull.Value OrElse row("01:30") = Nothing, DBNull.Value, Left(row("01:30").ToString.Replace(",", ""), 20))
            osEdit.hh0200 = IIf(row("02:00") Is DBNull.Value OrElse row("02:00") = Nothing, DBNull.Value, Left(row("02:00").ToString.Replace(",", ""), 20))
            osEdit.hh0230 = IIf(row("02:30") Is DBNull.Value OrElse row("02:30") = Nothing, DBNull.Value, Left(row("02:30").ToString.Replace(",", ""), 20))
            osEdit.hh0300 = IIf(row("03:00") Is DBNull.Value OrElse row("03:00") = Nothing, DBNull.Value, Left(row("03:00").ToString.Replace(",", ""), 20))
            osEdit.hh0330 = IIf(row("03:30") Is DBNull.Value OrElse row("03:30") = Nothing, DBNull.Value, Left(row("03:30").ToString.Replace(",", ""), 20))
            osEdit.hh0400 = IIf(row("04:00") Is DBNull.Value OrElse row("04:00") = Nothing, DBNull.Value, Left(row("04:00").ToString.Replace(",", ""), 20))
            osEdit.hh0430 = IIf(row("04:30") Is DBNull.Value OrElse row("04:30") = Nothing, DBNull.Value, Left(row("04:30").ToString.Replace(",", ""), 20))
            osEdit.hh0500 = IIf(row("05:00") Is DBNull.Value OrElse row("05:00") = Nothing, DBNull.Value, Left(row("05:00").ToString.Replace(",", ""), 20))
            osEdit.hh0530 = IIf(row("05:30") Is DBNull.Value OrElse row("05:30") = Nothing, DBNull.Value, Left(row("05:30").ToString.Replace(",", ""), 20))
            osEdit.hh0600 = IIf(row("06:00") Is DBNull.Value OrElse row("06:00") = Nothing, DBNull.Value, Left(row("06:00").ToString.Replace(",", ""), 20))
            osEdit.hh0630 = IIf(row("06:30") Is DBNull.Value OrElse row("06:30") = Nothing, DBNull.Value, Left(row("06:30").ToString.Replace(",", ""), 20))
            osEdit.hh0700 = IIf(row("07:00") Is DBNull.Value OrElse row("07:00") = Nothing, DBNull.Value, Left(row("07:00").ToString.Replace(",", ""), 20))
            osEdit.hh0730 = IIf(row("07:30") Is DBNull.Value OrElse row("07:30") = Nothing, DBNull.Value, Left(row("07:30").ToString.Replace(",", ""), 20))
            osEdit.hh0800 = IIf(row("08:00") Is DBNull.Value OrElse row("08:00") = Nothing, DBNull.Value, Left(row("08:00").ToString.Replace(",", ""), 20))
            osEdit.hh0830 = IIf(row("08:30") Is DBNull.Value OrElse row("08:30") = Nothing, DBNull.Value, Left(row("08:30").ToString.Replace(",", ""), 20))
            osEdit.hh0900 = IIf(row("09:00") Is DBNull.Value OrElse row("09:00") = Nothing, DBNull.Value, Left(row("09:00").ToString.Replace(",", ""), 20))
            osEdit.hh0930 = IIf(row("09:30") Is DBNull.Value OrElse row("09:30") = Nothing, DBNull.Value, Left(row("09:30").ToString.Replace(",", ""), 20))
            osEdit.hh1000 = IIf(row("10:00") Is DBNull.Value OrElse row("10:00") = Nothing, DBNull.Value, Left(row("10:00").ToString.Replace(",", ""), 20))
            osEdit.hh1030 = IIf(row("10:30") Is DBNull.Value OrElse row("10:30") = Nothing, DBNull.Value, Left(row("10:30").ToString.Replace(",", ""), 20))
            osEdit.hh1100 = IIf(row("11:00") Is DBNull.Value OrElse row("11:00") = Nothing, DBNull.Value, Left(row("11:00").ToString.Replace(",", ""), 20))
            osEdit.hh1130 = IIf(row("11:30") Is DBNull.Value OrElse row("11:30") = Nothing, DBNull.Value, Left(row("11:30").ToString.Replace(",", ""), 20))
            osEdit.hh1200 = IIf(row("12:00") Is DBNull.Value OrElse row("12:00") = Nothing, DBNull.Value, Left(row("12:00").ToString.Replace(",", ""), 20))
            osEdit.hh1230 = IIf(row("12:30") Is DBNull.Value OrElse row("12:30") = Nothing, DBNull.Value, Left(row("12:30").ToString.Replace(",", ""), 20))
            osEdit.hh1300 = IIf(row("13:00") Is DBNull.Value OrElse row("13:00") = Nothing, DBNull.Value, Left(row("13:00").ToString.Replace(",", ""), 20))
            osEdit.hh1330 = IIf(row("13:30") Is DBNull.Value OrElse row("13:30") = Nothing, DBNull.Value, Left(row("13:30").ToString.Replace(",", ""), 20))
            osEdit.hh1400 = IIf(row("14:00") Is DBNull.Value OrElse row("14:00") = Nothing, DBNull.Value, Left(row("14:00").ToString.Replace(",", ""), 20))
            osEdit.hh1430 = IIf(row("14:30") Is DBNull.Value OrElse row("14:30") = Nothing, DBNull.Value, Left(row("14:30").ToString.Replace(",", ""), 20))
            osEdit.hh1500 = IIf(row("15:00") Is DBNull.Value OrElse row("15:00") = Nothing, DBNull.Value, Left(row("15:00").ToString.Replace(",", ""), 20))
            osEdit.hh1530 = IIf(row("15:30") Is DBNull.Value OrElse row("15:30") = Nothing, DBNull.Value, Left(row("15:30").ToString.Replace(",", ""), 20))
            osEdit.hh1600 = IIf(row("16:00") Is DBNull.Value OrElse row("16:00") = Nothing, DBNull.Value, Left(row("16:00").ToString.Replace(",", ""), 20))
            osEdit.hh1630 = IIf(row("16:30") Is DBNull.Value OrElse row("16:30") = Nothing, DBNull.Value, Left(row("16:30").ToString.Replace(",", ""), 20))
            osEdit.hh1700 = IIf(row("17:00") Is DBNull.Value OrElse row("17:00") = Nothing, DBNull.Value, Left(row("17:00").ToString.Replace(",", ""), 20))
            osEdit.hh1730 = IIf(row("17:30") Is DBNull.Value OrElse row("17:30") = Nothing, DBNull.Value, Left(row("17:30").ToString.Replace(",", ""), 20))
            osEdit.hh1800 = IIf(row("18:00") Is DBNull.Value OrElse row("18:00") = Nothing, DBNull.Value, Left(row("18:00").ToString.Replace(",", ""), 20))
            osEdit.hh1830 = IIf(row("18:30") Is DBNull.Value OrElse row("18:30") = Nothing, DBNull.Value, Left(row("18:30").ToString.Replace(",", ""), 20))
            osEdit.hh1900 = IIf(row("19:00") Is DBNull.Value OrElse row("19:00") = Nothing, DBNull.Value, Left(row("19:00").ToString.Replace(",", ""), 20))
            osEdit.hh1930 = IIf(row("19:30") Is DBNull.Value OrElse row("19:30") = Nothing, DBNull.Value, Left(row("19:30").ToString.Replace(",", ""), 20))
            osEdit.hh1930 = IIf(row("19:30") Is DBNull.Value OrElse row("19:30") = Nothing, DBNull.Value, Left(row("19:30").ToString.Replace(",", ""), 20))
            osEdit.hh2000 = IIf(row("20:00") Is DBNull.Value OrElse row("20:00") = Nothing, DBNull.Value, Left(row("20:00").ToString.Replace(",", ""), 20))
            osEdit.hh2030 = IIf(row("20:30") Is DBNull.Value OrElse row("20:30") = Nothing, DBNull.Value, Left(row("20:30").ToString.Replace(",", ""), 20))
            osEdit.hh2100 = IIf(row("21:00") Is DBNull.Value OrElse row("21:00") = Nothing, DBNull.Value, Left(row("21:00").ToString.Replace(",", ""), 20))
            osEdit.hh2130 = IIf(row("21:30") Is DBNull.Value OrElse row("21:30") = Nothing, DBNull.Value, Left(row("21:30").ToString.Replace(",", ""), 20))
            osEdit.hh2200 = IIf(row("22:00") Is DBNull.Value OrElse row("22:00") = Nothing, DBNull.Value, Left(row("22:00").ToString.Replace(",", ""), 20))
            osEdit.hh2230 = IIf(row("22:30") Is DBNull.Value OrElse row("22:30") = Nothing, DBNull.Value, Left(row("22:30").ToString.Replace(",", ""), 20))
            osEdit.hh2300 = IIf(row("23:00") Is DBNull.Value OrElse row("23:00") = Nothing, DBNull.Value, Left(row("23:00").ToString.Replace(",", ""), 20))
            osEdit.hh2330 = IIf(row("23:30") Is DBNull.Value OrElse row("23:30") = Nothing, DBNull.Value, Left(row("23:30").ToString.Replace(",", ""), 20))

        Catch ex As Exception

            osEdit.StatusId = StatusEnum.Fail
            osEdit.Message = "Fail to import data (fill Operating schedule edit)."

        End Try

    End Sub

End Class