﻿Public Class FE_StaticVariable

    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) 'Implements IFillEditsDelegate.Fill

        Dim svEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Try

            svEdit.ImportId = importEdit.Id
            svEdit.StatusId = StatusEnum.In_Queue
            svEdit.Database = importEdit.DatabaseName

            svEdit.StaticVariableName = IIf(row("StaticVariableName") Is DBNull.Value OrElse row("StaticVariableName") = Nothing, DBNull.Value, row("StaticVariableName").ToString.Trim)
            svEdit.StaticVariableLevel = IIf(row("Level") Is DBNull.Value OrElse row("Level") = Nothing, DBNull.Value, row("Level").ToString.Trim)
            svEdit.BuildingMeterSerial = IIf(row("Building_MeterSerial") Is DBNull.Value OrElse row("Building_MeterSerial") = Nothing, DBNull.Value, row("Building_MeterSerial").ToString.Trim)
            svEdit.StaticVariableUnit = IIf(row("Unit") Is DBNull.Value OrElse row("Unit") = Nothing, DBNull.Value, row("Unit").ToString.Trim)
            svEdit.StaticVariableValue = row("Value")
            svEdit.StaticVariableKwhUnit = row("KwhUnit")


        Catch ex As Exception

            svEdit.StatusId = StatusEnum.Fail
            svEdit.Message = "Fail to import data (fill sv edit)."
            svEdit.Message = ex.Message.ToString

        End Try

    End Sub

End Class
