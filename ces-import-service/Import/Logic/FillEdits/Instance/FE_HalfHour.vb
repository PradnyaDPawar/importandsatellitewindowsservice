﻿Public Class FE_HalfHour


    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)
        Dim rtEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Try

            rtEdit.ImportId = importEdit.Id
            rtEdit.StatusId = StatusEnum.In_Queue
            rtEdit.Database = importEdit.DatabaseName

            rtEdit.Meter_Serial = IIf(row("MeterSerial") Is DBNull.Value OrElse row("MeterSerial") = Nothing, DBNull.Value, row("MeterSerial"))
            rtEdit.MPAN = IIf(row("MPAN") Is DBNull.Value OrElse row("MPAN") = Nothing, DBNull.Value, row("MPAN"))
            rtEdit.DateTime = row("Date(dd/MM/yyyy)")
            rtEdit.Force_kWh = row("ForcekWh")
            'Removes commas and only selects the first 20 characters as that is the maximum length accepted in DB for HH slots
            rtEdit.hh0000 = IIf(row("00:00") Is DBNull.Value OrElse row("00:00") = Nothing, DBNull.Value, Left(row("00:00").ToString.Replace(",", ""), 20))
            rtEdit.hh0030 = IIf(row("00:30") Is DBNull.Value OrElse row("00:30") = Nothing, DBNull.Value, Left(row("00:30").ToString.Replace(",", ""), 20))
            rtEdit.hh0100 = IIf(row("01:00") Is DBNull.Value OrElse row("01:00") = Nothing, DBNull.Value, Left(row("01:00").ToString.Replace(",", ""), 20))
            rtEdit.hh0130 = IIf(row("01:30") Is DBNull.Value OrElse row("01:30") = Nothing, DBNull.Value, Left(row("01:30").ToString.Replace(",", ""), 20))
            rtEdit.hh0200 = IIf(row("02:00") Is DBNull.Value OrElse row("02:00") = Nothing, DBNull.Value, Left(row("02:00").ToString.Replace(",", ""), 20))
            rtEdit.hh0230 = IIf(row("02:30") Is DBNull.Value OrElse row("02:30") = Nothing, DBNull.Value, Left(row("02:30").ToString.Replace(",", ""), 20))
            rtEdit.hh0300 = IIf(row("03:00") Is DBNull.Value OrElse row("03:00") = Nothing, DBNull.Value, Left(row("03:00").ToString.Replace(",", ""), 20))
            rtEdit.hh0330 = IIf(row("03:30") Is DBNull.Value OrElse row("03:30") = Nothing, DBNull.Value, Left(row("03:30").ToString.Replace(",", ""), 20))
            rtEdit.hh0400 = IIf(row("04:00") Is DBNull.Value OrElse row("04:00") = Nothing, DBNull.Value, Left(row("04:00").ToString.Replace(",", ""), 20))
            rtEdit.hh0430 = IIf(row("04:30") Is DBNull.Value OrElse row("04:30") = Nothing, DBNull.Value, Left(row("04:30").ToString.Replace(",", ""), 20))
            rtEdit.hh0500 = IIf(row("05:00") Is DBNull.Value OrElse row("05:00") = Nothing, DBNull.Value, Left(row("05:00").ToString.Replace(",", ""), 20))
            rtEdit.hh0530 = IIf(row("05:30") Is DBNull.Value OrElse row("05:30") = Nothing, DBNull.Value, Left(row("05:30").ToString.Replace(",", ""), 20))
            rtEdit.hh0600 = IIf(row("06:00") Is DBNull.Value OrElse row("06:00") = Nothing, DBNull.Value, Left(row("06:00").ToString.Replace(",", ""), 20))
            rtEdit.hh0630 = IIf(row("06:30") Is DBNull.Value OrElse row("06:30") = Nothing, DBNull.Value, Left(row("06:30").ToString.Replace(",", ""), 20))
            rtEdit.hh0700 = IIf(row("07:00") Is DBNull.Value OrElse row("07:00") = Nothing, DBNull.Value, Left(row("07:00").ToString.Replace(",", ""), 20))
            rtEdit.hh0730 = IIf(row("07:30") Is DBNull.Value OrElse row("07:30") = Nothing, DBNull.Value, Left(row("07:30").ToString.Replace(",", ""), 20))
            rtEdit.hh0800 = IIf(row("08:00") Is DBNull.Value OrElse row("08:00") = Nothing, DBNull.Value, Left(row("08:00").ToString.Replace(",", ""), 20))
            rtEdit.hh0830 = IIf(row("08:30") Is DBNull.Value OrElse row("08:30") = Nothing, DBNull.Value, Left(row("08:30").ToString.Replace(",", ""), 20))
            rtEdit.hh0900 = IIf(row("09:00") Is DBNull.Value OrElse row("09:00") = Nothing, DBNull.Value, Left(row("09:00").ToString.Replace(",", ""), 20))
            rtEdit.hh0930 = IIf(row("09:30") Is DBNull.Value OrElse row("09:30") = Nothing, DBNull.Value, Left(row("09:30").ToString.Replace(",", ""), 20))
            rtEdit.hh1000 = IIf(row("10:00") Is DBNull.Value OrElse row("10:00") = Nothing, DBNull.Value, Left(row("10:00").ToString.Replace(",", ""), 20))
            rtEdit.hh1030 = IIf(row("10:30") Is DBNull.Value OrElse row("10:30") = Nothing, DBNull.Value, Left(row("10:30").ToString.Replace(",", ""), 20))
            rtEdit.hh1100 = IIf(row("11:00") Is DBNull.Value OrElse row("11:00") = Nothing, DBNull.Value, Left(row("11:00").ToString.Replace(",", ""), 20))
            rtEdit.hh1130 = IIf(row("11:30") Is DBNull.Value OrElse row("11:30") = Nothing, DBNull.Value, Left(row("11:30").ToString.Replace(",", ""), 20))
            rtEdit.hh1200 = IIf(row("12:00") Is DBNull.Value OrElse row("12:00") = Nothing, DBNull.Value, Left(row("12:00").ToString.Replace(",", ""), 20))
            rtEdit.hh1230 = IIf(row("12:30") Is DBNull.Value OrElse row("12:30") = Nothing, DBNull.Value, Left(row("12:30").ToString.Replace(",", ""), 20))
            rtEdit.hh1300 = IIf(row("13:00") Is DBNull.Value OrElse row("13:00") = Nothing, DBNull.Value, Left(row("13:00").ToString.Replace(",", ""), 20))
            rtEdit.hh1330 = IIf(row("13:30") Is DBNull.Value OrElse row("13:30") = Nothing, DBNull.Value, Left(row("13:30").ToString.Replace(",", ""), 20))
            rtEdit.hh1400 = IIf(row("14:00") Is DBNull.Value OrElse row("14:00") = Nothing, DBNull.Value, Left(row("14:00").ToString.Replace(",", ""), 20))
            rtEdit.hh1430 = IIf(row("14:30") Is DBNull.Value OrElse row("14:30") = Nothing, DBNull.Value, Left(row("14:30").ToString.Replace(",", ""), 20))
            rtEdit.hh1500 = IIf(row("15:00") Is DBNull.Value OrElse row("15:00") = Nothing, DBNull.Value, Left(row("15:00").ToString.Replace(",", ""), 20))
            rtEdit.hh1530 = IIf(row("15:30") Is DBNull.Value OrElse row("15:30") = Nothing, DBNull.Value, Left(row("15:30").ToString.Replace(",", ""), 20))
            rtEdit.hh1600 = IIf(row("16:00") Is DBNull.Value OrElse row("16:00") = Nothing, DBNull.Value, Left(row("16:00").ToString.Replace(",", ""), 20))
            rtEdit.hh1630 = IIf(row("16:30") Is DBNull.Value OrElse row("16:30") = Nothing, DBNull.Value, Left(row("16:30").ToString.Replace(",", ""), 20))
            rtEdit.hh1700 = IIf(row("17:00") Is DBNull.Value OrElse row("17:00") = Nothing, DBNull.Value, Left(row("17:00").ToString.Replace(",", ""), 20))
            rtEdit.hh1730 = IIf(row("17:30") Is DBNull.Value OrElse row("17:30") = Nothing, DBNull.Value, Left(row("17:30").ToString.Replace(",", ""), 20))
            rtEdit.hh1800 = IIf(row("18:00") Is DBNull.Value OrElse row("18:00") = Nothing, DBNull.Value, Left(row("18:00").ToString.Replace(",", ""), 20))
            rtEdit.hh1830 = IIf(row("18:30") Is DBNull.Value OrElse row("18:30") = Nothing, DBNull.Value, Left(row("18:30").ToString.Replace(",", ""), 20))
            rtEdit.hh1900 = IIf(row("19:00") Is DBNull.Value OrElse row("19:00") = Nothing, DBNull.Value, Left(row("19:00").ToString.Replace(",", ""), 20))
            rtEdit.hh1930 = IIf(row("19:30") Is DBNull.Value OrElse row("19:30") = Nothing, DBNull.Value, Left(row("19:30").ToString.Replace(",", ""), 20))
            rtEdit.hh1930 = IIf(row("19:30") Is DBNull.Value OrElse row("19:30") = Nothing, DBNull.Value, Left(row("19:30").ToString.Replace(",", ""), 20))
            rtEdit.hh2000 = IIf(row("20:00") Is DBNull.Value OrElse row("20:00") = Nothing, DBNull.Value, Left(row("20:00").ToString.Replace(",", ""), 20))
            rtEdit.hh2030 = IIf(row("20:30") Is DBNull.Value OrElse row("20:30") = Nothing, DBNull.Value, Left(row("20:30").ToString.Replace(",", ""), 20))
            rtEdit.hh2100 = IIf(row("21:00") Is DBNull.Value OrElse row("21:00") = Nothing, DBNull.Value, Left(row("21:00").ToString.Replace(",", ""), 20))
            rtEdit.hh2130 = IIf(row("21:30") Is DBNull.Value OrElse row("21:30") = Nothing, DBNull.Value, Left(row("21:30").ToString.Replace(",", ""), 20))
            rtEdit.hh2200 = IIf(row("22:00") Is DBNull.Value OrElse row("22:00") = Nothing, DBNull.Value, Left(row("22:00").ToString.Replace(",", ""), 20))
            rtEdit.hh2230 = IIf(row("22:30") Is DBNull.Value OrElse row("22:30") = Nothing, DBNull.Value, Left(row("22:30").ToString.Replace(",", ""), 20))
            rtEdit.hh2300 = IIf(row("23:00") Is DBNull.Value OrElse row("23:00") = Nothing, DBNull.Value, Left(row("23:00").ToString.Replace(",", ""), 20))
            rtEdit.hh2330 = IIf(row("23:30") Is DBNull.Value OrElse row("23:30") = Nothing, DBNull.Value, Left(row("23:30").ToString.Replace(",", ""), 20))

        Catch ex As Exception

            rtEdit.StatusId = StatusEnum.Fail
            rtEdit.Message = "Fail to import data (fill HH edit)."

        End Try

    End Sub
End Class
