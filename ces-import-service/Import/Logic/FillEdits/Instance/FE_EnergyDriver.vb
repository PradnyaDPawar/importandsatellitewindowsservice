﻿Public Class FE_EnergyDriver
    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)
        Dim rtEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Try

            rtEdit.ImportId = importEdit.Id
            rtEdit.StatusId = StatusEnum.In_Queue
            rtEdit.Database = importEdit.DatabaseName

            rtEdit.EnergyDriverReference = IIf(row("EnergyDriverReference") Is DBNull.Value OrElse row("EnergyDriverReference") = Nothing, DBNull.Value.ToString(), row("EnergyDriverReference"))
            rtEdit.ED_Date = IIf(row("Date") Is DBNull.Value OrElse row("Date") = Nothing, DBNull.Value.ToString(), row("Date"))
            rtEdit.ED0000 = IIf(row("00:00") Is DBNull.Value OrElse row("00:00") = Nothing, DBNull.Value, row("00:00"))
            rtEdit.ED0030 = IIf(row("00:30") Is DBNull.Value OrElse row("00:30") = Nothing, DBNull.Value, row("00:30"))
            rtEdit.ED0100 = IIf(row("01:00") Is DBNull.Value OrElse row("01:00") = Nothing, DBNull.Value, row("01:00"))
            rtEdit.ED0130 = IIf(row("01:30") Is DBNull.Value OrElse row("01:30") = Nothing, DBNull.Value, row("01:30"))
            rtEdit.ED0200 = IIf(row("02:00") Is DBNull.Value OrElse row("02:00") = Nothing, DBNull.Value, row("02:00"))
            rtEdit.ED0230 = IIf(row("02:30") Is DBNull.Value OrElse row("02:30") = Nothing, DBNull.Value, row("02:30"))
            rtEdit.ED0300 = IIf(row("03:00") Is DBNull.Value OrElse row("03:00") = Nothing, DBNull.Value, row("03:00"))
            rtEdit.ED0330 = IIf(row("03:30") Is DBNull.Value OrElse row("03:30") = Nothing, DBNull.Value, row("03:30"))
            rtEdit.ED0400 = IIf(row("04:00") Is DBNull.Value OrElse row("04:00") = Nothing, DBNull.Value, row("04:00"))
            rtEdit.ED0430 = IIf(row("04:30") Is DBNull.Value OrElse row("04:30") = Nothing, DBNull.Value, row("04:30"))
            rtEdit.ED0500 = IIf(row("05:00") Is DBNull.Value OrElse row("05:00") = Nothing, DBNull.Value, row("05:00"))
            rtEdit.ED0530 = IIf(row("05:30") Is DBNull.Value OrElse row("05:30") = Nothing, DBNull.Value, row("05:30"))
            rtEdit.ED0600 = IIf(row("06:00") Is DBNull.Value OrElse row("06:00") = Nothing, DBNull.Value, row("06:00"))
            rtEdit.ED0630 = IIf(row("06:30") Is DBNull.Value OrElse row("06:30") = Nothing, DBNull.Value, row("06:30"))
            rtEdit.ED0700 = IIf(row("07:00") Is DBNull.Value OrElse row("07:00") = Nothing, DBNull.Value, row("07:00"))
            rtEdit.ED0730 = IIf(row("07:30") Is DBNull.Value OrElse row("07:30") = Nothing, DBNull.Value, row("07:30"))
            rtEdit.ED0800 = IIf(row("08:00") Is DBNull.Value OrElse row("08:00") = Nothing, DBNull.Value, row("08:00"))
            rtEdit.ED0830 = IIf(row("08:30") Is DBNull.Value OrElse row("08:30") = Nothing, DBNull.Value, row("08:30"))
            rtEdit.ED0900 = IIf(row("09:00") Is DBNull.Value OrElse row("09:00") = Nothing, DBNull.Value, row("09:00"))
            rtEdit.ED0930 = IIf(row("09:30") Is DBNull.Value OrElse row("09:30") = Nothing, DBNull.Value, row("09:30"))
            rtEdit.ED1000 = IIf(row("10:00") Is DBNull.Value OrElse row("10:00") = Nothing, DBNull.Value, row("10:00"))
            rtEdit.ED1030 = IIf(row("10:30") Is DBNull.Value OrElse row("10:30") = Nothing, DBNull.Value, row("10:30"))
            rtEdit.ED1100 = IIf(row("11:00") Is DBNull.Value OrElse row("11:00") = Nothing, DBNull.Value, row("11:00"))
            rtEdit.ED1130 = IIf(row("11:30") Is DBNull.Value OrElse row("11:30") = Nothing, DBNull.Value, row("11:30"))
            rtEdit.ED1200 = IIf(row("12:00") Is DBNull.Value OrElse row("12:00") = Nothing, DBNull.Value, row("12:00"))
            rtEdit.ED1230 = IIf(row("12:30") Is DBNull.Value OrElse row("12:30") = Nothing, DBNull.Value, row("12:30"))
            rtEdit.ED1300 = IIf(row("13:00") Is DBNull.Value OrElse row("13:00") = Nothing, DBNull.Value, row("13:00"))
            rtEdit.ED1330 = IIf(row("13:30") Is DBNull.Value OrElse row("13:30") = Nothing, DBNull.Value, row("13:30"))
            rtEdit.ED1400 = IIf(row("14:00") Is DBNull.Value OrElse row("14:00") = Nothing, DBNull.Value, row("14:00"))
            rtEdit.ED1430 = IIf(row("14:30") Is DBNull.Value OrElse row("14:30") = Nothing, DBNull.Value, row("14:30"))
            rtEdit.ED1500 = IIf(row("15:00") Is DBNull.Value OrElse row("15:00") = Nothing, DBNull.Value, row("15:00"))
            rtEdit.ED1530 = IIf(row("15:30") Is DBNull.Value OrElse row("15:30") = Nothing, DBNull.Value, row("15:30"))
            rtEdit.ED1600 = IIf(row("16:00") Is DBNull.Value OrElse row("16:00") = Nothing, DBNull.Value, row("16:00"))
            rtEdit.ED1630 = IIf(row("16:30") Is DBNull.Value OrElse row("16:30") = Nothing, DBNull.Value, row("16:30"))
            rtEdit.ED1700 = IIf(row("17:00") Is DBNull.Value OrElse row("17:00") = Nothing, DBNull.Value, row("17:00"))
            rtEdit.ED1730 = IIf(row("17:30") Is DBNull.Value OrElse row("17:30") = Nothing, DBNull.Value, row("17:30"))
            rtEdit.ED1800 = IIf(row("18:00") Is DBNull.Value OrElse row("18:00") = Nothing, DBNull.Value, row("18:00"))
            rtEdit.ED1830 = IIf(row("18:30") Is DBNull.Value OrElse row("18:30") = Nothing, DBNull.Value, row("18:30"))
            rtEdit.ED1900 = IIf(row("19:00") Is DBNull.Value OrElse row("19:00") = Nothing, DBNull.Value, row("19:00"))
            rtEdit.ED1930 = IIf(row("19:30") Is DBNull.Value OrElse row("19:30") = Nothing, DBNull.Value, row("19:30"))
            rtEdit.ED2000 = IIf(row("20:00") Is DBNull.Value OrElse row("20:00") = Nothing, DBNull.Value, row("20:00"))
            rtEdit.ED2030 = IIf(row("20:30") Is DBNull.Value OrElse row("20:30") = Nothing, DBNull.Value, row("20:30"))
            rtEdit.ED2100 = IIf(row("21:00") Is DBNull.Value OrElse row("21:00") = Nothing, DBNull.Value, row("21:00"))
            rtEdit.ED2130 = IIf(row("21:30") Is DBNull.Value OrElse row("21:30") = Nothing, DBNull.Value, row("21:30"))
            rtEdit.ED2200 = IIf(row("22:00") Is DBNull.Value OrElse row("22:00") = Nothing, DBNull.Value, row("22:00"))
            rtEdit.ED2230 = IIf(row("22:30") Is DBNull.Value OrElse row("22:30") = Nothing, DBNull.Value, row("22:30"))

            rtEdit.ED2300 = IIf(row("23:00") Is DBNull.Value OrElse row("23:00") = Nothing, DBNull.Value, row("23:00"))
            rtEdit.ED2330 = IIf(row("23:30") Is DBNull.Value OrElse row("23:30") = Nothing, DBNull.Value, row("23:30"))



        Catch ex As Exception

            rtEdit.StatusId = StatusEnum.Fail
            rtEdit.Message = "Fail to import data (fill HH edit)."

        End Try
    End Sub

End Class
