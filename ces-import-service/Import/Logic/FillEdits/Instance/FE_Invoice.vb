﻿Public Class FE_Invoice

    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)

        Dim invEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Try
            invEdit.ImportId = importEdit.Id
            invEdit.StatusId = StatusEnum.In_Queue
            invEdit.Database = importEdit.DatabaseName

            invEdit.InvoiceNumber = IIf(row("InvoiceNumber") Is DBNull.Value OrElse row("InvoiceNumber") = Nothing, DBNull.Value, row("InvoiceNumber").ToString.Trim)
            invEdit.Meter_Serial = IIf(row("MeterSerial") Is DBNull.Value OrElse row("MeterSerial") = Nothing, DBNull.Value, row("MeterSerial").ToString.Trim)
            invEdit.MPAN = IIf(row("MPAN") Is DBNull.Value OrElse row("MPAN") = Nothing, DBNull.Value, row("MPAN").ToString.Trim)
            invEdit.Tariff = IIf(row("Tariff") Is DBNull.Value OrElse row("Tariff") = Nothing, DBNull.Value, row("Tariff").ToString.Trim)
            invEdit.StartDate = IIf(row("StartDate") Is DBNull.Value OrElse row("StartDate") = Nothing, DBNull.Value.ToString(), row("StartDate").ToString.Trim)
            invEdit.EndDate = IIf(row("EndDate") Is DBNull.Value OrElse row("EndDate") = Nothing, DBNull.Value.ToString(), row("EndDate").ToString.Trim)
            invEdit.StartRead = IIf(row("StartRead") Is DBNull.Value OrElse row("StartRead") = Nothing, DBNull.Value, row("StartRead").ToString.Trim.Replace(",", ""))
            invEdit.StartReadEstimated = IIf(row("StartReadEstimated") Is DBNull.Value OrElse row("StartReadEstimated") = Nothing, DBNull.Value, GetEstimatedValue(row("StartReadEstimated").ToString.Trim))
            invEdit.EndRead = IIf(row("EndRead") Is DBNull.Value OrElse row("EndRead") = Nothing, DBNull.Value, row("EndRead").ToString.Trim.Replace(",", ""))
            invEdit.EndReadEstimated = IIf(row("EndReadEstimated") Is DBNull.Value OrElse row("EndReadEstimated") = Nothing, DBNull.Value, GetEstimatedValue(row("EndReadEstimated").ToString.Trim))
            invEdit.Consumption = IIf(row("Consumption") Is DBNull.Value OrElse row("Consumption") = Nothing, DBNull.Value, row("Consumption").ToString.Trim.Replace(",", ""))
            invEdit.ForcekWh = IIf(row("ForcekWh") Is DBNull.Value OrElse row("ForcekWh") = Nothing, DBNull.Value, GetRowValue(row("ForcekWh").ToString.Trim))
            invEdit.InvoiceRate = IIf(row("InvoiceRate") Is DBNull.Value OrElse row("InvoiceRate") = Nothing, DBNull.Value, row("InvoiceRate").ToString.Trim.Replace(",", ""))
            invEdit.CostUnit = IIf(row("CostUnit") Is DBNull.Value OrElse row("CostUnit") = Nothing, DBNull.Value, row("CostUnit").ToString.Trim.Replace(",", ""))
            invEdit.Vat = IIf(row("Vat") Is DBNull.Value OrElse row("Vat") = Nothing, DBNull.Value, row("Vat").ToString.Trim.Replace(",", ""))
            invEdit.Net = IIf(row("Net") Is DBNull.Value OrElse row("Net") = Nothing, DBNull.Value, row("Net").ToString.Trim.Replace(",", ""))
            invEdit.Gross = IIf(row("Gross") Is DBNull.Value OrElse row("Gross") = Nothing, DBNull.Value, row("Gross").ToString.Trim.Replace(",", ""))
            invEdit.CostOnly = IIf(row("CostOnly") Is DBNull.Value OrElse row("CostOnly") = Nothing, DBNull.Value, GetRowValue(row("CostOnly").ToString.Trim))
            invEdit.Description = IIf(row("Description") Is DBNull.Value OrElse row("Description") = Nothing, DBNull.Value.ToString(), row("Description").ToString.Trim)
            invEdit.TaxPointDate = IIf(row("TaxPointDate") Is DBNull.Value OrElse row("TaxPointDate") = Nothing, DBNull.Value, row("TaxPointDate").ToString.Trim)
            invEdit.IsLastDayApportion = IIf(row("IsLastDayApportion") Is DBNull.Value OrElse row("IsLastDayApportion") = Nothing, DBNull.Value, row("IsLastDayApportion").ToString.Trim)
            invEdit.IsTransmissionTariff = IIf(row("IsTransmissionTariff") Is DBNull.Value OrElse row("IsTransmissionTariff") = Nothing, DBNull.Value, GetRowValue(row("IsTransmissionTariff").ToString.Trim))

        Catch ex As Exception

            invEdit.StatusId = StatusEnum.Fail
            invEdit.Message = "Fail to import data (fill invoice edit)."
            invEdit.Message = ex.Message.ToString

        End Try

    End Sub

    Public Shared Function GetEstimatedValue(ByVal rowValue As String) As String
        Dim value As String

        If rowValue.ToLower.Trim = "actual" Or rowValue.ToLower.Trim = "no" Or rowValue.ToLower.Trim = "false" Or rowValue.ToLower.Trim = "a" _
            Or rowValue.ToLower.Trim = "c" Or rowValue.ToLower.Trim = "m" Or rowValue.ToLower.Trim = "r" Or rowValue.ToLower.Trim = "act" Then
            value = "A"
        ElseIf rowValue.ToLower.Trim = "estimated" Or rowValue.ToLower.Trim = "yes" Or rowValue.ToLower.Trim = "true" Or rowValue.ToLower.Trim = "e" Or rowValue.ToLower.Trim = "" Then
            value = "E"
        Else
            value = rowValue
        End If

        Return value

    End Function

    Public Shared Function GetRowValue(ByVal rowvalue As String) As String
        Dim value As String

        If rowvalue.ToLower.Trim = "yes" Or rowvalue.ToLower.Trim = "true" Or rowvalue.ToLower.Trim = "1" Then
            value = "Yes"
        ElseIf rowvalue.ToLower.Trim = "no" Or rowvalue.ToLower.Trim = "false" Or rowvalue.ToLower.Trim = "0" Then
            value = "No"
        Else
            value = rowvalue

        End If
        Return value
    End Function

End Class
