﻿Public Class FE_ProjectMeter

    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)

        Dim dsEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Try

            dsEdit.ImportId = importEdit.Id
            dsEdit.StatusId = StatusEnum.In_Queue
            dsEdit.Database = importEdit.DatabaseName

            dsEdit.SerialNumber = IIf(row("SerialNumber") Is DBNull.Value OrElse row("SerialNumber") = Nothing, String.Empty, row("SerialNumber").ToString.Trim)
            dsEdit.ProjectName = IIf(row("ProjectName") Is DBNull.Value OrElse row("ProjectName") = Nothing, String.Empty, row("ProjectName").ToString.Trim)
            dsEdit.Remove = IIf(row("Remove") Is DBNull.Value OrElse row("Remove") = Nothing, String.Empty, row("Remove").ToString.Trim)

        Catch ex As Exception

            dsEdit.StatusId = StatusEnum.Fail
            dsEdit.Message = "Fail to import data (fill project meter edit)."
            dsEdit.Message = ex.Message.ToString

        End Try

    End Sub


End Class
