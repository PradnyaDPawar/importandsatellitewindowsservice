﻿Public Class FE_AssetStatus
    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)
        Dim rtEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Dim deconfig As DeConfig = New DeConfig()
        Dim ImportVersion As String = deconfig.GetConfig().ImportVersion



        rtEdit.ImportId = importEdit.Id
        rtEdit.StatusId = StatusEnum.In_Queue
        rtEdit.Database = importEdit.DatabaseName

        rtEdit.AssetStatus = IIf(row("status") Is DBNull.Value OrElse row("status") = Nothing, DBNull.Value.ToString(), row("status"))
        rtEdit.AssetStatusStartDate = IIf(row("startdate") Is DBNull.Value OrElse row("startdate") = Nothing, DBNull.Value.ToString(), row("startdate"))
        rtEdit.AssetStatusEndDate = IIf(row("enddate") Is DBNull.Value OrElse row("enddate") = Nothing, DBNull.Value.ToString(), row("enddate"))
        rtEdit.AssetStatusUprn = IIf(row("uprn") Is DBNull.Value OrElse row("uprn") = Nothing, DBNull.Value.ToString(), row("uprn"))

    End Sub
End Class
