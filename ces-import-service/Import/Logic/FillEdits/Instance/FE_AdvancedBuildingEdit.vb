﻿Public Class FE_AdvancedBuildingEdit
    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)

        Dim dsEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Dim deconfig As DeConfig = New DeConfig()
        Dim ImportVersion As String = deconfig.GetConfig().ImportVersion

        Try

            dsEdit.ImportId = importEdit.Id
            dsEdit.StatusId = StatusEnum.In_Queue
            dsEdit.Database = importEdit.DatabaseName

            dsEdit.UPRN = IIf(row("UPRN") Is DBNull.Value OrElse row("UPRN") = Nothing, String.Empty, row("UPRN").ToString.Trim)
            If ImportVersion <> "Vestigo" Then
                dsEdit.BuildingOwner = IIf(row("AssetOwner") Is DBNull.Value OrElse row("AssetOwner") = Nothing, String.Empty, row("AssetOwner").ToString.Trim)
            Else
                dsEdit.BuildingOwner = IIf(row("BuildingOwner") Is DBNull.Value OrElse row("BuildingOwner") = Nothing, String.Empty, row("BuildingOwner").ToString.Trim)
            End If
            dsEdit.Tenant = IIf(row("Tenant") Is DBNull.Value OrElse row("Tenant") = Nothing, String.Empty, row("Tenant").ToString.Trim)
            dsEdit.FacilitiesManager = IIf(row("FacilitiesManager") Is DBNull.Value OrElse row("FacilitiesManager") = Nothing, String.Empty, row("FacilitiesManager").ToString.Trim)
            dsEdit.EnergyChampion = IIf(row("EnergyChampion") Is DBNull.Value OrElse row("EnergyChampion") = Nothing, String.Empty, row("EnergyChampion").ToString.Trim)
            dsEdit.MainSectorBenchmark = IIf(row("MainSectorBenchmark") Is DBNull.Value OrElse row("MainSectorBenchmark") = Nothing, String.Empty, row("MainSectorBenchmark").ToString.Trim)
            dsEdit.SubSectorBenchmark = IIf(row("SubSectorBenchmark") Is DBNull.Value OrElse row("SubSectorBenchmark") = Nothing, String.Empty, row("SubSectorBenchmark").ToString.Trim)
            dsEdit.TotalFloorArea = IIf(row("TotalFloorArea") Is DBNull.Value OrElse row("TotalFloorArea") = Nothing, String.Empty, row("TotalFloorArea").ToString.Trim)
            dsEdit.NumberOfOccupants = IIf(row("NumberOfOccupants") Is DBNull.Value OrElse row("NumberOfOccupants") = Nothing, String.Empty, row("NumberOfOccupants").ToString.Trim)
            dsEdit.NumberOfPupils = IIf(row("NumberOfPupils") Is DBNull.Value OrElse row("NumberOfPupils") = Nothing, String.Empty, row("NumberOfPupils").ToString.Trim)
            dsEdit.AnnualHoursOfOccupancy = IIf(row("AnnualHoursOfOccupancy") Is DBNull.Value OrElse row("AnnualHoursOfOccupancy") = Nothing, String.Empty, row("AnnualHoursOfOccupancy").ToString.Trim)
            dsEdit.MainHeatingSystem = IIf(row("MainHeatingSystem") Is DBNull.Value OrElse row("MainHeatingSystem") = Nothing, String.Empty, row("MainHeatingSystem").ToString.Trim)
            dsEdit.MainHeatingFuel = IIf(row("MainHeatingFuel") Is DBNull.Value OrElse row("MainHeatingFuel") = Nothing, String.Empty, row("MainHeatingFuel").ToString.Trim)
            dsEdit.HeatingSetpoint = IIf(row("HeatingSetpoint") Is DBNull.Value OrElse row("HeatingSetpoint") = Nothing, String.Empty, row("HeatingSetpoint").ToString.Trim)
            dsEdit.CoolingSetpoint = IIf(row("CoolingSetpoint") Is DBNull.Value OrElse row("CoolingSetpoint") = Nothing, String.Empty, row("CoolingSetpoint").ToString.Trim)
            dsEdit.PropertyRef = IIf(row("PropertyRef") Is DBNull.Value OrElse row("PropertyRef") = Nothing, String.Empty, row("PropertyRef").ToString.Trim)
            dsEdit.BuiltDate = IIf(row("BuiltDate") Is DBNull.Value OrElse row("BuiltDate") = Nothing, String.Empty, row("BuiltDate").ToString.Trim)
            dsEdit.Subdivision = IIf(row("Subdivision") Is DBNull.Value OrElse row("Subdivision") = Nothing, String.Empty, row("Subdivision").ToString.Trim)
            dsEdit.EPCGrade = IIf(row("EPCGrade") Is DBNull.Value OrElse row("EPCGrade") = Nothing, String.Empty, row("EPCGrade").ToString.Trim)
            dsEdit.EPCRating = IIf(row("EPCRating") Is DBNull.Value OrElse row("EPCRating") = Nothing, String.Empty, row("EPCRating").ToString.Trim)
            dsEdit.EPCOther = IIf(row("EPCOther") Is DBNull.Value OrElse row("EPCOther") = Nothing, String.Empty, row("EPCOther").ToString.Trim)
            dsEdit.Notes = IIf(row("Notes") Is DBNull.Value OrElse row("Notes") = Nothing, String.Empty, row("Notes").ToString.Trim)
            dsEdit.RateableValue = IIf(row("RateableValue") Is DBNull.Value OrElse row("RateableValue") = Nothing, String.Empty, row("RateableValue").ToString.Trim)
            dsEdit.NetInternalArea = IIf(row("NetInternalArea") Is DBNull.Value OrElse row("NetInternalArea") = Nothing, String.Empty, row("NetInternalArea").ToString.Trim)
            dsEdit.Category = IIf(row("Category") Is DBNull.Value OrElse row("Category") = Nothing, String.Empty, row("Category").ToString.Trim)
            dsEdit.PropertyManager = IIf(row("PropertyManager") Is DBNull.Value OrElse row("PropertyManager") = Nothing, String.Empty, row("PropertyManager").ToString.Trim)
            dsEdit.PropertySurveyor = IIf(row("PropertySurveyor") Is DBNull.Value OrElse row("PropertySurveyor") = Nothing, String.Empty, row("PropertySurveyor").ToString.Trim)
            dsEdit.Administrator = IIf(row("Administrator") Is DBNull.Value OrElse row("Administrator") = Nothing, String.Empty, row("Administrator").ToString.Trim)
            If ImportVersion <> "Vestigo" Then
                dsEdit.BuildingCategory = IIf(row("AssetCategory") Is DBNull.Value OrElse row("AssetCategory") = Nothing, String.Empty, row("AssetCategory").ToString.Trim)
            Else
                dsEdit.BuildingCategory = IIf(row("BuildingCategory") Is DBNull.Value OrElse row("BuildingCategory") = Nothing, String.Empty, row("BuildingCategory").ToString.Trim)
            End If

            dsEdit.BudgetCode = IIf(row("BudgetCode") Is DBNull.Value OrElse row("BudgetCode") = Nothing, String.Empty, row("BudgetCode").ToString.Trim)
            dsEdit.Division = IIf(row("Division") Is DBNull.Value OrElse row("Division") = Nothing, String.Empty, row("Division").ToString.Trim)
            dsEdit.PropertyType = IIf(row("PropertyType") Is DBNull.Value OrElse row("PropertyType") = Nothing, String.Empty, row("PropertyType").ToString.Trim)
            dsEdit.Service = IIf(row("Service") Is DBNull.Value OrElse row("Service") = Nothing, String.Empty, row("Service").ToString.Trim)

        Catch ex As Exception

            dsEdit.StatusId = StatusEnum.Fail
            dsEdit.Message = "Fail to import data (fill advanced building edit)."
            dsEdit.Message = ex.Message.ToString

        End Try

    End Sub
End Class
