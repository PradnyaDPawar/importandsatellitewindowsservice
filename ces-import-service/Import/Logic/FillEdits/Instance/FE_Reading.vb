﻿Public Class FE_Reading
    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)
        Dim rtEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Dim deconfig As DeConfig = New DeConfig()
        Dim ImportVersion As String = deconfig.GetConfig().ImportVersion



        rtEdit.ImportId = importEdit.Id
        rtEdit.StatusId = StatusEnum.In_Queue
        rtEdit.Database = importEdit.DatabaseName

        rtEdit.ReadingMeterSerial = IIf(row("meterserial") Is DBNull.Value OrElse row("meterserial") = Nothing, DBNull.Value.ToString(), row("meterserial"))
        rtEdit.ReadingDate = IIf(row("date") Is DBNull.Value OrElse row("date") = Nothing, DBNull.Value.ToString(), row("date"))
        rtEdit.Reading = IIf(row("reading") Is DBNull.Value OrElse row("reading") = Nothing, DBNull.Value.ToString(), row("reading"))
        rtEdit.ReadingEstimate = IIf(row("estimated") Is DBNull.Value OrElse row("estimated") = Nothing, DBNull.Value.ToString(), row("estimated"))
        rtEdit.ReadingIsWrapRound = IIf(row("iswrapround") Is DBNull.Value OrElse row("iswrapround") = Nothing, DBNull.Value.ToString(), row("iswrapround"))
        rtEdit.ReadingIsReset = IIf(row("isreset") Is DBNull.Value OrElse row("isreset") = Nothing, DBNull.Value.ToString(), row("isreset"))
        rtEdit.ReadingIsConsumption = IIf(row("isconsumption") Is DBNull.Value OrElse row("isconsumption") = Nothing, DBNull.Value.ToString(), row("isconsumption"))
        rtEdit.ReadingMpan = IIf(row("mpan") Is DBNull.Value OrElse row("mpan") = Nothing, DBNull.Value.ToString(), row("mpan"))
        rtEdit.ReadingRegister = IIf(row("register") Is DBNull.Value OrElse row("register") = Nothing, DBNull.Value.ToString(), row("register"))
        rtEdit.ReadingUpdatedBy = IIf(importEdit.UploadedBy Is DBNull.Value OrElse importEdit.UploadedBy = Nothing, "", importEdit.UploadedBy)


    End Sub
End Class
