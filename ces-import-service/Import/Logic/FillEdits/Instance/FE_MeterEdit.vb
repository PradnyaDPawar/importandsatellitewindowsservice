﻿Public Class FE_MeterEdit
    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit, ByRef isSubmeter As Boolean)

        Dim rtEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Dim deconfig As DeConfig = New DeConfig()
        Dim ImportVersion As String = deconfig.GetConfig().ImportVersion

        Try

            rtEdit.ImportId = importEdit.Id
            rtEdit.StatusId = StatusEnum.In_Queue
            rtEdit.Database = importEdit.DatabaseName

            rtEdit.Client = IIf(row("Client") Is DBNull.Value OrElse row("Client") = Nothing, String.Empty, TrimColumnSpace(row("Client")))
            rtEdit.MeterFuel = IIf(row("MeterFuel") Is DBNull.Value OrElse row("MeterFuel") = Nothing, String.Empty, TrimColumnSpace(row("MeterFuel")))
            rtEdit.CurrentSerialNumber = IIf(row("CurrentSerialNumber") Is DBNull.Value OrElse row("CurrentSerialNumber") = Nothing, String.Empty, TrimColumnSpace(row("CurrentSerialNumber")))
            rtEdit.NewSerialNumber = IIf(row("NewSerialNumber") Is DBNull.Value OrElse row("NewSerialNumber") = Nothing, String.Empty, TrimColumnSpace(row("NewSerialNumber")))
            If ImportVersion <> "Vestigo" Then
                rtEdit.BuildingUPRN = IIf(row("AssetUPRN") Is DBNull.Value OrElse row("AssetUPRN") = Nothing, String.Empty, TrimColumnSpace(row("AssetUPRN")))
            Else
                rtEdit.BuildingUPRN = IIf(row("BuildingUPRN") Is DBNull.Value OrElse row("BuildingUPRN") = Nothing, String.Empty, TrimColumnSpace(row("BuildingUPRN")))
            End If
            rtEdit.MeterName = IIf(row("MeterName") Is DBNull.Value OrElse row("MeterName") = Nothing, String.Empty, TrimColumnSpace(row("MeterName")))
            rtEdit.Units = IIf(row("Units") Is DBNull.Value OrElse row("Units") = Nothing, String.Empty, TrimColumnSpace(row("Units")))
            rtEdit.ReportingMeter = IIf(row("ReportingMeter") Is DBNull.Value OrElse row("ReportingMeter") = Nothing, String.Empty, TrimColumnSpace(row("ReportingMeter")))
            rtEdit.IsActive = IIf(row("IsActive") Is DBNull.Value OrElse row("IsActive") = Nothing, String.Empty, TrimColumnSpace(row("IsActive")))
            rtEdit.MeterDeactivationDate = IIf(row("MeterDeactivationDate") Is DBNull.Value OrElse row("MeterDeactivationDate") = Nothing, String.Empty, TrimColumnSpace(row("MeterDeactivationDate")))
            rtEdit.OperatingSchedule1 = IIf(row("OperatingSchedule1") Is DBNull.Value OrElse row("OperatingSchedule1") = Nothing, String.Empty, TrimColumnSpace(row("OperatingSchedule1")))
            rtEdit.OperatingSchedule2 = IIf(row("OperatingSchedule2") Is DBNull.Value OrElse row("OperatingSchedule2") = Nothing, String.Empty, TrimColumnSpace(row("OperatingSchedule2")))
            rtEdit.Location = IIf(row("Location") Is DBNull.Value OrElse row("Location") = Nothing, String.Empty, TrimColumnSpace(row("Location")))
            rtEdit.Description = IIf(row("Description") Is DBNull.Value OrElse row("Description") = Nothing, String.Empty, TrimColumnSpace(row("Description")))
            rtEdit.CorrectionFactor = IIf(row("CorrectionFactor") Is DBNull.Value OrElse row("CorrectionFactor") = Nothing, String.Empty, TrimColumnSpace(row("CorrectionFactor")))
            rtEdit.ConversionFactor = IIf(row("ConversionFactor") Is DBNull.Value OrElse row("ConversionFactor") = Nothing, String.Empty, TrimColumnSpace(row("ConversionFactor")))
            rtEdit.CustomMultiplier = IIf(row("CustomMultiplier") Is DBNull.Value OrElse row("CustomMultiplier") = Nothing, String.Empty, TrimColumnSpace(row("CustomMultiplier")))
            rtEdit.ValidateReadings = IIf(row("ValidateReadings") Is DBNull.Value OrElse row("ValidateReadings") = Nothing, String.Empty, TrimColumnSpace(row("ValidateReadings")))
            rtEdit.ReadingWrapRound = IIf(row("ReadingWrapRound") Is DBNull.Value OrElse row("ReadingWrapRound") = Nothing, String.Empty, TrimColumnSpace(row("ReadingWrapRound")))
            rtEdit.MaxReadConsumption = IIf(row("MaxReadConsumption") Is DBNull.Value OrElse row("MaxReadConsumption") = Nothing, String.Empty, TrimColumnSpace(row("MaxReadConsumption")))
            rtEdit.RealTime = IIf(row("RealTime") Is DBNull.Value OrElse row("RealTime") = Nothing, String.Empty, TrimColumnSpace(row("RealTime")))
            rtEdit.WrapRoundValue = IIf(row("WrapRoundValue") Is DBNull.Value OrElse row("WrapRoundValue") = Nothing, String.Empty, TrimColumnSpace(row("WrapRoundValue")))
            rtEdit.IsCumulative = IIf(row("IsCumulative") Is DBNull.Value OrElse row("IsCumulative") = Nothing, String.Empty, TrimColumnSpace(row("IsCumulative")))
            rtEdit.IsRaw = IIf(row("IsRaw") Is DBNull.Value OrElse row("IsRaw") = Nothing, String.Empty, TrimColumnSpace(row("IsRaw")))
            rtEdit.MinEntry = IIf(row("MinEntry") Is DBNull.Value OrElse row("MinEntry") = Nothing, String.Empty, TrimColumnSpace(row("MinEntry")))
            rtEdit.MaxEntry = IIf(row("MaxEntry") Is DBNull.Value OrElse row("MaxEntry") = Nothing, String.Empty, TrimColumnSpace(row("MaxEntry")))
            rtEdit.Multiplier = IIf(row("Multiplier") Is DBNull.Value OrElse row("Multiplier") = Nothing, String.Empty, TrimColumnSpace(row("Multiplier")))
            rtEdit.SerialAlias = IIf(row("SerialAlias") Is DBNull.Value OrElse row("SerialAlias") = Nothing, String.Empty, TrimColumnSpace(row("SerialAlias")))
            rtEdit.CreatekWhExportAssociateMeter = IIf(row("CreatekWhExportAssociateMeter") Is DBNull.Value OrElse row("CreatekWhExportAssociateMeter") = Nothing, String.Empty, TrimColumnSpace(row("CreatekWhExportAssociateMeter")))
            rtEdit.CreatekVarhExportAssociateMeter = IIf(row("CreatekVarhExportAssociateMeter") Is DBNull.Value OrElse row("CreatekVarhExportAssociateMeter") = Nothing, String.Empty, TrimColumnSpace(row("CreatekVarhExportAssociateMeter")))
            rtEdit.CreatekVarhAssociateMeter = IIf(row("CreatekVarhAssociateMeter") Is DBNull.Value OrElse row("CreatekVarhAssociateMeter") = Nothing, String.Empty, TrimColumnSpace(row("CreatekVarhAssociateMeter")))
            rtEdit.ExportMPAN = IIf(row("ExportMPAN") Is DBNull.Value OrElse row("ExportMPAN") = Nothing, String.Empty, TrimColumnSpace(row("ExportMPAN")))

            If (Not isSubmeter) Then

                rtEdit.MPAN = IIf(row("MPAN") Is DBNull.Value OrElse row("MPAN") = Nothing, String.Empty, TrimColumnSpace(row("MPAN")))
                rtEdit.AMR = IIf(row("AMR") Is DBNull.Value OrElse row("AMR") = Nothing, String.Empty, TrimColumnSpace(row("AMR")))
                rtEdit.AMRInstalledDate = IIf(row("AMRInstalledDate") Is DBNull.Value OrElse row("AMRInstalledDate") = Nothing, String.Empty, TrimColumnSpace(row("AMRInstalledDate")))
                rtEdit.CRCMeterType = IIf(row("CRCMeterType") Is DBNull.Value OrElse row("CRCMeterType") = Nothing, String.Empty, TrimColumnSpace(row("CRCMeterType")))
                rtEdit.CRCFuelType = IIf(row("CRCFuelType") Is DBNull.Value OrElse row("CRCFuelType") = Nothing, String.Empty, TrimColumnSpace(row("CRCFuelType")))
                rtEdit.IncludeAsCRCResidual = IIf(row("IncludeAsCRCResidual") Is DBNull.Value OrElse row("IncludeAsCRCResidual") = Nothing, String.Empty, TrimColumnSpace(row("IncludeAsCRCResidual")))
                rtEdit.CarbonTrustStandard = IIf(row("CarbonTrustStandard") Is DBNull.Value OrElse row("CarbonTrustStandard") = Nothing, String.Empty, TrimColumnSpace(row("CarbonTrustStandard")))
                rtEdit.CTSStartDate = IIf(row("CTSStartDate") Is DBNull.Value OrElse row("CTSStartDate") = Nothing, String.Empty, TrimColumnSpace(row("CTSStartDate")))
                rtEdit.CalorificValue = IIf(row("CalorificValue") Is DBNull.Value OrElse row("CalorificValue") = Nothing, String.Empty, TrimColumnSpace(row("CalorificValue")))
                rtEdit.DateInstalled = IIf(row("DateInstalled") Is DBNull.Value OrElse row("DateInstalled") = Nothing, String.Empty, TrimColumnSpace(row("DateInstalled")))
                rtEdit.DateCommissioned = IIf(row("DateCommissioned") Is DBNull.Value OrElse row("DateCommissioned") = Nothing, String.Empty, TrimColumnSpace(row("DateCommissioned")))
                rtEdit.IsStockMeter = IIf(row("IsStockMeter") Is DBNull.Value OrElse row("IsStockMeter") = Nothing, String.Empty, TrimColumnSpace(row("IsStockMeter")))
                rtEdit.Account_Number = IIf(row("AccountNumber").ToString() Is DBNull.Value OrElse row("AccountNumber").ToString() = Nothing, String.Empty, TrimColumnSpace(row("AccountNumber").ToString()))
                rtEdit.InvoiceFrequency = IIf(row("InvoiceFrequency") Is DBNull.Value OrElse row("InvoiceFrequency") = Nothing, String.Empty, TrimColumnSpace(row("InvoiceFrequency")))
                rtEdit.AuthorisedCapacity = IIf(row("AuthorisedCapacity") Is DBNull.Value OrElse row("AuthorisedCapacity") = Nothing, String.Empty, TrimColumnSpace(row("AuthorisedCapacity")))
                rtEdit.ExcessCapacity = IIf(row("ExcessCapacity") Is DBNull.Value OrElse row("ExcessCapacity") = Nothing, String.Empty, TrimColumnSpace(row("ExcessCapacity")))

                If ImportVersion <> "Vestigo" Then

                    rtEdit.DataCollector = IIf(row("DataCollector") Is DBNull.Value OrElse row("DataCollector") = Nothing, String.Empty, TrimColumnSpace(row("DataCollector")))
                    rtEdit.ESOSComplianceMethod = IIf(row("ESOSComplianceMethod") Is DBNull.Value OrElse row("ESOSComplianceMethod") = Nothing, String.Empty, TrimColumnSpace(row("ESOSComplianceMethod")))
                    rtEdit.TankSize = IIf(row("TankSize") Is DBNull.Value OrElse row("TankSize") = Nothing, String.Empty, TrimColumnSpace(row("TankSize")))
                    rtEdit.CRCConsumptionSource = IIf(row("CRCConsumptionSource") Is DBNull.Value OrElse row("CRCConsumptionSource") = Nothing, String.Empty, TrimColumnSpace(row("CRCConsumptionSource")))
                Else
                  
                    rtEdit.TimeZone = IIf(row("TimeZone") Is DBNull.Value OrElse row("TimeZone") = Nothing, String.Empty, TrimColumnSpace(row("TimeZone")))
                    rtEdit.EnableDayNightActivePeriod = IIf(row("EnableDayNightActivePeriod") Is DBNull.Value OrElse row("EnableDayNightActivePeriod") = Nothing, String.Empty, TrimColumnSpace(row("EnableDayNightActivePeriod")))
                    rtEdit.DayPeriod = IIf(row("DayPeriod") Is DBNull.Value OrElse row("DayPeriod") = Nothing, String.Empty, TrimColumnSpace(row("DayPeriod")))
                    rtEdit.NightPeriod = IIf(row("NightPeriod") Is DBNull.Value OrElse row("NightPeriod") = Nothing, String.Empty, TrimColumnSpace(row("NightPeriod")))

                End If
                rtEdit.MeterCategory = IIf(row("Category") Is DBNull.Value OrElse row("Category") = Nothing, String.Empty, TrimColumnSpace(row("Category")))
                rtEdit.Status = IIf(row("Status") Is DBNull.Value OrElse row("Status") = Nothing, String.Empty, TrimColumnSpace(row("Status")))
                rtEdit.MeterOperator = IIf(row("MeterOperator") Is DBNull.Value OrElse row("MeterOperator") = Nothing, String.Empty, TrimColumnSpace(row("MeterOperator")))
                rtEdit.MeterOperatorContact = IIf(row("MeterOperatorContact") Is DBNull.Value OrElse row("MeterOperatorContact") = Nothing, String.Empty, TrimColumnSpace(row("MeterOperatorContact")))
            End If
            rtEdit.ConsumptionSource = IIf(row("ConsumptionSource") Is DBNull.Value OrElse row("ConsumptionSource") = Nothing, String.Empty, TrimColumnSpace(row("ConsumptionSource")))
            If row.Table.Columns.Contains("ParentMeterSerial") Then 'And row.Table.Rows.IndexOf(row) = 0 Then

                rtEdit.ParentMeterSerial = IIf(row("ParentMeterSerial") Is DBNull.Value OrElse row("ParentMeterSerial") = Nothing, DBNull.Value.ToString(), row("ParentMeterSerial"))
                rtEdit.IsSubMeter = "Yes"

            Else

                rtEdit.ParentMeterSerial = IIf(row("CurrentSerialNumber") Is DBNull.Value OrElse row("CurrentSerialNumber") = Nothing, DBNull.Value.ToString(), row("CurrentSerialNumber"))
                rtEdit.IsSubMeter = "No"
            End If
        Catch ex As Exception

            rtEdit.StatusId = StatusEnum.Fail
            rtEdit.Message = "Fail to import data (fill meter edit edit)."

        End Try
    End Sub

    Private Shared Function TrimColumnSpace(ByVal columnValue As String) As String
        Return Convert.ToString(columnValue).Trim()
    End Function

End Class