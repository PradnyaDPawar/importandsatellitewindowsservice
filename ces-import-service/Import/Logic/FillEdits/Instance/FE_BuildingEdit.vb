﻿Public Class FE_BuildingEdit
    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)
        Dim rtEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Try

            rtEdit.ImportId = importEdit.Id
            rtEdit.StatusId = StatusEnum.In_Queue
            rtEdit.Database = importEdit.DatabaseName

            rtEdit.BuildingName = IIf(row("Name") Is DBNull.Value OrElse row("Name") = Nothing, String.Empty, row("Name"))
            rtEdit.Address1 = IIf(row("Address1") Is DBNull.Value OrElse row("Address1") = Nothing, String.Empty, row("Address1"))
            rtEdit.Address2 = IIf(row("Address2") Is DBNull.Value OrElse row("Address2") = Nothing, String.Empty, row("Address2"))
            rtEdit.Address3 = IIf(row("Address3") Is DBNull.Value OrElse row("Address3") = Nothing, String.Empty, row("Address3"))
            rtEdit.Address4 = IIf(row("Address4") Is DBNull.Value OrElse row("Address4") = Nothing, String.Empty, row("Address4"))
            rtEdit.Town = IIf(row("Town") Is DBNull.Value OrElse row("Town") = Nothing, String.Empty, row("Town"))
            rtEdit.PostCode = IIf(row("PostCode") Is DBNull.Value OrElse row("PostCode") = Nothing, String.Empty, row("PostCode"))
            rtEdit.CurrentUPRN = IIf(row("CurrentUPRN") Is DBNull.Value OrElse row("CurrentUPRN") = Nothing, String.Empty, row("CurrentUPRN"))
            rtEdit.NewUPRN = IIf(row("NewUPRN") Is DBNull.Value OrElse row("NewUPRN") = Nothing, String.Empty, row("NewUPRN"))
            rtEdit.CRCUndertaking = IIf(row("ParentOrganisation") Is DBNull.Value OrElse row("ParentOrganisation") = Nothing, String.Empty, row("ParentOrganisation"))
            rtEdit.Occupier = IIf(row("Occupier") Is DBNull.Value OrElse row("Occupier") = Nothing, String.Empty, row("Occupier"))
            rtEdit.Client = IIf(row("Client") Is DBNull.Value OrElse row("Client") = Nothing, String.Empty, row("Client"))
            rtEdit.Group = IIf(row("Group") Is DBNull.Value OrElse row("Group") = Nothing, String.Empty, row("Group"))

        Catch ex As Exception

            rtEdit.StatusId = StatusEnum.Fail
            rtEdit.Message = "Fail to import data (fill building edit edit)."

        End Try
    End Sub
End Class
