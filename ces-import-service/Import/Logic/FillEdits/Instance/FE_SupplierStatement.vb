﻿Public Class FE_SupplierStatement
    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) 'Implements IFillEditsDelegate.Fill

        Dim svEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Try

            svEdit.ImportId = importEdit.Id
            svEdit.StatusId = StatusEnum.In_Queue
            svEdit.Database = importEdit.DatabaseName

            svEdit.InvoiceNumber = IIf(row("InvoiceNumber") Is DBNull.Value OrElse row("InvoiceNumber") = Nothing, DBNull.Value, row("InvoiceNumber").ToString.Trim)
            svEdit.MPAN = IIf(row("MPAN") Is DBNull.Value OrElse row("MPAN") = Nothing, DBNull.Value, row("MPAN").ToString.Trim)
            svEdit.StartDate = IIf(row("StartDate") Is DBNull.Value OrElse row("StartDate") = Nothing, DBNull.Value.ToString, row("StartDate").ToString.Trim)
            svEdit.EndDate = IIf(row("EndDate") Is DBNull.Value OrElse row("EndDate") = Nothing, DBNull.Value.ToString, row("EndDate").ToString.Trim)
            svEdit.Estimated = row("Estimated")
            svEdit.kWh = row("kWh")
            svEdit.Description = row("Description")

        Catch ex As Exception

            svEdit.StatusId = StatusEnum.Fail
            svEdit.Message = "Fail to import data (fill ss edit)."
            svEdit.Message = ex.Message.ToString

        End Try

    End Sub
End Class
