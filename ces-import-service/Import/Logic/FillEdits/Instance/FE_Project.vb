﻿Public Class FE_Project

    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)

        Dim dsEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Try

            dsEdit.ImportId = importEdit.Id
            dsEdit.StatusId = StatusEnum.In_Queue
            dsEdit.Database = importEdit.DatabaseName

            dsEdit.UPRN = IIf(row("UPRN") Is DBNull.Value OrElse row("UPRN") = Nothing, String.Empty, row("UPRN").ToString.Trim)
            dsEdit.ProjectName = IIf(row("ProjectName") Is DBNull.Value OrElse row("ProjectName") = Nothing, String.Empty, row("ProjectName").ToString.Trim)
            dsEdit.Description = IIf(row("Description") Is DBNull.Value OrElse row("Description") = Nothing, String.Empty, row("Description").ToString.Trim)
            dsEdit.Rationale = IIf(row("Rationale") Is DBNull.Value OrElse row("Rationale") = Nothing, String.Empty, row("Rationale").ToString.Trim)
            dsEdit.Risks = IIf(row("Risks") Is DBNull.Value OrElse row("Risks") = Nothing, String.Empty, row("Risks").ToString.Trim)
            dsEdit.FuelType = IIf(row("FuelType") Is DBNull.Value OrElse row("FuelType") = Nothing, String.Empty, row("FuelType").ToString.Trim)
            dsEdit.AnnualEnergySaving = IIf(row("AnnualEnergySaving") Is DBNull.Value OrElse row("AnnualEnergySaving") = Nothing, String.Empty, row("AnnualEnergySaving").ToString.Trim)
            dsEdit.AnnualCarbon = IIf(row("AnnualCarbon") Is DBNull.Value OrElse row("AnnualCarbon") = Nothing, String.Empty, row("AnnualCarbon").ToString.Trim)
            dsEdit.AnnualCostSaving = IIf(row("AnnualCostSaving") Is DBNull.Value OrElse row("AnnualCostSaving") = Nothing, String.Empty, row("AnnualCostSaving").ToString.Trim)
            dsEdit.EstimatedCost = IIf(row("EstimatedCost") Is DBNull.Value OrElse row("EstimatedCost") = Nothing, String.Empty, row("EstimatedCost").ToString.Trim)
            dsEdit.ProjectSpecificUnitCost = IIf(row("ProjectSpecificUnitCost") Is DBNull.Value OrElse row("ProjectSpecificUnitCost") = Nothing, String.Empty, row("ProjectSpecificUnitCost").ToString.Trim)
            dsEdit.PaybackPeriod = IIf(row("PaybackPeriod") Is DBNull.Value OrElse row("PaybackPeriod") = Nothing, String.Empty, row("PaybackPeriod").ToString.Trim)
            dsEdit.LifeExpectancy = IIf(row("LifeExpectancy") Is DBNull.Value OrElse row("LifeExpectancy") = Nothing, String.Empty, row("LifeExpectancy").ToString.Trim)
            dsEdit.ProjectOwner = IIf(row("ProjectOwner") Is DBNull.Value OrElse row("ProjectOwner") = Nothing, String.Empty, row("ProjectOwner").ToString.Trim)
            dsEdit.OwnerRole = IIf(row("OwnerRole") Is DBNull.Value OrElse row("OwnerRole") = Nothing, String.Empty, row("OwnerRole").ToString.Trim)
            dsEdit.ProjectStartDate = IIf(row("ProjectStartDate") Is DBNull.Value OrElse row("ProjectStartDate") = Nothing, String.Empty, row("ProjectStartDate").ToString.Trim)
            dsEdit.ProjectCompletionDate = IIf(row("ProjectCompletionDate") Is DBNull.Value OrElse row("ProjectCompletionDate") = Nothing, String.Empty, row("ProjectCompletionDate").ToString.Trim)
            dsEdit.BaselineStartDate = IIf(row("BaselineStartDate") Is DBNull.Value OrElse row("BaselineStartDate") = Nothing, String.Empty, row("BaselineStartDate").ToString.Trim)
            dsEdit.BaselineEndDate = IIf(row("BaselineEndDate") Is DBNull.Value OrElse row("BaselineEndDate") = Nothing, String.Empty, row("BaselineEndDate").ToString.Trim)
            dsEdit.CurrentStatus = IIf(row("CurrentStatus") Is DBNull.Value OrElse row("CurrentStatus") = Nothing, String.Empty, row("CurrentStatus").ToString.Trim)
            dsEdit.TechnologyGroup = IIf(row("TechnologyGroup") Is DBNull.Value OrElse row("TechnologyGroup") = Nothing, String.Empty, row("TechnologyGroup").ToString.Trim)
            dsEdit.MainTechnology = IIf(row("MainTechnology") Is DBNull.Value OrElse row("MainTechnology") = Nothing, String.Empty, row("MainTechnology").ToString.Trim)
            dsEdit.SubTechnology = IIf(row("SubTechnology") Is DBNull.Value OrElse row("SubTechnology") = Nothing, String.Empty, row("SubTechnology").ToString.Trim)
            dsEdit.TypeOfAction = IIf(row("TypeOfAction") Is DBNull.Value OrElse row("TypeOfAction") = Nothing, String.Empty, row("TypeOfAction").ToString.Trim)
            dsEdit.ImplementationSteps = IIf(row("ImplementationSteps") Is DBNull.Value OrElse row("ImplementationSteps") = Nothing, String.Empty, row("ImplementationSteps").ToString.Trim)
            dsEdit.RelatedInformation = IIf(row("RelatedInformation") Is DBNull.Value OrElse row("RelatedInformation") = Nothing, String.Empty, row("RelatedInformation").ToString.Trim)
            dsEdit.RelatedWebLink1 = IIf(row("RelatedWebLink1") Is DBNull.Value OrElse row("RelatedWebLink1") = Nothing, String.Empty, row("RelatedWebLink1").ToString.Trim)
            dsEdit.RelatedWebLink2 = IIf(row("RelatedWebLink2") Is DBNull.Value OrElse row("RelatedWebLink2") = Nothing, String.Empty, row("RelatedWebLink2").ToString.Trim)

        Catch ex As Exception

            dsEdit.StatusId = StatusEnum.Fail
            dsEdit.Message = "Fail to import data (fill project edit)."
            dsEdit.Message = ex.Message.ToString

        End Try

    End Sub


End Class
