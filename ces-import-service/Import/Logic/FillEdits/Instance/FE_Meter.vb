﻿Public Class FE_Meter
    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit, ByRef isSubmeter As Boolean)

        Dim rtEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Dim deconfig As DeConfig = New DeConfig()
        Dim ImportVersion As String = deconfig.GetConfig().ImportVersion

        Try

            rtEdit.ImportId = importEdit.Id
            rtEdit.StatusId = StatusEnum.In_Queue
            rtEdit.Database = importEdit.DatabaseName

            rtEdit.BuildingUPRN = IIf(row("BuildingUPRN") Is DBNull.Value OrElse row("BuildingUPRN") = Nothing, DBNull.Value.ToString(), row("BuildingUPRN"))
            rtEdit.MeterName = IIf(row("MeterName") Is DBNull.Value OrElse row("MeterName") = Nothing, DBNull.Value.ToString(), row("MeterName"))
            rtEdit.Serial_Number = IIf(row("SerialNumber") Is DBNull.Value OrElse row("SerialNumber") = Nothing, DBNull.Value.ToString(), row("SerialNumber"))
            rtEdit.Units = IIf(row("Units") Is DBNull.Value OrElse row("Units") = Nothing, DBNull.Value.ToString(), row("Units"))
            rtEdit.ReportingMeter = IIf(row("ReportingMeter") Is DBNull.Value OrElse row("ReportingMeter") = Nothing, DBNull.Value.ToString(), GetRowValue(row("ReportingMeter").ToString.Trim))
            rtEdit.OperatingSchedule1 = IIf(row("OperatingSchedule1") Is DBNull.Value OrElse row("OperatingSchedule1") = Nothing, DBNull.Value.ToString(), row("OperatingSchedule1"))
            rtEdit.OperatingSchedule2 = IIf(row("OperatingSchedule2") Is DBNull.Value OrElse row("OperatingSchedule2") = Nothing, DBNull.Value.ToString(), row("OperatingSchedule2"))
            rtEdit.ConsumptionSource = IIf(row("ConsumptionSource") Is DBNull.Value OrElse row("ConsumptionSource") = Nothing, String.Empty, row("ConsumptionSource"))
            rtEdit.Location = IIf(row("Location") Is DBNull.Value OrElse row("Location") = Nothing, DBNull.Value.ToString(), row("Location"))
            rtEdit.Description = IIf(row("Description") Is DBNull.Value OrElse row("Description") = Nothing, DBNull.Value.ToString(), row("Description"))
            rtEdit.CorrectionFactor = IIf(row("CorrectionFactor") Is DBNull.Value OrElse row("CorrectionFactor") = Nothing, DBNull.Value.ToString(), row("CorrectionFactor"))
            rtEdit.ConversionFactor = IIf(row("ConversionFactor") Is DBNull.Value OrElse row("ConversionFactor") = Nothing, DBNull.Value.ToString(), row("ConversionFactor"))
            rtEdit.CustomMultiplier = IIf(row("CustomMultiplier") Is DBNull.Value OrElse row("CustomMultiplier") = Nothing, DBNull.Value.ToString(), row("CustomMultiplier"))
            rtEdit.ValidateReadings = IIf(row("ValidateReadings") Is DBNull.Value OrElse row("ValidateReadings") = Nothing, DBNull.Value.ToString(), GetRowValue(row("ValidateReadings").ToString.Trim))
            rtEdit.ReadingWrapRound = IIf(row("ReadingWrapRound") Is DBNull.Value OrElse row("ReadingWrapRound") = Nothing, DBNull.Value.ToString(), row("ReadingWrapRound"))
            rtEdit.MaxReadConsumption = IIf(row("MaxReadConsumption") Is DBNull.Value OrElse row("MaxReadConsumption") = Nothing, DBNull.Value.ToString(), row("MaxReadConsumption"))
            rtEdit.RealTime = IIf(row("RealTime") Is DBNull.Value OrElse row("RealTime") = Nothing, DBNull.Value.ToString(), GetRowValue(row("RealTime").ToString.Trim))
            rtEdit.WrapRoundValue = IIf(row("WrapRoundValue") Is DBNull.Value OrElse row("WrapRoundValue") = Nothing, DBNull.Value.ToString(), row("WrapRoundValue"))
            rtEdit.IsCumulative = IIf(row("IsCumulative") Is DBNull.Value OrElse row("IsCumulative") = Nothing, DBNull.Value.ToString(), GetRowValue(row("IsCumulative").ToString.Trim))
            rtEdit.IsRaw = IIf(row("IsRaw") Is DBNull.Value OrElse row("IsRaw") = Nothing, DBNull.Value.ToString(), GetRowValue(row("IsRaw").ToString.Trim))
            rtEdit.MinEntry = IIf(row("MinEntry") Is DBNull.Value OrElse row("MinEntry") = Nothing, DBNull.Value.ToString(), row("MinEntry"))
            rtEdit.MaxEntry = IIf(row("MaxEntry") Is DBNull.Value OrElse row("MaxEntry") = Nothing, DBNull.Value.ToString(), row("MaxEntry"))
            rtEdit.Multiplier = IIf(row("Multiplier") Is DBNull.Value OrElse row("Multiplier") = Nothing, DBNull.Value.ToString(), row("Multiplier"))
            rtEdit.SerialAlias = IIf(row("SerialAlias") Is DBNull.Value OrElse row("SerialAlias") = Nothing, DBNull.Value.ToString(), row("SerialAlias"))

            rtEdit.CreatekWhExportAssociateMeter = IIf(row("CreatekWhExportAssociateMeter") Is DBNull.Value OrElse row("CreatekWhExportAssociateMeter") = Nothing, DBNull.Value.ToString(), GetRowValue(row("CreatekWhExportAssociateMeter").ToString.Trim))
            rtEdit.CreatekVarhExportAssociateMeter = IIf(row("CreatekVarhExportAssociateMeter") Is DBNull.Value OrElse row("CreatekVarhExportAssociateMeter") = Nothing, DBNull.Value.ToString(), GetRowValue(row("CreatekVarhExportAssociateMeter").ToString.Trim))
            rtEdit.CreatekVarhAssociateMeter = IIf(row("CreatekVarhAssociateMeter") Is DBNull.Value OrElse row("CreatekVarhAssociateMeter") = Nothing, DBNull.Value.ToString(), GetRowValue(row("CreatekVarhAssociateMeter").ToString.Trim))
            rtEdit.ExportMPAN = IIf(row("ExportMPAN") Is DBNull.Value OrElse row("ExportMPAN") = Nothing, DBNull.Value.ToString(), row("ExportMPAN"))

            If (Not isSubmeter) Then

                rtEdit.MPAN = IIf(row("MPAN") Is DBNull.Value OrElse row("MPAN") = Nothing, DBNull.Value.ToString(), row("MPAN"))
                rtEdit.MeterFuel = IIf(row("MeterFuel") Is DBNull.Value OrElse row("MeterFuel") = Nothing, DBNull.Value.ToString(), row("MeterFuel"))
                rtEdit.AMR = IIf(row("AMR") Is DBNull.Value OrElse row("AMR") = Nothing, DBNull.Value.ToString(), GetRowValue(row("AMR").ToString.Trim))
                rtEdit.AMRInstalledDate = IIf(row("AMRInstalledDate") Is DBNull.Value OrElse row("AMRInstalledDate") = Nothing, DBNull.Value.ToString(), row("AMRInstalledDate"))
                rtEdit.IsActive = IIf(row("IsActive") Is DBNull.Value OrElse row("IsActive") = Nothing, DBNull.Value.ToString(), GetRowValue(row("IsActive").ToString.Trim))
                rtEdit.CRCMeterType = IIf(row("CRCMeterType") Is DBNull.Value OrElse row("CRCMeterType") = Nothing, DBNull.Value.ToString(), row("CRCMeterType"))
                rtEdit.CRCFuelType = IIf(row("CRCFuelType") Is DBNull.Value OrElse row("CRCFuelType") = Nothing, DBNull.Value.ToString(), row("CRCFuelType"))
                rtEdit.IncludeAsCRCResidual = IIf(row("IncludeAsCRCResidual") Is DBNull.Value OrElse row("IncludeAsCRCResidual") = Nothing, DBNull.Value.ToString(), GetRowValue(row("IncludeAsCRCResidual").ToString.Trim))
                rtEdit.CarbonTrustStandard = IIf(row("CarbonTrustStandard") Is DBNull.Value OrElse row("CarbonTrustStandard") = Nothing, DBNull.Value.ToString(), GetRowValue(row("CarbonTrustStandard").ToString.Trim))
                rtEdit.CTSStartDate = IIf(row("CTSStartDate") Is DBNull.Value OrElse row("CTSStartDate") = Nothing, DBNull.Value.ToString(), row("CTSStartDate"))
                rtEdit.InvoiceFrequency = IIf(row("InvoiceFrequency") Is DBNull.Value OrElse row("InvoiceFrequency") = Nothing, DBNull.Value.ToString(), row("InvoiceFrequency"))
                rtEdit.AuthorisedCapacity = IIf(row("AuthorisedCapacity") Is DBNull.Value OrElse row("AuthorisedCapacity") = Nothing, DBNull.Value.ToString(), row("AuthorisedCapacity"))
                rtEdit.ExcessCapacity = IIf(row("ExcessCapacity") Is DBNull.Value OrElse row("ExcessCapacity") = Nothing, DBNull.Value.ToString(), row("ExcessCapacity"))
                rtEdit.Account_Number = IIf(row("AccountNumber").ToString() Is DBNull.Value OrElse row("AccountNumber").ToString() = Nothing, DBNull.Value.ToString(), row("AccountNumber").ToString())
                rtEdit.CalorificValue = IIf(row("CalorificValue") Is DBNull.Value OrElse row("CalorificValue") = Nothing, DBNull.Value.ToString(), row("CalorificValue"))
                rtEdit.DateInstalled = IIf(row("DateInstalled") Is DBNull.Value OrElse row("DateInstalled") = Nothing, DBNull.Value.ToString(), row("DateInstalled"))
                rtEdit.DateCommissioned = IIf(row("DateCommissioned") Is DBNull.Value OrElse row("DateCommissioned") = Nothing, DBNull.Value.ToString(), row("DateCommissioned"))
                rtEdit.IsStockMeter = IIf(row("IsStockMeter") Is DBNull.Value OrElse row("IsStockMeter") = Nothing, DBNull.Value.ToString(), GetRowValue(row("IsStockMeter").ToString.Trim))
                rtEdit.TankSize = IIf(row("TankSize") Is DBNull.Value OrElse row("TankSize") = Nothing, String.Empty, row("TankSize"))

                If ImportVersion = "Professional" Then

                    rtEdit.DataCollector = IIf(row("DataCollector") Is DBNull.Value OrElse row("DataCollector") = Nothing, String.Empty, row("DataCollector"))
                    rtEdit.ESOSComplianceMethod = IIf(row("ESOSComplianceMethod") Is DBNull.Value OrElse row("ESOSComplianceMethod") = Nothing, String.Empty, row("ESOSComplianceMethod"))
                End If

                If ImportVersion = "Vestigo" Then

                    rtEdit.TimeZone = IIf(row("TimeZone") Is DBNull.Value OrElse row("TimeZone") = Nothing, String.Empty, row("TimeZone"))
                    rtEdit.EnableDayNightActivePeriod = IIf(row("EnableDayNightActivePeriod") Is DBNull.Value OrElse row("EnableDayNightActivePeriod") = Nothing, String.Empty, row("EnableDayNightActivePeriod"))
                    rtEdit.DayPeriod = IIf(row("DayPeriod") Is DBNull.Value OrElse row("DayPeriod") = Nothing, String.Empty, row("DayPeriod"))
                    rtEdit.NightPeriod = IIf(row("NightPeriod") Is DBNull.Value OrElse row("NightPeriod") = Nothing, String.Empty, row("NightPeriod"))
                    rtEdit.ConsumptionSource = IIf(row("ConsumptionSource") Is DBNull.Value OrElse row("ConsumptionSource") = Nothing, String.Empty, row("ConsumptionSource"))

                End If

                rtEdit.CRCConsumptionSource = IIf(row("CRCConsumptionSource") Is DBNull.Value OrElse row("CRCConsumptionSource") = Nothing, String.Empty, row("CRCConsumptionSource"))
                rtEdit.MeterCategory = IIf(row("Category") Is DBNull.Value OrElse row("Category") = Nothing, String.Empty, row("Category"))
                rtEdit.Status = IIf(row("Status") Is DBNull.Value OrElse row("Status") = Nothing, String.Empty, row("Status"))
                rtEdit.MeterOperator = IIf(row("MeterOperator") Is DBNull.Value OrElse row("MeterOperator") = Nothing, String.Empty, row("MeterOperator"))
                rtEdit.MeterOperatorContact = IIf(row("MeterOperatorContact") Is DBNull.Value OrElse row("MeterOperatorContact") = Nothing, String.Empty, row("MeterOperatorContact"))

            End If

            If row.Table.Columns.Contains("ParentMeterSerial") Then 'And row.Table.Rows.IndexOf(row) = 0 Then
                rtEdit.ParentMeterSerial = IIf(row("ParentMeterSerial") Is DBNull.Value OrElse row("ParentMeterSerial") = Nothing, DBNull.Value.ToString(), row("ParentMeterSerial"))
                rtEdit.IsSubMeter = "Yes"
            Else
                rtEdit.ParentMeterSerial = IIf(row("SerialNumber") Is DBNull.Value OrElse row("SerialNumber") = Nothing, DBNull.Value.ToString(), row("SerialNumber"))
                rtEdit.IsSubMeter = "No"
            End If

        Catch ex As Exception

            rtEdit.StatusId = StatusEnum.Fail
            rtEdit.Message = "Fail to import data (fill meter edit)."

        End Try
    End Sub

    Public Shared Function GetRowValue(ByVal rowvalue As String) As String
        Dim value As String

        If rowvalue.ToLower.Trim = "yes" Or rowvalue.ToLower.Trim = "true" Or rowvalue.ToLower.Trim = "1" Then
            value = "Yes"
        ElseIf rowvalue.ToLower.Trim = "no" Or rowvalue.ToLower.Trim = "false" Or rowvalue.ToLower.Trim = "0" Then
            value = "No"
        Else
            value = rowvalue

        End If
        Return value
    End Function
End Class