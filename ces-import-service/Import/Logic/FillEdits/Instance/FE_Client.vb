﻿Public Class FE_Client

    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) 'Implements IFillEditsDelegate.Fill

        Dim svEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Try

            svEdit.ImportId = importEdit.Id
            svEdit.StatusId = StatusEnum.In_Queue
            svEdit.Database = importEdit.DatabaseName

            svEdit.ClientName = IIf(row("Name") Is DBNull.Value OrElse row("Name") = Nothing, DBNull.Value, row("Name").ToString.Trim)

        Catch ex As Exception

            svEdit.StatusId = StatusEnum.Fail
            svEdit.Message = "Fail to import data (fill client edit)."
            svEdit.Message = ex.Message.ToString

        End Try

    End Sub


End Class
