﻿Public Class FE_AssetCode
    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit)
        Dim rtEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Dim deconfig As DeConfig = New DeConfig()
        Dim ImportVersion As String = deconfig.GetConfig().ImportVersion



        rtEdit.ImportId = importEdit.Id
        rtEdit.StatusId = StatusEnum.In_Queue
        rtEdit.Database = importEdit.DatabaseName

        rtEdit.AssetCode = IIf(row("code") Is DBNull.Value OrElse row("code") = Nothing, DBNull.Value.ToString(), row("code"))
        rtEdit.AssetCodeUprn = IIf(row("uprn") Is DBNull.Value OrElse row("uprn") = Nothing, DBNull.Value.ToString(), row("uprn"))
        rtEdit.AssetCodeStartDate = IIf(row("startdate") Is DBNull.Value OrElse row("startdate") = Nothing, DBNull.Value.ToString(), row("startdate"))
        rtEdit.AssetCodeEndDate = IIf(row("enddate") Is DBNull.Value OrElse row("enddate") = Nothing, DBNull.Value.ToString(), row("enddate"))
        rtEdit.AssetCodeName = IIf(row("codename") Is DBNull.Value OrElse row("codename") = Nothing, DBNull.Value.ToString(), row("codename"))

    End Sub
End Class
