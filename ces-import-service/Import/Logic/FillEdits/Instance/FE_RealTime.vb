﻿Public Class FE_RealTime




    Public Shared Sub Fill(ByRef row As System.Data.DataRow, ByRef dsEdits As ImportDataStagingEdits, ByRef importEdit As ImportEdit) 'Implements IFillEditsDelegate.Fill

        Dim rtEdit As ImportDataStagingEdit = dsEdits.NewEdit()

        Try

            rtEdit.ImportId = importEdit.Id
            rtEdit.StatusId = StatusEnum.In_Queue
            rtEdit.Database = importEdit.DatabaseName

            rtEdit.Meter_Name = row("MeterName")
            rtEdit.DateTime = row("DateTime")
            rtEdit.CountUnits = row("CountUnits").ToString.Replace(",", "")

        Catch ex As Exception

            rtEdit.StatusId = StatusEnum.Fail
            rtEdit.Message = "Fail to import data (fill rt edit)."

        End Try

    End Sub
End Class
