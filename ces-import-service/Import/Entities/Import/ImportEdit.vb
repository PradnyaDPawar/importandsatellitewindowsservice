﻿
Imports Framework

Public Class ImportEdit
    Inherits BaseEdit

    Property Id() As Integer
        Get
            Return GetItem("Id")
        End Get
        Set(ByVal value As Integer)
            _itemRow("Id") = value
        End Set
    End Property

    Property Name() As String
        Get
            Return GetItem("Name")
        End Get
        Set(ByVal value As String)
            _itemRow("Name") = value
        End Set
    End Property

    Property DatabaseName() As String
        Get
            Return GetItem("DataBaseName")

        End Get
        Set(ByVal value As String)
            _itemRow("DataBaseName") = value

        End Set
    End Property

    Property StatusId() As Integer
        Get
            Return GetItem("StatusId")
        End Get
        Set(ByVal value As Integer)
            _itemRow("StatusId") = value

        End Set
    End Property

    Property Format() As String
        Get
            Return GetItem("Format")
        End Get
        Set(ByVal value As String)
            _itemRow("Format") = value

        End Set
    End Property

    Property DeType() As String
        Get
            Return GetItem("DeType")
        End Get
        Set(ByVal value As String)
            _itemRow("DeType") = value
        End Set
    End Property


    Property DeTypeId() As Integer
        Get
            Return GetItem("DeTypeId")
        End Get
        Set(ByVal value As Integer)
            _itemRow("DeTypeId") = value
        End Set
    End Property

    ReadOnly Property FileExtension() As String
        Get
            Return IO.Path.GetExtension(GetItem("Name"))
        End Get
    End Property

    Property Message() As String
        Get
            Return GetItem("Message")
        End Get
        Set(ByVal value As String)
            _itemRow("Message") = value
        End Set
    End Property

    Property TotalRow() As String
        Get
            Return GetItem("TotalRow")
        End Get
        Set(ByVal value As String)
            _itemRow("TotalRow") = value
        End Set
    End Property

    'ReadOnly Property ImportTypeId() As Integer
    '    Get
    '        Return GetItem("ImportTypeId")
    '    End Get
    'End Property

    Property UploadedBy() As String
        Get
            Return GetItem("UploadedBy")
        End Get
        Set(ByVal value As String)
            _itemRow("UploadedBy") = value
        End Set
    End Property




    'ImportType, ExpiryDate, UploadedBy, UploadedOn, FileStatus, TotalRow, ErrorRow

End Class
