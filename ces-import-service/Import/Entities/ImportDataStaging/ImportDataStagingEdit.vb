﻿
Public Class ImportDataStagingEdit
    Inherits BaseEdit

#Region " System "

    Property ImportId() As Integer
        Get
            Return GetItem("ImportId")
        End Get
        Set(ByVal value As Integer)
            _itemRow("ImportId") = value
        End Set
    End Property

    Property DeTypeId() As Integer
        Get
            Return GetItem("DeTypeId")
        End Get
        Set(ByVal value As Integer)
            _itemRow("DeTypeId") = value
        End Set
    End Property

    Property StatusId() As Integer
        Get
            Return GetItem("StatusId")
        End Get
        Set(ByVal value As Integer)
            _itemRow("StatusId") = value
        End Set
    End Property

    Property Message() As String
        Get
            Return GetItem("Message")
        End Get
        Set(ByVal value As String)
            _itemRow("Message") = value
        End Set
    End Property

    Property Database() As String
        Get
            Return GetItem("Database")
        End Get
        Set(ByVal value As String)
            _itemRow("Database") = value
        End Set
    End Property

#End Region

#Region " Find Meter "


    WriteOnly Property MPAN() As Object
        Set(ByVal value As Object)
            _itemRow("MPAN") = value
        End Set
    End Property


    WriteOnly Property Meter_Serial() As Object
        Set(ByVal value As Object)
            _itemRow("Meter Serial") = value
        End Set
    End Property

    WriteOnly Property Meter_Name() As Object
        Set(ByVal value As Object)
            _itemRow("Meter Name") = value
        End Set
    End Property

#End Region

#Region " Date "

    Property DateTime() As String
        Get
            Return GetItem("DateTime")
        End Get
        Set(ByVal value As String)
            _itemRow("DateTime") = value
        End Set
    End Property

    Property StartDate() As String
        Get
            Return GetItem("StartDate")
        End Get
        Set(ByVal value As String)
            _itemRow("StartDate") = value
        End Set
    End Property

    Property EndDate() As String
        Get
            Return GetItem("EndDate")
        End Get
        Set(ByVal value As String)
            _itemRow("EndDate") = value
        End Set
    End Property


#End Region

#Region " Consumption "

    Property Force_kWh() As String
        Get
            Return GetItem("Force kWh")
        End Get
        Set(ByVal value As String)
            _itemRow("Force kWh") = value
        End Set
    End Property

    WriteOnly Property hh0000() As Object
        Set(ByVal value As Object)
            _itemRow("00:00") = value
        End Set
    End Property

    WriteOnly Property hh0030() As Object
        Set(ByVal value As Object)
            _itemRow("00:30") = value
        End Set
    End Property

    WriteOnly Property hh0100() As Object
        Set(ByVal value As Object)
            _itemRow("01:00") = value
        End Set
    End Property

    WriteOnly Property hh0130() As Object
        Set(ByVal value As Object)
            _itemRow("01:30") = value
        End Set
    End Property

    WriteOnly Property hh0200() As Object
        Set(ByVal value As Object)
            _itemRow("02:00") = value
        End Set
    End Property

    WriteOnly Property hh0230() As Object
        Set(ByVal value As Object)
            _itemRow("02:30") = value
        End Set
    End Property

    WriteOnly Property hh0300() As Object
        Set(ByVal value As Object)
            _itemRow("03:00") = value
        End Set
    End Property

    WriteOnly Property hh0330() As Object
        Set(ByVal value As Object)
            _itemRow("03:30") = value
        End Set
    End Property

    WriteOnly Property hh0400() As Object
        Set(ByVal value As Object)
            _itemRow("04:00") = value
        End Set
    End Property

    WriteOnly Property hh0430() As Object
        Set(ByVal value As Object)
            _itemRow("04:30") = value
        End Set
    End Property

    WriteOnly Property hh0500() As Object
        Set(ByVal value As Object)
            _itemRow("05:00") = value
        End Set
    End Property

    WriteOnly Property hh0530() As Object
        Set(ByVal value As Object)
            _itemRow("05:30") = value
        End Set
    End Property

    WriteOnly Property hh0600() As Object
        Set(ByVal value As Object)
            _itemRow("06:00") = value
        End Set
    End Property

    WriteOnly Property hh0630() As Object
        Set(ByVal value As Object)
            _itemRow("06:30") = value
        End Set
    End Property

    WriteOnly Property hh0700() As Object
        Set(ByVal value As Object)
            _itemRow("07:00") = value
        End Set
    End Property

    WriteOnly Property hh0730() As Object
        Set(ByVal value As Object)
            _itemRow("07:30") = value
        End Set
    End Property

    WriteOnly Property hh0800() As Object
        Set(ByVal value As Object)
            _itemRow("08:00") = value
        End Set
    End Property

    WriteOnly Property hh0830() As Object
        Set(ByVal value As Object)
            _itemRow("08:30") = value
        End Set
    End Property

    WriteOnly Property hh0900() As Object
        Set(ByVal value As Object)
            _itemRow("09:00") = value
        End Set
    End Property

    WriteOnly Property hh0930() As Object
        Set(ByVal value As Object)
            _itemRow("09:30") = value
        End Set
    End Property

    WriteOnly Property hh1000() As Object
        Set(ByVal value As Object)
            _itemRow("10:00") = value
        End Set
    End Property

    WriteOnly Property hh1030() As Object
        Set(ByVal value As Object)
            _itemRow("10:30") = value
        End Set
    End Property

    WriteOnly Property hh1100() As Object
        Set(ByVal value As Object)
            _itemRow("11:00") = value
        End Set
    End Property

    WriteOnly Property hh1130() As Object
        Set(ByVal value As Object)
            _itemRow("11:30") = value
        End Set
    End Property

    WriteOnly Property hh1200() As Object
        Set(ByVal value As Object)
            _itemRow("12:00") = value
        End Set
    End Property

    WriteOnly Property hh1230() As Object
        Set(ByVal value As Object)
            _itemRow("12:30") = value
        End Set
    End Property

    WriteOnly Property hh1300() As Object
        Set(ByVal value As Object)
            _itemRow("13:00") = value
        End Set
    End Property

    WriteOnly Property hh1330() As Object
        Set(ByVal value As Object)
            _itemRow("13:30") = value
        End Set
    End Property

    WriteOnly Property hh1400() As Object
        Set(ByVal value As Object)
            _itemRow("14:00") = value
        End Set
    End Property

    WriteOnly Property hh1430() As Object
        Set(ByVal value As Object)
            _itemRow("14:30") = value
        End Set
    End Property

    WriteOnly Property hh1500() As Object
        Set(ByVal value As Object)
            _itemRow("15:00") = value
        End Set
    End Property

    WriteOnly Property hh1530() As Object
        Set(ByVal value As Object)
            _itemRow("15:30") = value
        End Set
    End Property

    WriteOnly Property hh1600() As Object
        Set(ByVal value As Object)
            _itemRow("16:00") = value
        End Set
    End Property

    WriteOnly Property hh1630() As Object
        Set(ByVal value As Object)
            _itemRow("16:30") = value
        End Set
    End Property

    WriteOnly Property hh1700() As Object
        Set(ByVal value As Object)
            _itemRow("17:00") = value
        End Set
    End Property

    WriteOnly Property hh1730() As Object
        Set(ByVal value As Object)
            _itemRow("17:30") = value
        End Set
    End Property

    WriteOnly Property hh1800() As Object
        Set(ByVal value As Object)
            _itemRow("18:00") = value
        End Set
    End Property

    WriteOnly Property hh1830() As Object
        Set(ByVal value As Object)
            _itemRow("18:30") = value
        End Set
    End Property

    WriteOnly Property hh1900() As Object
        Set(ByVal value As Object)
            _itemRow("19:00") = value
        End Set
    End Property

    WriteOnly Property hh1930() As Object
        Set(ByVal value As Object)
            _itemRow("19:30") = value
        End Set
    End Property

    WriteOnly Property hh2000() As Object
        Set(ByVal value As Object)
            _itemRow("20:00") = value
        End Set
    End Property

    WriteOnly Property hh2030() As Object
        Set(ByVal value As Object)
            _itemRow("20:30") = value
        End Set
    End Property

    WriteOnly Property hh2100() As Object
        Set(ByVal value As Object)
            _itemRow("21:00") = value
        End Set
    End Property

    WriteOnly Property hh2130() As Object
        Set(ByVal value As Object)
            _itemRow("21:30") = value
        End Set
    End Property

    WriteOnly Property hh2200() As Object
        Set(ByVal value As Object)
            _itemRow("22:00") = value
        End Set
    End Property

    WriteOnly Property hh2230() As Object
        Set(ByVal value As Object)
            _itemRow("22:30") = value
        End Set
    End Property

    WriteOnly Property hh2300() As Object
        Set(ByVal value As Object)
            _itemRow("23:00") = value
        End Set
    End Property

    WriteOnly Property hh2330() As Object
        Set(ByVal value As Object)
            _itemRow("23:30") = value
        End Set
    End Property

#End Region

#Region "Static Variable"

    WriteOnly Property StaticVariableLevel() As Object
        Set(ByVal value As Object)
            _itemRow("Level") = value
        End Set
    End Property

    WriteOnly Property StaticVariableName() As Object
        Set(ByVal value As Object)
            _itemRow("Name") = value
        End Set
    End Property

    WriteOnly Property BuildingMeterSerial() As Object
        Set(ByVal value As Object)
            _itemRow("Building_MeterSerial") = value
        End Set
    End Property

    WriteOnly Property StaticVariableValue() As Object
        Set(ByVal value As Object)
            _itemRow("Value") = value
        End Set
    End Property

    WriteOnly Property StaticVariableUnit() As Object
        Set(ByVal value As Object)
            _itemRow("Unit") = value
        End Set
    End Property

    WriteOnly Property StaticVariableKwhUnit() As Object
        Set(ByVal value As Object)
            _itemRow("KwhUnit") = value
        End Set
    End Property

#End Region

#Region "Client"

    WriteOnly Property ClientName() As Object
        Set(ByVal value As Object)
            _itemRow("ClientName") = value
        End Set
    End Property
#End Region

#Region "Invoice"

    WriteOnly Property InvoiceNumber() As Object
        Set(ByVal value As Object)
            _itemRow("InvoiceNumber") = value
        End Set
    End Property

    WriteOnly Property Tariff() As Object
        Set(ByVal value As Object)
            _itemRow("Tariff") = value
        End Set
    End Property

    WriteOnly Property StartRead() As Object
        Set(ByVal value As Object)
            _itemRow("StartRead") = value
        End Set
    End Property

    WriteOnly Property StartReadEstimated() As Object
        Set(ByVal value As Object)
            _itemRow("StartReadEstimated") = value
        End Set
    End Property

    WriteOnly Property EndRead() As Object
        Set(ByVal value As Object)
            _itemRow("EndRead") = value
        End Set
    End Property

    WriteOnly Property EndReadEstimated() As Object
        Set(ByVal value As Object)
            _itemRow("EndReadEstimated") = value
        End Set
    End Property

    WriteOnly Property Consumption() As Object
        Set(ByVal value As Object)
            _itemRow("Consumption") = value
        End Set
    End Property

    WriteOnly Property ForcekWh() As Object
        Set(ByVal value As Object)
            _itemRow("ForcekWh") = value
        End Set
    End Property

    WriteOnly Property InvoiceRate() As Object
        Set(ByVal value As Object)
            _itemRow("InvoiceRate") = value
        End Set
    End Property

    WriteOnly Property CostUnit() As Object
        Set(ByVal value As Object)
            _itemRow("CostUnit") = value
        End Set
    End Property

    WriteOnly Property Vat() As Object
        Set(ByVal value As Object)
            _itemRow("Vat") = value
        End Set
    End Property

    WriteOnly Property Net() As Object
        Set(ByVal value As Object)
            _itemRow("Net") = value
        End Set
    End Property

    WriteOnly Property Gross() As Object
        Set(ByVal value As Object)
            _itemRow("Gross") = value
        End Set
    End Property

    WriteOnly Property CostOnly() As Object
        Set(ByVal value As Object)
            _itemRow("CostOnly") = value
        End Set
    End Property

    WriteOnly Property TaxPointDate() As Object

        Set(ByVal value As Object)
            _itemRow("TaxPointDate") = value
        End Set
    End Property

    WriteOnly Property IsLastDayApportion() As Object
        Set(ByVal value As Object)
            _itemRow("IsLastDayApportion") = value
        End Set
    End Property

    WriteOnly Property IsTransmissionTariff() As Object
        Set(ByVal value As Object)
            _itemRow("IsTransmissionTariff") = value
        End Set
    End Property

#End Region

#Region "Advanced BuildIng"

    Property UPRN() As String
        Get
            Return GetItem("UPRN")
        End Get
        Set(ByVal value As String)
            _itemRow("UPRN") = Truncate(value, 255)
        End Set
    End Property

    Property BuildingOwner() As String
        Get
            Return GetItem("BuildingOwner")
        End Get
        Set(ByVal value As String)
            _itemRow("BuildingOwner") = Truncate(value, 51)
        End Set
    End Property

    Property Tenant() As String
        Get
            Return GetItem("Tenant")
        End Get
        Set(ByVal value As String)
            _itemRow("Tenant") = Truncate(value, 51)
        End Set
    End Property

    Property FacilitiesManager() As String
        Get
            Return GetItem("FacilitiesManager")
        End Get
        Set(ByVal value As String)
            _itemRow("FacilitiesManager") = Truncate(value, 451)
        End Set
    End Property

    Property EnergyChampion() As String
        Get
            Return GetItem("EnergyChampion")
        End Get
        Set(ByVal value As String)
            _itemRow("EnergyChampion") = Truncate(value, 451)
        End Set
    End Property

    Property MainSectorBenchmark() As String
        Get
            Return GetItem("MainSectorBenchmark")
        End Get
        Set(ByVal value As String)
            _itemRow("MainSectorBenchmark") = Truncate(value, 151)
        End Set
    End Property

    Property SubSectorBenchmark() As String
        Get
            Return GetItem("SubSectorBenchmark")
        End Get
        Set(ByVal value As String)
            _itemRow("SubSectorBenchmark") = Truncate(value, 151)
        End Set
    End Property

    Property TotalFloorArea() As String
        Get
            Return GetItem("TotalFloorArea")
        End Get
        Set(ByVal value As String)
            _itemRow("TotalFloorArea") = Truncate(value, 20)
        End Set
    End Property

    Property NumberOfOccupants() As String
        Get
            Return GetItem("NumberOfOccupants")
        End Get
        Set(ByVal value As String)
            _itemRow("NumberOfOccupants") = Truncate(value, 20)
        End Set
    End Property

    Property NumberOfPupils() As String
        Get
            Return GetItem("NumberOfPupils")
        End Get
        Set(ByVal value As String)
            _itemRow("NumberOfPupils") = Truncate(value, 20)
        End Set
    End Property

    Property AnnualHoursOfOccupancy() As String
        Get
            Return GetItem("AnnualHoursOfOccupancy")
        End Get
        Set(ByVal value As String)
            _itemRow("AnnualHoursOfOccupancy") = Truncate(value, 20)
        End Set
    End Property

    Property MainHeatingSystem() As String
        Get
            Return GetItem("MainHeatingSystem")
        End Get
        Set(ByVal value As String)
            _itemRow("MainHeatingSystem") = Truncate(value, 51)
        End Set
    End Property

    Property MainHeatingFuel() As String
        Get
            Return GetItem("MainHeatingFuel")
        End Get
        Set(ByVal value As String)
            _itemRow("MainHeatingFuel") = Truncate(value, 50)
        End Set
    End Property

    Property HeatingSetpoint() As String
        Get
            Return GetItem("HeatingSetpoint")
        End Get
        Set(ByVal value As String)
            _itemRow("HeatingSetpoint") = Truncate(value, 20)
        End Set
    End Property

    Property CoolingSetpoint() As String
        Get
            Return GetItem("CoolingSetpoint")
        End Get
        Set(ByVal value As String)
            _itemRow("CoolingSetpoint") = Truncate(value, 20)
        End Set
    End Property

    Property PropertyRef() As String
        Get
            Return GetItem("PropertyRef")
        End Get
        Set(ByVal value As String)
            _itemRow("PropertyRef") = Truncate(value, 51)
        End Set
    End Property

    Property BuiltDate() As String
        Get
            Return GetItem("BuiltDate")
        End Get
        Set(ByVal value As String)
            _itemRow("BuiltDate") = Truncate(value, 10)
        End Set
    End Property

    Property Subdivision() As String
        Get
            Return GetItem("Subdivision")
        End Get
        Set(ByVal value As String)
            _itemRow("Subdivision") = Truncate(value, 10)
        End Set
    End Property

    Property EPCGrade() As String
        Get
            Return GetItem("EPCGrade")
        End Get
        Set(ByVal value As String)
            _itemRow("EPCGrade") = Truncate(value, 50)
        End Set
    End Property

    Property EPCRating() As String
        Get
            Return GetItem("EPCRating")
        End Get
        Set(ByVal value As String)
            _itemRow("EPCRating") = Truncate(value, 20)
        End Set
    End Property

    Property EPCOther() As String
        Get
            Return GetItem("EPCOther")
        End Get
        Set(ByVal value As String)
            _itemRow("EPCOther") = Truncate(value, 201)
        End Set
    End Property

    Property Notes() As String
        Get
            Return GetItem("Notes")
        End Get
        Set(ByVal value As String)
            _itemRow("Notes") = Truncate(value, 751)
        End Set
    End Property

    Property RateableValue() As String
        Get
            Return GetItem("RateableValue")
        End Get
        Set(ByVal value As String)
            _itemRow("RateableValue") = Truncate(value, 20)
        End Set
    End Property

    Property NetInternalArea() As String
        Get
            Return GetItem("NetInternalArea")
        End Get
        Set(ByVal value As String)
            _itemRow("NetInternalArea") = Truncate(value, 20)
        End Set
    End Property

    Property Category() As String
        Get
            Return GetItem("Category")
        End Get
        Set(ByVal value As String)
            _itemRow("Category") = Truncate(value, 201)
        End Set
    End Property

    Property PropertyManager() As String
        Get
            Return GetItem("PropertyManager")
        End Get
        Set(ByVal value As String)
            _itemRow("PropertyManager") = Truncate(value, 451)
        End Set
    End Property

    Property PropertySurveyor() As String
        Get
            Return GetItem("PropertySurveyor")
        End Get
        Set(ByVal value As String)
            _itemRow("PropertySurveyor") = Truncate(value, 451)
        End Set
    End Property

    Property Administrator() As String
        Get
            Return GetItem("Administrator")
        End Get
        Set(ByVal value As String)
            _itemRow("Administrator") = Truncate(value, 451)
        End Set
    End Property

    Property AssetCategory() As String
        Get
            Return GetItem("AssetCategory")
        End Get
        Set(ByVal value As String)
            _itemRow("AssetCategory") = Truncate(value, 50)
        End Set
    End Property

    Property BudgetCode() As String
        Get
            Return GetItem("BudgetCode")
        End Get
        Set(ByVal value As String)
            _itemRow("BudgetCode") = Truncate(value, 50)
        End Set
    End Property

    Property Division() As String
        Get
            Return GetItem("Division")
        End Get
        Set(ByVal value As String)
            _itemRow("Division") = Truncate(value, 50)
        End Set
    End Property

    Property PropertyType() As String
        Get
            Return GetItem("PropertyType")
        End Get
        Set(ByVal value As String)
            _itemRow("PropertyType") = Truncate(value, 50)
        End Set
    End Property

    Property Service() As String
        Get
            Return GetItem("Service")
        End Get
        Set(ByVal value As String)
            _itemRow("Service") = Truncate(value, 50)
        End Set
    End Property
    Property BuildingCategory() As String
        Get
            Return GetItem("BuildingCategory")
        End Get
        Set(ByVal value As String)
            _itemRow("BuildingCategory") = Truncate(value, 50)
        End Set
    End Property

#End Region

#Region "Project"

    ' Property UPRN() As String is defined in "Advanced Building"

    Property ProjectName() As String
        Get
            Return GetItem("ProjectName")
        End Get
        Set(ByVal value As String)
            _itemRow("ProjectName") = Truncate(value, 501)
        End Set
    End Property

    Property Description() As String
        Get
            Return GetItem("Description")
        End Get
        Set(ByVal value As String)
            _itemRow("Description") = Truncate(value, 4001)
        End Set
    End Property

    Property Rationale() As String
        Get
            Return GetItem("Rationale")
        End Get
        Set(ByVal value As String)
            _itemRow("Rationale") = Truncate(value, 4001)
        End Set
    End Property

    Property Risks() As String
        Get
            Return GetItem("Risks")
        End Get
        Set(ByVal value As String)
            _itemRow("Risks") = Truncate(value, 2501)
        End Set
    End Property

    Property FuelType() As String
        Get
            Return GetItem("FuelType")
        End Get
        Set(ByVal value As String)
            _itemRow("FuelType") = Truncate(value, 51)
        End Set
    End Property

    Property AnnualEnergySaving() As String
        Get
            Return GetItem("AnnualEnergySaving")
        End Get
        Set(ByVal value As String)
            _itemRow("AnnualEnergySaving") = Truncate(value, 20)
        End Set
    End Property

    Property AnnualCarbon() As String
        Get
            Return GetItem("AnnualCarbon")
        End Get
        Set(ByVal value As String)
            _itemRow("AnnualCarbon") = Truncate(value, 20)
        End Set
    End Property

    Property AnnualCostSaving() As String
        Get
            Return GetItem("AnnualCostSaving")
        End Get
        Set(ByVal value As String)
            _itemRow("AnnualCostSaving") = Truncate(value, 20)
        End Set
    End Property

    Property EstimatedCost() As String
        Get
            Return GetItem("EstimatedCost")
        End Get
        Set(ByVal value As String)
            _itemRow("EstimatedCost") = Truncate(value, 20)
        End Set
    End Property

    Property ProjectSpecificUnitCost() As String
        Get
            Return GetItem("ProjectSpecificUnitCost")
        End Get
        Set(ByVal value As String)
            _itemRow("ProjectSpecificUnitCost") = Truncate(value, 20)
        End Set
    End Property

    Property PaybackPeriod() As String
        Get
            Return GetItem("PaybackPeriod")
        End Get
        Set(ByVal value As String)
            _itemRow("PaybackPeriod") = Truncate(value, 20)
        End Set
    End Property

    Property LifeExpectancy() As String
        Get
            Return GetItem("LifeExpectancy")
        End Get
        Set(ByVal value As String)
            _itemRow("LifeExpectancy") = Truncate(value, 20)
        End Set
    End Property

    Property ProjectOwner() As String
        Get
            Return GetItem("ProjectOwner")
        End Get
        Set(ByVal value As String)
            _itemRow("ProjectOwner") = Truncate(value, 451)
        End Set
    End Property

    Property OwnerRole() As String
        Get
            Return GetItem("OwnerRole")
        End Get
        Set(ByVal value As String)
            _itemRow("OwnerRole") = Truncate(value, 101)
        End Set
    End Property

    Property ProjectStartDate() As String
        Get
            Return GetItem("ProjectStartDate")
        End Get
        Set(ByVal value As String)
            _itemRow("ProjectStartDate") = Truncate(value, 11)
        End Set
    End Property

    Property ProjectCompletionDate() As String
        Get
            Return GetItem("ProjectCompletionDate")
        End Get
        Set(ByVal value As String)
            _itemRow("ProjectCompletionDate") = Truncate(value, 11)
        End Set
    End Property

    Property BaselineStartDate() As String
        Get
            Return GetItem("BaselineStartDate")
        End Get
        Set(ByVal value As String)
            _itemRow("BaselineStartDate") = Truncate(value, 11)
        End Set
    End Property

    Property BaselineEndDate() As String
        Get
            Return GetItem("BaselineEndDate")
        End Get
        Set(ByVal value As String)
            _itemRow("BaselineEndDate") = Truncate(value, 11)
        End Set
    End Property

    Property CurrentStatus() As String
        Get
            Return GetItem("CurrentStatus")
        End Get
        Set(ByVal value As String)
            _itemRow("CurrentStatus") = Truncate(value, 51)
        End Set
    End Property

    Property TechnologyGroup() As String
        Get
            Return GetItem("TechnologyGroup")
        End Get
        Set(ByVal value As String)
            _itemRow("TechnologyGroup") = Truncate(value, 256)
        End Set
    End Property

    Property MainTechnology() As String
        Get
            Return GetItem("MainTechnology")
        End Get
        Set(ByVal value As String)
            _itemRow("MainTechnology") = Truncate(value, 256)
        End Set
    End Property

    Property SubTechnology() As String
        Get
            Return GetItem("SubTechnology")
        End Get
        Set(ByVal value As String)
            _itemRow("SubTechnology") = Truncate(value, 256)
        End Set
    End Property

    Property TypeOfAction() As String
        Get
            Return GetItem("TypeOfAction")
        End Get
        Set(ByVal value As String)
            _itemRow("TypeOfAction") = Truncate(value, 256)
        End Set
    End Property

    Property ImplementationSteps() As String
        Get
            Return GetItem("ImplementationSteps")
        End Get
        Set(ByVal value As String)
            _itemRow("ImplementationSteps") = Truncate(value, 2501)
        End Set
    End Property

    Property RelatedInformation() As String
        Get
            Return GetItem("RelatedInformation")
        End Get
        Set(ByVal value As String)
            _itemRow("RelatedInformation") = Truncate(value, 2501)
        End Set
    End Property

    Property RelatedWebLink1() As String
        Get
            Return GetItem("RelatedWebLink1")
        End Get
        Set(ByVal value As String)
            _itemRow("RelatedWebLink1") = Truncate(value, 101)
        End Set
    End Property

    Property RelatedWebLink2() As String
        Get
            Return GetItem("RelatedWebLink2")
        End Get
        Set(ByVal value As String)
            _itemRow("RelatedWebLink2") = Truncate(value, 101)
        End Set
    End Property

#End Region

#Region "Project"

    ' Property ProjectName() As String is defined in "Project"

    Property SerialNumber() As String
        Get
            Return GetItem("SerialNumber")
        End Get
        Set(ByVal value As String)
            _itemRow("SerialNumber") = Truncate(value, 51)
        End Set
    End Property

    Property Remove() As String
        Get
            Return GetItem("Remove")
        End Get
        Set(ByVal value As String)
            _itemRow("Remove") = Truncate(value, 4)
        End Set
    End Property

#End Region

    Property TimeStamp() As String
        Get
            Return GetItem("DateTime")
        End Get
        Set(ByVal value As String)
            _itemRow("DateTime") = value
        End Set
    End Property

    Property MeterName() As String
        Get
            Return GetItem("MeterName")

        End Get
        Set(ByVal value As String)

            _itemRow("MeterName") = value

        End Set
    End Property

    Property MeterId() As Integer
        Get
            Return GetItem("MeterId")
        End Get
        Set(ByVal value As Integer)
            _itemRow("MeterId") = value
        End Set
    End Property

    Property CountUnits() As Object
        Get
            Return GetItem("CountUnits")
        End Get
        Set(ByVal value As Object)
            _itemRow("CountUnits") = value
        End Set
    End Property

    Private Function Truncate(ByRef theString As String, ByVal atlength As Integer) As String
        Try
            If theString IsNot Nothing Then
                If theString.Length > atlength Then
                    Return theString.Substring(0, atlength)
                End If
            End If
            Return theString

        Catch ex As Exception
            Return theString

        End Try

    End Function

#Region "MeterImport"
    Property BuildingUPRN() As String
        Get
            Return GetItem("BuildingUPRN")
        End Get
        Set(ByVal value As String)
            _itemRow("BuildingUPRN") = value
        End Set
    End Property
    Property Serial_Number() As String
        Get
            Return GetItem("SerialNumber")
        End Get
        Set(ByVal value As String)
            _itemRow("SerialNumber") = value
        End Set
    End Property
    Property Account_Number() As String
        Get
            Return GetItem("AccountNumber")
        End Get
        Set(ByVal value As String)
            _itemRow("AccountNumber") = value
        End Set
    End Property
    Property MeterFuel() As String
        Get
            Return GetItem("MeterFuel")
        End Get
        Set(ByVal value As String)
            _itemRow("MeterFuel") = value
        End Set
    End Property
    Property Units() As String
        Get
            Return GetItem("Units")
        End Get
        Set(ByVal value As String)
            _itemRow("Units") = value
        End Set
    End Property

    Property AMR() As String
        Get
            Return GetItem("AMR")
        End Get
        Set(ByVal value As String)
            _itemRow("AMR") = value
        End Set
    End Property
    Property AMRInstalledDate() As String
        Get
            Return GetItem("AMRInstalledDate")
        End Get
        Set(ByVal value As String)
            _itemRow("AMRInstalledDate") = value
        End Set
    End Property
    Property IsActive() As String
        Get
            Return GetItem("IsActive")
        End Get
        Set(ByVal value As String)
            _itemRow("IsActive") = value
        End Set
    End Property
    Property ReportingMeter() As String
        Get
            Return GetItem("ReportingMeter")
        End Get
        Set(ByVal value As String)
            _itemRow("ReportingMeter") = value
        End Set
    End Property
    Property CRCMeterType() As String
        Get
            Return GetItem("CRCMeterType")
        End Get
        Set(ByVal value As String)
            _itemRow("CRCMeterType") = value
        End Set
    End Property
    Property CRCFuelType() As String
        Get
            Return GetItem("CRCFuelType")
        End Get
        Set(ByVal value As String)
            _itemRow("CRCFuelType") = value
        End Set
    End Property
    Property IncludeAsCRCResidual() As String
        Get
            Return GetItem("IncludeAsCRCResidual")
        End Get
        Set(ByVal value As String)
            _itemRow("IncludeAsCRCResidual") = value
        End Set
    End Property
    Property CarbonTrustStandard() As String
        Get
            Return GetItem("CarbonTrustStandard")
        End Get
        Set(ByVal value As String)
            _itemRow("CarbonTrustStandard") = value
        End Set
    End Property
    Property OperatingSchedule1() As String
        Get
            Return GetItem("OperatingSchedule1")
        End Get
        Set(ByVal value As String)
            _itemRow("OperatingSchedule1") = value
        End Set
    End Property
    Property OperatingSchedule2() As String
        Get
            Return GetItem("OperatingSchedule2")
        End Get
        Set(ByVal value As String)
            _itemRow("OperatingSchedule2") = value
        End Set
    End Property
    Property CTSStartDate() As String
        Get
            Return GetItem("CTSStartDate")
        End Get
        Set(ByVal value As String)
            _itemRow("CTSStartDate") = value
        End Set
    End Property
    Property CalorificValue() As String
        Get
            Return GetItem("CalorificValue")
        End Get
        Set(ByVal value As String)
            _itemRow("CalorificValue") = value
        End Set
    End Property
    Property DateInstalled() As String
        Get
            Return GetItem("DateInstalled")
        End Get
        Set(ByVal value As String)
            _itemRow("DateInstalled") = value
        End Set
    End Property
    Property DateCommissioned() As String
        Get
            Return GetItem("DateCommissioned")
        End Get
        Set(ByVal value As String)
            _itemRow("DateCommissioned") = value
        End Set
    End Property
    Property IsStockMeter() As String
        Get
            Return GetItem("IsStockMeter")
        End Get
        Set(ByVal value As String)
            _itemRow("IsStockMeter") = value
        End Set
    End Property
    Property InvoiceFrequency() As String
        Get
            Return GetItem("InvoiceFrequency")
        End Get
        Set(ByVal value As String)
            _itemRow("InvoiceFrequency") = value
        End Set
    End Property
    Property Location() As String
        Get
            Return GetItem("Location")
        End Get
        Set(ByVal value As String)
            _itemRow("Location") = value
        End Set
    End Property

    Property CorrectionFactor() As String
        Get
            Return GetItem("CorrectionFactor")
        End Get
        Set(ByVal value As String)
            _itemRow("CorrectionFactor") = value
        End Set
    End Property
    Property ConversionFactor() As String
        Get
            Return GetItem("ConversionFactor")
        End Get
        Set(ByVal value As String)
            _itemRow("ConversionFactor") = value
        End Set
    End Property
    Property CustomMultiplier() As String
        Get
            Return GetItem("CustomMultiplier")
        End Get
        Set(ByVal value As String)
            _itemRow("CustomMultiplier") = value
        End Set
    End Property
    Property ValidateReadings() As String
        Get
            Return GetItem("ValidateReadings")
        End Get
        Set(ByVal value As String)
            _itemRow("ValidateReadings") = value
        End Set
    End Property
    Property ReadingWrapRound() As String
        Get
            Return GetItem("ReadingWrapRound")
        End Get
        Set(ByVal value As String)
            _itemRow("ReadingWrapRound") = value
        End Set
    End Property
    Property MaxReadConsumption() As String
        Get
            Return GetItem("MaxReadConsumption")
        End Get
        Set(ByVal value As String)
            _itemRow("MaxReadConsumption") = value
        End Set
    End Property
    Property RealTime() As String
        Get
            Return GetItem("RealTime")
        End Get
        Set(ByVal value As String)
            _itemRow("RealTime") = value
        End Set
    End Property
    Property WrapRoundValue() As String
        Get
            Return GetItem("WrapRoundValue")
        End Get
        Set(ByVal value As String)
            _itemRow("WrapRoundValue") = value
        End Set
    End Property
    Property IsCumulative() As String
        Get
            Return GetItem("IsCumulative")
        End Get
        Set(ByVal value As String)
            _itemRow("IsCumulative") = value
        End Set
    End Property
    Property IsRaw() As String
        Get
            Return GetItem("IsRaw")
        End Get
        Set(ByVal value As String)
            _itemRow("IsRaw") = value
        End Set
    End Property
    Property MinEntry() As String
        Get
            Return GetItem("MinEntry")
        End Get
        Set(ByVal value As String)
            _itemRow("MinEntry") = value
        End Set
    End Property
    Property MaxEntry() As String
        Get
            Return GetItem("MaxEntry")
        End Get
        Set(ByVal value As String)
            _itemRow("MaxEntry") = value
        End Set
    End Property
    Property Multiplier() As String
        Get
            Return GetItem("Multiplier")
        End Get
        Set(ByVal value As String)
            _itemRow("Multiplier") = value
        End Set
    End Property
    Property SerialAlias() As String
        Get
            Return GetItem("SerialAlias")
        End Get
        Set(ByVal value As String)
            _itemRow("SerialAlias") = value
        End Set
    End Property
    Property CreatekWhExportAssociateMeter() As String
        Get
            Return GetItem("CreatekWhExportAssociateMeter")
        End Get
        Set(ByVal value As String)
            _itemRow("CreatekWhExportAssociateMeter") = value
        End Set
    End Property
    Property CreatekVarhExportAssociateMeter() As String
        Get
            Return GetItem("CreatekVarhExportAssociateMeter")
        End Get
        Set(ByVal value As String)
            _itemRow("CreatekVarhExportAssociateMeter") = value
        End Set
    End Property
    Property CreatekVarhAssociateMeter() As String
        Get
            Return GetItem("CreatekVarhAssociateMeter")
        End Get
        Set(ByVal value As String)
            _itemRow("CreatekVarhAssociateMeter") = value
        End Set
    End Property
    Property ExportMPAN() As String
        Get
            Return GetItem("ExportMPAN")
        End Get
        Set(ByVal value As String)
            _itemRow("ExportMPAN") = value
        End Set
    End Property
    Property AuthorisedCapacity() As String
        Get
            Return GetItem("AuthorisedCapacity")
        End Get
        Set(ByVal value As String)
            _itemRow("AuthorisedCapacity") = value
        End Set
    End Property
    Property ExcessCapacity() As String
        Get
            Return GetItem("ExcessCapacity")
        End Get
        Set(ByVal value As String)
            _itemRow("ExcessCapacity") = value
        End Set
    End Property
    Property ParentMeterSerial() As String
        Get
            Return GetItem("ParentMeterSerial")
        End Get
        Set(ByVal value As String)
            _itemRow("ParentMeterSerial") = value
        End Set
    End Property
    Property IsSubMeter() As String
        Get
            Return GetItem("IsSubMeter")
        End Get
        Set(ByVal value As String)
            _itemRow("IsSubMeter") = value
        End Set
    End Property

#End Region

#Region "BuildingImport"
    Property BuildingName() As String
        Get
            Return GetItem("Name")

        End Get
        Set(ByVal value As String)

            _itemRow("Name") = value

        End Set
    End Property
    Property Address1() As String
        Get
            Return GetItem("Address1")

        End Get
        Set(ByVal value As String)

            _itemRow("Address1") = value
        End Set
    End Property
    Property Address2() As String
        Get
            Return GetItem("Address2")

        End Get
        Set(ByVal value As String)

            _itemRow("Address2") = value

        End Set
    End Property
    Property Address3() As String
        Get
            Return GetItem("Address3")

        End Get
        Set(ByVal value As String)

            _itemRow("Address3") = value

        End Set
    End Property
    Property Address4() As String
        Get
            Return GetItem("Address4")

        End Get
        Set(ByVal value As String)

            _itemRow("Address4") = value

        End Set
    End Property
    Property Town() As String
        Get
            Return GetItem("Town")

        End Get
        Set(ByVal value As String)

            _itemRow("Town") = value

        End Set
    End Property
    Property PostCode() As String
        Get
            Return GetItem("PostCode")

        End Get
        Set(ByVal value As String)

            _itemRow("PostCode") = value

        End Set
    End Property
    Property CRCUndertaking() As String
        Get
            Return GetItem("CRCUndertaking")

        End Get
        Set(ByVal value As String)

            _itemRow("CRCUndertaking") = value

        End Set
    End Property
    Property Occupier() As String
        Get
            Return GetItem("Occupier")

        End Get
        Set(ByVal value As String)

            _itemRow("Occupier") = value

        End Set
    End Property
    Property Client() As String
        Get
            Return GetItem("Client")

        End Get
        Set(ByVal value As String)

            _itemRow("Client") = value

        End Set
    End Property
    Property Group() As String
        Get
            Return GetItem("Group")

        End Get
        Set(ByVal value As String)

            _itemRow("Group") = value

        End Set
    End Property

    Property UndertakingId() As String
        Get
            Return GetItem("UndertakingId")

        End Get
        Set(ByVal value As String)

            _itemRow("UndertakingId") = value

        End Set
    End Property

    Property GroupId() As String
        Get
            Return GetItem("GroupId")

        End Get
        Set(ByVal value As String)

            _itemRow("GroupId") = value

        End Set
    End Property

#End Region

#Region "MeterEdit Additional Properties "
    Property CurrentSerialNumber() As String
        Get
            Return GetItem("CurrentSerialNumber")

        End Get
        Set(ByVal value As String)

            _itemRow("CurrentSerialNumber") = value

        End Set
    End Property
    Property NewSerialNumber() As String
        Get
            Return GetItem("NewSerialNumber")

        End Get
        Set(ByVal value As String)

            _itemRow("NewSerialNumber") = value

        End Set
    End Property
    Property MeterDeactivationDate() As String
        Get
            Return GetItem("MeterDeactivationDate")

        End Get
        Set(ByVal value As String)

            _itemRow("MeterDeactivationDate") = value

        End Set
    End Property
    Property ESOSComplianceMethod() As String
        Get
            Return GetItem("ESOSComplianceMethod")

        End Get
        Set(ByVal value As String)

            _itemRow("ESOSComplianceMethod") = value

        End Set
    End Property
    Property DataCollector() As String
        Get
            Return GetItem("DataCollector")

        End Get
        Set(ByVal value As String)

            _itemRow("DataCollector") = value

        End Set
    End Property
    Property TankSize() As String
        Get
            Return GetItem("TankSize")

        End Get
        Set(ByVal value As String)

            _itemRow("TankSize") = value

        End Set
    End Property

    Property ConsumptionSource() As String
        Get
            Return GetItem("ConsumptionSource")

        End Get
        Set(ByVal value As String)

            _itemRow("ConsumptionSource") = value

        End Set
    End Property

    Property CRCConsumptionSource() As String
        Get
            Return GetItem("CRCConsumptionSource")

        End Get
        Set(ByVal value As String)

            _itemRow("CRCConsumptionSource") = value

        End Set
    End Property

    Property TimeZone() As String
        Get
            Return GetItem("TimeZone")

        End Get
        Set(ByVal value As String)

            _itemRow("TimeZone") = value

        End Set
    End Property

    Property MeterCategory() As String
        Get
            Return GetItem("Category")

        End Get
        Set(ByVal value As String)

            _itemRow("Category") = value

        End Set
    End Property

    Property Status() As String
        Get
            Return GetItem("Status")

        End Get
        Set(ByVal value As String)

            _itemRow("Status") = value

        End Set
    End Property

    Property MeterOperator() As String
        Get
            Return GetItem("MeterOperator")

        End Get
        Set(ByVal value As String)

            _itemRow("MeterOperator") = value

        End Set
    End Property

    Property MeterOperatorContact() As String
        Get
            Return GetItem("MeterOperatorContact")

        End Get
        Set(ByVal value As String)

            _itemRow("MeterOperatorContact") = value

        End Set
    End Property

    Property EnableDayNightActivePeriod() As String
        Get
            Return GetItem("EnableDayNightActivePeriod")

        End Get
        Set(ByVal value As String)

            _itemRow("EnableDayNightActivePeriod") = value

        End Set
    End Property

    Property DayPeriod() As String
        Get
            Return GetItem("DayPeriod")

        End Get
        Set(ByVal value As String)

            _itemRow("DayPeriod") = value

        End Set
    End Property

    Property NightPeriod() As String
        Get
            Return GetItem("NightPeriod")

        End Get
        Set(ByVal value As String)

            _itemRow("NightPeriod") = value

        End Set
    End Property

#End Region

#Region "Asset Edit Additional Properties"
    Property CurrentUPRN() As String
        Get
            Return GetItem("CurrentUPRN")

        End Get
        Set(ByVal value As String)

            _itemRow("CurrentUPRN") = value

        End Set
    End Property
    Property NewUPRN() As String
        Get
            Return GetItem("NewUPRN")

        End Get
        Set(ByVal value As String)

            _itemRow("NewUPRN") = value

        End Set
    End Property
#End Region

#Region "EnergyDriverImport"
    Property EnergyDriverReference() As String
        Get
            Return GetItem("EnergyDriverReference")

        End Get
        Set(ByVal value As String)

            _itemRow("EnergyDriverReference") = value

        End Set
    End Property
    Property ED_Date() As String
        Get
            Return GetItem("Date")

        End Get
        Set(ByVal value As String)

            _itemRow("Date") = value

        End Set
    End Property
    WriteOnly Property ED0000() As Object
        Set(ByVal value As Object)
            _itemRow("00:00") = value
        End Set
    End Property
    WriteOnly Property ED0030() As Object
        Set(ByVal value As Object)
            _itemRow("00:30") = value
        End Set
    End Property
    WriteOnly Property ED0100() As Object
        Set(ByVal value As Object)
            _itemRow("01:00") = value
        End Set
    End Property
    WriteOnly Property ED0130() As Object
        Set(ByVal value As Object)
            _itemRow("01:30") = value
        End Set
    End Property
    WriteOnly Property ED0200() As Object
        Set(ByVal value As Object)
            _itemRow("02:00") = value
        End Set
    End Property
    WriteOnly Property ED0230() As Object
        Set(ByVal value As Object)
            _itemRow("02:30") = value
        End Set
    End Property
    WriteOnly Property ED0300() As Object
        Set(ByVal value As Object)
            _itemRow("03:00") = value
        End Set
    End Property
    WriteOnly Property ED0330() As Object
        Set(ByVal value As Object)
            _itemRow("03:30") = value
        End Set
    End Property
    WriteOnly Property ED0400() As Object
        Set(ByVal value As Object)
            _itemRow("04:00") = value
        End Set
    End Property
    WriteOnly Property ED0430() As Object
        Set(ByVal value As Object)
            _itemRow("04:30") = value
        End Set
    End Property
    WriteOnly Property ED0500() As Object
        Set(ByVal value As Object)
            _itemRow("05:00") = value
        End Set
    End Property
    WriteOnly Property ED0530() As Object
        Set(ByVal value As Object)
            _itemRow("05:30") = value
        End Set
    End Property
    WriteOnly Property ED0600() As Object
        Set(ByVal value As Object)
            _itemRow("06:00") = value
        End Set
    End Property
    WriteOnly Property ED0630() As Object
        Set(ByVal value As Object)
            _itemRow("06:30") = value
        End Set
    End Property
    WriteOnly Property ED0700() As Object
        Set(ByVal value As Object)
            _itemRow("07:00") = value
        End Set
    End Property
    WriteOnly Property ED0730() As Object
        Set(ByVal value As Object)
            _itemRow("07:30") = value
        End Set
    End Property
    WriteOnly Property ED0800() As Object
        Set(ByVal value As Object)
            _itemRow("08:00") = value
        End Set
    End Property
    WriteOnly Property ED0830() As Object
        Set(ByVal value As Object)
            _itemRow("08:30") = value
        End Set
    End Property
    WriteOnly Property ED0900() As Object
        Set(ByVal value As Object)
            _itemRow("09:00") = value
        End Set
    End Property
    WriteOnly Property ED0930() As Object
        Set(ByVal value As Object)
            _itemRow("09:30") = value
        End Set
    End Property
    WriteOnly Property ED1000() As Object
        Set(ByVal value As Object)
            _itemRow("10:00") = value
        End Set
    End Property
    WriteOnly Property ED1030() As Object
        Set(ByVal value As Object)
            _itemRow("10:30") = value
        End Set
    End Property
    WriteOnly Property ED1100() As Object
        Set(ByVal value As Object)
            _itemRow("11:00") = value
        End Set
    End Property
    WriteOnly Property ED1130() As Object
        Set(ByVal value As Object)
            _itemRow("11:30") = value
        End Set
    End Property
    WriteOnly Property ED1200() As Object
        Set(ByVal value As Object)
            _itemRow("12:00") = value
        End Set
    End Property
    WriteOnly Property ED1230() As Object
        Set(ByVal value As Object)
            _itemRow("12:30") = value
        End Set
    End Property
    WriteOnly Property ED1300() As Object
        Set(ByVal value As Object)
            _itemRow("13:00") = value
        End Set
    End Property
    WriteOnly Property ED1330() As Object
        Set(ByVal value As Object)
            _itemRow("13:30") = value
        End Set
    End Property
    WriteOnly Property ED1400() As Object
        Set(ByVal value As Object)
            _itemRow("14:00") = value
        End Set
    End Property
    WriteOnly Property ED1430() As Object
        Set(ByVal value As Object)
            _itemRow("14:30") = value
        End Set
    End Property
    WriteOnly Property ED1500() As Object
        Set(ByVal value As Object)
            _itemRow("15:00") = value
        End Set
    End Property
    WriteOnly Property ED1530() As Object
        Set(ByVal value As Object)
            _itemRow("15:30") = value
        End Set
    End Property
    WriteOnly Property ED1600() As Object
        Set(ByVal value As Object)
            _itemRow("16:00") = value
        End Set
    End Property
    WriteOnly Property ED1630() As Object
        Set(ByVal value As Object)
            _itemRow("16:30") = value
        End Set
    End Property
    WriteOnly Property ED1700() As Object
        Set(ByVal value As Object)
            _itemRow("17:00") = value
        End Set
    End Property
    WriteOnly Property ED1730() As Object
        Set(ByVal value As Object)
            _itemRow("17:30") = value
        End Set
    End Property
    WriteOnly Property ED1800() As Object
        Set(ByVal value As Object)
            _itemRow("18:00") = value
        End Set
    End Property
    WriteOnly Property ED1830() As Object
        Set(ByVal value As Object)
            _itemRow("18:30") = value
        End Set
    End Property
    WriteOnly Property ED1900() As Object
        Set(ByVal value As Object)
            _itemRow("19:00") = value
        End Set
    End Property
    WriteOnly Property ED1930() As Object
        Set(ByVal value As Object)
            _itemRow("19:30") = value
        End Set
    End Property
    WriteOnly Property ED2000() As Object
        Set(ByVal value As Object)
            _itemRow("20:00") = value
        End Set
    End Property
    WriteOnly Property ED2030() As Object
        Set(ByVal value As Object)
            _itemRow("20:30") = value
        End Set
    End Property
    WriteOnly Property ED2100() As Object
        Set(ByVal value As Object)
            _itemRow("21:00") = value
        End Set
    End Property
    WriteOnly Property ED2130() As Object
        Set(ByVal value As Object)
            _itemRow("21:30") = value
        End Set
    End Property
    WriteOnly Property ED2200() As Object
        Set(ByVal value As Object)
            _itemRow("22:00") = value
        End Set
    End Property
    WriteOnly Property ED2230() As Object
        Set(ByVal value As Object)
            _itemRow("22:30") = value
        End Set
    End Property
    WriteOnly Property ED2300() As Object
        Set(ByVal value As Object)
            _itemRow("23:00") = value
        End Set
    End Property
    WriteOnly Property ED2330() As Object
        Set(ByVal value As Object)
            _itemRow("23:30") = value
        End Set
    End Property
#End Region

#Region "Operating Schedule"

    Public Property OS_Name() As Object

        Get
            Return GetItem("Name")
        End Get

        Set(ByVal value As Object)
            _itemRow("Name") = value
        End Set

    End Property

    Public Property OS_Date() As Object

        Get
            Return GetItem("Date")
        End Get

        Set(ByVal value As Object)
            _itemRow("Date") = value
        End Set

    End Property

    Public Property OS_DefaultForFuel() As Object

        Get
            Return GetItem("DefaultForFuel")
        End Get

        Set(ByVal value As Object)
            _itemRow("DefaultForFuel") = value
        End Set

    End Property

#End Region

#Region "TariffImport"

    Property MeterSerial() As String
        Get
            Return GetItem("MeterSerial")
        End Get
        Set(ByVal value As String)
            _itemRow("MeterSerial") = value
        End Set
    End Property

    Property TariffMpan() As String
        Get
            Return GetItem("MPAN")
        End Get
        Set(ByVal value As String)
            _itemRow("MPAN") = value
        End Set
    End Property
    Property TariffName() As String
        Get
            Return GetItem("TariffName")
        End Get
        Set(ByVal value As String)
            _itemRow("TariffName") = value
        End Set
    End Property

    Property ContractName() As String
        Get
            Return GetItem("ContractName")
        End Get
        Set(ByVal value As String)
            _itemRow("ContractName") = value
        End Set
    End Property

    Property Price() As String
        Get
            Return GetItem("Price")
        End Get
        Set(ByVal value As String)
            _itemRow("Price") = value
        End Set
    End Property

    Property TariffVat() As String
        Get
            Return GetItem("VAT")
        End Get
        Set(ByVal value As String)
            _itemRow("VAT") = value
        End Set
    End Property
#End Region

#Region "ReadingImport"
    Property ReadingMeterSerial() As String
        Get
            Return GetItem("meterserial")
        End Get
        Set(ByVal value As String)
            _itemRow("meterserial") = value
        End Set
    End Property

    Property ReadingDate() As String
        Get
            Return GetItem("date")
        End Get
        Set(ByVal value As String)
            _itemRow("date") = value
        End Set
    End Property
    Property Reading() As String
        Get
            Return GetItem("reading")
        End Get
        Set(ByVal value As String)
            _itemRow("reading") = value
        End Set
    End Property
    Property ReadingEstimate() As String
        Get
            Return GetItem("estimated")
        End Get
        Set(ByVal value As String)
            _itemRow("estimated") = value
        End Set
    End Property

    Property ReadingIsWrapRound() As String
        Get
            Return GetItem("iswrapround")
        End Get
        Set(ByVal value As String)
            _itemRow("iswrapround") = value
        End Set
    End Property
    Property ReadingIsReset() As String
        Get
            Return GetItem("isreset")
        End Get
        Set(ByVal value As String)
            _itemRow("isreset") = value
        End Set
    End Property
    Property ReadingIsConsumption() As String
        Get
            Return GetItem("isconsumption")
        End Get
        Set(ByVal value As String)
            _itemRow("isconsumption") = value
        End Set
    End Property

    Property ReadingMpan() As String
        Get
            Return GetItem("mpan")
        End Get
        Set(ByVal value As String)
            _itemRow("mpan") = value
        End Set
    End Property
    Property ReadingRegister() As String
        Get
            Return GetItem("register")
        End Get
        Set(ByVal value As String)
            _itemRow("register") = value
        End Set
    End Property
    Property ReadingDialId() As String
        Get
            Return GetItem("dialid")
        End Get
        Set(ByVal value As String)
            _itemRow("dialid") = value
        End Set
    End Property
    Property ReadingUpdatedBy() As String
        Get
            Return GetItem("updatedby")
        End Get
        Set(ByVal value As String)
            _itemRow("updatedby") = value
        End Set
    End Property

#End Region

#Region "SupplierStatement"
    Property Estimated() As String
        Get
            Return GetItem("Estimated")
        End Get
        Set(ByVal value As String)
            _itemRow("Estimated") = value
        End Set
    End Property

    Property kWh() As String
        Get
            Return GetItem("kWh")
        End Get
        Set(ByVal value As String)
            _itemRow("kWh") = value
        End Set
    End Property
#End Region

#Region "TenantImport"
    Property TenantName() As String
        Get
            Return GetItem("TenantName")
        End Get
        Set(ByVal value As String)
            _itemRow("TenantName") = value
        End Set
    End Property

    Property TenantStartDate() As String
        Get
            Return GetItem("StartDate")
        End Get
        Set(ByVal value As String)
            _itemRow("StartDate") = value
        End Set
    End Property

    Property TenantEndDate() As String
        Get
            Return GetItem("EndDate")
        End Get
        Set(ByVal value As String)
            _itemRow("EndDate") = value
        End Set
    End Property
    Property TenantContact() As String
        Get
            Return GetItem("Contact")
        End Get
        Set(ByVal value As String)
            _itemRow("Contact") = value
        End Set
    End Property
    Property TenantUprn() As String
        Get
            Return GetItem("UPRN")
        End Get
        Set(ByVal value As String)
            _itemRow("UPRN") = value
        End Set
    End Property




#End Region

#Region "AssetStatusImport"
    Property AssetStatus() As String
        Get
            Return GetItem("Status")
        End Get
        Set(ByVal value As String)
            _itemRow("Status") = value
        End Set
    End Property

    Property AssetStatusStartDate() As String
        Get
            Return GetItem("StartDate")
        End Get
        Set(ByVal value As String)
            _itemRow("StartDate") = value
        End Set
    End Property

    Property AssetStatusEndDate() As String
        Get
            Return GetItem("EndDate")
        End Get
        Set(ByVal value As String)
            _itemRow("EndDate") = value
        End Set
    End Property
    Property AssetStatusUprn() As String
        Get
            Return GetItem("UPRN")
        End Get
        Set(ByVal value As String)
            _itemRow("UPRN") = value
        End Set
    End Property




#End Region

#Region "AssetCodeImport"
    Property AssetCode() As String
        Get
            Return GetItem("Code")
        End Get
        Set(ByVal value As String)
            _itemRow("Code") = value
        End Set
    End Property

    Property AssetCodeStartDate() As String
        Get
            Return GetItem("StartDate")
        End Get
        Set(ByVal value As String)
            _itemRow("StartDate") = value
        End Set
    End Property

    Property AssetCodeEndDate() As String
        Get
            Return GetItem("EndDate")
        End Get
        Set(ByVal value As String)
            _itemRow("EndDate") = value
        End Set
    End Property
    Property AssetCodeName() As String
        Get
            Return GetItem("CodeName")
        End Get
        Set(ByVal value As String)
            _itemRow("CodeName") = value
        End Set
    End Property

    Property AssetCodeUprn() As String
        Get
            Return GetItem("UPRN")
        End Get
        Set(ByVal value As String)
            _itemRow("UPRN") = value
        End Set
    End Property


#End Region

End Class
