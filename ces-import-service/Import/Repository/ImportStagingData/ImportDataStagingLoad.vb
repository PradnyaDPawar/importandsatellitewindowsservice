﻿



Public Class ImportDataStagingLoad



    Public Shared Function GetEdit(ByVal deTypeId As Integer) As ImportDataStagingEdits

        Dim rtEdits As New ImportDataStagingEdits
        Dim procedure As New StoredProcedure
        Dim param As New Parameter
        procedure.Name = "ImportDataStaging_Schema"
        param.Name = "@DeTypeId"
        param.Value = deTypeId
        procedure.Parameters.Add(param)
        rtEdits.BaseLoad(DbReader.Invoke(procedure).Tables(0))

        'set dbName
        Dim stagingEnum As ImportDataStagingEnum
        stagingEnum = deTypeId
        rtEdits.Items.TableName = stagingEnum.ToString
        Return rtEdits

    End Function




End Class
