﻿
Namespace Repository.Import


    Public Class ImportEditsLoad

        Public Shared Function GetAll() As ImportEdits

            Dim importEdits As New ImportEdits()

            importEdits.BaseLoad(DbReader.Invoke("Import_Duel_L").Tables(0))

            Return importEdits

        End Function

        Public Shared Function GetFirst() As ImportEdits

            Dim importEdits As New ImportEdits()

            importEdits.BaseLoad(DbReader.Invoke("Import_GetFirst").Tables(0))

            Return importEdits

        End Function

    End Class


End Namespace
