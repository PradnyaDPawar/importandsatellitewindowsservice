﻿Imports ces_import_library.Repository
Imports ces_import_library
Imports ces_import_library.Repository.Import
Imports System.IO
Public Class Form1

    Private Const _LOG_FILE As String = "ServiceLog.txt"
    Private Const _APPDATA_DIR As String = "Cloud Energy Software"
    Private Const _APP_DIR As String = "CES Import Service"

    Private Sub Process()
        If Not Me.BackgroundWorker1.IsBusy Then
            Me.BackgroundWorker1.RunWorkerAsync()
        Else
            AddLogMsg("Worker is busy")

        End If
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        If BackgroundWorker1.CancellationPending Then
            e.Cancel = True
            Return
        End If
        AddLogMsg("Worker running")
        DoWork()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As System.Object, _
           ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) _
           Handles BackgroundWorker1.RunWorkerCompleted
        'AddLogMsg("Process complete")
    End Sub

    Private Sub DoWork()
        UpdateWorkerStatusLabel("Starting worker")

        'load config files
        UpdateWorkerStatusLabel("Loading configuration file")
        Dim deConfig As DeConfig = LoadConfig()

        If Not deConfig Is Nothing Then

            'load import records
            Dim importEdits As ImportEdits = Nothing
            Try
                UpdateWorkerStatusLabel("Loading import records")
                importEdits = ImportEditsLoad.GetAll()
            Catch ex As Exception
                AddLogMsg("Unable to load import records.")
            End Try

            If Not importEdits Is Nothing Then
                If importEdits.Count > 0 Then
                    If DoTranspose(importEdits, deConfig.ImportDir) Then
                        UpdateLog(importEdits)
                    End If
                Else
                    AddLogMsg("No files in queue.")

                End If

                UpdateWorkerStatusLabel("Disposing of import edits")
                importEdits.Items.Dispose()
            End If
        End If
        UpdateWorkerStatusLabel("")
    End Sub

    Private Function DoTranspose(ByRef importEdits As ImportEdits, ByVal importDir As String) As Boolean
        UpdateWorkerStatusLabel("Starting Transpose")

        'Get ImportDataStaging Schema
        Dim importEditOne As ImportEdit = importEdits(0)
        Dim dsEdits As ImportDataStagingEdits = Nothing

        Try
            UpdateWorkerStatusLabel("Loading staging table schema")
            dsEdits = ImportDataStagingLoad.GetEdit(importEditOne.DeTypeId)
            UpdateWorkerStatusLabel("Loaded staging table schema")
        Catch ex As Exception
            UpdateWorkerStatusLabel("Unable to load staging table schema")
            AddLogMsg("Unable to load staging table schema.")
            Return False
        End Try

        If Not dsEdits Is Nothing Then
            Dim msg As String
            Try
                Dim current As Integer = 0
                For Each importEdit As ImportEdit In importEdits
                    If BackgroundWorker1.CancellationPending Then
                        AddLogMsg("Worker was cancelled.")
                        dsEdits.Items.Dispose()
                        Return False
                    End If
                    current += 1
                    msg = String.Format("Transposing imortEdit {0} of {1}", current, importEdits.Count)
                    UpdateWorkerStatusLabel(msg)
                    Transpose.Transform(importEdit, dsEdits, importDir)
                    msg = String.Format("Transposed imortEdit {0} of {1}", current, importEdits.Count)
                    UpdateWorkerStatusLabel(msg)
                Next

                'Save Staging
                UpdateWorkerStatusLabel("Updating staging table")

                Dim SaveStatus As Integer = ImportDataStagingSave.Save(dsEdits)
                If SaveStatus <> StatusEnum.In_Queue Then
                    UpdateWorkerStatusLabel("Error updating staging table")
                    InvalidateImportEdits(importEdits, SaveStatus)
                End If

                'Save Import
                UpdateWorkerStatusLabel("Updating import table")
                ImportEditsSave.Save(importEdits)
                UpdateWorkerStatusLabel("Updated import table")

            Finally
                UpdateWorkerStatusLabel("Disposing of staging edits")
                dsEdits.Items.Dispose()
            End Try

        End If

        UpdateWorkerStatusLabel("Done Transpose")
        Return True
    End Function



    Private Sub InvalidateImportEdits(ByRef importEdits As ImportEdits, ByVal saveStatus As Integer)
        For Each importEdit As ImportEdit In importEdits
            importEdit.StatusId = saveStatus
        Next
    End Sub

    Private Function LoadConfig() As DeConfig
        Try
            Dim deConfig As New DeConfig
            Return deConfig.GetConfig
        Catch ex As Exception
            Timer1.Stop()
            AddLogMsg("Fail to load deConfig.")
            Return Nothing
        End Try
    End Function

    Private Sub UpdateLog(ByRef importEdits As ImportEdits)
        Dim importEdit As ImportEdit = importEdits(0)
        AddLogMsg("-----------------------------------------------------")
        AddLogMsg("First Client: " & importEdit.DatabaseName)
        AddLogMsg("First Import Type: " & importEdit.DeType & "-" & importEdit.Format)
        AddLogMsg("Import Files Processed: " & importEdits.Count.ToString)
        AddLogMsg("Import Files Failed: " & importEdits.Items.Select("StatusId = '" & StatusEnum.Fail & "'").Count)
        AddLogMsg("-----------------------------------------------------")
    End Sub

#Region " Event "

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Timer1.Interval = 1000 Then
            Timer1.Interval = 10000 '10 seconds
        End If
        Process()
    End Sub


#End Region

#Region " UI "

    Private Sub AddLogMsg(ByVal aMessage As String)
        Dim appDataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), _APPDATA_DIR)
        ' create the config file path from app data path and file name
        Dim _fullName As String = Path.Combine(Path.Combine(appDataFolder, _APP_DIR), _LOG_FILE)

        Dim fileExists As Boolean = File.Exists(_fullName)
        If (fileExists) Then
            Using sw As New StreamWriter(File.Open(_fullName, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
                sw.WriteLine(DateTime.Now.ToString & " ------ " & aMessage & vbCrLf)
                sw.Close()
            End Using
        Else
            Using sw As New StreamWriter(File.Create(_fullName, FileMode.OpenOrCreate))
                sw.WriteLine(DateTime.Now.ToString & " ------ " & aMessage & vbCrLf)
                sw.Close()
            End Using
        End If
    End Sub

    Private Delegate Sub UpdateWorkerStatusLabelInvoker(ByVal msg As String)
    Private Sub UpdateWorkerStatusLabel(ByVal msg As String)
        AddLogMsg(msg)
    End Sub
#End Region



    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        Timer1.Interval = 1000 '1 second
        Timer1.Start()
    End Sub

    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        If BackgroundWorker1.IsBusy Then
            BackgroundWorker1.CancelAsync()
        End If

        Timer1.Stop()
    End Sub
End Class
